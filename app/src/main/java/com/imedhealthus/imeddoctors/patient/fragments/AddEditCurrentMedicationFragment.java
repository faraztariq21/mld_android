package com.imedhealthus.imeddoctors.patient.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.CustomAutoCompleteAdapter;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.FileUtils;
import com.imedhealthus.imeddoctors.patient.models.CurrentMedication;
import com.imedhealthus.imeddoctors.patient.models.PatientProfile;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddEditCurrentMedicationFragment extends BaseFragment {

    @BindView(R.id.et_name)
    AutoCompleteTextView etName;
    @BindView(R.id.et_reason)
    AutoCompleteTextView etReason;

    @BindView(R.id.et_dose)
    EditText etDose;
    @BindView(R.id.et_frequency)
    EditText etFrequency;
    @BindView(R.id.cb_as_needed)
    CheckBox cbAdNeeded;

    @BindView(R.id.et_doctor_name)
    EditText etDocName;
    @BindView(R.id.et_doctor_phone)
    EditText etDocPhone;

    @BindView(R.id.et_pharmacy_name)
    EditText etPharmacyName;
    @BindView(R.id.et_pharmacy_phone)
    EditText etPharmacyPhone;
    @BindView(R.id.et_notes)
    EditText etNotes;

    private CurrentMedication medication;

    public AddEditCurrentMedicationFragment() {
        medication = SharedData.getInstance().getSelectedCurrentMedication();
    }


    @Override
    public String getName() {
        return "AddEditCurrentMedicationFragment";
    }

    @Override
    public String getTitle() {
        if (medication == null) {
            return "Add Current Medication";
        }else {
            return "Edit Current Medication";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_current_medication, container, false);
        ButterKnife.bind(this, view);

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

        ArrayList<String> medicines = FileUtils.parseFile(context, "medicines.txt", "\n");
        CustomAutoCompleteAdapter medicineAdapter = new CustomAutoCompleteAdapter(context, R.layout.simple_dropdown_item_1line, etName, medicines);
        etName.setAdapter(medicineAdapter);
        etName.setThreshold(1);
        etName.addTextChangedListener(medicineAdapter);

        ArrayList<String> reasons = FileUtils.parseFile(context, "diseases.txt", "\n");
        CustomAutoCompleteAdapter reasonAdapter = new CustomAutoCompleteAdapter(context, R.layout.simple_dropdown_item_1line, etReason, reasons);
        etReason.setAdapter(reasonAdapter);
        etReason.setThreshold(1);
        etReason.addTextChangedListener(reasonAdapter);
    }

    private void setData() {

        if (medication == null) {
            return;
        }

        etName.setText(medication.getName());
        etReason.setText(medication.getReason());

        etDose.setText(medication.getDose());
        etFrequency.setText(medication.getFrequency());
        cbAdNeeded.setChecked(medication.getAsNeeded());

        etDocName.setText(medication.getDoctorName());
        etDocPhone.setText(medication.getDoctorPhone());

        etPharmacyPhone.setText(medication.getPharmacyNumber());
        etPharmacyName.setText(medication.getPharmacyName());
        etNotes.setText(medication.getNotes());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditMedication();
                }
                break;

        }
    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etName.getText().toString().isEmpty() || etReason.getText().toString().isEmpty() ||
                etDose.getText().toString().isEmpty() || etFrequency.getText().toString().isEmpty() ||
                etDocName.getText().toString().isEmpty() || etDocPhone.getText().toString().isEmpty() ||
                etPharmacyName.getText().toString().isEmpty() || etPharmacyPhone.getText().toString().isEmpty() ||
                etNotes.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustMedications(RetrofitJSONResponse response, CurrentMedication updatedMedication) {

        if (medication == null) {//Add

            JSONObject medicationJson = response.optJSONObject("PastMedication");
            if (medicationJson != null) {
                CurrentMedication newMedication = new CurrentMedication(medicationJson);
                PatientProfile profile = SharedData.getInstance().getSelectedPatientProfile();
                profile.getCurrentMedications().add(newMedication);
            }

        }else  {

            medication.setName(updatedMedication.getName());
            medication.setReason(updatedMedication.getReason());

            medication.setDose(updatedMedication.getDose());
            medication.setFrequency(updatedMedication.getFrequency());
            medication.setAsNeeded(updatedMedication.getAsNeeded());

            medication.setDoctorName(updatedMedication.getDoctorName());
            medication.setDoctorPhone(updatedMedication.getDoctorPhone());

            medication.setPharmacyName(updatedMedication.getPharmacyName());
            medication.setPharmacyNumber(updatedMedication.getPharmacyNumber());
            medication.setNotes(updatedMedication.getNotes());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditMedication() {

        AlertUtils.showProgress(getActivity());

        final CurrentMedication updatedMedication = new CurrentMedication();
        if (medication != null) {
            updatedMedication.setId(medication.getId());
        }

        updatedMedication.setName(etName.getText().toString());
        updatedMedication.setReason(etReason.getText().toString());

        updatedMedication.setDose(etDose.getText().toString());
        updatedMedication.setFrequency(etFrequency.getText().toString());
        updatedMedication.setAsNeeded(cbAdNeeded.isChecked());

        updatedMedication.setDoctorName(etDocName.getText().toString());
        updatedMedication.setDoctorPhone(etDocPhone.getText().toString());

        updatedMedication.setPharmacyName(etPharmacyName.getText().toString());
        updatedMedication.setPharmacyNumber(etPharmacyPhone.getText().toString());
        updatedMedication.setNotes(etNotes.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditMedication(userId, updatedMedication, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                adjustMedications(response, updatedMedication);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Medication saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
