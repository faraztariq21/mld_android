package com.imedhealthus.imeddoctors.patient.listeners;

/**
 * Created by Malik Hamid on 1/9/2018.
 */

public interface OnMedicationItemClickListener {
    void onMedicationDetailClick(int idx);
    void onMedicationEditClick(int idx);
    void onMedicationDeleteClick(int idx);
}
