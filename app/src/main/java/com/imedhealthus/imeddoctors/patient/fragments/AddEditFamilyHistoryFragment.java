package com.imedhealthus.imeddoctors.patient.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.patient.models.FamilyHistory;
import com.imedhealthus.imeddoctors.patient.models.PatientProfile;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 2/6/18.
 */

public class AddEditFamilyHistoryFragment extends BaseFragment {

    @BindView(R.id.et_relation)
    EditText etRelation;
    @BindView(R.id.et_deceased_age)
    EditText etDeceasedAge;
    @BindView(R.id.et_medical_condition)
    EditText etMedicalCondition;
    @BindView(R.id.et_notes)
    EditText etNotes;

    private FamilyHistory familyHistory;
    public AddEditFamilyHistoryFragment() {
        familyHistory = SharedData.getInstance().getSelectedFamilyHistory();
    }

    @Override
    public String getName() {
        return "AddEditFamilyHistoryFragment";
    }

    @Override
    public String getTitle() {
        if (familyHistory == null) {
            return "Add Family History";
        }else {
            return "Edit Family History";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_family_history, container, false);
        ButterKnife.bind(this, view);

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    private void setData() {

        if (familyHistory == null) {
            return;
        }

        etRelation.setText(familyHistory.getRelation());
        etDeceasedAge.setText(familyHistory.getDeceasedAge());
        etMedicalCondition.setText(familyHistory.getMedicationCondition());
        etNotes.setText(familyHistory.getNotes());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditFamilyHistory();
                }
                break;

        }
    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etRelation.getText().toString().isEmpty() || etMedicalCondition.getText().toString().isEmpty() ||
                etNotes.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustFamilyHistories(RetrofitJSONResponse response, FamilyHistory updatedFamilyHistory) {

        if (familyHistory == null) {//Add

            JSONObject familyHistoryJson = response.optJSONObject("FamilyHistory");
            if (familyHistoryJson != null) {
                FamilyHistory newFamilyHistory = new FamilyHistory(familyHistoryJson);
                PatientProfile profile = SharedData.getInstance().getSelectedPatientProfile();
                profile.getFamilyHistories().add(newFamilyHistory);
            }

        }else  {

            familyHistory.setRelation(updatedFamilyHistory.getRelation());
            familyHistory.setDeceasedAge(updatedFamilyHistory.getDeceasedAge());
            familyHistory.setMedicationCondition(updatedFamilyHistory.getMedicationCondition());
            familyHistory.setNotes(updatedFamilyHistory.getNotes());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditFamilyHistory() {

        AlertUtils.showProgress(getActivity());

        final FamilyHistory updatedFamilyHistory = new FamilyHistory();
        if (familyHistory != null) {
            updatedFamilyHistory.setId(familyHistory.getId());
        }
        updatedFamilyHistory.setRelation(etRelation.getText().toString());
        updatedFamilyHistory.setDeceasedAge(etDeceasedAge.getText().toString());
        updatedFamilyHistory.setMedicationCondition(etMedicalCondition.getText().toString());
        updatedFamilyHistory.setNotes(etNotes.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditFamilyHistory(userId, updatedFamilyHistory, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                adjustFamilyHistories(response, updatedFamilyHistory);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Family history saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
