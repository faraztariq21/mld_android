package com.imedhealthus.imeddoctors.patient.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 2/6/18.
 */

public class FamilyHistory {

    private String id, relation, deceasedAge, medicationCondition, notes;

    public FamilyHistory() {
    }

    public FamilyHistory(JSONObject jsonObject) {

        id = jsonObject.optString("Id", "");

        relation = jsonObject.optString("Relation", "");
        deceasedAge = jsonObject.optString("DeceasedOrDeathAge", "");
        medicationCondition = jsonObject.optString("MedicalConditionsOrCauseDeath", "");
        notes = jsonObject.optString("Notes", "");

    }

    public static List<FamilyHistory> parseFamilyHistories(JSONArray familyHistoriesJson){

        List<FamilyHistory> familyHistories = new ArrayList<>();

        for(int i = 0; i < familyHistoriesJson.length(); i++){

            JSONObject familyHistoryJson = familyHistoriesJson.optJSONObject(i);
            if (familyHistoryJson == null) {
                continue;
            }
            FamilyHistory familyHistory = new FamilyHistory(familyHistoryJson);
            familyHistories.add(familyHistory);

        }
        return familyHistories;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getDeceasedAge() {
        return deceasedAge;
    }

    public void setDeceasedAge(String deceasedAge) {
        this.deceasedAge = deceasedAge;
    }

    public String getMedicationCondition() {
        return medicationCondition;
    }

    public void setMedicationCondition(String medicationCondition) {
        this.medicationCondition = medicationCondition;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
