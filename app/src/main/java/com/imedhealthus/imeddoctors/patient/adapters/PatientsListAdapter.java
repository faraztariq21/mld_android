package com.imedhealthus.imeddoctors.patient.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.patient.listeners.OnPatientItemClickListener;
import com.imedhealthus.imeddoctors.patient.models.Patient;
import com.imedhealthus.imeddoctors.patient.view_holder.PatientItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class PatientsListAdapter extends RecyclerView.Adapter<PatientItemViewHolder> {

    private List<Patient> patients;
    private WeakReference<TextView> tvNoRecords;
    private WeakReference<OnPatientItemClickListener> onPatientItemClickListener;
    private int idxShowingOptions;

    public PatientsListAdapter(TextView tvNoRecords, OnPatientItemClickListener onPatientItemClickListener) {

        super();

        this.tvNoRecords = new WeakReference<>(tvNoRecords);
        this.onPatientItemClickListener = new WeakReference<>(onPatientItemClickListener);
        idxShowingOptions = -1;

    }

    public void setPatients(List<Patient> patients) {

        this.patients = patients;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }

    public void toggleOptionsAt(int idx) {
        if (idx == this.idxShowingOptions) {
            this.idxShowingOptions = -1;
        }else {
            this.idxShowingOptions = idx;
        }
        notifyDataSetChanged();
    }

    @Override
    public PatientItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_patient, parent, false);

        OnPatientItemClickListener listener = null;
        if (onPatientItemClickListener != null && onPatientItemClickListener.get() != null) {
            listener = onPatientItemClickListener.get();
        }

        return new PatientItemViewHolder(view, listener);

    }

    @Override
    public void onBindViewHolder(PatientItemViewHolder viewHolder, int position) {
        viewHolder.setData(getItem(position), position == idxShowingOptions, position);
    }

    @Override
    public int getItemCount() {
        return patients == null ? 0 : patients.size();
    }

    public Patient getItem(int position) {
        if (position < 0 || position >= patients.size()) {
            return null;
        }
        return patients.get(position);
    }

}
