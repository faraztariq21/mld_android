package com.imedhealthus.imeddoctors.patient.models;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 1/8/2018.
 */

public class Attachment {

    private String id,title, notes, fileUrl,name;
    private long timeStamp;

    public Attachment() {
    }
    public static List<Attachment> getAttachment(JSONArray jsonObject){
        List<Attachment> attachments = new ArrayList<>();
        for(int i=0;i<jsonObject.length();i++){
            try {
                Attachment attachment=new Attachment(jsonObject.getJSONObject(i));
                attachments.add(attachment);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return attachments;
    }
    public Attachment(JSONObject jsonObject) {
        try {
            id = jsonObject.getString("Id");
            name = jsonObject.getString("Name");
            fileUrl = GenericUtils.getImageUrl(jsonObject.getString("Url"));
            title = jsonObject.getString("Title");
            notes = jsonObject.getString("Notes");
            String date = jsonObject.getString("Date");
            if(!date.equalsIgnoreCase("null"))
            timeStamp =GenericUtils.getTimeDateInLong(date);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Attachment(String title, String notes, String fileUrl, long timeStamp) {
        this.title = title;
        this.notes = notes;
        this.fileUrl = fileUrl;
        this.timeStamp = timeStamp;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getTitle() {
        return title;
    }

    public String getNotes() {
        return notes;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public long getTimeStamp() {
        return timeStamp;
    }
}
