package com.imedhealthus.imeddoctors.patient.fragments;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.adapters.CustomCitySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomCountrySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomStateSpinnerAdapter;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.fragments.FragmentSuggestionDialog;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.City;
import com.imedhealthus.imeddoctors.common.models.Country;
import com.imedhealthus.imeddoctors.common.models.ImageSelector;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.State;
import com.imedhealthus.imeddoctors.common.models.SuggestionListItem;
import com.imedhealthus.imeddoctors.common.models.User;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.FileUtils;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.common.utils.UploadImage;
import com.imedhealthus.imeddoctors.patient.models.PersonalInfo;
import com.soundcloud.android.crop.Crop;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class EditPatientPersonalInfoFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener, ImageSelector.OnImageSelectionListener {

    @BindView(R.id.iv_profile)
    ImageView ivProfile;

    @BindView(R.id.sp_prefix)
    Spinner spPrefix;
    @BindView(R.id.et_first_name)
    EditText etFirstName;
    @BindView(R.id.et_last_name)
    EditText etLastName;

    @BindView(R.id.tv_dob)
    TextView tvDob;
    @BindView(R.id.sp_gender)
    Spinner spGender;

    @BindView(R.id.et_occupation)
    EditText etOccupation;
    @BindView(R.id.sp_marital_status)
    Spinner spMaritalStatus;

    @BindView(R.id.et_religion)
    EditText etReligion;
    @BindView(R.id.sp_language)
    Spinner spLanguage;
    @BindView(R.id.sp_blood_group)
    Spinner spBloodGroup;

    @BindView(R.id.et_home_phone)
    EditText etHomePhone;
    @BindView(R.id.et_office_phone)
    EditText etOfficePhone;
    @BindView(R.id.et_personal_phone)
    EditText etPersonalPhone;
    @BindView(R.id.et_zip)
    EditText etZipCode;

    @BindView(R.id.et_primary_address)
    EditText etPrimaryAddress;
    @BindView(R.id.et_secondary_address)
    EditText etSecondaryAddress;

    @BindView(R.id.et_state)
    EditText etState;
    @BindView(R.id.et_city)
    EditText etCity;

    @BindView(R.id.et_country)
    EditText etCountry;
    private PersonalInfo personalInfo;

    private ImageSelector imageSelector;
    private boolean isImageSelected = false;
    private Uri selectedImgUri;
    private CustomStateSpinnerAdapter stateAdapter;
    private CustomCitySpinnerAdapter cityAdapter;
    private List<State> states;
    private List<City> cities;
    private CustomCountrySpinnerAdapter customCountryAdapter;
    private List<Country> countries;
    private Country selectedCountry;
    private City selectedCity;
    private State selectedState;

    public EditPatientPersonalInfoFragment() {
        personalInfo = SharedData.getInstance().getSelectedPatientProfile().getPersonalInfo();
    }


    @Override
    public String getName() {
        return "EditPatientPersonalInfo";
    }

    @Override
    public String getTitle() {
        return "Edit Personal Information";
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_edit_patient_personal_info, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;

    }

    public void setSpinnerData(final String countryId, final String stateId, final String cityId) {

        countries = SharedData.getInstance().getCountriesList();

        int selectedCountryIndex = SharedData.getInstance().getCountryIndexInList(countries, countryId);
        etCountry.setText(countries.get(selectedCountryIndex).getCountryName());


        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {

                states = SharedData.getInstance().getStatesListAgainstCountryByCountryId(countryId);
                int selectedStateIndex = SharedData.getInstance().getStateIndexInList(states, stateId);
                etState.setText(states.get(selectedStateIndex).getSuggestionText());

                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        cities = SharedData.getInstance().getCitiesListByStateId(stateId);
                        int selectedCityIndex = SharedData.getInstance().getCityIndexInList(cities, cityId);
                        etCity.setText(cities.get(selectedCityIndex).getSuggestionText());
                    }
                }, 100);
            }
        }, 100);


    }


    private void initView() {

        countries = SharedData.getInstance().getCountriesList();

        final FragmentSuggestionDialog.FragmentSuggestionDialogListener countrySelectionListener = new FragmentSuggestionDialog.FragmentSuggestionDialogListener() {
            @Override
            public void onSuggestionSelected(int index, SuggestionListItem suggestionListItem) {
                etCountry.setText(suggestionListItem.getSuggestionText());
                selectedCountry = (Country) suggestionListItem.getSuggestionListItem();
                states = SharedData.getInstance().
                        getStatesListAgainstCountryByCountryId(Integer.toString(selectedCountry.countryId));


            }
        };


        final FragmentSuggestionDialog.FragmentSuggestionDialogListener citySelectionListener = new FragmentSuggestionDialog.FragmentSuggestionDialogListener() {
            @Override
            public void onSuggestionSelected(int index, SuggestionListItem suggestionListItem) {
                etCity.setText(suggestionListItem.getSuggestionText());
                selectedCity = (City) suggestionListItem.getSuggestionListItem();
            }
        };

        final FragmentSuggestionDialog.FragmentSuggestionDialogListener stateSelectionListener = new FragmentSuggestionDialog.FragmentSuggestionDialogListener() {
            @Override
            public void onSuggestionSelected(int index, SuggestionListItem suggestionListItem) {
                etState.setText(suggestionListItem.getSuggestionText());
                selectedState = (State) suggestionListItem.getSuggestionListItem();

                cities = SharedData.getInstance().
                        getCitiesListByStateId((selectedState.getStateId()));
            }
        };


        etCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentSuggestionDialog.newInstance(getResources().getString(R.string.select_country), Country.getSuggestions((ArrayList<Country>) countries), false,
                        countrySelectionListener).show(getChildFragmentManager());

            }
        });

        etCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentSuggestionDialog.newInstance(getResources().getString(R.string.select_city), City.getSuggestions((ArrayList<City>) cities), false, citySelectionListener).show(getChildFragmentManager());

            }
        });

        etState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentSuggestionDialog.newInstance(getResources().getString(R.string.select_state), State.getSuggestions((ArrayList<State>) states), false, stateSelectionListener).show(getChildFragmentManager());

            }
        });


        customCountryAdapter = new CustomCountrySpinnerAdapter(getContext(), R.layout.custom_spinner_item, countries);


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    public void setData() {

        if (personalInfo == null) {
            return;
        }

        Glide.with(ApplicationManager.getContext()).load(personalInfo.getImageUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivProfile);

        List<String> prefixes = Arrays.asList(context.getResources().getStringArray(R.array.prefix));
        int idx = prefixes.indexOf(personalInfo.getPrefix());
        if (idx > 0) {
            spPrefix.setSelection(idx);
        }
        etFirstName.setText(personalInfo.getFirstName());
        etLastName.setText(personalInfo.getLastName());

        tvDob.setText(personalInfo.getDobFormatted());
        etOccupation.setText(personalInfo.getOccupation());

        List<String> genders = Arrays.asList(context.getResources().getStringArray(R.array.genders));
        idx = genders.indexOf(personalInfo.getGender());
        if (idx > 0) {
            spGender.setSelection(idx);
        }

        List<String> maritalStatus = Arrays.asList(context.getResources().getStringArray(R.array.marital_status));
        idx = maritalStatus.indexOf(personalInfo.getMaritalStatus());
        if (idx > 0) {
            spMaritalStatus.setSelection(idx);
        }

        etReligion.setText(personalInfo.getReligion());

        List<String> languages = Arrays.asList(context.getResources().getStringArray(R.array.languages));
        idx = languages.indexOf(personalInfo.getLanguages());
        if (idx > 0) {
            spLanguage.setSelection(idx);
        }

        List<String> bloodGroups = Arrays.asList(context.getResources().getStringArray(R.array.blood_groups));
        idx = bloodGroups.indexOf(personalInfo.getBloodGroup());
        if (idx > 0) {
            spBloodGroup.setSelection(idx);
        }

        etHomePhone.setText(personalInfo.getHomePhone());
        etOfficePhone.setText(personalInfo.getOfficePhone());
        etPersonalPhone.setText(personalInfo.getPersonalPhone());
        etZipCode.setText(personalInfo.getZip());

        etPrimaryAddress.setText(personalInfo.getPrimaryAddress());
        etSecondaryAddress.setText(personalInfo.getSecondaryAddress());

        setSpinnerData(personalInfo.getCountryId(), personalInfo.getStateId(), personalInfo.getCityId());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cont_sel_image:
                openImageSelector();
                break;

            case R.id.tv_dob:
                openDatePicker(tvDob.getText().toString(), "dob");
                break;

            case R.id.btn_save_info:
                if (validatePersonalInfo()) {
                    if (isImageSelected) {
                        uploadImage();
                    } else {
                        updatePersonalInfo(personalInfo.getImageUrl().replace(Constants.URLS.BaseApis, ""));
                    }
                }
                break;

        }
    }

    private void openImageSelector() {
        if (imageSelector == null) {
            imageSelector = new ImageSelector(this, getActivity(), this);
        }
        imageSelector.showSourceAlert();

    }

    private void openDatePicker(String dateStr, String tag) {

        Date date;
        if (dateStr.isEmpty()) {
            date = new Date();
        } else {
            date = GenericUtils.unFormatDate(dateStr);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        com.wdullaer.materialdatetimepicker.date.DatePickerDialog datePickerDialog = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setVersion(com.wdullaer.materialdatetimepicker.date.DatePickerDialog.Version.VERSION_2);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.light_blue));

        datePickerDialog.show(((Activity) context).getFragmentManager(), tag);

    }


    //Date picker callback
    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int month, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        tvDob.setText(GenericUtils.formatDate(calendar.getTime()));

    }


    //Image selection
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ImageSelector.CAMERA_REQUEST_CODE:
            case ImageSelector.GALLERY_REQUEST_CODE:
            case Crop.REQUEST_CROP:
                if (imageSelector != null) {
                    imageSelector.onActivityResult(requestCode, resultCode, data);
                }
                break;

        }

    }

    @Override
    public void onImageSelected(Bitmap bitmap, Uri uri, Uri fullImageUri) {

        isImageSelected = true;
        selectedImgUri = uri;

        if (bitmap != null) {
            ivProfile.setImageBitmap(bitmap);
        } else {
            ivProfile.setImageURI(uri);
        }

    }


    //Other
    private boolean validatePersonalInfo() {


        String errorMsg = null;

        if (etFirstName.getText().toString().isEmpty() || etLastName.getText().toString().isEmpty() ||
                etOfficePhone.getText().toString().isEmpty()
                ) {
            errorMsg = "Kindly fill all required fields to continue";
        }/*else if (spGender.getSelectedItemPosition() == 0) {
            errorMsg = "Kindly select a gender";
        }
        else if (spPrefix.getSelectedItemPosition() == 0) {
            errorMsg = "Kindly select a prefix";
        }else if (spMaritalStatus.getSelectedItemPosition() == 0) {
            errorMsg = "Kindly select marital status";
        }else if (spBloodGroup.getSelectedItemPosition() == 0) {
            errorMsg = "Kindly select blood group";
        }
*/
        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }


    private void adjustPersonalInfo(PersonalInfo updatedPersonalInfo) {

        personalInfo.setImageUrl(GenericUtils.getImageUrl(updatedPersonalInfo.getImageUrl()));

        personalInfo.setPrefix(updatedPersonalInfo.getPrefix());
        personalInfo.setFirstName(updatedPersonalInfo.getFirstName());
        personalInfo.setLastName(updatedPersonalInfo.getLastName());

        personalInfo.setDobFormatted(updatedPersonalInfo.getDobFormatted());
        personalInfo.setDob(updatedPersonalInfo.getDob());

        personalInfo.setGender(updatedPersonalInfo.getGender());
        personalInfo.setOccupation(updatedPersonalInfo.getOccupation());
        personalInfo.setMaritalStatus(updatedPersonalInfo.getMaritalStatus());

        personalInfo.setReligion(updatedPersonalInfo.getReligion());
        personalInfo.setLanguages(updatedPersonalInfo.getLanguages());
        personalInfo.setBloodGroup(updatedPersonalInfo.getBloodGroup());

        personalInfo.setHomePhone(updatedPersonalInfo.getHomePhone());
        personalInfo.setOfficePhone(updatedPersonalInfo.getOfficePhone());
        personalInfo.setPersonalPhone(updatedPersonalInfo.getPersonalPhone());
        personalInfo.setZip(updatedPersonalInfo.getZip());

        personalInfo.setPrimaryAddress(updatedPersonalInfo.getPrimaryAddress());
        personalInfo.setSecondaryAddress(updatedPersonalInfo.getSecondaryAddress());

        personalInfo.setCityId(updatedPersonalInfo.getCityId());
        personalInfo.setCityName(updatedPersonalInfo.getCityName());
        personalInfo.setStateId(updatedPersonalInfo.getStateId());
        personalInfo.setStateName(updatedPersonalInfo.getStateName());
        personalInfo.setCountryId(updatedPersonalInfo.getCountryId());
        personalInfo.setCountryName(updatedPersonalInfo.getCountryName());

        User user = CacheManager.getInstance().getCurrentUser();
        user.setImageUrl(GenericUtils.getImageUrl(updatedPersonalInfo.getImageUrl()));
        user.setFirstName(updatedPersonalInfo.getFirstName());
        user.setLastName(updatedPersonalInfo.getLastName());
        CacheManager.getInstance().setLoggedInUser(user);

        SharedData.getInstance().setRefreshRequired(true);

    }

    //Web services
    private void uploadImage() {

        AlertUtils.showProgress(getActivity());

        final String uploadPath = Constants.FTP.patientPath + (new Date()).getTime() + ".png";

        UploadImage uploadImage = new UploadImage(uploadPath, FileUtils.getPathFromUri(getActivity(), selectedImgUri), new UploadImage.OnImageUploadCompletionListener() {
            @Override
            public void onImageUploadComplete(boolean success, int index, final String uploadPath) {

                if (!success) {
                    AlertUtils.dismissProgress();
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, "Can't upload image now. Kindly try later.");
                    return;
                }

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        updatePersonalInfo(uploadPath);
                    }
                });

            }
        });
        uploadImage.execute();

    }

    private void updatePersonalInfo(String imageUrl) {

        if (!AlertUtils.isShowingProgress()) {
            AlertUtils.showProgress(getActivity());
        }

        final PersonalInfo updatedPersonalInfo = new PersonalInfo();

        updatedPersonalInfo.setImageUrl(imageUrl);
        updatedPersonalInfo.setEmail(personalInfo.getEmail());

        updatedPersonalInfo.setPrefix(spPrefix.getSelectedItemPosition() == 0 ? "" : spPrefix.getSelectedItem().toString());
        updatedPersonalInfo.setFirstName(etFirstName.getText().toString());
        updatedPersonalInfo.setLastName(etLastName.getText().toString());

        updatedPersonalInfo.setDobFormatted(tvDob.getText().toString());
        Date date = GenericUtils.unFormatDate(tvDob.getText().toString());
        updatedPersonalInfo.setDob(GenericUtils.getTimeDateString(date));

        updatedPersonalInfo.setGender(spGender.getSelectedItemPosition() == 0 ? "" : spGender.getSelectedItem().toString());
        updatedPersonalInfo.setOccupation(etOccupation.getText().toString());
        updatedPersonalInfo.setMaritalStatus(spMaritalStatus.getSelectedItemPosition() == 0 ? "" : spMaritalStatus.getSelectedItem().toString());

        updatedPersonalInfo.setReligion(etReligion.getText().toString());
        updatedPersonalInfo.setLanguages(spLanguage.getSelectedItemPosition() == 0 ? "" : spLanguage.getSelectedItem().toString());
        updatedPersonalInfo.setBloodGroup(spBloodGroup.getSelectedItemPosition() == 0 ? "" : spBloodGroup.getSelectedItem().toString());

        updatedPersonalInfo.setHomePhone(etHomePhone.getText().toString());
        updatedPersonalInfo.setOfficePhone(etOfficePhone.getText().toString());
        updatedPersonalInfo.setPersonalPhone(etPersonalPhone.getText().toString());
        updatedPersonalInfo.setZip(etZipCode.getText().toString());

        updatedPersonalInfo.setPrimaryAddress(etPrimaryAddress.getText().toString());
        updatedPersonalInfo.setSecondaryAddress(etSecondaryAddress.getText().toString());

        if (selectedState == null || selectedState.getStateId().equals("0")) {
            updatedPersonalInfo.setStateId("0");
            updatedPersonalInfo.setStateName("");
        } else {
            updatedPersonalInfo.setStateId(selectedState.getStateId());
            updatedPersonalInfo.setStateName(selectedState.getStateName());
        }

        if (selectedCountry == null || Integer.toString(selectedCountry.countryId).equals("0")) {
            updatedPersonalInfo.setCountryId("0");
            updatedPersonalInfo.setCountryName("");
        } else {
            updatedPersonalInfo.setCountryId(Integer.toString(selectedCountry.countryId));
            updatedPersonalInfo.setCountryName(selectedCountry.getCountryName());
        }

        if (selectedCity == null || (selectedCity.getCityId().equals("0"))) {
            updatedPersonalInfo.setCityId("0");
            updatedPersonalInfo.setCityName("");
        } else {
            updatedPersonalInfo.setCityId(selectedCity.getCityId());
            updatedPersonalInfo.setCityName(selectedCity.getCityName());
        }

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();
        String loginId = CacheManager.getInstance().getCurrentUser().getLoginId();

        WebServicesHandler.instance.updatePersonalInfo(userId, loginId, updatedPersonalInfo, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                adjustPersonalInfo(updatedPersonalInfo);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Personal information saved successfully.");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });
    }


}
