package com.imedhealthus.imeddoctors.patient.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.patient.models.PregnancyDetailsHistory;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.patient.models.Allergy;
import com.imedhealthus.imeddoctors.patient.models.CurrentMedication;
import com.imedhealthus.imeddoctors.patient.models.Disease;
import com.imedhealthus.imeddoctors.patient.models.FamilyHistory;
import com.imedhealthus.imeddoctors.patient.models.Hobby;
import com.imedhealthus.imeddoctors.patient.models.Hospitalization;
import com.imedhealthus.imeddoctors.patient.models.PastPharmacy;
import com.imedhealthus.imeddoctors.patient.models.PastPhysician;
import com.imedhealthus.imeddoctors.patient.models.PastSurgery;
import com.imedhealthus.imeddoctors.patient.models.PatientProfile;
import com.imedhealthus.imeddoctors.patient.models.PregnanciesHistory;
import com.imedhealthus.imeddoctors.patient.models.SocialHistory;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 2/6/18.
 */

public class MedicalHistoryFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.iv_add_disease)
    ImageView ivAddDisease;
    @BindView(R.id.cont_diseases)
    LinearLayout contDiseases;
    @BindView(R.id.tv_no_diseases)
    TextView tvNoDiseases;

    @BindView(R.id.iv_add_medication)
    ImageView ivAddMedication;
    @BindView(R.id.cont_medications)
    LinearLayout contMedications;
    @BindView(R.id.tv_no_medications)
    TextView tvNoMedications;

    @BindView(R.id.iv_add_physician)
    ImageView ivAddPhysician;
    @BindView(R.id.cont_physicians)
    LinearLayout contPhysicians;
    @BindView(R.id.tv_no_physicians)
    TextView tvNoPhysicians;

    @BindView(R.id.iv_add_surgery)
    ImageView ivAddSurgery;
    @BindView(R.id.cont_surgeries)
    LinearLayout contSurgeries;
    @BindView(R.id.tv_no_surgeries)
    TextView tvNoSurgeries;

    @BindView(R.id.iv_add_allergy)
    ImageView ivAddAllergy;
    @BindView(R.id.cont_allergies)
    LinearLayout contAllergies;
    @BindView(R.id.tv_no_allergies)
    TextView tvNoAllergies;

    @BindView(R.id.iv_add_pharmacy)
    ImageView ivAddPharmacy;
    @BindView(R.id.cont_pharmacies)
    LinearLayout contPharmacies;
    @BindView(R.id.tv_no_pharmacies)
    TextView tvNoPharmacies;

    @BindView(R.id.iv_add_hospital)
    ImageView ivAddHospital;
    @BindView(R.id.cont_hospitals)
    LinearLayout contHospitals;
    @BindView(R.id.tv_no_hospitals)
    TextView tvNoHospitals;

    @BindView(R.id.iv_add_family_history)
    ImageView ivAddFamilyHistory;
    @BindView(R.id.cont_family_histories)
    LinearLayout contFamilyHistories;
    @BindView(R.id.tv_no_family_histories)
    TextView tvNoFamilyHistories;

    @BindView(R.id.iv_add_hobby)
    ImageView ivAddHobby;
    @BindView(R.id.iv_edit_social_history)
    ImageView ivEditSocialHistory;
    @BindView(R.id.cont_social_history)
    LinearLayout contSocialHistory;
    @BindView(R.id.cont_hobbies)
    LinearLayout contHobbies;
    @BindView(R.id.tv_no_hobbies)
    TextView tvNoHobbies;
    @BindView(R.id.tv_no_social_history)
    TextView tvNoSocialHistory;

    @BindView(R.id.cont_pragnancy)
    RelativeLayout contPregnany;
    @BindView(R.id.iv_edit_pregnancy_history)
    ImageView ivEditPragnanciesHistory;
    @BindView(R.id.cont_pregnancies_history)
    LinearLayout contPregnancies;
    @BindView(R.id.cont_pregnancies_detail)
    LinearLayout contPregnancyDetail;
    @BindView(R.id.tv_no_data_pregnancy)
    TextView tvNoPregnancyData;
    @BindView(R.id.iv_add_pregnancy_detail)
    ImageView ivAddPragnancyDetail;

    private PatientProfile profile;
    private boolean isEditAllowed;

    public static MedicalHistoryFragment create(PatientProfile profile, boolean isEditAllowed) {

        MedicalHistoryFragment fragment = new MedicalHistoryFragment();

        fragment.profile = profile;
        fragment.isEditAllowed = isEditAllowed;

        return fragment;

    }

    @Override
    public String getName() {
        return "MedicalHistoryFragment";
    }

    @Override
    public String getTitle() {
        return "Medical History";
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_medical_history, container, false);
        ButterKnife.bind(this, view);

        adjustView();
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    @Override
    public void onResume() {
        super.onResume();
        if (SharedData.getInstance().isRefreshRequired()) {
            setData();
        }
    }


    private void setData() {

        if (profile == null) {
            return;
        }

        setDiseases(profile.getDiseases());
        setCurrentMedications(profile.getCurrentMedications());
        setPastPhysicians(profile.getPastPhysicians());
        setPastSurgeries(profile.getPastSurgeries());
        setAllergies(profile.getAllergies());
        setPastPharmacies(profile.getPastPharmacies());
        setHospitalizations(profile.getHospitalizations());
        setFamilyHistories(profile.getFamilyHistories());
        setHobbies(profile.getHobbies());
        setSocialHistory(profile.getSocialHistory());

        if(profile.getPersonalInfo().getGender().equalsIgnoreCase("Female")) {
            contPregnany.setVisibility(View.VISIBLE);
            setPragnencyDetail(profile.getPragnancyDetailsHistories());
            setPragnancyHistory(profile.getPregnanciesHistory());
        }else {
            contPregnany.setVisibility(View.GONE);
        }


    }

    private void adjustView() {

        int visibility = isEditAllowed ? View.VISIBLE : View.INVISIBLE;
        View.OnClickListener onClickListener = isEditAllowed ? this : null;

        ivEditSocialHistory.setVisibility(visibility);

        ivAddDisease.setVisibility(visibility);
        ivAddMedication.setVisibility(visibility);
        ivAddPhysician.setVisibility(visibility);

        ivAddSurgery.setVisibility(visibility);
        ivAddAllergy.setVisibility(visibility);
        ivAddPharmacy.setVisibility(visibility);

        ivAddHospital.setVisibility(visibility);
        ivAddFamilyHistory.setVisibility(visibility);
        ivAddHobby.setVisibility(visibility);
        ivAddPragnancyDetail.setVisibility(visibility);
        ivEditPragnanciesHistory.setVisibility(visibility);

        ivAddDisease.setOnClickListener(onClickListener);
        ivAddMedication.setOnClickListener(onClickListener);
        ivAddPhysician.setOnClickListener(onClickListener);

        ivAddSurgery.setOnClickListener(onClickListener);
        ivAddAllergy.setOnClickListener(onClickListener);
        ivAddPharmacy.setOnClickListener(onClickListener);

        ivAddHospital.setOnClickListener(onClickListener);
        ivAddFamilyHistory.setOnClickListener(onClickListener);
        ivAddHobby.setOnClickListener(onClickListener);

        ivEditSocialHistory.setOnClickListener(onClickListener);
        ivEditPragnanciesHistory.setOnClickListener(onClickListener);
        ivAddPragnancyDetail.setOnClickListener(onClickListener);
    }

    private void setDiseases(List<Disease> diseases) {

        contDiseases.removeAllViews();

        for (int i = 0; i < diseases.size(); i++) {

            Disease disease = diseases.get(i);
            View itemDisease = LayoutInflater.from(context).inflate(R.layout.list_item_disease, contDiseases, false);


            ((TextView)itemDisease.findViewById(R.id.tv_name)).setText(disease.getName());
            ((TextView)itemDisease.findViewById(R.id.tv_notes)).setText(disease.getNotes());

            View ivEdit = itemDisease.findViewById(R.id.iv_edit_disease);
            View ivDelete = itemDisease.findViewById(R.id.iv_delete_disease);

            if (isEditAllowed) {

                ivEdit.setTag(i);
                ivEdit.setOnClickListener(this);
                ivDelete.setTag(i);
                ivDelete.setOnClickListener(this);

            }else {

                ivEdit.setVisibility(View.GONE);
                ivDelete.setVisibility(View.GONE);

            }

            contDiseases.addView(itemDisease);

        }

        if (diseases.size() == 0) {
            tvNoDiseases.setVisibility(View.VISIBLE);
        }else {
            tvNoDiseases.setVisibility(View.GONE);
        }

    }

    private void setCurrentMedications(List<CurrentMedication> medications) {

        contMedications.removeAllViews();

        for (int i = 0; i < medications.size(); i++) {

            CurrentMedication medication = medications.get(i);
            View itemMedication = LayoutInflater.from(context).inflate(R.layout.list_item_current_medication, contMedications, false);


            ((TextView)itemMedication.findViewById(R.id.tv_name)).setText(medication.getName());

            String reasonStr = medication.getReason() + "\n" + medication.getDose() + "-";
            if (medication.getAsNeeded()) {
                reasonStr += " As Needed";
            }else {
                reasonStr += medication.getFrequency();
            }
            ((TextView)itemMedication.findViewById(R.id.tv_reason)).setText(reasonStr);

            ((TextView)itemMedication.findViewById(R.id.tv_doctor_info)).setText(medication.getDoctorName() + " (" + medication.getDoctorPhone() + ")");
            ((TextView)itemMedication.findViewById(R.id.tv_pharmacy_info)).setText(medication.getPharmacyName() + " (" + medication.getPharmacyNumber() + ")");

            View ivEdit = itemMedication.findViewById(R.id.iv_edit_current_medication);
            View ivDelete = itemMedication.findViewById(R.id.iv_delete_current_medication);

            if (isEditAllowed) {

                ivEdit.setTag(i);
                ivEdit.setOnClickListener(this);
                ivDelete.setTag(i);
                ivDelete.setOnClickListener(this);

            }else {

                ivEdit.setVisibility(View.GONE);
                ivDelete.setVisibility(View.GONE);

            }

            contMedications.addView(itemMedication);

        }

        if (medications.size() == 0) {
            tvNoMedications.setVisibility(View.VISIBLE);
        }else {
            tvNoMedications.setVisibility(View.GONE);
        }

    }

    private void setPastPhysicians(List<PastPhysician> physicians) {

        contPhysicians.removeAllViews();

        for (int i = 0; i < physicians.size(); i++) {

            PastPhysician physician= physicians.get(i);
            View itemMedication = LayoutInflater.from(context).inflate(R.layout.list_item_past_physician, contPhysicians, false);


            ((TextView)itemMedication.findViewById(R.id.tv_name)).setText(physician.getName() + " (" + physician.getPhone() + ")");

            ((TextView)itemMedication.findViewById(R.id.tv_speciality)).setText(physician.getSpeciality());

            ((TextView)itemMedication.findViewById(R.id.tv_hospital)).setText(physician.getHospital());
            ((TextView)itemMedication.findViewById(R.id.tv_address)).setText(physician.getLocation());

            View ivEdit = itemMedication.findViewById(R.id.iv_edit_past_physician);
            View ivDelete = itemMedication.findViewById(R.id.iv_delete_past_physician);

            if (isEditAllowed) {

                ivEdit.setTag(i);
                ivEdit.setOnClickListener(this);
                ivDelete.setTag(i);
                ivDelete.setOnClickListener(this);

            }else {

                ivEdit.setVisibility(View.GONE);
                ivDelete.setVisibility(View.GONE);

            }

            contPhysicians.addView(itemMedication);

        }

        if (physicians.size() == 0) {
            tvNoPhysicians.setVisibility(View.VISIBLE);
        }else {
            tvNoPhysicians.setVisibility(View.GONE);
        }

    }

    private void setSocialHistory(SocialHistory socialHistory) {

        if(socialHistory==null)
            return;
           contSocialHistory.removeAllViews();

            View itemSocialHistory = LayoutInflater.from(context).inflate(R.layout.list_item_social_history, contSurgeries, false);

            ((TextView)itemSocialHistory.findViewById(R.id.tv_do_smoking)).setText(socialHistory.getSmoking());
            ((TextView)itemSocialHistory.findViewById(R.id.tv_take_drugs)).setText(socialHistory.getDrugs());

            ((TextView)itemSocialHistory.findViewById(R.id.tv_tobacco_title)).setText("User Tobacco ("+socialHistory.getTobacco()+")");
            ((TextView)itemSocialHistory.findViewById(R.id.tv_type)).setText("Use Tobacco("+socialHistory.getTobaccoType()+")");

        ((TextView)itemSocialHistory.findViewById(R.id.tv_alcohol_title)).setText("Drink Alcohol ("+socialHistory.getAlcohol()+")");
        ((TextView)itemSocialHistory.findViewById(R.id.tv_alcohol_type)).setText("Prefereed Drink ("+socialHistory.getAlcoholPreferredDrink()+")");

        ((TextView)itemSocialHistory.findViewById(R.id.tv_recreational_drugs)).setText("Take recreational Drugs("+socialHistory.getRecreationalDrugs()+")");
        ((TextView)itemSocialHistory.findViewById(R.id.tv_hobbies)).setText("Hobbies ("+socialHistory.getHobbiesORLeisureActivities()+")");

        contSocialHistory.addView(itemSocialHistory);



        if (socialHistory == null) {
            tvNoSocialHistory.setVisibility(View.VISIBLE);
        }else {
            tvNoSocialHistory.setVisibility(View.GONE);
        }

    }

    private void setPragnancyHistory(PregnanciesHistory pragnancyHistory) {

        if(pragnancyHistory==null)
            return;
        contPregnancies.removeAllViews();

        View itemSocialHistory = LayoutInflater.from(context).inflate(R.layout.list_item_pragnancy_history, contPregnancies, false);

        ((TextView)itemSocialHistory.findViewById(R.id.tv_do_smoking)).setText("Mammogram on "+GenericUtils.formatDate(new Date(pragnancyHistory.getMammogram())));
        ((TextView)itemSocialHistory.findViewById(R.id.tv_take_drugs)).setText("Breast Exam on "+GenericUtils.formatDate(new Date(pragnancyHistory.getBreastExam())));

        ((TextView)itemSocialHistory.findViewById(R.id.tv_tobacco_title)).setText("Pap Smear on "+GenericUtils.formatDate(new Date(pragnancyHistory.getPapSmear())));

        ((TextView)itemSocialHistory.findViewById(R.id.tv_alcohol_title)).setText("Use Contraception ("+pragnancyHistory.getUseContraception()+")");
        ((TextView)itemSocialHistory.findViewById(R.id.tv_alcohol_type)).setText(" Type ("+pragnancyHistory.getContraceptionType()+")");

        ((TextView)itemSocialHistory.findViewById(R.id.tv_recreational_drugs)).setText("What Was Your Age At First Menses("+pragnancyHistory.getAgeAtFirstMenses()+")");
        ((TextView)itemSocialHistory.findViewById(R.id.tv_hobbies)).setText("Symptoms ("+pragnancyHistory.getSymptoms()+") \n "+"Notes ("+pragnancyHistory.getNotes()+")");

        contPregnancies.addView(itemSocialHistory);



        if (pragnancyHistory == null) {
            tvNoPregnancyData.setVisibility(View.VISIBLE);
        }else {
            tvNoPregnancyData.setVisibility(View.GONE);
        }

    }

    private void setPragnencyDetail(List<PregnancyDetailsHistory> pragnencyDetail) {

        contPregnancyDetail.removeAllViews();
        if(pragnencyDetail==null)
            return;
        for (int i = 0; i < pragnencyDetail.size(); i++) {

            PregnancyDetailsHistory pregnancyDetailsHistory = pragnencyDetail.get(i);
            View itemMedication = LayoutInflater.from(context).inflate(R.layout.list_item_pregnancy_detail, contPharmacies, false);

            ((TextView) itemMedication.findViewById(R.id.tv_number)).setText(String.valueOf(i + 1) + ".");
            ((TextView) itemMedication.findViewById(R.id.tv_name)).setText(GenericUtils.formatDate(new Date(pregnancyDetailsHistory.getDeliveryDate())));
            ((TextView) itemMedication.findViewById(R.id.tv_address)).setText(pregnancyDetailsHistory.getDeliveryType());
            ((TextView) itemMedication.findViewById(R.id.tv_country)).setText("Child Gender:"+pregnancyDetailsHistory.getGender());
            ((TextView) itemMedication.findViewById(R.id.tv_phone)).setText("Number Of pregnancies "+pregnancyDetailsHistory.getNumberOfPregnancies()+"\nTotal Living Child"+pregnancyDetailsHistory.getNumberOfLivingChild());
            ((TextView) itemMedication.findViewById(R.id.tv_fax)).setText("Abortions :"+pregnancyDetailsHistory.getNumberOfAbortions()+" Miscarriages:"+pregnancyDetailsHistory.getNumberOfMiscarriages());

            View ivEdit = itemMedication.findViewById(R.id.iv_edit_pregnancy_detail);
            View ivDelete = itemMedication.findViewById(R.id.iv_delete_pregnancy_detail);

            if (isEditAllowed) {

                ivEdit.setTag(i);
                ivEdit.setOnClickListener(this);
                ivDelete.setTag(i);
                ivDelete.setOnClickListener(this);

            } else {

                ivEdit.setVisibility(View.GONE);
                ivDelete.setVisibility(View.GONE);

            }

            contPregnancyDetail.addView(itemMedication);

        }
    }

    private void setPastSurgeries(List<PastSurgery> surgeries) {

        contSurgeries.removeAllViews();

        for (int i = 0; i < surgeries.size(); i++) {

            PastSurgery surgery = surgeries.get(i);
            View itemMedication = LayoutInflater.from(context).inflate(R.layout.list_item_past_surgery, contSurgeries, false);


            ((TextView)itemMedication.findViewById(R.id.tv_disease)).setText(surgery.getDisease());

            ((TextView)itemMedication.findViewById(R.id.tv_type)).setText(surgery.getType() + " (" + surgery.getYear() + ")");
            ((TextView)itemMedication.findViewById(R.id.tv_surgeon)).setText(surgery.getSurgeonName());

            View ivEdit = itemMedication.findViewById(R.id.iv_edit_past_surgery);
            View ivDelete = itemMedication.findViewById(R.id.iv_delete_past_surgery);

            if (isEditAllowed) {

                ivEdit.setTag(i);
                ivEdit.setOnClickListener(this);
                ivDelete.setTag(i);
                ivDelete.setOnClickListener(this);

            }else {

                ivEdit.setVisibility(View.GONE);
                ivDelete.setVisibility(View.GONE);

            }

            contSurgeries.addView(itemMedication);

        }

        if (surgeries.size() == 0) {
            tvNoSurgeries.setVisibility(View.VISIBLE);
        }else {
            tvNoSurgeries.setVisibility(View.GONE);
        }

    }

    private void setAllergies(List<Allergy> allergies) {

        contAllergies.removeAllViews();

        for (int i = 0; i < allergies.size(); i++) {

            Allergy allergy = allergies.get(i);
            View itemMedication = LayoutInflater.from(context).inflate(R.layout.list_item_allergy, contAllergies, false);


            ((TextView)itemMedication.findViewById(R.id.tv_allergy_to)).setText(allergy.getAllergyTo());
            ((TextView)itemMedication.findViewById(R.id.tv_reaction)).setText(allergy.getReaction());

            View ivEdit = itemMedication.findViewById(R.id.iv_edit_allergy);
            View ivDelete = itemMedication.findViewById(R.id.iv_delete_allergy);

            if (isEditAllowed) {

                ivEdit.setTag(i);
                ivEdit.setOnClickListener(this);
                ivDelete.setTag(i);
                ivDelete.setOnClickListener(this);

            }else {

                ivEdit.setVisibility(View.GONE);
                ivDelete.setVisibility(View.GONE);

            }

            contAllergies.addView(itemMedication);

        }

        if (allergies.size() == 0) {
            tvNoAllergies.setVisibility(View.VISIBLE);
        }else {
            tvNoAllergies.setVisibility(View.GONE);
        }

    }

    private void setPastPharmacies(List<PastPharmacy> pharmacies) {

        contPharmacies.removeAllViews();

        for (int i = 0; i < pharmacies.size(); i++) {

            PastPharmacy pharmacy = pharmacies.get(i);
            View itemMedication = LayoutInflater.from(context).inflate(R.layout.list_item_past_pharmacy, contPharmacies, false);


            ((TextView)itemMedication.findViewById(R.id.tv_name)).setText(pharmacy.getName());
            ((TextView)itemMedication.findViewById(R.id.tv_address)).setText(pharmacy.getAddress());
            ((TextView)itemMedication.findViewById(R.id.tv_country)).setText(pharmacy.getAddressDetails());
            ((TextView)itemMedication.findViewById(R.id.tv_phone)).setText(pharmacy.getPhoneNumber());
            ((TextView)itemMedication.findViewById(R.id.tv_fax)).setText(pharmacy.getFaxNumber());

            View ivEdit = itemMedication.findViewById(R.id.iv_edit_past_pharmacy);
            View ivDelete = itemMedication.findViewById(R.id.iv_delete_past_pharmacy);

            if (isEditAllowed) {

                ivEdit.setTag(i);
                ivEdit.setOnClickListener(this);
                ivDelete.setTag(i);
                ivDelete.setOnClickListener(this);

            }else {

                ivEdit.setVisibility(View.GONE);
                ivDelete.setVisibility(View.GONE);

            }

            contPharmacies.addView(itemMedication);

        }

        if (pharmacies.size() == 0) {
            tvNoPharmacies.setVisibility(View.VISIBLE);
        }else {
            tvNoPharmacies.setVisibility(View.GONE);
        }

    }

    private void setHospitalizations(List<Hospitalization> hospitalizations) {

        contHospitals.removeAllViews();

        for (int i = 0; i < hospitalizations.size(); i++) {

            Hospitalization hospitalization = hospitalizations.get(i);
            View itemMedication = LayoutInflater.from(context).inflate(R.layout.list_item_hospitalization, contHospitals, false);


            ((TextView)itemMedication.findViewById(R.id.tv_reason)).setText(hospitalization.getReason() + " (" + hospitalization.getYear() + ")");
            ((TextView)itemMedication.findViewById(R.id.tv_hospital)).setText(hospitalization.getHospitalName() + " (" + hospitalization.getPhone() + ")");
            ((TextView)itemMedication.findViewById(R.id.tv_address)).setText(hospitalization.getAddress());

            View ivEdit = itemMedication.findViewById(R.id.iv_edit_hospitalization);
            View ivDelete = itemMedication.findViewById(R.id.iv_delete_hospitalization);

            if (isEditAllowed) {

                ivEdit.setTag(i);
                ivEdit.setOnClickListener(this);
                ivDelete.setTag(i);
                ivDelete.setOnClickListener(this);

            }else {

                ivEdit.setVisibility(View.GONE);
                ivDelete.setVisibility(View.GONE);

            }

            contHospitals.addView(itemMedication);

        }

        if (hospitalizations.size() == 0) {
            tvNoHospitals.setVisibility(View.VISIBLE);
        }else {
            tvNoHospitals.setVisibility(View.GONE);
        }

    }

    private void setFamilyHistories(List<FamilyHistory> familyHistories) {

        contFamilyHistories.removeAllViews();

        for (int i = 0; i < familyHistories.size(); i++) {

            FamilyHistory familyHistory = familyHistories.get(i);
            View itemMedication = LayoutInflater.from(context).inflate(R.layout.list_item_familty_history, contFamilyHistories, false);


            ((TextView)itemMedication.findViewById(R.id.tv_relation)).setText(familyHistory.getRelation());

            String str = familyHistory.getMedicationCondition();
            if (!TextUtils.isEmpty(familyHistory.getDeceasedAge())) {
                str += " (" + familyHistory.getDeceasedAge() + ")";
            }
            ((TextView)itemMedication.findViewById(R.id.tv_details)).setText(str);

            View ivEdit = itemMedication.findViewById(R.id.iv_edit_family_history);
            View ivDelete = itemMedication.findViewById(R.id.iv_delete_family_history);

            if (isEditAllowed) {

                ivEdit.setTag(i);
                ivEdit.setOnClickListener(this);
                ivDelete.setTag(i);
                ivDelete.setOnClickListener(this);

            }else {

                ivEdit.setVisibility(View.GONE);
                ivDelete.setVisibility(View.GONE);

            }

            contFamilyHistories.addView(itemMedication);

        }

        if (familyHistories.size() == 0) {
            tvNoFamilyHistories.setVisibility(View.VISIBLE);
        }else {
            tvNoFamilyHistories.setVisibility(View.GONE);
        }

    }

    private void setHobbies(List<Hobby> hobbies) {

        contHobbies.removeAllViews();

        for (int i = 0; i < hobbies.size(); i++) {

            Hobby hobby = hobbies.get(i);
            View itemMedication = LayoutInflater.from(context).inflate(R.layout.list_item_hobby, contHobbies, false);


            ((TextView)itemMedication.findViewById(R.id.tv_name)).setText(hobby.getName());

            View ivEdit = itemMedication.findViewById(R.id.iv_edit_hobby);
            View ivDelete = itemMedication.findViewById(R.id.iv_delete_hobby);

            if (isEditAllowed) {

                ivEdit.setTag(i);
                ivEdit.setOnClickListener(this);
                ivDelete.setTag(i);
                ivDelete.setOnClickListener(this);

            }else {

                ivEdit.setVisibility(View.GONE);
                ivDelete.setVisibility(View.GONE);

            }

            contHobbies.addView(itemMedication);

        }

        if (hobbies.size() == 0) {
            tvNoHobbies.setVisibility(View.VISIBLE);
        }else {
            tvNoHobbies.setVisibility(View.GONE);
        }

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.iv_edit_pregnancy_history:
                openEditPregnancyHistory();
                break;

            case R.id.iv_edit_social_history:
                openEditSocialHistory();
                break;

            case R.id.iv_add_disease:
                openAddDisease();
                break;

            case R.id.iv_edit_disease:
                openEditDisease((Integer) view.getTag());
                break;

            case R.id.iv_delete_disease:
                deleteDisease((Integer) view.getTag());
                break;


            case R.id.iv_add_medication:
                openAddMedication();
                break;

            case R.id.iv_edit_current_medication:
                openEditMedication((Integer) view.getTag());
                break;

            case R.id.iv_delete_current_medication:
                deleteMedication((Integer) view.getTag());
                break;


            case R.id.iv_add_physician:
                openAddPastPhysician();
                break;

            case R.id.iv_edit_past_physician:
                openEditPastPhysician((Integer) view.getTag());
                break;

            case R.id.iv_delete_past_physician:
                deletePastPhysician((Integer) view.getTag());
                break;


            case R.id.iv_add_surgery:
                openAddPastSurgery();
                break;

            case R.id.iv_edit_past_surgery:
                openEditPastSurgery((Integer) view.getTag());
                break;

            case R.id.iv_delete_past_surgery:
                deletePastSurgery((Integer) view.getTag());
                break;


            case R.id.iv_add_pregnancy_detail:
                openAddPragnancyDetail();
                break;

            case R.id.iv_edit_pregnancy_detail:
                openEditPragnancyDetail((Integer) view.getTag());
                break;

            case R.id.iv_delete_pregnancy_detail:
                deletePragnancyDetail((Integer) view.getTag());
                break;

            case R.id.iv_add_allergy:
                openAddAllergy();
                break;

            case R.id.iv_edit_allergy:
                openEditAllergy((Integer) view.getTag());
                break;

            case R.id.iv_delete_allergy:
                deleteAllergy((Integer) view.getTag());
                break;


            case R.id.iv_add_pharmacy:
                openAddPastPharmacy();
                break;

            case R.id.iv_edit_past_pharmacy:
                openEditPastPharmacy((Integer) view.getTag());
                break;

            case R.id.iv_delete_past_pharmacy:
                deletePastPharmacy((Integer) view.getTag());
                break;


            case R.id.iv_add_hospital:
                openAddHospitalization();
                break;

            case R.id.iv_edit_hospitalization:
                openEditHospitalization((Integer) view.getTag());
                break;

            case R.id.iv_delete_hospitalization:
                deleteHospitalization((Integer) view.getTag());
                break;


            case R.id.iv_add_family_history:
                openAddFamilyHistory();
                break;

            case R.id.iv_edit_family_history:
                openEditFamilyHistory((Integer) view.getTag());
                break;

            case R.id.iv_delete_family_history:
                deleteFamilyHistory((Integer) view.getTag());
                break;


            case R.id.iv_add_hobby:
                openAddHobby();
                break;

            case R.id.iv_edit_hobby:
                openEditHobby((Integer) view.getTag());
                break;

            case R.id.iv_delete_hobby:
                deleteHobby((Integer) view.getTag());
                break;
        }
    }

    private void showDeleteAlert(DialogInterface.OnClickListener acceptClickListener) {

        new AlertDialog.Builder(context)
                .setTitle("Confirm")
                .setMessage("Are you sure you want to delete this record?")
                .setPositiveButton("Yes", acceptClickListener)
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }})
                .show();

    }


    //Disease
    private void openAddDisease() {

        SharedData.getInstance().setSelectedDisease(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditDisease.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditSocialHistory() {

        SharedData.getInstance().setSelectedSocialHistory(profile.getSocialHistory());

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditSocialHistory.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }
    private void openEditPregnancyHistory() {

        SharedData.getInstance().setSelectedPregnanciesHistory(profile.getPregnanciesHistory());

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditPregnancyHistory.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }
    private void openEditDisease(int idx) {

        if (idx < 0 || idx >= profile.getDiseases().size()) {
            return;
        }

        Disease disease = profile.getDiseases().get(idx);
        SharedData.getInstance().setSelectedDisease(disease);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditDisease.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteDisease(int idx) {

        if (idx < 0 || idx >= profile.getDiseases().size()) {
            return;
        }

        final Disease disease = profile.getDiseases().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteDisease(disease);

            }
        });

    }


    //Current Medications
    private void openAddMedication() {

        SharedData.getInstance().setSelectedCurrentMedication(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditCurrentMedication.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditMedication(int idx) {

        if (idx < 0 || idx >= profile.getCurrentMedications().size()) {
            return;
        }

        CurrentMedication medication = profile.getCurrentMedications().get(idx);
        SharedData.getInstance().setSelectedCurrentMedication(medication);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditCurrentMedication.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteMedication(int idx) {

        if (idx < 0 || idx >= profile.getCurrentMedications().size()) {
            return;
        }

        final CurrentMedication medication = profile.getCurrentMedications().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteMedication(medication);

            }
        });

    }

    private void openAddPragnancyDetail() {

        SharedData.getInstance().setSelectedPregnancyDetailsHistory(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditPregnancyDetail.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditPragnancyDetail(int idx) {

        if (idx < 0 || idx >= profile.getCurrentMedications().size()) {
            return;
        }

        PregnancyDetailsHistory pregnancyDetailsHistory = profile.getPragnancyDetailsHistories().get(idx);
        SharedData.getInstance().setSelectedPregnancyDetailsHistory(pregnancyDetailsHistory);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditPregnancyDetail.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deletePragnancyDetail(int idx) {

        if (idx < 0 || idx >= profile.getCurrentMedications().size()) {
            return;
        }

        final PregnancyDetailsHistory pregnancyDetailsHistory = profile.getPragnancyDetailsHistories().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deletePragnancyDetail(pregnancyDetailsHistory);

            }
        });

    }
    //Physician
    private void openAddPastPhysician() {

        SharedData.getInstance().setSelectedPastPhysician(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditPastPhysician.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditPastPhysician(int idx) {

        if (idx < 0 || idx >= profile.getPastPhysicians().size()) {
            return;
        }

        PastPhysician physician = profile.getPastPhysicians().get(idx);
        SharedData.getInstance().setSelectedPastPhysician(physician);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditPastPhysician.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deletePastPhysician(int idx) {

        if (idx < 0 || idx >= profile.getPastPhysicians().size()) {
            return;
        }

        final PastPhysician physician = profile.getPastPhysicians().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deletePastPhysician(physician);

            }
        });

    }


    //Surgeries
    private void openAddPastSurgery() {

        SharedData.getInstance().setSelectedPastSurgery(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditPastSurgery.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditPastSurgery(int idx) {

        if (idx < 0 || idx >= profile.getPastSurgeries().size()) {
            return;
        }

        PastSurgery surgery = profile.getPastSurgeries().get(idx);
        SharedData.getInstance().setSelectedPastSurgery(surgery);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditPastSurgery.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deletePastSurgery(int idx) {

        if (idx < 0 || idx >= profile.getPastSurgeries().size()) {
            return;
        }

        final PastSurgery surgery = profile.getPastSurgeries().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deletePastSurgery(surgery);

            }
        });

    }


    //Allergies
    private void openAddAllergy() {

        SharedData.getInstance().setSelectedAllergy(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditAllergy.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditAllergy(int idx) {

        if (idx < 0 || idx >= profile.getAllergies().size()) {
            return;
        }

        Allergy allergy = profile.getAllergies().get(idx);
        SharedData.getInstance().setSelectedAllergy(allergy);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditAllergy.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteAllergy(int idx) {

        if (idx < 0 || idx >= profile.getAllergies().size()) {
            return;
        }

        final Allergy allergy = profile.getAllergies().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteAllergy(allergy);

            }
        });

    }


    //Pharmacies
    private void openAddPastPharmacy() {

        SharedData.getInstance().setSelectedPastPharmacy(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditPastPharmacy.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditPastPharmacy(int idx) {

        if (idx < 0 || idx >= profile.getPastPharmacies().size()) {
            return;
        }

        PastPharmacy pharmacy = profile.getPastPharmacies().get(idx);
        SharedData.getInstance().setSelectedPastPharmacy(pharmacy);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditPastPharmacy.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deletePastPharmacy(int idx) {

        if (idx < 0 || idx >= profile.getPastPharmacies().size()) {
            return;
        }

        final PastPharmacy pharmacy = profile.getPastPharmacies().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deletePastPharmacy(pharmacy);

            }
        });

    }


    //Hospitalizations
    private void openAddHospitalization() {

        SharedData.getInstance().setSelectedHospitalization(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditHospitalization.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditHospitalization(int idx) {

        if (idx < 0 || idx >= profile.getHospitalizations().size()) {
            return;
        }

        Hospitalization hospitalization = profile.getHospitalizations().get(idx);
        SharedData.getInstance().setSelectedHospitalization(hospitalization);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditHospitalization.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteHospitalization(int idx) {

        if (idx < 0 || idx >= profile.getHospitalizations().size()) {
            return;
        }

        final Hospitalization hospitalization = profile.getHospitalizations().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteHospitalization(hospitalization);

            }
        });

    }


    //Family History
    private void openAddFamilyHistory() {

        SharedData.getInstance().setSelectedFamilyHistory(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditFamilyHistory.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditFamilyHistory(int idx) {

        if (idx < 0 || idx >= profile.getFamilyHistories().size()) {
            return;
        }

        FamilyHistory familyHistory = profile.getFamilyHistories().get(idx);
        SharedData.getInstance().setSelectedFamilyHistory(familyHistory);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditFamilyHistory.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteFamilyHistory(int idx) {

        if (idx < 0 || idx >= profile.getFamilyHistories().size()) {
            return;
        }

        final FamilyHistory familyHistory = profile.getFamilyHistories().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteFamilyHistory(familyHistory);

            }
        });

    }


    //Hobbies
    private void openAddHobby() {

        SharedData.getInstance().setSelectedHobby(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditHobby.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditHobby(int idx) {

        if (idx < 0 || idx >= profile.getHobbies().size()) {
            return;
        }

        Hobby hobby = profile.getHobbies().get(idx);
        SharedData.getInstance().setSelectedHobby(hobby);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditHobby.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteHobby(int idx) {

        if (idx < 0 || idx >= profile.getHobbies().size()) {
            return;
        }

        final Hobby hobby = profile.getHobbies().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteHobby(hobby);

            }
        });

    }

    private void deletePragnancyDetail(final PregnancyDetailsHistory pregnancyDetailsHistory) {
        profile.getPragnancyDetailsHistories().remove(pregnancyDetailsHistory);
        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deletePregnancyDetail(pregnancyDetailsHistory, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Medical history deleted successfully");

                List<PregnancyDetailsHistory> pregnancyDetailsHistories = profile.getPragnancyDetailsHistories();
                pregnancyDetailsHistories.remove(pregnancyDetailsHistory);
                setPragnencyDetail(pregnancyDetailsHistories);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });



    }

    //Web services
    private void deleteDisease(final Disease disease) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteDisease(disease, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Medical history deleted successfully");

                List<Disease> diseases = profile.getDiseases();
                diseases.remove(disease);
                setDiseases(diseases);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }


    private void deleteMedication(final CurrentMedication medication) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteMedication(medication, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Current medication deleted successfully");

                List<CurrentMedication> medications = profile.getCurrentMedications();
                medications.remove(medication);
                setCurrentMedications(medications);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void deletePastPhysician(final PastPhysician physician) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deletePastPhysician(physician, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Physician deleted successfully");

                List<PastPhysician> physicians = profile.getPastPhysicians();
                physicians.remove(physician);
                setPastPhysicians(physicians);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void deletePastSurgery(final PastSurgery surgery) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deletePastSurgery(surgery, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Surgical history deleted successfully");

                List<PastSurgery> surgeries = profile.getPastSurgeries();
                surgeries.remove(surgery);
                setPastSurgeries(surgeries);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void deleteAllergy(final Allergy allergy) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteAllergy(allergy, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Allergy history deleted successfully");

                List<Allergy> allergies = profile.getAllergies();
                allergies.remove(allergy);
                setAllergies(allergies);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void deletePastPharmacy(final PastPharmacy pharmacy) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deletePastPharmacy(pharmacy, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Pharmacy information deleted successfully");

                List<PastPharmacy> pharmacies = profile.getPastPharmacies();
                pharmacies.remove(pharmacy);
                setPastPharmacies(pharmacies);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void deleteHospitalization(final Hospitalization hospitalization) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteHospitalization(hospitalization, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Hospitalization information deleted successfully");

                List<Hospitalization> hospitalizations = profile.getHospitalizations();
                hospitalizations.remove(hospitalization);
                setHospitalizations(hospitalizations);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void deleteFamilyHistory(final FamilyHistory familyHistory) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteFamilyHistory(familyHistory, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Family history deleted successfully");

                List<FamilyHistory> familyHistories = profile.getFamilyHistories();
                familyHistories.remove(familyHistory);
                setFamilyHistories(familyHistories);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void deleteHobby(final Hobby hobby) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteHobby(hobby, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Hobby history deleted successfully");

                List<Hobby> hobbies = profile.getHobbies();
                hobbies.remove(hobby);
                setHobbies(hobbies);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

}
