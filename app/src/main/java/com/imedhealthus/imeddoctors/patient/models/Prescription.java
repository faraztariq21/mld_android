package com.imedhealthus.imeddoctors.patient.models;

import com.google.gson.annotations.SerializedName;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 1/11/2018.
 */

public class Prescription {

    @SerializedName("VisitId")
    private String id;

    @SerializedName("PatientId")
    private String patientId;
    @SerializedName("PatientName")
    private String patientName;

    @SerializedName("DoctorId")
    private String doctorId;
    @SerializedName("DoctorName")
    private String doctorName;
    @SerializedName("ProfilePicPath")
    private String doctorProfileImageUrl;

    @SerializedName("PharmacyId")
    private String pharmacyId;

    @SerializedName("SentRequest")
    private String sentRequest;

    @SerializedName("CreatedBy")
    private String createdBy;

    @SerializedName("Notes")
    private String notes;


    @SerializedName("PatientMedication")
    private List<PrescribedMedication> prescribedMedications;


    public Prescription() {

    }

    public String getPharmacyId() {
        return pharmacyId;
    }

    public void setPharmacyId(String pharmacyId) {
        this.pharmacyId = pharmacyId;
    }

    public String getSentRequest() {
        return sentRequest;
    }

    public void setSentRequest(String sentRequest) {
        this.sentRequest = sentRequest;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Prescription(JSONObject prescriptionJson) {

        id = prescriptionJson.optString("VisitId", "");

        patientId = prescriptionJson.optString("PatientId", "");
        patientName = prescriptionJson.optString("PatientName", "");

        doctorId = prescriptionJson.optString("DoctorId", "");
        doctorName = prescriptionJson.optString("DoctorName", "");
        doctorProfileImageUrl = GenericUtils.getImageUrl(prescriptionJson.optString("ProfilePicPath", "").replace("null",""));

        notes = android.text.Html.escapeHtml(prescriptionJson.optString("Notes", "")).toString();

        JSONArray medicationsJson = prescriptionJson.optJSONArray("MedicationList");
        if (medicationsJson == null) {
            prescribedMedications = new ArrayList<>();
        }else {
            prescribedMedications = PrescribedMedication.parsePrescribedMedications(medicationsJson);

        }

    }

    public static List<Prescription> parsePrescriptions(JSONArray prescriptionsJson){

        List<Prescription> prescriptions = new ArrayList<>();

        for(int i = 0; i < prescriptionsJson.length(); i++){

            JSONObject prescriptionJson = prescriptionsJson.optJSONObject(i);
            if (prescriptionJson != null) {
                Prescription prescription = new Prescription(prescriptionJson);
                prescriptions.add(prescription);
            }

        }

        return prescriptions;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDoctorProfileImageUrl() {
        return doctorProfileImageUrl;
    }

    public void setDoctorProfileImageUrl(String doctorProfileImageUrl) {
        this.doctorProfileImageUrl = doctorProfileImageUrl;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public List<PrescribedMedication> getPrescribedMedications() {
        return prescribedMedications;
    }

    public void setPrescribedMedications(List<PrescribedMedication> prescribedMedications) {
        this.prescribedMedications = prescribedMedications;
    }
}
