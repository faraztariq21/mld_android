package com.imedhealthus.imeddoctors.patient.models;

import com.google.gson.annotations.SerializedName;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Malik Hamid on 1/30/2018.
 */

public class PrescribedMedication {

    @SerializedName("Id")
    private String id;
    @SerializedName("Medication")
    private String name;
    @SerializedName("Frequency")
    private String frequency;

    @SerializedName("Dose")
    private String dose;
    @SerializedName("Unit")
    private String unit;
    @SerializedName("MedicationComments")
    private String notes;

    @SerializedName("StartDate")
    private String startingDate;
    @SerializedName("StopDate")
    private String endingDate;

    private String startingDateFormatted, endingDateFormatted;
    private String reason;

    public PrescribedMedication() {

    }

    public PrescribedMedication(JSONObject jsonObject) {

        id = jsonObject.optString("Id", "");
        name = jsonObject.optString("Medication", "");
        frequency = jsonObject.optString("Frequency", "");

        dose = jsonObject.optString("Dose", "");
        unit = jsonObject.optString("Unit", "");
        notes = jsonObject.optString("MedicationComments", "");

        startingDate = jsonObject.optString("StartDate", "");
        Date date = GenericUtils.getDateFromString(startingDate);
        startingDateFormatted = GenericUtils.formatDate(date);

        endingDate = jsonObject.optString("StopDate", "");
        date = GenericUtils.getDateFromString(endingDate);
        endingDateFormatted = GenericUtils.formatDate(date);

    }

    public static List<PrescribedMedication> parsePrescribedMedications(JSONArray medicationsJson){

        List<PrescribedMedication> medications = new ArrayList<>();

        for(int i = 0; i < medicationsJson.length(); i++){

            JSONObject medicationJson = medicationsJson.optJSONObject(i);
            if (medicationJson != null) {
                PrescribedMedication medication = new PrescribedMedication(medicationJson);
                medications.add(medication);
            }

        }

        return medications;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getStartingDateFormatted() {
        return startingDateFormatted;
    }

    public void setStartingDateFormatted(String startingDateFormatted) {
        this.startingDateFormatted = startingDateFormatted;
    }

    public String getEndingDateFormatted() {
        return endingDateFormatted;
    }

    public void setEndingDateFormatted(String endingDateFormatted) {
        this.endingDateFormatted = endingDateFormatted;
    }

    public String getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(String startingDate) {
        this.startingDate = startingDate;
    }

    public String getEndingDate() {
        return endingDate;
    }

    public void setEndingDate(String endingDate) {
        this.endingDate = endingDate;
    }

    public void setReason(String s) {
        reason = s;
    }

    public String getReason() {
        return reason;
    }
}
