package com.imedhealthus.imeddoctors.patient.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.CallActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.CallData;
import com.imedhealthus.imeddoctors.common.models.ChatItem;
import com.imedhealthus.imeddoctors.common.models.ChatUserModel;
import com.imedhealthus.imeddoctors.common.models.ChatUsers;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.User;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.Logs;
import com.imedhealthus.imeddoctors.patient.adapters.PatientsListAdapter;
import com.imedhealthus.imeddoctors.patient.listeners.OnPatientItemClickListener;
import com.imedhealthus.imeddoctors.patient.models.Patient;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/6/18.
 */

public class PatientsListFragment extends BaseFragment implements OnPatientItemClickListener, TextWatcher {

    @BindView(R.id.et_search)
    EditText etSearch;

    @BindView(R.id.rv_patients)
    RecyclerView rvPatients;
    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;

    private PatientsListAdapter adapterPatients;
    private List<Patient> patients;

    @Override
    public String getName() {
        return "PatientsListFragment";
    }

    @Override
    public String getTitle() {
        return "Patients List";
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_patients_list, container, false);
        ButterKnife.bind(this, rootView);

        initHelper();
        return rootView;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    private void initHelper() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rvPatients.setLayoutManager(layoutManager);

        adapterPatients = new PatientsListAdapter(tvNoRecords, this);
        rvPatients.setAdapter(adapterPatients);

        etSearch.addTextChangedListener(this);

        patients = new ArrayList<>();

    }


    //Patient Profile item click
    @Override
    public void onPatientItemInfoClick(int idx) {
        adapterPatients.toggleOptionsAt(idx);
    }

    @Override
    public void onPatientItemMessageClick(int idx) {

        Patient patient = adapterPatients.getItem(idx);
        if (patient == null) {
            return;
        }

        User user = CacheManager.getInstance().getCurrentUser();

        ArrayList<ChatUserModel> chatUsers = new ArrayList<>();
        chatUsers.add(new ChatUserModel(user.getLoginId(), "doctor"));
        chatUsers.add(new ChatUserModel(patient.getLoginId(), "patient"));
        ChatUsers users = new ChatUsers(chatUsers);

        startNewOrOpenChat(users);

    }

    @Override
    public void onPatientItemCallClick(int idx) {

        Patient patient = adapterPatients.getItem(idx);
        if (patient == null) {
            return;
        }

        if (!patient.isCallAllowed()) {
            AlertUtils.showAlert(context, Constants.ErrorAlertTitle, "You can't call right now. Kindly wait for appointed time.");
            return;
        }

        startCall(patient);

    }

    @Override
    public void onPatientItemPatientProfileClick(int idx) {

        Patient patient = adapterPatients.getItem(idx);
        if (patient == null) {
            return;
        }
        SharedData.getInstance().setSelectedPatient(patient);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.PatientProfile.ordinal());
        ((BaseActivity) context).startActivity(intent, true);

    }

    @Override
    public void onPatientItemTreatmentPlanClick(int idx) {

        Patient patient = adapterPatients.getItem(idx);
        if (patient == null) {
            return;
        }
        SharedData.getInstance().setSelectedPatient(patient);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.TreatmentPlan.ordinal());
        ((BaseActivity) context).startActivity(intent, true);

    }


    //Search filtering
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        filterPatients();
    }

    private void filterPatients() {

        String searchTxt = etSearch.getText().toString().toLowerCase();
        List<Patient> filteredPatients = new ArrayList<>();

        for (Patient patient : patients) {
            if (patient.getFullName().toLowerCase().contains(searchTxt) || patient.getEmail().toLowerCase().contains(searchTxt) || patient.getMrNumber().contains(searchTxt) || patient.getAddress().contains(searchTxt)) {
                filteredPatients.add(patient);
            }
        }
        adapterPatients.setPatients(filteredPatients);

    }


    //Webservices
    private void loadData() {

        AlertUtils.showProgress(getActivity());

        String id = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.getPatientList(id, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                AlertUtils.dismissProgress();
                Logs.apiResponse(response.toString());
                if (response.status()) {
                    patients = Patient.getPatientList(response);
                } else {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                }
                filterPatients();
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void startNewOrOpenChat(ChatUsers users) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.startChat(users, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                JSONObject chatJson = response.optJSONObject("Chats");
                if (chatJson == null) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
                    return;
                }

                ChatItem chatItem = new ChatItem(chatJson);
                SharedData.getInstance().setSelectedChatItem(chatItem);

                Intent intent = new Intent(context, SimpleFragmentActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.ChatFragment.ordinal());
                ((BaseActivity) context).startActivity(intent, true);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void startCall(final Patient patient) {

        AlertUtils.showProgress(getActivity());

        String userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        String partnerLoginId = patient.getLoginId();
        String myName = CacheManager.getInstance().getCurrentUser().getFullName();

        WebServicesHandler.instance.getDataForCallWithoutNotification(userLoginId, partnerLoginId, myName, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                String sessionId = response.optString("SessionId", "");
                String token = response.optString("TokenId", "");
                boolean isUserLive = response.optBoolean("IsUserLive", false);


                String senderName = CacheManager.getInstance().getCurrentUser().getFullName();
/*
                if (!isUserLive) {
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, patient.getFullName() + " is not online right now. We have notified him. Kindly try again in a few minutes.");

                    CallData data = new CallData(patient.getLoginId(), senderName, "", "","");
                   // OpenTokHandler.getInstance().sendPushNotification(data);

                    return;
                }
*/
                SharedData.getInstance().setOpenTokCallSessionId(sessionId);
                SharedData.getInstance().setOpenTokCallToken(token);

                CallData data = new CallData(patient.getLoginId(), senderName, sessionId, token, CacheManager.getInstance().getCurrentUser().getUserId());
                SharedData.getInstance().setCallData(data);
                /*OpenTokHandler.getInstance().sendCallSignal(data);*/

                Intent intent = new Intent(context, CallActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CallFragment.ordinal());
                ((BaseActivity) context).startActivity(intent, true);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
