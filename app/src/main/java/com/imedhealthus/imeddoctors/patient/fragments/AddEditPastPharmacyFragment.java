package com.imedhealthus.imeddoctors.patient.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.CustomCitySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomCountrySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomStateSpinnerAdapter;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.fragments.FragmentSuggestionDialog;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.City;
import com.imedhealthus.imeddoctors.common.models.Country;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.State;
import com.imedhealthus.imeddoctors.common.models.SuggestionListItem;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.models.Doctor;
import com.imedhealthus.imeddoctors.patient.models.PastPharmacy;
import com.imedhealthus.imeddoctors.patient.models.PatientProfile;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 2/6/18.
 */

public class AddEditPastPharmacyFragment extends BaseFragment {

    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_fax)
    EditText etFax;

    @BindView(R.id.sp_city)
    Spinner spCity;
    @BindView(R.id.sp_state)
    Spinner spState;
    @BindView(R.id.sp_country)
    Spinner spCountry;

    private List<Country> countries;
    private CustomCountrySpinnerAdapter customCountryAdapter;
    private CustomStateSpinnerAdapter stateAdapter;
    private List<State> states;
    private List<City> cities;
    private CustomCitySpinnerAdapter cityAdapter;

    private PastPharmacy pharmacy;

    public AddEditPastPharmacyFragment() {
        pharmacy = SharedData.getInstance().getSelectedPastPharmacy();
    }

    @Override
    public String getName() {
        return "AddEditPastPharmacyFragment";
    }

    @Override
    public String getTitle() {
        if (pharmacy == null) {
            return "Add Pharmacy";
        } else {
            return "Edit Pharmacy";
        }
    }



    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_past_pharmacy, container, false);
        ButterKnife.bind(this, view);

        initView();
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    private void initView() {

        countries = SharedData.getInstance().getCountriesList();
        customCountryAdapter = new CustomCountrySpinnerAdapter(getContext(), R.layout.custom_spinner_item, countries);
        spCountry.setAdapter(customCountryAdapter);

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                states = SharedData.getInstance().
                        getStatesListAgainstCountryByCountryId(Integer.toString(((Country) adapterView.getSelectedItem()).countryId));
                stateAdapter = new CustomStateSpinnerAdapter(getContext(), R.layout.custom_spinner_item, states);
                spState.setAdapter(stateAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (adapterView.getSelectedItem() == null)
                    return;
                cities = SharedData.getInstance().
                        getCitiesListByStateId(((State) adapterView.getItemAtPosition(i)).getStateId());
                cityAdapter = new CustomCitySpinnerAdapter(getContext(), R.layout.custom_spinner_item, cities);
                spCity.setAdapter(cityAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void setData() {

        if (pharmacy == null) {
            return;
        }

        etName.setText(pharmacy.getName());
        etAddress.setText(pharmacy.getAddress());
        etPhone.setText(pharmacy.getPhoneNumber());
        etFax.setText(pharmacy.getFaxNumber());

        setSpinnerData(pharmacy.getCountryId(), pharmacy.getStateId(), pharmacy.getCityId());

    }

    public void setSpinnerData(final String countryId, final String stateId, final String cityId) {

        countries = SharedData.getInstance().getCountriesList();
        customCountryAdapter = new CustomCountrySpinnerAdapter(getContext(), R.layout.custom_spinner_item, countries);

        spCountry.setAdapter(customCountryAdapter);
        int selectedCountryIndex = SharedData.getInstance().getCountryIndexInList(countries, countryId);
        spCountry.setSelection(selectedCountryIndex);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {

                states = SharedData.getInstance().getStatesListAgainstCountryByCountryId(countryId);
                stateAdapter = new CustomStateSpinnerAdapter(getContext(), R.layout.custom_spinner_item, states);

                spState.setAdapter(stateAdapter);
                int selectedStateIndex = SharedData.getInstance().getStateIndexInList(states, stateId);
                spState.setSelection(selectedStateIndex);

                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cities = SharedData.getInstance().getCitiesListByStateId(stateId);
                        cityAdapter = new CustomCitySpinnerAdapter(getContext(), R.layout.custom_spinner_item, cities);

                        spCity.setAdapter(cityAdapter);
                        int selectedCityIndex = SharedData.getInstance().getCityIndexInList(cities, cityId);
                        spCity.setSelection(selectedCityIndex);

                    }
                }, 200);
            }
        }, 200);

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditPharmacy();
                }
                break;

        }
    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etName.getText().toString().isEmpty() || etAddress.getText().toString().isEmpty() ||
                etPhone.getText().toString().isEmpty() || etFax.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustPharmacies(RetrofitJSONResponse response, PastPharmacy updatedPharmacy) {

        if (pharmacy == null) {//Add

            JSONObject pharmacyJson = response.optJSONObject("PharmacyInfo");
            if (pharmacyJson != null) {
                PastPharmacy newPharmacy = new PastPharmacy(pharmacyJson);
                PatientProfile profile = SharedData.getInstance().getSelectedPatientProfile();
                profile.getPastPharmacies().add(newPharmacy);
            }

        } else {

            pharmacy.setName(updatedPharmacy.getName());
            pharmacy.setAddress(updatedPharmacy.getAddress());

            pharmacy.setCityId(updatedPharmacy.getCityId());
            pharmacy.setCityName(updatedPharmacy.getCityName());
            pharmacy.setStateId(updatedPharmacy.getStateId());
            pharmacy.setStateName(updatedPharmacy.getStateName());
            pharmacy.setCountryId(updatedPharmacy.getCountryId());
            pharmacy.setCountryName(updatedPharmacy.getCountryName());

            pharmacy.setPhoneNumber(updatedPharmacy.getPhoneNumber());
            pharmacy.setFaxNumber(updatedPharmacy.getFaxNumber());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditPharmacy() {

        AlertUtils.showProgress(getActivity());

        final PastPharmacy updatedPharmacy = new PastPharmacy();
        if (pharmacy != null) {
            updatedPharmacy.setId(pharmacy.getId());
        }
        updatedPharmacy.setName(etName.getText().toString());
        updatedPharmacy.setAddress(etAddress.getText().toString());

        if (spState.getSelectedItem() == null || ((State) spState.getSelectedItem()).getStateId().equals("0")) {
            updatedPharmacy.setStateId("0");
            updatedPharmacy.setStateName("");
        } else {
            updatedPharmacy.setStateId(((State) spState.getSelectedItem()).getStateId());
            updatedPharmacy.setStateName(((State) spState.getSelectedItem()).getStateName());
        }

        if (spCountry.getSelectedItem() == null || Integer.toString(((Country) spCountry.getSelectedItem()).countryId).equals("0")) {
            updatedPharmacy.setCountryId("0");
            updatedPharmacy.setCountryName("");
        } else {
            updatedPharmacy.setCountryId(Integer.toString(((Country) spCountry.getSelectedItem()).countryId));
            updatedPharmacy.setCountryName(((Country) spCountry.getSelectedItem()).getCountryName());
        }

        if (spCity.getSelectedItem() == null || ((City) spCity.getSelectedItem()).getCityId().equals("0")) {
            updatedPharmacy.setCityId("0");
            updatedPharmacy.setCityName("");
        } else {
            updatedPharmacy.setCityId(((City) spCity.getSelectedItem()).getCityId());
            updatedPharmacy.setCityName(((City) spCity.getSelectedItem()).getCityName());
        }

        updatedPharmacy.setPhoneNumber(etPhone.getText().toString());
        updatedPharmacy.setFaxNumber(etFax.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditPastPharmacy(userId, updatedPharmacy, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                adjustPharmacies(response, updatedPharmacy);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Pharmacy information saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
