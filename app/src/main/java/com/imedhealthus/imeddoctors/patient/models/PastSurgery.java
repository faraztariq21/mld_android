package com.imedhealthus.imeddoctors.patient.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 2/6/18.
 */

public class PastSurgery {

    private String id, disease, type, surgeonName, year, notes;

    public PastSurgery() {
    }

    public PastSurgery(JSONObject jsonObject) {

        id = jsonObject.optString("Id", "");
        disease = jsonObject.optString("Disease", "");
        type = jsonObject.optString("SurgeryType", "");

        surgeonName = jsonObject.optString("SurgeonName", "");
        year = jsonObject.optString("Year", "");
        notes = jsonObject.optString("Notes", "");

    }

    public static List<PastSurgery> parsePastSurgeries(JSONArray surgeriesJson){

        List<PastSurgery> surgeries = new ArrayList<>();

        for(int i = 0; i < surgeriesJson.length(); i++){

            JSONObject surgeryJson = surgeriesJson.optJSONObject(i);
            if (surgeryJson == null) {
                continue;
            }
            PastSurgery surgery = new PastSurgery(surgeryJson);
            surgeries.add(surgery);

        }
        return surgeries;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSurgeonName() {
        return surgeonName;
    }

    public void setSurgeonName(String surgeonName) {
        this.surgeonName = surgeonName;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
