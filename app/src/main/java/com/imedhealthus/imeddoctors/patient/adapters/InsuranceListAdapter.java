package com.imedhealthus.imeddoctors.patient.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.patient.listeners.OnInsuranceItemClickListener;
import com.imedhealthus.imeddoctors.patient.models.Insurance;
import com.imedhealthus.imeddoctors.patient.view_holder.InsuranceItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Malik Hamid on 1/9/2018.
 */

public class InsuranceListAdapter extends RecyclerView.Adapter<InsuranceItemViewHolder> {

    private List<Insurance> insurances;
    private WeakReference<TextView> tvNoRecords;
    private WeakReference<OnInsuranceItemClickListener> onInsuranceItemClickListener;
    private boolean isEditAllowed;

    public InsuranceListAdapter(TextView tvNoRecords, boolean isEditAllowed, OnInsuranceItemClickListener onInsuranceItemClickListener) {

        super();
        this.tvNoRecords = new WeakReference<>(tvNoRecords);
        this.isEditAllowed = isEditAllowed;
        this.onInsuranceItemClickListener = new WeakReference<>(onInsuranceItemClickListener);

    }

    public void setInsurances(List<Insurance> insurances) {

        this.insurances = insurances;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }

    @Override
    public InsuranceItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_insurance, parent, false);

        OnInsuranceItemClickListener listener = null;
        if (onInsuranceItemClickListener != null && onInsuranceItemClickListener.get() != null) {
            listener = onInsuranceItemClickListener.get();
        }

        return new InsuranceItemViewHolder(view, isEditAllowed, listener);

    }

    @Override
    public void onBindViewHolder(InsuranceItemViewHolder viewHolder, int position) {
        viewHolder.setData(getItem(position), position);
    }

    @Override
    public int getItemCount() {
        return insurances == null ? 0 : insurances.size();
    }

    public Insurance getItem(int position) {
        if (position < 0 || position >= insurances.size()) {
            return null;
        }
        return insurances.get(position);
    }

}
