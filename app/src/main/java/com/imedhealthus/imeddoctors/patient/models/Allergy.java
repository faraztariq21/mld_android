package com.imedhealthus.imeddoctors.patient.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 2/6/18.
 */

public class Allergy {

    private String id, allergyTo, reaction, notes;

    public Allergy() {
    }

    public Allergy(JSONObject jsonObject) {

        id = jsonObject.optString("Id", "");
        allergyTo = jsonObject.optString("AllergyTo", "");
        reaction = jsonObject.optString("Recation", "");
        notes = jsonObject.optString("Notes", "");

    }

    public static List<Allergy> parseAllergies(JSONArray allergiesJson){

        List<Allergy> allergies = new ArrayList<>();

        for(int i = 0; i < allergiesJson.length(); i++){

            JSONObject allergyJson = allergiesJson.optJSONObject(i);
            if (allergyJson == null) {
                continue;
            }
            Allergy allergy = new Allergy(allergyJson);
            allergies.add(allergy);

        }
        return allergies;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAllergyTo() {
        return allergyTo;
    }

    public void setAllergyTo(String allergyTo) {
        this.allergyTo = allergyTo;
    }

    public String getReaction() {
        return reaction;
    }

    public void setReaction(String reaction) {
        this.reaction = reaction;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
