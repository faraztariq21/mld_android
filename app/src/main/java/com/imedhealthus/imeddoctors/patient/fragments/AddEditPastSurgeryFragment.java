package com.imedhealthus.imeddoctors.patient.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.patient.models.PastSurgery;
import com.imedhealthus.imeddoctors.patient.models.PatientProfile;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 2/6/18.
 */

public class AddEditPastSurgeryFragment extends BaseFragment {

    @BindView(R.id.et_disease)
    EditText etDisease;
    @BindView(R.id.et_type)
    EditText etType;
    @BindView(R.id.et_surgeon_name)
    EditText etSurgeonName;
    @BindView(R.id.et_year)
    EditText etYear;
    @BindView(R.id.et_notes)
    EditText etNotes;

    private PastSurgery surgery;
    public AddEditPastSurgeryFragment() {
        surgery = SharedData.getInstance().getSelectedPastSurgery();
    }

    @Override
    public String getName() {
        return "AddEditPastSurgeryFragment";
    }

    @Override
    public String getTitle() {
        if (surgery == null) {
            return "Add Surgical History";
        }else {
            return "Edit Surgical History";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_past_surgery, container, false);
        ButterKnife.bind(this, view);

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    private void setData() {

        if (surgery == null) {
            return;
        }

        etDisease.setText(surgery.getDisease());
        etType.setText(surgery.getType());
        etSurgeonName.setText(surgery.getSurgeonName());
        etYear.setText(surgery.getYear());
        etNotes.setText(surgery.getNotes());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditSurgery();
                }
                break;

        }
    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etDisease.getText().toString().isEmpty() || etType.getText().toString().isEmpty() ||
                etSurgeonName.getText().toString().isEmpty() || etYear.getText().toString().isEmpty() ||
                etNotes.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustSurgeries(RetrofitJSONResponse response, PastSurgery updatedSurgery) {

        if (surgery == null) {//Add

            JSONObject surgeryJson = response.optJSONObject("SurgicalHistory");
            if (surgeryJson != null) {
                PastSurgery newSurgery= new PastSurgery(surgeryJson);
                PatientProfile profile = SharedData.getInstance().getSelectedPatientProfile();
                profile.getPastSurgeries().add(newSurgery);
            }

        }else  {

            surgery.setDisease(updatedSurgery.getDisease());
            surgery.setType(updatedSurgery.getType());
            surgery.setSurgeonName(updatedSurgery.getSurgeonName());
            surgery.setYear(updatedSurgery.getYear());
            surgery.setNotes(updatedSurgery.getNotes());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditSurgery() {

        AlertUtils.showProgress(getActivity());

        final PastSurgery updatedSurgery = new PastSurgery();
        if (surgery != null) {
            updatedSurgery.setId(surgery.getId());
        }
        updatedSurgery.setDisease(etDisease.getText().toString());
        updatedSurgery.setType(etType.getText().toString());
        updatedSurgery.setSurgeonName(etSurgeonName.getText().toString());
        updatedSurgery.setYear(etYear.getText().toString());
        updatedSurgery.setNotes(etNotes.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditPastSurgery(userId, updatedSurgery, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                adjustSurgeries(response, updatedSurgery);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Surgical history saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
