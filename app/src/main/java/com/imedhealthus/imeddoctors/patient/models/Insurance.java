package com.imedhealthus.imeddoctors.patient.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 1/8/2018.
 */

public class Insurance {

    private String insuranceId,id;
    private String companyName, companyGroup, companyCode, companyPhone, companyAddress, companySSN;
    private String subscription, insuranceNotes;

    public Insurance() {
    }

    public  Insurance(JSONObject jsonObject){
        try {
            id = jsonObject.getString("Id");
            subscription = jsonObject.getString("SubscriberName");
            companyName = jsonObject.getString("CompanyName");
            companyCode = jsonObject.getString("CompanyCode");
            companyGroup = jsonObject.getString("CompanyGroup");
            companySSN = jsonObject.getString("SNN");
            companyAddress = jsonObject.getString("CompanyAddress");
            companyPhone = jsonObject.getString("PhoneNo");
            insuranceId = jsonObject.getString("InsuranceId");
            insuranceNotes= jsonObject.getString("Notes");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public static List<Insurance> getInsurance(JSONArray jsonObject){
        List<Insurance> insurances = new ArrayList<>();
        for(int i=0;i<jsonObject.length();i++){
            try {
                Insurance insurance=new Insurance(jsonObject.getJSONObject(i));
                insurances.add(insurance);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return insurances;
    }


    public Insurance(String insuranceId, String companyName, String companyGroup, String companyCode, String companyPhone, String companyAddress, String companySSN, String subscription, String insuranceNotes) {
        this.insuranceId = insuranceId;
        this.companyName = companyName;
        this.companyGroup = companyGroup;
        this.companyCode = companyCode;
        this.companyPhone = companyPhone;
        this.companyAddress = companyAddress;
        this.companySSN = companySSN;
        this.subscription = subscription;
        this.insuranceNotes = insuranceNotes;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setInsuranceId(String insuranceId) {
        this.insuranceId = insuranceId;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setCompanyGroup(String companyGroup) {
        this.companyGroup = companyGroup;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public void setCompanySSN(String companySSN) {
        this.companySSN = companySSN;
    }

    public void setSubscription(String subscription) {
        this.subscription = subscription;
    }

    public void setInsuranceNotes(String insuranceNotes) {
        this.insuranceNotes = insuranceNotes;
    }

    public String getInsuranceId() {
        return insuranceId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getCompanyGroup() {
        return companyGroup;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public String getCompanySSN() {
        return companySSN;
    }

    public String getSubscription() {
        return subscription;
    }

    public String getInsuranceNotes() {
        return insuranceNotes;
    }
}
