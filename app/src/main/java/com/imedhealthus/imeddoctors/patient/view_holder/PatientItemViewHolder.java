package com.imedhealthus.imeddoctors.patient.view_holder;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.patient.listeners.OnPatientItemClickListener;
import com.imedhealthus.imeddoctors.patient.models.Patient;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/6/18.
 */

public class PatientItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_info)
    ViewGroup contInfo;
    @BindView(R.id.cont_options)
    ViewGroup contOptions;

    @BindView(R.id.iv_dropdown)
    ImageView ivDropdown;

    @BindView(R.id.iv_profile)
    ImageView ivProfile;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_mr_number)
    TextView tvMrNumber;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_address)
    TextView tvAddress;

    @BindView(R.id.iv_message)
    ImageView ivMessage;
    @BindView(R.id.iv_call)
    ImageView ivCall;
    @BindView(R.id.iv_patient_profile)
    ImageView ivPatientProfile;
    @BindView(R.id.iv_treatment_plan)
    ImageView ivTreatmentPlan;
    @BindView(R.id.iv_pin)
    ImageView ivLocationPin;


    private int idx;
    private WeakReference<OnPatientItemClickListener> onPatientItemClickListener;

    public PatientItemViewHolder(View view, OnPatientItemClickListener onPatientItemClickListener) {

        super(view);
        ButterKnife.bind(this, view);

        contInfo.setOnClickListener(this);
        ivMessage.setOnClickListener(this);
        ivCall.setOnClickListener(this);
        ivPatientProfile.setOnClickListener(this);
        ivTreatmentPlan.setOnClickListener(this);
        ivMessage.setColorFilter(Color.WHITE);
        ivPatientProfile.setColorFilter(Color.WHITE);
        ivTreatmentPlan.setColorFilter(Color.WHITE);
        this.onPatientItemClickListener = new WeakReference<>(onPatientItemClickListener);

    }

    public void setData(Patient patient, boolean showOptions, int idx) {

        tvName.setText(patient.getFullName());
        tvMrNumber.setText(patient.getMrNumber());
        tvEmail.setText(patient.getEmail());

        if(patient.getAddress() != null && !patient.getAddress().equalsIgnoreCase("")
                && !patient.getAddress().equalsIgnoreCase("n/a")) {
            ivLocationPin.setVisibility(View.VISIBLE);
            tvAddress.setVisibility(View.VISIBLE);
            tvAddress.setText(patient.getAddress());
        } else {
            ivLocationPin.setVisibility(View.GONE);
            tvAddress.setVisibility(View.GONE);
        }

        Glide.with(ApplicationManager.getContext()).load(patient.getImageUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivProfile);

        if (showOptions) {
            contOptions.setVisibility(View.VISIBLE);
        }else {
            contOptions.setVisibility(View.GONE);
        }

        this.idx = idx;

        if (showOptions) {
            ivDropdown.setRotation(180);

        }else {
            ivDropdown.setRotation(0);

        }

    }


    @Override
    public void onClick(View view) {

        if (onPatientItemClickListener == null || onPatientItemClickListener.get() == null) {
            return;
        }

        switch (view.getId()){
            case R.id.cont_info:
                onPatientItemClickListener.get().onPatientItemInfoClick(idx);
                break;

            case R.id.iv_message:
                onPatientItemClickListener.get().onPatientItemMessageClick(idx);
                break;

            case R.id.iv_call:
                onPatientItemClickListener.get().onPatientItemCallClick(idx);
                break;

            case R.id.iv_patient_profile:
                onPatientItemClickListener.get().onPatientItemPatientProfileClick(idx);
                break;

            case R.id.iv_treatment_plan:
                onPatientItemClickListener.get().onPatientItemTreatmentPlanClick(idx);
                break;
        }
    }

}

