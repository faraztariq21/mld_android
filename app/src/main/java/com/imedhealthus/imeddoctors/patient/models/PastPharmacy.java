package com.imedhealthus.imeddoctors.patient.models;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 2/6/18.
 */

public class PastPharmacy {

    private String id, name, phoneNumber, faxNumber, address;
    private String cityId, cityName, stateId, stateName, countryId, countryName;

    public PastPharmacy() {
    }

    public PastPharmacy(JSONObject jsonObject) {

        id = jsonObject.optString("Id", "");
        name = jsonObject.optString("Name", "");
        phoneNumber = jsonObject.optString("PhoneNo", "");
        faxNumber = jsonObject.optString("FaxNo", "");

        address = jsonObject.optString("Address", "");
        cityId = jsonObject.optString("CityId", "").replace("null","");
        cityName = jsonObject.optString("CityName", "").replace("null","");
        stateId = jsonObject.optString("StateId", "").replace("null","");
        stateName = jsonObject.optString("StateName", "").replace("null","");
        countryId = jsonObject.optString("CountryId", "").replace("null","");
        countryName = jsonObject.optString("CountryName", "").replace("null","");

    }

    public static List<PastPharmacy> parsePastPharmacies(JSONArray pharmaciesJson){

        List<PastPharmacy> pharmacies = new ArrayList<>();

        for(int i = 0; i < pharmaciesJson.length(); i++){

            JSONObject pharmacyJson = pharmaciesJson.optJSONObject(i);
            if (pharmacyJson == null) {
                continue;
            }
            PastPharmacy pharmacy = new PastPharmacy(pharmacyJson);
            pharmacies.add(pharmacy);

        }
        return pharmacies;

    }

    public String getAddressDetails() {

        String address = cityName;
        if (!TextUtils.isEmpty(stateName)) {
            if (TextUtils.isEmpty(address)) {
                address = stateName;
            }else {
                address += ", " + stateName;
            }
        }
        if (!TextUtils.isEmpty(countryName)) {
            if (TextUtils.isEmpty(address)) {
                address = countryName;
            }else {
                address += ", " + countryName;
            }
        }

        return address;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
