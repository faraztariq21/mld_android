package com.imedhealthus.imeddoctors.patient.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 2/6/18.
 */

public class Hospitalization {

    private String id, hospitalName, phone, address, reason, year, notes;

    public Hospitalization() {
    }

    public Hospitalization(JSONObject jsonObject) {

        id = jsonObject.optString("Id", "");

        hospitalName = jsonObject.optString("HospitalName", "");
        phone = jsonObject.optString("PhoneNo", "");
        address = jsonObject.optString("Address", "");

        reason = jsonObject.optString("illnessORinjury", "");
        year = jsonObject.optString("Year", "");
        notes = jsonObject.optString("Notes", "");

    }

    public static List<Hospitalization> parseHospitalizations(JSONArray hospitalizationsJson){

        List<Hospitalization> hospitalizations = new ArrayList<>();

        for(int i = 0; i < hospitalizationsJson.length(); i++){

            JSONObject hospitalizationJson = hospitalizationsJson.optJSONObject(i);
            if (hospitalizationJson == null) {
                continue;
            }
            Hospitalization hospitalization = new Hospitalization(hospitalizationJson);
            hospitalizations.add(hospitalization);

        }
        return hospitalizations;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
