package com.imedhealthus.imeddoctors.patient.listeners;

/**
 * Created by Malik Hamid on 1/9/2018.
 */

public interface OnAttachmentItemClickListener {
    void onAttachmentDetailClick(int idx);

    void onAttachmentDeleteClick(int idx);

    void onAttachmentImageClick(int idx);
}
