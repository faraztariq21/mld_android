package com.imedhealthus.imeddoctors.patient.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.CustomCitySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomCountrySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomStateSpinnerAdapter;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.City;
import com.imedhealthus.imeddoctors.common.models.Country;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.State;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.models.WorkExperience;
import com.imedhealthus.imeddoctors.patient.models.Guardian;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddEditGuardianFragment extends BaseFragment {


    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.et_home_phone)
    EditText etHomePhone;
    @BindView(R.id.et_office_phone)
    EditText etOfficePhone;
    @BindView(R.id.et_personal_phone)
    EditText etPersonalPhone;
    @BindView(R.id.et_relation)
    EditText etRelation;

    @BindView(R.id.et_zip)
    EditText etZipCode;
    @BindView(R.id.et_primary_address)
    EditText etPrimaryAddress;
    @BindView(R.id.et_secondary_address)
    EditText etSecondaryAddress;

    @BindView(R.id.sp_city)
    Spinner spCity;
    @BindView(R.id.sp_state)
    Spinner spState;
    @BindView(R.id.sp_country)
    Spinner spCountry;

    private WorkExperience workExperience;
    private List<Country> countries;
    private CustomCountrySpinnerAdapter customCountryAdapter;
    private CustomStateSpinnerAdapter stateAdapter;
    private List<State> states;
    private List<City> cities;
    private CustomCitySpinnerAdapter cityAdapter;


    private Guardian guardian;

    public AddEditGuardianFragment() {
        guardian = SharedData.getInstance().getSelectedGuardian();
    }

    @Override
    public boolean showHeader() {
        return true;
    }

    @Override
    public String getName() {
        return "AddEditGuardianFragment";
    }

    @Override
    public String getTitle() {
        if(guardian==null) {
            return "Add Guardian";
        }else {
            return "Edit Guardian";
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view =inflater.inflate(R.layout.fragment_add_edit_guardian, container, false);
        ButterKnife.bind(this,view);
        initView();
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
    }

    public void setData(){

        if(guardian == null) {
            return;
        }

        etName.setText(guardian.getName());
        etEmail.setText(guardian.getEmail());

        etHomePhone.setText(guardian.getHomePhone());
        etOfficePhone.setText(guardian.getOfficePhone());
        etPersonalPhone.setText(guardian.getPersonalPhone());
        etRelation.setText(guardian.getRelation());

        etZipCode.setText(guardian.getZip());
        etPrimaryAddress.setText(guardian.getPrimaryAddress());
        etSecondaryAddress.setText(guardian.getSecondaryAddress());

        setSpinnerData(guardian.getCountryId(),guardian.getStateId(),guardian.getCityId());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save_guardian_info:
                if (validateGuardianInfo()) {
                    saveGuardianInfo();
                }
                break;

        }
    }

    public void setSpinnerData(final String countryId,final String stateId,final String cityId){

        countries = SharedData.getInstance().getCountriesList();
        customCountryAdapter = new CustomCountrySpinnerAdapter(getContext(), R.layout.custom_spinner_item,countries);
        spCountry.setAdapter(customCountryAdapter);
        int selectedCountryIndex = SharedData.getInstance().getCountryIndexInList(countries,countryId);
        spCountry.setSelection(selectedCountryIndex);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                states = SharedData.getInstance().
                        getStatesListAgainstCountryByCountryId(countryId);
                stateAdapter = new CustomStateSpinnerAdapter(getContext(), R.layout.custom_spinner_item, states);
                spState.setAdapter(stateAdapter);
                int selectedStateIndex =SharedData.getInstance().getStateIndexInList(states,stateId);
                spState.setSelection(selectedStateIndex);


                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cities = SharedData.getInstance().
                                getCitiesListByStateId(stateId);
                        cityAdapter = new CustomCitySpinnerAdapter(getContext(), R.layout.custom_spinner_item, cities);
                        spCity.setAdapter(cityAdapter);
                        int selectedCityIndex =SharedData.getInstance().getCityIndexInList(cities,cityId);
                        spCity.setSelection(selectedCityIndex);
                    }
                }, 100);
            }
        }, 100);


    }

    private void initView() {

        countries = SharedData.getInstance().getCountriesList();
        customCountryAdapter = new CustomCountrySpinnerAdapter(getContext(), R.layout.custom_spinner_item,countries);
        spCountry.setAdapter(customCountryAdapter);

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                states = SharedData.getInstance().
                        getStatesListAgainstCountryByCountryId(Integer.toString(((Country)adapterView.getSelectedItem()).countryId));
                stateAdapter = new CustomStateSpinnerAdapter(getContext(), R.layout.custom_spinner_item, states);
                spState.setAdapter(stateAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                cities = SharedData.getInstance().
                        getCitiesListByStateId(((State)adapterView.getItemAtPosition(i)).getStateId());
                cityAdapter = new CustomCitySpinnerAdapter(getContext(), R.layout.custom_spinner_item, cities);
                spCity.setAdapter(cityAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }
    private boolean validateGuardianInfo() {

        String errorMsg = null;
        if (etName.getText().toString().isEmpty() || etEmail.getText().toString().isEmpty() ||
                etHomePhone.getText().toString().isEmpty() || etOfficePhone.getText().toString().isEmpty()||
                etPersonalPhone.getText().toString().isEmpty() || etRelation.getText().toString().isEmpty() ||
                etZipCode.getText().toString().isEmpty() || etPrimaryAddress.getText().toString().isEmpty() ||
                etSecondaryAddress.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustGuardians(RetrofitJSONResponse response, Guardian updatedGuardian) {

        if (guardian == null) {

            JSONObject guardianJson = response.optJSONObject("GuardianInfo");
            if (guardianJson != null) {
                Guardian newGuardian = new Guardian(guardianJson);
                List<Guardian> guardians = SharedData.getInstance().getSelectedPatientProfile().getGuardians();
                guardians.add(newGuardian);
            }

        }else {

            guardian.setName(updatedGuardian.getName());
            guardian.setEmail(updatedGuardian.getEmail());

            guardian.setHomePhone(updatedGuardian.getHomePhone());
            guardian.setOfficePhone(updatedGuardian.getOfficePhone());
            guardian.setPersonalPhone(updatedGuardian.getPersonalPhone());
            guardian.setRelation(updatedGuardian.getRelation());

            guardian.setZip(updatedGuardian.getZip());
            guardian.setPrimaryAddress(updatedGuardian.getPrimaryAddress());
            guardian.setSecondaryAddress(updatedGuardian.getSecondaryAddress());

            guardian.setCityId(updatedGuardian.getCityId());
            guardian.setCityName(updatedGuardian.getCityName());
            guardian.setStateId(updatedGuardian.getStateId());
            guardian.setStateName(updatedGuardian.getStateName());
            guardian.setCountryId(updatedGuardian.getCountryId());
            guardian.setCountryName(updatedGuardian.getCountryName());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void saveGuardianInfo() {

        AlertUtils.showProgress(getActivity());
        final Guardian updatedGuardian = new Guardian();

        if(guardian != null) {
            updatedGuardian.setGuardianId(guardian.getGuardianId());
        }

        updatedGuardian.setName(etName.getText().toString());
        updatedGuardian.setEmail(etEmail.getText().toString());

        updatedGuardian.setHomePhone(etHomePhone.getText().toString());
        updatedGuardian.setOfficePhone(etOfficePhone.getText().toString());
        updatedGuardian.setPersonalPhone(etPersonalPhone.getText().toString());
        updatedGuardian.setRelation(etRelation.getText().toString());

        updatedGuardian.setZip(etZipCode.getText().toString());
        updatedGuardian.setPrimaryAddress(etPrimaryAddress.getText().toString());
        updatedGuardian.setSecondaryAddress(etSecondaryAddress.getText().toString());

        if(spState.getSelectedItem()!=null) {
            updatedGuardian.setStateId(((State) spState.getSelectedItem()).getStateId());
            updatedGuardian.setStateName(((State) spState.getSelectedItem()).getStateName());
        }
        else{
            updatedGuardian.setStateId("0");
            updatedGuardian.setStateName("");
        }
        if(spCountry.getSelectedItem()!=null) {
            updatedGuardian.setCountryId(Integer.toString(((Country) spCountry.getSelectedItem()).countryId));
            updatedGuardian.setCountryName(((Country) spCountry.getSelectedItem()).getCountryName());
        }else{
            updatedGuardian.setCountryId("0");
            updatedGuardian.setCountryName("");
        }
        if(spCity.getSelectedItem()!=null) {
            updatedGuardian.setCityId(((City) spCity.getSelectedItem()).getCityId());
            updatedGuardian.setCityName(((City) spCity.getSelectedItem()).getCityName());
        }else{
            updatedGuardian.setCityId("0");
            updatedGuardian.setCityName("");
        }

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditGuardian(userId, updatedGuardian, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                if(!response.status()){
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle,response.message());
                    return;
                }

                adjustGuardians(response, updatedGuardian);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Guardian saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
