package com.imedhealthus.imeddoctors.patient.activities;

import android.content.BroadcastReceiver;
import android.os.Bundle;
import android.view.View;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseDashboardActivity;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.utils.Constants;

import butterknife.ButterKnife;

/**
 * Created by umair on 1/5/18.
 */

public class PatientDashboardActivity extends BaseDashboardActivity {

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        ButterKnife.bind(this);
        changeFragment(Constants.Fragment.PatientHome, null);

        initMenu();
        SharedData.getInstance().updateUserDeviceToken();
        SharedData.getInstance().getStaticData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setMenuUserData();
    }


    public void onNavItemClick(View view) {
        switch (view.getId()) {
            case R.id.cont_nav_home:
                changeFragment(Constants.Fragment.PatientHome, null);
                break;

            case R.id.cont_nav_notification:
                changeFragment(Constants.Fragment.Notifications, null);
                break;

            case R.id.iv_nav_search:
                changeFragment(Constants.Fragment.SearchDoctor, null);
                break;

            case R.id.cont_nav_appointment:
                changeFragment(Constants.Fragment.Appointments, null);
                break;

            case R.id.cont_nav_profile:
                changeFragment(Constants.Fragment.PatientProfile, null);
                break;
        }
    }
}
