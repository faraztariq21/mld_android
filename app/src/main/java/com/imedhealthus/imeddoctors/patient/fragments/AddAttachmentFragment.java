package com.imedhealthus.imeddoctors.patient.fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.ImageSelector;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.FileUtils;
import com.imedhealthus.imeddoctors.common.utils.UploadImage;
import com.imedhealthus.imeddoctors.patient.models.Attachment;
import com.soundcloud.android.crop.Crop;

import org.json.JSONObject;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.imedhealthus.imeddoctors.common.fragments.ChatFragment.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE;


public class AddAttachmentFragment extends BaseFragment implements ImageSelector.OnImageSelectionListener {

    @BindView(R.id.iv_attachment)
    ImageView ivAttachment;

    @BindView(R.id.et_title)
    EditText etTitle;
    @BindView(R.id.et_notes)
    EditText etNotes;
    private ImageSelector imageSelector;
    private boolean isImageSelected = false;
    private Uri selectedImgUri;

    @Override
    public String getName() {
        return "AddAttachmentFragment";
    }

    @Override
    public String getTitle() {
        return "Add Attachment";
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_attachment, container, false);
        ButterKnife.bind(this, view);

        return view;

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_choose_file:
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                    openFileSelector();
                else
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                break;

            case R.id.btn_save:
                if (validateFields()) {
                    uploadFile();
                }
                break;
        }
    }

    private void openFileSelector() {
        if (imageSelector == null) {
            imageSelector = new ImageSelector(this, getActivity(), this);
        }
        imageSelector.showSourceAlert();

    }


    //Other
    private boolean validateFields() {

        String errorMsg = null;
        if (etTitle.getText().toString().isEmpty() || etNotes.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }
        if (!isImageSelected) {
            errorMsg = "Kindly select image to continue";
        }
        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustAttachments(RetrofitJSONResponse response) {

        JSONObject attachmentJson = response.optJSONObject("PatientAddPhoto");
        if (attachmentJson != null) {
            Attachment newAttachment = new Attachment(attachmentJson);
            List<Attachment> attachments = SharedData.getInstance().getSelectedPatientProfile().getAttachments();
            attachments.add(newAttachment);
        }

        SharedData.getInstance().setRefreshRequired(true);

    }

    //Image selection
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ImageSelector.CAMERA_REQUEST_CODE:
            case ImageSelector.GALLERY_REQUEST_CODE:
            case Crop.REQUEST_CROP:
                if (imageSelector != null) {
                    imageSelector.onActivityResult(requestCode, resultCode, data);
                }
                break;

        }

    }

    @Override
    public void onImageSelected(Bitmap bitmap, Uri uri, Uri fullImageUri) {
        isImageSelected = true;
        selectedImgUri = uri;
        if (bitmap != null) {
            ivAttachment.setImageBitmap(bitmap);
        } else {
            ivAttachment.setImageURI(uri);
        }
    }


    //Webservices
    private void uploadFile() {

        AlertUtils.showProgress(getActivity());

        final String uploadPath = Constants.FTP.patientDataPath + (new Date()).getTime() + ".png";

        UploadImage uploadImage = new UploadImage(uploadPath, FileUtils.getPathFromUri(getActivity(), selectedImgUri), new UploadImage.OnImageUploadCompletionListener() {
            @Override
            public void onImageUploadComplete(boolean success, int index, final String uploadPath) {

                if (!success) {
                    AlertUtils.dismissProgress();
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, "Can't upload image now. Kindly try later.");
                    return;
                }

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        addAttachment(uploadPath);
                    }
                });

            }
        });
        uploadImage.execute();

    }

    private void addAttachment(String uploadPath) {

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();


        Attachment attachment = new Attachment();
        attachment.setNotes(etNotes.getText().toString());
        attachment.setTitle(etTitle.getText().toString());

        attachment.setFileUrl(uploadPath);
        attachment.setTimeStamp((new Date()).getTime());

        WebServicesHandler.instance.addAttachment(attachment, userId, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if (!response.status()) {
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, response.message());
                    return;
                }

                adjustAttachments(response);
                AlertUtils.showAlertForBack(context, Constants.SuccessAlertTitle, "Attachment added successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(context, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE && grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
            openFileSelector();
        else
            Toast.makeText(getActivity(), getResources().getString(R.string.permission_error), Toast.LENGTH_SHORT).show();
    }
}
