package com.imedhealthus.imeddoctors.patient.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.patient.models.Hospitalization;
import com.imedhealthus.imeddoctors.patient.models.PatientProfile;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 2/6/18.
 */

public class AddEditHospitalizationFragment extends BaseFragment {

    @BindView(R.id.et_hospital_name)
    EditText etHospitalName;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.et_reason)
    EditText etReason;
    @BindView(R.id.et_year)
    EditText etYear;
    @BindView(R.id.et_notes)
    EditText etNotes;

    private Hospitalization hospitalization;
    public AddEditHospitalizationFragment() {
        hospitalization = SharedData.getInstance().getSelectedHospitalization();
    }

    @Override
    public String getName() {
        return "AddEditHospitalizationFragment";
    }

    @Override
    public String getTitle() {
        if (hospitalization == null) {
            return "Add Hospitalization Information";
        }else {
            return "Edit Hospitalization Information";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_hosptialization, container, false);
        ButterKnife.bind(this, view);

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    private void setData() {

        if (hospitalization == null) {
            return;
        }

        etHospitalName.setText(hospitalization.getHospitalName());
        etPhone.setText(hospitalization.getPhone());
        etAddress.setText(hospitalization.getAddress());

        etReason.setText(hospitalization.getReason());
        etYear.setText(hospitalization.getYear());
        etNotes.setText(hospitalization.getNotes());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditHospitalization();
                }
                break;

        }
    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etHospitalName.getText().toString().isEmpty() || etPhone.getText().toString().isEmpty() ||
                etAddress.getText().toString().isEmpty() || etReason.getText().toString().isEmpty() ||
                etYear.getText().toString().isEmpty() || etNotes.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustHospitalizations(RetrofitJSONResponse response, Hospitalization updatedHospitalization) {

        if (hospitalization == null) {//Add

            JSONObject hospitalizationJson = response.optJSONObject("HospitalizationInfo");
            if (hospitalizationJson != null) {
                Hospitalization newHospitalization = new Hospitalization(hospitalizationJson);
                PatientProfile profile = SharedData.getInstance().getSelectedPatientProfile();
                profile.getHospitalizations().add(newHospitalization);
            }

        }else  {

            hospitalization.setHospitalName(updatedHospitalization.getHospitalName());
            hospitalization.setPhone(updatedHospitalization.getPhone());
            hospitalization.setAddress(updatedHospitalization.getAddress());

            hospitalization.setReason(updatedHospitalization.getReason());
            hospitalization.setYear(updatedHospitalization.getYear());
            hospitalization.setNotes(updatedHospitalization.getNotes());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditHospitalization() {

        AlertUtils.showProgress(getActivity());

        final Hospitalization updatedHospitalization = new Hospitalization();
        if (hospitalization != null) {
            updatedHospitalization.setId(hospitalization.getId());
        }
        updatedHospitalization.setHospitalName(etHospitalName.getText().toString());
        updatedHospitalization.setPhone(etPhone.getText().toString());
        updatedHospitalization.setAddress(etAddress.getText().toString());

        updatedHospitalization.setReason(etReason.getText().toString());
        updatedHospitalization.setYear(etYear.getText().toString());
        updatedHospitalization.setNotes(etNotes.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditHospitalization(userId, updatedHospitalization, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                adjustHospitalizations(response, updatedHospitalization);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Hospitalization information saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
