package com.imedhealthus.imeddoctors.patient.models;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by Malik Hamid on 1/8/2018.
 */

public class PersonalInfo {

    private String mrNumber, imageUrl, prefix, firstName, lastName, occupation, bloodGroup, email;
    private String gender, maritalStatus, religion, languages;
    private String zip, primaryAddress, secondaryAddress, cityId, cityName, stateId, stateName, countryId, countryName;
    private String homePhone, officePhone, personalPhone;
    private String dob, dobFormatted;

    public PersonalInfo() {

    }

    public PersonalInfo(JSONObject infoJson){

        mrNumber = infoJson.optString("MRNumber", "");
        imageUrl = GenericUtils.getImageUrl(infoJson.optString("ProfilePix", ""));
        prefix = infoJson.optString("Title", "").replace("null","");
        firstName = infoJson.optString("FirstName", "");
        lastName = infoJson.optString("LastName", "");
        email = infoJson.optString("Email", "").replace("null","");

        occupation = infoJson.optString("Occupation", "").replace("null","");
        bloodGroup = infoJson.optString("BloodGroupId", "").replace("null","");

        gender = infoJson.optString("GenderId", "").replace("null","");
        maritalStatus = infoJson.optString("StatusId", "").replace("null","");
        religion = infoJson.optString("ReligionId", "").replace("null","");
        languages = infoJson.optString("LanguageId", "").replace("null","");

        zip = infoJson.optString("ZipCode", "").replace("null","");
        primaryAddress = infoJson.optString("PrimaryAddress", "").replace("null","");
        secondaryAddress = infoJson.optString("SecondaryAddress", "").replace("null","");

        cityId = infoJson.optString("CityId", "").replace("null","0");
        cityName = infoJson.optString("CityName", "").replace("null","");
        stateId = infoJson.optString("StateId", "").replace("null","0");
        stateName = infoJson.optString("StateName", "").replace("null","");
        countryId = infoJson.optString("CountryId", "").replace("null","0");
        countryName = infoJson.optString("CountryName", "").replace("null","");

        homePhone = infoJson.optString("HomePhone", "").replace("null","");
        officePhone = infoJson.optString("OfficePhone", "").replace("null","");
        personalPhone = infoJson.optString("PersonalPhone", "").replace("null","");

        dob = infoJson.optString("DOB", "").replace("null","");
        if(dob.equals(""))
            return;
        Date date = GenericUtils.getDateFromString(dob);
        dobFormatted = GenericUtils.formatDate(date);

    }

    public String getFullName() {
        return prefix + " " + firstName + " " + lastName;
    }

    public String getMrNumber() {
        return mrNumber;
    }

    public void setMrNumber(String mrNumber) {
        this.mrNumber = mrNumber;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPrimaryAddress() {
        return primaryAddress;
    }

    public void setPrimaryAddress(String primaryAddress) {
        this.primaryAddress = primaryAddress;
    }

    public String getSecondaryAddress() {
        return secondaryAddress;
    }

    public void setSecondaryAddress(String secondaryAddress) {
        this.secondaryAddress = secondaryAddress;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getOfficePhone() {
        return officePhone;
    }

    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }

    public String getPersonalPhone() {
        return personalPhone;
    }

    public void setPersonalPhone(String personalPhone) {
        this.personalPhone = personalPhone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDobFormatted() {
        return dobFormatted;
    }

    public void setDobFormatted(String dobFormatted) {
        this.dobFormatted = dobFormatted;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
