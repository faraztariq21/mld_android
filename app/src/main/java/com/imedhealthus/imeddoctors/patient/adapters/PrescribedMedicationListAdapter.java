package com.imedhealthus.imeddoctors.patient.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.patient.listeners.OnMedicationItemClickListener;
import com.imedhealthus.imeddoctors.patient.models.PrescribedMedication;
import com.imedhealthus.imeddoctors.patient.view_holder.PrescribedMedicationItemViewHolder;


import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Malik Hamid on 1/30/2018.
 */

public class PrescribedMedicationListAdapter extends RecyclerView.Adapter<PrescribedMedicationItemViewHolder> {

    private List<PrescribedMedication> medications;
    private WeakReference<TextView> tvNoRecords;
    private WeakReference<OnMedicationItemClickListener> onMedicationItemClickListener;
    private boolean isEditAllowed;

    public PrescribedMedicationListAdapter(TextView tvNoRecords, boolean isEditAllowed, OnMedicationItemClickListener onMedicationItemClickListener) {

        super();
        this.tvNoRecords = new WeakReference<>(tvNoRecords);
        this.isEditAllowed = isEditAllowed;
        this.onMedicationItemClickListener = new WeakReference<>(onMedicationItemClickListener);

    }

    public void setMedications(List<PrescribedMedication> medications) {

        this.medications = medications;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }

    @Override
    public PrescribedMedicationItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_prescribed_medication, parent, false);

        OnMedicationItemClickListener listener = null;
        if (onMedicationItemClickListener != null && onMedicationItemClickListener.get() != null) {
            listener = onMedicationItemClickListener.get();
        }

        return new PrescribedMedicationItemViewHolder(view, isEditAllowed, listener);

    }

    @Override
    public void onBindViewHolder(PrescribedMedicationItemViewHolder viewHolder, int position) {
        viewHolder.setData(getItem(position), position);
    }

    @Override
    public int getItemCount() {
        return medications == null ? 0 : medications.size();
    }

    public PrescribedMedication getItem(int position) {
        if (position < 0 || position >= medications.size()) {
            return null;
        }
        return medications.get(position);
    }

}
