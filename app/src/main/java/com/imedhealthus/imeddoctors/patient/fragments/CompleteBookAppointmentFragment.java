package com.imedhealthus.imeddoctors.patient.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.auth.activities.LoginActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.LiveNotifications;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.User;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.activities.DoctorDashboardActivity;
import com.imedhealthus.imeddoctors.doctor.models.Appointment;
import com.imedhealthus.imeddoctors.doctor.models.Doctor;
import com.imedhealthus.imeddoctors.patient.activities.PatientDashboardActivity;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/13/18.
 */

public class CompleteBookAppointmentFragment extends BaseFragment {

    @BindView(R.id.cont_confirmation)
    ViewGroup contConfirmation;


    @BindView(R.id.tv_location_title)
    TextView tvLocationTitle;
    @BindView(R.id.tv_location)
    TextView tvLocation;
    @BindView(R.id.tv_description_title)
    TextView tvDescriptionTitle;
    @BindView(R.id.tv_description)
    TextView tvDescription;

    @BindView(R.id.iv_profile)
    ImageView ivDoctorProfile;
    @BindView(R.id.tv_name)
    TextView tvDoctorName;
    @BindView(R.id.tv_speciality)
    TextView tvDoctorSpeciality;
    @BindView(R.id.tv_address)
    TextView tvDoctorAddress;
    @BindView(R.id.tv_rating)
    TextView tvDoctorRating;

    @BindView(R.id.iv_dropdown)
    ImageView ivDropDown;
    @BindView(R.id.iv_pin)
    ImageView ivLocationPin;

    @Override
    public String getName() {
        return "BookAppointmentFragment";
    }

    @Override
    public String getTitle() {
        return "Book Appointment";
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_complete_book_appointment, container, false);
        ButterKnife.bind(this, rootView);
        hideConfirmation();
        setData();
        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        bookAppointment();
    }

    private void setData() {

        Appointment appointment = SharedData.getInstance().getSelectedAppointment();
        Doctor doctor = SharedData.getInstance().getSelectedDoctor();

        if (appointment.getType() == Constants.AppointmentType.Tele) {
            tvLocationTitle.setVisibility(View.GONE);
            tvLocation.setVisibility(View.GONE);
        }


        if(appointment.getPhysicalLocation() != null && !appointment.getPhysicalLocation().equalsIgnoreCase("null") && !appointment.getPhysicalLocation().equalsIgnoreCase("")) {
            tvLocation.setText(appointment.getPhysicalLocation());
        }

        if(doctor.getAddress() != null && !doctor.getAddress().equalsIgnoreCase("null") && !doctor.getAddress().equalsIgnoreCase("")) {
            ivLocationPin.setVisibility(View.VISIBLE);
            tvDoctorAddress.setVisibility(View.VISIBLE);
            tvDoctorAddress.setText(doctor.getAddress());
        } else {
            ivLocationPin.setVisibility(View.GONE);
            tvDoctorAddress.setVisibility(View.GONE);
        }

        if(SharedData.getInstance().getBookAppointmentDescription() != null && !SharedData.getInstance().getBookAppointmentDescription().equalsIgnoreCase("")) {
            tvDescriptionTitle.setVisibility(View.VISIBLE);
            tvDescription.setVisibility(View.VISIBLE);
            tvDescription.setText(SharedData.getInstance().getBookAppointmentDescription());
        } else {
            tvDescriptionTitle.setVisibility(View.GONE);
            tvDescription.setVisibility(View.GONE);
        }

        tvDoctorName.setText(doctor.getFullName().replace("null",""));
        tvDoctorSpeciality.setText(doctor.getSpeciality());
        tvDoctorRating.setText(String.format("%.1f", doctor.getRating()));

        ivDropDown.setVisibility(View.GONE);

        Glide.with(context).load(doctor.getImageUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivDoctorProfile);

    }

    private void showConfirmation() {
        contConfirmation.setVisibility(View.VISIBLE);
    }

    private void hideConfirmation() {
        contConfirmation.setVisibility(View.GONE);
    }


    //Actions
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_ok:
               // ((BaseActivity)context).onBackPressed(true);
                openDashboard();
                break;
        }

    }

    private void openDashboard(){

        SharedData.getInstance().setSelectedFilter(null);

        Intent intent;
        if (CacheManager.getInstance().isUserLoggedIn()) {
            if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Patient) {
                intent = new Intent(context, PatientDashboardActivity.class);
            }else {
                intent = new Intent(context, DoctorDashboardActivity.class);
            }
        }else {
            intent= new Intent(context, LoginActivity.class);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        getActivity().startActivity(intent);

    }



    //Webservice
    private void bookAppointment() {

        AlertUtils.showProgress(getActivity());
        User user = CacheManager.getInstance().getCurrentUser();
        WebServicesHandler.instance.bookAppointment(user.getUserId(),
                Integer.toString(SharedData.getInstance().getSelectedAppointment().getAppointmentId())
                , SharedData.getInstance().getBookAppointmentDescription().toString(), new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlertForBack(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }
                showConfirmation();
                SharedData.getInstance().setShouldContinueBooking(false);
                SharedData.getInstance().setRefreshViaServerHit(true);
                OpenTokHandler.getInstance().sendLiveNotification(new LiveNotifications(SharedData.getInstance().getSelectedDoctor().getUserId(),"doctor"

                        ,CacheManager.getInstance().getCurrentUser().getFullName()+" booked your appointment"));
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlertForBack(getContext(),Constants.ErrorAlertTitle,Constants.GenericErrorMsg);

            }
        });


    }

}
