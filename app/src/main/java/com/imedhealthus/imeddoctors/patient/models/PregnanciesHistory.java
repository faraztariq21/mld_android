package com.imedhealthus.imeddoctors.patient.models;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONObject;

/**
 * Created by Malik Hamid on 2/8/2018.
 */

public class PregnanciesHistory {

    private long Mammogram,breastExam,papSmear;
    private String id,useContraception,contraceptionType,ageAtFirstMenses,menstrualPeriod,ageAtMenopause,symptoms,gynecologicalCondition
            ,notes;

    public PregnanciesHistory() {

    }

    public PregnanciesHistory(JSONObject jsonObject) {
        id = jsonObject.optString("Id", "");
        Mammogram = GenericUtils.getTimeDateInLong(jsonObject.optString("Mammogram", ""));
        breastExam = GenericUtils.getTimeDateInLong(jsonObject.optString("BreastExam", ""));
        papSmear = GenericUtils.getTimeDateInLong(jsonObject.optString("PapSmear", ""));
        useContraception = jsonObject.optString("Contraception", "");

        contraceptionType = jsonObject.optString("ContraceptionDetail", "");
        ageAtFirstMenses = jsonObject.optString("FirstMensesAge", "");

        menstrualPeriod = jsonObject.optString("MenstrualPeriods", "");
        ageAtMenopause = jsonObject.optString("MenopauseAge", "");
        symptoms = jsonObject.optString("HotFlashesOrOther", "");
        gynecologicalCondition = jsonObject.optString("GynecologicalConditions", "");
        notes = jsonObject.optString("Notes", "");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getMammogram() {
        return Mammogram;
    }

    public void setMammogram(long mammogram) {
        Mammogram = mammogram;
    }

    public long getBreastExam() {
        return breastExam;
    }

    public void setBreastExam(long breastExam) {
        this.breastExam = breastExam;
    }

    public long getPapSmear() {
        return papSmear;
    }

    public void setPapSmear(long papSmear) {
        this.papSmear = papSmear;
    }

    public String getUseContraception() {
        return useContraception;
    }

    public void setUseContraception(String useContraception) {
        this.useContraception = useContraception;
    }

    public String getContraceptionType() {
        return contraceptionType;
    }

    public void setContraceptionType(String contraceptionType) {
        this.contraceptionType = contraceptionType;
    }

    public String getAgeAtFirstMenses() {
        return ageAtFirstMenses;
    }

    public void setAgeAtFirstMenses(String ageAtFirstMenses) {
        this.ageAtFirstMenses = ageAtFirstMenses;
    }

    public String getMenstrualPeriod() {
        return menstrualPeriod;
    }

    public void setMenstrualPeriod(String menstrualPeriod) {
        this.menstrualPeriod = menstrualPeriod;
    }

    public String getAgeAtMenopause() {
        return ageAtMenopause;
    }

    public void setAgeAtMenopause(String ageAtMenopause) {
        this.ageAtMenopause = ageAtMenopause;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public String getGynecologicalCondition() {
        return gynecologicalCondition;
    }

    public void setGynecologicalCondition(String gynecologicalCondition) {
        this.gynecologicalCondition = gynecologicalCondition;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
