package com.imedhealthus.imeddoctors.patient.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.patient.models.SocialHistory;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddEditSocialHistoryFragment extends BaseFragment {

    @BindView(R.id.et_tabacco_number_of_year)
    EditText etTabaccoNumberOfYear;
    @BindView(R.id.et_tabacco_amount_pr_day)
    EditText etEtTabaccoAmountPrYear;
    @BindView(R.id.et_tabacco_leaving_year)
    EditText etEtTabaccoLeavingYear;
    @BindView(R.id.sp_tabacco_type)
    Spinner spTabaccoType;
    @BindView(R.id.sp_take_tabacco)
    Spinner spTakeTabacco;

    @BindView(R.id.sp_smoking)
    Spinner spTakeSmoking;

    @BindView(R.id.sp_drugs)
    Spinner spDrugs;


    @BindView(R.id.et_alcohol_year_quit)
    EditText etAlcoholQuitYear;
    @BindView(R.id.et_number_of_drinks_pr_day)
    EditText etAlcoholNumberOfDrinksPrDay;
    @BindView(R.id.sp_preferred_drinks)
    Spinner spPrefferedDrink;
    @BindView(R.id.sp_take_alcohol)
    Spinner spTakeAlcohol;

    @BindView(R.id.et_number_of_caffenated_drinks_pr_day)
    EditText etNumberOfCaffeinatedDrinksPrDay;
    @BindView(R.id.sp_take_caffeine)
    Spinner spTakeCaffine;

    @BindView(R.id.et_receraction_last_used)
    EditText etReceractionDrugsLastUsed;
    @BindView(R.id.et_receraction_amount_pr_week)
    EditText etReceractionDrugsAmountPrYear;
    @BindView(R.id.et_receraction_type)
    EditText etReceractionType;
    @BindView(R.id.sp_take_recreational_drugs)
    Spinner spTakeReceractionDrugs;


    @BindView(R.id.et_exercise_number_of_days_pr_week)
    EditText etExerciseNumberOfDaysPrYear;
    @BindView(R.id.et_exercise_type)
    EditText etExerciseType;
    @BindView(R.id.sp_exercise)
    Spinner spDoExercise;

    @BindView(R.id.et_hobbies)
    EditText etHobbies;

    @BindView(R.id.cb_last_month_feeling)
    CheckBox cbLastMonthFeeling;
    @BindView(R.id.cb_last_month_pleasure)
    CheckBox cdLastMonthPleasure;

    SocialHistory socialHistory ;
    public AddEditSocialHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_edit_social_history, container, false);
        ButterKnife.bind(this,view);
        socialHistory = SharedData.getInstance().getSelectedSocialHistory();
        setData();
        initView();
        return view;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.btn_save:
                onSaveClick();
                break;
        }
    }

    private void initView() {


        spTakeAlcohol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(adapterView.getItemAtPosition(i).toString().equalsIgnoreCase("yes"))
                    setAlcoholViewEnable(true);
                else
                    setAlcoholViewEnable(false);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spTakeReceractionDrugs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(adapterView.getItemAtPosition(i).toString().equalsIgnoreCase("yes"))
                    setReceractionViewEnable(true);
                else
                    setReceractionViewEnable(false);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spTakeTabacco.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(adapterView.getItemAtPosition(i).toString().equalsIgnoreCase("yes"))
                    setTobaccoViewEnable(true);
                else
                    setTobaccoViewEnable(false);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spTakeCaffine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(adapterView.getItemAtPosition(i).toString().equalsIgnoreCase("yes"))
                    setCaffieneViewEnable(true);
                else
                    setCaffieneViewEnable(false);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spDoExercise.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(adapterView.getItemAtPosition(i).toString().equalsIgnoreCase("yes"))
                    setExerciseViewEnable(true);
                else
                    setExerciseViewEnable(false);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setAlcoholViewEnable(boolean isEnable) {
        etAlcoholQuitYear.setEnabled(isEnable);
        etAlcoholNumberOfDrinksPrDay.setEnabled(isEnable);
        spPrefferedDrink.setEnabled(isEnable);

    }

    private void setReceractionViewEnable(boolean isEnable) {
        etReceractionDrugsAmountPrYear.setEnabled(isEnable);
        etReceractionDrugsLastUsed.setEnabled(isEnable);
        etReceractionType.setEnabled(isEnable);

    }

    private void setTobaccoViewEnable(boolean isEnable) {
        spTabaccoType.setEnabled(isEnable);
        etTabaccoNumberOfYear.setEnabled(isEnable);
        etEtTabaccoAmountPrYear.setEnabled(isEnable);
        etEtTabaccoLeavingYear.setEnabled(isEnable);

    }
    private void setCaffieneViewEnable(boolean isEnable) {
        etNumberOfCaffeinatedDrinksPrDay.setEnabled(isEnable);
    }
    private void setExerciseViewEnable(boolean isEnable) {
        etExerciseNumberOfDaysPrYear.setEnabled(isEnable);
        etExerciseType.setEnabled(isEnable);

    }

    private void setAlcoholValue() {
        if(!TextUtils.isEmpty(etAlcoholQuitYear.getText().toString()))
            socialHistory.setAlcoholYearQuit(Integer.parseInt(etAlcoholQuitYear.getText().toString()));
        socialHistory.setAlcoholDrinks(etAlcoholNumberOfDrinksPrDay.getText().toString());
        socialHistory.setAlcoholPreferredDrink(spPrefferedDrink.getSelectedItem().toString());
        socialHistory.setAlcohol("yes");
    }

    private void setReceractionValue() {
        socialHistory.setRecreationalDrugsAmountPerWeek(etReceractionDrugsAmountPrYear.getText().toString());
        socialHistory.setRecreationalDrugsLastUsed(etReceractionDrugsLastUsed.getText().toString());
        socialHistory.setRecreationalDrugsType(etReceractionType.getText().toString());
        socialHistory.setRecreationalDrugs("yes");
    }

    private void setTobaccoValue() {
        socialHistory.setTobaccoType(spTabaccoType.getSelectedItem().toString());
        socialHistory.setTobaccoUsedYear(Integer.parseInt(etTabaccoNumberOfYear.getText().toString()));
        socialHistory.setTobaccoAmountPerDay(etEtTabaccoAmountPrYear.getText().toString());
        socialHistory.setTobaccoYearQuit(Integer.parseInt(etEtTabaccoLeavingYear.getText().toString()));
        socialHistory.setTobacco("yes");
    }
    private void setCaffieneValue() {
        etNumberOfCaffeinatedDrinksPrDay.getText().toString();
        socialHistory.setCaffeine("yes");
    }
    private void setExerciseValue() {
        socialHistory.setExerciseNoOfDaysPerWeek(etExerciseNumberOfDaysPrYear.getText().toString());
        socialHistory.setExerciseType(etExerciseType.getText().toString());
        socialHistory.setExercise("yes");

    }


    public void setData(){
        if(socialHistory==null){
            return;
        }
        if(socialHistory.getAlcohol().equalsIgnoreCase("yes")) {
            setAlcoholViewEnable(true);
            spTakeAlcohol.setSelection(1);
            etAlcoholQuitYear.setText(Integer.toString(socialHistory.getAlcoholYearQuit()));
            etAlcoholNumberOfDrinksPrDay.setText(socialHistory.getAlcoholDrinks());
            spPrefferedDrink.setSelection(getValueFromListByCode(R.array.preferred_drinks, socialHistory.getAlcoholPreferredDrink()));
        }else if(socialHistory.getAlcohol().equalsIgnoreCase("no")){
            spTakeAlcohol.setSelection(2);
        }

        if(socialHistory.getRecreationalDrugs().equalsIgnoreCase("yes")) {
            setReceractionViewEnable(true);
            spTakeReceractionDrugs.setSelection(1);
            etReceractionDrugsAmountPrYear.setText(socialHistory.getRecreationalDrugsAmountPerWeek());
            etReceractionDrugsLastUsed.setText(socialHistory.getRecreationalDrugsLastUsed());
            etReceractionType.setText(socialHistory.getRecreationalDrugsType());
        }else if(socialHistory.getRecreationalDrugs().equalsIgnoreCase("no")){
            spTakeReceractionDrugs.setSelection(2);
        }


         if(socialHistory.getTobacco().equalsIgnoreCase("yes")) {
                spTakeTabacco.setSelection(1);
             setTobaccoViewEnable(true);
            spTabaccoType.setSelection(getValueFromListByCode(R.array.tabacco_type,socialHistory.getTobaccoType()));
            etTabaccoNumberOfYear.setText(Integer.toString(socialHistory.getTobaccoUsedYear()));
            etEtTabaccoAmountPrYear.setText(socialHistory.getTobaccoAmountPerDay());
            etEtTabaccoLeavingYear.setText(Integer.toString(socialHistory.getTobaccoYearQuit()));
         }
         else if(socialHistory.getTobacco().equalsIgnoreCase("no")){
             spTakeTabacco.setSelection(2);
         }

        if(socialHistory.getCaffeine().equalsIgnoreCase("yes")) {
            spTakeCaffine.setSelection(1);
            setCaffieneViewEnable(true);
            etNumberOfCaffeinatedDrinksPrDay.setText(socialHistory.getCaffeineDrinks());
        }else if(socialHistory.getCaffeine().equalsIgnoreCase("no")){
            spTakeCaffine.setSelection(2);
        }

        if(socialHistory.getExercise().equalsIgnoreCase("yes")) {
            spDoExercise.setSelection(1);
            setExerciseViewEnable(true);
            etExerciseNumberOfDaysPrYear.setText(socialHistory.getExerciseNoOfDaysPerWeek());
            etExerciseType.setText(socialHistory.getExerciseType());
        }else if(socialHistory.getExercise().equalsIgnoreCase("no")){
            spDoExercise.setSelection(2);
        }

        spDrugs.setSelection(getValueFromListByCode(R.array.drugs,socialHistory.getDrugs()));
        spTakeSmoking.setSelection(getValueFromListByCode(R.array.smoking,socialHistory.getSmoking()));

        etHobbies.setText(socialHistory.getHobbiesORLeisureActivities());
        cbLastMonthFeeling.setChecked(socialHistory.getLastMonthFeeling().equalsIgnoreCase("true")?true:false);
        cdLastMonthPleasure.setChecked(socialHistory.getLastMonthPleasure().equalsIgnoreCase("true")?true:false);
    }

    public void onSaveClick(){
        if(socialHistory == null)
            socialHistory = new SocialHistory();
        socialHistory.setDrugs(spDrugs.getSelectedItem().toString());
        socialHistory.setSmoking(spTakeSmoking.getSelectedItem().toString());

        String doExercise=spDoExercise.getSelectedItem().toString();
        if(doExercise.equalsIgnoreCase("yes")){
            setExerciseValue();
        }else if(doExercise.equalsIgnoreCase("no")){
            socialHistory.setExercise("no");
        }

        String takeCaffeine=spTakeCaffine.getSelectedItem().toString();
        if(takeCaffeine.equalsIgnoreCase("yes")){
            setCaffieneValue();
        }else if(takeCaffeine.equalsIgnoreCase("no")){
            socialHistory.setCaffeine("no");
        }

        String takeTobacco=spTakeTabacco.getSelectedItem().toString();
        if(takeTobacco.equalsIgnoreCase("yes")){
            setTobaccoValue();
        }else if(takeTobacco.equalsIgnoreCase("no")){
            socialHistory.setTobacco("no");
        }

        String takeAlcohol=spTakeAlcohol.getSelectedItem().toString();
        if(takeAlcohol.equalsIgnoreCase("yes")){
            setAlcoholValue();
        }else if(takeAlcohol.equalsIgnoreCase("no")){
            socialHistory.setAlcohol("no");
        }

        String takeRecreationDrugs=spTakeReceractionDrugs.getSelectedItem().toString();
        if(takeRecreationDrugs.equalsIgnoreCase("yes")){
            setReceractionValue();
        }else if(takeRecreationDrugs.equalsIgnoreCase("no")){
            socialHistory.setRecreationalDrugs("no");
        }

        socialHistory.setExerciseNoOfDaysPerWeek(etHobbies.getText().toString());
        socialHistory.setLastMonthFeeling(cbLastMonthFeeling.isChecked()?"true":"false");
        socialHistory.setLastMonthPleasure(cdLastMonthPleasure.isChecked()?"true":"false");
        socialHistory.setHobbiesORLeisureActivities(etHobbies.getText().toString());
        AlertUtils.showProgress(getActivity());
        if(socialHistory.getSocailId()=="0")
        {
            WebServicesHandler.instance.addSocialHistory(socialHistory, CacheManager.getInstance().getCurrentUser().getUserId(), new CustomCallback<RetrofitJSONResponse>() {
                @Override
                public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                    AlertUtils.dismissProgress();
                    if(response.status()){
                        AlertUtils.showAlert(context, Constants.SuccessAlertTitle,"Data has been saved");
                        SharedData.getInstance().getSelectedPatientProfile().setSocialHistory(socialHistory);
                        SharedData.getInstance().setRefreshRequired(true);
                    }else{
                        AlertUtils.showAlert(context, Constants.ErrorAlertTitle,response.message());
                    }
                }

                @Override
                public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                    AlertUtils.dismissProgress();
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
                }
            });
        }else{
            WebServicesHandler.instance.updateSocialHistory(socialHistory, new CustomCallback<RetrofitJSONResponse>() {
                @Override
                public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                    AlertUtils.dismissProgress();
                    if(response.status()){

                        AlertUtils.showAlertForBack(context, Constants.SuccessAlertTitle,"Data has been saved");
                        SharedData.getInstance().getSelectedPatientProfile().setSocialHistory(socialHistory);
                        SharedData.getInstance().setRefreshRequired(true);
                    }else{
                        AlertUtils.showAlert(context, Constants.ErrorAlertTitle,response.message());
                    }
                }

                @Override
                public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                    AlertUtils.dismissProgress();
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
                }
            });
        }

    }

    private int getValueFromListByCode(int id,String code) {
        int i = -1;
        for (String cc: getResources().getStringArray(id)) {
            i++;
            if (cc.equalsIgnoreCase(code))
                break;
        }
        return i;
    }

    @Override
    public boolean showHeader() {
        return true;
    }

    @Override
    public String getName() {
        return "social_history";
    }

    @Override
    public String getTitle() {
        return "Social History";
    }
}
