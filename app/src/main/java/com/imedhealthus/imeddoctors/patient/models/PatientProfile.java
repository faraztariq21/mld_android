package com.imedhealthus.imeddoctors.patient.models;

import com.imedhealthus.imeddoctors.doctor.models.SocialLink;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 1/8/2018.
 */

public class PatientProfile {

    private PersonalInfo personalInfo = new PersonalInfo();
    private List<EmergencyContact> emergencyContacts = new ArrayList<>();
    private List<Guardian> guardians = new ArrayList<>();

    private List<Insurance> insurances = new ArrayList<>();
    private List<Attachment> attachments = new ArrayList<>();

    private List<Disease> diseases = new ArrayList<>();
    private List<CurrentMedication> currentMedications = new ArrayList<>();
    private List<PastPhysician> pastPhysicians = new ArrayList<>();
    private List<PastSurgery> pastSurgeries = new ArrayList<>();
    private List<Allergy> allergies = new ArrayList<>();
    private List<PastPharmacy> pastPharmacies = new ArrayList<>();
    private List<Hospitalization> hospitalizations = new ArrayList<>();
    private List<FamilyHistory> familyHistories = new ArrayList<>();
    private List<Hobby> hobbies = new ArrayList<>();
    private SocialHistory socialHistory ;
    private PregnanciesHistory pregnanciesHistory ;
    private List<PregnancyDetailsHistory> pragnancyDetailsHistories = new ArrayList<>();
    private List<SocialLink> socialLinks = new ArrayList<>();


    public PatientProfile() {

    }

    public PatientProfile(JSONObject jsonObject){

        JSONObject personalInfoJson = jsonObject.optJSONObject("PatientPersonalInfo");
        if (personalInfoJson != null) {
            personalInfo = new PersonalInfo(personalInfoJson);
        }

        JSONArray arrJson = jsonObject.optJSONArray("Insurances");
        if (arrJson != null) {
            insurances = Insurance.getInsurance(arrJson);
        }

        arrJson = jsonObject.optJSONArray("Attachments");
        if (arrJson != null) {
            attachments = Attachment.getAttachment(arrJson);
        }

        arrJson = jsonObject.optJSONArray("EmergencyContact");
        if (arrJson != null) {
            emergencyContacts = EmergencyContact.getEmergencyContactList(arrJson);
        }

        arrJson = jsonObject.optJSONArray("Guardian");
        if (arrJson != null) {
            guardians = Guardian.parseGuardians(arrJson);
        }

        arrJson = jsonObject.optJSONArray("PastMedicalHistory");
        if (arrJson != null) {
            diseases = Disease.parseDiseases(arrJson);
        }

        arrJson = jsonObject.optJSONArray("PastMedications");
        if (arrJson != null) {
            currentMedications = CurrentMedication.parseCurrentMedications(arrJson);
        }

        arrJson = jsonObject.optJSONArray("MyPhysician");
        if (arrJson != null) {
            pastPhysicians = PastPhysician.parsePastPhysicians(arrJson);
        }

        arrJson = jsonObject.optJSONArray("SurgicalHistory");
        if (arrJson != null) {
            pastSurgeries = PastSurgery.parsePastSurgeries(arrJson);
        }

        arrJson = jsonObject.optJSONArray("AllergiesHistory");
        if (arrJson != null) {
            allergies = Allergy.parseAllergies(arrJson);
        }

        arrJson = jsonObject.optJSONArray("PharmacyInfo");
        if (arrJson != null) {
            pastPharmacies = PastPharmacy.parsePastPharmacies(arrJson);
        }

        arrJson = jsonObject.optJSONArray("HospitalizationsInfo");
        if (arrJson != null) {
            hospitalizations = Hospitalization.parseHospitalizations(arrJson);
        }

        arrJson = jsonObject.optJSONArray("FamilyHistory");
        if (arrJson != null) {
            familyHistories = FamilyHistory.parseFamilyHistories(arrJson);
        }

        arrJson = jsonObject.optJSONArray("HobbiesHistory");
        if (arrJson != null) {
            hobbies = Hobby.parseHobbies(arrJson);
        }

        JSONObject socialHistoryJson = jsonObject.optJSONObject("SocialHistory");
        if (socialHistoryJson != null) {
            socialHistory = new  SocialHistory(socialHistoryJson);
        }

        arrJson = jsonObject.optJSONArray("SocialLink");
        if (arrJson != null) {
            socialLinks = SocialLink.parseSocialLinks(arrJson);
        }

        JSONObject pregnanciesHistoryJson = jsonObject.optJSONObject("PregnanciesHistory");
        if (pregnanciesHistoryJson != null) {
            pregnanciesHistory = new PregnanciesHistory(pregnanciesHistoryJson);
        }

        arrJson = jsonObject.optJSONArray("DetailPregnanciesHistory");
        if (arrJson != null) {
            pragnancyDetailsHistories = PregnancyDetailsHistory.parsePregnancyDetailsHistories(arrJson);
        }

    }

    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }

    public List<EmergencyContact> getEmergencyContacts() {
        return emergencyContacts;
    }

    public void setEmergencyContacts(List<EmergencyContact> emergencyContacts) {
        this.emergencyContacts = emergencyContacts;
    }

    public List<Guardian> getGuardians() {
        return guardians;
    }

    public void setGuardians(List<Guardian> guardians) {
        this.guardians = guardians;
    }

    public List<Insurance> getInsurances() {
        return insurances;
    }

    public void setInsurances(List<Insurance> insurances) {
        this.insurances = insurances;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    public List<Disease> getDiseases() {
        return diseases;
    }

    public void setDiseases(List<Disease> diseases) {
        this.diseases = diseases;
    }

    public List<CurrentMedication> getCurrentMedications() {
        return currentMedications;
    }

    public void setCurrentMedications(List<CurrentMedication> currentMedications) {
        this.currentMedications = currentMedications;
    }

    public List<PastPhysician> getPastPhysicians() {
        return pastPhysicians;
    }

    public void setPastPhysicians(List<PastPhysician> pastPhysicians) {
        this.pastPhysicians = pastPhysicians;
    }

    public List<PastSurgery> getPastSurgeries() {
        return pastSurgeries;
    }

    public void setPastSurgeries(List<PastSurgery> pastSurgeries) {
        this.pastSurgeries = pastSurgeries;
    }

    public List<Allergy> getAllergies() {
        return allergies;
    }

    public void setAllergies(List<Allergy> allergies) {
        this.allergies = allergies;
    }

    public List<PastPharmacy> getPastPharmacies() {
        return pastPharmacies;
    }

    public void setPastPharmacies(List<PastPharmacy> pastPharmacies) {
        this.pastPharmacies = pastPharmacies;
    }

    public List<Hospitalization> getHospitalizations() {
        return hospitalizations;
    }

    public void setHospitalizations(List<Hospitalization> hospitalizations) {
        this.hospitalizations = hospitalizations;
    }

    public List<FamilyHistory> getFamilyHistories() {
        return familyHistories;
    }

    public void setFamilyHistories(List<FamilyHistory> familyHistories) {
        this.familyHistories = familyHistories;
    }

    public List<Hobby> getHobbies() {
        return hobbies;
    }

    public void setHobbies(List<Hobby> hobbies) {
        this.hobbies = hobbies;
    }

    public SocialHistory getSocialHistory() {
        return socialHistory;
    }

    public void setSocialHistory(SocialHistory socialHistory) {
        this.socialHistory = socialHistory;
    }

    public PregnanciesHistory getPregnanciesHistory() {
        return pregnanciesHistory;
    }

    public void setPregnanciesHistory(PregnanciesHistory pregnanciesHistory) {
        this.pregnanciesHistory = pregnanciesHistory;
    }

    public List<PregnancyDetailsHistory> getPragnancyDetailsHistories() {
        return pragnancyDetailsHistories;
    }

    public void setPragnancyDetailsHistories(List<PregnancyDetailsHistory> pragnancyDetailsHistories) {
        this.pragnancyDetailsHistories = pragnancyDetailsHistories;
    }

    public List<SocialLink> getSocialLinks() {
        return socialLinks;
    }

    public void setSocialLinks(List<SocialLink> socialLinks) {
        this.socialLinks = socialLinks;
    }
}
