package com.imedhealthus.imeddoctors.patient.listeners;

/**
 * Created by Malik Hamid on 2/5/2018.
 */

public interface OnGuardianClickListener {
    public void onGuardianDetailClick(int idx) ;
    public void onGuardianDeleteClick(int idx);
}
