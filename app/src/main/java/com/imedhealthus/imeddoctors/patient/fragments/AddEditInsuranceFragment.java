package com.imedhealthus.imeddoctors.patient.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.patient.models.Insurance;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddEditInsuranceFragment extends BaseFragment {

    @BindView(R.id.et_company_name)
    EditText etCompanyName;
    @BindView(R.id.et_company_group)
    EditText etCompanyGroup;

    @BindView(R.id.et_company_code)
    EditText etCompanyCode;
    @BindView(R.id.et_company_ssn)
    EditText etCompanySSN;

    @BindView(R.id.et_company_phone)
    EditText etCompanyPhone;
    @BindView(R.id.et_company_address)
    EditText etCompanyAddress;

    @BindView(R.id.et_subscription)
    EditText etSubscription;
    @BindView(R.id.et_notes)
    EditText etNotes;

    private Insurance insurance;

    public AddEditInsuranceFragment() {
        insurance = SharedData.getInstance().getSelectedInsurance();
    }


    @Override
    public String getName() {
        return "AddEditInsuranceFragment";
    }

    @Override
    public String getTitle() {
        if (insurance == null) {
            return "Add AcceptedInsurance";
        }else {
            return "Edit AcceptedInsurance";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_insurance, container, false);
        ButterKnife.bind(this, view);

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    private void setData() {

        if (insurance == null) {
            return;
        }

        etCompanyName.setText(insurance.getCompanyName());
        etCompanyGroup.setText(insurance.getCompanyGroup());

        etCompanyCode.setText(insurance.getCompanyCode());
        etCompanySSN.setText(insurance.getCompanySSN());

        etCompanyPhone.setText(insurance.getCompanyPhone());
        etCompanyAddress.setText(insurance.getCompanyAddress());

        etSubscription.setText(insurance.getSubscription());
        etNotes.setText(insurance.getInsuranceNotes());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditInsurance();
                }
                break;

        }
    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;
        if (etCompanyAddress.getText().toString().isEmpty() || etNotes.getText().toString().isEmpty()
                || etCompanyAddress.getText().toString().isEmpty()|| etCompanyCode.getText().toString().isEmpty()
                || etCompanyGroup.getText().toString().isEmpty()|| etCompanyName.getText().toString().isEmpty()
                || etCompanyPhone.getText().toString().isEmpty()|| etCompanySSN.getText().toString().isEmpty()
                ) {
            errorMsg = "Kindly fill all fields to continue";
        }
        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }
        return true;

    }

    private void adjustInsurance(RetrofitJSONResponse response, Insurance updatedInsurance) {

        if (insurance == null) {

            JSONObject insuranceJson = response.optJSONObject("PatientInsurance");
            if (insuranceJson != null) {
                Insurance newInsurance = new Insurance(insuranceJson);
                List<Insurance> insurances = SharedData.getInstance().getSelectedPatientProfile().getInsurances();
                insurances.add(newInsurance);
            }

        }else {

            insurance.setCompanyName(updatedInsurance.getCompanyName());
            insurance.setCompanyGroup(updatedInsurance.getCompanyGroup());
            insurance.setCompanyCode(updatedInsurance.getCompanyCode());
            insurance.setCompanySSN(updatedInsurance.getCompanySSN());
            insurance.setCompanyPhone(updatedInsurance.getCompanyPhone());
            insurance.setCompanyAddress(updatedInsurance.getCompanyAddress());

            insurance.setSubscription(updatedInsurance.getSubscription());
            insurance.setInsuranceNotes(updatedInsurance.getInsuranceNotes());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditInsurance() {

        AlertUtils.showProgress(getActivity());

        final Insurance updatedInsurance = new Insurance();
        if (insurance != null) {
            updatedInsurance.setId(insurance.getId());
        }

        updatedInsurance.setCompanyAddress(etCompanyAddress.getText().toString());
        updatedInsurance.setCompanyCode(etCompanyCode.getText().toString());
        updatedInsurance.setCompanyGroup(etCompanyGroup.getText().toString());
        updatedInsurance.setCompanyName(etCompanyName.getText().toString());
        updatedInsurance.setCompanyPhone(etCompanyPhone.getText().toString());
        updatedInsurance.setCompanySSN(etCompanySSN.getText().toString());
        updatedInsurance.setSubscription(etSubscription.getText().toString());
        updatedInsurance.setInsuranceNotes(etNotes.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditInsurance(userId, updatedInsurance, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(response.status()){
                    AlertUtils.showAlert(getContext(),Constants.SuccessAlertTitle,response.message());
                }

                adjustInsurance(response, updatedInsurance);
                AlertUtils.showAlertForBack(context, Constants.SuccessAlertTitle, "Insurance details saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

}
