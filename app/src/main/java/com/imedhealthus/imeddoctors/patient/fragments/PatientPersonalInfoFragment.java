package com.imedhealthus.imeddoctors.patient.fragments;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.IntentUtils;
import com.imedhealthus.imeddoctors.doctor.models.OverviewItem;
import com.imedhealthus.imeddoctors.doctor.models.SocialLink;
import com.imedhealthus.imeddoctors.patient.models.EmergencyContact;
import com.imedhealthus.imeddoctors.patient.models.Guardian;
import com.imedhealthus.imeddoctors.patient.models.PatientProfile;
import com.imedhealthus.imeddoctors.patient.models.PersonalInfo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PatientPersonalInfoFragment extends BaseFragment implements  View.OnClickListener {



    @BindView(R.id.tv_patient_dob)
    TextView tvPatientDob;
    @BindView(R.id.tv_patient_gender)
    TextView tvPatientGender;

    @BindView(R.id.tv_patient_religion)
    TextView tvPatientReligion;



    @BindView(R.id.tv_patient_primary_address)
    TextView tvPatientPrimaryAddress;



    @BindView(R.id.iv_edit_personal_info)
    ImageView ivEditPersonalInfo;

    @BindView(R.id.iv_add_emergency_contact)
    ImageView ivAddEmergencyContact;
    @BindView(R.id.cont_emergency_contact)
    LinearLayout contEmergencyContact;
    @BindView(R.id.tv_no_emergency_contact)
    TextView tvNoEmergencyContact;

    @BindView(R.id.iv_add_guardian)
    ImageView ivAddGuardian;
    @BindView(R.id.cont_guardian)
    LinearLayout contGuardian;
    @BindView(R.id.tv_no_guardian)
    TextView tvNoGuardian;

    @BindView(R.id.iv_add_social_link)
    ImageView ivAddSocialLink;
    @BindView(R.id.cont_social_links)
    LinearLayout conSocialLinks;
    @BindView(R.id.tv_no_social_links)
    TextView tvNoSocialLinks;

    private PatientProfile profile;
    private boolean isEditAllowed = false;

    @Override
    public String getName() {
        return "PatientPersonalInfoFragment";
    }

    @Override
    public String getTitle() {
        return "Personal Info";
    }

    public static PatientPersonalInfoFragment create(PatientProfile profile) {
        return PatientPersonalInfoFragment.create(profile, false);
    }

    public static PatientPersonalInfoFragment create(PatientProfile profile, boolean isEditAllowed) {

        PatientPersonalInfoFragment fragment = new PatientPersonalInfoFragment();
        fragment.profile = profile;
        fragment.isEditAllowed = isEditAllowed;
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_patient_personal_info, container, false);
        ButterKnife.bind(this,view);

        adjustView();
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SharedData.getInstance().isRefreshRequired()) {
            setData();
        }
    }

    public void setData(){

        if (profile == null) {
            return;
        }

        //Personal
        PersonalInfo personalInfo = profile.getPersonalInfo();


        if(personalInfo.getDobFormatted()!=null)
        tvPatientDob.setText("DOB "+personalInfo.getDobFormatted());
        tvPatientGender.setText(personalInfo.getGender());

        tvPatientReligion.setText(personalInfo.getReligion());

      //  tvHomeNumber.setText(personalInfo.getHomePhone());

     //   tvPatientPrimaryAddress.setText(personalInfo.getPrimaryAddress());

        setEmergencyContacts(profile.getEmergencyContacts());
        setGuardianDetail(profile.getGuardians());
        setSocialLinkIcons(profile.getSocialLinks());

    }


    private void setEmergencyContacts(List<EmergencyContact> emergencyContactList) {

        contEmergencyContact.removeAllViews();

        for (int i = 0; i < emergencyContactList.size(); i++) {

            EmergencyContact emergencyContact = emergencyContactList.get(i);
            View itemMedication = LayoutInflater.from(context).inflate(R.layout.list_item_emergency_contact, contEmergencyContact, false);


            ((TextView)itemMedication.findViewById(R.id.tv_relation)).setText(emergencyContact.getEmergencyContactRelation());


            ((TextView)itemMedication.findViewById(R.id.tv_details)).setText(emergencyContact.getEmergencyContactName()+"("+emergencyContact.getEmergencyContactPhone()+")");

            View ivEdit = itemMedication.findViewById(R.id.iv_edit_emergency_contact);
            View ivDelete = itemMedication.findViewById(R.id.iv_delete_emergency_contact);

            if (isEditAllowed) {

                ivEdit.setTag(i);
                ivEdit.setOnClickListener(this);
                ivDelete.setTag(i);
                ivDelete.setOnClickListener(this);

            }else {

                ivEdit.setVisibility(View.GONE);
                ivDelete.setVisibility(View.GONE);

            }

            contEmergencyContact.addView(itemMedication);

        }

        if(emergencyContactList.size() == 0) {
            tvNoEmergencyContact.setVisibility(View.VISIBLE);
        }else {
            tvNoEmergencyContact.setVisibility(View.GONE);
        }

    }
    private void setGuardianDetail(List<Guardian> guardians) {

        contGuardian.removeAllViews();

        for (int i = 0; i < guardians.size(); i++) {

            Guardian guardian = guardians.get(i);
            View itemMedication = LayoutInflater.from(context).inflate(R.layout.list_item_guardian, contGuardian, false);


            ((TextView) itemMedication.findViewById(R.id.tv_guardian_name)).setText(guardian.getName());
            ((TextView) itemMedication.findViewById(R.id.tv_guardian_home_phone)).setText(guardian.getHomePhone());
            ((TextView) itemMedication.findViewById(R.id.tv_guardian_personal_phone)).setText("Personal Number: "+guardian.getPersonalPhone());
            ((TextView) itemMedication.findViewById(R.id.tv_guardian_email)).setText("Email: "+guardian.getEmail());
            ((TextView) itemMedication.findViewById(R.id.tv_guardian_relation)).setText(guardian.getRelation());
            ((TextView) itemMedication.findViewById(R.id.tv_guardian_primary_address)).setText(guardian.getPrimaryAddress());


            View ivEdit = itemMedication.findViewById(R.id.iv_edit_guardian);
            View ivDelete = itemMedication.findViewById(R.id.iv_delete_guardian);

            if (isEditAllowed) {

                ivEdit.setTag(i);
                ivEdit.setOnClickListener(this);
                ivDelete.setTag(i);
                ivDelete.setOnClickListener(this);

            } else {

                ivEdit.setVisibility(View.GONE);
                ivDelete.setVisibility(View.GONE);

            }

            contGuardian.addView(itemMedication);

        }

        if(guardians.size() == 0) {
            tvNoGuardian.setVisibility(View.VISIBLE);
        }else {
            tvNoGuardian.setVisibility(View.GONE);
        }

    }




    private void setSocialLinkIcons(List<SocialLink> socialLinks) {

        conSocialLinks.removeAllViews();

        for (int i = 0; i < socialLinks.size(); i++) {

            OverviewItem item = socialLinks.get(i);
            View itemSocial = LayoutInflater.from(context).inflate(R.layout.list_item_social_links, conSocialLinks, false);

            // ivReviews.setImageResource(R.drawable.ic_review_gray);

            ImageView social =((ImageView)itemSocial.findViewById(R.id.iv_social_link));
            if(item.getDisplayTitle().equalsIgnoreCase("Facebook")){
                social.setImageResource(R.drawable.ic_social_facebook);
            }else if(item.getDisplayTitle().equalsIgnoreCase("Instagram")){
                social.setImageResource(R.drawable.ic_social_instagram);
            }else if(item.getDisplayTitle().equalsIgnoreCase("Twitter")){
                social.setImageResource(R.drawable.ic_social_twitter);
            }else{
                social.setImageResource(R.drawable.ic_social_wifi);
            }


                social.setTag(item.getDisplayDetail());
                social.setOnClickListener(this);



            conSocialLinks.addView(itemSocial);

        }

        if (socialLinks.size() == 0) {
            tvNoSocialLinks.setVisibility(View.VISIBLE);
        }else {
            tvNoSocialLinks.setVisibility(View.GONE);
        }

    }

    private void adjustView() {

        int visibility = isEditAllowed ? View.VISIBLE : View.INVISIBLE;
        View.OnClickListener onClickListener = isEditAllowed ? this : null;

        ivEditPersonalInfo.setVisibility(visibility);
        ivAddEmergencyContact.setVisibility(visibility);
        ivAddGuardian.setVisibility(visibility);
        ivAddSocialLink.setVisibility(visibility);

        ivEditPersonalInfo.setOnClickListener(onClickListener);
        ivAddEmergencyContact.setOnClickListener(onClickListener);
        ivAddGuardian.setOnClickListener(onClickListener);
        ivAddSocialLink.setOnClickListener(onClickListener);

    }

    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_edit_personal_info:
                openEditInfo();
                break;

            case R.id.iv_add_guardian:
                openAddGuardian();
                break;

            case R.id.iv_edit_guardian:
                openEditGuardian((int)view.getTag());
                break;

            case R.id.iv_delete_guardian:
                deleteGuardian((int)view.getTag());
                break;

            case R.id.iv_add_emergency_contact:
                openAddEmergencyContact();
                break;

            case R.id.iv_edit_emergency_contact:
                openEditEmergencyContact((int)view.getTag());
                break;

            case R.id.iv_delete_emergency_contact:
                deleteEmergencyContact((int)view.getTag());
                break;

            case R.id.iv_add_social_link:
                openAddSocialLink();
                break;

            case R.id.iv_social_link:
                IntentUtils.openUrl(context,(String) view.getTag());
                break;

        }
    }

    public void openEditInfo(){

        SharedData.getInstance().setSelectedPatientProfile(profile);

        Intent intent=new Intent(getContext(), SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.EditPatientPersonalInfo.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void showDeleteAlert(DialogInterface.OnClickListener acceptClickListener) {

        new AlertDialog.Builder(context)
                .setTitle("Confirm")
                .setMessage("Are you sure you want to delete this record?")
                .setPositiveButton("Yes", acceptClickListener)
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }})
                .show();

    }


    //AddEditGuardian
    public void openAddGuardian(){

        SharedData.getInstance().setSelectedGuardian(null);

        Intent intent=new Intent(getContext(), SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditGuardian.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditGuardian(int idx) {

        SharedData.getInstance().setSelectedGuardian(profile.getGuardians().get(idx));

        Intent intent=new Intent(getContext(), SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditGuardian.ordinal());
        ((BaseActivity)context).startActivity(intent, true);
    }

    public void deleteGuardian(int idx) {
        if (idx < 0 || idx >= profile.getGuardians().size()) {
            return;
        }

        final Guardian guardian = profile.getGuardians().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteGuardian(guardian);

            }
        });

    }


    //Emergency Contact
    public void openAddEmergencyContact(){

        SharedData.getInstance().setSelectedEmergencyContact(null);

        Intent intent=new Intent(getContext(), SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditEmergencyContact.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void openEditEmergencyContact(int idx) {
        SharedData.getInstance().setSelectedEmergencyContact(profile.getEmergencyContacts().get(idx));

        Intent intent=new Intent(getContext(), SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditEmergencyContact.ordinal());
        ((BaseActivity)context).startActivity(intent, true);
    }


    public void deleteEmergencyContact(int idx) {
        if (idx < 0 || idx >= profile.getEmergencyContacts().size()) {
            return;
        }

        final EmergencyContact emergencyContact = profile.getEmergencyContacts().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteEmergencyContact(emergencyContact);

            }
        });

    }


    //SocialLink
    private void openAddSocialLink() {

        SharedData.getInstance().setSelectedSocialLink(profile.getSocialLinks());
        SharedData.getInstance().setSelectedPatientProfile(profile);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditSocialLink.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }


    public void deleteSocialLink(int idx) {

        if (idx < 0 || idx >= profile.getSocialLinks().size()) {
            return;
        }

        final SocialLink socialLink = profile.getSocialLinks().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteSocialLink(socialLink);

            }
        });

    }


    //Webservices
    private void deleteEmergencyContact(final EmergencyContact emergencyContact) {

        AlertUtils.showProgress((Activity)context);

        WebServicesHandler.instance.deleteEmergencyContact(emergencyContact, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Emergency Contact deleted successfully");

                List<EmergencyContact> diseases = profile.getEmergencyContacts();
                diseases.remove(emergencyContact);
                setEmergencyContacts(diseases);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });
    }

    private void deleteGuardian(final Guardian guardian) {

        AlertUtils.showProgress((Activity)context);

        WebServicesHandler.instance.deleteGuardian(guardian, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Guardian deleted successfully");

                List<Guardian> guardians = profile.getGuardians();
                guardians.remove(guardian);
                setGuardianDetail(guardians);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });
    }

    private void deleteSocialLink(final SocialLink socialLink) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteSocialLink(socialLink,Constants.UserType.Patient, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Social link details deleted successfully");

                List<SocialLink> socialLinks = profile.getSocialLinks();
                socialLinks.remove(socialLink);
                setSocialLinkIcons(socialLinks);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

}
