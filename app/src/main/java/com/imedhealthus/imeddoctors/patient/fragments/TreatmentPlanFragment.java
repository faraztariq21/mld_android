package com.imedhealthus.imeddoctors.patient.fragments;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.adapters.PrescriptionListAdapter;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.listeners.OnPrescriptionItemClickListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.User;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.patient.models.Patient;
import com.imedhealthus.imeddoctors.patient.models.PrescribedMedication;
import com.imedhealthus.imeddoctors.patient.models.Prescription;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TreatmentPlanFragment extends BaseFragment implements OnPrescriptionItemClickListener {


    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;
    @BindView(R.id.rv_prescriptions)
    RecyclerView rvPrescriptions;
    @BindView(R.id.btn_add_plan)
    Button btnAddPlan;

    private PrescriptionListAdapter adapterPrescriptions;

    private boolean isDataLoaded;
    private String patientId, patientName;

    public TreatmentPlanFragment() {

        if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Patient) {
            patientId = CacheManager.getInstance().getCurrentUser().getUserId();
            patientName = CacheManager.getInstance().getCurrentUser().getFullName();
        } else {
            Patient selectedPatient = SharedData.getInstance().getSelectedPatient();
            if (selectedPatient != null) {
                patientId = SharedData.getInstance().getSelectedPatient().getUserId();
                patientName = SharedData.getInstance().getSelectedPatient().getFullName();
            } else {
                patientId = SharedData.getInstance().getSelectedPatientId();
                patientName = SharedData.getInstance().getSelectedPatientName();
            }
        }

    }


    @Override
    public String getName() {
        return "TreatmentPlanFragment";
    }

    @Override
    public String getTitle() {
        return "Treatment Plan";
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_treatment_plan, container, false);
        ButterKnife.bind(this, view);
        initializeShimmer(view);

        initHelper();
        return view;

    }

    @Override
    public void onResume() {

        super.onResume();
        if (!isDataLoaded || SharedData.getInstance().isRefreshRequired()) {
            loadData();
            isDataLoaded = true;
        }

    }

    private void initHelper() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rvPrescriptions.setLayoutManager(layoutManager);

        boolean canShowEdit = CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor;
        adapterPrescriptions = new PrescriptionListAdapter(tvNoRecords, canShowEdit, this);
        rvPrescriptions.setAdapter(adapterPrescriptions);

        isDataLoaded = false;

        if (canShowEdit) {
            btnAddPlan.setVisibility(View.VISIBLE);
        } else {
            btnAddPlan.setVisibility(View.GONE);
        }

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add_plan:
                openAddTreatmentPlan();
                break;
        }
    }

    private void openAddTreatmentPlan() {

        User doctor = CacheManager.getInstance().getCurrentUser();

        Prescription prescription = new Prescription();

        prescription.setPatientId(patientId);
        prescription.setPatientName(patientName);

        prescription.setDoctorId(doctor.getUserId());
        prescription.setDoctorName(doctor.getFullName());
        prescription.setDoctorProfileImageUrl(doctor.getImageUrl());

        prescription.setPrescribedMedications(new ArrayList<PrescribedMedication>());

        SharedData.getInstance().setSelectedPrescription(prescription);

        Intent intent = new Intent(getContext(), SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.PrescriptionDetail.ordinal());
        ((BaseActivity) context).startActivity(intent, true);

    }


    //On item click
    @Override
    public void OnPrescriptionDetailClickListener(int idx) {

        Prescription prescription = adapterPrescriptions.getItem(idx);
        if (prescription == null) {
            return;
        }
        SharedData.getInstance().setSelectedPrescription(prescription);

        Intent intent = new Intent(getContext(), SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.PrescriptionDetail.ordinal());
        ((BaseActivity) context).startActivity(intent, true);

    }

    @Override
    public void OnPrescriptionEditClickListener(int idx) {

        Prescription prescription = adapterPrescriptions.getItem(idx);
        if (prescription == null) {
            return;
        }
        SharedData.getInstance().setSelectedPrescription(prescription);

        Intent intent = new Intent(getContext(), SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.PrescriptionDetail.ordinal());
        ((BaseActivity) context).startActivity(intent, true);

    }

    @Override
    public void OnPrescriptionDeleteClickListener(int idx) {

        Prescription prescription = adapterPrescriptions.getItem(idx);
        if (prescription == null) {
            return;
        }
        showDeleteAlert(prescription);

    }

    private void showDeleteAlert(final Prescription prescription) {

        new AlertDialog.Builder(context)
                .setTitle("Confirm")
                .setMessage("Are you sure you want to delete this prescription?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        deletePrescription(prescription);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        dialog.dismiss();

                    }
                })
                .show();

    }


    //Webservices
    private void loadData() {

        // AlertUtils.showProgress((Activity) context);
        startShimmerAnimation();
        WebServicesHandler.instance.getTreatmentPlan(patientId, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                stopShimmerAnimation();
                //AlertUtils.dismissProgress();

                List<Prescription> prescriptions = new ArrayList<>();

                if (!response.status()) {
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, response.message());
                } else {
                    JSONArray prescriptionsJson = response.optJSONArray("TreatmentPlan");
                    if (prescriptionsJson != null) {
                        prescriptions = Prescription.parsePrescriptions(prescriptionsJson);
                    }
                }
                adapterPrescriptions.setPrescriptions(prescriptions);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

                stopShimmerAnimation();
                //AlertUtils.dismissProgress();
                AlertUtils.showAlert(context, Constants.SuccessAlertTitle, Constants.GenericErrorMsg);
                adapterPrescriptions.setPrescriptions(new ArrayList<Prescription>());

            }

        });

    }

    private void deletePrescription(final Prescription prescription) {

        AlertUtils.showProgress((Activity) context);

        WebServicesHandler.instance.deletePrescription(prescription, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                if (!response.status()) {
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(context, Constants.SuccessAlertTitle, "Prescription deleted successfully");
                adapterPrescriptions.removePrescription(prescription);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, Constants.GenericErrorMsg);

            }

        });

    }

}
