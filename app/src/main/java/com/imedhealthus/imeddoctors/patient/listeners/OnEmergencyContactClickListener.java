package com.imedhealthus.imeddoctors.patient.listeners;

/**
 * Created by Malik Hamid on 2/3/2018.
 */

public interface OnEmergencyContactClickListener {
    public void onEmergencyContactDetailClick(int idx) ;
    public void onEmergencyContactDeleteClick(int idx);
}
