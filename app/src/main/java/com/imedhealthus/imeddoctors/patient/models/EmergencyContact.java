package com.imedhealthus.imeddoctors.patient.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 2/3/2018.
 */

public class EmergencyContact {
    private String emergencyContactId,emergencyContactName, emergencyContactPhone, emergencyContactRelation;

    public static List<EmergencyContact> getEmergencyContactList(JSONArray jsonObject){
        List<EmergencyContact> emergencyContacts = new ArrayList<>();
        for(int i=0;i<jsonObject.length();i++){
            try {
                EmergencyContact emergencyContact =new EmergencyContact(jsonObject.getJSONObject(i));
                emergencyContacts.add(emergencyContact);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return emergencyContacts;
    }

    public EmergencyContact(JSONObject emergenceyJson) {
        this.emergencyContactId = emergenceyJson.optString("Id","");
        this.emergencyContactName = emergenceyJson.optString("Name","");
        this.emergencyContactPhone = emergenceyJson.optString("ContactNo","");
        this.emergencyContactRelation = emergenceyJson.optString("Relationship","");
    }

    public EmergencyContact() {
    }

    public String getEmergencyContactId() {
        return emergencyContactId;
    }

    public void setEmergencyContactId(String emergencyContactId) {
        this.emergencyContactId = emergencyContactId;
    }

    public String getEmergencyContactName() {
        return emergencyContactName;
    }

    public void setEmergencyContactName(String emergencyContactName) {
        this.emergencyContactName = emergencyContactName;
    }

    public String getEmergencyContactPhone() {
        return emergencyContactPhone;
    }

    public void setEmergencyContactPhone(String emergencyContactPhone) {
        this.emergencyContactPhone = emergencyContactPhone;
    }

    public String getEmergencyContactRelation() {
        return emergencyContactRelation;
    }

    public void setEmergencyContactRelation(String emergencyContactRelation) {
        this.emergencyContactRelation = emergencyContactRelation;
    }
}
