package com.imedhealthus.imeddoctors.patient.listeners;

/**
 * Created by Malik Hamid on 1/9/2018.
 */

public interface OnInsuranceItemClickListener {
    void onInsuranceDetailClick(int idx);
    void onInsuranceEditClick(int idx);
    void onInsuranceDeleteClick(int idx);
}
