package com.imedhealthus.imeddoctors.patient.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 2/6/18.
 */

public class Disease {

    private String id, name, notes;

    public Disease() {
    }

    public Disease(JSONObject jsonObject) {

        id = jsonObject.optString("Id", "");
        name = jsonObject.optString("Name", "");
        notes = jsonObject.optString("Notes", "");

    }

    public static List<Disease> parseDiseases(JSONArray diseasesJson){

        List<Disease> diseases = new ArrayList<>();

        for(int i = 0; i < diseasesJson.length(); i++){

            JSONObject diseaseJson = diseasesJson.optJSONObject(i);
            if (diseaseJson == null) {
                continue;
            }
            Disease medication = new Disease(diseaseJson);
            diseases.add(medication);

        }
        return diseases;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
