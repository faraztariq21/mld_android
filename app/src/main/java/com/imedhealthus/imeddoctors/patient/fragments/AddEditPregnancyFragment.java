package com.imedhealthus.imeddoctors.patient.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.patient.models.PatientProfile;
import com.imedhealthus.imeddoctors.patient.models.PregnanciesHistory;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddEditPregnancyFragment extends BaseFragment implements com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener {


    @BindView(R.id.tv_mammogram)
    TextView tvMammogram;
    @BindView(R.id.tv_breast_exam)
    TextView tvBreastExam;

    @BindView(R.id.tv_pap_smear)
    TextView tvPapSmear;

    @BindView(R.id.sp_contraception)
    Spinner sp_contraception;

    @BindView(R.id.et_contaception_type)
    EditText etContraceptionType;

    @BindView(R.id.et_age_at_menses)
    EditText etAgeAtMenses;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.et_symptoms)
    EditText etSymptoms;

    @BindView(R.id.et_gynecological_condition)
    EditText etGynacologicalCondition;

    @BindView(R.id.sp_menstrual)
    Spinner spMenstrual;
    @BindView(R.id.et_menstual_period)
    EditText etMenstrualPeriod;

    long mammogramTime=0,breastExamTime=0,papSmearTime = 0;

    PregnanciesHistory pregnanciesHistory;
    final String MammogramDate ="Mammogram ",BreastExam ="BreastExam",PapSmear ="PapSmear";

    public AddEditPregnancyFragment() {
        // Required empty public constructor
    }


    @Override
    public boolean showHeader() {
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_add_edit_pregnancy_history, container, false);
        ButterKnife.bind(this,view);

        pregnanciesHistory = SharedData.getInstance().getSelectedPregnanciesHistory();
        initView();
        setData();
        return view;
    }

    private void setData() {
        if(pregnanciesHistory ==null)
            return;

        mammogramTime = pregnanciesHistory.getMammogram();
        papSmearTime = pregnanciesHistory.getPapSmear();
        breastExamTime = pregnanciesHistory.getBreastExam();

        tvMammogram.setText(GenericUtils.formatDate(new Date(pregnanciesHistory.getMammogram())));
        tvBreastExam.setText(GenericUtils.formatDate(new Date(pregnanciesHistory.getBreastExam())));
        tvPapSmear.setText(GenericUtils.formatDate(new Date(pregnanciesHistory.getPapSmear())));

        sp_contraception.setSelection(GenericUtils.getValueFromListByCode(R.array.yes_no, pregnanciesHistory.getUseContraception()));
        spMenstrual.setSelection(GenericUtils.getValueFromListByCode(R.array.menstrual, pregnanciesHistory.getMenstrualPeriod()));

        etAgeAtMenses.setText(pregnanciesHistory.getAgeAtFirstMenses());
        etContraceptionType.setText(pregnanciesHistory.getContraceptionType());
        etMenstrualPeriod.setText(pregnanciesHistory.getAgeAtMenopause());
        etSymptoms.setText(pregnanciesHistory.getSymptoms());
        etGynacologicalCondition.setText(pregnanciesHistory.getGynecologicalCondition());
        etNotes.setText(pregnanciesHistory.getNotes());

    }


    private void openDatePicker(String tag, Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);


        com.wdullaer.materialdatetimepicker.date.DatePickerDialog datePickerDialog = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setVersion(com.wdullaer.materialdatetimepicker.date.DatePickerDialog.Version.VERSION_2);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.light_blue));

        datePickerDialog.show(((Activity)context).getFragmentManager(), tag);

    }

    public void initView(){
        sp_contraception.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(adapterView.getSelectedItem().toString().equalsIgnoreCase("yes")){
                    etContraceptionType.setEnabled(true);
                    if(pregnanciesHistory!=null)
                    etContraceptionType.setText(pregnanciesHistory.getUseContraception());
                }else{
                    etContraceptionType.setText("");
                    etContraceptionType.setEnabled(false);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spMenstrual.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(adapterView.getSelectedItem().toString().equalsIgnoreCase("menopausal")){
                    etMenstrualPeriod.setEnabled(true);
                    if(pregnanciesHistory!=null)
                    etMenstrualPeriod.setText(pregnanciesHistory.getMenstrualPeriod());
                }else{
                    etMenstrualPeriod.setText("");
                    etMenstrualPeriod.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                saveData();
                break;
            case R.id.tv_mammogram:
                openDatePicker(this.MammogramDate,new Date());
                break;
            case R.id.tv_breast_exam:
                openDatePicker(this.BreastExam,new Date());
                break;
            case R.id.tv_pap_smear:
                openDatePicker(this.PapSmear,new Date());
                break;
        }
    }

    @Override
    public String getName() {
        return "pregnancy_detail";
    }

    @Override
    public String getTitle() {
        return "Pregnancy Detail";
    }

    public void saveData(){
        AlertUtils.showProgress(getActivity());
        if(pregnanciesHistory ==null)
           pregnanciesHistory = new PregnanciesHistory();
        pregnanciesHistory.setMammogram(mammogramTime);
        pregnanciesHistory.setBreastExam(breastExamTime);
        pregnanciesHistory.setPapSmear(papSmearTime);

        pregnanciesHistory.setUseContraception(sp_contraception.getSelectedItem().toString());
        pregnanciesHistory.setMenstrualPeriod(spMenstrual.getSelectedItem().toString());

        pregnanciesHistory.setAgeAtFirstMenses(etAgeAtMenses.getText().toString());
        pregnanciesHistory.setContraceptionType(etContraceptionType.getText().toString());
        pregnanciesHistory.setAgeAtMenopause(etMenstrualPeriod.getText().toString());
        pregnanciesHistory.setSymptoms(etSymptoms.getText().toString());
        pregnanciesHistory.setGynecologicalCondition(etGynacologicalCondition.getText().toString());
        pregnanciesHistory.setNotes(etNotes.getText().toString());
        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addMHPregnanciesHistory(userId, pregnanciesHistory, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }
                PatientProfile  patientProfile=SharedData.getInstance().getSelectedPatientProfile();
                patientProfile.setPregnanciesHistory(pregnanciesHistory);
                SharedData.getInstance().setSelectedPatientProfile(patientProfile);
                SharedData.getInstance().setRefreshRequired(true);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Hobby history saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });


    }

    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        if(view.getTag().equals(this.MammogramDate)){
            mammogramTime = calendar.getTimeInMillis();
            tvMammogram.setText(GenericUtils.formatDate(new Date(calendar.getTimeInMillis())));
        }
        else if(view.getTag().equals(this.BreastExam)){
            breastExamTime =calendar.getTimeInMillis();
            tvBreastExam.setText(GenericUtils.formatDate(new Date(calendar.getTimeInMillis())));
        }
        else if(view.getTag().equals(this.PapSmear)){
            papSmearTime = calendar.getTimeInMillis();
            tvPapSmear.setText(GenericUtils.formatDate(new Date(calendar.getTimeInMillis())));
        }

    }
}
