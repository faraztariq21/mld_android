package com.imedhealthus.imeddoctors.patient.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.patient.adapters.InsuranceListAdapter;
import com.imedhealthus.imeddoctors.patient.listeners.OnInsuranceItemClickListener;
import com.imedhealthus.imeddoctors.patient.models.Insurance;
import com.imedhealthus.imeddoctors.patient.models.PatientProfile;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 1/9/2018.
 */

public class InsurancesFragment extends BaseFragment implements OnInsuranceItemClickListener {

    @BindView(R.id.rv_insurances)
    RecyclerView rvInsurances;
    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;

    @BindView(R.id.btn_add_insurance)
    Button btnAddInsurance;

    private InsuranceListAdapter adapterInsurances;

    private PatientProfile profile;
    private boolean isEditAllowed;

    public static InsurancesFragment create(PatientProfile profile, boolean isEditAllowed) {

        InsurancesFragment fragment = new InsurancesFragment();

        fragment.profile = profile;
        fragment.isEditAllowed = isEditAllowed;

        return fragment;

    }


    @Override
    public String getName() {
        return "InsurancesFragment";
    }
    @Override
    public String getTitle() {
        return "Insurance";
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_patient_insurances, container, false);
        ButterKnife.bind(this, rootView);

        initHelper();
        return rootView;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SharedData.getInstance().isRefreshRequired()) {
            setData();
        }
    }

    private void initHelper() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rvInsurances.setLayoutManager(layoutManager);

        adapterInsurances = new InsuranceListAdapter(tvNoRecords, isEditAllowed,this);
        rvInsurances.setAdapter(adapterInsurances);

        if(isEditAllowed) {
            btnAddInsurance.setVisibility(View.VISIBLE);
        }else {
            btnAddInsurance.setVisibility(View.GONE);
        }

    }

    private void setData() {
        adapterInsurances.setInsurances(profile.getInsurances());
    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add_insurance:
                openAddInsurance();
                break;

        }
    }

    public void openAddInsurance(){

        SharedData.getInstance().setSelectedInsurance(null);

        Intent intent=new Intent(getContext(), SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditInsurance.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }


    //Insurance item click
    @Override
    public void onInsuranceDetailClick(int idx) {

    }

    @Override
    public void onInsuranceEditClick(int idx) {

        Insurance insurance = adapterInsurances.getItem(idx);
        if (insurance == null) {
            return;
        }
        SharedData.getInstance().setSelectedInsurance(insurance);

        Intent intent=new Intent(getContext(), SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditInsurance.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    @Override
    public void onInsuranceDeleteClick(int idx) {

        Insurance insurance = adapterInsurances.getItem(idx);
        if (insurance == null) {
            return;
        }
        showDeleteAlert(insurance);

    }

    private void showDeleteAlert(final Insurance insurance) {

        new AlertDialog.Builder(context)
                .setTitle("Confirm")
                .setMessage("Are you sure you want to delete this record?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        deleteInsurance(insurance);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        dialog.dismiss();

                    }})
                .show();

    }

    //Other
    private void adjustInsurances(Insurance insurance) {

        profile.getInsurances().remove(insurance);
        adapterInsurances.setInsurances(profile.getInsurances());

    }


    //Web services
    private void deleteInsurance(final Insurance insurance) {

        AlertUtils.showProgress(getActivity());
        WebServicesHandler.instance.deleteInsurance(insurance, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                adjustInsurances(insurance);
                AlertUtils.showAlert(getContext(),Constants.SuccessAlertTitle, "Insurance details deleted successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,Constants.GenericErrorMsg);
            }
        });

    }

}
