package com.imedhealthus.imeddoctors.patient.view_holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.patient.listeners.OnInsuranceItemClickListener;
import com.imedhealthus.imeddoctors.patient.models.Insurance;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 1/9/2018.
 */

public class InsuranceItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    RelativeLayout contItem;

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_details)
    TextView tvDetails;

    @BindView(R.id.iv_edit)
    ImageView ivEdit;
    @BindView(R.id.iv_delete)
    ImageView ivDelete;

    private int idx;
    private WeakReference<OnInsuranceItemClickListener> onInsuranceItemClickListener;

    public InsuranceItemViewHolder(View view, boolean isEditAllowed, OnInsuranceItemClickListener onInsuranceItemClickListener) {

        super(view);
        ButterKnife.bind(this, view);

        if (isEditAllowed) {
            ivEdit.setVisibility(View.VISIBLE);
            ivDelete.setVisibility(View.VISIBLE);
        }else {
            ivEdit.setVisibility(View.GONE);
            ivDelete.setVisibility(View.GONE);
        }

        contItem.setOnClickListener(this);
        ivEdit.setOnClickListener(this);
        ivDelete.setOnClickListener(this);
        this.onInsuranceItemClickListener = new WeakReference<>(onInsuranceItemClickListener);

    }

    public void setData(Insurance insurance, int idx) {

        tvName.setText(insurance.getCompanyName());
        tvDetails.setText(insurance.getSubscription());

        this.idx = idx;

    }

    @Override
    public void onClick(View view) {

        if (onInsuranceItemClickListener == null || onInsuranceItemClickListener.get() == null) {
            return;
        }

        switch (view.getId()){
            case R.id.cont_item:
                onInsuranceItemClickListener.get().onInsuranceDetailClick(idx);
                break;

            case R.id.iv_edit:
                onInsuranceItemClickListener.get().onInsuranceEditClick(idx);
                break;

            case R.id.iv_delete:
                onInsuranceItemClickListener.get().onInsuranceDeleteClick(idx);
                break;
        }
    }

}

