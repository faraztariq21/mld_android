package com.imedhealthus.imeddoctors.patient.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 1/10/2018.
 */

public class Guardian {

    private String guardianId, name, relation;
    private String zip, primaryAddress, secondaryAddress;
    private String cityId, cityName, stateId, stateName, countryId, countryName;
    private String homePhone, officePhone, personalPhone, email;

    public Guardian() {
    }

    public String getGuardianId() {
        return guardianId;
    }

    public Guardian(JSONObject jsonObject){

        guardianId = jsonObject.optString("Id", "");
        name = jsonObject.optString("GuardianName", "");
        relation = jsonObject.optString("GuardianRelation", "");

        zip = jsonObject.optString("ZidCode", "");
        primaryAddress = jsonObject.optString("PrimaryAddress", "");
        secondaryAddress = jsonObject.optString("SecondaryAddress", "");

        cityId = jsonObject.optString("CityId", "");
        cityName = jsonObject.optString("CityName", "");
        stateId = jsonObject.optString("StateId", "");
        stateName = jsonObject.optString("StateName", "");
        countryId = jsonObject.optString("CountryId", "");
        countryName = jsonObject.optString("CountryName", "");

        homePhone = jsonObject.optString("GuardianHomeContact", "");
        officePhone = jsonObject.optString("GuardianOfficeContact", "");
        personalPhone = jsonObject.optString("GuardianPersonalContact", "");
        email = jsonObject.optString("Email", "");

    }

    public static List<Guardian> parseGuardians(JSONArray arrJson){

        List<Guardian> guardians = new ArrayList<>();
        for(int i = 0; i < arrJson.length(); i++){
            JSONObject guardianJson = arrJson.optJSONObject(i);
            if (guardianJson != null) {
                Guardian guardian = new Guardian(guardianJson);
                guardians.add(guardian);
            }
        }
        return guardians;
    }

    public void setGuardianId(String guardianId) {
        this.guardianId = guardianId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPrimaryAddress() {
        return primaryAddress;
    }

    public void setPrimaryAddress(String primaryAddress) {
        this.primaryAddress = primaryAddress;
    }

    public String getSecondaryAddress() {
        return secondaryAddress;
    }

    public void setSecondaryAddress(String secondaryAddress) {
        this.secondaryAddress = secondaryAddress;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getOfficePhone() {
        return officePhone;
    }

    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }

    public String getPersonalPhone() {
        return personalPhone;
    }

    public void setPersonalPhone(String personalPhone) {
        this.personalPhone = personalPhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
