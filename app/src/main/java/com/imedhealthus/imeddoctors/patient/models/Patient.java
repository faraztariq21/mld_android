package com.imedhealthus.imeddoctors.patient.models;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by umair on 1/11/18.
 */

public class Patient {

    private String userId, mrNumber, imageUrl, firstName, lastName, email, address;

    private long appointmentStartTimeStamp, appointmentEndTimeStamp;
    private String loginId;

    public Patient() {
    }
    public static List<Patient> getPatientList(JSONObject jsonObject){
        ArrayList<Patient> patients = new ArrayList<>();
        try {
            JSONArray jsonArray = jsonObject.getJSONArray("PatientsList");
            for(int i=0;i<jsonArray.length();i++){
                Patient patient =new Patient(jsonArray.getJSONObject(i));
                patients.add(patient);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return patients;
    }

    public Patient(JSONObject jsonObject) {

        userId = jsonObject.optString("PatientId", "").replace("null","N/A");
        mrNumber = jsonObject.optString("MRNumber", "").replace("null","N/A");
        firstName = jsonObject.optString("FirstName", "").replace("null","N/A");
        lastName = jsonObject.optString("LastName", "").replace("null","N/A");
        email = jsonObject.optString("Email", "").replace("null","N/A");
        imageUrl = GenericUtils.getImageUrl(jsonObject.optString("ImageUrl",""));
        address = jsonObject.optString("Address", "").replace("null","N/A");

        String date = jsonObject.optString("appointmentDate", "").split("T")[0];
        appointmentStartTimeStamp = GenericUtils.getLocalTimeFromUTC(date+"T"+jsonObject.optString("StartTime", ""));
        appointmentEndTimeStamp = GenericUtils.getLocalTimeFromUTC(date+"T"+jsonObject.optString("EndTime", ""));
        loginId = jsonObject.optString("LoginId", "").replace("null","");

    }

    public boolean isCallAllowed() {


        long currentTimeStamp = new Date().getTime();
        return appointmentStartTimeStamp < currentTimeStamp && currentTimeStamp < appointmentEndTimeStamp;


    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMrNumber() {
        return mrNumber;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }


    public long getAppointmentStartTimeStamp() {
        return appointmentStartTimeStamp;
    }

    public void setAppointmentStartTimeStamp(long appointmentStartTimeStamp) {
        this.appointmentStartTimeStamp = appointmentStartTimeStamp;
    }

    public long getAppointmentEndTimeStamp() {
        return appointmentEndTimeStamp;
    }

    public void setAppointmentEndTimeStamp(long appointmentEndTimeStamp) {
        this.appointmentEndTimeStamp = appointmentEndTimeStamp;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

}
