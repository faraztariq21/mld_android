package com.imedhealthus.imeddoctors.patient.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.services.CallService;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.Logs;
import com.imedhealthus.imeddoctors.patient.models.Patient;
import com.imedhealthus.imeddoctors.patient.models.PatientProfile;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PatientProfileFragment extends BaseFragment {

    @BindView(R.id.iv_profile)
    ImageView ivProfile;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_mr_number)
    TextView tvMrNumber;
    @BindView(R.id.tv_email)
    TextView tvEmail;

    @BindView(R.id.iv_overview)
    ImageView ivOverview;
    @BindView(R.id.iv_medical_history)
    ImageView ivMedicalHistory;
    @BindView(R.id.iv_insurance)
    ImageView ivInsurance;
    @BindView(R.id.iv_attachment)
    ImageView ivAttachment;

    @BindView(R.id.tv_nav_overview)
    TextView tvNavOverview;
    @BindView(R.id.tv_nav_medical_history)
    TextView tvNavMedicalHistory;
    @BindView(R.id.tv_nav_insurance)
    TextView tvNavInsurance;
    @BindView(R.id.tv_nav_attachment)
    TextView tvNavAttachment;

    @BindView(R.id.cont_nav_overview)
    LinearLayout contOverview;
    @BindView(R.id.cont_nav_insurance)
    LinearLayout contInsurance;
    @BindView(R.id.cont_nav_medical_history)
    LinearLayout contMedicalHistory;
    @BindView(R.id.cont_nav_attachment)
    LinearLayout contAttachments;

    @BindView(R.id.iv_edit_profile)
    ImageView ivEditProfile;

    @BindView(R.id.tv_patient_personal_phone)
    TextView tvMobile;

    private String patientId;

    boolean isPatientLoginId = false;
    private PatientProfile profile;
    private BaseFragment currentFragment;
    private boolean isEditAllowed;
    private boolean isEditProfileAllowed;

    @Override
    public void onPause() {
        super.onPause();
        SharedData.getInstance().setEditPatientProfile(false);
    }

    public PatientProfileFragment() {

        if (SharedData.getInstance().getSelectedPatientLoginId() != null) {
            patientId = SharedData.getInstance().getSelectedPatientLoginId();
            isPatientLoginId = true;
        } else if (SharedData.getInstance().getSelectedPatientId() != null) {
            patientId = SharedData.getInstance().getSelectedPatientId();
        } else if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Patient) {
            patientId = CacheManager.getInstance().getCurrentUser().getUserId();
            isEditAllowed = true;
        } else {
            Patient selectedPatient = SharedData.getInstance().getSelectedPatient();
            if (selectedPatient != null) {
                patientId = SharedData.getInstance().getSelectedPatient().getUserId();
            } else {
                patientId = SharedData.getInstance().getSelectedPatientId();
            }
            isEditAllowed = false;
        }


        isEditProfileAllowed = isEditAllowed;

        if (SharedData.getInstance().isEditPatientProfile())
            isEditAllowed = true;

    }

    @Override
    public String getName() {
        return "PatientProfileFragment";
    }

    @Override
    public String getTitle() {
        return "Patient Profile";
    }

    @Override
    public boolean showHeader() {
        return !isEditAllowed;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_patient_profile, container, false);
        ButterKnife.bind(this, view);

        ivEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedData.getInstance().setSelectedPatientProfile(profile);
                Intent intent = new Intent(getContext(), SimpleFragmentActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.EditPatientPersonalInfo.ordinal());
                ((BaseActivity) context).startActivity(intent, true);

            }
        });

        return view;

    }

    @Override
    public void onResume() {

        super.onResume();
        if (profile == null) {
            loadData();
        } else {
            setProfileData();
        }

        if (CallService.screenSharingCapturer != null)
            CallService.screenSharingCapturer.setContentView(getView());

    }

    private void setData(PatientProfile profile) {

        this.profile = profile;
        SharedData.getInstance().setSelectedPatientProfile(profile);
        setProfileData();


        LinearLayout imageView;
        if (currentFragment instanceof MedicalHistoryFragment) {
            imageView = contMedicalHistory;
        } else if (currentFragment instanceof InsurancesFragment) {
            imageView = contInsurance;
        } else if (currentFragment instanceof AttachmentsFragment) {
            imageView = contAttachments;
        } else {
            imageView = contOverview;
        }
        onClick(imageView);

    }

    private void setProfileData() {

        tvName.setText(profile.getPersonalInfo().getFullName());
        tvMrNumber.setText(profile.getPersonalInfo().getMrNumber());
        tvEmail.setText(profile.getPersonalInfo().getEmail());
        tvMobile.setText(profile.getPersonalInfo().getPersonalPhone());
        Glide.with(ApplicationManager.getContext()).load(profile.getPersonalInfo().getImageUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivProfile);
        if (isEditProfileAllowed)
            ivEditProfile.setVisibility(View.VISIBLE);
        else
            ivEditProfile.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {

        if (profile == null) {
            return;
        }

        switch (view.getId()) {
            case R.id.cont_nav_overview:
                changeChildFragment(PatientPersonalInfoFragment.create(profile, isEditProfileAllowed));
                adjustNavBar(ivOverview);
                break;

            case R.id.cont_nav_medical_history:
                changeChildFragment(MedicalHistoryFragment.create(profile, isEditAllowed));
                adjustNavBar(ivMedicalHistory);
                break;

            case R.id.cont_nav_insurance:
                changeChildFragment(InsurancesFragment.create(profile, isEditAllowed));
                adjustNavBar(ivInsurance);
                break;

            case R.id.cont_nav_attachment:
                changeChildFragment(AttachmentsFragment.create(profile, isEditAllowed));
                adjustNavBar(ivAttachment);
                break;

            default:
                if (currentFragment != null) {
                    currentFragment.onClick(view);
                }

        }

    }

    private void changeChildFragment(BaseFragment fragment) {

        currentFragment = fragment;
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.cont_inner_content, fragment, fragment.getName());
        fragmentTransaction.commitAllowingStateLoss();
        getChildFragmentManager().executePendingTransactions();

    }

    private void adjustNavBar(ImageView ivSelected) {

        int normalColor = Color.WHITE;
        int selectedColor = getResources().getColor(R.color.light_blue);
        int normalTextColor = getResources().getColor(R.color.icon_green);

        if (ivOverview == ivSelected) {
            ivOverview.setColorFilter(normalColor);
            contOverview.setBackgroundColor(selectedColor);
            tvNavOverview.setTextColor(normalColor);
        } else {
            ivOverview.setColorFilter(normalTextColor);
            contOverview.setBackgroundColor(normalColor);
            tvNavOverview.setTextColor(normalTextColor);
        }

        if (ivMedicalHistory == ivSelected) {
            ivMedicalHistory.setColorFilter(normalColor);
            contMedicalHistory.setBackgroundColor(selectedColor);
            tvNavMedicalHistory.setTextColor(normalColor);
        } else {
            ivMedicalHistory.setColorFilter(normalTextColor);
            contMedicalHistory.setBackgroundColor(normalColor);
            tvNavMedicalHistory.setTextColor(normalTextColor);
        }

        if (ivInsurance == ivSelected) {
            ivInsurance.setColorFilter(normalColor);
            contInsurance.setBackgroundColor(selectedColor);
            tvNavInsurance.setTextColor(normalColor);
        } else {
            ivInsurance.setColorFilter(normalTextColor);
            contInsurance.setBackgroundColor(normalColor);
            tvNavInsurance.setTextColor(normalTextColor);
        }

        if (ivAttachment == ivSelected) {
            ivAttachment.setColorFilter(normalColor);
            contAttachments.setBackgroundColor(selectedColor);
            tvNavAttachment.setTextColor(normalColor);
        } else {
            ivAttachment.setColorFilter(normalTextColor);
            contAttachments.setBackgroundColor(normalColor);
            tvNavAttachment.setTextColor(normalTextColor);
        }

    }

    //Webservices
    private void loadData() {

        AlertUtils.showProgress(getActivity());

        if (isPatientLoginId) {
            WebServicesHandler.instance.getPatientProfileByLoginId(patientId, new CustomCallback<RetrofitJSONResponse>() {
                @Override
                public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                    SharedData.getInstance().setSelectedPatientLoginId(null);
                    Logs.apiResponse(response.toString());
                    AlertUtils.dismissProgress();

                    if (!response.status()) {
                        AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                        return;
                    }

                    PatientProfile patientProfile = new PatientProfile(response);
                    setData(patientProfile);

                }

                @Override
                public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                    SharedData.getInstance().setSelectedPatientLoginId(null);
                    AlertUtils.dismissProgress();
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
                }
            });

        } else {
            WebServicesHandler.instance.getPatientProfileById(patientId, new CustomCallback<RetrofitJSONResponse>() {
                @Override
                public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                    Logs.apiResponse(response.toString());
                    AlertUtils.dismissProgress();

                    if (!response.status()) {
                        AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                        return;
                    }

                    PatientProfile patientProfile = new PatientProfile(response);
                    setData(patientProfile);

                }

                @Override
                public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                    AlertUtils.dismissProgress();
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
                }
            });
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedData.getInstance().setSelectedPatientId(null);
    }
}
