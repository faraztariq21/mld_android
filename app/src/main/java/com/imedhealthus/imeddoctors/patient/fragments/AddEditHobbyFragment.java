package com.imedhealthus.imeddoctors.patient.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.patient.models.Hobby;
import com.imedhealthus.imeddoctors.patient.models.PatientProfile;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 2/6/18.
 */

public class AddEditHobbyFragment extends BaseFragment {

    @BindView(R.id.et_name)
    EditText etName;

    private Hobby hobby;
    public AddEditHobbyFragment() {
        hobby = SharedData.getInstance().getSelectedHobby();
    }

    @Override
    public String getName() {
        return "AddEditHobbyFragment";
    }

    @Override
    public String getTitle() {
        if (hobby == null) {
            return "Add Hobby History";
        }else {
            return "Edit Hobby History";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_hobby, container, false);
        ButterKnife.bind(this, view);

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    private void setData() {

        if (hobby == null) {
            return;
        }

        etName.setText(hobby.getName());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditHobby();
                }
                break;

        }
    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etName.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustHobbies(RetrofitJSONResponse response, Hobby updatedHobby) {

        if (hobby == null) {//Add

            JSONObject hobbyJson = response.optJSONObject("PatientHobbiesHistory");
            if (hobbyJson != null) {
                Hobby newHobby = new Hobby(hobbyJson);
                PatientProfile profile = SharedData.getInstance().getSelectedPatientProfile();
                profile.getHobbies().add(newHobby);
            }

        }else  {

            hobby.setName(updatedHobby.getName());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditHobby() {

        AlertUtils.showProgress(getActivity());

        final Hobby updatedHobby = new Hobby();
        if (hobby != null) {
            updatedHobby.setId(hobby.getId());
        }
        updatedHobby.setName(etName.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditHobby(userId, updatedHobby, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                adjustHobbies(response, updatedHobby);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Hobby history saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
