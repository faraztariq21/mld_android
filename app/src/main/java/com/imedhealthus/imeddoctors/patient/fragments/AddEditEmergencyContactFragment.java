package com.imedhealthus.imeddoctors.patient.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.patient.models.EmergencyContact;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddEditEmergencyContactFragment extends BaseFragment {


    @BindView(R.id.et_emergency_contact_name)
    EditText etEmergencyContactName;
    @BindView(R.id.et_emergency_contact_phone)
    EditText etEmergencyPhone;
    @BindView(R.id.et_emergency_contact_relation)
    EditText etEmergencyRelation;

    EmergencyContact emergencyContact;

    public AddEditEmergencyContactFragment() {
        emergencyContact = SharedData.getInstance().getSelectedEmergencyContact();
    }


    @Override
    public String getName() {
        return "emergencyContact";
    }

    @Override
    public String getTitle() {
        if(emergencyContact == null) {
            return "Add Emergency Contact";
        }else {
            return "Edit Emergency Contact";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view =inflater.inflate(R.layout.fragment_add_edit_emergency_contact, container, false);
        ButterKnife.bind(this,view);

        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
    }

    public void setData(){

        if(emergencyContact==null) {
            return;
        }

        etEmergencyContactName.setText(emergencyContact.getEmergencyContactName());
        etEmergencyPhone.setText(emergencyContact.getEmergencyContactPhone());
        etEmergencyRelation.setText(emergencyContact.getEmergencyContactRelation());

    }

    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save_emergency_contact:
                if(validateEmergencyContactInfo())
                    saveEmergencyContactInfo();
                break;

        }
    }

    private boolean validateEmergencyContactInfo() {

        String errorMsg = null;
        if (etEmergencyRelation.getText().toString().isEmpty() || etEmergencyPhone.getText().toString().isEmpty()
                ||etEmergencyContactName.getText().toString().isEmpty()  ) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustEmergencyContact(RetrofitJSONResponse response, EmergencyContact updatedEmergencyContact) {

        if (emergencyContact == null) {

            JSONObject emergencyContactJson = response.optJSONObject("EmergencyContactInfo");
            if (emergencyContactJson != null) {
                EmergencyContact newEmergencyContact = new EmergencyContact(emergencyContactJson);
                List<EmergencyContact> emergencyContacts = SharedData.getInstance().getSelectedPatientProfile().getEmergencyContacts();
                emergencyContacts.add(newEmergencyContact);
            }

        }else {

            emergencyContact.setEmergencyContactName(updatedEmergencyContact.getEmergencyContactName());
            emergencyContact.setEmergencyContactPhone(updatedEmergencyContact.getEmergencyContactPhone());
            emergencyContact.setEmergencyContactRelation(updatedEmergencyContact.getEmergencyContactRelation());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }

    //Webservices
    private void saveEmergencyContactInfo() {

        AlertUtils.showProgress(getActivity());

        final EmergencyContact updatedEmergencyContact = new EmergencyContact();

        if(emergencyContact != null) {
            updatedEmergencyContact.setEmergencyContactId(emergencyContact.getEmergencyContactId());
        }

        updatedEmergencyContact.setEmergencyContactName( etEmergencyContactName.getText().toString());
        updatedEmergencyContact.setEmergencyContactPhone( etEmergencyPhone.getText().toString());
        updatedEmergencyContact.setEmergencyContactRelation( etEmergencyRelation.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditEmergencyContact(userId, updatedEmergencyContact, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle,response.message());
                    return;
                }

                adjustEmergencyContact(response, updatedEmergencyContact);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Emergency contact saved successfully");


            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
