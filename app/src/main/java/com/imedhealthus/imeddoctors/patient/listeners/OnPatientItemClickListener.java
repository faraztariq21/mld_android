package com.imedhealthus.imeddoctors.patient.listeners;

/**
 * Created by umair on 1/6/18.
 */

public interface OnPatientItemClickListener {
    void onPatientItemInfoClick(int idx);
    void onPatientItemMessageClick(int idx);
    void onPatientItemCallClick(int idx);
    void onPatientItemPatientProfileClick(int idx);
    void onPatientItemTreatmentPlanClick(int idx);
}
