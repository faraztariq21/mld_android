package com.imedhealthus.imeddoctors.patient.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 2/6/18.
 */

public class PastPhysician {

    private String id, name, location, hospital, phone, speciality, notes;

    public PastPhysician() {
    }

    public PastPhysician(JSONObject jsonObject) {

        id = jsonObject.optString("Id", "");
        name = jsonObject.optString("Name", "");
        location = jsonObject.optString("Location", "");

        hospital = jsonObject.optString("Hospital", "");
        phone = jsonObject.optString("PhoneNo", "");

        speciality = jsonObject.optString("Specialty", "");
        notes = jsonObject.optString("Notes", "");

    }

    public static List<PastPhysician> parsePastPhysicians(JSONArray physiciansJson){

        List<PastPhysician> physicians = new ArrayList<>();

        for(int i = 0; i < physiciansJson.length(); i++){

            JSONObject physicianJson = physiciansJson.optJSONObject(i);
            if (physicianJson == null) {
                continue;
            }
            PastPhysician physician = new PastPhysician(physicianJson);
            physicians.add(physician);

        }
        return physicians;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
