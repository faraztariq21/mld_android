package com.imedhealthus.imeddoctors.patient.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 1/6/2018.
 */

public class CurrentMedication {

    private String id, name, reason, dose, frequency, doctorName, doctorPhone, pharmacyName, pharmacyNumber, notes;
    Boolean asNeeded;

    public CurrentMedication() {
    }

    public CurrentMedication(JSONObject jsonObject) {

        id = jsonObject.optString("Id", "");
        name = jsonObject.optString("Name", "");

        reason = jsonObject.optString("ReasonForMedicine", "");
        dose = jsonObject.optString("Dose", "");

        frequency = jsonObject.optString("DoseFrequency", "");
        asNeeded = jsonObject.optBoolean("AsNeeded", false);

        doctorName = jsonObject.optString("DocName", "");
        doctorPhone = jsonObject.optString("DocNumber", "");

        pharmacyName = jsonObject.optString("PharName", "");
        pharmacyNumber = jsonObject.optString("PharNumber", "");

        notes = jsonObject.optString("Notes", "");

    }

    public static List<CurrentMedication> parseCurrentMedications(JSONArray medicationsJson){

        List<CurrentMedication> medications = new ArrayList<>();

        for(int i = 0; i < medicationsJson.length(); i++){

            JSONObject medicationJson = medicationsJson.optJSONObject(i);
            if (medicationJson == null) {
                continue;
            }
            CurrentMedication medication = new CurrentMedication(medicationJson);
            medications.add(medication);

        }
        return medications;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDoctorPhone() {
        return doctorPhone;
    }

    public void setDoctorPhone(String doctorPhone) {
        this.doctorPhone = doctorPhone;
    }

    public String getPharmacyName() {
        return pharmacyName;
    }

    public void setPharmacyName(String pharmacyName) {
        this.pharmacyName = pharmacyName;
    }

    public String getPharmacyNumber() {
        return pharmacyNumber;
    }

    public void setPharmacyNumber(String pharmacyNumber) {
        this.pharmacyNumber = pharmacyNumber;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Boolean getAsNeeded() {
        return asNeeded;
    }

    public void setAsNeeded(Boolean asNeeded) {
        this.asNeeded = asNeeded;
    }
}
