package com.imedhealthus.imeddoctors.patient.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.CustomAutoCompleteAdapter;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.FileUtils;
import com.imedhealthus.imeddoctors.patient.models.Disease;
import com.imedhealthus.imeddoctors.patient.models.PatientProfile;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 2/6/18.
 */

public class AddEditDiseaseFragment extends BaseFragment {

    @BindView(R.id.et_disease_name)
    AutoCompleteTextView etDiseaseName;
    @BindView(R.id.et_notes)
    EditText etNotes;

    private Disease disease;
    public AddEditDiseaseFragment() {
        disease = SharedData.getInstance().getSelectedDisease();
    }

    @Override
    public String getName() {
        return "AddEditDiseaseFragment";
    }

    @Override
    public String getTitle() {
        if (disease == null) {
            return "Add Medical History";
        }else {
            return "Edit Medical History";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_disease, container, false);
        ButterKnife.bind(this, view);

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        ArrayList<String> reasons = FileUtils.parseFile(context, "diseases.txt", "\n");
        CustomAutoCompleteAdapter reasonAdapter = new CustomAutoCompleteAdapter(context, R.layout.simple_dropdown_item_1line, etDiseaseName, reasons);
        etDiseaseName.setAdapter(reasonAdapter);
        etDiseaseName.setThreshold(1);
        etDiseaseName.addTextChangedListener(reasonAdapter);

        setData();

    }

    private void setData() {

        if (disease == null) {
            return;
        }

        etDiseaseName.setText(disease.getName());
        etNotes.setText(disease.getNotes());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditDisease();
                }
                break;

        }
    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etDiseaseName.getText().toString().isEmpty() || etNotes.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustDiseases(RetrofitJSONResponse response, Disease updatedDisease) {

        if (disease == null) {//Add

            JSONObject diseaseJson = response.optJSONObject("PastMedicalHistory");
            if (diseaseJson != null) {
                Disease newDisease = new Disease(diseaseJson);
                PatientProfile profile = SharedData.getInstance().getSelectedPatientProfile();
                profile.getDiseases().add(newDisease);
            }

        }else  {

            disease.setName(updatedDisease.getName());
            disease.setNotes(updatedDisease.getNotes());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditDisease() {

        AlertUtils.showProgress(getActivity());

        final Disease updatedDisease = new Disease();
        if (disease != null) {
            updatedDisease.setId(disease.getId());
        }
        updatedDisease.setName(etDiseaseName.getText().toString());
        updatedDisease.setNotes(etNotes.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditDisease(userId, updatedDisease, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                adjustDiseases(response, updatedDisease);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Medical history saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
