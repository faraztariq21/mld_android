package com.imedhealthus.imeddoctors.patient.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.patient.models.PregnancyDetailsHistory;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddEditPragnencyDetailFragment extends BaseFragment implements com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener {


    @BindView(R.id.tv_mammogram)
    TextView tvMammogram;


    @BindView(R.id.sp_gender)
    Spinner spChildGender;

    @BindView(R.id.et_delivery_type)
    EditText etDeliveryType;

    @BindView(R.id.et_number_of_living_children)
    EditText etNumberOfLivingChildren;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.et_number_of_abortion)
    EditText etNumberAbortion;

    @BindView(R.id.et_number_of_miscarriages)
    EditText etNumberOfMisscarriages;


    @BindView(R.id.et_number_of_pragnancies)
    EditText etNumberOfPragnancies;

    long deliveryTime=0;

    PregnancyDetailsHistory pregnancyDetailsHistory;
    final String MammogramDate ="Mammogram ",BreastExam ="BreastExam",PapSmear ="PapSmear";

    public AddEditPragnencyDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public boolean showHeader() {
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_add_edit_pragnency_detail, container, false);
        ButterKnife.bind(this,view);
        initView();
        pregnancyDetailsHistory = SharedData.getInstance().getSelectedPregnancyDetailsHistory();
        setData();
        return view;
    }

    private void setData() {
        if(pregnancyDetailsHistory ==null)
            return;

        tvMammogram.setText(GenericUtils.formatDate(new Date(pregnancyDetailsHistory.getDeliveryDate())));


        spChildGender.setSelection(GenericUtils.getValueFromListByCode(R.array.genders_extended, pregnancyDetailsHistory.getGender()));


        etNumberAbortion.setText(pregnancyDetailsHistory.getNumberOfAbortions());
        etDeliveryType.setText(pregnancyDetailsHistory.getDeliveryType());
        etNumberOfPragnancies.setText(pregnancyDetailsHistory.getNumberOfPregnancies());
        etNumberOfLivingChildren.setText(pregnancyDetailsHistory.getNumberOfLivingChild());
        etNumberOfMisscarriages.setText(pregnancyDetailsHistory.getNumberOfMiscarriages());
        etNotes.setText(pregnancyDetailsHistory.getComplications());

    }


    private void openDatePicker(String tag, Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);


        com.wdullaer.materialdatetimepicker.date.DatePickerDialog datePickerDialog = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setVersion(com.wdullaer.materialdatetimepicker.date.DatePickerDialog.Version.VERSION_2);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.light_blue));

        datePickerDialog.show(((Activity)context).getFragmentManager(), tag);

    }

    public void initView(){


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                saveData();
                break;
            case R.id.tv_mammogram:
                openDatePicker(this.MammogramDate,new Date());
                break;

        }
    }

    @Override
    public String getName() {
        return "pregnancy_detail";
    }

    @Override
    public String getTitle() {
        return "Pregnancy Detail";
    }

    private void adjustPregnancyDetails(RetrofitJSONResponse response, PregnancyDetailsHistory updatePregnancyDetailsHistory) {

        if (pregnancyDetailsHistory == null) {//Add

            JSONObject pregnancyDetailJson = response.optJSONObject("MHDetailPregnanciesHistory");
            if (pregnancyDetailJson != null) {

                PregnancyDetailsHistory newPregnancyDetailsHistory = new PregnancyDetailsHistory(pregnancyDetailJson);
                if(SharedData.getInstance().getSelectedPatientProfile().getPragnancyDetailsHistories()==null)
                    SharedData.getInstance().getSelectedPatientProfile().setPragnancyDetailsHistories(new ArrayList<PregnancyDetailsHistory>());

                SharedData.getInstance().getSelectedPatientProfile().getPragnancyDetailsHistories().add(newPregnancyDetailsHistory);
            }

        }else  {

            pregnancyDetailsHistory.setDeliveryDate(deliveryTime);
            pregnancyDetailsHistory.setGender(spChildGender.getSelectedItem().toString());
            pregnancyDetailsHistory.setDeliveryType(etDeliveryType.getText().toString());

            pregnancyDetailsHistory.setNumberOfAbortions(etNumberAbortion.getText().toString());
            pregnancyDetailsHistory.setNumberOfLivingChild(etNumberOfLivingChildren.getText().toString());
            pregnancyDetailsHistory.setNumberOfMiscarriages(etNumberOfMisscarriages.getText().toString());
            pregnancyDetailsHistory.setNumberOfPregnancies(etNumberOfPragnancies.getText().toString());
            pregnancyDetailsHistory.setComplications(etNotes.getText().toString());

        }


        SharedData.getInstance().setRefreshRequired(true);

    }

    public void saveData(){

        AlertUtils.showProgress(getActivity());
        final PregnancyDetailsHistory updatePregnancyDetailsHistory = new PregnancyDetailsHistory();
        if(pregnancyDetailsHistory!=null)
            updatePregnancyDetailsHistory.setId(pregnancyDetailsHistory.getId());
        updatePregnancyDetailsHistory.setDeliveryDate(deliveryTime);
        updatePregnancyDetailsHistory.setGender(spChildGender.getSelectedItem().toString());
        updatePregnancyDetailsHistory.setDeliveryType(etDeliveryType.getText().toString());

        updatePregnancyDetailsHistory.setNumberOfAbortions(etNumberAbortion.getText().toString());
        updatePregnancyDetailsHistory.setNumberOfLivingChild(etNumberOfLivingChildren.getText().toString());
        updatePregnancyDetailsHistory.setNumberOfMiscarriages(etNumberOfMisscarriages.getText().toString());
        updatePregnancyDetailsHistory.setNumberOfPregnancies(etNumberOfPragnancies.getText().toString());
        updatePregnancyDetailsHistory.setComplications(etNotes.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();
        WebServicesHandler.instance.addPregnancyDetail(userId, updatePregnancyDetailsHistory, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                adjustPregnancyDetails(response, updatePregnancyDetailsHistory);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Education details saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });



    }

    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            deliveryTime = calendar.getTimeInMillis();
            tvMammogram.setText(GenericUtils.formatDate(new Date(calendar.getTimeInMillis())));


    }
}
