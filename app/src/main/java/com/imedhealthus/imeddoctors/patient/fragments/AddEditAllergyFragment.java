package com.imedhealthus.imeddoctors.patient.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.CustomAutoCompleteAdapter;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.FileUtils;
import com.imedhealthus.imeddoctors.patient.models.Allergy;
import com.imedhealthus.imeddoctors.patient.models.PatientProfile;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 2/6/18.
 */

public class AddEditAllergyFragment extends BaseFragment {

    @BindView(R.id.et_allergy_to)
    AutoCompleteTextView etAllergyTo;
    @BindView(R.id.et_reaction)
    AutoCompleteTextView etReaction;
    @BindView(R.id.et_notes)
    EditText etNotes;

    private Allergy allergy;
    public AddEditAllergyFragment() {
        allergy = SharedData.getInstance().getSelectedAllergy();
    }

    @Override
    public String getName() {
        return "AddEditAllergyFragment";
    }

    @Override
    public String getTitle() {
        if (allergy == null) {
            return "Add Allergy History";
        }else {
            return "Edit Allergy History";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_allergy, container, false);
        ButterKnife.bind(this, view);

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        ArrayList<String> medicines = FileUtils.parseFile(context, "allergies.txt", ",");
        CustomAutoCompleteAdapter medicineAdapter = new CustomAutoCompleteAdapter(context, R.layout.simple_dropdown_item_1line, etAllergyTo, medicines);
        etAllergyTo.setAdapter(medicineAdapter);
        etAllergyTo.setThreshold(1);
        etAllergyTo.addTextChangedListener(medicineAdapter);

        ArrayList<String> reasons = FileUtils.parseFile(context, "diseases.txt", "\n");
        CustomAutoCompleteAdapter reasonAdapter = new CustomAutoCompleteAdapter(context, R.layout.simple_dropdown_item_1line, etReaction, reasons);
        etReaction.setAdapter(reasonAdapter);
        etReaction.setThreshold(1);
        etReaction.addTextChangedListener(reasonAdapter);

        setData();

    }

    private void setData() {

        if (allergy == null) {
            return;
        }

        etAllergyTo.setText(allergy.getAllergyTo());
        etReaction.setText(allergy.getReaction());
        etNotes.setText(allergy.getNotes());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditAllergy();
                }
                break;

        }
    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etAllergyTo.getText().toString().isEmpty() || etReaction.getText().toString().isEmpty() ||
                etNotes.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustAllergies(RetrofitJSONResponse response, Allergy updatedAllergy) {

        if (allergy == null) {//Add

            JSONObject allergyJson = response.optJSONObject("AllergyHistory");
            if (allergyJson != null) {
                Allergy newAllergy = new Allergy(allergyJson);
                PatientProfile profile = SharedData.getInstance().getSelectedPatientProfile();
                profile.getAllergies().add(newAllergy);
            }

        }else  {

            allergy.setAllergyTo(updatedAllergy.getAllergyTo());
            allergy.setReaction(updatedAllergy.getReaction());
            allergy.setNotes(updatedAllergy.getNotes());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditAllergy() {

        AlertUtils.showProgress(getActivity());

        final Allergy updatedAllergy = new Allergy();
        if (allergy != null) {
            updatedAllergy.setId(allergy.getId());
        }
        updatedAllergy.setAllergyTo(etAllergyTo.getText().toString());
        updatedAllergy.setReaction(etReaction.getText().toString());
        updatedAllergy.setNotes(etNotes.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditAllergy(userId, updatedAllergy, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                adjustAllergies(response, updatedAllergy);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Allergy history saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
