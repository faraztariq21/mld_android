package com.imedhealthus.imeddoctors.patient.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.fragments.FragmentSuggestionDialog;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.SuggestionListItem;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.models.Doctor;
import com.imedhealthus.imeddoctors.doctor.models.Filter;
import com.imedhealthus.imeddoctors.patient.models.PastPhysician;
import com.imedhealthus.imeddoctors.patient.models.PatientProfile;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 2/6/18.
 */

public class AddEditPastPhysicianFragment extends BaseFragment {

    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_location)
    EditText etLocation;
    @BindView(R.id.et_hospital)
    EditText etHospital;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_speciality)
    EditText etSpeciality;
    @BindView(R.id.et_notes)
    EditText etNotes;

    private PastPhysician physician;
    private List<Doctor> activeDoctors;
    private List<Doctor> recommendedDoctors;

    public AddEditPastPhysicianFragment() {
        physician = SharedData.getInstance().getSelectedPastPhysician();
    }

    FragmentSuggestionDialog.FragmentSuggestionDialogListener onDoctorSelectedListener = new FragmentSuggestionDialog.FragmentSuggestionDialogListener() {
        @Override
        public void onSuggestionSelected(int index, SuggestionListItem suggestionListItem) {
            Doctor doctor = (Doctor) suggestionListItem;
            etName.setText(doctor.getFullName());
            etLocation.setText(doctor.getAddress());
            etSpeciality.setText(doctor.getSpeciality());

            if (etLocation.getText().toString().isEmpty())
                etLocation.setFocusable(true);
            else
                etLocation.setFocusable(false);

            if (etSpeciality.getText().toString().isEmpty())
                etSpeciality.setFocusable(true);
            else
                etSpeciality.setFocusable(false);


        }
    };

    @Override
    public String getName() {
        return "AddEditPastPhysicianFragment";
    }

    @Override
    public String getTitle() {
        if (physician == null) {
            return "Add My Physician";
        } else {
            return "Edit My Physician";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_past_physician, container, false);
        ButterKnife.bind(this, view);
        loadData();
        etName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentSuggestionDialog.newInstance(getResources().getString(R.string.select_doctor), (ArrayList<? extends SuggestionListItem>) recommendedDoctors, false, onDoctorSelectedListener).show(getChildFragmentManager());
            }
        });
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        //setData();


    }

    private void setData() {

        if (physician == null) {
            return;
        }

        etName.setText(physician.getName());
        etLocation.setText(physician.getLocation());
        etHospital.setText(physician.getHospital());
        etPhone.setText(physician.getPhone());
        etSpeciality.setText(physician.getSpeciality());
        etNotes.setText(physician.getNotes());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditPhysician();
                }
                break;

        }
    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etName.getText().toString().isEmpty() || etLocation.getText().toString().isEmpty() ||
                etHospital.getText().toString().isEmpty() || etPhone.getText().toString().isEmpty() ||
                etSpeciality.getText().toString().isEmpty() || etNotes.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustPhysicians(RetrofitJSONResponse response, PastPhysician updatedPhysician) {

        if (physician == null) {//Add

            JSONObject physicianJson = response.optJSONObject("MyPhysician");
            if (physicianJson != null) {
                PastPhysician newPhysician = new PastPhysician(physicianJson);
                PatientProfile profile = SharedData.getInstance().getSelectedPatientProfile();
                profile.getPastPhysicians().add(newPhysician);
            }

        } else {

            physician.setName(updatedPhysician.getName());
            physician.setLocation(updatedPhysician.getLocation());
            physician.setHospital(updatedPhysician.getHospital());
            physician.setPhone(updatedPhysician.getPhone());
            physician.setSpeciality(updatedPhysician.getSpeciality());
            physician.setNotes(updatedPhysician.getNotes());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditPhysician() {

        AlertUtils.showProgress(getActivity());

        final PastPhysician updatedPhysician = new PastPhysician();
        if (physician != null) {
            updatedPhysician.setId(physician.getId());
        }
        updatedPhysician.setName(etName.getText().toString());
        updatedPhysician.setLocation(etLocation.getText().toString());
        updatedPhysician.setHospital(etHospital.getText().toString());
        updatedPhysician.setPhone(etPhone.getText().toString());
        updatedPhysician.setSpeciality(etSpeciality.getText().toString());
        updatedPhysician.setNotes(etNotes.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditPastPhysician(userId, updatedPhysician, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                adjustPhysicians(response, updatedPhysician);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Physician saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }


    //Webservices
    private void loadData() {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.findDoctors(new Filter(), new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                } else {

                    activeDoctors = Doctor.parseAvailableDoctors(response);

                    recommendedDoctors = Doctor.parseRecommendedDoctors(response);

                    recommendedDoctors.addAll(activeDoctors);

                }

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);

            }

        });

    }


}
