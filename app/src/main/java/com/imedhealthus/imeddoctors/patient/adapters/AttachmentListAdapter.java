package com.imedhealthus.imeddoctors.patient.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.patient.listeners.OnAttachmentItemClickListener;
import com.imedhealthus.imeddoctors.patient.models.Attachment;
import com.imedhealthus.imeddoctors.patient.view_holder.AttachmentItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Malik Hamid on 1/9/2018.
 */

public class AttachmentListAdapter extends RecyclerView.Adapter<AttachmentItemViewHolder> {

    private List<Attachment> attachments;
    private WeakReference<TextView> tvNoRecords;
    private WeakReference<OnAttachmentItemClickListener> onAttachmentItemClickListener;
    boolean isEditAllowed;


    public AttachmentListAdapter(TextView tvNoRecords, boolean isEditAllowed, OnAttachmentItemClickListener onAttachmentItemClickListener) {

        super();
        this.tvNoRecords = new WeakReference<>(tvNoRecords);
        this.isEditAllowed = isEditAllowed;
        this.onAttachmentItemClickListener = new WeakReference<>(onAttachmentItemClickListener);

    }

    public void setAttachments(List<Attachment> attachments) {

        this.attachments = attachments;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }

    @Override
    public AttachmentItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_attachment, parent, false);

        OnAttachmentItemClickListener listener = null;
        if (onAttachmentItemClickListener != null && onAttachmentItemClickListener.get() != null) {
            listener = onAttachmentItemClickListener.get();
        }

        return new AttachmentItemViewHolder(view, isEditAllowed, listener);

    }

    @Override
    public void onBindViewHolder(AttachmentItemViewHolder viewHolder, int position) {
        viewHolder.setData(getItem(position), position);
    }

    @Override
    public int getItemCount() {
        return attachments == null ? 0 : attachments.size();
    }

    public Attachment getItem(int position) {
        if (position < 0 || position >= attachments.size()) {
            return null;
        }
        return attachments.get(position);
    }

}
