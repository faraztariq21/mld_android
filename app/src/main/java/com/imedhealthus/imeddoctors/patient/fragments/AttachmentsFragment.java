package com.imedhealthus.imeddoctors.patient.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.fragments.FragmentImage;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.patient.adapters.AttachmentListAdapter;
import com.imedhealthus.imeddoctors.patient.listeners.OnAttachmentItemClickListener;
import com.imedhealthus.imeddoctors.patient.models.Attachment;
import com.imedhealthus.imeddoctors.patient.models.PatientProfile;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 1/9/2018.
 */

public class AttachmentsFragment extends BaseFragment implements OnAttachmentItemClickListener {

    @BindView(R.id.rv_attachments)
    RecyclerView rvAttachments;
    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;

    @BindView(R.id.btn_add_attachment)
    Button btnAddAttachment;

    private AttachmentListAdapter adapterAttachments;

    private PatientProfile profile;
    private boolean isEditAllowed;

    public static AttachmentsFragment create(PatientProfile profile, boolean isEditAllowed) {

        AttachmentsFragment fragment = new AttachmentsFragment();

        fragment.profile = profile;
        fragment.isEditAllowed = isEditAllowed;

        return fragment;

    }

    @Override
    public String getName() {
        return "AttachmentFragment";
    }

    @Override
    public String getTitle() {
        return "Attachment";
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_attachments, container, false);
        ButterKnife.bind(this, rootView);

        initHelper();
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SharedData.getInstance().isRefreshRequired()) {
            setData();
        }
    }

    private void initHelper() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rvAttachments.setLayoutManager(layoutManager);

        adapterAttachments = new AttachmentListAdapter(tvNoRecords, isEditAllowed, this);
        rvAttachments.setAdapter(adapterAttachments);

        if (isEditAllowed) {
            btnAddAttachment.setVisibility(View.VISIBLE);
        } else {
            btnAddAttachment.setVisibility(View.GONE);
        }

    }

    private void setData() {
        adapterAttachments.setAttachments(profile.getAttachments());
    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add_attachment:
                openAddAttachment();
                break;

        }
    }

    public void openAddAttachment() {

        Intent intent = new Intent(getContext(), SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddAttachment.ordinal());
        ((BaseActivity) context).startActivity(intent, true);

    }


    //Attachment item click
    @Override
    public void onAttachmentDetailClick(int idx) {

    }

    @Override
    public void onAttachmentDeleteClick(int idx) {

        Attachment attachment = adapterAttachments.getItem(idx);
        if (attachment == null) {
            return;
        }
        showDeleteAlert(attachment);

    }

    @Override
    public void onAttachmentImageClick(int idx) {
        FragmentImage.newInstance(null, adapterAttachments.getItem(idx).getFileUrl()).show(getFragmentManager());
    }

    private void showDeleteAlert(final Attachment attachment) {

        new AlertDialog.Builder(context)
                .setTitle("Confirm")
                .setMessage("Are you sure you want to delete this record?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        deleteAttachment(attachment);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        dialog.dismiss();

                    }
                })
                .show();

    }


    //Other
    private void adjustAttachments(Attachment attachment) {

        profile.getAttachments().remove(attachment);
        adapterAttachments.setAttachments(profile.getAttachments());

    }


    //Web services
    private void deleteAttachment(final Attachment attachment) {

        AlertUtils.showProgress(getActivity());
        WebServicesHandler.instance.deleteAttachment(attachment, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                adjustAttachments(attachment);
                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Attachment deleted successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }


}
