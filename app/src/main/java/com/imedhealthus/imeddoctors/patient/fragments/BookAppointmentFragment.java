package com.imedhealthus.imeddoctors.patient.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.auth.activities.LoginActivity;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.doctor.models.Appointment;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/13/18.
 */

public class BookAppointmentFragment extends BaseFragment {

    @BindView(R.id.tv_appointment_type)
    TextView tvAppointmentType;
    @BindView(R.id.tv_time)
    TextView tvTime;


    @BindView(R.id.tv_physical_location)
    TextView tvPhysicalLocation;
    @BindView(R.id.et_description)
    EditText etDescription;

    Appointment appointment = null;

    @Override
    public String getName() {
        return "BookAppointmentFragment";
    }

    @Override
    public String getTitle() {
        return "Book Appointment";
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_book_appointment, container, false);
        ButterKnife.bind(this, rootView);
        GenericUtils.setEditTextScrollable(etDescription, getActivity());
        setData();
        return rootView;

    }

    private void setData() {

        appointment = SharedData.getInstance().getSelectedAppointment();

        tvAppointmentType.setText(appointment.getType().toString(context));

        if (appointment.getType() == Constants.AppointmentType.Tele) {
            tvPhysicalLocation.setVisibility(View.GONE);
        } else {
            tvPhysicalLocation.setText(appointment.getPhysicalLocation());
        }


        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        tvTime.setText(format.format(appointment.getStartDate()) + " at " + GenericUtils.formatTime(new Date(appointment.getStartTimeStamp())));

    }


    //Actions
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_next:
                if (validateFields()) {
                    bookAppointment();
                }
                break;

        }

    }

    private boolean validateFields() {

        String errorMsg = null;
        if (TextUtils.isEmpty(etDescription.getText().toString().trim())) {
            errorMsg = "Kindly tell us more to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(context, Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void bookAppointment() {

        SharedData.getInstance().setBookAppointmentDescription(etDescription.getText().toString());
        SharedData.getInstance().setShouldContinueBooking(true);

        Intent intent;

        if (!CacheManager.getInstance().isUserLoggedIn()) {

            intent = new Intent(context, LoginActivity.class);

        } else {

            intent = new Intent(context, SimpleFragmentActivity.class);
            intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CompleteAppointment.ordinal());
/*
            List<PaymentMethod> paymentMethods = CacheManager.getInstance().getCurrentUser().getPaymentMethods();
            if (paymentMethods != null && paymentMethods.size() > 0) {
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CompleteAppointment.ordinal());
            }else {
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditPaymentMethod.ordinal());
            }
*/
        }

        ((BaseActivity) context).startActivity(intent, true);

    }

}
