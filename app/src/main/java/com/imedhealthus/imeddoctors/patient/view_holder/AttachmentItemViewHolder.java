package com.imedhealthus.imeddoctors.patient.view_holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.patient.listeners.OnAttachmentItemClickListener;
import com.imedhealthus.imeddoctors.patient.models.Attachment;

import java.lang.ref.WeakReference;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 1/9/2018.
 */

public class AttachmentItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    RelativeLayout contItem;

    @BindView(R.id.iv_icon)
    ImageView ivIcon;
    @BindView(R.id.tv_title)
    TextView tvName;
    @BindView(R.id.tv_date)
    TextView tvDate;

    @BindView(R.id.tv_details)
    TextView tvDetail;
    @BindView(R.id.iv_attachment)
    ImageView ivAttachment;

    @BindView(R.id.iv_delete)
    ImageView ivDelete;

    private int idx;
    private WeakReference<OnAttachmentItemClickListener> onAttachmentItemClickListener;

    public AttachmentItemViewHolder(View view, boolean isEditAllowed, OnAttachmentItemClickListener onAttachmentItemClickListener) {

        super(view);
        ButterKnife.bind(this, view);

        if (isEditAllowed) {
            ivDelete.setVisibility(View.VISIBLE);
        } else {
            ivDelete.setVisibility(View.GONE);
        }

        contItem.setOnClickListener(this);
        ivDelete.setOnClickListener(this);
        this.onAttachmentItemClickListener = new WeakReference<>(onAttachmentItemClickListener);

    }

    public void setData(final Attachment attachment, final int idx) {

        tvName.setText(attachment.getTitle());
        tvDetail.setText(attachment.getNotes());
        if (attachment.getTimeStamp() != 0)
            tvDate.setText(GenericUtils.formatDate(new Date(attachment.getTimeStamp())));
        Glide.with(ApplicationManager.getContext()).load(attachment.getFileUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivAttachment);
        ivAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAttachmentItemClickListener.get().onAttachmentImageClick(idx);
            }
        });
        this.idx = idx;

    }

    @Override
    public void onClick(View view) {

        if (onAttachmentItemClickListener == null || onAttachmentItemClickListener.get() == null) {
            return;
        }

        switch (view.getId()) {
            case R.id.cont_item:
                onAttachmentItemClickListener.get().onAttachmentDetailClick(idx);
                break;

            case R.id.iv_delete:
                onAttachmentItemClickListener.get().onAttachmentDeleteClick(idx);
                break;
        }
    }

}

