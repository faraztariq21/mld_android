package com.imedhealthus.imeddoctors.patient.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Malik Hamid on 2/8/2018.
 */

public class SocialHistory {

    String socailId,smoking,drugs,alcohol,alcoholDrinks,alcoholPreferredDrink,caffeine,caffeineDrinks,tobacco,tobaccoType
            ,tobaccoAmountPerDay,recreationalDrugs,recreationalDrugsType,recreationalDrugsAmountPerWeek
            ,recreationalDrugsLastUsed,exercise,exerciseType,exerciseNoOfDaysPerWeek,hobbiesORLeisureActivities,
            lastMonthFeeling,lastMonthPleasure;
    int alcoholYearQuit,tobaccoYearQuit,tobaccoUsedYear;
    public SocialHistory(JSONObject jsonObject) {

        try {
            if(jsonObject.getString("Id")==null)
                socailId="0";
            else
                socailId = jsonObject.getString("Id").replace("null","");
            smoking = jsonObject.getString("Smoking").replace("null","");
            alcohol = jsonObject.getString("Alcohol").replace("null","");
            alcoholDrinks = jsonObject.getString("AlcoholDrinks").replace("null","");
            alcoholPreferredDrink = jsonObject.getString("AlcoholPreferredDrink").replace("null","");
            alcoholYearQuit = Integer.parseInt(jsonObject.getString("AlcoholYearQuit").replace("null","0"));
            drugs = jsonObject.getString("Drugs").replace("null","");
            caffeine = jsonObject.getString("Caffeine").replace("null","");
            caffeineDrinks = jsonObject.getString("CaffeineDrinks").replace("null","");

            tobacco = jsonObject.optString("Tobacco","").replace("null","");
            tobaccoType = jsonObject.optString("TobaccoType","").replace("null","");

            tobaccoUsedYear = Integer.parseInt(jsonObject.getString("TobaccoUsedYear").replace("null","0"));
            tobaccoAmountPerDay = jsonObject.getString("TobaccoAmountPerDay").replace("null","");
            tobaccoYearQuit = Integer.parseInt(jsonObject.getString("TobaccoYearQuit").replace("null","0"));
            recreationalDrugs = jsonObject.getString("RecreationalDrugs").replace("null","");

            recreationalDrugsType = jsonObject.optString("RecreationalDrugsType","").replace("null","");
            recreationalDrugsAmountPerWeek = jsonObject.optString("RecreationalDrugsAmountPerWeek","").replace("null","");

            recreationalDrugsLastUsed = jsonObject.getString("RecreationalDrugsLastUsed").replace("null","");
            exercise = jsonObject.getString("Exercise").replace("null","");
            exerciseType = jsonObject.getString("ExerciseType").replace("null","");
            exerciseNoOfDaysPerWeek = jsonObject.getString("ExerciseNoOfDaysPerWeek").replace("null","");

            hobbiesORLeisureActivities = jsonObject.optString("HobbiesORLeisureActivities","").replace("null","");
            lastMonthFeeling = jsonObject.optString("LastMonthFeeling","");
            lastMonthPleasure = jsonObject.optString("LastMonthPleasure","");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public SocialHistory() {
        socailId="0";
        smoking = "";
        alcohol ="";
        alcoholDrinks = "";
        alcoholPreferredDrink = "";
        alcoholYearQuit = 0;
        drugs = "";
        caffeine = "";
        caffeineDrinks = "";

        tobacco = "";
        tobaccoType = "";

        tobaccoUsedYear = 0;
        tobaccoAmountPerDay = "";
        tobaccoYearQuit = 0;
        recreationalDrugs = "";

        recreationalDrugsType ="";
        recreationalDrugsAmountPerWeek = "";

        recreationalDrugsLastUsed ="";
        exercise = "";
        exerciseType = "";
        exerciseNoOfDaysPerWeek = "";

        hobbiesORLeisureActivities = "";
        lastMonthFeeling = "";
        lastMonthPleasure = "";
    }

    public String getSocailId() {
        return socailId;
    }

    public void setSocailId(String socailId) {
        this.socailId = socailId;
    }

    public String getSmoking() {
        return smoking;
    }

    public void setSmoking(String smoking) {
        this.smoking = smoking;
    }

    public String getDrugs() {
        return drugs;
    }

    public void setDrugs(String drugs) {
        this.drugs = drugs;
    }

    public String getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(String alcohol) {
        this.alcohol = alcohol;
    }

    public String getAlcoholDrinks() {
        return alcoholDrinks;
    }

    public void setAlcoholDrinks(String alcoholDrinks) {
        this.alcoholDrinks = alcoholDrinks;
    }

    public String getAlcoholPreferredDrink() {
        return alcoholPreferredDrink;
    }

    public void setAlcoholPreferredDrink(String alcoholPreferredDrink) {
        this.alcoholPreferredDrink = alcoholPreferredDrink;
    }

    public int getAlcoholYearQuit() {
        return alcoholYearQuit;
    }

    public void setAlcoholYearQuit(int alcoholYearQuit) {
        this.alcoholYearQuit = alcoholYearQuit;
    }

    public int getTobaccoYearQuit() {
        return tobaccoYearQuit;
    }

    public void setTobaccoYearQuit(int tobaccoYearQuit) {
        this.tobaccoYearQuit = tobaccoYearQuit;
    }

    public int getTobaccoUsedYear() {
        return tobaccoUsedYear;
    }

    public void setTobaccoUsedYear(int tobaccoUsedYear) {
        this.tobaccoUsedYear = tobaccoUsedYear;
    }

    public String getCaffeine() {
        return caffeine;
    }

    public void setCaffeine(String caffeine) {
        this.caffeine = caffeine;
    }

    public String getCaffeineDrinks() {
        return caffeineDrinks;
    }

    public void setCaffeineDrinks(String caffeineDrinks) {
        this.caffeineDrinks = caffeineDrinks;
    }

    public String getTobacco() {
        return tobacco;
    }

    public void setTobacco(String tobacco) {
        this.tobacco = tobacco;
    }

    public String getTobaccoType() {
        return tobaccoType;
    }

    public void setTobaccoType(String tobaccoType) {
        this.tobaccoType = tobaccoType;
    }



    public String getTobaccoAmountPerDay() {
        return tobaccoAmountPerDay;
    }

    public void setTobaccoAmountPerDay(String tobaccoAmountPerDay) {
        this.tobaccoAmountPerDay = tobaccoAmountPerDay;
    }



    public String getRecreationalDrugs() {
        return recreationalDrugs;
    }

    public void setRecreationalDrugs(String recreationalDrugs) {
        this.recreationalDrugs = recreationalDrugs;
    }

    public String getRecreationalDrugsType() {
        return recreationalDrugsType;
    }

    public void setRecreationalDrugsType(String recreationalDrugsType) {
        this.recreationalDrugsType = recreationalDrugsType;
    }

    public String getRecreationalDrugsAmountPerWeek() {
        return recreationalDrugsAmountPerWeek;
    }

    public void setRecreationalDrugsAmountPerWeek(String recreationalDrugsAmountPerWeek) {
        this.recreationalDrugsAmountPerWeek = recreationalDrugsAmountPerWeek;
    }

    public String getRecreationalDrugsLastUsed() {
        return recreationalDrugsLastUsed;
    }

    public void setRecreationalDrugsLastUsed(String recreationalDrugsLastUsed) {
        this.recreationalDrugsLastUsed = recreationalDrugsLastUsed;
    }

    public String getExercise() {
        return exercise;
    }

    public void setExercise(String exercise) {
        this.exercise = exercise;
    }

    public String getExerciseType() {
        return exerciseType;
    }

    public void setExerciseType(String exerciseType) {
        this.exerciseType = exerciseType;
    }

    public String getExerciseNoOfDaysPerWeek() {
        return exerciseNoOfDaysPerWeek;
    }

    public void setExerciseNoOfDaysPerWeek(String exerciseNoOfDaysPerWeek) {
        this.exerciseNoOfDaysPerWeek = exerciseNoOfDaysPerWeek;
    }

    public String getHobbiesORLeisureActivities() {
        return hobbiesORLeisureActivities;
    }

    public void setHobbiesORLeisureActivities(String hobbiesORLeisureActivities) {
        this.hobbiesORLeisureActivities = hobbiesORLeisureActivities;
    }

    public String getLastMonthFeeling() {
        return lastMonthFeeling;
    }

    public void setLastMonthFeeling(String lastMonthFeeling) {
        this.lastMonthFeeling = lastMonthFeeling;
    }

    public String getLastMonthPleasure() {
        return lastMonthPleasure;
    }

    public void setLastMonthPleasure(String lastMonthPleasure) {
        this.lastMonthPleasure = lastMonthPleasure;
    }
}
