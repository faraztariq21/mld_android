package com.imedhealthus.imeddoctors.patient.models;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 2/8/2018.
 */

public class PregnancyDetailsHistory {

    private long  deliveryDate;
    private String id,deliveryType,gender,numberOfPregnancies,numberOfLivingChild,numberOfAbortions,numberOfMiscarriages, complications;

    public PregnancyDetailsHistory() {

    }

    public PregnancyDetailsHistory(JSONObject jsonObject) {

        id = jsonObject.optString("Id", "");
        deliveryDate = GenericUtils.getTimeDateInLong(jsonObject.optString("DeliveryDate", ""));
        deliveryType = jsonObject.optString("DeliveryType", "");

        gender = jsonObject.optString("Gender", "");
        complications = jsonObject.optString("Complications", "");

        numberOfPregnancies = jsonObject.optString("NoOfPregnancies", "");
        numberOfLivingChild = jsonObject.optString("NoOfLivingChildren", "");
        numberOfAbortions = jsonObject.optString("NoOfAbortions", "");
        numberOfMiscarriages = jsonObject.optString("NoOfMiscarriages", "");


    }

    public static List<PregnancyDetailsHistory> parsePregnancyDetailsHistories(JSONArray pregnancyDetailsHistoriesJson){

        ArrayList<PregnancyDetailsHistory> pregnancyDetailsHistories = new ArrayList<>();

        for (int i = 0; i < pregnancyDetailsHistoriesJson.length(); i++){

            JSONObject pregnancyDetailsHistoryJson = pregnancyDetailsHistoriesJson.optJSONObject(i);
            if (pregnancyDetailsHistoryJson != null) {
                PregnancyDetailsHistory pregnancyDetailsHistory = new PregnancyDetailsHistory(pregnancyDetailsHistoryJson);
                pregnancyDetailsHistories.add(pregnancyDetailsHistory);
            }

        }

        return pregnancyDetailsHistories;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(long deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNumberOfPregnancies() {
        return numberOfPregnancies;
    }

    public void setNumberOfPregnancies(String numberOfPregnancies) {
        this.numberOfPregnancies = numberOfPregnancies;
    }

    public String getNumberOfLivingChild() {
        return numberOfLivingChild;
    }

    public void setNumberOfLivingChild(String numberOfLivingChild) {
        this.numberOfLivingChild = numberOfLivingChild;
    }

    public String getNumberOfAbortions() {
        return numberOfAbortions;
    }

    public void setNumberOfAbortions(String numberOfAbortions) {
        this.numberOfAbortions = numberOfAbortions;
    }

    public String getNumberOfMiscarriages() {
        return numberOfMiscarriages;
    }

    public void setNumberOfMiscarriages(String numberOfMiscarriages) {
        this.numberOfMiscarriages = numberOfMiscarriages;
    }

    public String getComplications() {
        return complications;
    }

    public void setComplications(String complications) {
        this.complications = complications;
    }
}
