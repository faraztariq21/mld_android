package com.imedhealthus.imeddoctors.patient.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 2/6/18.
 */

public class Hobby {

    private String id, name;

    public Hobby() {
    }

    public Hobby(JSONObject jsonObject) {

        id = jsonObject.optString("Id", "");
        name = jsonObject.optString("Hobbies", "");

    }

    public static List<Hobby> parseHobbies(JSONArray hobbiesJson){

        List<Hobby> hobbies = new ArrayList<>();

        for(int i = 0; i < hobbiesJson.length(); i++){

            JSONObject hobbyJson = hobbiesJson.optJSONObject(i);
            if (hobbyJson == null) {
                continue;
            }
            Hobby hobby = new Hobby(hobbyJson);
            hobbies.add(hobby);

        }
        return hobbies;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
