package com.imedhealthus.imeddoctors.application;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.imedhealthus.imeddoctors.common.utils.NetworkChangeReceiver;

/**
 * Created by umair on 1/1/18
 */

public class ApplicationManager extends Application {

    private static Context context;
    private Activity mCurrentActivity = null;
    @Override
    public void onCreate() {

        super.onCreate();
        context = this;

        PRDownloader.initialize(getApplicationContext());

        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setDatabaseEnabled(true)
                .build();
        PRDownloader.initialize(getApplicationContext(), config);

       config = PRDownloaderConfig.newBuilder()
                .setReadTimeout(30_000)
                .setConnectTimeout(30_000)
                .build();
        PRDownloader.initialize(getApplicationContext(), config);

    }



    public static Context getContext() {
        return context;
    }

    public Activity getCurrentActivity(){
        return mCurrentActivity;
    }

    public void setCurrentActivity(Activity mCurrentActivity){
        this.mCurrentActivity = mCurrentActivity;
    }



}

