package com.imedhealthus.imeddoctors;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.imedhealthus.imeddoctors.common.fragments.FragmentRichEditor;

public class SampleTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_test);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new FragmentRichEditor(), FragmentRichEditor.class.toString()).commit();


    }
}
