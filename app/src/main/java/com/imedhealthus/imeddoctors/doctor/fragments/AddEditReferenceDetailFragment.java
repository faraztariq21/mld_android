package com.imedhealthus.imeddoctors.doctor.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.imedhealthus.imeddoctors.doctor.models.ReferenceDetail;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddEditReferenceDetailFragment extends BaseFragment {

    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_relation)
    EditText etRelation;
    @BindView(R.id.et_years)
    EditText etYears;

    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_fax)
    EditText etFax;
    @BindView(R.id.et_address)
    EditText etAddress;

    private ReferenceDetail referenceDetail;

    public AddEditReferenceDetailFragment() {
        referenceDetail = SharedData.getInstance().getSelectedReferenceDetail();
    }


    @Override
    public String getName() {
        return "AddEditReferenceDetailFragment";
    }

    @Override
    public String getTitle() {
        if (referenceDetail == null) {
            return "Add Reference";
        }else {
            return "Edit Reference";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_reference_detail, container, false);
        ButterKnife.bind(this, view);

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    private void setData() {

        if (referenceDetail == null) {
            return;
        }

        etName.setText(referenceDetail.getName());
        etRelation.setText(referenceDetail.getRelation());
        etYears.setText(String.valueOf(referenceDetail.getYears()));

        etEmail.setText(referenceDetail.getEmail());
        etPhone.setText(referenceDetail.getPhone());
        etFax.setText(referenceDetail.getFax());
        etAddress.setText(referenceDetail.getAddress());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditReferenceDetail();
                }
                break;

        }
    }


    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etName.getText().toString().isEmpty() || etRelation.getText().toString().isEmpty() ||
                etYears.getText().toString().isEmpty() || etEmail.getText().toString().isEmpty() ||
                etPhone.getText().toString().isEmpty() || etFax.getText().toString().isEmpty() ||
                etAddress.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustReferenceDetails(RetrofitJSONResponse response, ReferenceDetail updatedReferenceDetail) {

        if (referenceDetail == null) {//Add

            JSONObject referenceDetailJson = response.optJSONObject("DoctorReference");
            if (referenceDetailJson != null) {
                ReferenceDetail newReferenceDetail = new ReferenceDetail(referenceDetailJson);
                DoctorProfile profile = SharedData.getInstance().getSelectedDoctorProfile();
                profile.getReferenceDetails().add(newReferenceDetail);
            }

        }else  {

            referenceDetail.setName(updatedReferenceDetail.getName());
            referenceDetail.setRelation(updatedReferenceDetail.getRelation());
            referenceDetail.setYears(updatedReferenceDetail.getYears());

            referenceDetail.setEmail(updatedReferenceDetail.getEmail());
            referenceDetail.setPhone(updatedReferenceDetail.getPhone());
            referenceDetail.setFax(updatedReferenceDetail.getFax());
            referenceDetail.setAddress(updatedReferenceDetail.getAddress());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditReferenceDetail() {

        AlertUtils.showProgress(getActivity());

        final ReferenceDetail updatedReferenceDetail = new ReferenceDetail();
        if (referenceDetail != null) {
            updatedReferenceDetail.setId(referenceDetail.getId());
        }

        updatedReferenceDetail.setName(etName.getText().toString());
        updatedReferenceDetail.setRelation(etRelation.getText().toString());

        int years = 0;
        try {
            years = Integer.parseInt(etYears.getText().toString());
        }catch (Exception ex) {}
        updatedReferenceDetail.setYears(years);

        updatedReferenceDetail.setEmail(etEmail.getText().toString());
        updatedReferenceDetail.setPhone(etPhone.getText().toString());
        updatedReferenceDetail.setFax(etFax.getText().toString());
        updatedReferenceDetail.setAddress(etAddress.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditReferenceDetail(userId, updatedReferenceDetail, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                adjustReferenceDetails(response, updatedReferenceDetail);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Reference details saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
