package com.imedhealthus.imeddoctors.doctor.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseDashboardActivity;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.LiveNotifications;
import com.imedhealthus.imeddoctors.common.models.Notification;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.User;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.NetworkResponseParser;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.Logs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.nekocode.badge.BadgeDrawable;

/**
 * Created by umair on 1/5/18.
 */

public class DoctorDashboardActivity extends BaseDashboardActivity {

    @BindView(R.id.iv_notifications_badge)
    ImageView ivNotificationBadge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        ButterKnife.bind(this);





        changeFragment(Constants.Fragment.DoctorHome, null);

        initMenu();
        SharedData.getInstance().updateUserDeviceToken();
        SharedData.getInstance().getStaticData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setMenuUserData();
        getNotificationsData();
    }

    public void applyBadge(ImageView view, int count) {
        final BadgeDrawable drawable =
                new BadgeDrawable.Builder()
                        .type(BadgeDrawable.TYPE_NUMBER)
                        .number(count)
                        .build();
        view.setVisibility(View.VISIBLE);
        view.setImageDrawable(drawable);
    }


    public void getNotificationsData() {
        User user = CacheManager.getInstance().getCurrentUser();
        WebServicesHandler.instance.getNotifications(user, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                if (response.status()) {
                    List<Notification> notifications = NetworkResponseParser.getNotifications(response.getJSONArray("GetNotification"));
                    int unSeenNotifications = SharedData.getInstance().getUnSeenNotifications();
                    if (unSeenNotifications > 0)
                        applyBadge(ivNotificationBadge, unSeenNotifications);
                    else
                        ivNotificationBadge.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

            }
        });

    }

    public void onNavItemClick(View view) {
        switch (view.getId()) {
            case R.id.cont_nav_home:
                changeFragment(Constants.Fragment.DoctorHome, null);
                break;

            case R.id.cont_nav_notification:
                changeFragment(Constants.Fragment.Notifications, null);
                break;

            case R.id.iv_nav_search:
                changeFragment(Constants.Fragment.PatientsList, null);
                break;

            case R.id.cont_nav_appointment:
                changeFragment(Constants.Fragment.Appointments, null);
                break;

            case R.id.cont_nav_profile:
                changeFragment(Constants.Fragment.DoctorProfile, null);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
