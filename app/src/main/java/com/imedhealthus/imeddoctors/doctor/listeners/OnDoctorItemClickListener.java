package com.imedhealthus.imeddoctors.doctor.listeners;

/**
 * Created by umair on 1/6/18.
 */

public interface OnDoctorItemClickListener {
    void onDoctorItemInfoClick(int idx);
    void onDoctorItemMessageClick(int idx);
    void onDoctorItemCallClick(int idx);
    void onDoctorItemDocProfileClick(int idx);
    void onDoctorItemAppointmentClick(int idx);
}
