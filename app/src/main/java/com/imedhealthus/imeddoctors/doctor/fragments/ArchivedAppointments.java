package com.imedhealthus.imeddoctors.doctor.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;

import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.User;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.doctor.adapters.ArchivedAppointmentsListAdapter;
import com.imedhealthus.imeddoctors.doctor.listeners.OnArchivedAppointmentItemClickListener;
import com.imedhealthus.imeddoctors.doctor.models.ArchivedAppointment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 2/21/2018.
 */

public class ArchivedAppointments extends BaseFragment implements OnArchivedAppointmentItemClickListener{





    @BindView(R.id.tv_month)
    TextView tvMonth;




    @BindView(R.id.rv_appointments)
    RecyclerView rvAppointments;
    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;


    private List<ArchivedAppointment> archivedAppointments;
    private Date currentDate;
    private ArchivedAppointmentsListAdapter adapterAppointments;
    private SimpleDateFormat dateFormat;

    @Override
    public String getName() {
        return "ArchivedAppointmentFragment";
    }

    @Override
    public String getTitle() {
        return "Archived Appointment";
    }

    @Override
    public boolean showHeader() {
        return true;
    }

    public ArchivedAppointments() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_archived_appointment, container, false);
        ButterKnife.bind(this, view);

        initHelper();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    public void initHelper(){

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvAppointments.setLayoutManager(mLayoutManager);

        adapterAppointments = new ArchivedAppointmentsListAdapter(tvNoRecords, this);
        rvAppointments.setAdapter(adapterAppointments);

        archivedAppointments = new ArrayList<>();

        dateFormat = new SimpleDateFormat("MMMM yyyy");
        currentDate = new Date();



        filterAppointments(currentDate);

    }

    private void setData(List<ArchivedAppointment> bookedAppointment) {

        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d");
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");

        for (ArchivedAppointment appointment : bookedAppointment) {
            appointment.adjustData(dateFormat, timeFormat, currentDate);
        }
        this.archivedAppointments = bookedAppointment;
        filterAppointments(currentDate);

    }

    //Filtering
    private void filterAppointments(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int filterYear = calendar.get(Calendar.YEAR);
        int filterMonth = calendar.get(Calendar.MONTH);

        List<ArchivedAppointment> filteredAppointments = new ArrayList<>();

        if(archivedAppointments == null)
            return;
        for (ArchivedAppointment appointment : archivedAppointments) {
            if (appointment.isSameMonth(filterYear, filterMonth)) {
                filteredAppointments.add(appointment);
            }

        }

        adapterAppointments.setAppointments(filteredAppointments);

        String plans;
        if (adapterAppointments.getItemCount() == 0) {
            plans = "No plans for today";
        }else if (adapterAppointments.getItemCount() == 1) {
            plans = "1 plan for today";
        }else {
            plans = adapterAppointments.getItemCount() + " plans for today";
        }


        tvMonth.setText(dateFormat.format(date));

        currentDate = date;

    }









    //Other
    private void showPostponeAlert(final ArchivedAppointment appointment) {

        new AlertDialog.Builder(context)
                .setTitle("Confirm")
                .setMessage("Are you sure you want to Complete this appointment?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        undoCompletionOfficeAppointment(appointment);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        dialog.dismiss();

                    }})
                .show();

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_previous_month:
                filterAppointments(GenericUtils.getDateWithMonthDiff(currentDate, -1));
                break;

            case R.id.iv_next_month:
                filterAppointments(GenericUtils.getDateWithMonthDiff(currentDate, 1));
                break;
        }
    }

    //Webservices
    private void loadData() {

        AlertUtils.showProgress(getActivity());
        User user = CacheManager.getInstance().getCurrentUser();
        String userId = user.getUserId();


        WebServicesHandler.instance.getArchivedAppointments(userId, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                AlertUtils.dismissProgress();
                if(response.status()){
                    archivedAppointments = ArchivedAppointment.getBookedAppointments(response);

                }else {
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                }
                setData(archivedAppointments);
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,Constants.GenericErrorMsg);
            }
        });


    }

    private void undoCompletionOfficeAppointment(final ArchivedAppointment appointment) {

        AlertUtils.showProgress(getActivity());
        WebServicesHandler.instance.undoCompleteAppointment(appointment.getBookedAppintmentId(), new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(context,Constants.ErrorAlertTitle,Constants.GenericErrorMsg);
                    return;
                }
                archivedAppointments.remove(appointment);
                AlertUtils.showAlert(context,Constants.SuccessAlertTitle,response.message());
                setData(archivedAppointments);
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(context,Constants.ErrorAlertTitle,Constants.GenericErrorMsg);
            }
        });

    }




    @Override
    public void onArchivedAppointmentItemDetailClick(int idx) {

        if(!adapterAppointments.getItem(idx).getAction().equalsIgnoreCase("Completed")
                || adapterAppointments.getItem(idx).getType()==Constants.AppointmentType.Tele)
            return;
        adapterAppointments.toggleExpandAt(idx);
    }

    @Override
    public void onArchivedAppointmentItemUndoCompletionClick(int idx) {
        ArchivedAppointment appointment = adapterAppointments.getItem(idx);
        if (appointment == null) {
            return;
        }
        showPostponeAlert(appointment);
    }


}
