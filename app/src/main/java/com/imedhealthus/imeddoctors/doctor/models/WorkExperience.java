package com.imedhealthus.imeddoctors.doctor.models;

import android.text.TextUtils;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class WorkExperience extends OverviewItem {

    private String id, jobTitle, organizationName, supervisor;
    private String email, phone, fax, address;
    private String cityId, cityName, stateId, stateName, countryId, countryName;
    private String startDate, endDate, startDateFormatted, endDateFormatted;
    private boolean isCurrentJob;

    public WorkExperience() {
    }

    public WorkExperience(JSONObject educationJson) {

        id = educationJson.optString("Id", "");
        jobTitle = educationJson.optString("JobTitle", "").replace("null","");
        organizationName = educationJson.optString("OrganizationName", "").replace("null","");
        supervisor = educationJson.optString("SupervisorName", "").replace("null","");

        startDate = educationJson.optString("StartDate", "").replace("null","");
        Date date = GenericUtils.getDateFromString(startDate);
        startDateFormatted = GenericUtils.formatDate(date);

        endDate = educationJson.optString("EndDate", "");
        date = GenericUtils.getDateFromString(endDate);
        endDateFormatted = GenericUtils.formatDate(date);


        email = educationJson.optString("Email", "").replace("null","");
        address = educationJson.optString("Address", "").replace("null","");
        phone = educationJson.optString("ContactNo", "").replace("null","");
        fax = educationJson.optString("Fax", "").replace("null","");

        cityId = educationJson.optString("CityId", "").replace("null","");
        cityName = educationJson.optString("CityName", "").replace("null","");
        stateId = educationJson.optString("StateId", "").replace("null","");
        stateName = educationJson.optString("StateName", "").replace("null","");
        countryId = educationJson.optString("CountryId", "").replace("null","");
        countryName = educationJson.optString("CountryName", "").replace("null","");

        isCurrentJob = educationJson.optBoolean("IsCurrentlyWorking", false);

    }

    public static List<WorkExperience> parseWorkExperiences(JSONArray workExperiencesJson){

        ArrayList<WorkExperience> workExperiences = new ArrayList<>();

        for (int i = 0; i < workExperiencesJson.length(); i++){

            JSONObject workExperienceJson = workExperiencesJson.optJSONObject(i);
            if (workExperienceJson != null) {
                WorkExperience workExperience = new WorkExperience(workExperienceJson);
                workExperiences.add(workExperience);
            }

        }

        return workExperiences;

    }


    @Override
    public String getDisplayTitle() {

        String displayTitle =  jobTitle;
        return displayTitle;

    }

    @Override
    public String getDisplayDetail() {

        String displayDetail = organizationName;

        if (!TextUtils.isEmpty(supervisor)) {
            displayDetail += " - " + supervisor;
        }

        return displayDetail;

    }

    @Override
    public String getDisplayDuration() {

        String displayDuration = startDateFormatted;
        if (!TextUtils.isEmpty(endDateFormatted)) {
            displayDuration += " - " + endDateFormatted;
        }
        return displayDuration;

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDateFormatted() {
        return startDateFormatted;
    }

    public void setStartDateFormatted(String startDateFormatted) {
        this.startDateFormatted = startDateFormatted;
    }

    public String getEndDateFormatted() {
        return endDateFormatted;
    }

    public void setEndDateFormatted(String endDateFormatted) {
        this.endDateFormatted = endDateFormatted;
    }

    public boolean isCurrentJob() {
        return isCurrentJob;
    }

    public void setCurrentJob(boolean currentJob) {
        isCurrentJob = currentJob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
