package com.imedhealthus.imeddoctors.doctor.view_holders;

import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.utils.CustomLinearLayoutManager;
import com.imedhealthus.imeddoctors.doctor.listeners.OnDoctorItemClickListener;
import com.imedhealthus.imeddoctors.doctor.models.Doctor;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/6/18.
 */

public class DoctorItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_info)
    ViewGroup contInfo;
    @BindView(R.id.cont_options)
    ViewGroup contOptions;

    @BindView(R.id.iv_profile)
    ImageView ivProfile;
    @BindView(R.id.iv_pin)
    ImageView ivLocationPin;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_speciality)
    TextView tvSpeciality;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_rating)
    TextView tvRating;

    @BindView(R.id.iv_dropdown)
    ImageView ivDropdown;


    @BindView(R.id.iv_message)
    ImageView ivMessage;
    @BindView(R.id.iv_call)
    ImageView ivCall;
    @BindView(R.id.iv_doc_profile)
    ImageView ivDocProfile;
    @BindView(R.id.iv_appointment)
    ImageView ivAppointment;
    @BindView(R.id.rv_slots)
    public RecyclerView rvSlots;
    @BindView(R.id.tv_slot_availability)
    public TextView tvAvailability;
    private int idx;
    private WeakReference<OnDoctorItemClickListener> onDoctorItemClickListener;

    public DoctorItemViewHolder(View view, OnDoctorItemClickListener onDoctorItemClickListener) {

        super(view);
        ButterKnife.bind(this, view);

        contInfo.setOnClickListener(this);
        ivMessage.setOnClickListener(this);
        ivCall.setOnClickListener(this);
        ivDocProfile.setOnClickListener(this);
        ivAppointment.setOnClickListener(this);
        ivMessage.setColorFilter(Color.WHITE);
        ivDropdown.setVisibility(View.GONE);
        this.onDoctorItemClickListener = new WeakReference<>(onDoctorItemClickListener);
        rvSlots.setLayoutManager(new CustomLinearLayoutManager(ApplicationManager.getContext(), LinearLayoutManager.HORIZONTAL, false));
    }

    public void setData(Doctor doctor, boolean showOptions, int idx) {

        tvName.setText(doctor.getFullName().replace("null", ""));
        tvSpeciality.setText(doctor.getSpeciality().replace("null", "----"));

        if (doctor.getAddress() != null && !doctor.getAddress().equalsIgnoreCase("")) {
            ivLocationPin.setVisibility(View.VISIBLE);
            tvAddress.setVisibility(View.VISIBLE);
            tvAddress.setText(doctor.getAddress());
        } else {
            ivLocationPin.setVisibility(View.GONE);
            tvAddress.setVisibility(View.GONE);
        }
        tvRating.setText(String.format("%.1f", doctor.getRating()));
        Glide.with(ApplicationManager.getContext()).load(doctor.getImageUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivProfile);

        if (showOptions) {
            contOptions.setVisibility(View.VISIBLE);
        } else {
            contOptions.setVisibility(View.GONE);
        }

        this.idx = idx;

    }


    @Override
    public void onClick(View view) {

        if (onDoctorItemClickListener == null || onDoctorItemClickListener.get() == null) {
            return;
        }

        switch (view.getId()) {
            case R.id.cont_info:
                onDoctorItemClickListener.get().onDoctorItemInfoClick(idx);
                break;

            case R.id.iv_message:
                onDoctorItemClickListener.get().onDoctorItemMessageClick(idx);
                break;

            case R.id.iv_call:
                onDoctorItemClickListener.get().onDoctorItemCallClick(idx);
                break;

            case R.id.iv_doc_profile:
                onDoctorItemClickListener.get().onDoctorItemDocProfileClick(idx);
                break;


            case R.id.iv_appointment:
                onDoctorItemClickListener.get().onDoctorItemAppointmentClick(idx);
                break;
        }
    }

}

