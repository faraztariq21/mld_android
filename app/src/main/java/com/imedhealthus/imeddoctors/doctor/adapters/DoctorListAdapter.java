package com.imedhealthus.imeddoctors.doctor.adapters;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.adapters.AdapterListItemDoctorSlot;
import com.imedhealthus.imeddoctors.common.listeners.OnNestedListItemClickListener;
import com.imedhealthus.imeddoctors.common.utils.CustomLinearLayoutManager;
import com.imedhealthus.imeddoctors.doctor.listeners.OnDoctorItemClickListener;
import com.imedhealthus.imeddoctors.doctor.models.Appointment;
import com.imedhealthus.imeddoctors.doctor.models.Doctor;
import com.imedhealthus.imeddoctors.doctor.view_holders.DoctorItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class DoctorListAdapter extends RecyclerView.Adapter<DoctorItemViewHolder> {

    private final RecyclerView.RecycledViewPool viewPool;
    private List<Doctor> doctors;
    private WeakReference<TextView> tvNoRecords;

    private WeakReference<OnDoctorItemClickListener> onDoctorItemClickListener;
    private WeakReference<OnNestedListItemClickListener> onNestedListItemClickListenerWeakReference;
    AdapterListItemDoctorSlot adapterListItemDoctorSlot;
    private boolean canShowOptions;
    private int idxShowingOptions;


    public DoctorListAdapter(TextView tvNoRecords, boolean canShowOptions, OnDoctorItemClickListener onDoctorItemClickListener, OnNestedListItemClickListener onNestedListItemClickListener) {

        super();

        this.tvNoRecords = new WeakReference<>(tvNoRecords);
        this.onDoctorItemClickListener = new WeakReference<>(onDoctorItemClickListener);
        this.onNestedListItemClickListenerWeakReference = new WeakReference<>(onNestedListItemClickListener);
        this.canShowOptions = canShowOptions;
        idxShowingOptions = -1;
        viewPool = new RecyclerView.RecycledViewPool();
    }

    public void setDoctors(List<Doctor> doctors) {

        this.doctors = doctors;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        } else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }

    public void toggleOptionsAt(int idx) {
        if (idx == this.idxShowingOptions) {
            this.idxShowingOptions = -1;
        } else {
            this.idxShowingOptions = idx;
        }
        notifyDataSetChanged();
    }

    @Override
    public DoctorItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_doctor, parent, false);
        OnDoctorItemClickListener listener = null;
        if (onDoctorItemClickListener != null && onDoctorItemClickListener.get() != null) {
            listener = onDoctorItemClickListener.get();
        }
        DoctorItemViewHolder doctorItemViewHolder = new DoctorItemViewHolder(view, listener);
       // doctorItemViewHolder.rvSlots.setRecycledViewPool(viewPool);
        return doctorItemViewHolder;

    }

    @Override
    public void onBindViewHolder(DoctorItemViewHolder viewHolder, int position) {
        viewHolder.setData(getItem(position), (canShowOptions && position == idxShowingOptions), position);
        if (getItem(position).getAppointmentSlots() != null && getItem(position).getAppointmentSlots().size() > 0) {
            adapterListItemDoctorSlot = new AdapterListItemDoctorSlot(ApplicationManager.getContext(), getItem(position).getAppointmentSlots(), onNestedListItemClickListenerWeakReference.get(), position);
            viewHolder.rvSlots.setLayoutManager(new CustomLinearLayoutManager(ApplicationManager.getContext(), LinearLayoutManager.HORIZONTAL, false));
           // viewHolder.rvSlots.setHasFixedSize(true);
            viewHolder.rvSlots.setAdapter(adapterListItemDoctorSlot);
            viewHolder.tvAvailability.setTextColor(ContextCompat.getColor(ApplicationManager.getContext(), R.color.icon_green));
            Appointment appointment = getItem(position).getAppointmentSlots().get(0);
            if (appointment.isToday)
                viewHolder.tvAvailability.setText("Available Today");
            else if (appointment.isWithInTenDays)
                viewHolder.tvAvailability.setText("Available within 10 days");
            else if (appointment.isAfterTenDays)
                viewHolder.tvAvailability.setText("Available after 10 days");

        } else {
            viewHolder.tvAvailability.setText("No Availability");
            viewHolder.tvAvailability.setTextColor(ContextCompat.getColor(ApplicationManager.getContext(), R.color.red));
        }


    }

    @Override
    public int getItemCount() {
        return doctors == null ? 0 : doctors.size();
    }

    public Doctor getItem(int position) {
        if (position < 0 || position >= doctors.size()) {
            return null;
        }
        return doctors.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
