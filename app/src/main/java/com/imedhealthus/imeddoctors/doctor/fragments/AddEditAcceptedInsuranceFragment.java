package com.imedhealthus.imeddoctors.doctor.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.models.AcceptedInsurance;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddEditAcceptedInsuranceFragment extends BaseFragment {

    @BindView(R.id.et_accepted_insurance)
    EditText etAcceptedInsurance;

    private AcceptedInsurance acceptedInsurance;

    public AddEditAcceptedInsuranceFragment() {
        acceptedInsurance = SharedData.getInstance().getSelectedAcceptedInsurance();
    }


    @Override
    public String getName() {
        return "AddEditAcceptedInsuranceFragment";
    }

    @Override
    public String getTitle() {
        if (acceptedInsurance == null) {
            return "Add Accepted Insurance";
        }else {
            return "Edit Accepted Insurance";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_accepted_insurance, container, false);
        ButterKnife.bind(this, view);

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    private void setData() {

        if (acceptedInsurance == null) {
            return;
        }

        etAcceptedInsurance.setText(acceptedInsurance.getName());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditAcceptedInsurance();
                }
                break;

        }
    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etAcceptedInsurance.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustAcceptedInsurances(RetrofitJSONResponse response, AcceptedInsurance updatedAcceptedInsurance) {

        if (acceptedInsurance == null) {//Add

            JSONObject acceptedInsuranceJson = response.optJSONObject("DoctorAcceptedInsurance");
            if (acceptedInsuranceJson != null) {
                AcceptedInsurance newAcceptedInsurance = new AcceptedInsurance(acceptedInsuranceJson);
                DoctorProfile profile = SharedData.getInstance().getSelectedDoctorProfile();
                profile.getAcceptedInsurances().add(newAcceptedInsurance);
            }

        }else  {

            acceptedInsurance.setName(updatedAcceptedInsurance.getName());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditAcceptedInsurance() {

        AlertUtils.showProgress(getActivity());

        final AcceptedInsurance updatedAcceptedInsurance = new AcceptedInsurance();
        if (acceptedInsurance != null) {
            updatedAcceptedInsurance.setId(acceptedInsurance.getId());
        }

        updatedAcceptedInsurance.setName(etAcceptedInsurance.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditAcceptedInsurance(userId, updatedAcceptedInsurance, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                adjustAcceptedInsurances(response, updatedAcceptedInsurance);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Accepted insurance saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
