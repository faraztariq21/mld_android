package com.imedhealthus.imeddoctors.doctor.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.doctor.adapters.AppointmentsListAdapter;
import com.imedhealthus.imeddoctors.doctor.listeners.OnAppointmentItemClickListener;
import com.imedhealthus.imeddoctors.doctor.models.Appointment;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/6/18.
 */

public class AppointmentsFragment extends BaseFragment implements OnAppointmentItemClickListener, FragmentAppointmentSlotDetail.FragmentAppointmentSlotClickListener {

    @BindView(R.id.btn_physical_appointment)
    TextView btnPhysicalAppointment;
    @BindView(R.id.btn_tele_appointment)
    TextView btnTeleAppointment;

    @BindView(R.id.tv_month)
    TextView tvMonth;
    @BindView(R.id.iv_add_appointment)
    ImageView ivAddAppointment;


    @BindView(R.id.rv_appointments)
    RecyclerView rvAppointments;
    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;
    @BindView(R.id.btn_request_appointment)
    TextView bRequestAppointment;


    @BindView(R.id.tv_previous_date)
    TextView tvPreviousDate;
    @BindView(R.id.tv_current_date)
    TextView tvCurrentDate;
    @BindView(R.id.tv_next_date)
    TextView tvNextDate;
    @BindView(R.id.ll_next_appointment)
    LinearLayout llNextAppointment;
    @BindView(R.id.tv_next_apt_date)
    TextView tvNextAppointmentDate;

    public static final int REQUEST_CALL_PERMISSION = 956;
    private AppointmentsListAdapter adapterAppointments;

    private SimpleDateFormat monthFormatter, dayFormatter;
    private Date currentDate;

    private DoctorProfile profile;
    private boolean isPersonalProfile;
    private Constants.AppointmentType selectedAppointmentType;

    public static AppointmentsFragment create(DoctorProfile profile, boolean isPersonalProfile, Constants.AppointmentType selectedAppointmentType) {

        AppointmentsFragment fragment = new AppointmentsFragment();

        fragment.profile = profile;
        fragment.isPersonalProfile = isPersonalProfile;
        fragment.selectedAppointmentType = selectedAppointmentType;

        return fragment;

    }

    @Override
    public void onPause() {
        super.onPause();
        SharedData.getInstance().setSelectedDoctorProfileTab(DoctorProfileFragment.DoctorProfileTabs.PERSONAL_INFO_TAB);
    }

    @Override
    public String getName() {
        return "AppointmentsFragment";
    }

    @Override
    public String getTitle() {
        return "Appointments";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_doctor_appointments, container, false);
        ButterKnife.bind(this, rootView);

        initHelper();
        return rootView;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        filterAppointmentsByDate(currentDate);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SharedData.getInstance().isRefreshRequired()) {
            filterAppointmentsByDate(currentDate);
        }
    }

    private void initHelper() {

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        rvAppointments.setLayoutManager(layoutManager);

        if (isPersonalProfile) {
            adapterAppointments = new AppointmentsListAdapter(tvNoRecords, bRequestAppointment, null, true, false, this);

        } else {
            adapterAppointments = new AppointmentsListAdapter(tvNoRecords, bRequestAppointment, null, false, false, this);
        }
        rvAppointments.setAdapter(adapterAppointments);

        monthFormatter = new SimpleDateFormat("MMMM yyyy");


        dayFormatter = new SimpleDateFormat("dd EEE");

        if (isPersonalProfile) {
            ivAddAppointment.setVisibility(View.VISIBLE);

        } else {
            ivAddAppointment.setVisibility(View.GONE);

        }
        currentDate = new Date();

        toggleAppointmentType(false);

    }

    private void toggleAppointmentType(boolean isPhysical) {

        btnPhysicalAppointment.setSelected(isPhysical);
        btnTeleAppointment.setSelected(!isPhysical);

        if (isPhysical) {
            selectedAppointmentType = Constants.AppointmentType.OFFICE;
        } else {
            selectedAppointmentType = Constants.AppointmentType.Tele;
        }
        filterAppointmentsByDate(currentDate);

    }

    public void setSelectedAppointmentType(Constants.AppointmentType selectedAppointmentType) {
        this.selectedAppointmentType = selectedAppointmentType;
        filterAppointments(currentDate);
    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_tele_appointment:
                toggleAppointmentType(false);
                break;

            case R.id.btn_physical_appointment:
                toggleAppointmentType(true);
                break;
            case R.id.iv_previous_month:
                filterAppointmentsByDate(GenericUtils.getDateWithMonthDiff(currentDate, -1));
                break;

            case R.id.iv_next_month:
                filterAppointmentsByDate(GenericUtils.getDateWithMonthDiff(currentDate, 1));
                break;

            case R.id.iv_add_appointment:
            case R.id.tv_add_appointment:
                openAddAppointmentSlots();
                break;


            case R.id.iv_previous_date:
            case R.id.tv_previous_date:
                filterAppointmentsByDate(GenericUtils.getDateWithHourDiff(currentDate, -24));
                break;

            case R.id.iv_next_date:
            case R.id.tv_next_date:
                filterAppointmentsByDate(GenericUtils.getDateWithHourDiff(currentDate, 24));
                break;
            case R.id.btn_request_appointment:
                if (android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PERMISSION);
                    return;
                }
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + getActivity().getString(R.string.contact_support)));
                context.startActivity(intent);
                break;
        }
    }

    private void openAddAppointmentSlots() {

        SharedData.getInstance().setSelectedAppointment(null);


        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddAppointmentSlots.ordinal());
        ((BaseActivity) context).startActivity(intent, true);

    }


    //Other
    private void filterAppointmentsByDate(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int filterYear = calendar.get(Calendar.YEAR);
        int filterMonth = calendar.get(Calendar.MONTH);
        int filterDay = calendar.get(Calendar.DAY_OF_MONTH);

        List<Appointment> appointments = profile.getAppointments();
        List<Appointment> filteredAppointments = new ArrayList<>();

        for (Appointment appointment : appointments) {
            if (appointment.isSameDay(filterYear, filterMonth, filterDay)
                    && !appointment.hasPassed()) {
                filteredAppointments.add(appointment);
            }
        }

        adapterAppointments.setAppointments(filteredAppointments);
        if (filteredAppointments.size() == 0) {

            final Appointment appointment = getNextAppointment(date, (ArrayList<Appointment>) appointments);
            if (appointment != null) {
                llNextAppointment.setVisibility(View.VISIBLE);
                tvNextAppointmentDate.setText(appointment.getFormattedDate());
                tvNextAppointmentDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        filterAppointmentsByDate(appointment.getStartDate());
                    }
                });
            } else {
                llNextAppointment.setVisibility(View.GONE);
            }

        } else
            llNextAppointment.setVisibility(View.GONE);


        tvMonth.setText(monthFormatter.format(date));
        tvCurrentDate.setText(dayFormatter.format(date));

        calendar.add(Calendar.HOUR, -24);
        tvPreviousDate.setText(dayFormatter.format(calendar.getTime()));

        calendar.add(Calendar.HOUR, 48);
        tvNextDate.setText(dayFormatter.format(calendar.getTime()));

        currentDate = date;

    }

    private Appointment getNextAppointment(Date currentDate, ArrayList<Appointment> appointments) {


        ArrayList<Appointment> appointmentsAfterGivenDate = new ArrayList<>();

        for (int i = 0; i < appointments.size(); i++) {
            if (appointments.get(i).getStartDate().compareTo(currentDate) > 0) {
                appointmentsAfterGivenDate.add(appointments.get(i));
            }
        }


        Collections.sort(appointmentsAfterGivenDate, new Comparator<Appointment>() {
            @Override
            public int compare(Appointment o1, Appointment o2) {
                return o1.getStartDate().compareTo(o2.getStartDate());
            }
        });

        if (appointmentsAfterGivenDate.size() > 0)
            return appointmentsAfterGivenDate.get(0);


        return null;
    }

    private void filterAppointments(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int filterYear = calendar.get(Calendar.YEAR);
        int filterMonth = calendar.get(Calendar.MONTH);

        List<Appointment> appointments = profile.getAppointments();
        List<Appointment> filteredAppointments = new ArrayList<>();

        for (Appointment appointment : appointments) {
            if (/*selectedAppointmentType == appointment.getType() && */appointment.isSameMonth(filterYear, filterMonth)) {
                filteredAppointments.add(appointment);
            }
        }

        adapterAppointments.setAppointments(filteredAppointments);
        tvMonth.setText(monthFormatter.format(date));
        currentDate = date;

    }

    private void showCancelAlert(final Appointment appointment) {

        new AlertDialog.Builder(context)
                .setTitle("Confirm")
                .setMessage("Are you sure you want to delete this appointment?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        deleteAppointmentSlot(appointment);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        dialog.dismiss();

                    }
                })
                .show();

    }

    private void showOptionAlert(final Appointment appointment) {


        showCancelAlert(appointment);
    /*    new AlertDialog.Builder(context)
                .setTitle("What would you like to do?")
                .setNegativeButton("Update", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        updateAppointmentSlot(appointment);

                    }
                })
                .setPositiveButton("Delete",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        showCancelAlert(appointment);

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        dialog.dismiss();

                    }})
                .show();
*/

    }

    private void updateAppointmentSlot(Appointment appointment) {

        SharedData.getInstance().setSelectedAppointment(appointment);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddAppointmentSlots.ordinal());
        ((BaseActivity) context).startActivity(intent, true);
    }

    //Appointment item click
    @Override
    public void onAppointmentItemClick(int idx) {

        Appointment appointment = adapterAppointments.getItem(idx);

        FragmentAppointmentSlotDetail.newInstance(appointment, idx, isPersonalProfile).show(getChildFragmentManager());

    }

    //Web services
    public void deleteAppointmentSlot(final Appointment appointment) {

        AlertUtils.showProgress(getActivity());
        WebServicesHandler.instance.deleteAppointmentSlot(Integer.toString(appointment.getAppointmentId()), new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                if (!response.status()) {
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, response.message());
                    return;
                }

                profile.getAppointments().remove(appointment);
                AlertUtils.showAlert(context, Constants.SuccessAlertTitle, "Appointment deleted successfully");

                toggleAppointmentType(false);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(context, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (getActivity().checkSelfPermission(android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {
                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case REQUEST_CALL_PERMISSION: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(getActivity(), "Permission granted", Toast.LENGTH_SHORT).show();
                    if (android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PERMISSION);
                        return;
                    }
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + getActivity().getString(R.string.contact_support)));
                    context.startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onAppointmentCancelTapped(int index) {
        Appointment appointment = adapterAppointments.getItem(index);
        showOptionAlert(appointment);

    }

    @Override
    public void onAppointmentBookTapped(int index) {
        Appointment appointment = adapterAppointments.getItem(index);
        if (appointment.hasPassed()) {
            AlertUtils.showAlert(context, Constants.ErrorAlertTitle, "This appointment is expired.");
            return;
        }


        SharedData.getInstance().setSelectedAppointment(appointment);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.BookAppointment.ordinal());
        ((BaseActivity) context).startActivity(intent, true);
    }
}
