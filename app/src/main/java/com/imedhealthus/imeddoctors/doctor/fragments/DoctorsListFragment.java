package com.imedhealthus.imeddoctors.doctor.fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.CallActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.listeners.OnNestedListItemClickListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.CallData;
import com.imedhealthus.imeddoctors.common.models.ChatItem;
import com.imedhealthus.imeddoctors.common.models.ChatUserModel;
import com.imedhealthus.imeddoctors.common.models.ChatUsers;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.User;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.adapters.DoctorListAdapter;
import com.imedhealthus.imeddoctors.doctor.listeners.OnDoctorItemClickListener;
import com.imedhealthus.imeddoctors.doctor.models.Appointment;
import com.imedhealthus.imeddoctors.doctor.models.Doctor;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/6/18.
 */

public class DoctorsListFragment extends BaseFragment implements OnDoctorItemClickListener, TextWatcher, OnNestedListItemClickListener {

    @BindView(R.id.et_search)
    EditText etSearch;

    @BindView(R.id.rv_doctors)
    RecyclerView rvDoctors;
    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;

    private DoctorListAdapter adapterDoctors;
    private List<Doctor> doctors = new ArrayList<>();
    private int numberOfDoctorsAppointmentsLoaded;

    @Override
    public String getName() {
        return "DoctorsListFragment";
    }

    @Override
    public String getTitle() {
        return "Doctors List";
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_doctors_list, container, false);
        ButterKnife.bind(this, rootView);
        initializeShimmer(rootView);
        initHelper();
        return rootView;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    private void initHelper() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rvDoctors.setLayoutManager(layoutManager);

        adapterDoctors = new DoctorListAdapter(tvNoRecords, true, this, this);
        rvDoctors.setAdapter(adapterDoctors);

        etSearch.addTextChangedListener(this);

    }


    //DoctorProfile item click
    @Override
    public void onDoctorItemInfoClick(int idx) {

        adapterDoctors.toggleOptionsAt(idx);

    }

    @Override
    public void onDoctorItemMessageClick(int idx) {

        Doctor doctor = adapterDoctors.getItem(idx);
        if (doctor == null) {
            return;
        }

        User user = CacheManager.getInstance().getCurrentUser();

        ArrayList<ChatUserModel> chatUsers = new ArrayList<>();
        chatUsers.add(new ChatUserModel(user.getLoginId(), "patient"));
        chatUsers.add(new ChatUserModel(doctor.getLoginId(), "doctor"));
        ChatUsers users = new ChatUsers(chatUsers);

        startNewOrOpenChat(users);

    }

    @Override
    public void onDoctorItemCallClick(int idx) {

        Doctor doctor = adapterDoctors.getItem(idx);
        if (doctor == null) {
            return;
        }

        if (!doctor.isCallAllowed()) {
            AlertUtils.showAlert(context, Constants.ErrorAlertTitle, "You can't call right now. Kindly wait for appointed time.");
            return;
        }

        startCall(doctor);

    }

    public class LoadDoctorsAppointments extends AsyncTask<List<Doctor>, List<Doctor>, List<Doctor>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            startShimmerAnimation();

        }

        @Override
        protected List<Doctor> doInBackground(final List<Doctor>... lists) {

            for (final Doctor doctor : lists[0]) {
                WebServicesHandler.instance.getDoctorsAvaiableAppointmentSlots(doctor.getUserId(), new CustomCallback<RetrofitJSONResponse>() {
                    @Override
                    public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                        //  AlertUtils.dismissProgress();

                        if (!response.status()) {
                            AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                            return;
                        }

                        List<Appointment> appointments = Appointment.parseAppointments(response.getJSONArray("Appointments"));


                        Date currentDate = new Date();
                        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd/MM/yyyy");
                        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");

                        for (Appointment appointment : appointments) {
                            appointment.adjustData(dateFormat, timeFormat, currentDate);
                        }

                        ArrayList<Appointment> finalAppointmentSlots = new ArrayList<>();
                        for (Appointment appointment : appointments) {
                            if (!appointment.hasPassed())


                                finalAppointmentSlots.add(appointment);
                        }


                        doctor.setAppointmentSlots(finalAppointmentSlots);
                        numberOfDoctorsAppointmentsLoaded++;
                        publishProgress(lists);
                    }

                    @Override
                    public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                        numberOfDoctorsAppointmentsLoaded++;
                        publishProgress(lists);
                    }
                });
            }
            publishProgress(lists);

            return lists[0];
        }

        @Override
        protected void onProgressUpdate(List<Doctor>... values) {
            super.onProgressUpdate(values);
            /*recommendedDoctors = values[0];
            toggleDoctorsList(true);
*/


            if (numberOfDoctorsAppointmentsLoaded == doctors.size()) {
                doctors = values[0];
                Collections.sort(doctors, new Comparator<Doctor>() {
                    @Override
                    public int compare(Doctor o1, Doctor o2) {
                        return (o2.getAppointmentSlots() != null ? o2.getAppointmentSlots().size() : 0) - (o1.getAppointmentSlots() != null ? o1.getAppointmentSlots().size() : 0);
                    }
                });


                filterDoctors();
                stopShimmerAnimation();
            }

        }

        @Override
        protected void onPostExecute(List<Doctor> doctors) {
            super.onPostExecute(doctors);

        }
    }

    @Override
    public void onDoctorItemDocProfileClick(int idx) {

        Doctor doctor = adapterDoctors.getItem(idx);
        if (doctor == null) {
            return;
        }
        SharedData.getInstance().setSelectedDoctor(doctor);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.DoctorProfile.ordinal());
        ((BaseActivity) context).startActivity(intent, true);

    }

    @Override
    public void onDoctorItemAppointmentClick(int idx) {

        Doctor doctor = adapterDoctors.getItem(idx);
        if (doctor == null) {
            return;
        }
        SharedData.getInstance().setSelectedDoctor(doctor);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.SelectTime.ordinal());
        ((BaseActivity) context).startActivity(intent, true);

    }


    //Search filtering
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        filterDoctors();
    }

    private void filterDoctors() {

        String searchTxt = etSearch.getText().toString().toLowerCase();
        List<Doctor> filteredDoctors = new ArrayList<>();

        for (Doctor doctor : doctors) {
            if (doctor.getFullName().toLowerCase().contains(searchTxt) || doctor.getSpeciality().toLowerCase().contains(searchTxt)) {
                filteredDoctors.add(doctor);
            }
        }
        adapterDoctors.setDoctors(filteredDoctors);

    }


    //Webservices
    private void loadData() {

        startShimmerAnimation();
        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.getDoctorsListForPatient(userId, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                stopShimmerAnimation();
                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                } else {

                    JSONArray doctorsJson = response.optJSONArray("DoctorList");
                    if (doctorsJson != null) {
                        doctors = Doctor.parseDoctors(doctorsJson);
                    }

                }

                new LoadDoctorsAppointments().execute(doctors);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void startNewOrOpenChat(ChatUsers users) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.startChat(users, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                JSONObject chatJson = response.optJSONObject("Chats");
                if (chatJson == null) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
                    return;
                }

                ChatItem chatItem = new ChatItem(chatJson);
                SharedData.getInstance().setSelectedChatItem(chatItem);

                Intent intent = new Intent(context, SimpleFragmentActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.ChatFragment.ordinal());
                ((BaseActivity) context).startActivity(intent, true);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void startCall(final Doctor doctor) {

        AlertUtils.showProgress(getActivity());

        String userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        String partnerLoginId = doctor.getLoginId();
        String myName = CacheManager.getInstance().getCurrentUser().getFullName();

        WebServicesHandler.instance.getDataForCallWithoutNotification(userLoginId, partnerLoginId, myName, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                String sessionId = response.optString("SessionId", "");
                String token = response.optString("TokenId", "");
                boolean isUserLive = response.optBoolean("IsUserLive", false);


                String senderName = CacheManager.getInstance().getCurrentUser().getFullName();

                if (!isUserLive) {
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, doctor.getFullName() + " is not online right now. We have notified him. Kindly try again in a few minutes.");

                    CallData data = new CallData(doctor.getLoginId(), senderName, "", "", "");
                    // OpenTokHandler.getInstance().sendPushNotification(data);

                    return;
                }

                SharedData.getInstance().setOpenTokCallSessionId(sessionId);
                SharedData.getInstance().setOpenTokCallToken(token);

                CallData data = new CallData(doctor.getLoginId(), senderName, sessionId, token, doctor.getUserId());
                //OpenTokHandler.getInstance().sendCallSignal(data);
                SharedData.getInstance().setCallData(data);
                Intent intent = new Intent(context, CallActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CallFragment.ordinal());
                ((BaseActivity) context).startActivity(intent, true);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

    @Override
    public void onNestedListItemClicked(int parentIndex, int childIndex) {
        bookAppointment(doctors.get(parentIndex).getAppointmentSlots().get(childIndex));
    }

    public void bookAppointment(Appointment appointment) {

        if (appointment.hasPassed()) {
            AlertUtils.showAlert(context, Constants.ErrorAlertTitle, "This appointment is expired.");
            return;
        }


        SharedData.getInstance().setSelectedAppointment(appointment);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.BookAppointment.ordinal());
        ((BaseActivity) context).startActivity(intent, true);
    }

}
