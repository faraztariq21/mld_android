package com.imedhealthus.imeddoctors.doctor.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.Logs;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/13/18.
 */

public class DoctorProfileFragment extends BaseFragment {

    private boolean isDoctorLoginId = false;
    @BindView(R.id.iv_profile)
    ImageView ivProfile;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_speciality)
    TextView tvSpeciality;

    @BindView(R.id.iv_star)
    ImageView ivStart;

    @BindView(R.id.iv_overview)
    ImageView ivOverview;
    @BindView(R.id.iv_appointment)
    ImageView ivAppointments;
    @BindView(R.id.iv_services)
    ImageView ivServices;
    @BindView(R.id.iv_reviews)
    ImageView ivReviews;

    @BindView(R.id.tv_nav_overview)
    TextView tvNavOverview;
    @BindView(R.id.tv_nav_appointments)
    TextView tvNavAppointments;
    @BindView(R.id.tv_nav_services)
    TextView tvNavServices;
    @BindView(R.id.tv_nav_review)
    TextView tvNavFeedback;

    @BindView(R.id.cont_nav_overview)
    LinearLayout contOverview;
    @BindView(R.id.cont_nav_appointment)
    LinearLayout contAppontments;
    @BindView(R.id.cont_nav_services)
    LinearLayout contServices;
    @BindView(R.id.cont_nav_reviews)
    LinearLayout contReview;

    @BindView(R.id.tv_personal_phone)
    TextView tvMobile;
    @BindView(R.id.iv_edit_profile)
    ImageView ivEditProfile;

    private String doctorId;
    private DoctorProfile profile;
    public static BaseFragment currentFragment;
    private boolean isPersonalProfile;
    private static LinearLayout selectedCountaner;


    int selectedTab;
    public static final String SELECTED_TAB = "selected_tab";
    public static final String IS_PERSONAL_PROFILE = "is_personal_profile";
    public static final String DOCTOR_ID = "doctor_id";

    public interface DoctorProfileTabs {

        public static final int PERSONAL_INFO_TAB = 0;
        public static final int APPOINTMENTS_TAB = 1;
        public static final int SERVICES_TAB = 2;
        public static final int REVIEWS_TAB = 3;
    }




    public static DoctorProfileFragment newInstance(int tab) {
        DoctorProfileFragment doctorProfileFragment = new DoctorProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(SELECTED_TAB, tab);
        doctorProfileFragment.setArguments(bundle);
        return doctorProfileFragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            selectedTab = getArguments().getInt(SELECTED_TAB);

        }
    }

    public DoctorProfileFragment() {
        if (SharedData.getInstance().getSelectedDoctorLoginId() != null) {
            doctorId = SharedData.getInstance().getSelectedDoctorLoginId();
            isDoctorLoginId = true;
        } else if (SharedData.getInstance().getSelectedDoctorId() != null) {
            doctorId = SharedData.getInstance().getSelectedDoctorId();
        } else if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor) {
            doctorId = CacheManager.getInstance().getCurrentUser().getUserId();
            isPersonalProfile = true;

        } else if (SharedData.getInstance().getSelectedDoctor() != null) {
            doctorId = SharedData.getInstance().getSelectedDoctor().getUserId();
            isPersonalProfile = false;

        }

    }

    @Override
    public String getName() {
        return "DoctorProfileFragment";
    }

    @Override
    public String getTitle() {
        return "Doctor Profile";
    }

    @Override
    public boolean showHeader() {
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_doctor_profile, container, false);
        ButterKnife.bind(this, view);


        if (selectedTab == DoctorProfileTabs.PERSONAL_INFO_TAB)
            selectedCountaner = contOverview;
        else if (selectedTab == DoctorProfileTabs.APPOINTMENTS_TAB)
            selectedCountaner = contAppontments;
        else if (selectedTab == DoctorProfileTabs.REVIEWS_TAB)
            selectedCountaner = contReview;
        else
            selectedCountaner = contServices;

        initHelper();

        ivEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SimpleFragmentActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.EditDoctorPersonalInfo.ordinal());
                ((BaseActivity) context).startActivity(intent, true);

            }
        });

        if (isPersonalProfile)
            ivEditProfile.setVisibility(View.VISIBLE);
        else
            ivEditProfile.setVisibility(View.GONE);
        return view;

    }

    @Override
    public void onResume() {
        super.onResume();


        if (profile == null) {
            loadData();
        } else {
            if (SharedData.getInstance().getRefreshViaServerHit()) {
                loadData();
            } else {
                setProfileData();

            }
        }
    }

    private void initHelper() {


    }

    private void setData(DoctorProfile profile) {

        this.profile = profile;
        SharedData.getInstance().setSelectedDoctorProfile(profile);

        setProfileData();

        ImageView imageView;
        if (currentFragment instanceof AppointmentsFragment) {
            imageView = ivAppointments;
        } else if (currentFragment instanceof ServicesFragment) {
            imageView = ivServices;
        } else if (currentFragment instanceof FeedbackFragment) {
            imageView = ivReviews;
        } else {
            imageView = ivOverview;
        }
        //onClick(contOverview);

    }

    private void setProfileData() {

        tvName.setText(profile.getPersonalInfo().getFullName());


        if (SharedData.getInstance().getSelectedDoctor() != null) {
            tvSpeciality.setText(SharedData.getInstance().getSelectedDoctor().getSpeciality());

            tvMobile.setText(String.format("%.1f", profile.getFeedbackOverallRating()));
            ivStart.setVisibility(View.VISIBLE);
        } else {
            tvSpeciality.setText(profile.getPersonalInfo().getPrimaryEmail());
            tvMobile.setText(profile.getPersonalInfo().getPersonalPhone());
        }


        if (selectedTab == DoctorProfileTabs.PERSONAL_INFO_TAB)
            changeChildFragment(DoctorPersonalInfoFragment.create(profile, isPersonalProfile));
        else if (selectedTab == DoctorProfileTabs.APPOINTMENTS_TAB)
            changeChildFragment(AppointmentsFragment.create(profile, isPersonalProfile, Constants.AppointmentType.OFFICE));
        else if (selectedTab == DoctorProfileTabs.SERVICES_TAB)
            changeChildFragment(ServicesFragment.create(profile, isPersonalProfile));
        Glide.with(ApplicationManager.getContext()).load(profile.getPersonalInfo().getProfileImageUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivProfile);

        onClick(selectedCountaner);

    }


    //Actions
    public void onClick(View view) {

        if (profile == null) {
            return;
        }

        switch (view.getId()) {


            case R.id.cont_nav_overview:
                changeChildFragment(DoctorPersonalInfoFragment.create(profile, isPersonalProfile));
                selectedCountaner = (LinearLayout) view;
                adjustNavBar(ivOverview);
                break;

            case R.id.cont_nav_appointment:
                changeChildFragment(AppointmentsFragment.create(profile, isPersonalProfile, Constants.AppointmentType.Tele));
                selectedCountaner = (LinearLayout) view;
                adjustNavBar(ivAppointments);
                break;

            case R.id.cont_nav_services:
                changeChildFragment(ServicesFragment.create(profile, isPersonalProfile));
                selectedCountaner = (LinearLayout) view;
                adjustNavBar(ivServices);
                break;

            case R.id.cont_nav_reviews:
                changeChildFragment(FeedbackFragment.create(profile, !isPersonalProfile));
                selectedCountaner = (LinearLayout) view;
                adjustNavBar(ivReviews);
                break;

            default:
                if (currentFragment != null) {
                    currentFragment.onClick(view);
                }
        }
    }

    private void changeChildFragment(BaseFragment fragment) {

        currentFragment = fragment;
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.cont_inner_content, fragment, fragment.getName());
        fragmentTransaction.commitAllowingStateLoss();
        getChildFragmentManager().executePendingTransactions();

    }

    private void adjustNavBar(ImageView ivSelected) {

        int normalColor = Color.WHITE;
        int selectedColor = getResources().getColor(R.color.light_blue);
        int normalTextColor = getResources().getColor(R.color.icon_green);

        if (ivOverview == ivSelected) {
            ivOverview.setImageResource(R.drawable.ic_overview_white);
            ivOverview.setColorFilter(normalColor);
            contOverview.setBackgroundColor(selectedColor);
            tvNavOverview.setTextColor(normalColor);
        } else {
            ivOverview.setImageResource(R.drawable.ic_overview_dark_gray);
            ivOverview.setColorFilter(normalTextColor);
            contOverview.setBackgroundColor(normalColor);
            tvNavOverview.setTextColor(normalTextColor);
        }

        if (ivAppointments == ivSelected) {
            ivAppointments.setImageResource(R.drawable.ic_appointment_white);
            ivAppointments.setColorFilter(normalColor);
            contAppontments.setBackgroundColor(selectedColor);
            tvNavAppointments.setTextColor(normalColor);
        } else {
            ivAppointments.setImageResource(R.drawable.ic_appointment_gray);
            ivAppointments.setColorFilter(normalTextColor);
            contAppontments.setBackgroundColor(normalColor);
            tvNavAppointments.setTextColor(normalTextColor);
        }

        if (ivServices == ivSelected) {
            ivServices.setImageResource(R.drawable.ic_services_white);
            ivServices.setColorFilter(normalColor);
            contServices.setBackgroundColor(selectedColor);
            tvNavServices.setTextColor(normalColor);
        } else {
            ivServices.setImageResource(R.drawable.ic_services_gray);
            ivServices.setColorFilter(normalTextColor);
            contServices.setBackgroundColor(normalColor);
            tvNavServices.setTextColor(normalTextColor);
        }

        if (ivReviews == ivSelected) {
            ivReviews.setImageResource(R.drawable.ic_review_white);
            ivReviews.setColorFilter(normalColor);
            contReview.setBackgroundColor(selectedColor);
            tvNavFeedback.setTextColor(normalColor);
        } else {
            ivReviews.setImageResource(R.drawable.ic_review_gray);
            ivReviews.setColorFilter(normalTextColor);
            contReview.setBackgroundColor(normalColor);
            tvNavFeedback.setTextColor(normalTextColor);
        }

    }


    //Web services
    public void loadData() {

        AlertUtils.showProgress(getActivity());
        if (isDoctorLoginId) {
            try {


                WebServicesHandler.instance.getDoctorByLoginId(doctorId, new CustomCallback<RetrofitJSONResponse>() {
                    @Override
                    public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                        AlertUtils.dismissProgress();
                        SharedData.getInstance().setSelectedDoctorLoginId(null);
                        Logs.apiResponse(response.toString());
                        if (!response.status()) {
                            AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                            return;
                        }
                        DoctorProfile doctor = new DoctorProfile(response);
                        setData(doctor);


                    }

                    @Override
                    public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                        SharedData.getInstance().setSelectedDoctorLoginId(null);
                        AlertUtils.dismissProgress();
                        AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
                    }
                });
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }

        } else {
            try {


                WebServicesHandler.instance.getDoctorById(doctorId, new CustomCallback<RetrofitJSONResponse>() {
                    @Override
                    public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                        AlertUtils.dismissProgress();
                        Logs.apiResponse(response.toString());
                        if (!response.status()) {
                            AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                            return;
                        }
                        DoctorProfile doctor = new DoctorProfile(response);
                        setData(doctor);


                    }

                    @Override
                    public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                        AlertUtils.dismissProgress();
                        AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
                    }
                });
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedData.getInstance().setSelectedDoctorId(null);
    }
}
