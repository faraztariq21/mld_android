package com.imedhealthus.imeddoctors.doctor.models;

import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class SubSpeciality {

    @SerializedName("SubSpecialityId")
    private String id;

    private String name;

    public SubSpeciality() {
    }

    public SubSpeciality(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public SubSpeciality(JSONObject educationJson) {

        id = educationJson.optString("SubSpecialityId", "");
        name = educationJson.optString("SubspecialityName", "").replace("null","");
    }

    public static List<SubSpeciality> parseSubSpecialities(JSONArray subSpecialitiesJson){

        ArrayList<SubSpeciality> subSpecialities = new ArrayList<>();

        for (int i = 0; i < subSpecialitiesJson.length(); i++){

            JSONObject subSpecialityJson = subSpecialitiesJson.optJSONObject(i);
            if (subSpecialityJson != null) {
                SubSpeciality subSpeciality = new SubSpeciality(subSpecialityJson);
                subSpecialities.add(subSpeciality);
            }

        }

        return subSpecialities;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
