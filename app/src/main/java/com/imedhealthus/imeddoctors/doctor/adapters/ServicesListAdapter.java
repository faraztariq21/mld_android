package com.imedhealthus.imeddoctors.doctor.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.doctor.listeners.OnServiceItemClickListener;
import com.imedhealthus.imeddoctors.doctor.models.Service;
import com.imedhealthus.imeddoctors.doctor.view_holders.ServiceItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class ServicesListAdapter extends RecyclerView.Adapter<ServiceItemViewHolder> {

    private List<Service> services;
    private WeakReference<TextView> tvNoRecords;
    private boolean isEditAllowed;
    private WeakReference<OnServiceItemClickListener> onServiceItemClickListener;

    public ServicesListAdapter(TextView tvNoRecords, boolean isEditAllowed, OnServiceItemClickListener onServiceItemClickListener) {

        super();

        this.tvNoRecords = new WeakReference<>(tvNoRecords);
        this.isEditAllowed = isEditAllowed;
        this.onServiceItemClickListener = new WeakReference<>(onServiceItemClickListener);

    }

    public void setServices(List<Service> services) {

        this.services = services;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }

    @Override
    public ServiceItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        OnServiceItemClickListener listener = null;
        if (onServiceItemClickListener != null && onServiceItemClickListener.get() != null) {
            listener = onServiceItemClickListener.get();
        }
        View view = inflater.inflate(R.layout.list_item_doctor_service, parent, false);
        return new ServiceItemViewHolder(view, isEditAllowed, listener);

    }

    @Override
    public void onBindViewHolder(ServiceItemViewHolder viewHolder, int position) {
        viewHolder.setData(getItem(position), position);
    }

    @Override
    public int getItemCount() {
        return services == null ? 0 : services.size();
    }

    public Service getItem(int position) {
        if (position < 0 || position >= services.size()) {
            return null;
        }
        return services.get(position);
    }

}
