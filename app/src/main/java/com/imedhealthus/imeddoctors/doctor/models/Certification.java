package com.imedhealthus.imeddoctors.doctor.models;

import android.text.TextUtils;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class Certification extends OverviewItem {

    private String id, certificateNumber, type, fileUrl;
    private String stateId, stateName, countryId, countryName;
    private String issueDate, expiryDate, issueDateFormatted, expiryDateFormatted;

    public Certification() {
    }

    public Certification(JSONObject educationJson) {

        id = educationJson.optString("Id", "");
        certificateNumber = educationJson.optString("RegistrationId", "").replace("null","");
        type = educationJson.optString("RegistrationTitle", "").replace("null","");
        fileUrl = educationJson.optString("DocPath", "").replace("null","");

        issueDate = educationJson.optString("RegistrationDate", "").replace("null","");
        Date date = GenericUtils.getDateFromString(issueDate);
        issueDateFormatted = GenericUtils.formatDate(date);

        expiryDate = educationJson.optString("RegistrationEndDate", "").replace("null","");
        date = GenericUtils.getDateFromString(expiryDate);
        expiryDateFormatted = GenericUtils.formatDate(date);

        stateId = educationJson.optString("StateId", "").replace("null","");
        stateName = educationJson.optString("StateName", "").replace("null","");
        countryId = educationJson.optString("CountryId", "").replace("null","");
        countryName = educationJson.optString("CountryName", "").replace("null","");

    }

    public static List<Certification> parseCertifications(JSONArray certificationsJson){

        ArrayList<Certification> certifications = new ArrayList<>();

        for (int i = 0; i < certificationsJson.length(); i++){

            JSONObject certificationJson = certificationsJson.optJSONObject(i);
            if (certificationJson != null) {
                Certification certification = new Certification(certificationJson);
                certifications.add(certification);
            }

        }

        return certifications;

    }


    @Override
    public String getDisplayTitle() {

        String displayTitle =  type;
        if (!TextUtils.isEmpty(certificateNumber)) {
            displayTitle += " (" + certificateNumber + ")";
        }
        return displayTitle;

    }

    @Override
    public String getDisplayDetail() {

        String displayDetail = stateName;
        if (!TextUtils.isEmpty(countryName)) {
            if (TextUtils.isEmpty(displayDetail)) {
                displayDetail = countryName;
            }else {
                displayDetail += ", " + countryName;
            }
        }
        return displayDetail;

    }

    @Override
    public String getDisplayDuration() {

        String displayDuration = issueDateFormatted;
        if (!TextUtils.isEmpty(expiryDateFormatted)) {
            displayDuration += " - " + expiryDateFormatted;
        }
        return displayDuration;

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCertificateNumber() {
        return certificateNumber;
    }

    public void setCertificateNumber(String certificateNumber) {
        this.certificateNumber = certificateNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getIssueDateFormatted() {
        return issueDateFormatted;
    }

    public void setIssueDateFormatted(String issueDateFormatted) {
        this.issueDateFormatted = issueDateFormatted;
    }

    public String getExpiryDateFormatted() {
        return expiryDateFormatted;
    }

    public void setExpiryDateFormatted(String expiryDateFormatted) {
        this.expiryDateFormatted = expiryDateFormatted;
    }
}
