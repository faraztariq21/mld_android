package com.imedhealthus.imeddoctors.doctor.models;

import android.text.TextUtils;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class Membership extends OverviewItem {

    private String id, designation;
    private String cityId, cityName, stateId, stateName, countryId, countryName;
    private String startDate, endDate, startDateFormatted, endDateFormatted;
    private boolean isCurrentlyMember;

    public Membership() {
    }

    public Membership(JSONObject educationJson) {

        id = educationJson.optString("Id", "");
        designation = educationJson.optString("MembershipId", "").replace("null","");

        startDate = educationJson.optString("MembershipStartDate", "").replace("null","");
        Date date = GenericUtils.getDateFromString(startDate);
        startDateFormatted = GenericUtils.formatDate(date);

        endDate = educationJson.optString("MembershipEndDate", "").replace("null","");
        date = GenericUtils.getDateFromString(endDate);
        endDateFormatted = GenericUtils.formatDate(date);

        cityId = educationJson.optString("CityId", "").replace("null","");
        cityName = educationJson.optString("CityName", "").replace("null","");
        stateId = educationJson.optString("StateId", "").replace("null","");
        stateName = educationJson.optString("StateName", "").replace("null","");
        countryId = educationJson.optString("CountryId", "").replace("null","");
        countryName = educationJson.optString("CountryName", "").replace("null","");

        isCurrentlyMember = educationJson.optBoolean("IscurrentlyMember", false);

    }

    public static List<Membership> parseMemberships(JSONArray membershipsJson){

        ArrayList<Membership> memberships = new ArrayList<>();

        for (int i = 0; i < membershipsJson.length(); i++){

            JSONObject membershipJson = membershipsJson.optJSONObject(i);
            if (membershipJson != null) {
                Membership membership = new Membership(membershipJson);
                memberships.add(membership);
            }

        }

        return memberships;

    }


    @Override
    public String getDisplayTitle() {

        String displayTitle =  designation;
        return displayTitle;

    }

    @Override
    public String getDisplayDetail() {

        String displayDetail = cityName;
        if (!TextUtils.isEmpty(stateName)) {
            if (TextUtils.isEmpty(displayDetail)) {
                displayDetail = stateName;
            }else {
                displayDetail += ", " + stateName;
            }
        }
        if (!TextUtils.isEmpty(countryName)) {
            if (TextUtils.isEmpty(displayDetail)) {
                displayDetail = countryName;
            }else {
                displayDetail += ", " + countryName;
            }
        }
        return displayDetail;

    }

    @Override
    public String getDisplayDuration() {

        String displayDuration = startDateFormatted;
        if (!TextUtils.isEmpty(endDateFormatted)) {
            displayDuration += " - " + endDateFormatted;
        }
        return displayDuration;

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDateFormatted() {
        return startDateFormatted;
    }

    public void setStartDateFormatted(String startDateFormatted) {
        this.startDateFormatted = startDateFormatted;
    }

    public String getEndDateFormatted() {
        return endDateFormatted;
    }

    public void setEndDateFormatted(String endDateFormatted) {
        this.endDateFormatted = endDateFormatted;
    }

    public boolean isCurrentlyMember() {
        return isCurrentlyMember;
    }

    public void setCurrentlyMember(boolean currentlyMember) {
        isCurrentlyMember = currentlyMember;
    }
}
