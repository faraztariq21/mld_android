package com.imedhealthus.imeddoctors.doctor.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.doctor.models.Feedback;
import com.imedhealthus.imeddoctors.doctor.view_holders.FeedbackItemViewHolder;

import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class FeedbackListAdapter extends RecyclerView.Adapter<FeedbackItemViewHolder> {

    private List<Feedback> feedbacks;

    public FeedbackListAdapter(List<Feedback> feedbacks) {
        this.feedbacks = feedbacks;
    }

    public void setFeedbacks(List<Feedback> feedbacks) {

        this.feedbacks = feedbacks;
        notifyDataSetChanged();

    }

    @Override
    public FeedbackItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_doctor_feedback, parent, false);
        return new FeedbackItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(FeedbackItemViewHolder viewHolder, int position) {
        viewHolder.setData(getItem(position));
    }

    @Override
    public int getItemCount() {
        return feedbacks == null ? 0 : feedbacks.size();
    }

    public Feedback getItem(int position) {
        if (position < 0 || position >= feedbacks.size()) {
            return null;
        }
        return feedbacks.get(position);
    }

}
