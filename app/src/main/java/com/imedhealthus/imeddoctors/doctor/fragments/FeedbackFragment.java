package com.imedhealthus.imeddoctors.doctor.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.auth.activities.LoginActivity;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.doctor.adapters.FeedbackListAdapter;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.imedhealthus.imeddoctors.doctor.models.Feedback;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/6/18.
 */

public class FeedbackFragment extends BaseFragment {

    @BindView(R.id.tv_comments_count)
    TextView tvCommentsCount;
    @BindView(R.id.tv_like_percent)
    TextView tvLikePercent;
    @BindView(R.id.tv_dislike_percent)
    TextView tvDislikePercent;
    @BindView(R.id.tv_overall_rating)
    TextView tvOverAllRating;

    @BindView(R.id.rv_feedback)
    RecyclerView rvFeedback;
    @BindView(R.id.btn_expand)
    ViewGroup btnExpand;
    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;

    @BindView(R.id.cont_user_feedback)
    ViewGroup contUserFeedback;


    private FeedbackListAdapter adapterFeedback;

    private DoctorProfile profile;
    private boolean isListExpanded, isUserFeedbackAllowed ;

    public static FeedbackFragment create(DoctorProfile profile, boolean isUserFeedbackAllowed) {

        FeedbackFragment fragment = new FeedbackFragment();

        fragment.profile = profile;
        fragment.isUserFeedbackAllowed = isUserFeedbackAllowed;

        return fragment;

    }

    @Override
    public String getName() {
        return "FeedbackFragment";
    }

    @Override
    public String getTitle() {
        return "Feedback";
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_doctor_feedback, container, false);
        ButterKnife.bind(this, rootView);

        initHelper();
        return rootView;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SharedData.getInstance().isRefreshRequired()) {
            setData();
        }
    }

    private void initHelper(){

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rvFeedback.setLayoutManager(layoutManager);

        adapterFeedback = new FeedbackListAdapter(profile.getFeedbacks());
        rvFeedback.setAdapter(adapterFeedback);

        if (isUserFeedbackAllowed) {
            contUserFeedback.setVisibility(View.VISIBLE);
        }else {
            contUserFeedback.setVisibility(View.GONE);
        }
        expandList(true);

    }

    private void setData() {

        if (profile == null) {
            return;
        }

        tvCommentsCount.setText(profile.getFeedbacks().size() + " Comments");
        tvLikePercent.setText(profile.getFeedbackLikePercent() + "%");
        tvDislikePercent.setText(profile.getFeedbackDislikePercent() + "%");
        tvOverAllRating.setText(String.format("%.1f", profile.getFeedbackOverallRating()));

        adapterFeedback.setFeedbacks(profile.getFeedbacks());

        if (profile.getFeedbacks().size() == 0) {
            rvFeedback.setVisibility(View.GONE);
            btnExpand.setVisibility(View.GONE);
            tvNoRecords.setVisibility(View.VISIBLE);
        }else {
            rvFeedback.setVisibility(View.VISIBLE);
            btnExpand.setVisibility(View.VISIBLE);
            tvNoRecords.setVisibility(View.GONE);
        }

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_expand:
                expandList(!isListExpanded);
                break;

            case R.id.btn_add_feedback:
                openSubmitFeedback();
                break;

        }
    }

    private void expandList(boolean expand) {

        isListExpanded = expand;

        ViewGroup.LayoutParams params = rvFeedback.getLayoutParams();
        params.height = getListHeight(expand ? adapterFeedback.getItemCount() : 1);
        rvFeedback.setLayoutParams(params);
        rvFeedback.requestLayout();

        btnExpand.setRotation(expand ? 180 : 0);
        btnExpand.requestLayout();

    }

    private void openSubmitFeedback() {

        SharedData.getInstance().setCallFeedback(false);
        SharedData.getInstance().setCallData(null);
        SharedData.getInstance().setRefreshViaServerHit(true);

        Intent intent;
        if(!CacheManager.getInstance().isUserLoggedIn()) {
            intent = new Intent(context, LoginActivity.class);
        }else{
            intent = new Intent(context, SimpleFragmentActivity.class);
            intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.SubmitDoctorFeedback.ordinal());
        }
        ((BaseActivity)context).startActivity(intent, true);

    }


    //Other
    private int getListHeight(int upToIdx) {

        int totalHeight = 0;
        int verticalHeightConstant = (int)GenericUtils.convertDpToPixels(context, 92);

        int tvCommentMaxWidth = (int) (GenericUtils.getScreenWidth((Activity) context) - GenericUtils.convertDpToPixels(context, 132));
        int tvCommentTextSize = GenericUtils.convertSpToPixels(context, 20);
        Typeface tvCommentTypeFace = tvCommentsCount.getTypeface();
        int tvCommentMaxLines = 3;

        for (int i = 0; i < adapterFeedback.getItemCount() && i < upToIdx; i++) {

            Feedback feedback = adapterFeedback.getItem(i);
            int rowHeight = GenericUtils.getTextHeight(feedback.getComment(), tvCommentMaxWidth, tvCommentTextSize, tvCommentTypeFace, tvCommentMaxLines);
            totalHeight += rowHeight + verticalHeightConstant;

        }

        return totalHeight;

    }

}
