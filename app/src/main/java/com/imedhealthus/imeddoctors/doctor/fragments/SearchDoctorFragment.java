package com.imedhealthus.imeddoctors.doctor.fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.listeners.FragmentChangeHandler;
import com.imedhealthus.imeddoctors.common.listeners.OnNestedListItemClickListener;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.widgets.CustomArrayAdapterFilter;
import com.imedhealthus.imeddoctors.doctor.adapters.DoctorListAdapter;
import com.imedhealthus.imeddoctors.doctor.listeners.OnDoctorItemClickListener;
import com.imedhealthus.imeddoctors.doctor.models.Appointment;
import com.imedhealthus.imeddoctors.doctor.models.Doctor;
import com.imedhealthus.imeddoctors.doctor.models.Filter;
import com.imedhealthus.imeddoctors.doctor.models.StaticData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/6/18.
 */

public class SearchDoctorFragment extends BaseFragment implements OnDoctorItemClickListener, TextWatcher, OnNestedListItemClickListener {

    @BindView(R.id.et_search)
    EditText etSearch;

    @BindView(R.id.sp_speciality)
    Spinner spSpeciality;

    @BindView(R.id.rv_doctors)
    RecyclerView rvDoctors;
    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;

    // This variable is a counter for how many doctors appointment slots have been loaded
    int numberOfDoctorsAppointmentsLoaded = 0;


    private DoctorListAdapter adapterDoctors;
    private Filter filter;
    private List<Doctor> recommendedDoctors = new ArrayList<>();
    private List<Doctor> activeDoctors = new ArrayList<>();
    private List<Doctor> doctors = new ArrayList<>();
    List<Doctor> filteredDoctors = new ArrayList<>();

    boolean isFirstLoad = false;

    @Override
    public String getName() {
        return "SearchDoctorFragment";
    }

    @Override
    public String getTitle() {
        return "Search Results";
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_search_doctor, container, false);
        ButterKnife.bind(this, rootView);
        initializeShimmer(rootView);

        initHelper();
        return rootView;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    private void initHelper() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rvDoctors.setLayoutManager(layoutManager);

        adapterDoctors = new DoctorListAdapter(tvNoRecords, false, this, this);
        rvDoctors.setAdapter(adapterDoctors);

        etSearch.addTextChangedListener(this);
        filter = SharedData.getInstance().getSelectedFilter();
        if (filter == null)
            filter = new Filter();

        List<StaticData> listSpeciality = SharedData.getInstance().getSpecialityList();

        CustomArrayAdapterFilter dataAdapter = new CustomArrayAdapterFilter(context, R.layout.search_doctor_spinner_item, listSpeciality);
        spSpeciality.setAdapter(dataAdapter);


        spSpeciality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("Value", "" + i);


                if (spSpeciality.getAdapter() != null) {
                    ((TextView) (spSpeciality.getSelectedView())).setTextColor(getResources().getColor(R.color.white));
                    ((TextView) (spSpeciality.getSelectedView())).setSingleLine();

                    if (!isFirstLoad) {
                        isFirstLoad = true;
                        return;
                    }
                    if (spSpeciality.getAdapter().getCount() != 0) {

                        filter.setSpeciality(((StaticData) spSpeciality.getSelectedItem()).getId().replace("-1", ""));
                        loadData();
                    } else
                        filter.setSpeciality("");
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_filters:
                openFilters();
                break;


        }
    }

    private void openFilters() {

        if (context instanceof FragmentChangeHandler) {
            ((FragmentChangeHandler) context).showOverlayFragment(new FiltersFragment(), filter);
        }
    }

    private void toggleDoctorsList(boolean showRecommended) {


        if (showRecommended) {
            doctors = recommendedDoctors;
        } else {
            doctors = activeDoctors;
        }
        filterDoctors();

    }


    //Filter response handling
    @Override
    public void onDismissOverlayFragment(Object data) {
        filter = (Filter) data;
        loadData();
    }


    //DoctorProfile item click
    @Override
    public void onDoctorItemInfoClick(int idx) {

        Doctor doctor = adapterDoctors.getItem(idx);
        if (doctor == null) {
            return;
        }
        SharedData.getInstance().setSelectedDoctor(doctor);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.DoctorProfile.ordinal());
        ((BaseActivity) context).startActivity(intent, true);

    }

    @Override
    public void onDoctorItemMessageClick(int idx) {

    }

    @Override
    public void onDoctorItemCallClick(int idx) {

    }

    @Override
    public void onDoctorItemDocProfileClick(int idx) {

    }

    @Override
    public void onDoctorItemAppointmentClick(int idx) {

    }


    //Search filtering
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        filterDoctors();
    }

    private void filterDoctors() {

        String searchTxt = etSearch.getText().toString().toLowerCase();
        filteredDoctors = new ArrayList<>();

        for (Doctor doctor : doctors) {
            if (doctor.getFullName().toLowerCase().contains(searchTxt) ||
                    doctor.getSpeciality().toLowerCase().contains(searchTxt)) {
                filteredDoctors.add(doctor);
            }
        }
        adapterDoctors = new DoctorListAdapter(tvNoRecords, false, this, this);
        rvDoctors.setAdapter(adapterDoctors);
        adapterDoctors.setDoctors(filteredDoctors);

    }


    //Webservices
    private void loadData() {

        startShimmerAnimation();
        WebServicesHandler.instance.findDoctors(filter, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {


                //AlertUtils.dismissProgress();

                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                } else {

                    activeDoctors = Doctor.parseAvailableDoctors(response);

                    recommendedDoctors = Doctor.parseRecommendedDoctors(response);

                    recommendedDoctors.addAll(activeDoctors);

                }

                new LoadDoctorsAppointments().execute(recommendedDoctors);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

                //AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);

            }

        });

    }

    @Override
    public void onNestedListItemClicked(int parentIndex, int childIndex) {

        bookAppointment(filteredDoctors.get(parentIndex).getAppointmentSlots().get(childIndex));
    }


    public class LoadDoctorsAppointments extends AsyncTask<List<Doctor>, List<Doctor>, List<Doctor>> {

        @Override
        protected List<Doctor> doInBackground(final List<Doctor>... lists) {

            for (final Doctor doctor : lists[0]) {
                WebServicesHandler.instance.getDoctorsAvaiableAppointmentSlots(doctor.getUserId(), new CustomCallback<RetrofitJSONResponse>() {
                    @Override
                    public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                        //  AlertUtils.dismissProgress();

                        if (!response.status()) {
                            AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                            return;
                        }

                        List<Appointment> appointments = Appointment.parseAppointments(response.getJSONArray("Appointments"));


                        Date currentDate = new Date();
                        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd/MM/yyyy");
                        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");

                        for (Appointment appointment : appointments) {
                            appointment.adjustData(dateFormat, timeFormat, currentDate);
                        }

                        ArrayList<Appointment> finalAppointmentSlots = new ArrayList<>();
                        for (Appointment appointment : appointments) {
                            if (!appointment.hasPassed())


                                finalAppointmentSlots.add(appointment);
                        }


                        doctor.setAppointmentSlots(finalAppointmentSlots);
                        numberOfDoctorsAppointmentsLoaded++;
                        publishProgress(lists);
                    }

                    @Override
                    public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                        numberOfDoctorsAppointmentsLoaded++;
                        publishProgress(lists);
                    }
                });
            }
            publishProgress(lists);

            return lists[0];
        }

        @Override
        protected void onProgressUpdate(List<Doctor>... values) {
            super.onProgressUpdate(values);
            /*recommendedDoctors = values[0];
            toggleDoctorsList(true);
*/


            if (numberOfDoctorsAppointmentsLoaded == recommendedDoctors.size()) {
                recommendedDoctors = values[0];
                Collections.sort(recommendedDoctors, new Comparator<Doctor>() {
                    @Override
                    public int compare(Doctor o1, Doctor o2) {
                        return (o2.getAppointmentSlots() != null ? o2.getAppointmentSlots().size() : 0) - (o1.getAppointmentSlots() != null ? o1.getAppointmentSlots().size() : 0);
                    }
                });


                toggleDoctorsList(true);
                stopShimmerAnimation();
            }

        }

        @Override
        protected void onPostExecute(List<Doctor> doctors) {
            super.onPostExecute(doctors);

        }
    }


    @Override
    public void onPause() {
        super.onPause();
        //stopShimmerAnimation();
    }

    public void bookAppointment(Appointment appointment) {

        if (appointment.hasPassed()) {
            AlertUtils.showAlert(context, Constants.ErrorAlertTitle, "This appointment is expired.");
            return;
        }


        SharedData.getInstance().setSelectedAppointment(appointment);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.BookAppointment.ordinal());
        ((BaseActivity) context).startActivity(intent, true);
    }

}
