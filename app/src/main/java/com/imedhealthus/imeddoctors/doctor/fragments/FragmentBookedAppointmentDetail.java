package com.imedhealthus.imeddoctors.doctor.fragments;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.fragments.BaseDialogFragment;
import com.imedhealthus.imeddoctors.common.fragments.FragmentAttachMultipleDocuments;
import com.imedhealthus.imeddoctors.common.fragments.FragmentAttachMultipleImages;
import com.imedhealthus.imeddoctors.common.models.BookedAppointment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.ListItemImage;
import com.imedhealthus.imeddoctors.common.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentBookedAppointmentDetail extends BaseDialogFragment {
    @BindView(R.id.btn_close)
    Button bClose;

    @BindView(R.id.tv_appointment_title)
    TextView tvAppointmentTitle;
    @BindView(R.id.tv_appointment_doctor_name)
    TextView tvAppointmentDoctorName;
    @BindView(R.id.tv_mr_no)
    TextView tvMrNo;
    @BindView(R.id.tv_location)
    TextView tvLocation;
    @BindView(R.id.tv_location_heading)
    TextView tvLocationHeading;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_description)
    TextView tvDescription;
    @BindView(R.id.tv_mr_no_heading)
    TextView tvMrNoHeading;
    @BindView(R.id.ll_location)
    LinearLayout llLocation;

    BookedAppointment appointment;
    public static final String BOOKED_APPOINTMENT = "booked_appointment";

    public FragmentBookedAppointmentDetail() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public static FragmentBookedAppointmentDetail newInstance(BookedAppointment bookedAppointment) {
        FragmentBookedAppointmentDetail fragmentBookedAppointmentDetail = new FragmentBookedAppointmentDetail();
        Bundle bundle = new Bundle();
        bundle.putSerializable(BOOKED_APPOINTMENT, bookedAppointment);
        fragmentBookedAppointmentDetail.setArguments(bundle);
        return fragmentBookedAppointmentDetail;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            appointment = (BookedAppointment) getArguments().getSerializable(BOOKED_APPOINTMENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_booked_appointment_detail, container, false);

        ButterKnife.bind(this, view);

        if (appointment != null) {
            tvAppointmentTitle.setText(appointment.getType().toString(ApplicationManager.getContext()));
            tvDescription.setText(appointment.getReason());
            tvLocation.setText(appointment.getPhysicalLocation());
            tvTime.setText(appointment.getFormattedTime());

            if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Patient) {
                tvMrNo.setVisibility(View.GONE);
                tvMrNoHeading.setVisibility(View.GONE);
                tvAppointmentDoctorName.setText(appointment.getDoctorFullName());
                /*btnCancel.setVisibility(View.GONE);*/
            } else {
                tvMrNo.setVisibility(View.VISIBLE);
                tvAppointmentDoctorName.setText(appointment.getPatientFullName());
                tvMrNo.setText(appointment.getMrNumber());
            }

            if (appointment.getType() == Constants.AppointmentType.Tele) {
                tvLocation.setVisibility(View.GONE);
                tvLocationHeading.setVisibility(View.GONE);
                llLocation.setVisibility(View.GONE);


            }

            getChildFragmentManager().beginTransaction().replace(R.id.fl_images_container, FragmentAttachMultipleImages.newInstance(false, appointment.getAttachmentsImages() != null ? appointment.getAttachmentsImages() : new ArrayList<ListItemImage>())).commit();
            getChildFragmentManager().beginTransaction().replace(R.id.fl_documents_container, FragmentAttachMultipleDocuments.newInstance(false, appointment.getAttachmentsDocuments() != null ? appointment.getAttachmentsDocuments() : new ArrayList<ListItemImage>())).commit();

        }


        bClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentBookedAppointmentDetail.this.dismiss();
            }
        });
        return view;
    }

}
