package com.imedhealthus.imeddoctors.doctor.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.listeners.FragmentChangeHandler;
import com.imedhealthus.imeddoctors.common.listeners.PopUpWindowListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.ChatMessage;
import com.imedhealthus.imeddoctors.common.models.ChatWrapper;
import com.imedhealthus.imeddoctors.common.models.ForumComment;
import com.imedhealthus.imeddoctors.common.models.ForumCommentWrapper;
import com.imedhealthus.imeddoctors.common.models.LiveNotifications;
import com.imedhealthus.imeddoctors.common.models.Notification;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.User;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.NetworkResponseParser;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.Logs;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.nekocode.badge.BadgeDrawable;

/**
 * Created by umair on 1/5/18.
 */

public class DoctorHomeFragment extends BaseFragment {

    PopUpWindowListener popUpWindowListener;

    @BindView(R.id.iv_profile)
    ImageView ivProfile;
    @BindView(R.id.iv_forum_comments_badge)
    ImageView ivForumCommentsBadge;
    @BindView(R.id.iv_appointments_badge)
    ImageView ivAppointmentsBadge;
    @BindView(R.id.iv_questions_badge)
    ImageView ivQuestionsBadge;
    @BindView(R.id.iv_conversations_badge)
    ImageView ivConversationsBadge;

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.ll_name_email)
    LinearLayout llNameEmail;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PopUpWindowListener)
            popUpWindowListener = (PopUpWindowListener) context;
    }

    @Override
    public boolean showHeader() {
        return true;
    }

    @Override
    public String getName() {
        return "DoctorHomeFragment";
    }

    @Override
    public String getTitle() {
        return "Home";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_doctor_home, container, false);
        ButterKnife.bind(this, rootView);

        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open(Constants.Fragment.DoctorProfile);
            }
        });

        llNameEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open(Constants.Fragment.DoctorProfile);

            }
        });

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
    }

    private void setData() {

        User user = CacheManager.getInstance().getCurrentUser();

        tvName.setText(user.getFullName());
        tvEmail.setText(user.getEmail());
        Glide.with(ApplicationManager.getContext()).load(user.getImageUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivProfile);

    }

    public void applyConversationBadge() {
        int unSeenConversations = SharedData.getInstance().getUnSeenConversations();
        if (unSeenConversations > 0) {
            applyBadge(ivConversationsBadge, unSeenConversations);
        } else {
            ivConversationsBadge.setVisibility(View.GONE);
        }
    }

    public void applyNotificationBadges() {
        int unSeenAppointments = SharedData.getInstance().getUnSeenAppointments();
        int unSeenForumComments = SharedData.getInstance().getUnSeenForumComments();
        int unSeenQuestions = SharedData.getInstance().getUnSeenQuestions();
        int unSeenQuestionComments = SharedData.getInstance().getUnSeenQuestionComments();

        if (unSeenForumComments > 0) {
            applyBadge(ivForumCommentsBadge, unSeenForumComments);
        } else {
            ivForumCommentsBadge.setVisibility(View.GONE);
        }

        /*if (unSeenAppointments > 0) {
            applyBadge(ivAppointmentsBadge, unSeenAppointments);
        } else {
            ivAppointmentsBadge.setVisibility(View.GONE);
        }
*/
        if (unSeenQuestions + unSeenQuestionComments > 0) {
            applyBadge(ivQuestionsBadge, unSeenQuestions + unSeenQuestionComments);
        } else {
            ivQuestionsBadge.setVisibility(View.GONE);
        }

    }

    public void applyBadge(ImageView view, int count) {
        final BadgeDrawable drawable =
                new BadgeDrawable.Builder()
                        .type(BadgeDrawable.TYPE_NUMBER)
                        .number(count)
                        .build();
        view.setVisibility(View.VISIBLE);
        view.setImageDrawable(drawable);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cont_public_forum:
                open(Constants.Fragment.Forum);
                break;

            case R.id.cont_patients_list:
                open(Constants.Fragment.PatientsList);
                break;

            case R.id.cont_appointment:
                /*if (popUpWindowListener != null)
                    popUpWindowListener.displayPopUpWindow(view, null);*/
                open(Constants.Fragment.Appointments);
                break;

            case R.id.cont_questions:
              /*  if (popUpWindowListener != null)
                    popUpWindowListener.dismissPopUpWindow(view);*/

                open(Constants.Fragment.Questions);
                break;

            case R.id.cont_create_appointment_slots:
                SharedData.getInstance().setSelectedDoctorProfileTab(DoctorProfileFragment.DoctorProfileTabs.APPOINTMENTS_TAB);
                open(Constants.Fragment.DoctorProfile);
                break;

            case R.id.cont_chat:
                open(Constants.Fragment.Inbox);
                break;

        }
    }


    private void open(Constants.Fragment item) {
        if (context instanceof FragmentChangeHandler) {
            ((FragmentChangeHandler) context).changeFragment(item, null);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getNotificationsData();
        getReadCount();
        loadDoctorProfile();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, new IntentFilter(Constants.FirebaseMessage));

    }


    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    public void getNotificationsData() {
        User user = CacheManager.getInstance().getCurrentUser();
        WebServicesHandler.instance.getNotifications(user, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                if (response.status()) {
                    List<Notification> notifications = NetworkResponseParser.getNotifications(response.getJSONArray("GetNotification"));
                    applyNotificationBadges();
                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

            }
        });

    }

    public void getReadCount() {
        User user = CacheManager.getInstance().getCurrentUser();
        WebServicesHandler.instance.getReadCount(user.getLoginId(), new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                if (response.status()) {
                    int count = response.optInt("TotalCount");
                    SharedData.getInstance().setUnSeenConversations(count);
                    applyConversationBadge();
                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                getReadCount();
            }
        });

    }


    public void loadDoctorProfile() {

        //AlertUtils.showProgress(getActivity());

        String doctorId;
        if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor)
            doctorId = CacheManager.getInstance().getCurrentUser().getUserId();
        else
            doctorId = SharedData.getInstance().getSelectedDoctor().getUserId();

        try {
            WebServicesHandler.instance.getDoctorById(doctorId, new CustomCallback<RetrofitJSONResponse>() {
                @Override
                public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                    //AlertUtils.dismissProgress();
                    Logs.apiResponse(response.toString());
                    if (!response.status()) {
                        AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                        return;
                    }
                    DoctorProfile doctor = new DoctorProfile(response);
                    SharedData.getInstance().setSelectedDoctorProfile(doctor);

                }

                @Override
                public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
              /*      AlertUtils.dismissProgress();
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
              */
                }
            });
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onMessageReceived(ChatMessage chatMessage) {
        super.onMessageReceived(chatMessage);
        getNotificationsData();
        getReadCount();
    }

    @Override
    public void onMessageReceived(ChatWrapper chatWrapper) {
        super.onMessageReceived(chatWrapper);
        getNotificationsData();
        getReadCount();
    }

    @Override
    public void onNotificationReceived(LiveNotifications liveNotifications) {
        super.onNotificationReceived(liveNotifications);
        getNotificationsData();
        getReadCount();
    }

    @Override
    public void onCommentReceived(ForumComment forumComment) {
        super.onCommentReceived(forumComment);
    }


    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getNotificationsData();
            getReadCount();
        }
    };


}
