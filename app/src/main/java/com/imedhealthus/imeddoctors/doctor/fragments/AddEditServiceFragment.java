package com.imedhealthus.imeddoctors.doctor.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.imedhealthus.imeddoctors.doctor.models.Service;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddEditServiceFragment extends BaseFragment {

    @BindView(R.id.et_title)
    EditText etTitle;
    @BindView(R.id.et_description)
    EditText etDescription;
    @BindView(R.id.et_price)
    EditText etPrice;

    private Service service;

    public AddEditServiceFragment() {
        service = SharedData.getInstance().getSelectedService();
    }


    @Override
    public String getName() {
        return "AddEditServiceFragment";
    }

    @Override
    public String getTitle() {
        if (service == null) {
            return "Add Service";
        }else {
            return "Edit Service";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_service, container, false);
        ButterKnife.bind(this, view);

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    private void setData() {

        if (service == null) {
            return;
        }

        etTitle.setText(service.getTitle());
        etDescription.setText(service.getDetails());
        etPrice.setText(String.valueOf(service.getPrice()));

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditService();
                }
                break;

        }
    }


    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etTitle.getText().toString().isEmpty() || etDescription.getText().toString().isEmpty() ||
                etPrice.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustServices(RetrofitJSONResponse response, Service updatedService) {

        if (service == null) {//Add

            JSONObject serviceJson = response.optJSONObject("DoctorService");
            if (serviceJson != null) {
                Service newService = new Service(serviceJson);
                DoctorProfile profile = SharedData.getInstance().getSelectedDoctorProfile();
                profile.getServices().add(newService);
            }

        }else  {

            service.setTitle(updatedService.getTitle());
            service.setDetails(updatedService.getDetails());
            service.setPrice(updatedService.getPrice());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditService() {

        AlertUtils.showProgress(getActivity());

        final Service updatedService = new Service();
        if (service != null) {
            updatedService.setId(service.getId());
        }

        updatedService.setTitle(etTitle.getText().toString());
        updatedService.setDetails(etDescription.getText().toString());
        double price = 0;
        try {
            price = Double.parseDouble(etPrice.getText().toString());
        }catch (Exception ex) {}
        updatedService.setPrice(price);

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditService(userId, updatedService, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                adjustServices(response, updatedService);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Service details saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
