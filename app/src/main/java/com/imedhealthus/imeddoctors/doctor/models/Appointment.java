package com.imedhealthus.imeddoctors.doctor.models;

import com.imedhealthus.imeddoctors.common.models.Attachment;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by umair on 1/6/18.
 */

public class Appointment implements Serializable {

    protected int appointmentId;
    protected long startTimeStamp, endTimeStamp;
    protected Constants.AppointmentType type;
    protected String physicalLocation;

    public Date startDate, endDate;
    protected String formattedTime, formattedDate, notes;
    public boolean hasPassed, isToday = false, isWithInTenDays = false, isAfterTenDays = false;
    private String formattedStartTime;

    ArrayList<Attachment> attachmentsImages;
    ArrayList<Attachment> attachmentsDocuments;

    public ArrayList<Attachment> getAttachmentsImages() {
        return attachmentsImages;
    }

    public void setAttachmentsImages(ArrayList<Attachment> attachmentsImages) {
        this.attachmentsImages = attachmentsImages;
    }

    public ArrayList<Attachment> getAttachmentsDocuments() {
        return attachmentsDocuments;
    }

    public void setAttachmentsDocuments(ArrayList<Attachment> attachmentsDocuments) {
        this.attachmentsDocuments = attachmentsDocuments;
    }

    public Appointment() {
    }


    public Appointment(JSONObject jsonObject) {

        appointmentId = jsonObject.optInt("Id", 0);

        startTimeStamp = GenericUtils.getTimeDateInLong(jsonObject.optString("StartTimeStamp", ""));
        endTimeStamp = GenericUtils.getTimeDateInLong(jsonObject.optString("EndTimeStamp", ""));

        type = ((jsonObject.optString("Type", "").equalsIgnoreCase("Tele")) ? Constants.AppointmentType.Tele : Constants.AppointmentType.OFFICE);
        physicalLocation = jsonObject.optString("PhysicalLocation", "");
        notes = jsonObject.optString("Notes", "");

    }


    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }


    public Appointment(JSONObject jsonObject, int index) {

        appointmentId = jsonObject.optInt("Id", 0);

        startTimeStamp = GenericUtils.getTimeDateInLong(jsonObject.optString("StartTimeStamp", ""));

        endTimeStamp = GenericUtils.getTimeDateInLong(jsonObject.optString("EndTimeStamp", ""));

        type = ((jsonObject.optString("Type", "").equalsIgnoreCase("Tele")) ? Constants.AppointmentType.Tele : Constants.AppointmentType.OFFICE);
        physicalLocation = jsonObject.optString("PhysicalLocation", "");
        notes = jsonObject.optString("Notes", "");

    }


    public boolean isOverlaped(long endTime, long startTime) {

        if ((this.endTimeStamp < endTime && this.startTimeStamp > endTime)
                || (this.endTimeStamp > startTime && this.startTimeStamp < startTime))
            return true;
        return false;
    }

    public static List<Appointment> parseAddNewAppointmentsResponse(JSONArray appointmentsJson) {

        List<Appointment> appointments = new ArrayList<>();
        for (int i = 0; i < appointmentsJson.length(); i++) {

            JSONObject appointmentJson = appointmentsJson.optJSONObject(i);
            if (appointmentJson != null) {
                Appointment appointment = new Appointment(appointmentJson, 0);
                appointments.add(appointment);
            }

        }

        return appointments;

    }

    public static List<Appointment> parseAppointments(JSONArray appointmentsJson) {

        List<Appointment> appointments = new ArrayList<>();
        for (int i = 0; i < appointmentsJson.length(); i++) {

            JSONObject appointmentJson = appointmentsJson.optJSONObject(i);
            if (appointmentJson != null) {
                Appointment appointment = new Appointment(appointmentJson);
                appointments.add(appointment);
            }

        }

        return appointments;

    }


    public void adjustData(SimpleDateFormat dateFormat, SimpleDateFormat timeFormat, Date currentDate) {
        dateFormat = new SimpleDateFormat("EEEE, dd/MM/yyyy");

        startDate = new Date(startTimeStamp);
        endDate = new Date(endTimeStamp);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);

        formattedDate = dateFormat.format(startDate) /*+ GenericUtils.getDayOfMonthSuffix(calendar.get(Calendar.DAY_OF_MONTH))*/;
        formattedTime = timeFormat.format(startDate) + " - " + timeFormat.format(endDate);
        try {
            formattedStartTime = formattedTime.split("-")[0];
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            formattedStartTime = formattedTime;
        }
        hasPassed = currentDate.compareTo(startDate) > 0;
        if (!hasPassed)
            isToday = currentDate.compareTo(startDate) == 0;
        if (!isToday)
            isWithInTenDays = TimeUnit.DAYS.convert(startDate.getTime() - currentDate.getTime(), TimeUnit.MILLISECONDS) <= 10;
        if (!isWithInTenDays)
            isAfterTenDays = true;
    }


    public String getFormattedStartTime() {
        return formattedStartTime;
    }

    public boolean isSameMonth(int year, int month) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);

        if (year == calendar.get(Calendar.YEAR) && month == calendar.get(Calendar.MONTH)) {
            return true;
        }
        return false;

    }

    public boolean isSameDay(int year, int month, int day) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);

        if (year == calendar.get(Calendar.YEAR) && month == calendar.get(Calendar.MONTH) && day == calendar.get(Calendar.DAY_OF_MONTH)) {
            return true;
        }
        return false;

    }

    public int getAppointmentId() {
        return appointmentId;
    }

    public long getStartTimeStamp() {
        return startTimeStamp;
    }

    public long getEndTimeStamp() {
        return endTimeStamp;
    }

    public Constants.AppointmentType getType() {
        return type;
    }

    public String getPhysicalLocation() {
        return physicalLocation;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getFormattedTime() {
        return formattedTime;
    }

    public String getFormattedDate() {
        return formattedDate;
    }

    public boolean hasPassed() {


        return hasPassed;
    }


    public boolean checkPastAvailable() {


        return startTimeStamp < new Date().getTime();
    }

    public boolean isItTime() {
        boolean timeToCall = false;
        long currentTime = new Date().getTime();
        if ((startTimeStamp < currentTime || startTimeStamp == currentTime) && (endTimeStamp > currentTime || endTimeStamp == currentTime))
            timeToCall = true;

        return timeToCall;
    }
}
