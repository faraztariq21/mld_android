package com.imedhealthus.imeddoctors.doctor.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RatingBar;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.auth.activities.LoginActivity;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.activities.DoctorDashboardActivity;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.imedhealthus.imeddoctors.doctor.models.Feedback;
import com.imedhealthus.imeddoctors.patient.activities.PatientDashboardActivity;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubmitDoctorsFeedbackFragment extends BaseFragment {

    @BindView(R.id.rb_promptness)
    RatingBar rbPromptness;
    @BindView(R.id.rb_accurate_diaganosis)
    RatingBar rbAccurateDiaganosis;
    @BindView(R.id.rb_bedsides_manner)
    RatingBar rbBedsidesManner;
    @BindView(R.id.rb_courteous_staff)
    RatingBar rbCourteousStaff;
    @BindView(R.id.rb_follow_up)
    RatingBar rbFollowUp;
    @BindView(R.id.rb_medical_knowledge)
    RatingBar rbMedicalKnowledge;
    @BindView(R.id.rb_spending_time_with_me)
    RatingBar rbSpeningTime;
    @BindView(R.id.rb_ease_of_appointment)
    RatingBar rbEaseOfRating;

    @BindView(R.id.et_wait_time)
    EditText etWaitTime;
    @BindView(R.id.et_reason)
    EditText etReason;
    @BindView(R.id.et_description)
    EditText etDescription;

    @BindView(R.id.rd_yes)
    RadioButton rdRecommendYes;

    @Override
    public String getName() {
        return "DoctorFeedback";
    }

    @Override
    public String getTitle() {
        return "Your Feedback";
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_submit_doctors_feedback, container, false);
        ButterKnife.bind(this, view);

        return view;
    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_submit:
              //  if(validateFields())
                 submitFeedback();
                break;

        }
    }

    private boolean validateFields() {

        String errorMsg = null;
        if (etWaitTime.getText().toString().isEmpty() || etReason.getText().toString().isEmpty() || etDescription.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(context, Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }


    //Other
    private void adjustFeedbacks(RetrofitJSONResponse response) {

        DoctorProfile profile = SharedData.getInstance().getSelectedDoctorProfile();
        if (profile == null) {
            return;
        }

        JSONObject feedbackJson = response.optJSONObject("Feedback");
        if (feedbackJson == null) {
            return;
        }
        Feedback feedback = new Feedback(feedbackJson);
        profile.getFeedbacks().add(feedback);

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void submitFeedback() {

        AlertUtils.showProgress(getActivity());
        String userId = CacheManager.getInstance().getCurrentUser().getUserId();
        String docId = "0";

            docId = SharedData.getInstance().getSelectedChatDocttorId();

        String waitTime = etWaitTime.getText().toString();
         String reason = etReason.getText().toString();
        String description = etDescription.getText().toString();
        float rbPromptnessRating =   rbPromptness.getRating();
        float rbAccurateDiaganosisRating =    rbAccurateDiaganosis.getRating();
        float rbBedsidesMannerRating =   rbBedsidesManner.getRating();
        float rbCourteousStaffRating =    rbCourteousStaff.getRating();
        float rbFollowUpRating =   rbFollowUp.getRating();
        float rbMedicalKnowledgeRating =    rbMedicalKnowledge.getRating();
        float rbSpeningTimeRating =   rbSpeningTime.getRating();
        float rbEaseOfRatingRating =   rbEaseOfRating.getRating();

        waitTime =waitTime.isEmpty()?"":waitTime;
        reason =reason.isEmpty()?"":reason;
        description =description.isEmpty()?"":description;

        boolean isRecommended = rdRecommendYes.isChecked();

        WebServicesHandler.instance.submitDoctorFeedback(docId, userId, waitTime, isRecommended, rbEaseOfRatingRating,
                rbAccurateDiaganosisRating, rbFollowUpRating, rbPromptnessRating,
                rbBedsidesMannerRating, rbMedicalKnowledgeRating, rbCourteousStaffRating, rbSpeningTimeRating, reason, description, new CustomCallback<RetrofitJSONResponse>() {
                    @Override
                    public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                        AlertUtils.dismissProgress();
                        if(!response.status()){
                            AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                            return;
                        }

                        adjustFeedbacks(response);
                        if(SharedData.getInstance().isCallFeedback()){
                            openDashboard();
                        }else{
                            AlertUtils.showAlertForBack(getContext(),Constants.SuccessAlertTitle,"Your feedback is submitted successfully");
                        }


                    }

                    @Override
                    public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                        AlertUtils.dismissProgress();
                        AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
                    }
                });



    }
    private void openDashboard(){

        SharedData.getInstance().setSelectedFilter(null);

        Intent intent = new Intent(context, PatientDashboardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        ((BaseActivity)context).startActivity(intent, true);

    }

}
