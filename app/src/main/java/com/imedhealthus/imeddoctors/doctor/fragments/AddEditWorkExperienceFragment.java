package com.imedhealthus.imeddoctors.doctor.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.CustomCitySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomCountrySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomStateSpinnerAdapter;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.City;
import com.imedhealthus.imeddoctors.common.models.Country;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.State;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.imedhealthus.imeddoctors.doctor.models.WorkExperience;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddEditWorkExperienceFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.et_job_title)
    EditText etJobTitle;

    @BindView(R.id.tv_start_date)
    TextView tvStartDate;
    @BindView(R.id.tv_end_date)
    TextView tvEndDate;
    @BindView(R.id.cb_is_current_job)
    CheckBox cbIsCurrentJob;

    @BindView(R.id.et_organization)
    EditText etOrganization;
    @BindView(R.id.et_supervisor)
    EditText etSupervisor;

    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_fax)
    EditText etFax;
    @BindView(R.id.et_address)
    EditText etAddress;



    @BindView(R.id.sp_city)
    Spinner spCity;
    @BindView(R.id.sp_state)
    Spinner spState;
    @BindView(R.id.sp_country)
    Spinner spCountry;

    private WorkExperience workExperience;
    private List<Country> countries;
    private CustomCountrySpinnerAdapter customCountryAdapter;
    private CustomStateSpinnerAdapter stateAdapter;
    private List<State> states;
    private List<City> cities;
    private CustomCitySpinnerAdapter cityAdapter;

    public AddEditWorkExperienceFragment() {
        workExperience = SharedData.getInstance().getSelectedWorkExperience();
    }


    @Override
    public String getName() {
        return "AddEditWorkExperienceFragment";
    }

    @Override
    public String getTitle() {
        if (workExperience == null) {
            return "Add Work Experience";
        }else {
            return "Edit Work Experience";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_work_experience, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    public void setSpinnerData(final String countryId,final String stateId,final String cityId){

        countries = SharedData.getInstance().getCountriesList();
        customCountryAdapter = new CustomCountrySpinnerAdapter(getContext(), R.layout.custom_spinner_item,countries);
        spCountry.setAdapter(customCountryAdapter);
        int selectedCountryIndex = SharedData.getInstance().getCountryIndexInList(countries,countryId);
        spCountry.setSelection(selectedCountryIndex);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                states = SharedData.getInstance().
                        getStatesListAgainstCountryByCountryId(countryId);
                stateAdapter = new CustomStateSpinnerAdapter(getContext(), R.layout.custom_spinner_item, states);
                spState.setAdapter(stateAdapter);
                int selectedStateIndex =SharedData.getInstance().getStateIndexInList(states,stateId);
                spState.setSelection(selectedStateIndex);


                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cities = SharedData.getInstance().
                                getCitiesListByStateId(stateId);
                        cityAdapter = new CustomCitySpinnerAdapter(getContext(), R.layout.custom_spinner_item, cities);
                        spCity.setAdapter(cityAdapter);
                        int selectedCityIndex =SharedData.getInstance().getCityIndexInList(cities,cityId);
                        spCity.setSelection(selectedCityIndex);
                    }
                }, 200);
            }
        }, 200);


    }


    private void initView() {

        countries = SharedData.getInstance().getCountriesList();
        customCountryAdapter = new CustomCountrySpinnerAdapter(getContext(), R.layout.custom_spinner_item,countries);
        spCountry.setAdapter(customCountryAdapter);

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                states = SharedData.getInstance().
                        getStatesListAgainstCountryByCountryId(Integer.toString(((Country)adapterView.getSelectedItem()).countryId));
                stateAdapter = new CustomStateSpinnerAdapter(getContext(), R.layout.custom_spinner_item, states);
                spState.setAdapter(stateAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                cities = SharedData.getInstance().
                        getCitiesListByStateId(((State)adapterView.getItemAtPosition(i)).getStateId());
                cityAdapter = new CustomCitySpinnerAdapter(getContext(), R.layout.custom_spinner_item, cities);
                spCity.setAdapter(cityAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void setData() {

        if (workExperience == null) {
            return;
        }

        etJobTitle.setText(workExperience.getJobTitle());

        tvStartDate.setText(workExperience.getStartDateFormatted());
        tvEndDate.setText(workExperience.getEndDateFormatted());
        cbIsCurrentJob.setChecked(workExperience.isCurrentJob());

        etOrganization.setText(workExperience.getOrganizationName());
        etSupervisor.setText(workExperience.getSupervisor());

        etEmail.setText(workExperience.getEmail());
        etPhone.setText(workExperience.getPhone());
        etFax.setText(workExperience.getFax());
        etAddress.setText(workExperience.getAddress());

        setSpinnerData(workExperience.getCountryId(),workExperience.getStateId(),workExperience.getCityId());


    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditWorkExperience();
                }
                break;

            case R.id.tv_start_date:
                openDatePicker(tvStartDate.getText().toString(), "startDate");
                break;

            case R.id.tv_end_date:
                openDatePicker(tvEndDate.getText().toString(), "endDate");
                break;

        }
    }


    private void openDatePicker(String dateStr, String tag) {

        Date date;
        if (dateStr.isEmpty()) {
            date = new Date();
        }else {
            date = GenericUtils.unFormatDate(dateStr);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setVersion(DatePickerDialog.Version.VERSION_2);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.light_blue));

        datePickerDialog.show(((Activity)context).getFragmentManager(), tag);

    }


    //Date picker callback
    @Override
    public void onDateSet(DatePickerDialog view, int year, int month, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        if(view.getTag().equalsIgnoreCase("startDate")){
            tvStartDate.setText(GenericUtils.formatDate(calendar.getTime()));
        }else{
            tvEndDate.setText(GenericUtils.formatDate(calendar.getTime()));
        }

    }


    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etJobTitle.getText().toString().isEmpty() ||
                tvStartDate.getText().toString().isEmpty() || tvEndDate.getText().toString().isEmpty() ||
                etOrganization.getText().toString().isEmpty() || etSupervisor.getText().toString().isEmpty() ||
                etEmail.getText().toString().isEmpty() || etFax.getText().toString().isEmpty() ||
                etPhone.getText().toString().isEmpty() || etAddress.getText().toString().isEmpty() ) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustWorkExperiences(RetrofitJSONResponse response, WorkExperience updatedWorkExperience) {

        if (workExperience == null) {//Add

            JSONObject workExperienceJson = response.optJSONObject("DoctorExperience");
            if (workExperienceJson != null) {
                WorkExperience newWorkExperience = new WorkExperience(workExperienceJson);
                DoctorProfile profile = SharedData.getInstance().getSelectedDoctorProfile();
                profile.getWorkExperiences().add(newWorkExperience);
            }

        }else  {

            workExperience.setJobTitle(updatedWorkExperience.getJobTitle());

            workExperience.setStartDate(updatedWorkExperience.getStartDate());
            workExperience.setEndDate(updatedWorkExperience.getEndDate());
            workExperience.setCurrentJob(updatedWorkExperience.isCurrentJob());

            workExperience.setOrganizationName(updatedWorkExperience.getOrganizationName());
            workExperience.setSupervisor(updatedWorkExperience.getSupervisor());

            workExperience.setEmail(updatedWorkExperience.getEmail());
            workExperience.setPhone(updatedWorkExperience.getPhone());
            workExperience.setFax(updatedWorkExperience.getFax());
            workExperience.setAddress(updatedWorkExperience.getAddress());

            workExperience.setCityId(updatedWorkExperience.getCityId());
            workExperience.setCityName(updatedWorkExperience.getCityName());
            workExperience.setStateId(updatedWorkExperience.getStateId());
            workExperience.setStateName(updatedWorkExperience.getStateName());
            workExperience.setCountryId(updatedWorkExperience.getCountryId());
            workExperience.setCountryName(updatedWorkExperience.getCountryName());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditWorkExperience() {

        AlertUtils.showProgress(getActivity());

        final WorkExperience updatedWorkExperience = new WorkExperience();
        if (workExperience != null) {
            updatedWorkExperience.setId(workExperience.getId());
        }

        updatedWorkExperience.setJobTitle(etJobTitle.getText().toString());

        updatedWorkExperience.setStartDateFormatted(tvStartDate.getText().toString());
        Date date = GenericUtils.unFormatDate(tvStartDate.getText().toString());
        updatedWorkExperience.setStartDate(GenericUtils.getTimeDateString(date));

        updatedWorkExperience.setEndDateFormatted(tvEndDate.getText().toString());
        date = GenericUtils.unFormatDate(tvEndDate.getText().toString());
        updatedWorkExperience.setEndDate(GenericUtils.getTimeDateString(date));

        updatedWorkExperience.setCurrentJob(cbIsCurrentJob.isChecked());

        updatedWorkExperience.setOrganizationName(etOrganization.getText().toString());
        updatedWorkExperience.setSupervisor(etSupervisor.getText().toString());

        updatedWorkExperience.setEmail(etEmail.getText().toString());
        updatedWorkExperience.setPhone(etPhone.getText().toString());
        updatedWorkExperience.setFax(etFax.getText().toString());
        updatedWorkExperience.setAddress(etAddress.getText().toString());

        if(spState.getSelectedItem() == null || ((State) spState.getSelectedItem()).getStateId().equals("0")) {
            updatedWorkExperience.setStateId("0");
            updatedWorkExperience.setStateName("");
        }else {
            updatedWorkExperience.setStateId(((State) spState.getSelectedItem()).getStateId());
            updatedWorkExperience.setStateName(((State) spState.getSelectedItem()).getStateName());
        }

        if(spCountry.getSelectedItem() == null || Integer.toString(((Country) spCountry.getSelectedItem()).countryId).equals("0")) {
            updatedWorkExperience.setCountryId("0");
            updatedWorkExperience.setCountryName("");
        }else {
            updatedWorkExperience.setCountryId(Integer.toString(((Country) spCountry.getSelectedItem()).countryId));
            updatedWorkExperience.setCountryName(((Country) spCountry.getSelectedItem()).getCountryName());
        }

        if(spCity.getSelectedItem() == null || ((City) spCity.getSelectedItem()).getCityId().equals("0")) {
            updatedWorkExperience.setCityId("0");
            updatedWorkExperience.setCityName("");
        }else {
            updatedWorkExperience.setCityId(((City) spCity.getSelectedItem()).getCityId());
            updatedWorkExperience.setCityName(((City) spCity.getSelectedItem()).getCityName());
        }

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addDoctorExperience(userId, updatedWorkExperience, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                adjustWorkExperiences(response, updatedWorkExperience);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Work experience saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
