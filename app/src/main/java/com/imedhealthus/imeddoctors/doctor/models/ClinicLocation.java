package com.imedhealthus.imeddoctors.doctor.models;

import android.text.TextUtils;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class ClinicLocation extends OverviewItem {

    private String id, address, postalCode;
    private String cityName, stateName, countryName;
    private String startTime, endTime, startTimeFormatted, endTimeFormatted;
    private double radius, latitude, longitude;

    public ClinicLocation() {
    }

    public ClinicLocation(JSONObject educationJson) {

        id = educationJson.optString("Id", "");
        address = educationJson.optString("StreetAddress", "").replace("null", "");
        postalCode = educationJson.optString("PostalCode", "").replace("null", "");

        startTime = educationJson.optString("StartTime", "");
        Date date = GenericUtils.getDateFromString(startTime);
        startTimeFormatted = GenericUtils.formatTime(date);

        endTime = educationJson.optString("EndTime", "");
        date = GenericUtils.getDateFromString(endTime);
        endTimeFormatted = GenericUtils.formatTime(date);

        cityName = educationJson.optString("City", "").replace("null", "");
        stateName = educationJson.optString("State", "").replace("null", "");
        countryName = educationJson.optString("Country", "").replace("null", "");

        radius = educationJson.optDouble("Radius", 0);
        latitude = educationJson.optDouble("Latitude", 0);
        longitude = educationJson.optDouble("Longitude", 0);

    }

    public static List<ClinicLocation> parseClinicLocations(JSONArray clinicLocationsJson) {

        ArrayList<ClinicLocation> clinicLocations = new ArrayList<>();

        for (int i = 0; i < clinicLocationsJson.length(); i++) {

            JSONObject clinicLocationJson = clinicLocationsJson.optJSONObject(i);
            if (clinicLocationJson != null) {
                ClinicLocation clinicLocation = new ClinicLocation(clinicLocationJson);
                clinicLocations.add(clinicLocation);
            }

        }

        return clinicLocations;

    }

    public String getFullAddress() {

        String fullAddress = address;

        if (!TextUtils.isEmpty(cityName)) {
            fullAddress += ", " + cityName;
        }
        if (!TextUtils.isEmpty(postalCode)) {
            fullAddress += ", " + postalCode;
        }
        if (!TextUtils.isEmpty(stateName)) {
            fullAddress += ", " + stateName;
        }
        if (!TextUtils.isEmpty(countryName)) {
            fullAddress += ", " + countryName;
        }

        return fullAddress != null ? fullAddress : "";

    }


    @Override
    public String getDisplayTitle() {

        String displayTitle = address;
        if (!TextUtils.isEmpty(postalCode)) {
            displayTitle += ", " + postalCode;
        }
        return displayTitle != null ? displayTitle : "";

    }

    @Override
    public String getDisplayDetail() {

        String displayDetail = cityName;

        if (!TextUtils.isEmpty(stateName)) {
            if (TextUtils.isEmpty(displayDetail)) {
                displayDetail = stateName;
            } else {
                displayDetail += ", " + stateName;
            }
        }
        if (!TextUtils.isEmpty(countryName)) {
            if (TextUtils.isEmpty(displayDetail)) {
                displayDetail = countryName;
            } else {
                displayDetail += ", " + countryName;
            }
        }

        return displayDetail != null ? displayDetail : "";

    }

    @Override
    public String getDisplayDuration() {

        String displayDuration = startTimeFormatted;
        if (!TextUtils.isEmpty(endTimeFormatted)) {
            displayDuration += " - " + endTimeFormatted;
        }
        return displayDuration != null ? displayDuration : "";

    }


    public String getId() {
        return id != null ? id : "1";
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address != null ? address : "";
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode != null ? postalCode : "";
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCityName() {
        return cityName != null ? cityName : "";
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateName() {
        return stateName != null ? stateName : "";
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryName() {
        return countryName != null ? countryName : "";
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStartTimeFormatted() {
        return startTimeFormatted;
    }

    public void setStartTimeFormatted(String startTimeFormatted) {
        this.startTimeFormatted = startTimeFormatted;
    }

    public String getEndTimeFormatted() {
        return endTimeFormatted;
    }

    public void setEndTimeFormatted(String endTimeFormatted) {
        this.endTimeFormatted = endTimeFormatted;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
