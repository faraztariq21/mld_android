package com.imedhealthus.imeddoctors.doctor.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.doctor.models.Feedback;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/6/18.
 */

public class FeedbackItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.iv_profile)
    ImageView ivProfile;

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_time)
    TextView tvTime;

    @BindView(R.id.tv_comment)
    TextView tvComment;
    @BindView(R.id.tv_rating)
    TextView tvRating;

    @BindView(R.id.iv_like)
    ImageView ivLike;
    @BindView(R.id.iv_dislike)
    ImageView ivDislike;

    public FeedbackItemViewHolder(View view) {

        super(view);
        ButterKnife.bind(this, view);

    }

    public void setData(Feedback feedback) {

        tvName.setText(feedback.getUserFullName());
        tvTime.setText(feedback.getTimeAgo());

        tvComment.setText(feedback.getComment());
        tvRating.setText(String.format("%.1f", feedback.getRating()));

        if (feedback.hasUserLiked()) {
            ivLike.setImageResource(R.drawable.ic_like_light_blue);
            ivDislike.setImageResource(R.drawable.ic_dislike_gray);
        }else {
            ivLike.setImageResource(R.drawable.ic_like_gray);
            ivDislike.setImageResource(R.drawable.ic_dislike_light_blue);
        }

        Glide.with(ApplicationManager.getContext()).load(feedback.getUserImageUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivProfile);

    }

}

