package com.imedhealthus.imeddoctors.doctor.models;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 1/7/18.
 */

public class Service {

    private String id, title, details;
    private double price;

    public Service() {
    }

    public Service(JSONObject jsonObject) {
        try {
            id = jsonObject.getString("Id");
            title = jsonObject.getString("Service").replace("null","");
            details = GenericUtils.decodeHtml(jsonObject.getString("Description").replace("null",""));
            price = jsonObject.getInt("Price");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static List<Service> parseServices(JSONArray servicesJson){

        ArrayList<Service> services = new ArrayList<>();

        for (int i = 0; i < servicesJson.length(); i++){

            JSONObject serviceJson = servicesJson.optJSONObject(i);
            if (serviceJson != null) {
                Service service = new Service(serviceJson);
                services.add(service);
            }

        }

        return services;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
