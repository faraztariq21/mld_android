package com.imedhealthus.imeddoctors.doctor.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.imedhealthus.imeddoctors.doctor.models.Language;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddEditLanguageFragment extends BaseFragment {

    @BindView(R.id.et_language)
    EditText etLanguage;

    private Language language;

    public AddEditLanguageFragment() {
        language = SharedData.getInstance().getSelectedLanguage();
    }


    @Override
    public String getName() {
        return "AddEditLanguageFragment";
    }

    @Override
    public String getTitle() {
        if (language == null) {
            return "Add Language";
        }else {
            return "Edit Language";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_language, container, false);
        ButterKnife.bind(this, view);

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    private void setData() {

        if (language == null) {
            return;
        }

        etLanguage.setText(language.getName());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditLanguage();
                }
                break;

        }
    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etLanguage.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustLanguages(RetrofitJSONResponse response, Language updatedLanguage) {

        if (language == null) {//Add

            JSONObject languageJson = response.optJSONObject("DoctorLanguage");
            if (languageJson != null) {
                Language newLanguage = new Language(languageJson);
                DoctorProfile profile = SharedData.getInstance().getSelectedDoctorProfile();
                profile.getLanguages().add(newLanguage);
            }

        }else  {

            language.setName(updatedLanguage.getName());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditLanguage() {

        AlertUtils.showProgress(getActivity());

        final Language updatedLanguage = new Language();
        if (language != null) {
            updatedLanguage.setId(language.getId());
        }

        updatedLanguage.setName(etLanguage.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditLanguage(userId, updatedLanguage, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                adjustLanguages(response, updatedLanguage);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Language saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
