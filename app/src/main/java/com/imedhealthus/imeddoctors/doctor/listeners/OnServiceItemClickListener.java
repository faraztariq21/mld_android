package com.imedhealthus.imeddoctors.doctor.listeners;

/**
 * Created by umair on 1/7/18.
 */

public interface OnServiceItemClickListener {
    void onServiceDetailClick(int idx);
    void onServiceEditClick(int idx);
    void onServiceDeleteClick(int idx);
}
