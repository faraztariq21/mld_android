package com.imedhealthus.imeddoctors.doctor.fragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.imedhealthus.imeddoctors.doctor.models.OverviewItem;
import com.imedhealthus.imeddoctors.doctor.models.SocialLink;
import com.imedhealthus.imeddoctors.patient.models.PatientProfile;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddEditSocialLinkFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.sp_type)
    Spinner spType;
    @BindView(R.id.et_url)
    EditText etUrl;

    @BindView(R.id.cont_social_links)
    LinearLayout conSocialLinks;
    @BindView(R.id.tv_no_data)
    TextView tvNoSocialLinks;

    private List<SocialLink> socialLinks;
    private SocialLink socialLink;


    public AddEditSocialLinkFragment() {

    }


    @Override
    public String getName() {
        return "AddEditSocialLinkFragment";
    }

    @Override
    public String getTitle() {
        if (socialLink == null) {
            return "Add Social Link";
        }else {
            return "Edit Social Link";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_social_link, container, false);
        ButterKnife.bind(this, view);
        socialLinks = SharedData.getInstance().getSelectedSocialLink();
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    private void setData() {

        if (socialLinks == null) {
            return;
        }

        setSocialLinks(socialLinks);

    }


    private void setSocialLinks(List<SocialLink> socialLinks) {

        conSocialLinks.removeAllViews();

        for (int i = 0; i < socialLinks.size(); i++) {

            OverviewItem item = socialLinks.get(i);
            View itemDisease = LayoutInflater.from(context).inflate(R.layout.list_item_doctor_overview, conSocialLinks, false);

           // ivReviews.setImageResource(R.drawable.ic_review_gray);

            ((TextView)itemDisease.findViewById(R.id.tv_title)).setText(item.getDisplayTitle());
            ((TextView)itemDisease.findViewById(R.id.tv_details)).setText(item.getDisplayDetail());
            ((TextView)itemDisease.findViewById(R.id.tv_duration)).setText(item.getDisplayDuration());

            View ivEdit = itemDisease.findViewById(R.id.iv_edit_item);
            View ivDelete = itemDisease.findViewById(R.id.iv_delete_item);


                ivEdit.setTag(i);
                ivEdit.setOnClickListener(this);
                ivDelete.setTag(i);
                ivDelete.setOnClickListener(this);



            conSocialLinks.addView(itemDisease);

        }

        if (socialLinks.size() == 0) {
            tvNoSocialLinks.setVisibility(View.VISIBLE);
        }else {
            tvNoSocialLinks.setVisibility(View.GONE);
        }

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditSocialLink();
                }
                break;
            case R.id.iv_edit_item:
                editSocialLink((int)view.getTag());
                break;
            case R.id.iv_delete_item:
                deleteSocialLink((int)view.getTag());
                break;

        }
    }

    private void editSocialLink(int idx) {
        if (idx < 0 || idx >= socialLinks.size()) {
            return;
        }

        socialLink = socialLinks.get(idx);
        List<String> social = Arrays.asList(context.getResources().getStringArray(R.array.social_media));
        idx = social.indexOf(socialLink.getType());
        if (idx > 0) {
            spType.setSelection(idx);
        }
        etUrl.setText(socialLink.getUrl());

    }

    private void showDeleteAlert(DialogInterface.OnClickListener acceptClickListener) {

        new AlertDialog.Builder(context)
                .setTitle("Confirm")
                .setMessage("Are you sure you want to delete this record?")
                .setPositiveButton("Yes", acceptClickListener)
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }})
                .show();

    }

    public void deleteSocialLink(int idx) {

        if (idx < 0 || idx >= socialLinks.size()) {
            return;
        }

       socialLink = socialLinks.get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteSocialLink(socialLink);

            }
        });

    }

    private void deleteSocialLink(final SocialLink socialLink) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteSocialLink(socialLink,Constants.UserType.Patient, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Social link details deleted successfully");

                if(CacheManager.getInstance().getCurrentUser().getUserType()== Constants.UserType.Patient){
                    SharedData.getInstance().getSelectedPatientProfile().getSocialLinks().remove(socialLink);
                }
                else{
                    SharedData.getInstance().getSelectedDoctorProfile().getSocialLinks().remove(socialLink);
                }
                setSocialLinks(socialLinks);
                SharedData.getInstance().setRefreshRequired(true);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }
    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if ( etUrl.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }
        if (spType.getSelectedItemPosition()==0 ) {
            errorMsg = "Kindly select link type to continue";
        }
        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustSocialLinks(RetrofitJSONResponse response, SocialLink updatedSocialLink) {

        if (socialLink == null) {//Add

            JSONObject socialLinkJson;
            if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Patient) {
                socialLinkJson = response.optJSONObject("PatientSocialLink");
            }else {
                socialLinkJson = response.optJSONObject("DoctorSocialLink");
            }

            if(CacheManager.getInstance().getCurrentUser().getUserType()== Constants.UserType.Doctor){
                if (socialLinkJson != null) {
                    SocialLink newSocialLink = new SocialLink(socialLinkJson);
                    DoctorProfile profile = SharedData.getInstance().getSelectedDoctorProfile();
                    profile.getSocialLinks().add(newSocialLink);
                }
            }else{
                if (socialLinkJson != null) {
                    SocialLink newSocialLink = new SocialLink(socialLinkJson);
                    PatientProfile profile = SharedData.getInstance().getSelectedPatientProfile();
                    profile.getSocialLinks().add(newSocialLink);
                }
            }
        }else  {

            socialLink.setType(updatedSocialLink.getType());
            socialLink.setUrl(updatedSocialLink.getUrl());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditSocialLink() {

        AlertUtils.showProgress(getActivity());

        final SocialLink updatedSocialLink = new SocialLink();
        if (socialLink != null) {
            if (socialLink.getId() != null)
              updatedSocialLink.setId(socialLink.getId());
        }

        updatedSocialLink.setType(spType.getSelectedItem().toString());
        updatedSocialLink.setUrl(etUrl.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditSocialLink(userId, updatedSocialLink,
                CacheManager.getInstance().getCurrentUser().getUserType(), new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                adjustSocialLinks(response, updatedSocialLink);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Social link saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
