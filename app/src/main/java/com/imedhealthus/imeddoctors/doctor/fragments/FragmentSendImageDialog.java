package com.imedhealthus.imeddoctors.doctor.fragments;


import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentSendImageDialog#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentSendImageDialog extends BaseDialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String IMAGE_URI = "param1";


    private String imageUri;
    @BindView(R.id.et_new_message)
    EditText etMessage;
    @BindView(R.id.btn_send)
    Button bSend;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.iv_image_to_send)
    ImageView ivImageToSend;

    FragmentSendImageClickListener fragmentSendImageClickListener;

    public FragmentSendImageDialog() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentSendImageClickListener)
            fragmentSendImageClickListener = (FragmentSendImageClickListener) context;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param imageUri Parameter 1.
     * @return A new instance of fragment FragmentSendImageDialog.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentSendImageDialog newInstance(String imageUri) {
        FragmentSendImageDialog fragment = new FragmentSendImageDialog();
        Bundle args = new Bundle();
        args.putString(IMAGE_URI, imageUri);

        fragment.setArguments(args);
        return fragment;
    }

    public void setFragmentSendImageClickListener(FragmentSendImageClickListener fragmentSendImageClickListener) {
        this.fragmentSendImageClickListener = fragmentSendImageClickListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            imageUri = getArguments().getString(IMAGE_URI);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_send_image_dialog, container, false);
        ButterKnife.bind(this, view);

        if (fragmentSendImageClickListener == null && getActivity() instanceof FragmentSendImageClickListener)
            fragmentSendImageClickListener = (FragmentSendImageClickListener) getActivity();

        if (fragmentSendImageClickListener == null && getParentFragment() instanceof FragmentSendImageClickListener)
            fragmentSendImageClickListener = (FragmentSendImageClickListener) getParentFragment();

        if (imageUri != null)
            ivImageToSend.setImageURI(Uri.parse(imageUri));

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragmentSendImageClickListener != null)
                    fragmentSendImageClickListener.onSendImageBackTapped();

                FragmentSendImageDialog.this.dismiss();
            }
        });

        bSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragmentSendImageClickListener != null)
                    fragmentSendImageClickListener.onSendImageSendTapped(etMessage.getText().toString(), imageUri);

                FragmentSendImageDialog.this.dismiss();

            }
        });

        return view;
    }

    public interface FragmentSendImageClickListener {
        public void onSendImageBackTapped();

        public void onSendImageSendTapped(String text, String imageUri);
    }

}
