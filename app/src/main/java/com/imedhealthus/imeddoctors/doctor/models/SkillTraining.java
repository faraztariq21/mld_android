package com.imedhealthus.imeddoctors.doctor.models;

import android.text.TextUtils;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class SkillTraining extends OverviewItem {

    private String id, educationType, skill, institute, supervisor, notes;
    private String address, phone, cityId, cityName, stateId, stateName, countryId, countryName;
    private String startDate, endDate, startDateFormatted, endDateFormatted;

    public SkillTraining() {
    }

    public SkillTraining(JSONObject educationJson) {

        id = educationJson.optString("Id", "");
        educationType = educationJson.optString("EducationTypeId", "").replace("null","");
        skill = educationJson.optString("SkillTitle", "").replace("null","");

        supervisor = educationJson.optString("Supervisor", "").replace("null","");
        institute = educationJson.optString("InstituteId", "").replace("null","");
        notes = educationJson.optString("Description", "").replace("null","");


        startDate = educationJson.optString("StartDate", "").replace("null","");
        Date date = GenericUtils.getDateFromString(startDate);
        startDateFormatted = GenericUtils.formatDate(date);

        endDate = educationJson.optString("EndDate", "").replace("null","");
        date = GenericUtils.getDateFromString(endDate);
        endDateFormatted = GenericUtils.formatDate(date);


        address = educationJson.optString("Address", "").replace("null","");
        phone = educationJson.optString("PhoneNo", "").replace("null","");

        cityId = educationJson.optString("CityId", "").replace("null","");
        cityName = educationJson.optString("CityName", "").replace("null","");
        stateId = educationJson.optString("StateId", "").replace("null","");
        stateName = educationJson.optString("StateName", "").replace("null","");
        countryId = educationJson.optString("CountryId", "").replace("null","");
        countryName = educationJson.optString("CountryName", "").replace("null","");

    }

    public static List<SkillTraining> parseSkillTrainings(JSONArray skillsTrainingsJson){

        ArrayList<SkillTraining> skillTrainings = new ArrayList<>();

        for (int i = 0; i < skillsTrainingsJson.length(); i++){

            JSONObject skillsTrainingJson = skillsTrainingsJson.optJSONObject(i);
            if (skillsTrainingJson != null) {
                SkillTraining skillTraining = new SkillTraining(skillsTrainingJson);
                skillTrainings.add(skillTraining);
            }

        }

        return skillTrainings;

    }


    @Override
    public String getDisplayTitle() {

        String displayTitle =  skill;
        if (!TextUtils.isEmpty(educationType)) {
            displayTitle += " (" + educationType + ")";
        }
        return displayTitle;

    }

    @Override
    public String getDisplayDetail() {

        String displayDetail = institute;

        if (!TextUtils.isEmpty(supervisor)) {
            displayDetail += " - " + supervisor;
        }

        String address = cityName;
        if (!TextUtils.isEmpty(stateName)) {
            if (TextUtils.isEmpty(address)) {
                address = stateName;
            }else {
                address += ", " + stateName;
            }
        }
        if (!TextUtils.isEmpty(countryName)) {
            if (TextUtils.isEmpty(address)) {
                address = countryName;
            }else {
                address += ", " + countryName;
            }
        }

        if (!TextUtils.isEmpty(address)) {
            displayDetail += " (" + address + ")";
        }
        return displayDetail;

    }

    @Override
    public String getDisplayDuration() {

        String displayDuration = startDateFormatted;
        if (!TextUtils.isEmpty(endDateFormatted)) {
            displayDuration += " - " + endDateFormatted;
        }
        return displayDuration;

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEducationType() {
        return educationType;
    }

    public void setEducationType(String educationType) {
        this.educationType = educationType;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDateFormatted() {
        return startDateFormatted;
    }

    public void setStartDateFormatted(String startDateFormatted) {
        this.startDateFormatted = startDateFormatted;
    }

    public String getEndDateFormatted() {
        return endDateFormatted;
    }

    public void setEndDateFormatted(String endDateFormatted) {
        this.endDateFormatted = endDateFormatted;
    }

}
