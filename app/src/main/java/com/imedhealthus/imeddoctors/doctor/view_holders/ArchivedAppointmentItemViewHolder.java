package com.imedhealthus.imeddoctors.doctor.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.listeners.OnArchivedAppointmentItemClickListener;
import com.imedhealthus.imeddoctors.doctor.models.ArchivedAppointment;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 2/22/2018.
 */

public class ArchivedAppointmentItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    ViewGroup contItem;

    @BindView(R.id.iv_type)
    ImageView ivType;
    @BindView(R.id.iv_dropdown)
    ImageView ivDropdown;

    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_reason)
    TextView tvDescription;

    @BindView(R.id.cont_location)
    ViewGroup contLocation;
    @BindView(R.id.iv_pin)
    ImageView ivAddressPin;
    @BindView(R.id.tv_address)
    TextView tvAddress;

    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.cont_buttons)
    ViewGroup contButtons;

    @BindView(R.id.btn_postpone)
    Button btnComplete;


    private int idx;
    private WeakReference<OnArchivedAppointmentItemClickListener> onArchivedAppointmentItemClickListener;

    public ArchivedAppointmentItemViewHolder(View view, OnArchivedAppointmentItemClickListener onArchivedAppointmentItemClickListener) {

        super(view);
        ButterKnife.bind(this, view);

        contItem.setOnClickListener(this);
        btnComplete.setOnClickListener(this);

        this.onArchivedAppointmentItemClickListener = new WeakReference<>(onArchivedAppointmentItemClickListener);

    }

    public void setData(ArchivedAppointment appointment, boolean isSelected, int idx) {

        if (appointment.getType() == Constants.AppointmentType.OFFICE) {//TODO: Change icon accordingly
            ivType.setImageResource(R.drawable.ic_profile_placeholder);
        }else {
            ivType.setImageResource(R.drawable.ic_profile_placeholder);
        }

        if(new Date().getTime()>appointment.getEndTimeStamp())
            btnComplete.setVisibility(View.VISIBLE);
        tvType.setText(appointment.getType().toString(ApplicationManager.getContext()));
        tvDescription.setText(appointment.getNotes());
        tvAddress.setText(appointment.getPhysicalLocation());
        tvTime.setText( new SimpleDateFormat("hh:mm a").format(new Date(appointment.getStartTimeStamp()))
                        + " " + appointment.getAction());

        tvName.setText(appointment.getPatientFullName());

        if (appointment.getType() == Constants.AppointmentType.OFFICE && isSelected) {
            contLocation.setVisibility(View.VISIBLE);
        }else {
            contLocation.setVisibility(View.GONE);
        }

        if (isSelected) {
            ivDropdown.setRotation(180);
            contButtons.setVisibility(View.VISIBLE);
        }else {
            ivDropdown.setRotation(0);
            contButtons.setVisibility(View.GONE);
        }

        this.idx = idx;

    }


    @Override
    public void onClick(View view) {

        if (onArchivedAppointmentItemClickListener == null || onArchivedAppointmentItemClickListener.get() == null) {
            return;
        }

        switch (view.getId()){
            case R.id.cont_item:
                onArchivedAppointmentItemClickListener.get().onArchivedAppointmentItemDetailClick(idx);
                break;

            case R.id.btn_postpone:
                onArchivedAppointmentItemClickListener.get().onArchivedAppointmentItemUndoCompletionClick(idx);
                break;


        }
    }


}

