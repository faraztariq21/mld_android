package com.imedhealthus.imeddoctors.doctor.models;

import com.imedhealthus.imeddoctors.common.models.SuggestionListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 2/1/2018.
 */

public class StaticData implements SuggestionListItem {
    private String tableId;
    private String id;
    private String value;

    public StaticData() {
    }

    public StaticData(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public boolean compaireKey(String key) {
        if (id.equalsIgnoreCase(key))
            return true;
        return false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static StaticData getObjectFromValue(List<StaticData> staticData, String value) {
        for (int i = 0; i < staticData.size(); i++) {
            if (staticData.get(i).getValue().toLowerCase().equals(value.toLowerCase().trim()))
                return staticData.get(i);
        }
        return null;
    }

    @Override
    public String getSuggestionText() {
        return value;
    }

    @Override
    public SuggestionListItem getSuggestionListItem() {
        return StaticData.this;
    }

    public static ArrayList<SuggestionListItem> getSuggestions(ArrayList<StaticData> staticData){
        ArrayList<SuggestionListItem> suggestionListItems = new ArrayList<>();
        for(StaticData staticData1:staticData){
            suggestionListItems.add(staticData1);
        }
        return suggestionListItems;
    }
}
