package com.imedhealthus.imeddoctors.doctor.models;

/**
 * Created by umair on 2/11/18.
 */

public abstract class OverviewItem {

    public enum Type {
        speciality, experience, education,
        language, award, membership,
        skillTraining, trainingProvider, certificate,
        insurance, reference, location,
        socialLink;
    }

    public abstract String getDisplayTitle();

    public abstract String getDisplayDetail();

    public abstract String getDisplayDuration();

}
