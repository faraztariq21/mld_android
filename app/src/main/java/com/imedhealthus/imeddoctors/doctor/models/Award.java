package com.imedhealthus.imeddoctors.doctor.models;

import android.text.TextUtils;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class Award extends OverviewItem {

    private String id, title, institute, date, dateFormatted;
    private String cityId, cityName, stateId, stateName, countryId, countryName;

    public Award() {
    }

    public Award(JSONObject educationJson) {

        id = educationJson.optString("Id", "").replace("null","");
        title = educationJson.optString("AwardTitle", "").replace("null","");
        institute = educationJson.optString("InstituteId", "").replace("null","");

        date = educationJson.optString("AwardDate", "").replace("null","");
        Date date = GenericUtils.getDateFromString(this.date);
        dateFormatted = GenericUtils.formatDate(date);

        cityId = educationJson.optString("CityId", "").replace("null","");
        cityName = educationJson.optString("CityName", "").replace("null","");
        stateId = educationJson.optString("StateId", "").replace("null","");
        stateName = educationJson.optString("StateName", "").replace("null","");
        countryId = educationJson.optString("CountryId", "").replace("null","");
        countryName = educationJson.optString("CountryName", "").replace("null","");
    }

    public static List<Award> parseAwards(JSONArray awardsJson){

        ArrayList<Award> awards = new ArrayList<>();

        for (int i = 0; i < awardsJson.length(); i++){

            JSONObject awardJson = awardsJson.optJSONObject(i);
            if (awardJson != null) {
                Award award = new Award(awardJson);
                awards.add(award);
            }

        }

        return awards;

    }


    @Override
    public String getDisplayTitle() {

        String displayTitle =  title;
        if (!TextUtils.isEmpty(institute)) {
            displayTitle += " (" + institute + ")";
        }
        return displayTitle;

    }

    @Override
    public String getDisplayDetail() {

        String displayDetail = cityName;
        if (!TextUtils.isEmpty(stateName)) {
            if (TextUtils.isEmpty(displayDetail)) {
                displayDetail = stateName;
            }else {
                displayDetail += ", " + stateName;
            }
        }
        if (!TextUtils.isEmpty(countryName)) {
            if (TextUtils.isEmpty(displayDetail)) {
                displayDetail = countryName;
            }else {
                displayDetail += ", " + countryName;
            }
        }
        return displayDetail;

    }

    @Override
    public String getDisplayDuration() {

        String displayDuration = dateFormatted;
        return displayDuration;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateFormatted() {
        return dateFormatted;
    }

    public void setDateFormatted(String dateFormatted) {
        this.dateFormatted = dateFormatted;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
