package com.imedhealthus.imeddoctors.doctor.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.listeners.OnAppointmentItemClickListener;
import com.imedhealthus.imeddoctors.doctor.models.Appointment;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/6/18.
 */

public class AppointmentItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_appointment)
    ViewGroup contAppointment;

    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_time)
    TextView tvTime;

    @BindView(R.id.iv_remove_indicator)
    ImageView ivDeleteIndicator;

    private boolean typeBasedItems;
    private int idx;
    private WeakReference<OnAppointmentItemClickListener> onAppointmentItemClickListener;

    public AppointmentItemViewHolder(View view, boolean typeBasedItems, OnAppointmentItemClickListener onAppointmentItemClickListener) {

        super(view);
        ButterKnife.bind(this, view);

        contAppointment.setOnClickListener(this);
        ivDeleteIndicator.setOnClickListener(this);

        this.typeBasedItems = typeBasedItems;
        this.onAppointmentItemClickListener = new WeakReference<>(onAppointmentItemClickListener);

    }

    public void setData(Appointment appointment, boolean isSelected, int idx) {

        this.idx = idx;

        tvDate.setText(appointment.getFormattedDate());
        tvTime.setText(appointment.getFormattedTime());

       /* if (CacheManager.getInstance().getCurrentUser() != null)
            if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor)
                ivDeleteIndicator.setVisibility(View.VISIBLE);*/
        /*if (typeBasedItems) {
         */
        if (appointment.hasPassed()) {
            contAppointment.setBackgroundResource(R.drawable.apt_passed_bg);
        } else if (isSelected) {
            contAppointment.setBackgroundResource(R.drawable.apt_selected_bg);
        } else if (appointment.getType() == Constants.AppointmentType.OFFICE) {
            contAppointment.setBackgroundResource(R.drawable.apt_physical_bg);
        } else {
            contAppointment.setBackgroundResource(R.drawable.apt_tele_bg);
        }

        /*}else {

            if (appointment.hasPassed()) {
                contAppointment.setBackgroundResource(R.drawable.apt_disable_bg);
            } else {
                contAppointment.setBackgroundResource(R.drawable.apt_normal_bg);
            }

        }
*/
    }

    @Override
    public void onClick(View view) {

        if (onAppointmentItemClickListener == null || onAppointmentItemClickListener.get() == null) {
            return;
        }
        onAppointmentItemClickListener.get().onAppointmentItemClick(idx);

    }
}

