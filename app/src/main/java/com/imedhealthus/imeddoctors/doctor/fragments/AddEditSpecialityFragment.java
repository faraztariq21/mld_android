package com.imedhealthus.imeddoctors.doctor.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.CustomArrayAdapter;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.Logs;
import com.imedhealthus.imeddoctors.doctor.adapters.CustomMultiAutoCompleteAdapter;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.imedhealthus.imeddoctors.doctor.models.Speciality;
import com.imedhealthus.imeddoctors.doctor.models.StaticData;
import com.imedhealthus.imeddoctors.doctor.models.SubSpeciality;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddEditSpecialityFragment extends BaseFragment {

    @BindView(R.id.et_speciality)
    EditText etSpeciality;
    @BindView(R.id.et_sub_speciality)
    MultiAutoCompleteTextView etSubSpeciality;


    @BindView(R.id.sp_speciality)
    Spinner spSpeciality;

    List<StaticData> listSpeciality;
    private Speciality speciality;

    public AddEditSpecialityFragment() {
        speciality = SharedData.getInstance().getSelectedSpeciality();
    }


    @Override
    public String getName() {
        return "AddEditSpecialityFragment";
    }

    @Override
    public String getTitle() {
        if (speciality == null) {
            return "Add Speciality";
        }else {
            return "Edit Speciality";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_speciality, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;

    }

    private void initView() {

        listSpeciality = SharedData.getInstance().getSpecialityList();

        CustomMultiAutoCompleteAdapter reasonsAdapter = new CustomMultiAutoCompleteAdapter(getActivity(),
                R.layout.simple_dropdown_item_1line, etSubSpeciality, listSpeciality);
        etSubSpeciality.setAdapter(reasonsAdapter);
        etSubSpeciality.setThreshold(1);

        etSubSpeciality.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());


        CustomArrayAdapter dataAdapter = new CustomArrayAdapter(context, R.layout.custom_spinner_item, listSpeciality);
        spSpeciality.setAdapter(dataAdapter);
        spSpeciality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
              /*  if(i==listSpeciality.size()-1){
                    etSpeciality.setVisibility(View.VISIBLE);
                    etSubSpeciality.setVisibility(View.GONE);
                    return;
                }else{
                    etSpeciality.setVisibility(View.GONE);
                    etSubSpeciality.setVisibility(View.VISIBLE);
                }
                */
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    private void setData() {

        if (speciality == null) {
            return;
        }

        spSpeciality.setSelection(SharedData.getInstance().getIndexInList(listSpeciality,speciality.getName()));
        etSpeciality.setVisibility(View.GONE);

        StringBuilder builder = new StringBuilder();
        for (SubSpeciality subSpeciality : speciality.getSubSpecialities()) {
            builder.append(subSpeciality.getName()).append(", ");
        }
        etSubSpeciality.setText(builder.toString());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditSpeciality();
                }
                break;

        }
    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if(spSpeciality.getSelectedItemPosition()==0){
            errorMsg = "Kindly fill select speciality to continue";
        } else if(spSpeciality.getSelectedItemPosition()==listSpeciality.size()-1){
            if ( etSpeciality.getText().toString().isEmpty()) {
                errorMsg = "Kindly fill select speciality to continue";
            }
        } else if ( etSubSpeciality.getText().toString().isEmpty()) {
                errorMsg = "Kindly add one sub-speciality to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustSpecialities(RetrofitJSONResponse response, Speciality updatedSpeciality) {

        if (updatedSpeciality == null) {//Add

            if (response != null) {
                Speciality newSpeciality = new Speciality(response);
                DoctorProfile profile = SharedData.getInstance().getSelectedDoctorProfile();
                profile.getSpecialities().add(newSpeciality);
            }

        }else  {

            speciality.setName(updatedSpeciality.getName());
            speciality.setSubSpecialities(updatedSpeciality.getSubSpecialities());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditSpeciality() {



        final Speciality updatedSpeciality = new Speciality();
        if (speciality != null) {
            updatedSpeciality.setUniqueId(speciality.getUniqueId());
        } else {
            updatedSpeciality.setUniqueId("0");
        }



        StaticData speciality = (StaticData) spSpeciality.getSelectedItem();

        updatedSpeciality.setName(speciality.getValue());
        updatedSpeciality.setId(speciality.getId());

        String subSpeciality = etSubSpeciality.getText().toString();
        List<SubSpeciality> subSpecialities = new ArrayList<>();
        String[] subSpecialityArray = subSpeciality.split(",");
        for (int i = 0; i < subSpecialityArray.length; i++) {
            StaticData staticData = StaticData.getObjectFromValue(listSpeciality,subSpeciality.split(",")[i]);
            if(subSpeciality.split(",")[i].trim().equals("")) {
                continue;
            } else {
                if (staticData == null) {
                    AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, "Kindly add valid sub-speciality to continue");
                    return;
                }
            }
            SubSpeciality subSpec = new SubSpeciality(staticData.getId(),staticData.getValue());

            subSpecialities.add(subSpec);
        }

        if(subSpecialities.size() == 0) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, "Kindly add valid sub-speciality to continue");
            return;
        }

        updatedSpeciality.setSubSpecialities(subSpecialities);

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();
        updatedSpeciality.setDoctorId(userId);


        AlertUtils.showProgress(getActivity());
        WebServicesHandler.instance.addEditSpeciality(updatedSpeciality, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse("add/updateSpeciality : " + response.toString());
                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

               // adjustSpecialities(response, updatedSpeciality);
                SharedData.getInstance().setRefreshViaServerHit(true);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Specialty details saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
