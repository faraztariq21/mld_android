package com.imedhealthus.imeddoctors.doctor.models;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by umair on 1/6/18.
 */

public class PersonalInfo {

    private String id, profileImageUrl, prefix, firstName, lastName, secondaryEmail,primaryEmail;
    private String dob, dobFormatted, gender, maritalStatus, occupation, primaryAddress, secondaryAddress;
    private String cityId, cityName, stateId, stateName, countryId, countryName, practiceCountryId, practiceCountryName;
    private String homePhone, officePhone, personalPhone, fax, introduction;


    public PersonalInfo() {
    }

    public PersonalInfo(JSONObject educationJson) {

        id = educationJson.optString("Id", "").replace("null","");
        profileImageUrl = GenericUtils.getImageUrl(educationJson.optString("ProfilePicPath", ""));
        prefix = educationJson.optString("Prefix", "").replace("null","");
        firstName = educationJson.optString("FirstName", "").replace("null","");
        lastName = educationJson.optString("LastName", "").replace("null","");
        secondaryEmail = educationJson.optString("SecondaryEmail", "").replace("null","");
        primaryEmail =educationJson.optString("PrimaryEmail", "").replace("null","");

        dob = educationJson.optString("DOB", "").replace("null","");
        if(!dob.isEmpty()&&!dob.equals("")){
            Date date = GenericUtils.getDateFromString(dob);
            dobFormatted = GenericUtils.formatDate(date);
        }


        gender = educationJson.optString("GenderId", "").replace("null","");
        maritalStatus = educationJson.optString("MaritalStatusId", "").replace("null","");
        occupation = educationJson.optString("Occupation", "").replace("null","");
        primaryAddress = educationJson.optString("PrimaryAddress", "").replace("null","");
        secondaryAddress = educationJson.optString("SecondaryAddress", "").replace("null","");

        cityId = educationJson.optString("CityId", "").replace("null","");
        cityName = educationJson.optString("CityName", "").replace("null","");
        stateId = educationJson.optString("StateId", "").replace("null","");
        stateName = educationJson.optString("StateName", "").replace("null","");
        countryId = educationJson.optString("CountryId", "").replace("null","");
        countryName = educationJson.optString("CountryName", "").replace("null","");
        practiceCountryId = educationJson.optString("PracticeCountryId", "").replace("null","");
        practiceCountryName = educationJson.optString("PracticeCountryName", "").replace("null","");

        homePhone = educationJson.optString("HomePhone", "").replace("null","");
        officePhone = educationJson.optString("OfficePhone", "").replace("null","");
        personalPhone = educationJson.optString("PhoneNo", "").replace("null","");


        fax = educationJson.optString("Fax", "").replace("null","");

        introduction =GenericUtils.decodeHtml( educationJson.optString("ProfessionalSummary", "").replace("null",""));

    }

    public String getFullName() {
        return prefix + " " + firstName  + " " + lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getPrimaryEmail() {
        return primaryEmail;
    }

    public void setPrimaryEmail(String primaryEmail) {
        this.primaryEmail = primaryEmail;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondaryEmail() {
        return secondaryEmail;
    }

    public void setSecondaryEmail(String secondaryEmail) {
        this.secondaryEmail = secondaryEmail;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDobFormatted() {
        return dobFormatted;
    }

    public void setDobFormatted(String dobFormatted) {
        this.dobFormatted = dobFormatted;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getPrimaryAddress() {
        return primaryAddress;
    }

    public void setPrimaryAddress(String primaryAddress) {
        this.primaryAddress = primaryAddress;
    }

    public String getSecondaryAddress() {
        return secondaryAddress;
    }

    public void setSecondaryAddress(String secondaryAddress) {
        this.secondaryAddress = secondaryAddress;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getPracticeCountryId() {
        return practiceCountryId;
    }

    public void setPracticeCountryId(String practiceCountryId) {
        this.practiceCountryId = practiceCountryId;
    }

    public String getPracticeCountryName() {
        return practiceCountryName;
    }

    public void setPracticeCountryName(String practiceCountryName) {
        this.practiceCountryName = practiceCountryName;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getOfficePhone() {
        return officePhone;
    }

    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }

    public String getPersonalPhone() {
        return personalPhone;
    }

    public void setPersonalPhone(String personalPhone) {
        this.personalPhone = personalPhone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }
}
