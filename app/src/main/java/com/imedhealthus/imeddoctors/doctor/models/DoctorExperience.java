package com.imedhealthus.imeddoctors.doctor.models;

import org.json.JSONObject;

/**
 * Created by Malik Hamid on 1/26/2018.
 */

public class DoctorExperience {

            String id;
            String jobTitle;
            long startDate;
            long endDate;
            boolean IsCurrentlyWorking;
            String organizationName;
            String supervisorName;
            String address;
            int countryId;
            int stateId;
            int cityId;
            String contactNo;
            String fax;
            long dateCreated;
            long modifiedDate;
            String countryName;
            String stateName;
            String cityName,email;


    public DoctorExperience(JSONObject jsonObject){

            id = jsonObject.optString("Id", "");
            jobTitle=  jsonObject.optString(   "JobTitle","").replace("null","");
           // startDate = jsonObject.getLong("StartDate");
            //endDate = jsonObject.getLong("EndDate");
            IsCurrentlyWorking = jsonObject.optBoolean("IsCurrentlyWorking");
            organizationName = jsonObject.optString("OrganizationName","").replace("null","");
            supervisorName = jsonObject.optString("SupervisorName","").replace("null","");
            address = jsonObject.optString("Address","").replace("null","");
           // countryId = jsonObject.getInt("CountryId");
            //    jsonObject.optString("StateId","");
            //    jsonObject.optString("CityId","");
             contactNo = jsonObject.optString("ContactNo","").replace("null","");
            fax = jsonObject.optString("Fax","").replace("null","");
            //jsonObject.optString("DateCreated","");
            //jsonObject.optString("ModifiedDate","");
            cityName = jsonObject.optString("CountryName","").replace("null","");
            stateName = jsonObject.optString("StateName","").replace("null","");
            cityName = jsonObject.optString("CityName","").replace("null","");


    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public boolean isCurrentlyWorking() {
        return IsCurrentlyWorking;
    }

    public void setCurrentlyWorking(boolean currentlyWorking) {
        IsCurrentlyWorking = currentlyWorking;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getSupervisorName() {
        return supervisorName;
    }

    public void setSupervisorName(String supervisorName) {
        this.supervisorName = supervisorName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public long getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(long modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
