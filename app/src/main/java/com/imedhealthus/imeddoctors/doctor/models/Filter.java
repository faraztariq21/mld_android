package com.imedhealthus.imeddoctors.doctor.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class Filter {

    public static final int MinDistance = 1, MaxDistance = 50;
    public static final int MinFee = 5, MaxFee = 500;


    private boolean isOfficeAppointment,allAppointments;
    @SerializedName("AppointmentType")
    private String appointmentype;


    private String address;

    @SerializedName("SpecialityId")
    private String speciality;

    @SerializedName("Gender")
    private String gender;

    @SerializedName("Insurance")
    private String insurance;

    @SerializedName("Country")
    private String country;

    @SerializedName("City")
    private String city;

    @SerializedName("Latitude")
    private double latitude;

    @SerializedName("Longitude")
    private double longitude;
    @SerializedName("MaxConsultationFee")
    private int consultationFee;

    @SerializedName("MinConsultationFee")
    private int minConsultationFee;

    @SerializedName("MinimumRating")
    private int rating;

    @SerializedName("Radius")
    private int distance;

    @SerializedName("Language")
    private List<String> languages;

    public Filter() {
        this.isOfficeAppointment = false;
        this.allAppointments = true;
        this.appointmentype = "";
        this.address = "";
        this.speciality = "";
        this.city = "";
        this.country = "";
        this.insurance = "";
        this.gender = "";
        this.latitude = 0;
        this.longitude = 0;
        this.consultationFee = 500;
        this.rating = 0;
        this.distance = 50;
        this.minConsultationFee = 5;
        this.languages = new ArrayList<>();
    }


    public boolean isAllAppointments() {
        return allAppointments;
    }

    public void setAllAppointments(boolean allAppointments) {
        this.allAppointments = allAppointments;
    }

    public String getAppointmentype() {
        return appointmentype;
    }

    public void setAppointmentype(String appointmentype) {
        this.appointmentype = appointmentype;
    }

    public void setConsultationFee(int consultationFee) {
        this.consultationFee = consultationFee;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getMinConsultationFee() {
        return minConsultationFee;
    }

    public void setMinConsultationFee(int minConsultationFee) {
        this.minConsultationFee = minConsultationFee;
    }

    public boolean isOfficeAppointment() {
        return isOfficeAppointment;
    }

    public void setOfficeAppointment(boolean officeAppointment) {
        if(officeAppointment)
            appointmentype="office";
        else
            appointmentype="tele";
        isOfficeAppointment = officeAppointment;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getConsultationFee() {
        return consultationFee;
    }

    public void setConsultationFee(double consultationFee) {
        this.consultationFee = (int)consultationFee;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = (int)rating;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }
}
