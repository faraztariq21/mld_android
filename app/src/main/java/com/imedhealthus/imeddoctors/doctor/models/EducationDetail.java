package com.imedhealthus.imeddoctors.doctor.models;

import android.text.TextUtils;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class EducationDetail extends OverviewItem {

    private String id, educationType, degree, startDate, endDate, institute, majors, notes;
    private String cityId, cityName, stateId, stateName, countryId, countryName;
    private String startDateFormatted, endDateFormatted;
    private boolean isComplete;

    public EducationDetail() {
    }

    public EducationDetail(JSONObject educationJson) {

        id = educationJson.optString("Id", "");
        educationType = educationJson.optString("EducationTypeId", "").replace("null","");
        degree = educationJson.optString("DegreeId", "").replace("null","");

        startDate = educationJson.optString("StartDate", "");
        Date date = GenericUtils.getDateFromString(startDate);
        startDateFormatted = GenericUtils.formatDate(date);

        endDate = educationJson.optString("EndDate", "");
        date = GenericUtils.getDateFromString(endDate);
        endDateFormatted = GenericUtils.formatDate(date);

        institute = educationJson.optString("InstituteId", "").replace("null","");
        majors = educationJson.optString("Majors", "").replace("null","");
        notes = educationJson.optString("DegreeDescription", "").replace("null","");

        cityId = educationJson.optString("CityId", "").replace("null","");
        cityName = educationJson.optString("CityName", "").replace("null","");
        stateId = educationJson.optString("StateId", "").replace("null","");
        stateName = educationJson.optString("StateName", "").replace("null","");
        countryId = educationJson.optString("CountryId", "").replace("null","");
        countryName = educationJson.optString("CountryName", "").replace("null","");

        isComplete = educationJson.optBoolean("IsCompleted", false);

    }

    public static List<EducationDetail> parseEductionDetails(JSONArray educationDetailsJson){

        ArrayList<EducationDetail> educationDetails = new ArrayList<>();

        for (int i = 0; i < educationDetailsJson.length(); i++){

            JSONObject educationDetailJson = educationDetailsJson.optJSONObject(i);
            if (educationDetailJson != null) {
                EducationDetail educationDetail = new EducationDetail(educationDetailJson);
                educationDetails.add(educationDetail);
            }

        }

        return educationDetails;

    }


    @Override
    public String getDisplayTitle() {

        String displayTitle =  degree;
        if (!TextUtils.isEmpty(educationType)) {
            displayTitle += " (" + educationType + ")";
        }
        return displayTitle;

    }

    @Override
    public String getDisplayDetail() {

        String displayDetail = institute;

        String address = cityName;
        if (!TextUtils.isEmpty(stateName)) {
            if (TextUtils.isEmpty(address)) {
                address = stateName;
            }else {
                address += ", " + stateName;
            }
        }
        if (!TextUtils.isEmpty(countryName)) {
            if (TextUtils.isEmpty(address)) {
                address = countryName;
            }else {
                address += ", " + countryName;
            }
        }

        if (!TextUtils.isEmpty(address)) {
            displayDetail += " (" + address + ")";
        }
        return displayDetail;

    }

    @Override
    public String getDisplayDuration() {

        String displayDuration = startDateFormatted;
        if (!TextUtils.isEmpty(endDateFormatted)) {
            displayDuration += " - " + endDateFormatted;
        }
        return displayDuration;

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEducationType() {
        return educationType;
    }

    public void setEducationType(String educationType) {
        this.educationType = educationType;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getMajors() {
        return majors;
    }

    public void setMajors(String majors) {
        this.majors = majors;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStartDateFormatted() {
        return startDateFormatted;
    }

    public void setStartDateFormatted(String startDateFormatted) {
        this.startDateFormatted = startDateFormatted;
    }

    public String getEndDateFormatted() {
        return endDateFormatted;
    }

    public void setEndDateFormatted(String endDateFormatted) {
        this.endDateFormatted = endDateFormatted;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}
