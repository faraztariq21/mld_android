package com.imedhealthus.imeddoctors.doctor.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.BaseDialogFragment;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.models.Appointment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentAppointmentSlotDetail extends BaseDialogFragment {



    @BindView(R.id.tv_appointment_title)
    TextView tvAppointmentTitle;
    @BindView(R.id.tv_doctor)
    TextView tvDoctor;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_duration)
    TextView tvDuration;
    @BindView(R.id.tv_location)
    TextView tvLocation;
    @BindView(R.id.tv_location_heading)
    TextView tvLocationHeading;
    @BindView(R.id.tv_notes)
    TextView tvNotes;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.btn_appointment_slot)
    Button bAppointment;

    public static final String INDEX = "index";
    public static final String APPOINTMENT = "appointment";
    public static final String IS_PERSONAL_PROFILE = "is_personal_profile";
    Appointment appointment;
    int index;
    boolean isPersonalProfile;

    FragmentAppointmentSlotClickListener fragmentAppointmentSlotClickListener;

    public static FragmentAppointmentSlotDetail newInstance(Appointment appointment, int index, boolean isPersonalProfile) {
        FragmentAppointmentSlotDetail fragmentAppointmentSlotDetail = new FragmentAppointmentSlotDetail();
        Bundle bundle = new Bundle();
        bundle.putSerializable(APPOINTMENT, appointment);
        bundle.putInt(INDEX, index);
        bundle.putBoolean(IS_PERSONAL_PROFILE, isPersonalProfile);
        fragmentAppointmentSlotDetail.setArguments(bundle);
        return fragmentAppointmentSlotDetail;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            appointment = (Appointment) getArguments().getSerializable(APPOINTMENT);
            index = getArguments().getInt(INDEX);
            isPersonalProfile = getArguments().getBoolean(IS_PERSONAL_PROFILE);


        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentAppointmentSlotClickListener)
            fragmentAppointmentSlotClickListener = (FragmentAppointmentSlotClickListener) context;
    }

    public FragmentAppointmentSlotDetail() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_appointment_slot_detail, container, false);
        ButterKnife.bind(this, view);

        if (fragmentAppointmentSlotClickListener == null && getActivity() instanceof FragmentAppointmentSlotClickListener)
            fragmentAppointmentSlotClickListener = (FragmentAppointmentSlotClickListener) getActivity();

        if (fragmentAppointmentSlotClickListener == null && getParentFragment() instanceof FragmentAppointmentSlotClickListener)
            fragmentAppointmentSlotClickListener = (FragmentAppointmentSlotClickListener) getParentFragment();

        tvAppointmentTitle.setText("Appointment Slot - " + appointment.getType());
        if (appointment.getType() == Constants.AppointmentType.Tele) {
            tvLocation.setVisibility(View.GONE);
            tvLocationHeading.setVisibility(View.GONE);
        } else {
            tvLocation.setText(appointment.getPhysicalLocation());

        }


        tvDate.setText(appointment.getFormattedDate());
        tvTime.setText(appointment.getFormattedTime());
        tvNotes.setText(appointment.getNotes());

        if (isPersonalProfile) {
            bAppointment.setText(getResources().getString(R.string.delete_this_slot));
            bAppointment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentAppointmentSlotClickListener.onAppointmentCancelTapped(index);

                    FragmentAppointmentSlotDetail.this.dismiss();
                }
            });
        } else {
            bAppointment.setText(getResources().getString(R.string.book_your_appointment));
            bAppointment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentAppointmentSlotClickListener.onAppointmentBookTapped(index);

                    FragmentAppointmentSlotDetail.this.dismiss();
                }
            });
        }


        return view;
    }

    public interface FragmentAppointmentSlotClickListener {
        public void onAppointmentCancelTapped(int index);

        public void onAppointmentBookTapped(int index);
    }

}
