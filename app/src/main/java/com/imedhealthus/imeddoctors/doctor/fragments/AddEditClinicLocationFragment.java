package com.imedhealthus.imeddoctors.doctor.fragments;


import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.GooglePlaceArrayAdapter;
import com.imedhealthus.imeddoctors.common.fragments.MapBaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.GooglePlaceAutoComplete;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.common.utils.ImageUtils;
import com.imedhealthus.imeddoctors.doctor.models.ClinicLocation;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddEditClinicLocationFragment extends MapBaseFragment implements TimePickerDialog.OnTimeSetListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, AdapterView.OnItemClickListener, ResultCallback<PlaceBuffer>, TextWatcher {

    @BindView(R.id.et_address)
    AutoCompleteTextView etAddress;
    @BindView(R.id.et_radius)
    EditText etRadius;
    /*
        @BindView(R.id.tv_start_time)
        TextView tvStartTime;
        @BindView(R.id.tv_end_time)
        TextView tvEndTime;
    */
    private ClinicLocation clinicLocation;

    private GoogleApiClient googleApiClient;
    private GooglePlaceArrayAdapter adapterGooglePlaces;
    private Marker currentMarker;
    private Circle currentCircle;

    private LatLng selectedLatLng = new LatLng(0, 0);
    private double selectedRadius = 0;
    private String selectedAddress = "", selectedCity = "", selectedPostalCode = "", selectedState = "", selectedCountry = "";

    public AddEditClinicLocationFragment() {
        clinicLocation = SharedData.getInstance().getSelectedClinicLocation();
    }


    @Override
    public String getName() {
        return "AddEditClinicLocationFragment";
    }

    @Override
    public String getTitle() {
        if (clinicLocation == null) {
            return "Add Clinic Location";
        } else {
            return "Edit Clinic Location";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_clinic_location, container, false);
        ButterKnife.bind(this, view);

        initHelper();
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }


    @Override
    protected void initHelper() {

        super.initHelper();

        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Places.GEO_DATA_API)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .build();

        etAddress.setThreshold(3);
        etAddress.setOnItemClickListener(this);

        LatLngBounds bounds = new LatLngBounds(new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
        adapterGooglePlaces = new GooglePlaceArrayAdapter(context, android.R.layout.simple_list_item_1, bounds, null);
        etAddress.setAdapter(adapterGooglePlaces);

        etRadius.addTextChangedListener(this);

    }

    private void setData() {

        if (clinicLocation == null) {
            return;
        }

        selectedLatLng = new LatLng(clinicLocation.getLatitude(), clinicLocation.getLongitude());
        selectedRadius = clinicLocation.getRadius();

        selectedAddress = clinicLocation.getAddress();
        selectedCity = clinicLocation.getCityName();
        selectedPostalCode = clinicLocation.getPostalCode();
        selectedState = clinicLocation.getStateName();
        selectedCountry = clinicLocation.getCountryName();

        etAddress.setText(clinicLocation.getFullAddress());
        etRadius.setText(String.valueOf(clinicLocation.getRadius()));
/*
        tvStartTime.setText(clinicLocation.getStartTimeFormatted());
        tvEndTime.setText(clinicLocation.getEndTimeFormatted());
*/
    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditClinicLocation();
                }
                break;
/*
            case R.id.tv_start_time:
                openDatePicker(tvStartTime.getText().toString(), "startTime");
                break;

            case R.id.tv_end_time:
                openDatePicker(tvEndTime.getText().toString(), "endTime");
                break;
*/
        }
    }


    private void openDatePicker(String timeStr, String tag) {

        Date date;
        if (timeStr.isEmpty()) {
            date = new Date();
        } else {
            date = GenericUtils.unFormatTime(timeStr);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(this, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.YEAR), true);
        timePickerDialog.setVersion(TimePickerDialog.Version.VERSION_2);
        timePickerDialog.setAccentColor(getResources().getColor(R.color.light_blue));

        timePickerDialog.show(((Activity) context).getFragmentManager(), tag);

    }


    //Time picker callback
    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);
/*
        if(view.getTag().equalsIgnoreCase("startTime")){
            tvStartTime.setText(GenericUtils.formatTime(calendar.getTime()));
        }else{
            tvEndTime.setText(GenericUtils.formatTime(calendar.getTime()));
        }
*/
    }


    //Overlay fragment - Not used here
    @Override
    public void setData(Object data) {

    }

    @Override
    public Object getData() {
        return null;
    }


    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etAddress.getText().toString().isEmpty() || etRadius.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustClinicLocations(RetrofitJSONResponse response, ClinicLocation updatedClinicLocation) {

        if (clinicLocation == null) {//Add

            JSONObject clinicLocationJson = response.optJSONObject("DoctorLocation");
            if (clinicLocationJson != null) {
                ClinicLocation newClinicLocation = new ClinicLocation(clinicLocationJson);
                DoctorProfile profile = SharedData.getInstance().getSelectedDoctorProfile();
                profile.getClinicLocations().add(newClinicLocation);
            }

        } else {

            clinicLocation.setAddress(updatedClinicLocation.getAddress());
            clinicLocation.setPostalCode(updatedClinicLocation.getPostalCode());

            clinicLocation.setCityName(updatedClinicLocation.getCityName());
            clinicLocation.setStateName(updatedClinicLocation.getStateName());
            clinicLocation.setCountryName(updatedClinicLocation.getCountryName());
/*
            clinicLocation.setStartTime(updatedClinicLocation.getStartTime());
            clinicLocation.setStartTimeFormatted(updatedClinicLocation.getStartTimeFormatted());

            clinicLocation.setEndTime(updatedClinicLocation.getEndTime());
            clinicLocation.setEndTimeFormatted(updatedClinicLocation.getEndTimeFormatted());
*/
            clinicLocation.setRadius(updatedClinicLocation.getRadius());
            clinicLocation.setLatitude(updatedClinicLocation.getLatitude());
            clinicLocation.setLongitude(updatedClinicLocation.getLongitude());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Maps
    @Override
    public void onResume() {
        super.onResume();
        googleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        googleApiClient.disconnect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("iMed", "googleApiClient - onConnectionFailed");
    }

    //Google api client connection
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        adapterGooglePlaces.setGoogleApiClient(googleApiClient);
    }

    @Override
    public void onConnectionSuspended(int i) {
        adapterGooglePlaces.setGoogleApiClient(null);
        Log.e("iMed", "googleApiClient - onConnectionSuspended");
    }

    //Item selection (auto complete)
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

        GooglePlaceAutoComplete item = adapterGooglePlaces.getItem(position);
        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(googleApiClient, String.valueOf(item.getPlaceId()));
        placeResult.setResultCallback(this);

        GenericUtils.hideKeyboard(context, etAddress);

    }

    //Places result
    @Override
    public void onResult(@NonNull PlaceBuffer places) {

        if (!places.getStatus().isSuccess() || places.getCount() == 0) {
            Log.e("iMed", "Place query did not complete. Error: " + places.getStatus().toString());
            return;
        }

        Place place = places.get(0);
        addMarker(place.getLatLng(), "Selected location");
        reverseGeocode(place.getLatLng(), false);

    }

    //User location
    @Override
    public void locationReceived(LatLng latLng) {
        if (currentMarker == null) {
            reverseGeocode(latLng, true);
            addMarker(latLng, "Selected location");
        }
    }

    public void reverseGeocode(LatLng latLng, boolean shouldSetField) {
        new ReverseGeocodeTask(latLng, shouldSetField).execute();
    }

    private void addMarker(LatLng latLng, String title) {

        if (currentMarker != null) {
            currentMarker.remove();
        }
        if (currentCircle != null) {
            currentCircle.remove();
        }

        CircleOptions circleOptions = new CircleOptions()
                .center(latLng)
                .radius(selectedRadius)
                .fillColor(context.getResources().getColor(R.color.map_overlay_fill))
                .strokeColor(context.getResources().getColor(R.color.map_overlay_border));

        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng).title(title)
                .icon(ImageUtils.getBitmapDescriptor(context, R.drawable.ic_marker));

        selectedLatLng = latLng;

        currentMarker = googleMap.addMarker(markerOptions);
        currentCircle = googleMap.addCircle(circleOptions);

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

    }

    //Text watcher
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

        try {
            selectedRadius = Integer.parseInt(etRadius.getText().toString());
            addMarker(selectedLatLng, "Selected location");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    //Webservices
    private void addEditClinicLocation() {

        AlertUtils.showProgress(getActivity());

        final ClinicLocation updatedClinicLocation = new ClinicLocation();
        if (clinicLocation != null) {
            updatedClinicLocation.setId(clinicLocation.getId());
        }

        updatedClinicLocation.setAddress(selectedAddress);
        updatedClinicLocation.setPostalCode(selectedPostalCode);
        updatedClinicLocation.setCityName(selectedCity);
        updatedClinicLocation.setStateName(selectedState);
        updatedClinicLocation.setCountryName(selectedCountry);

        updatedClinicLocation.setRadius(selectedRadius);
        updatedClinicLocation.setLatitude(selectedLatLng.latitude);
        updatedClinicLocation.setLongitude(selectedLatLng.longitude);
/*
        updatedClinicLocation.setStartTimeFormatted(tvStartTime.getText().toString());
        Date date = GenericUtils.unFormatTime(tvStartTime.getText().toString());
        updatedClinicLocation.setStartTime(GenericUtils.getTimeDateString(date));

        updatedClinicLocation.setEndTimeFormatted(tvEndTime.getText().toString());
        date = GenericUtils.unFormatTime(tvEndTime.getText().toString());
        updatedClinicLocation.setEndTime(GenericUtils.getTimeDateString(date));
*/
        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditClinicLocation(userId, updatedClinicLocation, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                adjustClinicLocations(response, updatedClinicLocation);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Clinic location saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }


    //Reverser geocode
    private class ReverseGeocodeTask extends AsyncTask<Object, Object, Object> {

        private LatLng latLng;
        private boolean shouldSetField;

        public ReverseGeocodeTask(LatLng latLng, boolean shouldSetField) {
            this.latLng = latLng;
            this.shouldSetField = shouldSetField;
        }

        @Override
        protected Object doInBackground(Object[] objects) {

            Geocoder geocoder = new Geocoder(context, Locale.getDefault());

            try {
                List<Address> placeAddresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);

                if (placeAddresses.isEmpty()) {
                    return null;
                }

                Address placeAddress = placeAddresses.get(0);

                selectedAddress = placeAddress.getAddressLine(0);
                selectedCity = placeAddress.getLocality();
                selectedPostalCode = placeAddress.getPostalCode();
                selectedState = placeAddress.getAdminArea();
                selectedCountry = placeAddress.getCountryName();

            } catch (Exception ex) {
                ex.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (shouldSetField) {
                etAddress.setText(selectedAddress);
            }
        }

    }
}
