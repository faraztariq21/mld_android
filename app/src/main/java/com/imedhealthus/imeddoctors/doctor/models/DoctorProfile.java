package com.imedhealthus.imeddoctors.doctor.models;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class DoctorProfile {

    private PersonalInfo personalInfo = new PersonalInfo();

    private List<EducationDetail> educationDetails = new ArrayList<>();
    private List<Speciality> specialities = new ArrayList<>();
    private List<SkillTraining> skillTrainings = new ArrayList<>();
    private List<WorkExperience> workExperiences = new ArrayList<>();
    private List<ClinicLocation> clinicLocations = new ArrayList<>();
    private List<ReferenceDetail> referenceDetails = new ArrayList<>();
    private List<AcceptedInsurance> acceptedInsurances = new ArrayList<>();
    private List<Language> languages = new ArrayList<>();
    private List<SocialLink> socialLinks = new ArrayList<>();
    private List<TrainingProviderDetail> trainingProviderDetails = new ArrayList<>();
    private List<Certification> certifications = new ArrayList<>();
    private List<Membership> memberships = new ArrayList<>();
    private List<Award> awards = new ArrayList<>();

    private List<Appointment> appointments = new ArrayList<>();
    private List<Service> services = new ArrayList<>();

    private int feedbackLikePercent, feedbackDislikePercent;
    private double feedbackOverallRating;
    private List<Feedback> feedbacks = new ArrayList<>();

    public DoctorProfile() {
    }

    public DoctorProfile(JSONObject jsonObject){

        JSONObject doctorJson = jsonObject.optJSONObject("DoctorProfile");
        if (doctorJson == null) {
            return;
        }

        JSONObject personalInfoJson = doctorJson.optJSONObject("DoctorProfileDetail");
        if (personalInfoJson != null) {
            personalInfo = new PersonalInfo(personalInfoJson);
        }

        JSONArray arrJson = doctorJson.optJSONArray("DoctorEducationDetail");
        if (arrJson != null) {
            educationDetails = EducationDetail.parseEductionDetails(arrJson);
        }

        arrJson = doctorJson.optJSONArray("DoctorSpecialityDetail");
        if (arrJson != null) {
            specialities = Speciality.parseSpecialities(arrJson);
        }

        arrJson = doctorJson.optJSONArray("DoctorSkillDetail");
        if (arrJson != null) {
            skillTrainings = SkillTraining.parseSkillTrainings(arrJson);
        }

        arrJson = doctorJson.optJSONArray("DoctorExperienceDetail");
        if (arrJson != null) {
            workExperiences = WorkExperience.parseWorkExperiences(arrJson);
        }

        arrJson = doctorJson.optJSONArray("DoctorLocationDetail");
        if (arrJson != null) {
            clinicLocations = ClinicLocation.parseClinicLocations(arrJson);
        }

        arrJson = doctorJson.optJSONArray("DoctorReferenceDetail");
        if (arrJson != null) {
            referenceDetails = ReferenceDetail.parseReferenceDetails(arrJson);
        }

        arrJson = doctorJson.optJSONArray("DoctorInsuranceDetail");
        if (arrJson != null) {
            acceptedInsurances = AcceptedInsurance.parseInsurances(arrJson);
        }

        arrJson = doctorJson.optJSONArray("DoctorLanguageDetail");
        if (arrJson != null) {
            languages = Language.parseLanguages(arrJson);
        }

        arrJson = doctorJson.optJSONArray("DoctorSocialLinkDetail");
        if (arrJson != null) {
            socialLinks = SocialLink.parseSocialLinks(arrJson);
        }

        arrJson = doctorJson.optJSONArray("DoctorTrainingProviderDetail");
        if (arrJson != null) {
            trainingProviderDetails = TrainingProviderDetail.parseTrainingProviderDetails(arrJson);
        }

        arrJson = doctorJson.optJSONArray("DoctorRegistrationDetail");
        if (arrJson != null) {
            certifications = Certification.parseCertifications(arrJson);
        }

        arrJson = doctorJson.optJSONArray("DoctorMembershipDetail");
        if (arrJson != null) {
            memberships = Membership.parseMemberships(arrJson);
        }

        arrJson = doctorJson.optJSONArray("DoctorAwardDetail");
        if (arrJson != null) {
            awards = Award.parseAwards(arrJson);
        }

        arrJson = doctorJson.optJSONArray("DoctorAvailableAppointmentDetail");
        if (arrJson != null) {
            appointments = Appointment.parseAppointments(arrJson);
        }

        arrJson = doctorJson.optJSONArray("DoctorServicesDetail");
        if (arrJson != null) {
            services = Service.parseServices(arrJson);
        }

        JSONObject feedbackJson = doctorJson.optJSONObject("DoctorFeedback");
        if (feedbackJson != null) {

            feedbackLikePercent = feedbackJson.optInt("LikePercentage", 0);
            feedbackDislikePercent = feedbackJson.optInt("DislikePercentage", 0);
            feedbackOverallRating = feedbackJson.optDouble("OverallRating", 0.0);

            arrJson = feedbackJson.optJSONArray("Feedbacks");
            if (arrJson != null) {
                feedbacks = Feedback.parseFeedbacks(arrJson);
            }

        }

        adjustData();

    }

    public void adjustData() {

        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d");
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");

        for (Appointment appointment : appointments) {
            appointment.adjustData(dateFormat, timeFormat, currentDate);
        }

        for (Feedback feedback : feedbacks) {
            feedback.setTimeAgo(GenericUtils.getPassedTime(feedback.getTimeStamp()));
        }

    }


    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }

    public List<EducationDetail> getEducationDetails() {
        return educationDetails;
    }

    public void setEducationDetails(List<EducationDetail> educationDetails) {
        this.educationDetails = educationDetails;
    }

    public List<Speciality> getSpecialities() {
        return specialities;
    }

    public void setSpecialities(List<Speciality> specialities) {
        this.specialities = specialities;
    }

    public List<SkillTraining> getSkillTrainings() {
        return skillTrainings;
    }

    public void setSkillTrainings(List<SkillTraining> skillTrainings) {
        this.skillTrainings = skillTrainings;
    }

    public List<WorkExperience> getWorkExperiences() {
        return workExperiences;
    }

    public void setWorkExperiences(List<WorkExperience> workExperiences) {
        this.workExperiences = workExperiences;
    }

    public List<ClinicLocation> getClinicLocations() {
        return clinicLocations;
    }

    public void setClinicLocations(List<ClinicLocation> clinicLocations) {
        this.clinicLocations = clinicLocations;
    }

    public List<ReferenceDetail> getReferenceDetails() {
        return referenceDetails;
    }

    public void setReferenceDetails(List<ReferenceDetail> referenceDetails) {
        this.referenceDetails = referenceDetails;
    }

    public List<AcceptedInsurance> getAcceptedInsurances() {
        return acceptedInsurances;
    }

    public void setAcceptedInsurances(List<AcceptedInsurance> acceptedInsurances) {
        this.acceptedInsurances = acceptedInsurances;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public List<SocialLink> getSocialLinks() {
        return socialLinks;
    }

    public void setSocialLinks(List<SocialLink> socialLinks) {
        this.socialLinks = socialLinks;
    }

    public List<TrainingProviderDetail> getTrainingProviderDetails() {
        return trainingProviderDetails;
    }

    public void setTrainingProviderDetails(List<TrainingProviderDetail> trainingProviderDetails) {
        this.trainingProviderDetails = trainingProviderDetails;
    }

    public List<Certification> getCertifications() {
        return certifications;
    }

    public void setCertifications(List<Certification> certifications) {
        this.certifications = certifications;
    }

    public List<Membership> getMemberships() {
        return memberships;
    }

    public void setMemberships(List<Membership> memberships) {
        this.memberships = memberships;
    }

    public List<Award> getAwards() {
        return awards;
    }

    public void setAwards(List<Award> awards) {
        this.awards = awards;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public int getFeedbackLikePercent() {
        return feedbackLikePercent;
    }

    public void setFeedbackLikePercent(int feedbackLikePercent) {
        this.feedbackLikePercent = feedbackLikePercent;
    }

    public int getFeedbackDislikePercent() {
        return feedbackDislikePercent;
    }

    public void setFeedbackDislikePercent(int feedbackDislikePercent) {
        this.feedbackDislikePercent = feedbackDislikePercent;
    }

    public double getFeedbackOverallRating() {
        return feedbackOverallRating;
    }

    public void setFeedbackOverallRating(double feedbackOverallRating) {
        this.feedbackOverallRating = feedbackOverallRating;
    }

    public List<Feedback> getFeedbacks() {
        return feedbacks;
    }

    public void setFeedbacks(List<Feedback> feedbacks) {
        this.feedbacks = feedbacks;
    }
}
