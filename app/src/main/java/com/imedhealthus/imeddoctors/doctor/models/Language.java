package com.imedhealthus.imeddoctors.doctor.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class Language extends OverviewItem {

    private String id, name;

    public Language() {
    }

    public Language(JSONObject educationJson) {

        id = educationJson.optString("Id", "");
        name = educationJson.optString("LanguageId", "");

    }

    public static List<Language> parseLanguages(JSONArray languagesJson){

        ArrayList<Language> languages = new ArrayList<>();

        for (int i = 0; i < languagesJson.length(); i++){

            JSONObject languageJson = languagesJson.optJSONObject(i);
            if (languageJson != null) {
                Language language = new Language(languageJson);
                languages.add(language);
            }

        }

        return languages;

    }


    @Override
    public String getDisplayTitle() {

        String displayTitle =  name;
        return displayTitle;

    }

    @Override
    public String getDisplayDetail() {

        return "";

    }

    @Override
    public String getDisplayDuration() {

        return "";

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
