package com.imedhealthus.imeddoctors.doctor.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.listeners.OnArchivedAppointmentItemClickListener;
import com.imedhealthus.imeddoctors.doctor.models.ArchivedAppointment;
import com.imedhealthus.imeddoctors.doctor.view_holders.ArchivedAppointmentItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Malik Hamid on 2/21/2018.
 */

public class ArchivedAppointmentsListAdapter extends RecyclerView.Adapter<ArchivedAppointmentItemViewHolder> {

    private List<ArchivedAppointment> appointments;
    private WeakReference<TextView> tvNoRecords;
    private WeakReference<OnArchivedAppointmentItemClickListener> onBookedAppointmentItemClickListener;
    private int expandedIdx;

    public ArchivedAppointmentsListAdapter(TextView tvNoRecords,OnArchivedAppointmentItemClickListener onArchivedAppointmentItemClickListener) {

        super();
        this.tvNoRecords = new WeakReference<>(tvNoRecords);
        this.onBookedAppointmentItemClickListener = new WeakReference<>(onArchivedAppointmentItemClickListener);
        expandedIdx = -1;

    }

    public void setAppointments(List<ArchivedAppointment> appointments) {

        this.appointments = appointments;
        expandedIdx = -1;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }

    public void toggleExpandAt(int idx) {

        if(CacheManager.getInstance().getCurrentUser().getUserType()== Constants.UserType.Patient)
            return;
        if (idx == expandedIdx) {
            expandedIdx = -1;
        }else {
            expandedIdx = idx;
        }
        notifyDataSetChanged();

    }

    @Override
    public ArchivedAppointmentItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_archived_appointment, parent, false);

        OnArchivedAppointmentItemClickListener listener = null;
        if (onBookedAppointmentItemClickListener != null && onBookedAppointmentItemClickListener.get() != null) {
            listener = onBookedAppointmentItemClickListener.get();
        }

        return new ArchivedAppointmentItemViewHolder(view, listener);

    }

    @Override
    public void onBindViewHolder(ArchivedAppointmentItemViewHolder viewHolder, int position) {
        viewHolder.setData(getItem(position), position == expandedIdx, position);
    }

    @Override
    public int getItemCount() {
        return appointments == null ? 0 : appointments.size();
    }

    public ArchivedAppointment getItem(int position) {
        if (position < 0 || position >= appointments.size()) {
            return null;
        }
        return appointments.get(position);
    }

}