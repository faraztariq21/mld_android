package com.imedhealthus.imeddoctors.doctor.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.CustomCitySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomCountrySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomStateSpinnerAdapter;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.City;
import com.imedhealthus.imeddoctors.common.models.Country;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.State;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.imedhealthus.imeddoctors.doctor.models.SkillTraining;
import com.imedhealthus.imeddoctors.doctor.models.WorkExperience;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddEditSkillTrainingFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.et_education_type)
    EditText etEducationType;
    @BindView(R.id.et_skill)
    EditText etSkill;

    @BindView(R.id.tv_start_date)
    TextView tvStartDate;
    @BindView(R.id.tv_end_date)
    TextView tvEndDate;

    @BindView(R.id.et_institute)
    EditText etInstitute;
    @BindView(R.id.et_supervisor)
    EditText etSupervisor;

    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_address)
    EditText etAddress;

    @BindView(R.id.sp_city)
    Spinner spCity;
    @BindView(R.id.sp_state)
    Spinner spState;
    @BindView(R.id.sp_country)
    Spinner spCountry;

    private WorkExperience workExperience;
    private List<Country> countries;
    private CustomCountrySpinnerAdapter customCountryAdapter;
    private CustomStateSpinnerAdapter stateAdapter;
    private List<State> states;
    private List<City> cities;
    private CustomCitySpinnerAdapter cityAdapter;

    @BindView(R.id.et_notes)
    EditText etNotes;

    private SkillTraining skillTraining;

    public AddEditSkillTrainingFragment() {
        skillTraining = SharedData.getInstance().getSelectedSkillTraining();
    }


    @Override
    public String getName() {
        return "AddEditSkillTrainingFragment";
    }

    @Override
    public String getTitle() {
        if (skillTraining == null) {
            return "Add Skill Training";
        }else {
            return "Edit Skill Training";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_skill_training, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    private void setData() {

        if (skillTraining == null) {
            return;
        }

        etEducationType.setText(skillTraining.getEducationType());
        etSkill.setText(skillTraining.getSkill());

        tvStartDate.setText(skillTraining.getStartDateFormatted());
        tvEndDate.setText(skillTraining.getEndDateFormatted());

        etInstitute.setText(skillTraining.getInstitute());
        etSupervisor.setText(skillTraining.getSupervisor());

        etPhone.setText(skillTraining.getPhone());
        etAddress.setText(skillTraining.getAddress());


        setSpinnerData(skillTraining.getCountryId(),skillTraining.getStateId(),skillTraining.getCityId());
        etNotes.setText(skillTraining.getNotes());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditSkillTraining();
                }
                break;

            case R.id.tv_start_date:
                openDatePicker(tvStartDate.getText().toString(), "startDate");
                break;

            case R.id.tv_end_date:
                openDatePicker(tvEndDate.getText().toString(), "endDate");
                break;

        }
    }


    private void openDatePicker(String dateStr, String tag) {

        Date date;
        if (dateStr.isEmpty()) {
            date = new Date();
        }else {
            date = GenericUtils.unFormatDate(dateStr);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setVersion(DatePickerDialog.Version.VERSION_2);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.light_blue));

        datePickerDialog.show(((Activity)context).getFragmentManager(), tag);

    }


    //Date picker callback
    @Override
    public void onDateSet(DatePickerDialog view, int year, int month, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        if(view.getTag().equalsIgnoreCase("startDate")){
            tvStartDate.setText(GenericUtils.formatDate(calendar.getTime()));
        }else{
            tvEndDate.setText(GenericUtils.formatDate(calendar.getTime()));
        }

    }


    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etEducationType.getText().toString().isEmpty() || etSkill.getText().toString().isEmpty() ||
                tvStartDate.getText().toString().isEmpty() || tvEndDate.getText().toString().isEmpty() ||
                etInstitute.getText().toString().isEmpty() || etSupervisor.getText().toString().isEmpty() ||
                etPhone.getText().toString().isEmpty() || etAddress.getText().toString().isEmpty() || etNotes.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustSkillTrainings(RetrofitJSONResponse response, SkillTraining updatedSkillTraining) {

        if (skillTraining == null) {//Add

            JSONObject skillTrainingJson = response.optJSONObject("DoctorSkill");
            if (skillTrainingJson != null) {
                SkillTraining newSkillTraining = new SkillTraining(skillTrainingJson);
                DoctorProfile profile = SharedData.getInstance().getSelectedDoctorProfile();
                profile.getSkillTrainings().add(newSkillTraining);
            }

        }else  {

            skillTraining.setEducationType(updatedSkillTraining.getEducationType());
            skillTraining.setSkill(updatedSkillTraining.getSkill());

            skillTraining.setStartDate(updatedSkillTraining.getStartDate());
            skillTraining.setStartDateFormatted(updatedSkillTraining.getStartDateFormatted());
            skillTraining.setEndDate(updatedSkillTraining.getEndDate());
            skillTraining.setEndDateFormatted(updatedSkillTraining.getEndDateFormatted());

            skillTraining.setInstitute(updatedSkillTraining.getInstitute());
            skillTraining.setSupervisor(updatedSkillTraining.getSupervisor());

            skillTraining.setPhone(updatedSkillTraining.getPhone());
            skillTraining.setAddress(updatedSkillTraining.getAddress());

            skillTraining.setCityId(updatedSkillTraining.getCityId());
            skillTraining.setCityName(updatedSkillTraining.getCityName());
            skillTraining.setStateId(updatedSkillTraining.getStateId());
            skillTraining.setStateName(updatedSkillTraining.getStateName());
            skillTraining.setCountryId(updatedSkillTraining.getCountryId());
            skillTraining.setCountryName(updatedSkillTraining.getCountryName());

            skillTraining.setNotes(updatedSkillTraining.getNotes());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }

    public void setSpinnerData(final String countryId,final String stateId,final String cityId){

        countries = SharedData.getInstance().getCountriesList();
        customCountryAdapter = new CustomCountrySpinnerAdapter(getContext(), R.layout.custom_spinner_item,countries);
        spCountry.setAdapter(customCountryAdapter);
        int selectedCountryIndex = SharedData.getInstance().getCountryIndexInList(countries,countryId);
        spCountry.setSelection(selectedCountryIndex);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                states = SharedData.getInstance().
                        getStatesListAgainstCountryByCountryId(countryId);
                stateAdapter = new CustomStateSpinnerAdapter(getContext(), R.layout.custom_spinner_item, states);
                spState.setAdapter(stateAdapter);
                int selectedStateIndex =SharedData.getInstance().getStateIndexInList(states,stateId);
                spState.setSelection(selectedStateIndex);


                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cities = SharedData.getInstance().
                                getCitiesListByStateId(stateId);
                        cityAdapter = new CustomCitySpinnerAdapter(getContext(), R.layout.custom_spinner_item, cities);
                        spCity.setAdapter(cityAdapter);
                        int selectedCityIndex =SharedData.getInstance().getCityIndexInList(cities,cityId);
                        spCity.setSelection(selectedCityIndex);
                    }
                }, 100);
            }
        }, 100);


    }


    private void initView() {

        countries = SharedData.getInstance().getCountriesList();
        customCountryAdapter = new CustomCountrySpinnerAdapter(getContext(), R.layout.custom_spinner_item,countries);
        spCountry.setAdapter(customCountryAdapter);

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                states = SharedData.getInstance().
                        getStatesListAgainstCountryByCountryId(Integer.toString(((Country)adapterView.getSelectedItem()).countryId));
                stateAdapter = new CustomStateSpinnerAdapter(getContext(), R.layout.custom_spinner_item, states);
                spState.setAdapter(stateAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                cities = SharedData.getInstance().
                        getCitiesListByStateId(((State)adapterView.getItemAtPosition(i)).getStateId());
                cityAdapter = new CustomCitySpinnerAdapter(getContext(), R.layout.custom_spinner_item, cities);
                spCity.setAdapter(cityAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }
    //Webservices
    private void addEditSkillTraining() {

        AlertUtils.showProgress(getActivity());

        final SkillTraining updatedSkillTraining = new SkillTraining();
        if (skillTraining != null) {
            updatedSkillTraining.setId(skillTraining.getId());
        }

        updatedSkillTraining.setEducationType(etEducationType.getText().toString());
        updatedSkillTraining.setSkill(etSkill.getText().toString());

        updatedSkillTraining.setStartDateFormatted(tvStartDate.getText().toString());
        Date date = GenericUtils.unFormatDate(tvStartDate.getText().toString());
        updatedSkillTraining.setStartDate(GenericUtils.getTimeDateString(date));

        updatedSkillTraining.setEndDateFormatted(tvEndDate.getText().toString());
        date = GenericUtils.unFormatDate(tvEndDate.getText().toString());
        updatedSkillTraining.setEndDate(GenericUtils.getTimeDateString(date));

        updatedSkillTraining.setInstitute(etInstitute.getText().toString());
        updatedSkillTraining.setSupervisor(etSupervisor.getText().toString());

        updatedSkillTraining.setPhone(etPhone.getText().toString());
        updatedSkillTraining.setAddress(etAddress.getText().toString());

        if(spState.getSelectedItem() == null || ((State) spState.getSelectedItem()).getStateId().equals("0")) {
            updatedSkillTraining.setStateId("0");
            updatedSkillTraining.setStateName("");
        }else {
            updatedSkillTraining.setStateId(((State) spState.getSelectedItem()).getStateId());
            updatedSkillTraining.setStateName(((State) spState.getSelectedItem()).getStateName());
        }

        if(spCountry.getSelectedItem() == null || Integer.toString(((Country) spCountry.getSelectedItem()).countryId).equals("0")) {
            updatedSkillTraining.setCountryId("0");
            updatedSkillTraining.setCountryName("");
        }else {
            updatedSkillTraining.setCountryId(Integer.toString(((Country) spCountry.getSelectedItem()).countryId));
            updatedSkillTraining.setCountryName(((Country) spCountry.getSelectedItem()).getCountryName());
        }

        if(spCity.getSelectedItem() == null || ((City) spCity.getSelectedItem()).getCityId().equals("0")) {
            updatedSkillTraining.setCityId("0");
            updatedSkillTraining.setCityName("");
        }else {
            updatedSkillTraining.setCityId(((City) spCity.getSelectedItem()).getCityId());
            updatedSkillTraining.setCityName(((City) spCity.getSelectedItem()).getCityName());
        }

        updatedSkillTraining.setNotes(etNotes.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditSkillTraining(userId, updatedSkillTraining, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                adjustSkillTrainings(response, updatedSkillTraining);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Skill training saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
