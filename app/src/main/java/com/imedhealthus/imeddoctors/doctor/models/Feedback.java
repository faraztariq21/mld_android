package com.imedhealthus.imeddoctors.doctor.models;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 1/7/18.
 */

public class Feedback {

    private String id,userImageUrl, userFullName, comment, timeAgo;
    private long timeStamp;
    private double rating;
    private boolean hasUserLiked;

    public Feedback() {
    }
    public Feedback(JSONObject jsonObject) {
        try {
            id = jsonObject.optString("Id", "");
            userFullName = jsonObject.optString("FullName", "");
            comment = jsonObject.optString("Comment", "");
            rating = jsonObject.getDouble("Rating");
            hasUserLiked = jsonObject.getBoolean("HasUserLiked");
            timeStamp = GenericUtils.getTimeDateInLong(jsonObject.optString("TimeStamp", ""));
            userImageUrl = GenericUtils.getImageUrl(jsonObject.optString("UserImageUrl",""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static List<Feedback> parseFeedbacks(JSONArray feedbacksJson){

        ArrayList<Feedback> feedbacks = new ArrayList<>();

        for (int i = 0; i < feedbacksJson.length(); i++){

            JSONObject feedbackJson = feedbacksJson.optJSONObject(i);
            if (feedbackJson != null) {
                Feedback feedback = new Feedback(feedbackJson);
                feedbacks.add(feedback);
            }

        }

        return feedbacks;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isHasUserLiked() {
        return hasUserLiked;
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public void setUserImageUrl(String userImageUrl) {
        this.userImageUrl = userImageUrl;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(String timeAgo) {
        this.timeAgo = timeAgo;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public boolean hasUserLiked() {
        return hasUserLiked;
    }

    public void setHasUserLiked(boolean hasUserLiked) {
        this.hasUserLiked = hasUserLiked;
    }
}
