package com.imedhealthus.imeddoctors.doctor.models;

import android.text.TextUtils;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class TrainingProviderDetail extends OverviewItem {

    private String id, type, title, institute, department, supervisor, notes;
    private String phone, address, cityId, cityName, stateId, stateName, countryId, countryName;
    private String startDate, endDate, startDateFormatted, endDateFormatted;

    public TrainingProviderDetail() {
    }

    public TrainingProviderDetail(JSONObject educationJson) {

        id = educationJson.optString("Id", "");
        type = educationJson.optString("TrainingTypeId", "").replace("null","");
        title = educationJson.optString("TrainingTitle", "").replace("null","");

        startDate = educationJson.optString("StartDate", "");
        Date date = GenericUtils.getDateFromString(startDate);
        startDateFormatted = GenericUtils.formatDate(date);

        endDate = educationJson.optString("EndDate", "");
        date = GenericUtils.getDateFromString(endDate);
        endDateFormatted = GenericUtils.formatDate(date);

        institute = educationJson.optString("InstituteId", "").replace("null","");
        department = educationJson.optString("Department", "").replace("null","");
        supervisor = educationJson.optString("Supervisor", "").replace("null","");
        notes = educationJson.optString("Description", "").replace("null","");

        phone = educationJson.optString("PhoneNo", "").replace("null","");
        address = educationJson.optString("Address", "").replace("null","");

        cityId = educationJson.optString("CityId", "").replace("null","");
        cityName = educationJson.optString("CityName", "").replace("null","");
        stateId = educationJson.optString("StateId", "").replace("null","");
        stateName = educationJson.optString("StateName", "").replace("null","");
        countryId = educationJson.optString("CountryId", "").replace("null","");
        countryName = educationJson.optString("CountryName", "").replace("null","");

    }

    public static List<TrainingProviderDetail> parseTrainingProviderDetails(JSONArray trainingProviderDetailsJson){

        ArrayList<TrainingProviderDetail> trainingProviderDetails = new ArrayList<>();

        for (int i = 0; i < trainingProviderDetailsJson.length(); i++){

            JSONObject trainingProviderDetailJson = trainingProviderDetailsJson.optJSONObject(i);
            if (trainingProviderDetailJson != null) {
                TrainingProviderDetail trainingProviderDetail = new TrainingProviderDetail(trainingProviderDetailJson);
                trainingProviderDetails.add(trainingProviderDetail);
            }

        }

        return trainingProviderDetails;

    }


    @Override
    public String getDisplayTitle() {

        String displayTitle =  title;
        if (!TextUtils.isEmpty(type)) {
            displayTitle += " (" + type + ")";
        }
        return displayTitle;

    }

    @Override
    public String getDisplayDetail() {

        String displayDetail = institute;

        if (!TextUtils.isEmpty(phone)) {
            displayDetail += " (" + department + ")";
        }

        if (!TextUtils.isEmpty(supervisor)) {
            displayDetail += " - " + supervisor;
        }

        return displayDetail;

    }

    @Override
    public String getDisplayDuration() {

        String displayDuration = startDateFormatted;
        if (!TextUtils.isEmpty(endDateFormatted)) {
            displayDuration += " - " + endDateFormatted;
        }
        return displayDuration;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDateFormatted() {
        return startDateFormatted;
    }

    public void setStartDateFormatted(String startDateFormatted) {
        this.startDateFormatted = startDateFormatted;
    }

    public String getEndDateFormatted() {
        return endDateFormatted;
    }

    public void setEndDateFormatted(String endDateFormatted) {
        this.endDateFormatted = endDateFormatted;
    }
}
