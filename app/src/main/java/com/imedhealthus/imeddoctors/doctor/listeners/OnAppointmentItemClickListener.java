package com.imedhealthus.imeddoctors.doctor.listeners;

/**
 * Created by umair on 1/7/18.
 */

public interface OnAppointmentItemClickListener {
    void onAppointmentItemClick(int idx);
}
