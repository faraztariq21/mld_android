package com.imedhealthus.imeddoctors.doctor.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class AcceptedInsurance extends OverviewItem {

    private String id, name;

    public AcceptedInsurance() {
    }

    public AcceptedInsurance(JSONObject educationJson) {

        id = educationJson.optString("Id", "");
        name = educationJson.optString("InsuranceId", "");

    }

    public static List<AcceptedInsurance> parseInsurances(JSONArray insurancesJson){

        ArrayList<AcceptedInsurance> acceptedInsurances = new ArrayList<>();

        for (int i = 0; i < insurancesJson.length(); i++){

            JSONObject insuranceJson = insurancesJson.optJSONObject(i);
            if (insuranceJson != null) {
                AcceptedInsurance acceptedInsurance = new AcceptedInsurance(insuranceJson);
                acceptedInsurances.add(acceptedInsurance);
            }

        }

        return acceptedInsurances;

    }


    @Override
    public String getDisplayTitle() {

        String displayTitle =  name;
        return displayTitle;

    }

    @Override
    public String getDisplayDetail() {

        return "";

    }

    @Override
    public String getDisplayDuration() {

        return "";

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
