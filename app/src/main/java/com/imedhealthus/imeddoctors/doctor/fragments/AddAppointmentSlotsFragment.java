package com.imedhealthus.imeddoctors.doctor.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.fragments.FragmentSuggestionDialog;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.GeneralSuggestionItem;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.SuggestionListItem;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.doctor.models.Appointment;
import com.imedhealthus.imeddoctors.doctor.models.ClinicLocation;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;


public class AddAppointmentSlotsFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, FragmentSuggestionDialog.FragmentSuggestionDialogListener {


    int spLocationCheck = 0;

    @BindView(R.id.sp_type)
    Spinner spType;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.et_slot_duration)
    EditText etSlotDuration;

    @BindView(R.id.tv_start_time)
    TextView tvStartTime;
    @BindView(R.id.tv_end_time)
    TextView tvEndTime;

    /*@BindView(R.id.sp_location)
    Spinner spLocation;*/
    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.cont_location)
    LinearLayout llLocation;


    Appointment appointment;
    List<String> locationAddresses;

    @BindView(R.id.et_location)
    EditText etLocation;
    @BindView(R.id.sp_slot_type)
    Spinner spSlotTypes;
    private int startHour;
    private int startMinute;
    private int startSeconds;

    public AddAppointmentSlotsFragment() {
        appointment = SharedData.getInstance().getSelectedAppointment();
    }


    @Override
    public String getName() {
        return "AddAppointmentSlotsFragment";
    }

    @Override
    public String getTitle() {
        return "Create Appointment";
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_appointment_slots, container, false);
        ButterKnife.bind(this, view);

        etLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationAddresses.add("Lahore");
                FragmentSuggestionDialog.newInstance("Choose Location", GeneralSuggestionItem.getSuggestions((ArrayList<String>) locationAddresses), false, AddAppointmentSlotsFragment.this).show(getFragmentManager());
            }
        });
        initHelper();
        return view;

    }

    private void initHelper() {

        spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 2) {
                    llLocation.setVisibility(View.VISIBLE);
                } else {
                    llLocation.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spSlotTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        TransitionManager.beginDelayedTransition((ViewGroup) view.findViewById(R.id.ll_cont_main));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (position == 1) {
                    tvEndTime.setVisibility(View.GONE);
                } else {
                    tvEndTime.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spType.setSelection(0);

        setUpLocationsSpinner();

        setData();

    }

    private void setUpLocationsSpinner() {
        DoctorProfile profile = SharedData.getInstance().getSelectedDoctorProfile();
        if (profile == null) {
            return;
        }

        locationAddresses = new ArrayList<>(profile.getClinicLocations().size());
        //locationAddresses.add("Select Appointment Location");
        for (ClinicLocation location : profile.getClinicLocations()) {
            locationAddresses.add(location.getAddress());
        }

        locationAddresses.add(0, "Add New Location");
       /* ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, locationAddresses);
        spLocation.setAdapter(adapter);

        spLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                if (position == 1) {
                    Intent intent = new Intent(context, SimpleFragmentActivity.class);
                    intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditLocation.ordinal());
                    intent.putExtra(Constants.IS_APPOINTMENT_SLOT_FRAGMENT, true);
                    ((BaseActivity) context).startActivityForResult(intent, Constants.ADD_CLINIC_REQUEST, true);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });*/

    }

    private void setData() {

        if (appointment == null)
            return;

        int selection = appointment.getType() == Constants.AppointmentType.OFFICE ? 2 : 1;
        spType.setSelection(selection);
        long duration = (appointment.getEndTimeStamp() - appointment.getStartTimeStamp()) / 60000;
        etSlotDuration.setText(String.valueOf(duration));

        tvDate.setText(GenericUtils.formatDate(appointment.getStartDate()));

        tvStartTime.setText(GenericUtils.formatTime(appointment.getStartDate()));
        tvEndTime.setText(GenericUtils.formatTime(appointment.getEndDate()));


        //spLocation.setSelection(locationAddresses.indexOf(appointment.getPhysicalLocation()));
        etNotes.setText(appointment.getNotes().replace("null", ""));

    }

    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditAppointmentSlots();
                }
                break;

            case R.id.tv_date:
                openDatePicker(tvDate.getText().toString(), "date");
                break;

            case R.id.tv_start_time:
                openTimeDialog("Start Time");
                break;

            case R.id.tv_end_time:
                openTimeDialog("End Time");
//                openTimePicker(tvEndTime.getText().toString(), "EndTime");
                break;
        }
    }


    private void openDatePicker(String dateStr, String tag) {

        Date date;
        if (dateStr.isEmpty()) {
            date = new Date();
        } else {
            date = GenericUtils.unFormatDate(dateStr);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setVersion(DatePickerDialog.Version.VERSION_2);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.light_blue));

        datePickerDialog.show(((Activity) context).getFragmentManager(), tag);

    }

    private void openTimePicker(String timeStr, String tag) {

        Date date;
        if (timeStr.isEmpty()) {
            date = new Date();
        } else {
            date = GenericUtils.unFormatTime(timeStr);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(this, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.YEAR), true);
        timePickerDialog.setVersion(TimePickerDialog.Version.VERSION_2);
        timePickerDialog.setAccentColor(getResources().getColor(R.color.light_blue));

        timePickerDialog.show(((Activity) context).getFragmentManager(), tag);

    }


    //Date picker callback
    @Override
    public void onDateSet(DatePickerDialog view, int year, int month, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        tvDate.setText(GenericUtils.formatDate(calendar.getTime()));

    }

    //Time picker callback
    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);

        startHour = hourOfDay;
        startMinute = minute;
        startSeconds = second;
        if (view.getTag().equalsIgnoreCase("startTime")) {
            tvStartTime.setText(GenericUtils.formatTime(calendar.getTime()));
        } else {
            tvEndTime.setText(GenericUtils.formatTime(calendar.getTime()));
        }

    }


    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (tvDate.getText().toString().isEmpty() || etSlotDuration.getText().toString().isEmpty() ||
                tvStartTime.getText().toString().isEmpty() || (tvEndTime.getText().toString().isEmpty() && tvEndTime.getVisibility() == View.VISIBLE)) {
            errorMsg = "Kindly fill all fields to continue";
        } else if (spType.getSelectedItemPosition() == 0) {
            errorMsg = "Kindly select a type";
        } else if (spType.getSelectedItemPosition() == 2 && /*spLocation.getSelectedItemPosition() == 0*/ etLocation.getText().toString().isEmpty()) {
            errorMsg = "Kindly select a location for physical appointment";
        }

        Date dateNow = new Date();
        Date dateStart = GenericUtils.unFormatTime(tvStartTime.getText().toString());

        dateStart.setYear(dateNow.getYear());
        dateStart.setMonth(dateNow.getMonth());
        dateStart.setDate(dateNow.getDate());


        Date endDate = null;
        if (tvEndTime.getVisibility() == View.VISIBLE) {
            endDate = GenericUtils.unFormatTime(tvEndTime.getText().toString());
        } else {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, startHour);
            calendar.set(Calendar.MINUTE, startMinute);
            calendar.set(Calendar.SECOND, startSeconds);
            calendar.add(Calendar.MINUTE, Integer.parseInt(etSlotDuration.getText().toString()));
            endDate = GenericUtils.unFormatTime(GenericUtils.formatTime(calendar.getTime()));
        }

        endDate.setYear(dateNow.getYear());
        endDate.setMonth(dateNow.getMonth());
        endDate.setDate(dateNow.getDate());

        String duartion = etSlotDuration.getText().toString();


        if (!(dateStart.getTime() < endDate.getTime())) {
            errorMsg = "Check Your start and end time again";
        }


        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        int slotDuration = Integer.parseInt(duartion);
        if ((endDate.getTime() - dateStart.getTime()) < slotDuration) {
            errorMsg = "End time is not set correctly";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }
        return true;

    }

    private void adjustAppointments(RetrofitJSONResponse response) {

        DoctorProfile profile = SharedData.getInstance().getSelectedDoctorProfile();
        if (profile == null) {
            return;
        }

        JSONArray appointmentsJson = response.optJSONArray("DoctorAppointments");
        if (appointmentsJson == null) {
            return;
        }
        List<Appointment> appointments = Appointment.parseAddNewAppointmentsResponse(appointmentsJson);
        profile.getAppointments().addAll(appointments);

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditAppointmentSlots() {

        AlertUtils.showProgress(getActivity());

        String type = spType.getSelectedItem().toString();
        String slotDuration = etSlotDuration.getText().toString();

        Date dateNow = null;
        Date date = GenericUtils.unFormatDate(tvDate.getText().toString());

        dateNow = GenericUtils.unFormatDate(tvDate.getText().toString());

        long start = 0;
        long end = 0;
        String appointmentDate = GenericUtils.getTimeDateString(date);


        date = GenericUtils.unFormatTime(tvStartTime.getText().toString());

        date.setYear(dateNow.getYear());
        date.setMonth(dateNow.getMonth());
        date.setDate(dateNow.getDate());

        start = date.getTime();

        if (start < new Date().getTime()) {
            String errorMsg = "Appointment start time has already passed";
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            AlertUtils.dismissProgress();
            return;
        }
        String startTime = GenericUtils.getTimeDateString(date);

        if (tvEndTime.getVisibility() == View.VISIBLE)
            date = GenericUtils.unFormatTime(tvEndTime.getText().toString());
        else {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, startHour);
            calendar.set(Calendar.MINUTE, startMinute);
            calendar.set(Calendar.SECOND, startSeconds);

            calendar.add(Calendar.MINUTE, Integer.parseInt(etSlotDuration.getText().toString()));
            date = GenericUtils.unFormatTime(GenericUtils.formatTime(calendar.getTime()));
            //date.setTime(date.getTime() + Long.parseLong(etSlotDuration.getText().toString()));
        }
        date.setYear(dateNow.getYear());
        date.setMonth(dateNow.getMonth());
        date.setDate(dateNow.getDate());

        end = date.getTime();
        String endTime = GenericUtils.getTimeDateString(date);

//        if(checkForOverlaping(start,end))
//            return;


        String location = etLocation.getText().toString();
        /*if (spLocation.getSelectedItemPosition() != 0)
            location = spLocation.getSelectedItem().toString();*/

        String notes = etNotes.getText().toString();


        String userId = CacheManager.getInstance().getCurrentUser().getUserId();
        String appointmentId = appointment == null ? "" : String.valueOf(appointment.getAppointmentId());

        WebServicesHandler.instance.addEditAppointmentSlots(appointmentId, userId, type, appointmentDate,
                startTime, endTime, slotDuration, location, notes, new CustomCallback<RetrofitJSONResponse>() {
                    @Override
                    public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                        AlertUtils.dismissProgress();
                        if (!response.status()) {
                            AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                            return;
                        }

                        //adjustAppointments(response);
                        SharedData.getInstance().setRefreshViaServerHit(true);
                        AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Appointment slots saved successfully");

                    }

                    @Override
                    public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                        AlertUtils.dismissProgress();
                        AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
                    }
                });

    }

    private boolean checkForOverlaping(long start, long end) {
        List<Appointment> list = SharedData.getInstance().getSelectedDoctorProfile().getAppointments();

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isOverlaped(end, start)) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(context, Constants.ErrorAlertTitle,
                        "The appointment you are trying to add is overlapping an existing one.");
                return true;
            }
        }
        return false;
    }

    public void openTimeDialog(final String title) {
        Calendar now = Calendar.getInstance();
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.time_picker);
        dialog.setCancelable(false);

        final TimePicker tp = (TimePicker) dialog.findViewById(R.id.time_picker);
        TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
        TextView tvOK = (TextView) dialog.findViewById(R.id.tvOK);

        tvTitle.setText(title);
        String time = "";
        if (title.equalsIgnoreCase("Start Time")) {
            time = tvStartTime.getText().toString();
        } else {
            time = tvEndTime.getText().toString();
        }
        Date date;
        if (TextUtils.isEmpty(time)) {
            date = new Date();
        } else {
            date = GenericUtils.unFormatTime(time);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        if (Build.VERSION.SDK_INT < 23) {
            tp.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
            tp.setCurrentMinute(calendar.get(Calendar.MINUTE));
        } else {
            tp.setHour(calendar.get(Calendar.HOUR_OF_DAY));
            tp.setMinute(calendar.get(Calendar.MINUTE));
        }

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tvOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                int hour, minute, second;
                if (Build.VERSION.SDK_INT < 23) {
                    hour = tp.getCurrentHour();
                    minute = tp.getCurrentMinute();
                    second = 0;
                } else {
                    hour = tp.getHour();
                    minute = tp.getMinute();
                    second = 0;
                }

                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, hour);
                calendar.set(Calendar.MINUTE, minute);
                calendar.set(Calendar.SECOND, second);

                startHour = hour;
                startMinute = minute;
                startSeconds = second;

                if (title.equalsIgnoreCase("Start Time")) {
                    tvStartTime.setText(GenericUtils.formatTime(calendar.getTime()));
                } else {
                    tvEndTime.setText(GenericUtils.formatTime(calendar.getTime()));
                }
            }
        });
        dialog.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.ADD_CLINIC_REQUEST && resultCode == RESULT_OK) {
            setUpLocationsSpinner();
        }
    }

    @Override
    public void onSuggestionSelected(int index, SuggestionListItem suggestionListItem) {
        if (suggestionListItem.getSuggestionText().equals("Add New Location")) {
            Intent intent = new Intent(context, SimpleFragmentActivity.class);
            intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditLocation.ordinal());
            intent.putExtra(Constants.IS_APPOINTMENT_SLOT_FRAGMENT, true);
            ((BaseActivity) context).startActivityForResult(intent, Constants.ADD_CLINIC_REQUEST, true);

        } else {
            etLocation.setText(locationAddresses.get(index));
        }
    }
}
