package com.imedhealthus.imeddoctors.doctor.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.CustomCitySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomCountrySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomStateSpinnerAdapter;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.City;
import com.imedhealthus.imeddoctors.common.models.Country;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.State;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.doctor.models.Award;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddEditAwardFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.et_title)
    EditText etTitle;
    @BindView(R.id.et_institute)
    EditText etInstitute;
    @BindView(R.id.tv_date)
    TextView tvDate;


    @BindView(R.id.sp_city)
    Spinner spCity;
    @BindView(R.id.sp_state)
    Spinner spState;
    @BindView(R.id.sp_country)
    Spinner spCountry;


    private List<Country> countries;
    private CustomCountrySpinnerAdapter customCountryAdapter;
    private CustomStateSpinnerAdapter stateAdapter;
    private List<State> states;
    private List<City> cities;
    private CustomCitySpinnerAdapter cityAdapter;

    private Award award;

    public AddEditAwardFragment() {
        award = SharedData.getInstance().getSelectedAward();
    }


    @Override
    public String getName() {
        return "AddEditAwardFragment";
    }

    @Override
    public String getTitle() {
        if (award == null) {
            return "Add Award/Recognition";
        }else {
            return "Edit Award/Recognition";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_award, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    private void setData() {

        if (award == null) {
            return;
        }

        etTitle.setText(award.getTitle());
        etInstitute.setText(award.getInstitute());
        tvDate.setText(award.getDateFormatted());

        setSpinnerData(award.getCountryId(),award.getStateId(),award.getCityId());
    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditAward();
                }
                break;

            case R.id.tv_date:
                openDatePicker(tvDate.getText().toString(), "date");
                break;

        }
    }


    private void openDatePicker(String dateStr, String tag) {

        Date date;
        if (dateStr.isEmpty()) {
            date = new Date();
        }else {
            date = GenericUtils.unFormatDate(dateStr);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setVersion(DatePickerDialog.Version.VERSION_2);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.light_blue));

        datePickerDialog.show(((Activity)context).getFragmentManager(), tag);

    }


    //Date picker callback
    @Override
    public void onDateSet(DatePickerDialog view, int year, int month, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        tvDate.setText(GenericUtils.formatDate(calendar.getTime()));

    }


    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etTitle.getText().toString().isEmpty() || etInstitute.getText().toString().isEmpty() ||
                tvDate.getText().toString().isEmpty()  ) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustAwards(RetrofitJSONResponse response, Award updatedAward) {

        if (award == null) {//Add

            JSONObject awardJson = response.optJSONObject("DoctorAward");
            if (awardJson != null) {
                Award newAward = new Award(awardJson);
                DoctorProfile profile = SharedData.getInstance().getSelectedDoctorProfile();
                profile.getAwards().add(newAward);
            }

        }else  {

            award.setTitle(updatedAward.getTitle());
            award.setInstitute(updatedAward.getInstitute());

            award.setDate(updatedAward.getDate());
            award.setDateFormatted(updatedAward.getDateFormatted());

            award.setCityId(updatedAward.getCityId());
            award.setCityName(updatedAward.getCityName());
            award.setStateId(updatedAward.getStateId());
            award.setStateName(updatedAward.getStateName());
            award.setCountryId(updatedAward.getCountryId());
            award.setCountryName(updatedAward.getCountryName());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }

    public void setSpinnerData(final String countryId,final String stateId,final String cityId){

        countries = SharedData.getInstance().getCountriesList();
        customCountryAdapter = new CustomCountrySpinnerAdapter(getContext(), R.layout.custom_spinner_item,countries);
        spCountry.setAdapter(customCountryAdapter);
        int selectedCountryIndex = SharedData.getInstance().getCountryIndexInList(countries,countryId);
        spCountry.setSelection(selectedCountryIndex);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                states = SharedData.getInstance().
                        getStatesListAgainstCountryByCountryId(countryId);
                stateAdapter = new CustomStateSpinnerAdapter(getContext(), R.layout.custom_spinner_item, states);
                spState.setAdapter(stateAdapter);
                int selectedStateIndex =SharedData.getInstance().getStateIndexInList(states,stateId);
                spState.setSelection(selectedStateIndex);


                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cities = SharedData.getInstance().
                                getCitiesListByStateId(stateId);
                        cityAdapter = new CustomCitySpinnerAdapter(getContext(), R.layout.custom_spinner_item, cities);
                        spCity.setAdapter(cityAdapter);
                        int selectedCityIndex =SharedData.getInstance().getCityIndexInList(cities,cityId);
                        spCity.setSelection(selectedCityIndex);
                    }
                }, 100);
            }
        }, 100);


    }


    private void initView() {

        countries = SharedData.getInstance().getCountriesList();
        customCountryAdapter = new CustomCountrySpinnerAdapter(getContext(), R.layout.custom_spinner_item,countries);
        spCountry.setAdapter(customCountryAdapter);

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                states = SharedData.getInstance().
                        getStatesListAgainstCountryByCountryId(Integer.toString(((Country)adapterView.getSelectedItem()).countryId));
                stateAdapter = new CustomStateSpinnerAdapter(getContext(), R.layout.custom_spinner_item, states);
                spState.setAdapter(stateAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(adapterView.getSelectedItem()==null)
                    return;
                cities = SharedData.getInstance().
                        getCitiesListByStateId(((State)adapterView.getItemAtPosition(i)).getStateId());
                cityAdapter = new CustomCitySpinnerAdapter(getContext(), R.layout.custom_spinner_item, cities);
                spCity.setAdapter(cityAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }
    //Webservices
    private void addEditAward() {

        AlertUtils.showProgress(getActivity());

        final Award updatedAward = new Award();
        if (award != null) {
            updatedAward.setId(award.getId());
        }

        updatedAward.setTitle(etTitle.getText().toString());
        updatedAward.setInstitute(etInstitute.getText().toString());

        updatedAward.setDateFormatted(tvDate.getText().toString());
        Date date = GenericUtils.unFormatDate(tvDate.getText().toString());
        updatedAward.setDate(GenericUtils.getTimeDateString(date));

        if(spState.getSelectedItem() == null || ((State) spState.getSelectedItem()).getStateId().equals("0")) {
            updatedAward.setStateId("0");
            updatedAward.setStateName("");
        }else {
            updatedAward.setStateId(((State) spState.getSelectedItem()).getStateId());
            updatedAward.setStateName(((State) spState.getSelectedItem()).getStateName());
        }

        if(spCountry.getSelectedItem() == null || Integer.toString(((Country) spCountry.getSelectedItem()).countryId).equals("0")) {
            updatedAward.setCountryId("0");
            updatedAward.setCountryName("");
        }else {
            updatedAward.setCountryId(Integer.toString(((Country) spCountry.getSelectedItem()).countryId));
            updatedAward.setCountryName(((Country) spCountry.getSelectedItem()).getCountryName());
        }

        if(spCity.getSelectedItem() == null || ((City) spCity.getSelectedItem()).getCityId().equals("0")) {
            updatedAward.setCityId("0");
            updatedAward.setCityName("");
        }else {
            updatedAward.setCityId(((City) spCity.getSelectedItem()).getCityId());
            updatedAward.setCityName(((City) spCity.getSelectedItem()).getCityName());
        }

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditAward(userId, updatedAward, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                adjustAwards(response, updatedAward);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Award/Recognition details saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
