package com.imedhealthus.imeddoctors.doctor.fragments;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.CustomArrayAdapter;
import com.imedhealthus.imeddoctors.common.adapters.GooglePlaceArrayAdapter;
import com.imedhealthus.imeddoctors.common.fragments.MapBaseFragment;
import com.imedhealthus.imeddoctors.common.listeners.FragmentChangeHandler;
import com.imedhealthus.imeddoctors.common.models.GooglePlaceAutoComplete;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.utils.ImageUtils;
import com.imedhealthus.imeddoctors.common.widgets.MarginChangeAnim;
import com.imedhealthus.imeddoctors.doctor.adapters.CustomMultiAutoCompleteAdapter;
import com.imedhealthus.imeddoctors.doctor.models.Filter;
import com.imedhealthus.imeddoctors.doctor.models.StaticData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/6/18.
 */

public class FiltersFragment extends MapBaseFragment implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, AdapterView.OnItemClickListener, ResultCallback<PlaceBuffer>,
        SeekBar.OnSeekBarChangeListener, RatingBar.OnRatingBarChangeListener, TextWatcher, View.OnClickListener {

    @BindView(R.id.cont_filters)
    ViewGroup contFilters;
    @BindView(R.id.cont_map)
    ViewGroup contMap;

    @BindView(R.id.rd_all)
    RadioButton rdAll;
    @BindView(R.id.rd_physical)
    RadioButton rdPhysicalAppointment;
    @BindView(R.id.rd_tele)
    RadioButton rdTeleAppointment;

    @BindView(R.id.btn_select_loc)
    Button btnSelectLocation;
    @BindView(R.id.sb_distance)
    SeekBar sbDistance;

    @BindView(R.id.sp_speciality)
    Spinner spSpeciality;

    @BindView(R.id.rd_male)
    RadioButton rdGenderMale;

    @BindView(R.id.rd_gender_none)
    RadioButton rdGenderNone;

    @BindView(R.id.rd_female)
    RadioButton rdGenderFemale;

    @BindView(R.id.sb_fee)
    CrystalRangeSeekbar sbFee;
    @BindView(R.id.rb_rating)
    RatingBar rbRating;
    @BindView(R.id.et_rating)
    EditText etRating;

    @BindView(R.id.et_reason)
    MultiAutoCompleteTextView etLanguages;


    @BindView(R.id.et_search_location)
    AutoCompleteTextView etSearchLocation;

    @BindView(R.id.tv_max_fee)
    TextView tvMaxFee;
    @BindView(R.id.tv_min_fee)
    TextView tvMinFee;

    @BindView(R.id.tv_max_distance)
    TextView tvMaxDistance;


    @BindView(R.id.btn_search_filter)
    TextView btnSearch;

    @BindView(R.id.cont_location)
    LinearLayout contLocation;

    @BindView(R.id.btn_reset_filter)
    TextView btnReset;


    private Filter filter;
    private GoogleApiClient googleApiClient;
    private GooglePlaceArrayAdapter adapterGooglePlaces;
    private Marker currentMarker;

    @Override
    public String getName() {
        return "FiltersFragment";
    }

    @Override
    public String getTitle() {
        if (contMap.getVisibility() == View.VISIBLE) {
            return "Select Location";
        }
        return "Filters";
    }

    @Override
    public void setData(Object data) {
        filter = (Filter) data;
    }

    @Override
    public Object getData() {
        return createFilter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_filters, container, false);
        ButterKnife.bind(this, rootView);

        initHelper();
        return rootView;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            setData();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void initHelper() {

        super.initHelper();

        List<StaticData> languages = SharedData.getInstance().getLanguageList();

        CustomMultiAutoCompleteAdapter reasonsAdapter = new CustomMultiAutoCompleteAdapter(getActivity(),
                R.layout.simple_dropdown_item_1line, etLanguages, languages);
        etLanguages.setAdapter(reasonsAdapter);
        etLanguages.setThreshold(1);

        etLanguages.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Places.GEO_DATA_API)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .build();

        etSearchLocation.setThreshold(2);
        etSearchLocation.setOnItemClickListener(this);

        LatLngBounds bounds = new LatLngBounds(new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
        adapterGooglePlaces = new GooglePlaceArrayAdapter(context, android.R.layout.simple_list_item_1, bounds, null);
        etSearchLocation.setAdapter(adapterGooglePlaces);
        sbFee.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                tvMaxFee.setText("$" + maxValue.intValue());
                tvMinFee.setText("$" + minValue.intValue());
            }
        });

        rdTeleAppointment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    contLocation.setVisibility(View.GONE);
                else
                    contLocation.setVisibility(View.VISIBLE);
            }
        });

        List<StaticData> listSpeciality = SharedData.getInstance().getSpecialityList();

        CustomArrayAdapter dataAdapter = new CustomArrayAdapter(getContext(), R.layout.custom_spinner_item, listSpeciality);
        spSpeciality.setAdapter(dataAdapter);

        sbDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                filter.setDistance(seekBar.getProgress());
                tvMaxDistance.setText(seekBar.getProgress() + "km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        rbRating.setOnRatingBarChangeListener(this);
        etRating.addTextChangedListener(this);
        btnSearch.setOnClickListener(this);
        btnReset.setOnClickListener(this);
    }

    private void setData() throws NullPointerException {


        if (filter.isAllAppointments())
            rdAll.setChecked(true);
        else {
            if (filter.isOfficeAppointment()) {
                rdPhysicalAppointment.setChecked(true);
            } else {
                rdTeleAppointment.setChecked(true);
            }
        }
        if (!TextUtils.isEmpty(filter.getAddress())) {
            btnSelectLocation.setText(filter.getAddress());
            etSearchLocation.setText(filter.getAddress());
        }
        sbDistance.setProgress(filter.getDistance());

        List<String> specialities = Arrays.asList(context.getResources().getStringArray(R.array.specialities));
        int idx = specialities.indexOf(filter.getSpeciality());
        if (idx > 0) {
            spSpeciality.setSelection(idx);
        }

        if (filter.getGender().equalsIgnoreCase("")) {
            rdGenderNone.setChecked(true);
        } else {

            if (filter.getGender().equalsIgnoreCase("female")) {
                rdGenderFemale.setChecked(true);
            } else {
                rdGenderMale.setChecked(true);
            }

        }


        int maxConsultanceFee = (int) filter.getConsultationFee();
        int minConsultanceFee = (int) filter.getMinConsultationFee();


        sbFee.setMinStartValue(minConsultanceFee);
        sbFee.setMaxStartValue(maxConsultanceFee);
        sbFee.apply();

        rbRating.setRating((float) filter.getRating());
        etRating.setText(String.format("%.1f", filter.getRating()));
        etLanguages.setText(filter.getLanguages().toString().replace("[", "").replace("]", ""));


    }

    private Filter createFilter() {

        Filter filter = new Filter();

        filter.setOfficeAppointment(rdPhysicalAppointment.isChecked());
        filter.setAllAppointments(rdAll.isChecked());
        if (rdAll.isChecked())
            filter.setAppointmentype("");
        filter.setRating(rbRating.getRating());
        filter.setAddress(btnSelectLocation.getText().toString());
        if (currentMarker != null) {
            filter.setLatitude(currentMarker.getPosition().latitude);
            filter.setLongitude(currentMarker.getPosition().longitude);
        }
        filter.setDistance(sbDistance.getProgress());
        ArrayList<String> languages = new ArrayList<>();


        filter.setLanguages(languages);
        if (spSpeciality.getAdapter() != null) {
            if (spSpeciality.getAdapter().getCount() != 0)
                filter.setSpeciality(((StaticData) spSpeciality.getSelectedItem()).getId().replace("-1", ""));
            else
                filter.setSpeciality("");
        }


        filter.setGender(rdGenderNone.isChecked() ? "" : (rdGenderMale.isChecked()) ? "Male" : "Female");
        filter.setConsultationFee(sbFee.getSelectedMaxValue().intValue());
        filter.setMinConsultationFee(sbFee.getSelectedMinValue().intValue());
        filter.setDistance(sbDistance.getProgress());
        if (!etLanguages.getText().toString().isEmpty())
            filter.setLanguages(Arrays.asList(etLanguages.getText().toString().split(",")));

        return filter;

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_select_loc:
                openMap();
                break;

            case R.id.btn_search_filter:
                filterNow();
                break;

            case R.id.btn_reset_filter:
                filter = new Filter();
                spSpeciality.setSelection(0);
                try {
                    setData();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                break;

        }
    }

    private void filterNow() {
        if (context instanceof FragmentChangeHandler) {
            ((FragmentChangeHandler) context).hideOverlayFragment();
        }
    }


    //Map view show/hide
    private void openMap() {

        int contentWidth = contFilters.getWidth();

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) contMap.getLayoutParams();
        params.width = contentWidth;
        contMap.setLayoutParams(params);

        MarginChangeAnim anim = new MarginChangeAnim(contMap, contentWidth, 0);
        anim.setDuration(400);

        contMap.startAnimation(anim);
        contMap.setVisibility(View.VISIBLE);

        if (context instanceof FragmentChangeHandler) {
            ((FragmentChangeHandler) context).refreshOverlayHeader();
        }

        if (filter.getLatitude() != 0 && filter.getLongitude() != 0) {
            addMarker(new LatLng(filter.getLatitude(), filter.getLongitude()), "Selected location");
        }

    }

    private void closeMap() {

        btnSelectLocation.setText(etSearchLocation.getText().toString());
        filter.setAddress(etSearchLocation.getText().toString());
        MarginChangeAnim anim = new MarginChangeAnim(contMap, contMap.getLeft(), contMap.getWidth());
        anim.setDuration(400);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                contMap.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        contMap.startAnimation(anim);

    }

    @Override
    public boolean canHandleOnBackClick() {
        if (contMap.getVisibility() == View.VISIBLE) {
            return true;
        }
        return super.canHandleOnBackClick();
    }

    @Override
    public void onBackClick() {
        if (contMap.getVisibility() == View.VISIBLE) {
            closeMap();
        }
    }


    //Places
    @Override
    public void onResume() {
        super.onResume();
        googleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        googleApiClient.disconnect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("iMed - Filter", "onConnectionFailed");
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        adapterGooglePlaces.setGoogleApiClient(googleApiClient);
    }

    @Override
    public void onConnectionSuspended(int i) {
        adapterGooglePlaces.setGoogleApiClient(null);
        Log.e("iMed - Filter", "onConnectionSuspended");
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

        GooglePlaceAutoComplete item = adapterGooglePlaces.getItem(position);
        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(googleApiClient, String.valueOf(item.getPlaceId()));
        placeResult.setResultCallback(this);

    }

    @Override
    public void onResult(@NonNull PlaceBuffer places) {

        if (!places.getStatus().isSuccess() || places.getCount() == 0) {
            Log.e("iMed - Filter", "Place query did not complete. Error: " + places.getStatus().toString());
            return;
        }

        addMarker(places.get(0).getLatLng(), "Selected Location");

    }

    @Override
    public void locationReceived(LatLng latLng) {
        if (currentMarker == null) {
            etSearchLocation.setText(getAddress(latLng.latitude, latLng.longitude));
            addMarker(latLng, "Your Location");
        }
    }

    public String getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());

        String add = null;
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            if (addresses.isEmpty())
                return "Unable to get address";
            Address obj = addresses.get(0);
            add = obj.getAddressLine(0);

            add.replace("null", "");
            Log.v("IGA", "Address" + add);

        } catch (IOException e) {
            e.printStackTrace();
            add = e.getMessage();
        }
        return add.replace("null,", "");
    }

    private void addMarker(LatLng latLng, String title) {

        if (currentMarker != null) {
            currentMarker.remove();
        }

        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng).title(title)
                .icon(ImageUtils.getBitmapDescriptor(context, R.drawable.ic_marker));

        filter.setLatitude(latLng.latitude);
        filter.setLongitude(latLng.longitude);
        currentMarker = googleMap.addMarker(markerOptions);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

        switch (seekBar.getId()) {
            case R.id.sb_fee:
                filter.setConsultationFee(sbFee.getSelectedMaxValue().intValue());
                break;
            case R.id.sb_distance:
                filter.setDistance(sbDistance.getProgress());
                break;

        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
        try {
            etRating.setText(Float.toString(ratingBar.getRating()));
            filter.setRating(ratingBar.getRating());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        try {
            Float rating = Float.parseFloat(charSequence.toString());
            if (rating > 5) {
                rbRating.setRating(5);
                filter.setRating(5);
            } else {
                filter.setRating(rating);
                rbRating.setRating(rating);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void afterTextChanged(Editable editable) {

    }


    public interface FiltersFragmentListener {
        public void onFiltersButtonResetTapped();

        public void onFiltersButtonSearchTapped();
    }
}
