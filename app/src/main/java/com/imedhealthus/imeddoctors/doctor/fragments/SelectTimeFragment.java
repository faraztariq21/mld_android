package com.imedhealthus.imeddoctors.doctor.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.doctor.adapters.AppointmentsListAdapter;
import com.imedhealthus.imeddoctors.doctor.listeners.OnAppointmentItemClickListener;
import com.imedhealthus.imeddoctors.doctor.models.Appointment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/10/18.
 */

public class SelectTimeFragment extends BaseFragment implements OnAppointmentItemClickListener {

    @BindView(R.id.btn_physical_appointment)
    TextView btnPhysicalAppointment;
    @BindView(R.id.btn_tele_appointment)
    TextView btnTeleAppointment;

    @BindView(R.id.tv_month)
    TextView tvMonth;

    @BindView(R.id.tv_previous_date)
    TextView tvPreviousDate;
    @BindView(R.id.tv_current_date)
    TextView tvCurrentDate;
    @BindView(R.id.tv_next_date)
    TextView tvNextDate;

    @BindView(R.id.rv_appointments)
    RecyclerView rvAppointments;
    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;
    @BindView(R.id.btn_request_appointment)
    TextView bRequestAppointment;
    @BindView(R.id.btn_next)
    TextView bNext;

    public static final int REQUEST_CALL_PERMISSION = 791;

    private List<Appointment> appointments;
    private AppointmentsListAdapter adapterAppointments;
    private SimpleDateFormat monthFormatter, dayFormatter;
    private Date currentDate;
    private Constants.AppointmentType selectedAppointmentType;
    private boolean isDataLoaded = false;

    @Override
    public boolean showHeader() {
        return true;
    }

    @Override
    public String getName() {
        return "SelectTimeFragment";
    }

    @Override
    public String getTitle() {
        return "Select Time";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_select_time, container, false);
        ButterKnife.bind(this, rootView);

        initHelper();
        filterAppointments(new Date());

        return rootView;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!isDataLoaded) {
            loadData();
            isDataLoaded = true;
        }
    }

    private void initHelper() {

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        rvAppointments.setLayoutManager(layoutManager);

        adapterAppointments = new AppointmentsListAdapter(tvNoRecords, bRequestAppointment, bNext, false, true, this);
        rvAppointments.setAdapter(adapterAppointments);

        monthFormatter = new SimpleDateFormat("MMMM yyyy");
        dayFormatter = new SimpleDateFormat("dd EEE");
        appointments = new ArrayList<>();
        currentDate = new Date();

        toggleAppointmentType(false);

    }

    private void adjustView(List<Appointment> appointments) {

        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d");
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");

        for (Appointment appointment : appointments) {
            appointment.adjustData(dateFormat, timeFormat, currentDate);
        }
        this.appointments = appointments;
        filterAppointments(currentDate);

    }

    //Other
    private void filterAppointments(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int filterYear = calendar.get(Calendar.YEAR);
        int filterMonth = calendar.get(Calendar.MONTH);
        int filterDay = calendar.get(Calendar.DAY_OF_MONTH);

        List<Appointment> filteredAppointments = new ArrayList<>();

        for (Appointment appointment : appointments) {
            if (appointment.isSameDay(filterYear, filterMonth, filterDay)) {
                filteredAppointments.add(appointment);
            }
        }

        adapterAppointments.setAppointments(filteredAppointments);

        tvMonth.setText(monthFormatter.format(date));
        tvCurrentDate.setText(dayFormatter.format(date));

        calendar.add(Calendar.HOUR, -24);
        tvPreviousDate.setText(dayFormatter.format(calendar.getTime()));

        calendar.add(Calendar.HOUR, 48);
        tvNextDate.setText(dayFormatter.format(calendar.getTime()));

        currentDate = date;

    }

    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_tele_appointment:
                toggleAppointmentType(false);
                break;

            case R.id.btn_physical_appointment:
                toggleAppointmentType(true);
                break;

            case R.id.iv_previous_month:
                filterAppointments(GenericUtils.getDateWithMonthDiff(currentDate, -1));
                break;

            case R.id.iv_next_month:
                filterAppointments(GenericUtils.getDateWithMonthDiff(currentDate, 1));
                break;

            case R.id.iv_previous_date:
            case R.id.tv_previous_date:
                filterAppointments(GenericUtils.getDateWithHourDiff(currentDate, -24));
                break;

            case R.id.iv_next_date:
            case R.id.tv_next_date:
                filterAppointments(GenericUtils.getDateWithHourDiff(currentDate, 24));
                break;

            case R.id.btn_next:
                openBookAppointment();
                break;

            case R.id.btn_request_appointment:
                if (android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PERMISSION);
                    return;
                }
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + getActivity().getString(R.string.contact_support)));
                context.startActivity(intent);
                break;
        }
    }

    private void toggleAppointmentType(boolean isPhysical) {

        btnPhysicalAppointment.setSelected(isPhysical);
        btnTeleAppointment.setSelected(!isPhysical);

        if (isPhysical) {
            selectedAppointmentType = Constants.AppointmentType.OFFICE;
        } else {
            selectedAppointmentType = Constants.AppointmentType.Tele;
        }
        filterAppointments(currentDate);

    }

    private void openBookAppointment() {

        if (SharedData.getInstance().getSelectedAppointment() == null) {
            AlertUtils.showAlert(context, Constants.ErrorAlertTitle, "Select an appointment first");
            return;
        }

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.BookAppointment.ordinal());
        ((BaseActivity) context).startActivity(intent, true);

    }


    //Appointment item click
    @Override
    public void onAppointmentItemClick(int idx) {

        Appointment appointment = adapterAppointments.getItem(idx);
        if (appointment == null || appointment.hasPassed()) {
            return;
        }
        adapterAppointments.setSelectedAppointment(appointment);
        SharedData.getInstance().setSelectedAppointment(appointment);

    }

    //Webservices
    private void loadData() {

        AlertUtils.showProgress(getActivity());
        String doctorId = SharedData.getInstance().getSelectedDoctor().getUserId();


        WebServicesHandler.instance.getDoctorsAvaiableAppointmentSlots(doctorId, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                List<Appointment> appointments = Appointment.parseAppointments(response.getJSONArray("Appointments"));
                adjustView(appointments);

        }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case REQUEST_CALL_PERMISSION: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(getActivity(), "Permission granted", Toast.LENGTH_SHORT).show();
                    if (android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PERMISSION);
                        return;
                    }
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + getActivity().getString(R.string.contact_support)));
                    context.startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}
