package com.imedhealthus.imeddoctors.doctor.models;

import com.imedhealthus.imeddoctors.common.models.SuggestionListItem;
import com.imedhealthus.imeddoctors.common.models.User;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class Doctor implements SuggestionListItem {

    private String userId, imageUrl, firstName, lastName, speciality, address;
    private double rating = 0;

    private long appointmentStartTimeStamp, appointmentEndTimeStamp;
    private String loginId;


    ArrayList<Appointment> appointmentSlots;

    public ArrayList<Appointment> getAppointmentSlots() {
        return appointmentSlots;
    }

    public void setAppointmentSlots(ArrayList<Appointment> appointmentSlots) {
        this.appointmentSlots = appointmentSlots;
    }

    public Doctor() {
    }

    public Doctor(User user) {
        this.userId = user.getUserId();
        this.imageUrl = user.getImageUrl();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.address = user.getLocation();
    }

    public Doctor(JSONObject jsonObject) {

        userId = jsonObject.optString("UserId", "");
        firstName = jsonObject.optString("DoctorName", "").replace("null", "");
        ;
        imageUrl = GenericUtils.getImageUrl(jsonObject.optString("ImageUrl", "").replace("null", ""));
        address = jsonObject.optString("Address", "").replace("null", "");
        ;
        speciality = jsonObject.optString("Speciality", "").replace("null", "");
        ;

        try {
            rating = Double.parseDouble(jsonObject.optString("Rating", "0").replace("null", "0"));
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }

        appointmentStartTimeStamp = GenericUtils.getLocalTimeFromUTC(jsonObject.optString("StartTime", ""));
        appointmentEndTimeStamp = GenericUtils.getLocalTimeFromUTC(jsonObject.optString("EndTime", ""));
        loginId = jsonObject.optString("LoginId", "").replace("null", "");

    }

    public static List<Doctor> parseRecommendedDoctors(JSONObject json) {

        List<Doctor> doctors = new ArrayList<>();
        JSONArray doctorsJson = json.optJSONArray("Recommended");

        if (doctorsJson != null) {
            doctors.addAll(parseDoctors(doctorsJson));
        }

        return doctors;

    }

    public static List<Doctor> parseAvailableDoctors(JSONObject json) {

        List<Doctor> doctors = new ArrayList<>();
        JSONArray doctorsJson = json.optJSONArray("Available");

        if (doctorsJson != null) {
            doctors.addAll(parseDoctors(doctorsJson));
        }

        return doctors;

    }

    public static List<Doctor> parseDoctors(JSONArray doctorsJson) {

        ArrayList<Doctor> doctors = new ArrayList<>();

        for (int i = 0; i < doctorsJson.length(); i++) {

            JSONObject doctorJson = doctorsJson.optJSONObject(i);
            if (doctorJson != null) {
                Doctor doctor = new Doctor(doctorJson);
                doctors.add(doctor);
            }

        }

        return doctors;

    }

    public boolean isCallAllowed() {


        long currentTimeStamp = new Date().getTime();
        return appointmentStartTimeStamp < currentTimeStamp && currentTimeStamp < appointmentEndTimeStamp;


    }

    public String getFullName() {
        return (firstName + " " + lastName).replace("null", "");
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public long getAppointmentStartTimeStamp() {
        return appointmentStartTimeStamp;
    }

    public void setAppointmentStartTimeStamp(long appointmentStartTimeStamp) {
        this.appointmentStartTimeStamp = appointmentStartTimeStamp;
    }

    public long getAppointmentEndTimeStamp() {
        return appointmentEndTimeStamp;
    }

    public void setAppointmentEndTimeStamp(long appointmentEndTimeStamp) {
        this.appointmentEndTimeStamp = appointmentEndTimeStamp;
    }

    @Override
    public String getSuggestionText() {
        return getFullName();
    }

    @Override
    public SuggestionListItem getSuggestionListItem() {
        return Doctor.this;
    }
}
