package com.imedhealthus.imeddoctors.doctor.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.doctor.listeners.OnAppointmentItemClickListener;
import com.imedhealthus.imeddoctors.doctor.models.Appointment;
import com.imedhealthus.imeddoctors.doctor.view_holders.AppointmentItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class AppointmentsListAdapter extends RecyclerView.Adapter<AppointmentItemViewHolder> {

    private List<Appointment> appointments;
    private WeakReference<TextView> tvNoRecords, bRequestAppointment, bNext;
    private boolean isDoctorProfile, typeBasedItems;
    private WeakReference<OnAppointmentItemClickListener> onAppointmentItemClickListener;
    private Appointment selectedAppointment;

    public AppointmentsListAdapter(TextView tvNoRecords, TextView bRequestAppointment, TextView bNext, boolean isDoctorProfile, boolean typeBasedItems, OnAppointmentItemClickListener onAppointmentItemClickListener) {

        super();

        this.tvNoRecords = new WeakReference<>(tvNoRecords);
        this.bRequestAppointment = new WeakReference<>(bRequestAppointment);
        this.bNext = new WeakReference<>(bNext);
        this.isDoctorProfile = isDoctorProfile;
        this.typeBasedItems = typeBasedItems;
        this.onAppointmentItemClickListener = new WeakReference<>(onAppointmentItemClickListener);
        selectedAppointment = null;

    }

    public void setAppointments(List<Appointment> appointments) {

        this.appointments = appointments;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            if (isDoctorProfile) {
                tvNoRecords.get().setVisibility(View.VISIBLE);
                bRequestAppointment.get().setVisibility(View.GONE);
            } else {
                tvNoRecords.get().setVisibility(View.VISIBLE);
                bRequestAppointment.get().setVisibility(View.VISIBLE);
                if (typeBasedItems) {
                    bNext.get().setVisibility(View.GONE);
                }
            }
        } else {
            tvNoRecords.get().setVisibility(View.GONE);
            bRequestAppointment.get().setVisibility(View.GONE);
            if (typeBasedItems) {
                bNext.get().setVisibility(View.VISIBLE);
            }
        }

    }

    public void setSelectedAppointment(Appointment selectedAppointment) {
        this.selectedAppointment = selectedAppointment;
        notifyDataSetChanged();
    }

    @Override
    public AppointmentItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        if (typeBasedItems) {
            view = inflater.inflate(R.layout.list_item_select_time_appointment, parent, false);
        } else {
            view = inflater.inflate(R.layout.list_item_doctor_appointment, parent, false);
        }

        OnAppointmentItemClickListener listener = null;
        if (onAppointmentItemClickListener != null && onAppointmentItemClickListener.get() != null) {
            listener = onAppointmentItemClickListener.get();
        }

        return new AppointmentItemViewHolder(view, typeBasedItems, listener);

    }

    @Override
    public void onBindViewHolder(AppointmentItemViewHolder viewHolder, int position) {
        Appointment appointment = getItem(position);
        boolean isSelected = selectedAppointment != null && selectedAppointment.getAppointmentId() == appointment.getAppointmentId();

        viewHolder.setData(appointment, isSelected, position);
    }

    @Override
    public int getItemCount() {
        return appointments == null ? 0 : appointments.size();
    }

    public Appointment getItem(int position) {
        if (position < 0 || position >= appointments.size()) {
            return null;
        }
        return appointments.get(position);
    }

}
