package com.imedhealthus.imeddoctors.doctor.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.doctor.models.StaticData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hamid on 11/28/2017.
 */

public class CustomMultiAutoCompleteAdapter extends ArrayAdapter<StaticData> implements Filterable
{
        private MultiAutoCompleteTextView textView;
        private List<StaticData> allItems;
        private List<StaticData> filteredItems=new ArrayList<>();
        private Filter filter = new CustomFilter();

    public CustomMultiAutoCompleteAdapter(Context context, int resource, MultiAutoCompleteTextView textView, List<StaticData> allItems) {
        super(context, resource);
        TextView itemView = (TextView) LayoutInflater.from(context).inflate(resource, null);
        itemView.setPadding(0,0,0,0);
        this.textView = textView;
        this.allItems = allItems;
        filteredItems = new ArrayList<>();
    }

        @Nullable
        @Override
        public StaticData getItem(int position) {
        return filteredItems.get(position);
    }

        @Override
        public int getCount() {
        return filteredItems.size();
    }



        @NonNull
        @Override
        public Filter getFilter() {
        return filter;
    }
        private class CustomFilter extends Filter {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                filteredItems.clear();

                Log.e("FILTERRES", ""+filteredItems.size() + ", " + constraint);

                if(allItems != null && constraint != null) {
                    for(int i=0 ; i<allItems.size(); i++) {
                        if(allItems.get(i).getValue().toLowerCase().contains(constraint.toString().toLowerCase())) {
                            filteredItems.add(allItems.get(i));
                        }
                    }
                }

                FilterResults results = new FilterResults();
                results.values = filteredItems;
                results.count = filteredItems.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if(results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        }


    }
