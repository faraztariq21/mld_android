package com.imedhealthus.imeddoctors.doctor.models;

import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Malik Hamid on 2/22/2018.
 */

public class ArchivedAppointment extends Appointment {

    String notes,doctorId,patientFullName,bookedAppintmentId,action;
    boolean isActive;

    public static ArrayList<ArchivedAppointment> getBookedAppointments(JSONObject jsonObject){
        ArrayList<ArchivedAppointment> appointments = new ArrayList<>();
        try {
            JSONArray jsonArray = jsonObject.getJSONArray("ArchivedAppointments");
            for(int i=0;i<jsonArray.length();i++){
                ArchivedAppointment appointment =new ArchivedAppointment(jsonArray.getJSONObject(i));
                appointments.add(appointment);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return appointments;
    }

    public ArchivedAppointment() {
        super();
    }


    public ArchivedAppointment(JSONObject jsonObject) {
        try {
            appointmentId=jsonObject.getInt("AppointmentId");

            long date = GenericUtils.getTimeDateInLong(jsonObject.getString("Date"));

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(date);

            int filterDay = calendar.get(Calendar.DATE);
            int filterYear = calendar.get(Calendar.YEAR);
            int filterMonth = calendar.get(Calendar.MONTH);

            Calendar sCalendar = Calendar.getInstance();

            startTimeStamp = GenericUtils.getTimeInLong(jsonObject.getString("StartTime"));

            sCalendar.setTimeInMillis(startTimeStamp);
            sCalendar.set(Calendar.YEAR,filterYear);
            sCalendar.set(Calendar.MONTH,filterMonth);
            sCalendar.set(Calendar.DATE,filterDay);


            startTimeStamp = sCalendar.getTimeInMillis();

            endTimeStamp =  GenericUtils.getTimeInLong(jsonObject.getString("EndTime"));

            sCalendar.setTimeInMillis(endTimeStamp);
            sCalendar.set(Calendar.YEAR,filterYear);
            sCalendar.set(Calendar.MONTH,filterMonth);
            sCalendar.set(Calendar.DATE,filterDay);


            endTimeStamp = sCalendar.getTimeInMillis();

            type = ((jsonObject.optString("AppointmentType").equalsIgnoreCase("Tele"))? Constants.AppointmentType.Tele:Constants.AppointmentType.OFFICE);
            physicalLocation = jsonObject.optString("Location");
            doctorId = jsonObject.optString("DoctorId");
            patientFullName = jsonObject.optString("PatientName");
            notes = jsonObject.optString("Note","").replace("null","notes");
            action = jsonObject.optString("Action","").replace("null","completed");

            bookedAppintmentId = jsonObject.optString("BookedAppointmentId","").replace("null","0");


            isActive = jsonObject.optBoolean("IsActive",false);


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getPatientFullName() {
        return patientFullName;
    }

    public void setPatientFullName(String patientFullName) {
        this.patientFullName = patientFullName;
    }

    public String getBookedAppintmentId() {
        return bookedAppintmentId;
    }

    public void setBookedAppintmentId(String bookedAppintmentId) {
        this.bookedAppintmentId = bookedAppintmentId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
