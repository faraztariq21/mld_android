package com.imedhealthus.imeddoctors.doctor.models;

import com.google.gson.annotations.SerializedName;
import com.imedhealthus.imeddoctors.common.models.SuggestionListItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class Speciality extends OverviewItem implements SuggestionListItem {

    @SerializedName("Id")
    private String uniqueId;
    @SerializedName("DoctorId")
    private String doctorId;
    @SerializedName("SpecialityId")
    private String id;

    private String name;
    @SerializedName("DoctorSubSpeciality")
    private List<SubSpeciality> subSpecialities = new ArrayList<>();

    public Speciality() {
    }

    public Speciality(JSONObject educationJson) {

        uniqueId = educationJson.optString("Id", "");
        id = educationJson.optString("SpecialityId", "");
        name = educationJson.optString("SpecialityName", "");

        JSONArray subSpecialitiesJson = educationJson.optJSONArray("SubSpeciality");
        if (subSpecialitiesJson != null) {
            subSpecialities = SubSpeciality.parseSubSpecialities(subSpecialitiesJson);
        } else {
            subSpecialities = new ArrayList<>();
        }

    }

    public static List<Speciality> parseSpecialities(JSONArray specialitiesJson) {

        ArrayList<Speciality> specialities = new ArrayList<>();

        for (int i = 0; i < specialitiesJson.length(); i++) {

            JSONObject specialityJson = specialitiesJson.optJSONObject(i);
            if (specialityJson != null) {
                Speciality speciality = new Speciality(specialityJson);
                specialities.add(speciality);
            }

        }

        return specialities;

    }


    @Override
    public String getDisplayTitle() {

        String displayTitle = name;
        return displayTitle;

    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    @Override
    public String getDisplayDetail() {

        StringBuilder builder = new StringBuilder();
        for (SubSpeciality subSpeciality : subSpecialities) {
            builder.append(subSpeciality.getName()).append(", ");
        }
        String displayDetail = builder.toString();
        if (displayDetail.length() > 0) {
            displayDetail = displayDetail.substring(0, displayDetail.length() - 1);
        }
        return displayDetail;

    }

    @Override
    public String getDisplayDuration() {

        return "";

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SubSpeciality> getSubSpecialities() {
        return subSpecialities;
    }

    public void setSubSpecialities(List<SubSpeciality> subSpecialities) {
        this.subSpecialities = subSpecialities;
    }

    @Override
    public String getSuggestionText() {
        return name;
    }

    @Override
    public SuggestionListItem getSuggestionListItem() {
        return Speciality.this;
    }

    public static ArrayList<SuggestionListItem> getSuggestions(ArrayList<Speciality> specialities) {
        ArrayList<SuggestionListItem> suggestionListItems = new ArrayList<>();
        for (Speciality speciality : specialities)
            suggestionListItems.add(speciality);

        return suggestionListItems;
    }

}
