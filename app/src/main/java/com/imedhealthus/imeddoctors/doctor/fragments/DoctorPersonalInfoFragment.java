package com.imedhealthus.imeddoctors.doctor.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.IntentUtils;
import com.imedhealthus.imeddoctors.doctor.models.AcceptedInsurance;
import com.imedhealthus.imeddoctors.doctor.models.Award;
import com.imedhealthus.imeddoctors.doctor.models.Certification;
import com.imedhealthus.imeddoctors.doctor.models.ClinicLocation;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.imedhealthus.imeddoctors.doctor.models.EducationDetail;
import com.imedhealthus.imeddoctors.doctor.models.Language;
import com.imedhealthus.imeddoctors.doctor.models.Membership;
import com.imedhealthus.imeddoctors.doctor.models.OverviewItem;
import com.imedhealthus.imeddoctors.doctor.models.PersonalInfo;
import com.imedhealthus.imeddoctors.doctor.models.ReferenceDetail;
import com.imedhealthus.imeddoctors.doctor.models.SkillTraining;
import com.imedhealthus.imeddoctors.doctor.models.SocialLink;
import com.imedhealthus.imeddoctors.doctor.models.Speciality;
import com.imedhealthus.imeddoctors.doctor.models.TrainingProviderDetail;
import com.imedhealthus.imeddoctors.doctor.models.WorkExperience;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/6/18.
 */

public class DoctorPersonalInfoFragment extends BaseFragment implements View.OnClickListener {

    //Personal Info

    @BindView(R.id.tv_dob)
    TextView tvDob;
    @BindView(R.id.tv_gender)
    TextView tvGender;

    @BindView(R.id.tv_occupation)
    TextView tvOccupation;
    @BindView(R.id.tv_marital_status)
    TextView tvMaritalStatus;

    @BindView(R.id.tv_office_phone)
    TextView tvOfficePhone;
    @BindView(R.id.tv_fax)
    TextView tvFax;

    @BindView(R.id.tv_address)
    TextView tvAddress;

    @BindView(R.id.iv_edit_personal_info)
    ImageView ivEditPersonalInfo;


    //Overview
    @BindView(R.id.iv_add_specialization)
    ImageView ivAddSpecialization;
    @BindView(R.id.cont_specializations)
    ViewGroup contSpecializations;
    @BindView(R.id.tv_no_specializations)
    TextView tvNoSpecializations;

    @BindView(R.id.iv_add_experience)
    ImageView ivAddExperience;
    @BindView(R.id.cont_experiences)
    ViewGroup contExperiences;
    @BindView(R.id.tv_no_experiences)
    TextView tvNoExperiences;

    @BindView(R.id.iv_add_education)
    ImageView ivAddEducation;
    @BindView(R.id.cont_education)
    ViewGroup contEducation;
    @BindView(R.id.tv_no_education)
    TextView tvNoEducation;

    @BindView(R.id.iv_add_language)
    ImageView ivAddLanguage;
    @BindView(R.id.cont_languages)
    ViewGroup contLanguages;
    @BindView(R.id.tv_no_languages)
    TextView tvNoLanguages;

    @BindView(R.id.iv_add_award)
    ImageView ivAddAward;
    @BindView(R.id.cont_awards)
    ViewGroup contAwards;
    @BindView(R.id.tv_no_awards)
    TextView tvNoAwards;

    @BindView(R.id.iv_add_membership)
    ImageView ivAddMembership;
    @BindView(R.id.cont_memberships)
    ViewGroup contMemberships;
    @BindView(R.id.tv_no_memberships)
    TextView tvNoMemberships;

    @BindView(R.id.iv_add_skill_training)
    ImageView ivAddSkillTraining;
    @BindView(R.id.cont_skill_trainings)
    ViewGroup contSkillTrainings;
    @BindView(R.id.tv_no_skill_trainings)
    TextView tvNoSkillTrainings;

    @BindView(R.id.iv_add_training_provider)
    ImageView ivAddTrainingProvider;
    @BindView(R.id.cont_training_provider)
    ViewGroup contTrainingProvider;
    @BindView(R.id.tv_no_training_provider)
    TextView tvNoTrainingProvider;

    @BindView(R.id.iv_add_certificate)
    ImageView ivAddCertificate;
    @BindView(R.id.cont_certificates)
    ViewGroup contCertificates;
    @BindView(R.id.tv_no_certificates)
    TextView tvNoCertificates;

    @BindView(R.id.iv_add_insurance)
    ImageView ivAddInsurance;
    @BindView(R.id.cont_insurances)
    ViewGroup contInsurances;
    @BindView(R.id.tv_no_insurances)
    TextView tvNoInsurances;

    @BindView(R.id.iv_add_reference)
    ImageView ivAddReference;
    @BindView(R.id.cont_references)
    ViewGroup contReferences;
    @BindView(R.id.tv_no_references)
    TextView tvNoReferences;

    @BindView(R.id.iv_add_location)
    ImageView ivAddLocation;
    @BindView(R.id.cont_locations)
    ViewGroup contLocations;
    @BindView(R.id.tv_no_locations)
    TextView tvNoLocations;

    @BindView(R.id.iv_add_social_link)
    ImageView ivAddSocialLink;
    @BindView(R.id.cont_social_links)
    ViewGroup contSocialLinks;
    @BindView(R.id.tv_no_social_links)
    TextView tvNoSocialLinks;

    private DoctorProfile profile;
    private boolean isEditAllowed;

    public static DoctorPersonalInfoFragment create(DoctorProfile profile, boolean isEditAllowed) {

        DoctorPersonalInfoFragment fragment = new DoctorPersonalInfoFragment();
        fragment.profile = profile;
        fragment.isEditAllowed = isEditAllowed;

        return fragment;

    }

    @Override
    public String getName() {
        return "DoctorPersonalInfoFragment";
    }

    @Override
    public String getTitle() {
        return "Overview";
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_doctor_personal_info, container, false);
        ButterKnife.bind(this, rootView);

        adjustView();
        return rootView;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SharedData.getInstance().isRefreshRequired()) {
            setData();
        }
    }


    private void adjustView() {

        int visibility = isEditAllowed ? View.VISIBLE : View.INVISIBLE;
        View.OnClickListener onClickListener = isEditAllowed ? this : null;

        ivEditPersonalInfo.setVisibility(visibility);

        ivAddSpecialization.setVisibility(visibility);
        ivAddExperience.setVisibility(visibility);
        ivAddEducation.setVisibility(visibility);

        ivAddLanguage.setVisibility(visibility);
        ivAddAward.setVisibility(visibility);
        ivAddMembership.setVisibility(visibility);

        ivAddSkillTraining.setVisibility(visibility);
        ivAddTrainingProvider.setVisibility(visibility);
        ivAddCertificate.setVisibility(visibility);

        ivAddInsurance.setVisibility(visibility);
        ivAddReference.setVisibility(visibility);
        ivAddLocation.setVisibility(visibility);

       // ivAddSocialLink.setVisibility(visibility);


        ivEditPersonalInfo.setOnClickListener(onClickListener);

        ivAddSpecialization.setOnClickListener(onClickListener);
        ivAddExperience.setOnClickListener(onClickListener);
        ivAddEducation.setOnClickListener(onClickListener);

        ivAddLanguage.setOnClickListener(onClickListener);
        ivAddAward.setOnClickListener(onClickListener);
        ivAddMembership.setOnClickListener(onClickListener);

        ivAddSkillTraining.setOnClickListener(onClickListener);
        ivAddTrainingProvider.setOnClickListener(onClickListener);
        ivAddCertificate.setOnClickListener(onClickListener);

        ivAddInsurance.setOnClickListener(onClickListener);
        ivAddReference.setOnClickListener(onClickListener);
        ivAddLocation.setOnClickListener(onClickListener);

        ivAddSocialLink.setOnClickListener(onClickListener);

    }

    private void setData() {

        if (profile == null) {
            return;
        }

        setPersonalInfo(profile.getPersonalInfo());
        setOverviewItems(profile.getSpecialities(), contSpecializations, tvNoSpecializations, OverviewItem.Type.speciality);
        setOverviewItems(profile.getWorkExperiences(), contExperiences, tvNoExperiences, OverviewItem.Type.experience);
        setOverviewItems(profile.getEducationDetails(), contEducation, tvNoEducation, OverviewItem.Type.education);



        setOverviewItems(profile.getAwards(), contAwards, tvNoAwards, OverviewItem.Type.award);
        setOverviewItems(profile.getMemberships(), contMemberships, tvNoMemberships, OverviewItem.Type.membership);

        setOverviewItems(profile.getSkillTrainings(), contSkillTrainings, tvNoSkillTrainings, OverviewItem.Type.skillTraining);
        setOverviewItems(profile.getTrainingProviderDetails(), contTrainingProvider, tvNoTrainingProvider, OverviewItem.Type.trainingProvider);
        setOverviewItems(profile.getCertifications(), contCertificates, tvNoCertificates, OverviewItem.Type.certificate);

        setOverviewItems(profile.getReferenceDetails(), contReferences, tvNoReferences, OverviewItem.Type.reference);
        setOverviewItems(profile.getClinicLocations(), contLocations, tvNoLocations, OverviewItem.Type.location);
        if(isEditAllowed){
            setLanguages(profile.getLanguages(), contLanguages, tvNoLanguages, OverviewItem.Type.language);
            setLanguages(profile.getAcceptedInsurances(), contInsurances, tvNoInsurances, OverviewItem.Type.insurance);
        }else {
            setSingleLineItemsForUserView(profile.getLanguages(), contLanguages, tvNoLanguages, OverviewItem.Type.language);
            setSingleLineItemsForUserView(profile.getAcceptedInsurances(), contInsurances, tvNoInsurances, OverviewItem.Type.insurance);

        }


        setSocialLinkIcons(profile.getSocialLinks());

    }

    private void setPersonalInfo(PersonalInfo personalInfo) {


/*
        tvDob.setText(personalInfo.getDobFormatted());
        tvGender.setText(personalInfo.getGender());
        String personalNumber = personalInfo.getPersonalPhone().isEmpty()?"N/A":personalInfo.getPersonalPhone();
        tvOccupation.setText("Personal # "+ personalNumber);
        tvMaritalStatus.setText(personalInfo.getMaritalStatus());

        String officeNumber = personalInfo.getOfficePhone().isEmpty()?"N/A":personalInfo.getOfficePhone();
        tvOfficePhone.setText("Office # "+ officeNumber);

        String faxNumber = personalInfo.getFax().isEmpty()?"N/A":personalInfo.getFax();
        tvFax.setText("Fax # "+faxNumber);

*/
        tvAddress.setText(personalInfo.getIntroduction());

    }

    public void setSingleLineItemsForUserView(List<?extends OverviewItem> items, ViewGroup container, TextView tvNoRecords, OverviewItem.Type type) {
        container.removeAllViews();
        View itemDisease = LayoutInflater.from(context).inflate(R.layout.list_item_single_line, container, false);

        String data = "";
        for (int i = 0; i < items.size(); i++) {

            OverviewItem item = items.get(i);
            data = data +(i+1)+". "+item.getDisplayTitle()+"\n";


        }

        if (items.size() == 0) {
            tvNoRecords.setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.setVisibility(View.GONE);
            ((TextView)itemDisease.findViewById(R.id.tv_title)).setText(data);
            container.addView(itemDisease);

        }
    }

    public void setLanguages(List<?extends OverviewItem> items, ViewGroup container, TextView tvNoRecords, OverviewItem.Type type) {
        container.removeAllViews();

        for (int i = 0; i < items.size(); i++) {

            OverviewItem item = items.get(i);
            View itemDisease = LayoutInflater.from(context).inflate(R.layout.list_item_doctor_language, container, false);

            ((TextView)itemDisease.findViewById(R.id.tv_title)).setText(item.getDisplayTitle());



            View ivEdit = itemDisease.findViewById(R.id.iv_edit_item);
            View ivDelete = itemDisease.findViewById(R.id.iv_delete_item);

            if (isEditAllowed) {

                ivEdit.setTag(i);
                ivEdit.setTag(R.string.overview_item_type, type.ordinal());
                ivEdit.setOnClickListener(this);

                ivDelete.setTag(i);
                ivDelete.setTag(R.string.overview_item_type, type.ordinal());
                ivDelete.setOnClickListener(this);

            }else {

                ivEdit.setVisibility(View.GONE);
                ivDelete.setVisibility(View.GONE);

            }

            container.addView(itemDisease);

        }

        if (items.size() == 0) {
            tvNoRecords.setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.setVisibility(View.GONE);
        }
    }

    private void setOverviewItems(List<?extends OverviewItem> items, ViewGroup container, TextView tvNoRecords, OverviewItem.Type type) {

        container.removeAllViews();

        for (int i = 0; i < items.size(); i++) {

            OverviewItem item = items.get(i);
            View itemDisease = LayoutInflater.from(context).inflate(R.layout.list_item_doctor_overview, container, false);

            ((TextView)itemDisease.findViewById(R.id.tv_title)).setText(item.getDisplayTitle());
            ((TextView)itemDisease.findViewById(R.id.tv_details)).setText(item.getDisplayDetail());

            if(type!=OverviewItem.Type.location)
                ((TextView)itemDisease.findViewById(R.id.tv_duration)).setText(item.getDisplayDuration());
            else
                ((TextView)itemDisease.findViewById(R.id.tv_duration)).setVisibility(View.GONE);

            View ivEdit = itemDisease.findViewById(R.id.iv_edit_item);
            View ivDelete = itemDisease.findViewById(R.id.iv_delete_item);

            if (isEditAllowed) {

                ivEdit.setTag(i);
                ivEdit.setTag(R.string.overview_item_type, type.ordinal());
                ivEdit.setOnClickListener(this);

                ivDelete.setTag(i);
                ivDelete.setTag(R.string.overview_item_type, type.ordinal());
                ivDelete.setOnClickListener(this);

            }else {

                ivEdit.setVisibility(View.GONE);
                ivDelete.setVisibility(View.GONE);

            }

            container.addView(itemDisease);

        }

        if (items.size() == 0) {
            tvNoRecords.setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.setVisibility(View.GONE);
        }

    }

    private void setSocialLinkIcons(List<SocialLink> socialLinks) {

        contSocialLinks.removeAllViews();

        for (int i = 0; i < socialLinks.size(); i++) {

            OverviewItem item = socialLinks.get(i);
            View itemSocial = LayoutInflater.from(context).inflate(R.layout.list_item_social_links, contSocialLinks, false);

            // ivReviews.setImageResource(R.drawable.ic_review_gray);

            ImageView social =((ImageView)itemSocial.findViewById(R.id.iv_social_link));
            if(item.getDisplayTitle().equalsIgnoreCase("Facebook")){
                social.setImageResource(R.drawable.ic_social_facebook);
            }else if(item.getDisplayTitle().equalsIgnoreCase("Instagram")){
                social.setImageResource(R.drawable.ic_social_instagram);
            }else if(item.getDisplayTitle().equalsIgnoreCase("Twitter")){
                social.setImageResource(R.drawable.ic_social_twitter);
            }else if(item.getDisplayTitle().equalsIgnoreCase("youtube")){
                social.setImageResource(R.drawable.ic_youtube);
            }else if(item.getDisplayTitle().equalsIgnoreCase("Google+")){
                social.setImageResource(R.drawable.ic_google);
            }else if(item.getDisplayTitle().equalsIgnoreCase("Pinterest")){
                social.setImageResource(R.drawable.ic_pinterest);
            }else if(item.getDisplayTitle().equalsIgnoreCase("Tumblr")){
                social.setImageResource(R.drawable.ic_tumblr);
            }else if(item.getDisplayTitle().equalsIgnoreCase("Flikr")){
                social.setImageResource(R.drawable.ic_flickr);
            }else if(item.getDisplayTitle().equalsIgnoreCase("Redit")){
                social.setImageResource(R.drawable.ic_reddit);
            }else{
                social.setImageResource(R.drawable.ic_social_wifi);
            }


            social.setTag(item.getDisplayDetail());
            social.setOnClickListener(this);



            contSocialLinks.addView(itemSocial);

        }

        if (socialLinks.size() == 0) {
          //  tvNoSocialLinks.setVisibility(View.VISIBLE);
        }else {
            tvNoSocialLinks.setVisibility(View.GONE);
        }

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_edit_personal_info:
                openEditPersonalInfo();
                break;

            case R.id.iv_add_specialization:
                openAddSpeciality();
                break;

            case R.id.iv_add_experience:
                openAddExperience();
                break;

            case R.id.iv_add_education:
                openAddEducation();
                break;

            case R.id.iv_add_language:
                openAddLanguage();
                break;

            case R.id.iv_add_award:
                openAddAward();
                break;

            case R.id.iv_add_membership:
                openAddMembership();
                break;

            case R.id.iv_add_skill_training:
                openAddSkillTraining();
                break;

            case R.id.iv_add_training_provider:
                openAddTrainingProvider();
                break;

            case R.id.iv_add_certificate:
                openAddCertificate();
                break;

            case R.id.iv_add_insurance:
                openAddInsurance();
                break;

            case R.id.iv_add_reference:
                openAddReference();
                break;

            case R.id.iv_add_location:
                openAddLocation();
                break;

            case R.id.iv_add_social_link:
                openAddSocialLink();
                break;

            case R.id.iv_edit_item:
                openEditOverviewItem(view);
                break;

            case R.id.iv_delete_item:
                showDeleteOverviewItem(view);
                break;
            case R.id.iv_social_link:
                IntentUtils.openUrl(context,(String) view.getTag());
                break;

        }
    }

    private void openEditPersonalInfo() {
        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.EditDoctorPersonalInfo.ordinal());
        ((BaseActivity)context).startActivity(intent, true);
    }

    private void openEditOverviewItem(View view) {

        int idx = (int) view.getTag();
        int typeIdx = (int) view.getTag(R.string.overview_item_type);
        OverviewItem.Type type = OverviewItem.Type.values()[typeIdx];

        switch (type) {
            case speciality:
                openEditSpeciality(idx);
                break;

            case experience:
                openEditExperience(idx);
                break;

            case education:
                openEditEducation(idx);
                break;

            case language:
                openEditLanguage(idx);
                break;

            case award:
                openEditAward(idx);
                break;

            case membership:
                openEditMembership(idx);
                break;

            case skillTraining:
                openEditSkillTraining(idx);
                break;

            case trainingProvider:
                openEditTrainingProvider(idx);
                break;

            case certificate:
                openEditCertificate(idx);
                break;

            case insurance:
                openEditInsurance(idx);
                break;

            case reference:
                openEditReference(idx);
                break;

            case location:
                openEditLocation(idx);
                break;


        }


    }

    private void showDeleteOverviewItem(View view) {

        int idx = (int) view.getTag();
        int typeIdx = (int) view.getTag(R.string.overview_item_type);
        OverviewItem.Type type = OverviewItem.Type.values()[typeIdx];

        switch (type) {
            case speciality:
                deleteSpeciality(idx);
                break;

            case experience:
                deleteExperience(idx);
                break;

            case education:
                deleteEducation(idx);
                break;

            case language:
                deleteLanguage(idx);
                break;

            case award:
                deleteAward(idx);
                break;

            case membership:
                deleteMembership(idx);
                break;

            case skillTraining:
                deleteSkillTraining(idx);
                break;

            case trainingProvider:
                deleteTrainingProvider(idx);
                break;

            case certificate:
                deleteCertificate(idx);
                break;

            case insurance:
                deleteInsurance(idx);
                break;

            case reference:
                deleteReference(idx);
                break;

            case location:
                deleteLocation(idx);
                break;

            case socialLink:
                deleteSocialLink(idx);
                break;

        }

    }

    private void showDeleteAlert(DialogInterface.OnClickListener acceptClickListener) {

        new AlertDialog.Builder(context)
                .setTitle("Confirm")
                .setMessage("Are you sure you want to delete this record?")
                .setPositiveButton("Yes", acceptClickListener)
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }})
                .show();

    }


    //Speciality
    private void openAddSpeciality() {

        SharedData.getInstance().setSelectedSpeciality(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditSpeciality.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditSpeciality(int idx) {

        if (idx < 0 || idx >= profile.getSpecialities().size()) {
            return;
        }

        Speciality speciality = profile.getSpecialities().get(idx);
        SharedData.getInstance().setSelectedSpeciality(speciality);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditSpeciality.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteSpeciality(int idx) {

        if (idx < 0 || idx >= profile.getSpecialities().size()) {
            return;
        }

        final Speciality speciality = profile.getSpecialities().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteSpeciality(speciality);

            }
        });

    }


    //Experience
    private void openAddExperience() {

        SharedData.getInstance().setSelectedWorkExperience(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditExperience.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditExperience(int idx) {

        if (idx < 0 || idx >= profile.getWorkExperiences().size()) {
            return;
        }

        WorkExperience experience = profile.getWorkExperiences().get(idx);
        SharedData.getInstance().setSelectedWorkExperience(experience);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditExperience.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteExperience(int idx) {

        if (idx < 0 || idx >= profile.getWorkExperiences().size()) {
            return;
        }

        final WorkExperience experience = profile.getWorkExperiences().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteExperience(experience);

            }
        });

    }


    //Education
    private void openAddEducation() {

        SharedData.getInstance().setSelectedEducationDetail(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditEducationDetail.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditEducation(int idx) {

        if (idx < 0 || idx >= profile.getEducationDetails().size()) {
            return;
        }

        EducationDetail educationDetail = profile.getEducationDetails().get(idx);
        SharedData.getInstance().setSelectedEducationDetail(educationDetail);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditEducationDetail.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteEducation(int idx) {

        if (idx < 0 || idx >= profile.getEducationDetails().size()) {
            return;
        }

        final EducationDetail educationDetail = profile.getEducationDetails().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteEducation(educationDetail);

            }
        });

    }


    //Language
    private void openAddLanguage() {

        SharedData.getInstance().setSelectedLanguage(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditLanguage.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditLanguage(int idx) {

        if (idx < 0 || idx >= profile.getLanguages().size()) {
            return;
        }

        Language language = profile.getLanguages().get(idx);
        SharedData.getInstance().setSelectedLanguage(language);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditLanguage.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteLanguage(int idx) {

        if (idx < 0 || idx >= profile.getLanguages().size()) {
            return;
        }

        final Language language = profile.getLanguages().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteLanguage(language);

            }
        });

    }


    //Award
    private void openAddAward() {

        SharedData.getInstance().setSelectedAward(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditAward.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditAward(int idx) {

        if (idx < 0 || idx >= profile.getAwards().size()) {
            return;
        }

        Award award = profile.getAwards().get(idx);
        SharedData.getInstance().setSelectedAward(award);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditAward.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteAward(int idx) {

        if (idx < 0 || idx >= profile.getAwards().size()) {
            return;
        }

        final Award award = profile.getAwards().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteAward(award);

            }
        });

    }


    //Membership
    private void openAddMembership() {

        SharedData.getInstance().setSelectedMembership(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditMembership.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditMembership(int idx) {

        if (idx < 0 || idx >= profile.getMemberships().size()) {
            return;
        }

        Membership membership = profile.getMemberships().get(idx);
        SharedData.getInstance().setSelectedMembership(membership);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditMembership.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteMembership(int idx) {

        if (idx < 0 || idx >= profile.getMemberships().size()) {
            return;
        }

        final Membership membership = profile.getMemberships().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteMembership(membership);

            }
        });

    }


    //SkillTraining
    private void openAddSkillTraining() {

        SharedData.getInstance().setSelectedSkillTraining(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditSkillTraining.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditSkillTraining(int idx) {

        if (idx < 0 || idx >= profile.getSkillTrainings().size()) {
            return;
        }

        SkillTraining skillTraining = profile.getSkillTrainings().get(idx);
        SharedData.getInstance().setSelectedSkillTraining(skillTraining);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditSkillTraining.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteSkillTraining(int idx) {

        if (idx < 0 || idx >= profile.getSkillTrainings().size()) {
            return;
        }

        final SkillTraining skillTraining = profile.getSkillTrainings().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteSkillTraining(skillTraining);

            }
        });

    }


    //TrainingProvider
    private void openAddTrainingProvider() {

        SharedData.getInstance().setSelectedTrainingProviderDetail(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditTrainingProvider.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditTrainingProvider(int idx) {

        if (idx < 0 || idx >= profile.getTrainingProviderDetails().size()) {
            return;
        }

        TrainingProviderDetail trainingProviderDetail = profile.getTrainingProviderDetails().get(idx);
        SharedData.getInstance().setSelectedTrainingProviderDetail(trainingProviderDetail);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditTrainingProvider.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteTrainingProvider(int idx) {

        if (idx < 0 || idx >= profile.getTrainingProviderDetails().size()) {
            return;
        }

        final TrainingProviderDetail trainingProviderDetail = profile.getTrainingProviderDetails().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteTrainingProvider(trainingProviderDetail);

            }
        });

    }


    //Certificate
    private void openAddCertificate() {

        SharedData.getInstance().setSelectedCertification(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditCertificate.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditCertificate(int idx) {

        if (idx < 0 || idx >= profile.getCertifications().size()) {
            return;
        }

        Certification certification = profile.getCertifications().get(idx);
        SharedData.getInstance().setSelectedCertification(certification);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditCertificate.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteCertificate(int idx) {

        if (idx < 0 || idx >= profile.getCertifications().size()) {
            return;
        }

        final Certification certification = profile.getCertifications().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteCertificate(certification);

            }
        });

    }


    //Insurance
    private void openAddInsurance() {

        SharedData.getInstance().setSelectedAcceptedInsurance(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditAcceptedInsurance.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditInsurance(int idx) {

        if (idx < 0 || idx >= profile.getAcceptedInsurances().size()) {
            return;
        }

        AcceptedInsurance insurance = profile.getAcceptedInsurances().get(idx);
        SharedData.getInstance().setSelectedAcceptedInsurance(insurance);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditAcceptedInsurance.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteInsurance(int idx) {

        if (idx < 0 || idx >= profile.getAcceptedInsurances().size()) {
            return;
        }

        final AcceptedInsurance insurance = profile.getAcceptedInsurances().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteInsurance(insurance);

            }
        });

    }


    //Reference
    private void openAddReference() {

        SharedData.getInstance().setSelectedReferenceDetail(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditReference.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditReference(int idx) {

        if (idx < 0 || idx >= profile.getReferenceDetails().size()) {
            return;
        }

        ReferenceDetail referenceDetail = profile.getReferenceDetails().get(idx);
        SharedData.getInstance().setSelectedReferenceDetail(referenceDetail);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditReference.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteReference(int idx) {

        if (idx < 0 || idx >= profile.getReferenceDetails().size()) {
            return;
        }

        final ReferenceDetail referenceDetail = profile.getReferenceDetails().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteReference(referenceDetail);

            }
        });

    }


    //Location
    private void openAddLocation() {

        SharedData.getInstance().setSelectedClinicLocation(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditLocation.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    private void openEditLocation(int idx) {

        if (idx < 0 || idx >= profile.getClinicLocations().size()) {
            return;
        }

        ClinicLocation location = profile.getClinicLocations().get(idx);
        SharedData.getInstance().setSelectedClinicLocation(location);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditLocation.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    public void deleteLocation(int idx) {

        if (idx < 0 || idx >= profile.getClinicLocations().size()) {
            return;
        }

        final ClinicLocation location = profile.getClinicLocations().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteLocation(location);

            }
        });

    }


    //SocialLink
    private void openAddSocialLink() {

        SharedData.getInstance().setSelectedSocialLink(profile.getSocialLinks());

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditSocialLink.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }



    public void deleteSocialLink(int idx) {

        if (idx < 0 || idx >= profile.getSocialLinks().size()) {
            return;
        }

        final SocialLink socialLink = profile.getSocialLinks().get(idx);
        showDeleteAlert(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                deleteSocialLink(socialLink);

            }
        });

    }


    //Web services
    private void deleteSpeciality(final Speciality speciality) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteSpeciality(speciality, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Specialty detail deleted successfully");

                List<Speciality> specialities = profile.getSpecialities();
                specialities.remove(speciality);
                setOverviewItems(specialities, contSpecializations, tvNoSpecializations, OverviewItem.Type.speciality);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void deleteExperience(final WorkExperience workExperience) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteWorkExperience(workExperience, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Work experience detail deleted successfully");

                List<WorkExperience> experiences = profile.getWorkExperiences();
                experiences.remove(workExperience);
                setOverviewItems(experiences, contExperiences, tvNoExperiences, OverviewItem.Type.experience);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void deleteEducation(final EducationDetail educationDetail) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteEducationDetail(educationDetail, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Education detail deleted successfully");

                List<EducationDetail> educationDetails = profile.getEducationDetails();
                educationDetails.remove(educationDetail);
                setOverviewItems(educationDetails, contEducation, tvNoAwards, OverviewItem.Type.education);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }


    private void deleteLanguage(final Language language) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteLanguage(language, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Language detail deleted successfully");

                List<Language> languages = profile.getLanguages();
                languages.remove(language);
                setOverviewItems(languages, contLanguages, tvNoLanguages, OverviewItem.Type.language);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void deleteAward(final Award award) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteAward(award, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Award detail deleted successfully");

                List<Award> awards = profile.getAwards();
                awards.remove(award);
                setOverviewItems(awards, contAwards, tvNoAwards, OverviewItem.Type.award);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void deleteMembership(final Membership membership) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteMembership(membership, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Membership detail deleted successfully");

                List<Membership> memberships = profile.getMemberships();
                memberships.remove(membership);
                setOverviewItems(memberships, contMemberships, tvNoMemberships, OverviewItem.Type.membership);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }


    private void deleteSkillTraining(final SkillTraining skillTraining) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteSkillTraining(skillTraining, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Skill training deleted successfully");

                List<SkillTraining> skillTrainings = profile.getSkillTrainings();
                skillTrainings.remove(skillTraining);
                setOverviewItems(skillTrainings, contSkillTrainings, tvNoSkillTrainings, OverviewItem.Type.skillTraining);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void deleteTrainingProvider(final TrainingProviderDetail trainingProviderDetail) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteTrainingProviderDetail(trainingProviderDetail, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Training provider detail deleted successfully");

                List<TrainingProviderDetail> trainingProviderDetails = profile.getTrainingProviderDetails();
                trainingProviderDetails.remove(trainingProviderDetail);
                setOverviewItems(trainingProviderDetails, contTrainingProvider, tvNoTrainingProvider, OverviewItem.Type.trainingProvider);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void deleteCertificate(final Certification certification) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteCertification(certification, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "License/Certificate detail deleted successfully");

                List<Certification> certifications = profile.getCertifications();
                certifications.remove(certification);
                setOverviewItems(certifications, contCertificates, tvNoCertificates, OverviewItem.Type.certificate);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }


    private void deleteInsurance(final AcceptedInsurance insurance) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteAcceptedInsurance(insurance, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Insurance detail deleted successfully");

                List<AcceptedInsurance> insurances = profile.getAcceptedInsurances();
                insurances.remove(insurance);
                setOverviewItems(insurances, contInsurances, tvNoInsurances, OverviewItem.Type.insurance);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void deleteReference(final ReferenceDetail referenceDetail) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteReferenceDetail(referenceDetail, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Reference detail deleted successfully");

                List<ReferenceDetail> referenceDetails = profile.getReferenceDetails();
                referenceDetails.remove(referenceDetail);
                setOverviewItems(referenceDetails, contReferences, tvNoReferences, OverviewItem.Type.reference);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void deleteLocation(final ClinicLocation clinicLocation) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteClinicLocation(clinicLocation, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Location detail deleted successfully");

                List<ClinicLocation> locations = profile.getClinicLocations();
                locations.remove(clinicLocation);
                setOverviewItems(locations, contLocations, tvNoLocations, OverviewItem.Type.location);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }


    private void deleteSocialLink(final SocialLink socialLink) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteSocialLink(socialLink,Constants.UserType.Doctor, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Social link detail deleted successfully");

                List<SocialLink> socialLinks = profile.getSocialLinks();
                socialLinks.remove(socialLink);
                setOverviewItems(socialLinks, contSocialLinks, tvNoSocialLinks, OverviewItem.Type.socialLink);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

}
