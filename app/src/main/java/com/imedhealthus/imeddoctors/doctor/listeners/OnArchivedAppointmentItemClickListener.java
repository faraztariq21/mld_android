package com.imedhealthus.imeddoctors.doctor.listeners;

/**
 * Created by Malik Hamid on 2/22/2018.
 */

public interface OnArchivedAppointmentItemClickListener {

        void onArchivedAppointmentItemDetailClick(int idx);
        void onArchivedAppointmentItemUndoCompletionClick(int idx);

}
