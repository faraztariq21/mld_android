package com.imedhealthus.imeddoctors.doctor.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.adapters.ServicesListAdapter;
import com.imedhealthus.imeddoctors.doctor.listeners.OnServiceItemClickListener;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.imedhealthus.imeddoctors.doctor.models.Service;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/6/18.
 */

public class ServicesFragment extends BaseFragment implements OnServiceItemClickListener {

    @BindView(R.id.iv_add_service)
    ImageView ivAddService;
    @BindView(R.id.tv_add_service)
    TextView tvAddService;

    @BindView(R.id.rv_services)
    RecyclerView rvServices;
    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;

    private ServicesListAdapter adapterServices;

    private DoctorProfile profile;
    private boolean isPersonalProfile;

    public static ServicesFragment create(DoctorProfile profile, boolean isPersonalProfile) {

        ServicesFragment fragment = new ServicesFragment();

        fragment.profile = profile;
        fragment.isPersonalProfile = isPersonalProfile;

        return fragment;

    }

    @Override
    public String getName() {
        return "ServicesFragment";
    }

    @Override
    public String getTitle() {
        return "Services";
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_doctor_services, container, false);
        ButterKnife.bind(this, rootView);

        initHelper();
        return rootView;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SharedData.getInstance().isRefreshRequired()) {
            setData();
        }
    }

    private void initHelper(){

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rvServices.setLayoutManager(layoutManager);

        adapterServices = new ServicesListAdapter(tvNoRecords, isPersonalProfile, this);
        rvServices.setAdapter(adapterServices);

        if (isPersonalProfile) {
            ivAddService.setVisibility(View.VISIBLE);
            tvAddService.setVisibility(View.VISIBLE);
        }else {
            ivAddService.setVisibility(View.GONE);
            tvAddService.setVisibility(View.GONE);
        }

    }

    private void setData() {

        if (profile == null) {
            return;
        }
        adapterServices.setServices(profile.getServices());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_add_service:
            case R.id.tv_add_service:
                openAddService();
                break;
        }
    }

    private void openAddService() {

        SharedData.getInstance().setSelectedService(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditService.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    //On service item click listener
    @Override
    public void onServiceDetailClick(int idx) {

    }

    @Override
    public void onServiceEditClick(int idx) {

        if (idx < 0 || idx >= profile.getServices().size()) {
            return;
        }

        Service service = profile.getServices().get(idx);
        SharedData.getInstance().setSelectedService(service);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditService.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    @Override
    public void onServiceDeleteClick(int idx) {

        if (idx < 0 || idx >= profile.getServices().size()) {
            return;
        }

        final Service service = profile.getServices().get(idx);
        new AlertDialog.Builder(context)
                .setTitle("Confirm")
                .setMessage("Are you sure you want to delete this service?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        deleteService(service);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }})
                .show();


    }


    //Webservices
    private void deleteService(final Service service) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.deleteService(service, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Service deleted successfully");

                List<Service> services = profile.getServices();
                services.remove(services);
                adapterServices.setServices(services);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }


}
