package com.imedhealthus.imeddoctors.doctor.fragments;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.CustomCitySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomCountrySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomStateSpinnerAdapter;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.Country;
import com.imedhealthus.imeddoctors.common.models.ImageSelector;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.State;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.common.utils.UploadImage;
import com.imedhealthus.imeddoctors.doctor.models.Certification;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.imedhealthus.imeddoctors.doctor.models.Membership;
import com.imedhealthus.imeddoctors.doctor.models.WorkExperience;
import com.soundcloud.android.crop.Crop;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddEditCertificationFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener, ImageSelector.OnImageSelectionListener {

    @BindView(R.id.et_certificate_no)
    EditText etCertificateNo;
    @BindView(R.id.et_type)
    EditText etType;

    @BindView(R.id.tv_issue_date)
    TextView tvIssueDate;
    @BindView(R.id.tv_expiry_date)
    TextView tvExpiryDate;


    @BindView(R.id.sp_state)
    Spinner spState;
    @BindView(R.id.sp_country)
    Spinner spCountry;

    private WorkExperience workExperience;
    private List<Country> countries;
    private CustomCountrySpinnerAdapter customCountryAdapter;
    private CustomStateSpinnerAdapter stateAdapter;
    private List<State> states;

    private CustomCitySpinnerAdapter cityAdapter;
    private Membership membership;
    private Certification certification;

    private ImageSelector imageSelector;
    private boolean isImageSelected = false;
    private Uri selectedImgUri;

    public AddEditCertificationFragment() {
        certification = SharedData.getInstance().getSelectedCertification();
    }


    @Override
    public String getName() {
        return "AddEditCertificationFragment";
    }

    @Override
    public String getTitle() {
        if (certification == null) {
            return "Add License/Certificate";
        } else {
            return "Edit License/Certificate";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_certification, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    private void setData() {

        if (certification == null) {
            return;
        }

        etCertificateNo.setText(certification.getCertificateNumber());
        etType.setText(certification.getType());

        tvIssueDate.setText(certification.getIssueDateFormatted());
        tvExpiryDate.setText(certification.getExpiryDateFormatted());

        setSpinnerData(certification.getCountryId(), certification.getStateId());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_choose_file:
                openFileSelector();
                break;

            case R.id.btn_save:
                if (validateFields()) {
                    if (certification != null && !isImageSelected) {
                        addEditCertification(certification.getFileUrl());
                    } else {
                        uploadFile();
                    }
                }
                break;

            case R.id.tv_issue_date:
                openDatePicker(tvIssueDate.getText().toString(), "issueDate");
                break;

            case R.id.tv_expiry_date:
                openDatePicker(tvExpiryDate.getText().toString(), "expiryDate");
                break;

        }
    }

    private void openDatePicker(String dateStr, String tag) {

        Date date;
        if (dateStr.isEmpty()) {
            date = new Date();
        } else {
            date = GenericUtils.unFormatDate(dateStr);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setVersion(DatePickerDialog.Version.VERSION_2);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.light_blue));

        datePickerDialog.show(((Activity) context).getFragmentManager(), tag);

    }

    private void openFileSelector() {

        if (imageSelector == null) {
            imageSelector = new ImageSelector(this, (Activity) context, this);
        }
        imageSelector.showSourceAlert();

    }


    //Date picker callback
    @Override
    public void onDateSet(DatePickerDialog view, int year, int month, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        if (view.getTag().equalsIgnoreCase("issueDate")) {
            tvIssueDate.setText(GenericUtils.formatDate(calendar.getTime()));
        } else {
            tvExpiryDate.setText(GenericUtils.formatDate(calendar.getTime()));
        }

    }


    //Image selection
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ImageSelector.CAMERA_REQUEST_CODE:
            case ImageSelector.GALLERY_REQUEST_CODE:
            case Crop.REQUEST_CROP:
                if (imageSelector != null) {
                    imageSelector.onActivityResult(requestCode, resultCode, data);
                }
                break;

        }

    }

    @Override
    public void onImageSelected(Bitmap bitmap, Uri uri, Uri fullImageUri) {
        isImageSelected = true;
        selectedImgUri = uri;
    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etCertificateNo.getText().toString().isEmpty() || etType.getText().toString().isEmpty() ||
                tvIssueDate.getText().toString().isEmpty() || tvExpiryDate.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        } else if ((certification != null && certification.getFileUrl().isEmpty()) || (certification == null && !isImageSelected)) {
            errorMsg = "Kindly select some file to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustCertifications(RetrofitJSONResponse response, Certification updatedCertification) {

        if (certification == null) {//Add

            JSONObject certificationJson = response.optJSONObject("Certificate");
            if (certificationJson != null) {
                Certification newCertification = new Certification(certificationJson);
                DoctorProfile profile = SharedData.getInstance().getSelectedDoctorProfile();
                profile.getCertifications().add(newCertification);
            }

        } else {

            certification.setCertificateNumber(updatedCertification.getCertificateNumber());
            certification.setType(updatedCertification.getType());

            certification.setIssueDate(updatedCertification.getIssueDate());
            certification.setIssueDateFormatted(updatedCertification.getIssueDateFormatted());
            certification.setExpiryDate(updatedCertification.getExpiryDate());
            certification.setExpiryDateFormatted(updatedCertification.getExpiryDateFormatted());

            certification.setStateId(updatedCertification.getStateId());
            certification.setStateName(updatedCertification.getStateName());
            certification.setCountryId(updatedCertification.getCountryId());
            certification.setCountryName(updatedCertification.getCountryName());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }

    public void setSpinnerData(final String countryId, final String stateId) {

        countries = SharedData.getInstance().getCountriesList();
        customCountryAdapter = new CustomCountrySpinnerAdapter(getContext(), R.layout.custom_spinner_item, countries);
        spCountry.setAdapter(customCountryAdapter);
        int selectedCountryIndex = SharedData.getInstance().getCountryIndexInList(countries, countryId);
        spCountry.setSelection(selectedCountryIndex);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                states = SharedData.getInstance().
                        getStatesListAgainstCountryByCountryId(countryId);
                stateAdapter = new CustomStateSpinnerAdapter(getContext(), R.layout.custom_spinner_item, states);
                spState.setAdapter(stateAdapter);
                int selectedStateIndex = SharedData.getInstance().getStateIndexInList(states, stateId);
                spState.setSelection(selectedStateIndex);


            }
        }, 100);


    }

    private void initView() {

        countries = SharedData.getInstance().getCountriesList();
        customCountryAdapter = new CustomCountrySpinnerAdapter(getContext(), R.layout.custom_spinner_item, countries);
        spCountry.setAdapter(customCountryAdapter);

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                states = SharedData.getInstance().
                        getStatesListAgainstCountryByCountryId(Integer.toString(((Country) adapterView.getSelectedItem()).countryId));
                stateAdapter = new CustomStateSpinnerAdapter(getContext(), R.layout.custom_spinner_item, states);
                spState.setAdapter(stateAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    //Webservices
    private void addEditCertification(String fileUrl) {

        if (!AlertUtils.isShowingProgress()) {
            AlertUtils.showProgress((Activity) context);
        }

        final Certification updatedCertification = new Certification();
        if (certification != null) {
            updatedCertification.setId(certification.getId());
        }

        updatedCertification.setCertificateNumber(etCertificateNo.getText().toString());
        updatedCertification.setType(etType.getText().toString());
        updatedCertification.setFileUrl(fileUrl);

        updatedCertification.setIssueDateFormatted(tvIssueDate.getText().toString());
        Date date = GenericUtils.unFormatDate(tvIssueDate.getText().toString());
        updatedCertification.setIssueDate(GenericUtils.getTimeDateString(date));

        updatedCertification.setExpiryDateFormatted(tvExpiryDate.getText().toString());
        date = GenericUtils.unFormatDate(tvExpiryDate.getText().toString());
        updatedCertification.setExpiryDate(GenericUtils.getTimeDateString(date));

        if (spState.getSelectedItem() == null || ((State) spState.getSelectedItem()).getStateId().equals("0")) {
            updatedCertification.setStateId("0");
            updatedCertification.setStateName("");
        } else {
            updatedCertification.setStateId(((State) spState.getSelectedItem()).getStateId());
            updatedCertification.setStateName(((State) spState.getSelectedItem()).getStateName());
        }

        if (spCountry.getSelectedItem() == null || Integer.toString(((Country) spCountry.getSelectedItem()).countryId).equals("0")) {
            updatedCertification.setCountryId("0");
            updatedCertification.setCountryName("");
        } else {
            updatedCertification.setCountryId(Integer.toString(((Country) spCountry.getSelectedItem()).countryId));
            updatedCertification.setCountryName(((Country) spCountry.getSelectedItem()).getCountryName());
        }

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditCertification(userId, updatedCertification, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                adjustCertifications(response, updatedCertification);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "License/Certificate details saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

    private void uploadFile() {

        AlertUtils.showProgress(getActivity());

        final String uploadPath = Constants.FTP.patientDataPath + (new Date()).getTime() + ".png";

        UploadImage uploadImage = new UploadImage(uploadPath, selectedImgUri.getPath(), new UploadImage.OnImageUploadCompletionListener() {
            @Override
            public void onImageUploadComplete(boolean success, int index, String uploadPath) {

                if (!success) {
                    AlertUtils.dismissProgress();
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, "Can't upload image now. Kindly try later.");
                    return;
                }

                addEditCertification(uploadPath);

            }
        });
        uploadImage.execute();

    }

}
