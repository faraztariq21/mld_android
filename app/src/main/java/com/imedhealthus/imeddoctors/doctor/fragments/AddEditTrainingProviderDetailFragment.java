package com.imedhealthus.imeddoctors.doctor.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.CustomCitySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomCountrySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomStateSpinnerAdapter;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.City;
import com.imedhealthus.imeddoctors.common.models.Country;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.State;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.imedhealthus.imeddoctors.doctor.models.TrainingProviderDetail;
import com.imedhealthus.imeddoctors.doctor.models.WorkExperience;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddEditTrainingProviderDetailFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.et_training_type)
    EditText etTrainingType;
    @BindView(R.id.et_title)
    EditText etTitle;

    @BindView(R.id.et_institute)
    EditText etInstitute;
    @BindView(R.id.et_department)
    EditText etDepartment;
    @BindView(R.id.et_supervisor)
    EditText etSupervisor;

    @BindView(R.id.tv_start_date)
    TextView tvStartDate;
    @BindView(R.id.tv_end_date)
    TextView tvEndDate;

    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_address)
    EditText etAddress;

    @BindView(R.id.sp_city)
    Spinner spCity;
    @BindView(R.id.sp_state)
    Spinner spState;
    @BindView(R.id.sp_country)
    Spinner spCountry;

    private WorkExperience workExperience;
    private List<Country> countries;
    private CustomCountrySpinnerAdapter customCountryAdapter;
    private CustomStateSpinnerAdapter stateAdapter;
    private List<State> states;
    private List<City> cities;
    private CustomCitySpinnerAdapter cityAdapter;

    @BindView(R.id.et_notes)
    EditText etNotes;

    private TrainingProviderDetail trainingProviderDetail;

    public AddEditTrainingProviderDetailFragment() {
        trainingProviderDetail = SharedData.getInstance().getSelectedTrainingProviderDetail();
    }


    @Override
    public String getName() {
        return "AddEditTrainingProviderDetailFragment";
    }

    @Override
    public String getTitle() {
        if (trainingProviderDetail == null) {
            return "Add Type of Training Details";
        }else {
            return "Edit Type of Training Details";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_training_provider_detail, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    public void setSpinnerData(final String countryId,final String stateId,final String cityId){

        countries = SharedData.getInstance().getCountriesList();
        customCountryAdapter = new CustomCountrySpinnerAdapter(getContext(), R.layout.custom_spinner_item,countries);
        spCountry.setAdapter(customCountryAdapter);
        int selectedCountryIndex = SharedData.getInstance().getCountryIndexInList(countries,countryId);
        spCountry.setSelection(selectedCountryIndex);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                states = SharedData.getInstance().
                        getStatesListAgainstCountryByCountryId(countryId);
                stateAdapter = new CustomStateSpinnerAdapter(getContext(), R.layout.custom_spinner_item, states);
                spState.setAdapter(stateAdapter);
                int selectedStateIndex =SharedData.getInstance().getStateIndexInList(states,stateId);
                spState.setSelection(selectedStateIndex);


                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cities = SharedData.getInstance().
                                getCitiesListByStateId(stateId);
                        cityAdapter = new CustomCitySpinnerAdapter(getContext(), R.layout.custom_spinner_item, cities);
                        spCity.setAdapter(cityAdapter);
                        int selectedCityIndex =SharedData.getInstance().getCityIndexInList(cities,cityId);
                        spCity.setSelection(selectedCityIndex);
                    }
                }, 100);
            }
        }, 100);


    }


    private void initView() {

        countries = SharedData.getInstance().getCountriesList();
        customCountryAdapter = new CustomCountrySpinnerAdapter(getContext(), R.layout.custom_spinner_item,countries);
        spCountry.setAdapter(customCountryAdapter);

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                states = SharedData.getInstance().
                        getStatesListAgainstCountryByCountryId(Integer.toString(((Country)adapterView.getSelectedItem()).countryId));
                stateAdapter = new CustomStateSpinnerAdapter(getContext(), R.layout.custom_spinner_item, states);
                spState.setAdapter(stateAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                cities = SharedData.getInstance().
                        getCitiesListByStateId(((State)adapterView.getItemAtPosition(i)).getStateId());
                cityAdapter = new CustomCitySpinnerAdapter(getContext(), R.layout.custom_spinner_item, cities);
                spCity.setAdapter(cityAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void setData() {

        if (trainingProviderDetail == null) {
            return;
        }

        etTrainingType.setText(trainingProviderDetail.getType());
        etTitle.setText(trainingProviderDetail.getTitle());

        etInstitute.setText(trainingProviderDetail.getInstitute());
        etDepartment.setText(trainingProviderDetail.getDepartment());
        etSupervisor.setText(trainingProviderDetail.getSupervisor());

        tvStartDate.setText(trainingProviderDetail.getStartDateFormatted());
        tvEndDate.setText(trainingProviderDetail.getEndDateFormatted());

        etPhone.setText(trainingProviderDetail.getPhone());
        etAddress.setText(trainingProviderDetail.getAddress());

        setSpinnerData(trainingProviderDetail.getCountryId(),trainingProviderDetail.getStateId(),trainingProviderDetail.getCityId());

        etNotes.setText(trainingProviderDetail.getNotes());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditTrainingProviderDetail();
                }
                break;

            case R.id.tv_start_date:
                openDatePicker(tvStartDate.getText().toString(), "startDate");
                break;

            case R.id.tv_end_date:
                openDatePicker(tvEndDate.getText().toString(), "endDate");
                break;

        }
    }


    private void openDatePicker(String dateStr, String tag) {

        Date date;
        if (dateStr.isEmpty()) {
            date = new Date();
        }else {
            date = GenericUtils.unFormatDate(dateStr);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setVersion(DatePickerDialog.Version.VERSION_2);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.light_blue));

        datePickerDialog.show(((Activity)context).getFragmentManager(), tag);

    }


    //Date picker callback
    @Override
    public void onDateSet(DatePickerDialog view, int year, int month, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        if(view.getTag().equalsIgnoreCase("startDate")){
            tvStartDate.setText(GenericUtils.formatDate(calendar.getTime()));
        }else{
            tvEndDate.setText(GenericUtils.formatDate(calendar.getTime()));
        }

    }


    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etTrainingType.getText().toString().isEmpty() || etTitle.getText().toString().isEmpty() ||
                tvStartDate.getText().toString().isEmpty() || tvEndDate.getText().toString().isEmpty() ||
                etInstitute.getText().toString().isEmpty() || etDepartment.getText().toString().isEmpty() ||
                etSupervisor.getText().toString().isEmpty() || etPhone.getText().toString().isEmpty() ||
                etAddress.getText().toString().isEmpty() ||
                etNotes.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustTrainingProviderDetails(RetrofitJSONResponse response, TrainingProviderDetail updatedTrainingProviderDetail) {

        if (trainingProviderDetail == null) {//Add

            JSONObject trainingProviderDetailJson = response.optJSONObject("DoctorTrainingProvider");
            if (trainingProviderDetailJson != null) {
                TrainingProviderDetail newTrainingProviderDetail = new TrainingProviderDetail(trainingProviderDetailJson);
                DoctorProfile profile = SharedData.getInstance().getSelectedDoctorProfile();
                profile.getTrainingProviderDetails().add(newTrainingProviderDetail);
            }

        }else  {

            trainingProviderDetail.setType(updatedTrainingProviderDetail.getType());
            trainingProviderDetail.setTitle(updatedTrainingProviderDetail.getTitle());

            trainingProviderDetail.setStartDate(updatedTrainingProviderDetail.getStartDate());
            trainingProviderDetail.setStartDateFormatted(updatedTrainingProviderDetail.getStartDateFormatted());

            trainingProviderDetail.setEndDate(updatedTrainingProviderDetail.getEndDate());
            trainingProviderDetail.setEndDateFormatted(updatedTrainingProviderDetail.getEndDateFormatted());

            trainingProviderDetail.setInstitute(updatedTrainingProviderDetail.getInstitute());
            trainingProviderDetail.setDepartment(updatedTrainingProviderDetail.getDepartment());
            trainingProviderDetail.setSupervisor(updatedTrainingProviderDetail.getSupervisor());

            trainingProviderDetail.setPhone(updatedTrainingProviderDetail.getPhone());
            trainingProviderDetail.setAddress(updatedTrainingProviderDetail.getAddress());

            trainingProviderDetail.setCityId(updatedTrainingProviderDetail.getCityId());
            trainingProviderDetail.setCityName(updatedTrainingProviderDetail.getCityName());
            trainingProviderDetail.setStateId(updatedTrainingProviderDetail.getStateId());
            trainingProviderDetail.setStateName(updatedTrainingProviderDetail.getStateName());
            trainingProviderDetail.setCountryId(updatedTrainingProviderDetail.getCountryId());
            trainingProviderDetail.setCountryName(updatedTrainingProviderDetail.getCountryName());

            trainingProviderDetail.setNotes(updatedTrainingProviderDetail.getNotes());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }


    //Webservices
    private void addEditTrainingProviderDetail() {

        AlertUtils.showProgress(getActivity());

        final TrainingProviderDetail updatedTrainingProviderDetail = new TrainingProviderDetail();
        if (trainingProviderDetail != null) {
            updatedTrainingProviderDetail.setId(trainingProviderDetail.getId());
        }

        updatedTrainingProviderDetail.setType(etTrainingType.getText().toString());
        updatedTrainingProviderDetail.setTitle(etTitle.getText().toString());

        updatedTrainingProviderDetail.setStartDateFormatted(tvStartDate.getText().toString());
        Date date = GenericUtils.unFormatDate(tvStartDate.getText().toString());
        updatedTrainingProviderDetail.setStartDate(GenericUtils.getTimeDateString(date));

        updatedTrainingProviderDetail.setEndDateFormatted(tvEndDate.getText().toString());
        date = GenericUtils.unFormatDate(tvEndDate.getText().toString());
        updatedTrainingProviderDetail.setEndDate(GenericUtils.getTimeDateString(date));

        updatedTrainingProviderDetail.setInstitute(etInstitute.getText().toString());
        updatedTrainingProviderDetail.setDepartment(etDepartment.getText().toString());
        updatedTrainingProviderDetail.setSupervisor(etSupervisor.getText().toString());

        updatedTrainingProviderDetail.setPhone(etPhone.getText().toString());
        updatedTrainingProviderDetail.setAddress(etAddress.getText().toString());

        if(spState.getSelectedItem()!=null) {
            updatedTrainingProviderDetail.setStateId(((State) spState.getSelectedItem()).getStateId());
            updatedTrainingProviderDetail.setStateName(((State) spState.getSelectedItem()).getStateName());
        }
        else{
            updatedTrainingProviderDetail.setStateId("0");
            updatedTrainingProviderDetail.setStateName("");
        }
        if(spCountry.getSelectedItem()!=null) {
            updatedTrainingProviderDetail.setCountryId(Integer.toString(((Country) spCountry.getSelectedItem()).countryId));
            updatedTrainingProviderDetail.setCountryName(((Country) spCountry.getSelectedItem()).getCountryName());
        }else{
            updatedTrainingProviderDetail.setCountryId("0");
            updatedTrainingProviderDetail.setCountryName("");
        }
        if(spCity.getSelectedItem()!=null) {
            updatedTrainingProviderDetail.setCityId(((City) spCity.getSelectedItem()).getCityId());
            updatedTrainingProviderDetail.setCityName(((City) spCity.getSelectedItem()).getCityName());
        }else{
            updatedTrainingProviderDetail.setCityId("0");
            updatedTrainingProviderDetail.setCityName("");
        }

        updatedTrainingProviderDetail.setNotes(etNotes.getText().toString());

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditTrainingProviderDetail(userId, updatedTrainingProviderDetail, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                adjustTrainingProviderDetails(response, updatedTrainingProviderDetail);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Training provider details saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
