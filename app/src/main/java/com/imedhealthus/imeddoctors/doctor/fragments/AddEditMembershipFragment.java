package com.imedhealthus.imeddoctors.doctor.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.CustomCitySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomCountrySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomStateSpinnerAdapter;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.City;
import com.imedhealthus.imeddoctors.common.models.Country;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.State;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.imedhealthus.imeddoctors.doctor.models.Membership;
import com.imedhealthus.imeddoctors.doctor.models.WorkExperience;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddEditMembershipFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.et_designation)
    EditText etDesignation;

    @BindView(R.id.tv_start_date)
    TextView tvStartDate;
    @BindView(R.id.tv_end_date)
    TextView tvEndDate;
    @BindView(R.id.cb_is_current_member)
    CheckBox cbIsCurrentlyMember;

    @BindView(R.id.sp_city)
    Spinner spCity;
    @BindView(R.id.sp_state)
    Spinner spState;
    @BindView(R.id.sp_country)
    Spinner spCountry;

    private WorkExperience workExperience;
    private List<Country> countries;
    private CustomCountrySpinnerAdapter customCountryAdapter;
    private CustomStateSpinnerAdapter stateAdapter;
    private List<State> states;
    private List<City> cities;
    private CustomCitySpinnerAdapter cityAdapter;
    private Membership membership;

    public AddEditMembershipFragment() {
        membership = SharedData.getInstance().getSelectedMembership();
    }


    @Override
    public String getName() {
        return "AddEditMembershipFragment";
    }

    @Override
    public String getTitle() {
        if (membership == null) {
            return "Add Membership";
        }else {
            return "Edit Membership";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_membership, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        setData();

    }

    private void setData() {

        if (membership == null) {
            return;
        }

        etDesignation.setText(membership.getDesignation());

        tvStartDate.setText(membership.getStartDateFormatted());
        tvEndDate.setText(membership.getEndDateFormatted());
        cbIsCurrentlyMember.setChecked(membership.isCurrentlyMember());

         setSpinnerData(membership.getCountryId(),membership.getStateId(),membership.getCityId());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditMembership();
                }
                break;

            case R.id.tv_start_date:
                openDatePicker(tvStartDate.getText().toString(), "startDate");
                break;

            case R.id.tv_end_date:
                openDatePicker(tvEndDate.getText().toString(), "endDate");
                break;

        }
    }


    private void openDatePicker(String dateStr, String tag) {

        Date date;
        if (dateStr.isEmpty()) {
            date = new Date();
        }else {
            date = GenericUtils.unFormatDate(dateStr);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setVersion(DatePickerDialog.Version.VERSION_2);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.light_blue));

        datePickerDialog.show(((Activity)context).getFragmentManager(), tag);

    }


    //Date picker callback
    @Override
    public void onDateSet(DatePickerDialog view, int year, int month, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        if(view.getTag().equalsIgnoreCase("startDate")){
            tvStartDate.setText(GenericUtils.formatDate(calendar.getTime()));
        }else{
            tvEndDate.setText(GenericUtils.formatDate(calendar.getTime()));
        }

    }


    //Other
    private boolean validateFields() {

        String errorMsg = null;

        if (etDesignation.getText().toString().isEmpty() ||
                tvStartDate.getText().toString().isEmpty() || tvEndDate.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void adjustMemberships(RetrofitJSONResponse response, Membership updatedMembership) {

        if (membership == null) {//Add

            JSONObject membershipJson = response.optJSONObject("DoctorMembership");
            if (membershipJson != null) {
                Membership newMembership = new Membership(membershipJson);
                DoctorProfile profile = SharedData.getInstance().getSelectedDoctorProfile();
                profile.getMemberships().add(newMembership);
            }

        }else  {

            membership.setDesignation(updatedMembership.getDesignation());

            membership.setStartDate(updatedMembership.getStartDate());
            membership.setStartDateFormatted(updatedMembership.getStartDateFormatted());
            membership.setEndDate(updatedMembership.getEndDate());
            membership.setEndDateFormatted(updatedMembership.getEndDateFormatted());
            membership.setCurrentlyMember(updatedMembership.isCurrentlyMember());

            membership.setCityId(updatedMembership.getCityId());
            membership.setCityName(updatedMembership.getCityName());
            membership.setStateId(updatedMembership.getStateId());
            membership.setStateName(updatedMembership.getStateName());
            membership.setCountryId(updatedMembership.getCountryId());
            membership.setCountryName(updatedMembership.getCountryName());

        }

        SharedData.getInstance().setRefreshRequired(true);

    }

    public void setSpinnerData(final String countryId,final String stateId,final String cityId){

        countries = SharedData.getInstance().getCountriesList();
        customCountryAdapter = new CustomCountrySpinnerAdapter(getContext(), R.layout.custom_spinner_item,countries);
        spCountry.setAdapter(customCountryAdapter);
        int selectedCountryIndex = SharedData.getInstance().getCountryIndexInList(countries,countryId);
        spCountry.setSelection(selectedCountryIndex);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                states = SharedData.getInstance().
                        getStatesListAgainstCountryByCountryId(countryId);
                stateAdapter = new CustomStateSpinnerAdapter(getContext(), R.layout.custom_spinner_item, states);
                spState.setAdapter(stateAdapter);
                int selectedStateIndex =SharedData.getInstance().getStateIndexInList(states,stateId);
                spState.setSelection(selectedStateIndex);


                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cities = SharedData.getInstance().
                                getCitiesListByStateId(stateId);
                        cityAdapter = new CustomCitySpinnerAdapter(getContext(), R.layout.custom_spinner_item, cities);
                        spCity.setAdapter(cityAdapter);
                        int selectedCityIndex =SharedData.getInstance().getCityIndexInList(cities,cityId);
                        spCity.setSelection(selectedCityIndex);
                    }
                }, 100);
            }
        }, 100);


    }


    private void initView() {

        countries = SharedData.getInstance().getCountriesList();
        customCountryAdapter = new CustomCountrySpinnerAdapter(getContext(), R.layout.custom_spinner_item,countries);
        spCountry.setAdapter(customCountryAdapter);

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                states = SharedData.getInstance().
                        getStatesListAgainstCountryByCountryId(Integer.toString(((Country)adapterView.getSelectedItem()).countryId));
                stateAdapter = new CustomStateSpinnerAdapter(getContext(), R.layout.custom_spinner_item, states);
                spState.setAdapter(stateAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(adapterView.getSelectedItem()==null)
                    return;

                cities = SharedData.getInstance().
                        getCitiesListByStateId(((State)adapterView.getItemAtPosition(i)).getStateId());
                cityAdapter = new CustomCitySpinnerAdapter(getContext(), R.layout.custom_spinner_item, cities);
                spCity.setAdapter(cityAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }
    //Webservices
    private void addEditMembership() {

        AlertUtils.showProgress(getActivity());

        final Membership updatedMembership = new Membership();
        if (membership != null) {
            updatedMembership.setId(membership.getId());
        }

        updatedMembership.setDesignation(etDesignation.getText().toString());

        updatedMembership.setStartDateFormatted(tvStartDate.getText().toString());
        Date date = GenericUtils.unFormatDate(tvStartDate.getText().toString());
        updatedMembership.setStartDate(GenericUtils.getTimeDateString(date));

        updatedMembership.setEndDateFormatted(tvEndDate.getText().toString());
        date = GenericUtils.unFormatDate(tvEndDate.getText().toString());
        updatedMembership.setEndDate(GenericUtils.getTimeDateString(date));

        updatedMembership.setCurrentlyMember(cbIsCurrentlyMember.isChecked());

        if(spState.getSelectedItem() == null || ((State) spState.getSelectedItem()).getStateId().equals("0")) {
            updatedMembership.setStateId("0");
            updatedMembership.setStateName("");
        }else {
            updatedMembership.setStateId(((State) spState.getSelectedItem()).getStateId());
            updatedMembership.setStateName(((State) spState.getSelectedItem()).getStateName());
        }

        if(spCountry.getSelectedItem() == null || Integer.toString(((Country) spCountry.getSelectedItem()).countryId).equals("0")) {
            updatedMembership.setCountryId("0");
            updatedMembership.setCountryName("");
        }else {
            updatedMembership.setCountryId(Integer.toString(((Country) spCountry.getSelectedItem()).countryId));
            updatedMembership.setCountryName(((Country) spCountry.getSelectedItem()).getCountryName());
        }

        if(spCity.getSelectedItem() == null || ((City) spCity.getSelectedItem()).getCityId().equals("0")) {
            updatedMembership.setCityId("0");
            updatedMembership.setCityName("");
        }else {
            updatedMembership.setCityId(((City) spCity.getSelectedItem()).getCityId());
            updatedMembership.setCityName(((City) spCity.getSelectedItem()).getCityName());
        }

        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.addEditMembership(userId, updatedMembership, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(getContext(),Constants.ErrorAlertTitle,response.message());
                    return;
                }

                adjustMemberships(response, updatedMembership);
                AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, "Membership details saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
