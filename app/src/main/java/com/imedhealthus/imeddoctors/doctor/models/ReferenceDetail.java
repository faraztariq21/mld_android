package com.imedhealthus.imeddoctors.doctor.models;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class ReferenceDetail extends OverviewItem {

    private String id, name, relation;
    private String address, email, phone, fax;
    private int years;

    public ReferenceDetail() {
    }

    public ReferenceDetail(JSONObject educationJson) {


        id = educationJson.optString("Id", "");
        name = educationJson.optString("Name", "").replace("null","");
        relation = educationJson.optString("RelationshipId", "").replace("null","");
        years = educationJson.optInt("YearAssociated", 0);

        email = educationJson.optString("Email", "").replace("null","");
        phone = educationJson.optString("Phone", "").replace("null","");
        fax = educationJson.optString("Fax", "").replace("null","");
        address = educationJson.optString("Address", "").replace("null","");

    }

    public static List<ReferenceDetail> parseReferenceDetails(JSONArray referenceDetailsJson){

        ArrayList<ReferenceDetail> referenceDetails = new ArrayList<>();

        for (int i = 0; i < referenceDetailsJson.length(); i++){

            JSONObject referenceDetailJson = referenceDetailsJson.optJSONObject(i);
            if (referenceDetailJson != null) {
                ReferenceDetail referenceDetail = new ReferenceDetail(referenceDetailJson);
                referenceDetails.add(referenceDetail);
            }

        }

        return referenceDetails;

    }


    @Override
    public String getDisplayTitle() {

        String displayTitle =  name;
        if (!TextUtils.isEmpty(relation)) {
            displayTitle += " (" + relation + ")";
        }
        return displayTitle;

    }

    @Override
    public String getDisplayDetail() {

        String displayDetail = email;

        if (!TextUtils.isEmpty(phone)) {
            displayDetail += " (" + phone + ")";
        }

        return displayDetail;

    }

    @Override
    public String getDisplayDuration() {

        String displayDuration = years + " Years";
        return displayDuration;

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public int getYears() {
        return years;
    }

    public void setYears(int years) {
        this.years = years;
    }
}
