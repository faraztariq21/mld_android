package com.imedhealthus.imeddoctors.doctor.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.doctor.listeners.OnServiceItemClickListener;
import com.imedhealthus.imeddoctors.doctor.models.Service;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/6/18.
 */

public class ServiceItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    RelativeLayout contItem;

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_price)
    TextView tvPrice;
    @BindView(R.id.tv_details)
    TextView tvDetails;

    @BindView(R.id.iv_edit)
    ImageView ivEdit;
    @BindView(R.id.iv_delete)
    ImageView ivDelete;

    private int idx;
    private WeakReference<OnServiceItemClickListener> onServiceItemClickListener;

    public ServiceItemViewHolder(View view, boolean isEditAllowed, OnServiceItemClickListener onServiceItemClickListener) {

        super(view);
        ButterKnife.bind(this, view);

        if (isEditAllowed) {
            ivEdit.setVisibility(View.VISIBLE);
            ivDelete.setVisibility(View.VISIBLE);
        }else {
            ivEdit.setVisibility(View.GONE);
            ivDelete.setVisibility(View.GONE);
        }

        contItem.setOnClickListener(this);
        ivEdit.setOnClickListener(this);
        ivDelete.setOnClickListener(this);
        this.onServiceItemClickListener = new WeakReference<>(onServiceItemClickListener);

    }

    public void setData(Service service, int idx) {

        tvTitle.setText(service.getTitle());
        tvPrice.setText(String.format("$%.2f", service.getPrice()));
        tvDetails.setText(service.getDetails());

        this.idx = idx;

    }

    @Override
    public void onClick(View view) {

        if (onServiceItemClickListener == null || onServiceItemClickListener.get() == null) {
            return;
        }

        switch (view.getId()){
            case R.id.cont_item:
                onServiceItemClickListener.get().onServiceDetailClick(idx);
                break;

            case R.id.iv_edit:
                onServiceItemClickListener.get().onServiceEditClick(idx);
                break;

            case R.id.iv_delete:
                onServiceItemClickListener.get().onServiceDeleteClick(idx);
                break;
        }

    }
}

