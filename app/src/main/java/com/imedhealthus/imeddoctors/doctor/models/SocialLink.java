package com.imedhealthus.imeddoctors.doctor.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class SocialLink extends OverviewItem {

    private String id, type, url;

    public SocialLink() {
    }

    public SocialLink(JSONObject educationJson) {

        id = educationJson.optString("Id", "");
        type = educationJson.optString("LinkName", "");
        url = educationJson.optString("URL", "");

    }

    public static List<SocialLink> parseSocialLinks(JSONArray socialLinksJson) {

        ArrayList<SocialLink> socialLinks = new ArrayList<>();

        for (int i = 0; i < socialLinksJson.length(); i++) {

            JSONObject socialLinkJson = socialLinksJson.optJSONObject(i);
            if (socialLinkJson != null) {
                SocialLink socialLink = new SocialLink(socialLinkJson);
                socialLinks.add(socialLink);
            }

        }

        return socialLinks;

    }


    @Override
    public String getDisplayTitle() {

        String displayTitle =  type;
        return displayTitle;

    }

    @Override
    public String getDisplayDetail() {

        String displayDetail = url;
        return displayDetail;

    }

    @Override
    public String getDisplayDuration() {

        return "";

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
