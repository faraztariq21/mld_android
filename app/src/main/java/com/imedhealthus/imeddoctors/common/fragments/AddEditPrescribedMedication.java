package com.imedhealthus.imeddoctors.common.fragments;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.CustomAutoCompleteAdapter;
import com.imedhealthus.imeddoctors.common.models.GeneralSuggestionItem;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.SuggestionListItem;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.FileUtils;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.patient.models.PrescribedMedication;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 1/30/2018.
 */

public class AddEditPrescribedMedication extends BaseFragment implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.et_name)
    AutoCompleteTextView etName;
    @BindView(R.id.et_frequency)
    EditText etFrequency;

    @BindView(R.id.et_dose)
    EditText etDose;
    @BindView(R.id.et_unit)
    EditText etUnit;

    @BindView(R.id.tv_start_date)
    TextView tvStartDate;
    @BindView(R.id.tv_end_date)
    TextView tvEndDate;

    @BindView(R.id.et_reason)
    AutoCompleteTextView etReason;


    private PrescribedMedication medication;
    private Date startDate, endDate;
    private FragmentRichEditor fragmentRichEditor;

    public AddEditPrescribedMedication() {
        medication = SharedData.getInstance().getSelectedPrescribedMedication();
    }

    private String[] optionsEnglish = {"Once", "Daily", "Two times a day", "Three times a day", "Four times a day"
            , "Five times a day", "Six times a day", "Before Bed", "Before Breakfast", "Before Lunch"
            , "Before Dinner", "Before meals", "Before meals and bed time", "With meals", "After Breakfast"
            , "After Lunch", "After Dinner", "After meals", "With Breakfast", "With Lunch", "With Dinner"
            , "Every 1 hour", "Every 2 hours", "Every 3 hours", "Every 4 hours", "Every 5 hours"
            , "Every 6 hours", "Every 7 hours", "Every 8 hours", "Every 12 hours", "Every 16 hours"
            , "Every 18 hours", "Every 20 hours", "Every 24 hours", "Every 36 hours", "Every 48 hours"
            , "Every 72 hours", "Every 96 hours", "Every other day", "Every 2 days", "Every 3 days"
            , "Every 4 days", "Every week", "Every 7 days", "Every 10 days", "Every 15 days", "Once a month"};


    @Override
    public String getName() {
        return "AddEditPrescribedMedication";
    }

    @Override
    public String getTitle() {
        if (medication == null) {
            return "Add Medication";
        } else {
            return "Edit Medication";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    FragmentSuggestionDialog.FragmentSuggestionDialogListener doseFrequencyListener = new FragmentSuggestionDialog.FragmentSuggestionDialogListener() {
        @Override
        public void onSuggestionSelected(int index, SuggestionListItem suggestionListItem) {
            etFrequency.setText(suggestionListItem.getSuggestionText());
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_prescribed_medication, container, false);
        ButterKnife.bind(this, view);
        //GenericUtils.setEditTextScrollable(etNotes, getActivity());
        setUpDoseFrequencyDialog();
        fragmentRichEditor = FragmentRichEditor.newInstance(null);
        getChildFragmentManager().beginTransaction().replace(R.id.ll_rich_editor_container, fragmentRichEditor).commit();

        return view;

    }

    private void setUpDoseFrequencyDialog() {
        final ArrayList<GeneralSuggestionItem> generalSuggestionItems = new ArrayList<>();
        for (String option : optionsEnglish) {
            GeneralSuggestionItem generalSuggestionItem = new GeneralSuggestionItem(option);
            generalSuggestionItems.add(generalSuggestionItem);
        }
        etFrequency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentSuggestionDialog.newInstance(getResources().getString(R.string.select_dose_frequency), generalSuggestionItems, true, doseFrequencyListener).show(getChildFragmentManager());
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);


        ArrayList<String> medicines = FileUtils.parseFile(context, "medicines.txt", "\n");
        CustomAutoCompleteAdapter medicineAdapter = new CustomAutoCompleteAdapter(context, R.layout.simple_dropdown_item_1line, etName, medicines);
        etName.setAdapter(medicineAdapter);
        etName.setThreshold(1);
        etName.addTextChangedListener(medicineAdapter);

        ArrayList<String> reasons = FileUtils.parseFile(getActivity(), "updateddiseases.txt", "\n");
        CustomAutoCompleteAdapter reasonsAdapter = new CustomAutoCompleteAdapter(getActivity(), R.layout.simple_dropdown_item_1line, etReason, reasons);
        etReason.setAdapter(reasonsAdapter);
        etReason.setThreshold(1);
        etReason.addTextChangedListener(reasonsAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    private void setData() {

        if (medication == null) {
            return;
        }

        etName.setText(medication.getName());
        etFrequency.setText(medication.getFrequency());

        etDose.setText(medication.getDose());
        etUnit.setText(medication.getUnit());

        etReason.setText(medication.getReason());
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            fragmentRichEditor.setEditorText(medication.getNotes());
//        }
//        else{
//            fragmentRichEditor.setEditorText(Html.fromHtml(Html.fromHtml(medication.getNotes()).toString()).toString());
//        }

        tvStartDate.setText(medication.getStartingDateFormatted());
        tvEndDate.setText(medication.getEndingDateFormatted());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (validateFields()) {
                    addEditMedication();
                }
                break;

            case R.id.tv_start_date:
                openDatePicker(tvStartDate.getText().toString(), "startDate");
                break;

            case R.id.tv_end_date:
                openDatePicker(tvEndDate.getText().toString(), "endDate");
                break;
        }
    }

    private boolean validateFields() {

        String errorMsg = null;
        if (etName.getText().toString().isEmpty() || etFrequency.getText().toString().isEmpty() ||
                etDose.getText().toString().isEmpty() || etUnit.getText().toString().isEmpty() /*||
                etNotes.getText().toString().isEmpty()*/) {
            errorMsg = "Kindly fill all fields to continue";
        } /*else if (tvStartDate.getText().toString().isEmpty() || tvEndDate.getText().toString().isEmpty()) {
            errorMsg = "Kindly select dates";
        }*/

        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void openDatePicker(String dateStr, String tag) {

        Date date;
        if (dateStr.isEmpty()) {
            date = new Date();
        } else {
            date = GenericUtils.unFormatDate(dateStr);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setVersion(DatePickerDialog.Version.VERSION_2);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.light_blue));

        datePickerDialog.show(((Activity) context).getFragmentManager(), tag);

    }


    //Date picker callback
    @Override
    public void onDateSet(DatePickerDialog view, int year, int month, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        if (view.getTag().equalsIgnoreCase("startDate")) {
            startDate = new Date(year, month, dayOfMonth);
            tvStartDate.setText(GenericUtils.formatDate(calendar.getTime()));
            validateStartAndEndDate(startDate, endDate);
        } else {
            endDate = new Date(year, month, dayOfMonth);
            tvEndDate.setText(GenericUtils.formatDate(calendar.getTime()));
            validateStartAndEndDate(startDate, endDate);
        }

    }

    private void validateStartAndEndDate(Date startDate, Date endDate) {

        if (GenericUtils.validateStartAndEndDateOrder(startDate, endDate) == false) {
            tvEndDate.setText("");
            tvEndDate.setHint("End Date");
            tvStartDate.setText("");
            tvStartDate.setHint("Start Date");
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, getResources().getString(R.string.start_end_date_error));

            this.startDate = null;
            this.endDate = null;
        }

    }


    //Other
    private void addEditMedication() {

        AlertUtils.showProgress(getActivity());

        if (medication == null) {
            medication = new PrescribedMedication();
        }

        medication.setName(etName.getText().toString());
        medication.setFrequency(etFrequency.getText().toString());
        medication.setReason(etReason.getText().toString());

        medication.setDose(etDose.getText().toString());
        medication.setUnit(etUnit.getText().toString());

        medication.setStartingDateFormatted(tvStartDate.getText().toString());
        medication.setEndingDateFormatted(tvEndDate.getText().toString());

        Date date = GenericUtils.unFormatDate(tvStartDate.getText().toString());
        medication.setStartingDate(GenericUtils.getTimeDateString(date));

        date = GenericUtils.unFormatDate(tvEndDate.getText().toString());
        medication.setEndingDate(GenericUtils.getTimeDateString(date));

        medication.setNotes(fragmentRichEditor.getEditorText());

        SharedData.getInstance().setSelectedPrescribedMedication(medication);

        ((Activity) context).onBackPressed();

    }

}
