package com.imedhealthus.imeddoctors.common.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.PaymentMethod;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 1/12/2018.
 */

public class AddEditPaymentMethodFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener, AdapterView.OnItemSelectedListener {

    @BindView(R.id.sp_payment_method)
    Spinner spPaymentMethod;

    @BindView(R.id.cont_card)
    ViewGroup contCard;
    @BindView(R.id.cont_other)
    ViewGroup contOther;

    @BindView(R.id.et_card_number)
    EditText etCardNumber;
    @BindView(R.id.tv_card_expiry)
    TextView tvCardExpiry;
    @BindView(R.id.et_card_cvc)
    EditText etCVC;

    @BindView(R.id.et_details)
    EditText etOtherDetails;

    private PaymentMethod paymentMethod;
    private Date expiryDate;
    private SimpleDateFormat dateFormat;

    public AddEditPaymentMethodFragment() {
        paymentMethod = SharedData.getInstance().getSelectedPaymentMethod();
    }

    @Override
    public String getName() {
        return "AddEditPaymentMethodFragment";
    }

    @Override
    public String getTitle() {
        if (paymentMethod == null) {
            return "Add Payment Method";
        }else {
            return "Edit Payment Method";
        }
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_payment_method, container, false);
        ButterKnife.bind(this,view);

        initHelper();
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
    }

    private void initHelper() {

        expiryDate = new Date();
        dateFormat = new SimpleDateFormat("MM/yyyy");

        spPaymentMethod.setOnItemSelectedListener(this);

    }

    private void setData() {

        if (paymentMethod == null) {
            return;
        }

        String []paymentMethods = context.getResources().getStringArray(R.array.payment_methods);
        int idx = 0;
        for (; idx < paymentMethods.length; idx++) {
            if (paymentMethods[idx].equalsIgnoreCase(paymentMethod.getTitle())) {
                break;
            }
        }

        if (idx == 0) {//Card

            try {
                expiryDate = dateFormat.parse(paymentMethod.getCardExpiryMonth() + "/" + paymentMethod.getCardExpiryYear());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            etCardNumber.setText(paymentMethod.getCardNumber());
            tvCardExpiry.setText(dateFormat.format(expiryDate));
            etCVC.setText(paymentMethod.getCardCVC());

            spPaymentMethod.setSelection(0);
            contCard.setVisibility(View.VISIBLE);
            contOther.setVisibility(View.GONE);

        }else {

            etOtherDetails.setText(paymentMethod.getDetails());

            spPaymentMethod.setSelection(1);
            contCard.setVisibility(View.GONE);
            contOther.setVisibility(View.VISIBLE);

        }

    }

    //Actions
    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.tv_card_expiry:
                openDatePicker();
                break;

            case R.id.btn_save:
                if (validateFields()) {
                    if(paymentMethod!=null)
                        updateCard();
                    else
                        addPaymentMethods();
                }
                break;
        }
    }

    private void openDatePicker() {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(expiryDate);

        DatePickerDialog dialog = new DatePickerDialog(context, this,  calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMinDate(new Date().getTime());
        dialog.show();

    }

    private boolean validateFields() {

        String errorMsg = null;

        if (spPaymentMethod.getSelectedItemPosition() == 0) {//Card

            if (etCardNumber.getText().toString().isEmpty() || etCVC.getText().toString().isEmpty()) {
                errorMsg = "Kindly fill all fields to continue";
            }else if (tvCardExpiry.getText().toString().isEmpty()) {
                errorMsg = "Kindly select an expiry date";
            }else if(etCardNumber.getText().toString().length() != 16) {
                errorMsg = "Enter a valid card number ";
            }

        }else {

            if (etOtherDetails.getText().toString().isEmpty()) {
                errorMsg = "Kindly enter some details";
            }

        }

        if (errorMsg != null) {
            AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }
        return true;

    }


    //Date picker
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        expiryDate = calendar.getTime();

        SimpleDateFormat format = new SimpleDateFormat("MM/yyyy");
        tvCardExpiry.setText(format.format(calendar.getTime()));

    }


    //Other
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

        if (position == 0) {
            contCard.setVisibility(View.VISIBLE);
            contOther.setVisibility(View.GONE);
        }else {
            contCard.setVisibility(View.GONE);
            contOther.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void addPaymentMethods(){
        AlertUtils.showProgress(getActivity());
        String cardNumber,cvc,cardExpiringMonth,cardExpiringYear,otherDetails,userId,title;

            cardNumber = etCardNumber.getText().toString();
            title = spPaymentMethod.getSelectedItem().toString();
            userId = CacheManager.getInstance().getCurrentUser().getLoginId();
            cvc = etCVC.getText().toString();
            cardExpiringMonth = tvCardExpiry.getText().toString().split("/")[0];
            cardExpiringYear = tvCardExpiry.getText().toString().split("/")[1];
            otherDetails = etOtherDetails.getText().toString();

        WebServicesHandler.instance.addPaymentMethod(userId, title, otherDetails, cardNumber, cardExpiringMonth, cardExpiringYear, cvc, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(),"Status",response.message());

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(),"Status", Constants.GenericErrorMsg);
            }
        });
    }


    //Web services
    private void updateCard() {


        AlertUtils.showProgress(getActivity());
        paymentMethod.setCardNumber( etCardNumber.getText().toString());
        paymentMethod.setTitle(  spPaymentMethod.getSelectedItem().toString());
        //userId = CacheManager.getInstance().getCurrentUser().getLoginId();
        paymentMethod.setCardCVC(  etCVC.getText().toString());
        if( !tvCardExpiry.getText().toString().isEmpty()){
            paymentMethod.setCardExpiryMonth(  tvCardExpiry.getText().toString().split("/")[0]);
            paymentMethod.setCardExpiryYear(  tvCardExpiry.getText().toString().split("/")[1]);
        }
        paymentMethod.setDetails(  etOtherDetails.getText().toString());

        WebServicesHandler.instance.updatePaymentMethod(paymentMethod.getMethodId(), paymentMethod.getTitle(), paymentMethod.getDetails(), paymentMethod.getCardNumber(),
                paymentMethod.getCardExpiryMonth(), paymentMethod.getCardExpiryYear(), paymentMethod.getCardCVC(), new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(),"Status",response.message());

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(),"Status", Constants.GenericErrorMsg);
            }
        });
    }

}