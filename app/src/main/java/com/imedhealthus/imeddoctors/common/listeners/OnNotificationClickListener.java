package com.imedhealthus.imeddoctors.common.listeners;

/**
 * Created by Malik Hamid on 1/16/2018.
 */

public interface OnNotificationClickListener {
    void onNotificationItemClick(int idx);
    void onNotificationItemlongClick(int idx);
}

