package com.imedhealthus.imeddoctors.common.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.InvoicesListAdapter;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.Invoice;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InvoicesFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.tv_date)
    TextView tvDate;

    @BindView(R.id.rv_invoices)
    RecyclerView rvInvoices;
    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;

    private List<Invoice> invoices;
    private InvoicesListAdapter adapterInvoices;
    private Date currentDate;
    private SimpleDateFormat dateFormat;

    @Override
    public String getName() {
        return "InvoicesFragment";
    }

    @Override
    public String getTitle() {
        return "Invoices";
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_invoices, container, false);
        ButterKnife.bind(this, view);

        initHelper();
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    private void initHelper() {

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvInvoices.setLayoutManager(mLayoutManager);

        adapterInvoices = new InvoicesListAdapter(tvNoRecords);
        rvInvoices.setAdapter(adapterInvoices);

        invoices = new ArrayList<>();
        dateFormat = new SimpleDateFormat("MMMM yyyy");

        currentDate = new Date();
        filterAppointments(currentDate);

    }


    private void setData(List<Invoice> invoices) {

        for (Invoice invoice: invoices) {
            invoice.setDate(new Date(invoice.getTimeStamp() * 1000));
        }
        this.invoices = invoices;
        filterAppointments(currentDate);

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cont_date:
                openDatePicker(currentDate);
                break;


        }
    }

    private void openDatePicker(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setVersion(DatePickerDialog.Version.VERSION_2);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.light_blue));

        datePickerDialog.show(((Activity)context).getFragmentManager(), "DatePickerDialog");

    }

    //Date picker callback
    @Override
    public void onDateSet(DatePickerDialog view, int year, int month, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        filterAppointments(calendar.getTime());

    }

    //Filtering
    private void filterAppointments(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int filterYear = calendar.get(Calendar.YEAR);
        int filterMonth = calendar.get(Calendar.MONTH);

        List<Invoice> filteredInvoices = new ArrayList<>();

        for (Invoice invoice : invoices) {
            if (invoice.isSameMonth(filterYear, filterMonth)) {
                filteredInvoices.add(invoice);
            }

        }

        adapterInvoices.setInvoices(filteredInvoices);
        tvDate.setText(dateFormat.format(date));

        currentDate = date;

    }


    //Webservices
    private void loadData() {
        AlertUtils.showProgress(getActivity());
        String userId = CacheManager.getInstance().getCurrentUser().getUserId();

        WebServicesHandler.instance.getInvoices(userId, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                AlertUtils.dismissProgress();
                if(response.status()){
                    List<Invoice> invoices = Invoice.getInvoices(response);
                    setData(invoices);
                }else{
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle,response.message());
                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });


    }

}
