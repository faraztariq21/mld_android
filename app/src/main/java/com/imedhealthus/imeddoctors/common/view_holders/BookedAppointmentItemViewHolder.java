package com.imedhealthus.imeddoctors.common.view_holders;

import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.listeners.OnBookedAppointmentItemClickListener;
import com.imedhealthus.imeddoctors.common.models.BookedAppointment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.common.utils.TinyDB;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 1/10/2018.
 */

public class BookedAppointmentItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    ViewGroup contItem;

    @BindView(R.id.iv_type)
    ImageView ivType;
    @BindView(R.id.iv_chat)
    ImageView ivChat;

    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_mr_number)
    TextView tvMrNumber;
    @BindView(R.id.tv_reason)
    TextView tvDescription;

    @BindView(R.id.cont_location)
    ViewGroup contLocation;
    @BindView(R.id.iv_pin)
    ImageView ivAddressPin;
    @BindView(R.id.tv_address)
    TextView tvAddress;

    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.cont_buttons)
    ViewGroup contButtons;
    
    @BindView(R.id.btn_call)
    Button btnCall;
    @BindView(R.id.btn_call_video)
    Button btnCallVideo;
    @BindView(R.id.btn_postpone)
    Button btnComplete;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.iv_drop_down)
    ImageView ivDropDownArrow;

    private int idx;
    private WeakReference<OnBookedAppointmentItemClickListener> onBookedAppointmentItemClickListener;

    public BookedAppointmentItemViewHolder(View view, OnBookedAppointmentItemClickListener onBookedAppointmentItemClickListener) {

        super(view);
        ButterKnife.bind(this, view);

        contItem.setOnClickListener(this);
        btnComplete.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnCall.setOnClickListener(this);
        ivChat.setOnClickListener(this);
        btnCallVideo.setOnClickListener(this);
        ivDropDownArrow.setOnClickListener(this);
        ivType.setOnClickListener(this);
        tvName.setOnClickListener(this);
        this.onBookedAppointmentItemClickListener = new WeakReference<>(onBookedAppointmentItemClickListener);

    }

    public void setData(BookedAppointment appointment, boolean isSelected, int idx) {

        if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Patient) {
            Glide.with(ApplicationManager.getContext()).load(appointment.getDoctorProfile()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivType);
        } else {
            Glide.with(ApplicationManager.getContext()).load(appointment.getPatientProfile()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivType);
        }
/*

        if (new Date().getTime() > appointment.getEndTimeStamp())
            btnComplete.setVisibility(View.VISIBLE);
*/
//        if(CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Patient) {
//            ivChat.setVisibility(View.GONE);
//            btnCall.setVisibility(View.GONE);
//            btnCallVideo.setVisibility(View.GONE);
//        }
//        //  if(appointment.getType() == Constants.AppointmentType.Tele&&appointment.isItTime())



        //btnCall.setVisibility(View.VISIBLE);

        tvType.setText(appointment.getType().toString(ApplicationManager.getContext()));
        tvDescription.setText(appointment.getReason());
        tvAddress.setText(appointment.getPhysicalLocation());
        tvTime.setText(appointment.getFormattedTime());

        if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Patient) {
            tvMrNumber.setVisibility(View.GONE);
            tvName.setText(appointment.getDoctorFullName());
            btnCancel.setVisibility(View.GONE);
            btnCall.setVisibility(View.GONE);
            btnCallVideo.setVisibility(View.GONE);
            ivChat.setVisibility(View.GONE);
        } else {
            tvMrNumber.setVisibility(View.VISIBLE);
            tvName.setText(appointment.getPatientFullName());
            tvMrNumber.setText(appointment.getMrNumber());
        }

        if(GenericUtils.getDateTimeDifferenceInMilliSecond(new TinyDB(ApplicationManager.getContext()).getString(SharedData.getInstance().getSelectedDoctorId() + "_" + SharedData.getInstance().getSelectedPatientId() + Constants.END_CALL_TIME))< Constants.TWO_HOURS_DIFFERENCE_IN_MILLISECOND)
            ivChat.setVisibility(View.VISIBLE);

        /*if (appointment.getType() == Constants.AppointmentType.OFFICE && isSelected) {
            contLocation.setVisibility(View.VISIBLE);
            btnComplete.setVisibility(View.VISIBLE);
        } else {
            contLocation.setVisibility(View.GONE);
            btnComplete.setVisibility(View.GONE);
        }
*/
        contButtons.setVisibility(View.VISIBLE);
      /*  if (isSelected) {
            ivChat.setRotation(180);
            contButtons.setVisibility(View.VISIBLE);
            if(appointment.getType() == Constants.AppointmentType.Tele){


                btnComplete.setVisibility(View.GONE);
            }
        }else {
            ivChat.setRotation(0);
            contButtons.setVisibility(View.GONE);
        }
*/
        this.idx = idx;

        if (appointment.getType() == Constants.AppointmentType.OFFICE && CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor) {
            btnCall.setVisibility(View.GONE);
            btnCallVideo.setVisibility(View.GONE);
        }

    }


    @Override
    public void onClick(View view) {

        if (onBookedAppointmentItemClickListener == null || onBookedAppointmentItemClickListener.get() == null) {
            return;
        }

        switch (view.getId()) {

            case R.id.iv_drop_down:
                //toggleDescriptionVisiblity();
                onBookedAppointmentItemClickListener.get().onBookedAppointmentItemDropDownClick(idx);

                break;
            case R.id.cont_item:
                onBookedAppointmentItemClickListener.get().onBookedAppointmentItemDetailClick(idx);
                break;

            case R.id.iv_chat:
                onBookedAppointmentItemClickListener.get().onBookedAppointmentItemChatClick(idx);
                break;

            case R.id.btn_postpone:
                onBookedAppointmentItemClickListener.get().onBookedAppointmentItemCompleteClick(idx);
                break;

            case R.id.btn_cancel:
                onBookedAppointmentItemClickListener.get().onBookedAppointmentItemCancelClick(idx);
                break;

            case R.id.btn_call:
                onBookedAppointmentItemClickListener.get().onBookedAppointmentItemCallClick(idx);
                break;
            case R.id.btn_call_video:
                onBookedAppointmentItemClickListener.get().onBookedAppointmentItemVideoCallClick(idx);
                break;

            case R.id.tv_name:
            case R.id.iv_type:
                if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Patient)
                    onBookedAppointmentItemClickListener.get().onBookedAppointmentItemDoctorProfileClick(idx);
                else
                    onBookedAppointmentItemClickListener.get().onBookedAppointmentItemPatientProfileClick(idx);

                break;


        }
    }

    private void toggleDescriptionVisiblity() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.beginDelayedTransition(contItem);
        }
        if (tvDescription.getVisibility() == View.VISIBLE) {
            tvDescription.setVisibility(View.GONE);
            contLocation.setVisibility(View.GONE);
            ivDropDownArrow.setImageDrawable(ContextCompat.getDrawable(ApplicationManager.getContext(), R.drawable.ic_expand_more_black_24dp));
        } else {
            tvDescription.setVisibility(View.VISIBLE);
            contLocation.setVisibility(View.VISIBLE);
            ivDropDownArrow.setImageDrawable(ContextCompat.getDrawable(ApplicationManager.getContext(), R.drawable.ic_expand_less_black_24dp));

        }
    }


}

