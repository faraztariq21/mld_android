package com.imedhealthus.imeddoctors.common.utils;

import android.content.Context;

import com.imedhealthus.imeddoctors.common.models.ChatMessage;
import com.imedhealthus.imeddoctors.common.models.ForumComment;

public class SharedPrefsHelper {

    public static void saveFileLocalAddress(Context context, ChatMessage chatMessage, String filePath) {

        new TinyDB(context).putString(chatMessage.getChatId() + " " + chatMessage.getLoginId() + " " + chatMessage.getReceiverLoginId() + " " + chatMessage.getFileType() + " " + chatMessage.getImageUrl(), filePath);


    }

    public static void saveForumCommentFileLocalAddress(Context context, ForumComment forumComment, String filePath) {

        new TinyDB(context).putString(forumComment.getCommentId() + " " + forumComment.getLoginId() + " " + forumComment.getComment() + " " + forumComment.getFileType() + " " + forumComment.getFileUrl(), filePath);


    }

    public static void saveFileLocalAddress(Context context, String chatMessage, String filePath) {

        new TinyDB(context).putString(chatMessage, filePath);

    }

    public static String getFileLocalAddress(Context context, ChatMessage chatMessage) {
        String filePath = new TinyDB(context).getString(chatMessage.getChatId() + " " + chatMessage.getLoginId() + " " + chatMessage.getReceiverLoginId() + " " + chatMessage.getFileType() + " " + chatMessage.getImageUrl());
        return filePath;
    }

    public static void saveForumCommentFileLocalAddress(Context context, String forumComment, String filePath) {

        new TinyDB(context).putString(forumComment, filePath);

    }

    public static String getForumCommentFileLocalAddress(Context context, ForumComment forumComment) {
        String filePath = new TinyDB(context).getString(forumComment.getCommentId() + " " + forumComment.getLoginId() + " " + forumComment.getComment() + " " + forumComment.getFileType() + " " + forumComment.getFileUrl());
        return filePath;
    }

    /**
     * Whenever a download is started, this method saves that download id , chatItem properties and the address of the file to be downloaded in sharedPrefs.
     * It is helpful to save chat item file local path when download is complete.
     *
     * @param refId       download reference Id
     * @param chatMessage chatItem against which download is started.
     * @param fileAddress address of the file to be downloaded
     */
    public static void saveChatFileWithDownloadId(Context context, long refId, ChatMessage chatMessage, String fileAddress) {
        new TinyDB(context).putString("downloadId " + refId, chatMessage.getChatId() + " " + chatMessage.getLoginId() + " " + chatMessage.getReceiverLoginId() + " " + chatMessage.getFileType() + " " + chatMessage.getImageUrl() + "|" + fileAddress);

    }

    public static String getChatFileFromDownloadId(Context context, long downloadId) {
        String chatFile = new TinyDB(context).getString("downloadId " + downloadId);
        return chatFile;
    }

    public static void saveForumCommentFileWithDownloadId(Context context, long refId, ForumComment forumComment, String fileAddress) {
        new TinyDB(context).putString("document downloadId " + refId, forumComment.getCommentId() + " " + forumComment.getLoginId() + " " + forumComment.getComment() + " " + forumComment.getFileType() + " " + forumComment.getFileUrl() + "|" + fileAddress);

    }

    public static String getForumCommentFileFromDownloadId(Context context, long downloadId) {
        String chatFile = new TinyDB(context).getString("document downloadId " + downloadId);
        return chatFile;
    }

    public static void putFileDownloadStatus(Context context, long refId, boolean downloadStatus) {
        new TinyDB(context).putBoolean("downloadStatus " + refId, downloadStatus);
    }

    public static boolean checkFileDownloadStatus(Context context, long refId) {
        return new TinyDB(context).getBoolean("downloadStatus " + refId);
    }


}
