package com.imedhealthus.imeddoctors.common.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.models.Invoice;
import com.imedhealthus.imeddoctors.common.view_holders.InvoiceItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Malik Hamid on 1/5/2018.
 */

public class InvoicesListAdapter extends RecyclerView.Adapter<InvoiceItemViewHolder> {


    private List<Invoice> invoices;
    private WeakReference<TextView> tvNoRecords;

    public InvoicesListAdapter(TextView tvNoRecords) {

        super();
        this.tvNoRecords = new WeakReference<>(tvNoRecords);

    }


    public void setInvoices(List<Invoice> invoices) {

        this.invoices = invoices;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }

    @Override
    public InvoiceItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_invoice, parent, false);

        return new InvoiceItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(InvoiceItemViewHolder viewHolder, int position) {
        viewHolder.setData(getItem(position));
    }

    @Override
    public int getItemCount() {
        return invoices == null ? 0 : invoices.size();
    }

    public Invoice getItem(int position) {
        if (position < 0 || position >= invoices.size()) {
            return null;
        }
        return invoices.get(position);
    }

}