package com.imedhealthus.imeddoctors.common.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.IncomingCallActivity;
import com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.Constants;

import java.util.Date;
import java.util.HashMap;


/**
 * Created by umair on 9/14/17.
 */

public class OnCallNotification extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {


        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent = new Intent(context, IncomingCallActivity.class);
        notificationIntent.putExtra(Constants.Keys.PushNotificationData, (HashMap<String,String>)intent.getExtras().get(Constants.Keys.PushNotificationData));

        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(notificationIntent);

        //OpenTokHandler.getInstance().initializeSession();
        getStaticData();


        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT |
                        PendingIntent.FLAG_ONE_SHOT);
        PendingIntent pendingIntentButton = PendingIntent.getActivity(context, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        Notification notification  = new Notification.Builder(context)
                .setContentTitle("My Live Doctors")
                .setContentText("Incoming Call")
                .setSmallIcon(R.drawable.ic_icon)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();

        notification.ledARGB = Color.BLUE;
        notification.ledOffMS = 1000;
        notification.ledOnMS = 1000;
        notification.vibrate = new long[] { 0, 200, 200, 500, 0};
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.sound =  Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.notification);

        notificationManager.notify(Constants.NOTIFICATIONSID.callNotificationId, notification);

        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = powerManager.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
        wakeLock.acquire();

    }

    public void getStaticData(){

        String userLoginId = "0";
        if (CacheManager.getInstance().isUserLoggedIn()) {
            userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        }

        WebServicesHandler.instance.getStaticData(userLoginId, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                if(response.status()){


                    String session = response.optString("SessionId");
                    String token = response.optString("Token");
                    CacheManager.getInstance().setChatSessionId(session);
                    CacheManager.getInstance().setChatToken(token);
                    OpenTokHandler.getInstance().initializeSession();



                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

                //  showDeleteAlert(SplashActivity.this,"Problem!",Constants.GenericErrorMsg+" Would you like to retry?");
            }
        });

    }
}
