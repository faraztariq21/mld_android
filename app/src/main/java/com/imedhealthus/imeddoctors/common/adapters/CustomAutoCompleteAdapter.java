package com.imedhealthus.imeddoctors.common.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by umair on 6/10/17.
 */

public class CustomAutoCompleteAdapter extends ArrayAdapter<String> implements TextWatcher {

    private AutoCompleteTextView textView;
    private ArrayList<String> allItems;
    private ArrayList<String> filteredItems;

    public CustomAutoCompleteAdapter(Context context, int resource, AutoCompleteTextView textView, ArrayList<String> allItems) {
        super(context, resource);
        TextView itemView = (TextView) LayoutInflater.from(context).inflate(resource, null);
        itemView.setPadding(0,0,0,0);
        this.textView = textView;
        this.allItems = allItems;
        filteredItems = new ArrayList<>();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return filteredItems.get(position);
    }

    @Override
    public int getCount() {
        return filteredItems.size();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        filterItems(textView.getText().toString());
        notifyDataSetChanged();

    }

    private void filterItems(String toSearch) {
        toSearch = toSearch.toLowerCase();
        filteredItems.clear();

        //If starts with
        for (String item : allItems) {
            if (item.toLowerCase().startsWith(toSearch)) {
                filteredItems.add(item);
            }
        }

        if (filteredItems.size() ==0) {
            for (String item : allItems) {
                if (item.toLowerCase().contains(toSearch)) {
                    filteredItems.add(item);
                }
            }
        }

        if(filteredItems.size()>50){
            return;
        }
        //If still no results then check next tokens
        toSearch = " " + toSearch;
        for (String item : allItems) {
            if (item.toLowerCase().contains(toSearch)) {
                filteredItems.add(item);
            }
        }

    }

}
