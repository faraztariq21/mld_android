package com.imedhealthus.imeddoctors.common.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.listeners.OnPaymentMethodItemClickListener;
import com.imedhealthus.imeddoctors.common.models.PaymentMethod;
import com.imedhealthus.imeddoctors.common.view_holders.PaymentMethodItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class PaymentMethodsListAdapter extends RecyclerView.Adapter<PaymentMethodItemViewHolder> {

    private List<PaymentMethod> methods;
    private WeakReference<TextView> tvNoRecords;
    private WeakReference<OnPaymentMethodItemClickListener> onPaymentMethodItemClickListener;

    public PaymentMethodsListAdapter(TextView tvNoRecords, OnPaymentMethodItemClickListener onPaymentMethodItemClickListener) {

        super();

        this.tvNoRecords = new WeakReference<>(tvNoRecords);
        this.onPaymentMethodItemClickListener = new WeakReference<>(onPaymentMethodItemClickListener);

    }

    public void setPaymentMethods(List<PaymentMethod> methods) {

        this.methods = methods;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }

    @Override
    public PaymentMethodItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_payment_method, parent, false);

        OnPaymentMethodItemClickListener listener = null;
        if (onPaymentMethodItemClickListener != null && onPaymentMethodItemClickListener.get() != null) {
            listener = onPaymentMethodItemClickListener.get();
        }

        return new PaymentMethodItemViewHolder(view, listener);

    }

    @Override
    public void onBindViewHolder(PaymentMethodItemViewHolder viewHolder, int position) {
        viewHolder.setData(getItem(position), position);
    }

    @Override
    public int getItemCount() {
        return methods == null ? 0 : methods.size();
    }

    public PaymentMethod getItem(int position) {
        if (position < 0 || position >= methods.size()) {
            return null;
        }
        return methods.get(position);
    }

}
