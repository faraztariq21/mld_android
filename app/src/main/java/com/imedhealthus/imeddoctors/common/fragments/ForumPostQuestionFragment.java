package com.imedhealthus.imeddoctors.common.fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.CustomArrayAdapter;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.ImageSelector;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.FileUtils;
import com.imedhealthus.imeddoctors.common.utils.Logs;
import com.imedhealthus.imeddoctors.common.utils.UploadImage;
import com.imedhealthus.imeddoctors.doctor.models.Speciality;
import com.imedhealthus.imeddoctors.doctor.models.StaticData;
import com.soundcloud.android.crop.Crop;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;
import static com.imedhealthus.imeddoctors.common.fragments.ChatFragment.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE_DOCUMENT;


public class ForumPostQuestionFragment extends BaseFragment implements View.OnClickListener, ImageSelector.OnImageSelectionListener {


    private static final int PICK_IMAGE_FOR_FORUM_QUESTION = 211;

    Uri selectedImageUri;

    @BindView(R.id.et_speciality)
    EditText etSpeciality;
    @BindView(R.id.et_question)
    EditText etQuestion;


    @BindView(R.id.sp_speciality)
    Spinner spSpeciality;
    @BindView(R.id.cb_anonymous)
    CheckBox cbAnonymous;
    @BindView(R.id.tv_icon_plus)
    TextView tvIconPlus;
    @BindView(R.id.btn_post_question)
    RelativeLayout bPostQuestion;
    @BindView(R.id.btn_post_image)
    Button bPostImage;
    @BindView(R.id.iv_post_image)
    ImageView ivPostImage;

    @BindView(R.id.iv_cancel_image)
    ImageView ivCancelImage;
    @BindView(R.id.fl_image_container)
    FrameLayout flImageContainer;

    private boolean isPublic;

    List<StaticData> listSpeciality;
    private Speciality speciality;
    private ImageSelector imageSelector;
    private String uploadName;
    private String uploadPath;
    private String imageUploadPath;
    private String loginId;
    private String userId;
    private String isAnonymous;
    private String isPublicString;
    private String doctorProfileId;
    private String specialityId;
    private String question;
    private FragmentAttachMultipleImages fragmentAttachMultipleImages;
    private FragmentAttachMultipleDocuments fragmentAttachMultipleDocuments;

    public ForumPostQuestionFragment() {
        speciality = SharedData.getInstance().getSelectedSpeciality();
        isPublic = SharedData.getInstance().isPublic();
    }


    @Override
    public String getName() {
        return "ForumPostQuestionFragment";
    }

    @Override
    public String getTitle() {
        return "Post A Question";
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_forum_post_question, container, false);
        ButterKnife.bind(this, view);
        initView();
        if (SharedData.getInstance().isPostQuestion()) {
            etQuestion.setText(SharedData.getInstance().getPostQuestionText());
            spSpeciality.setSelection(SharedData.getInstance().getPostQuestionSpecialityIndex());
        }

        return view;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PICK_IMAGE_FOR_FORUM_QUESTION && grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            openImageSelector();
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.permission_error), Toast.LENGTH_SHORT).show();
            return;
        }

        if (requestCode == MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE_DOCUMENT) {
            if (fragmentAttachMultipleDocuments != null)
                fragmentAttachMultipleDocuments.onRequestPermissionsResult(requestCode, permissions, grantResults);
        } else {
            if (fragmentAttachMultipleImages != null)
                fragmentAttachMultipleImages.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


    }

    private void initView() {

        listSpeciality = SharedData.getInstance().getSpecialityList();

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "FontAwesome.ttf");
        tvIconPlus.setTypeface(typeface);

        bPostQuestion.setOnClickListener(this);
        ivCancelImage.setOnClickListener(this);
        CustomArrayAdapter dataAdapter = new CustomArrayAdapter(context, R.layout.custom_spinner_item, listSpeciality);
        spSpeciality.setAdapter(dataAdapter);
        spSpeciality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
              /*  if(i==listSpeciality.size()-1) {
                    etSpeciality.setVisibility(View.VISIBLE);
                    etSubSpeciality.setVisibility(View.GONE);
                    return;
                } else{
                    etSpeciality.setVisibility(View.GONE);
                    etSubSpeciality.setVisibility(View.VISIBLE);
                }
                */
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        /*fragmentAttachMultipleImages = FragmentAttachMultipleImages.newInstance(true, new ArrayList<GeneralListItemImage>());
        getFragmentManager().beginTransaction().replace(R.id.ll_multiple_images_container, fragmentAttachMultipleImages).commit();
        fragmentAttachMultipleDocuments = FragmentAttachMultipleDocuments.newInstance(true, new ArrayList<GeneralListItemImage>());
        getFragmentManager().beginTransaction().replace(R.id.ll_multiple_documents_container, fragmentAttachMultipleDocuments).commit();
        */
        setData();

    }

    private void setData() {

        if (speciality == null) {
            return;
        }

        spSpeciality.setSelection(SharedData.getInstance().getIndexInList(listSpeciality, speciality.getName()));
        etSpeciality.setVisibility(View.GONE);


    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_post_question:
                postQuestion();
                break;

            case R.id.iv_cancel_image:

                cancelImage();
                break;
            case R.id.btn_post_image:
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    openImageSelector();
                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PICK_IMAGE_FOR_FORUM_QUESTION);
                }

                break;

        }
    }

    private void openImageSelector() {
        imageSelector = new ImageSelector(this, getActivity(), this);
        imageSelector.showSourceAlert();

    }

    private void cancelImage() {
        selectedImageUri = null;
        flImageContainer.setVisibility(View.GONE);
    }

    private void postQuestion() {


        loginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        userId = CacheManager.getInstance().getCurrentUser().getUserId();
        isAnonymous = cbAnonymous.isChecked() ? "true" : "false";
        isPublicString = isPublic ? "true" : "false";
        doctorProfileId = isPublic ? "" : "-1";
        specialityId = Long.toString(spSpeciality.getSelectedItemId());

        question = etQuestion.getText().toString();


        if (TextUtils.isEmpty(question.trim())) {
            String errorMsg = "Kindly fill question to continue";
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return;
        }

        if (selectedImageUri == null) {
            AlertUtils.showProgress(getActivity());
            WebServicesHandler.instance.postForumQuestion(loginId, userId, doctorProfileId, specialityId, question, isAnonymous, isPublicString, uploadPath, Constants.FileType.FILE_TYPE_IMAGE, "0", customCallback);

        } else {
            AlertUtils.showProgress(getActivity());
            uploadFile(selectedImageUri);
        }

    }

    CustomCallback customCallback = new CustomCallback<RetrofitJSONResponse>() {
        @Override
        public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
            Logs.apiResponse(response.toString());
            // AlertUtils.dismissProgress();
            if (!response.status()) {
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                return;
            }

            SharedData.getInstance().setRefreshViaServerHit(true);
            AlertUtils.showAlertForBack(getContext(), Constants.SuccessAlertTitle, getString(R.string.alert_question_posted));

        }

        @Override
        public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
            AlertUtils.dismissProgress();
            AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
        }
    };

    private String uploadFile(Uri uri) {
        uploadName = (new Date()).getTime() + ".png";

        uploadPath = Constants.FTP.patientDataPath + uploadName;

        UploadImage.OnImageUploadCompletionListener onImageUploadCompletionListener = new UploadImage.OnImageUploadCompletionListener() {
            @Override
            public void onImageUploadComplete(boolean success, int index, String uploadPath) {
//                Toast.makeText(getActivity(), "Image Attatched", Toast.LENGTH_SHORT).show();
                imageUploadPath = ForumPostQuestionFragment.this.uploadPath;
                AlertUtils.dismissProgress();
                //    AlertUtils.showProgress(getActivity());
                WebServicesHandler.instance.postForumQuestion(loginId, userId, doctorProfileId, specialityId, question, isAnonymous, isPublicString, ForumPostQuestionFragment.this.uploadPath, Constants.FileType.FILE_TYPE_IMAGE, "0", customCallback);

            }
        };
        UploadImage uploadImage = null;
        uploadImage = new UploadImage(uploadPath, FileUtils.getPathFromUri(getActivity(), uri), onImageUploadCompletionListener);
        uploadImage.setContext(getActivity());
        uploadImage.execute();

        return uploadPath;
    }


    @Override
    public void onImageSelected(Bitmap bitmap, Uri uri, Uri fullImageUri) {

        /*flImageContainer.setVisibility(View.VISIBLE);
        ivPostImage.setImageURI(uri);
*/
        selectedImageUri = uri;

        CropImage.activity(selectedImageUri).setInitialCropWindowPaddingRatio(0).start(getActivity());

    }

    //Image selection
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)

        {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                selectedImageUri = resultUri;
                //fragmentSendImageDialog.setFragmentSendImageClickListener(this);
                // fragmentSendImageDialog.show(getActivity().getSupportFragmentManager());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
            if (selectedImageUri != null) {
                flImageContainer.setVisibility(View.VISIBLE);
                ivPostImage.setImageURI(selectedImageUri);
            }


        }

        if (fragmentAttachMultipleImages != null)
            fragmentAttachMultipleImages.onActivityResult(requestCode, resultCode, data);

        if (fragmentAttachMultipleDocuments != null)
            fragmentAttachMultipleDocuments.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ImageSelector.CAMERA_REQUEST_CODE:
            case ImageSelector.GALLERY_REQUEST_CODE:
            case Crop.REQUEST_CROP:
                if (imageSelector != null) {
                    imageSelector.onActivityResult(requestCode, resultCode, data);
                }
                break;

        }

    }

}
