package com.imedhealthus.imeddoctors.common.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.adapters.AdapterListItemImage;
import com.imedhealthus.imeddoctors.common.listeners.FragmentAttachImageListener;
import com.imedhealthus.imeddoctors.common.models.GeneralListItemImage;
import com.imedhealthus.imeddoctors.common.models.ImageSelector;
import com.imedhealthus.imeddoctors.common.models.ListItemImage;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.soundcloud.android.crop.Crop;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

import static android.app.Activity.RESULT_OK;
import static com.imedhealthus.imeddoctors.common.fragments.ChatFragment.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE;

public class FragmentAttachMultipleImages extends android.support.v4.app.Fragment implements ImageSelector.OnImageSelectionListener, FragmentAttachImageListener {

    @BindView(R.id.iv_add_image)
    ImageView ivAddImage;
    @BindView(R.id.rv_images)
    RecyclerView rvImages;
    @BindView(R.id.tv_no_images)
    TextView tvNoImages;
    @BindView(R.id.tv_heading)
    TextView tvHeading;
    boolean showAddImageButton;
    ArrayList<GeneralListItemImage> generalListItemImages;

    AdapterListItemImage adapterListItemImage;


    public static final String SHOW_ADD_IMAGE_BUTTON = "show_add_image_button";
    public static final String GENERAL_LIST_ITEM_IMAGES = "general_list_item_images";

    private ImageSelector imageSelector;
    private Uri selectedImageUri;
    private ArrayList<String> imagePaths;

    public static FragmentAttachMultipleImages newInstance(boolean showAddImageButton, ArrayList<? extends ListItemImage> generalListItemImages) {
        FragmentAttachMultipleImages fragmentAttachMultipleImages = new FragmentAttachMultipleImages();
        Bundle bundle = new Bundle();
        bundle.putBoolean(SHOW_ADD_IMAGE_BUTTON, showAddImageButton);
        bundle.putSerializable(GENERAL_LIST_ITEM_IMAGES, generalListItemImages);

        fragmentAttachMultipleImages.setArguments(bundle);
        return fragmentAttachMultipleImages;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            showAddImageButton = getArguments().getBoolean(SHOW_ADD_IMAGE_BUTTON);
            generalListItemImages = (ArrayList<GeneralListItemImage>) getArguments().getSerializable(GENERAL_LIST_ITEM_IMAGES);
            imagePaths = new ArrayList<>();
            if (generalListItemImages != null) {
                for (ListItemImage listItemImage : generalListItemImages) {
                    imagePaths.add(listItemImage.getImagePath());
                }
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attach_multiple_images, container, false);
        ButterKnife.bind(this, view);

        if (showAddImageButton)
            ivAddImage.setVisibility(View.VISIBLE);
        else {
            ivAddImage.setVisibility(View.GONE);
            if (generalListItemImages == null || generalListItemImages.size() == 0) {
                tvNoImages.setVisibility(View.VISIBLE);
            }

            tvHeading.setVisibility(View.GONE);

        }


        ivAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(getActivity(),
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                } else {
                    openImageSelector();
                }

            }
        });

        adapterListItemImage = new AdapterListItemImage(getActivity(), this, generalListItemImages, true);
        rvImages.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvImages.setItemAnimator(new DefaultItemAnimator());
        rvImages.setAdapter(adapterListItemImage);
        return view;
    }

    @Override
    public void onImageSelected(Bitmap bitmap, Uri croppedImageUri, Uri fullImageUri) {
        CropImage.activity(fullImageUri).start(getActivity());
        selectedImageUri = fullImageUri;
    }

    @Override
    public void onImageCancelTapped(int position) {
        //generalListItemImages.remove(position);
        imagePaths.remove(position);
        adapterListItemImage.removeImage(position);

    }

    @Override
    public void onImageTapped(int position) {
        SharedData.getInstance().setSelectedImageURL(generalListItemImages.get(position).getImagePath());
        SimpleFragmentActivity.startActivity(getActivity(), Constants.Fragment.FragmentImageFull, null);
    }


    private void openFileSelector() {
        if (imageSelector == null) {
            imageSelector = new ImageSelector(this, getActivity(), this);
        }
        imageSelector.showSourceAlert();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {

                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    openImageSelector();
                } else {
                    Toast.makeText(getActivity(), "Permission denied. Unable to proceed", Toast.LENGTH_SHORT).show();

                }
                return;
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case FilePickerConst.REQUEST_CODE_PHOTO:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    imagePaths = new ArrayList<>();
                    imagePaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
                    generalListItemImages = GeneralListItemImage.getGeneralListItemsFromStrings(imagePaths, false, true);
                    adapterListItemImage.updateImages(generalListItemImages);
                }
                break;
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                selectedImageUri = resultUri;

                generalListItemImages.add(new GeneralListItemImage(resultUri.toString(), true, false));
                adapterListItemImage.updateImages(generalListItemImages);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        } else {


            switch (requestCode)

            {
                case ImageSelector.CAMERA_REQUEST_CODE:
                case ImageSelector.GALLERY_REQUEST_CODE:
                case Crop.REQUEST_CROP:
                    if (imageSelector != null) {
                        imageSelector.onActivityResult(requestCode, resultCode, data);
                    }
                    break;


            }
        }

    }

    private void openImageSelector() {
        FilePickerBuilder.getInstance().setMaxCount(10)
                .setSelectedFiles(imagePaths)
                .setActivityTheme(R.style.LibAppTheme)
                .pickPhoto(getActivity());

    }

    public ArrayList<String> getImagePaths() {
        return imagePaths != null ? imagePaths : new ArrayList<String>();
    }

}
