package com.imedhealthus.imeddoctors.common.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ChatWrapper implements Serializable {

    @SerializedName("ChatItem")
    ChatItem chatItem;
    @SerializedName("ChatMessage")
    ChatMessage chatMessage;

    public ChatWrapper() {
    }

    public ChatWrapper(ChatItem chatItem, ChatMessage chatMessage) {
        this.chatItem = chatItem;
        this.chatMessage = chatMessage;
    }

    public ChatItem getChatItem() {
        return chatItem;
    }

    public void setChatItem(ChatItem chatItem) {
        this.chatItem = chatItem;
    }

    public ChatMessage getChatMessage() {
        return chatMessage;
    }

    public void setChatMessage(ChatMessage chatMessage) {
        this.chatMessage = chatMessage;
    }
}
