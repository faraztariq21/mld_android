package com.imedhealthus.imeddoctors.common.listeners;

/**
 * Created by Malik Hamid on 1/5/2018.
 */

public interface OnFacebookResultsListener {
    void onFacebookLoginComplete(String id, String fullName);
    void onFacebookLoginError(String error);
}
