package com.imedhealthus.imeddoctors.common.models;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 2/14/2018.
 */

public class City implements Serializable, SuggestionListItem {
    String cityId;
    String stateId;
    String cityName;


    public static List<City> getCitiesAgainstCountryId(List<City> cities, String stateId) {
        List<City> list = new ArrayList<>();
        list.add(cities.get(0));
        for (int i = 1; i < cities.size(); i++) {
            if (cities.get(i).getStateId().equalsIgnoreCase(stateId))
                list.add(cities.get(i));
        }
        return list;
    }


    public City(String dummy) {
        this.cityId = "0";
        this.stateId = "0";
        this.cityName = "Select City";
    }

    public City() {
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Override
    public String toString() {
        return cityName;
    }

    @Override
    public String getSuggestionText() {
        return cityName;
    }

    @Override
    public SuggestionListItem getSuggestionListItem() {
        return City.this;

    }

    public static ArrayList<SuggestionListItem> getSuggestions(ArrayList<City> cities) {
        ArrayList<SuggestionListItem> suggestionListItems = new ArrayList<>();
        for (City city : cities) {
            suggestionListItems.add(city);
        }
        return suggestionListItems;
    }
}

