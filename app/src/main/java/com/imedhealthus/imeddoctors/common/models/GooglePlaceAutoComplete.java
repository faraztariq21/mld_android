package com.imedhealthus.imeddoctors.common.models;

/**
 * Created by umair on 1/6/18.
 */

public class GooglePlaceAutoComplete {

    private CharSequence placeId;
    private CharSequence description;
    private long lat;
    private long lng;

    public GooglePlaceAutoComplete(CharSequence placeId, CharSequence description) {
        this.placeId = placeId;
        this.description = description;

    }

    public CharSequence getPlaceId() {
        return placeId;
    }

    public void setPlaceId(CharSequence placeId) {
        this.placeId = placeId;
    }

    public CharSequence getDescription() {
        return description;
    }

    public void setDescription(CharSequence description) {
        this.description = description;
    }

    public long getLat() {
        return lat;
    }

    public void setLat(long lat) {
        this.lat = lat;
    }

    public long getLng() {
        return lng;
    }

    public void setLng(long lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return description.toString();
    }

}
