package com.imedhealthus.imeddoctors.common.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.listeners.OnMenuItemClickListener;
import com.imedhealthus.imeddoctors.common.models.MenuItem;
import com.imedhealthus.imeddoctors.common.view_holders.MenuItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by umair on 1/6/18.
 */

public class MenuListAdapter extends RecyclerView.Adapter<MenuItemViewHolder> {

    private List<MenuItem> menuItems;
    private WeakReference<OnMenuItemClickListener> onMenuItemClickListener;
    private int selectedIdx;

    public MenuListAdapter(List<MenuItem> menuItems, OnMenuItemClickListener onMenuItemClickListener) {

        super();
        this.menuItems = menuItems;
        this.onMenuItemClickListener = new WeakReference<>(onMenuItemClickListener);
        selectedIdx = 0;

    }

    public void setMenuItems(List<MenuItem> menuItems) {

        this.menuItems = menuItems;
        notifyDataSetChanged();

    }

    public void setCountFor(String title, int count) {

        for (MenuItem item : menuItems) {
            if (item.getTitle().equalsIgnoreCase(title)) {
                item.setCount(count);
                break;
            }
        }
        notifyDataSetChanged();

    }

    public void setSelectedIdx(int selectedIdx) {
        this.selectedIdx = selectedIdx;
        notifyDataSetChanged();
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_menu, parent, false);

        OnMenuItemClickListener listener = null;
        if (onMenuItemClickListener != null && onMenuItemClickListener.get() != null) {
            listener = onMenuItemClickListener.get();
        }

        return new MenuItemViewHolder(view, listener);

    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder viewHolder, int position) {
        viewHolder.setData(getItem(position), position == selectedIdx, position);
    }

    @Override
    public int getItemCount() {
        return menuItems == null ? 0 : menuItems.size();
    }

    public MenuItem getItem(int position) {
        if (position < 0 || position >= menuItems.size()) {
            return null;
        }
        return menuItems.get(position);
    }

}
