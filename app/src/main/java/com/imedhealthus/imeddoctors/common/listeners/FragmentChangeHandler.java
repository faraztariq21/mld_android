package com.imedhealthus.imeddoctors.common.listeners;

import android.os.Bundle;

import com.imedhealthus.imeddoctors.common.fragments.OverlayFragment;
import com.imedhealthus.imeddoctors.common.utils.Constants;

/**
 * Created by umair on 1/5/18.
 */

public interface FragmentChangeHandler {
    void changeFragment(Constants.Fragment fragment, Bundle arguments);
    void showOverlayFragment(OverlayFragment fragment, Object data);
    void hideOverlayFragment();
    void refreshOverlayHeader();
}
