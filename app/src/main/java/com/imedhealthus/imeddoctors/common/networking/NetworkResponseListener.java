package com.imedhealthus.imeddoctors.common.networking;

public interface NetworkResponseListener {
    void onSuccess(String call, Object response);
    void onFailure(String call, Throwable t);
}
