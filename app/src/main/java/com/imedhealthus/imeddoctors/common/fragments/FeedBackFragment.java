package com.imedhealthus.imeddoctors.common.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedBackFragment extends BaseFragment {
    @BindView(R.id.et_subject)
    EditText et_subject;
    @BindView(R.id.et_comments)
    EditText et_comments;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    public FeedBackFragment() {
        // Required empty public constructor
    }

    @Override
    public boolean showHeader() {
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.feedback_fragment, container, false);
        ButterKnife.bind(this, rootView);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateFields())
                    sendFeedback();
            }
        });
        return rootView;
    }

    @Override
    public String getName() {
        return "feedBack";
    }

    @Override
    public String getTitle() {
        return "Feedback";
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;
        if (et_subject.getText().toString().isEmpty() || et_comments.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }
        if (errorMsg != null) {
            AlertUtils.showAlert(getActivity(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }
        return true;
    }

    public void sendFeedback(){

        AlertUtils.showProgress(getActivity());
        String id =  CacheManager.getInstance().getCurrentUser().getLoginId();
        String subject = et_subject.getText().toString();
        String description = et_comments.getText().toString();
        WebServicesHandler.instance.sendFeedback(id, subject, description, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                et_comments.setText("");
                et_subject.setText("");
                AlertUtils.showAlert(getContext(), Constants.SuccessAlertTitle, "Your feedback is submitted successfully.");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });
    }

}
