package com.imedhealthus.imeddoctors.common.listeners;

/**
 * Created by umair on 1/9/18.
 */

public interface OnMenuItemClickListener {
    void onMenuItemClick(int idx);
}
