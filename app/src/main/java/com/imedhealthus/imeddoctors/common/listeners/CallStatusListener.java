package com.imedhealthus.imeddoctors.common.listeners;

/**
 * Created by Malik Hamid on 3/15/2018.
 */

public interface CallStatusListener {
     void callRejectListener();
     void callCancelListener();
     void callAcceptListener();
}
