package com.imedhealthus.imeddoctors.common.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.adapters.PaymentMethodsListAdapter;
import com.imedhealthus.imeddoctors.common.listeners.OnPaymentMethodItemClickListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.PaymentMethod;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 1/9/2018.
 */

public class PaymentMethodsFragment extends BaseFragment implements OnPaymentMethodItemClickListener {

    @BindView(R.id.rv_methods)
    RecyclerView rvMethods;
    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;

    private PaymentMethodsListAdapter adapterPaymentMethods;

    @Override
    public String getName() {
        return "PaymentMethodsFragment";
    }
    @Override
    public String getTitle() {
        return "Payment Methods";
    }

    @Override
    public boolean showHeader() {
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_payment_methods, container, false);
        ButterKnife.bind(this, rootView);

        initHelper();
        return rootView;
    }

    private void initHelper() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rvMethods.setLayoutManager(layoutManager);

        adapterPaymentMethods = new PaymentMethodsListAdapter(tvNoRecords, this);
        rvMethods.setAdapter(adapterPaymentMethods);

        adapterPaymentMethods.setPaymentMethods(CacheManager.getInstance().getCurrentUser().getPaymentMethods());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add_method:
                openAddPaymentMethod();
                break;

        }
    }

    public void openAddPaymentMethod(){

        SharedData.getInstance().setSelectedPaymentMethod(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditPaymentMethod.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }


    //Attachment item click
    @Override
    public void onPaymentMethodItemDetailClick(int idx) {

    }

    @Override
    public void onPaymentMethodItemEditClick(int idx) {

        PaymentMethod method = adapterPaymentMethods.getItem(idx);
        if (method == null) {
            return;
        }
        SharedData.getInstance().setSelectedPaymentMethod(method);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditPaymentMethod.ordinal());
        ((BaseActivity)context).startActivity(intent, true);

    }

    @Override
    public void onPaymentMethodItemDeleteClick(int idx) {

        PaymentMethod method = adapterPaymentMethods.getItem(idx);
        if (method == null) {
            return;
        }
        showDeleteAlert(method);

    }

    private void showDeleteAlert(final PaymentMethod method) {

        new AlertDialog.Builder(context)
                .setTitle("Confirm")
                .setMessage("Are you sure you want to delete this record?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        deletePaymentMethod(method);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        dialog.dismiss();

                    }})
                .show();

    }


    //Web services
    private void deletePaymentMethod(PaymentMethod method) {

        SharedData.getInstance().setRefreshRequired(true);

    }

}
