package com.imedhealthus.imeddoctors.common.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class FragmentEditDelete extends BaseDialogFragment {


    public static final String BUTTON_ONE_TEXT = "button_one_text";
    public static final String BUTTON_TWO_TEXT = "button_two_text";
    public static final String LIST_INDEX = "list_index";

    String buttonOneText;
    String buttonTwoText;
    int index;

    @BindView(R.id.btn_edit)
    Button btnOne;
    @BindView(R.id.btn_delete)
    Button btnTwo;


    OnFragmentEditDeleteClickListener fragmentEditDeleteClickListener;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentEditDeleteClickListener)
            fragmentEditDeleteClickListener = (OnFragmentEditDeleteClickListener) context;

    }

    public static FragmentEditDelete newInstance(String buttonOneText, String buttonTwoText, int listIndex) {
        FragmentEditDelete fragmentEditDelete = new FragmentEditDelete();
        Bundle bundle = new Bundle();
        bundle.putString(BUTTON_ONE_TEXT, buttonOneText);
        bundle.putString(BUTTON_TWO_TEXT, buttonTwoText);
        bundle.putInt(LIST_INDEX, listIndex);
        fragmentEditDelete.setArguments(bundle);
        return fragmentEditDelete;

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            buttonOneText = getArguments().getString(BUTTON_ONE_TEXT);
            buttonTwoText = getArguments().getString(BUTTON_TWO_TEXT);
            index = getArguments().getInt(LIST_INDEX);

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_delete, container, false);
        ButterKnife.bind(this, view);
        btnOne.setText(buttonOneText);
        btnTwo.setText(buttonTwoText);

        if (fragmentEditDeleteClickListener == null)
            fragmentEditDeleteClickListener = (OnFragmentEditDeleteClickListener) getParentFragment();

        btnOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentEditDeleteClickListener.onEditClick(index);
                FragmentEditDelete.this.dismiss();
            }
        });

        btnTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentEditDeleteClickListener.onDeleteClick(index);
                FragmentEditDelete.this.dismiss();
            }
        });

        return view;
    }

    public interface OnFragmentEditDeleteClickListener {
        public void onEditClick(int index);

        public void onDeleteClick(int index);
    }
}
