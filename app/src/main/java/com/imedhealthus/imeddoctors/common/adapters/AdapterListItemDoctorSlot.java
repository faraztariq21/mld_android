package com.imedhealthus.imeddoctors.common.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.listeners.OnNestedListItemClickListener;
import com.imedhealthus.imeddoctors.doctor.models.Appointment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterListItemDoctorSlot extends RecyclerView.Adapter<AdapterListItemDoctorSlot.SlotItemViewHolder> {

    Context context;
    ArrayList<Appointment> slots;
    OnNestedListItemClickListener onNestedListItemClickListener;
    int parentIndex;

    public AdapterListItemDoctorSlot(Context context, ArrayList<Appointment> slots, OnNestedListItemClickListener onNestedListItemClickListener, int parentIndex) {
        this.context = context;
        this.slots = slots;
        this.onNestedListItemClickListener = onNestedListItemClickListener;
        this.parentIndex = parentIndex;
    }

    @NonNull
    @Override
    public SlotItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_doctor_time_slot, parent, false);
        return new SlotItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SlotItemViewHolder holder, final int position) {
        holder.tvSlotDate.setText(slots.get(position).getFormattedDate());
        holder.tvSlotTime.setText(slots.get(position).getFormattedStartTime());

        holder.tvSlotTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNestedListItemClickListener.onNestedListItemClicked(parentIndex, position);
            }
        });
        holder.clMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNestedListItemClickListener.onNestedListItemClicked(parentIndex, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return slots.size();
    }

    public class SlotItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_slot_date)
        TextView tvSlotDate;
        @BindView(R.id.tv_slot_time)
        TextView tvSlotTime;
        @BindView(R.id.main_layout)
        ConstraintLayout clMainLayout;

        public SlotItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}
