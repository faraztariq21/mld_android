package com.imedhealthus.imeddoctors.common.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.listeners.OnMenuItemClickListener;
import com.imedhealthus.imeddoctors.common.models.MenuItem;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/9/18.
 */

public class MenuItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_menu)
    ViewGroup contMenu;

    @BindView(R.id.iv_icon)
    ImageView ivIcon;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_count)
    TextView tvCount;

    private int idx;
    private WeakReference<OnMenuItemClickListener> onMenuItemClickListener;

    public MenuItemViewHolder(View view, OnMenuItemClickListener onMenuItemClickListener) {

        super(view);
        ButterKnife.bind(this, view);

        contMenu.setOnClickListener(this);
        this.onMenuItemClickListener = new WeakReference<>(onMenuItemClickListener);

    }

    public void setData(MenuItem item, boolean isSelected, int idx) {

        ivIcon.setImageResource(item.getIconResource());
        tvTitle.setText(item.getTitle());

        if (item.getCount() > 0) {
            tvCount.setText(String.valueOf(item.getCount()));
            tvCount.setVisibility(View.VISIBLE);
        }else {
            tvCount.setVisibility(View.INVISIBLE);
        }

        if (isSelected) {

            tvTitle.setTextColor(GenericUtils.getColor(R.color.icon_yellow));
            ivIcon.setColorFilter(GenericUtils.getColor(R.color.icon_yellow));

        }else {

            tvTitle.setTextColor(GenericUtils.getColor(R.color.dark_gray));
            ivIcon.setColorFilter(GenericUtils.getColor(R.color.dark_gray));

        }

        this.idx = idx;

    }


    @Override
    public void onClick(View view) {

        if (onMenuItemClickListener == null || onMenuItemClickListener.get() == null) {
            return;
        }
        onMenuItemClickListener.get().onMenuItemClick(idx);

    }

}
