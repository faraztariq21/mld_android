package com.imedhealthus.imeddoctors.common.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.receiver.OnCallNotification;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.NotificationUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;


/**
 * Created by umair on 2/14/18.
 */


public class MessagingService extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        super.onMessageReceived(remoteMessage);

        LocalBroadcastManager.getInstance(MessagingService.this).sendBroadcast(new Intent(Constants.FirebaseMessage));

        Log.e("iMed", "Push: MessagingService ");

        if (!CacheManager.getInstance().isUserLoggedIn()) {
            return;
        }


        Intent intent = new Intent(Constants.Keys.PushNotificationBroadCast);
        HashMap<String, String> data = new HashMap<>();

        if (remoteMessage.getNotification() != null) {

            data.put(Constants.Keys.NotificationTitle, remoteMessage.getNotification().getTitle());
            data.put(Constants.Keys.NotificationBody, remoteMessage.getNotification().getBody());

        }
        if (remoteMessage.getData() != null)
            data.putAll(remoteMessage.getData());

        intent.putExtra(Constants.Keys.PushNotificationData, data);
        String Type = null;
        if (data.containsKey("type"))
            Type = data.get("type");

       /* Intent intentOpenActivity=new Intent(this,IncomingCallActivity.class);
        startActivity(intentOpenActivity);
            ApplicationManager.getContext().sendBroadcast(intent);

            if(!NotificationUtils.isAppIsInBackground(getApplicationContext())){
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.showNotificationMessageForCall(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody(),"",data);
            }
            */

        Context context = ApplicationManager.getContext();
        if (Type != null && Type.equals("CALL")) {

            if (data.get("loginId").equals(CacheManager.getInstance().getCurrentUser().getLoginId())) {
                return;
            }

            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intentBroadcast = new Intent(context, OnCallNotification.class);

            intentBroadcast.putExtra(Constants.Keys.PushNotificationData, data);

            PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intentBroadcast, PendingIntent.FLAG_UPDATE_CURRENT);

            if (android.os.Build.VERSION.SDK_INT > 18) {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), alarmIntent);
            } else {
                alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), alarmIntent);
            }

        } else if (Type != null && Type.equals("MSG")) {


            NotificationUtils notificationUtils = new NotificationUtils(context);
            notificationUtils.showNotificationMessageForMessage("My Live Doctors", "You have a new message"
                    , new SimpleDateFormat("MMMM").format(new Date()), new Intent());

        } else if (Type != null && Type.equals("NOTIFICATION")) {

            String msg = data.get("msg");
            String title = data.get("msg");


            if (!NotificationUtils.isAppIsInBackground(ApplicationManager.getContext())) {
                ApplicationManager.getContext().sendBroadcast(intent);
                return;
            }
            NotificationUtils notificationUtils = new NotificationUtils(context);
            notificationUtils.showNotificationMessage(context, title, msg
                    , new SimpleDateFormat("MMMM").format(new Date()), new Intent(), "", "");

            //if app is running


        }


    }
}
