package com.imedhealthus.imeddoctors.common.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.models.GeneralListItemImage;
import com.imedhealthus.imeddoctors.common.models.ImageSelector;
import com.imedhealthus.imeddoctors.common.models.ListItemImage;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.soundcloud.android.crop.Crop;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

import static android.app.Activity.RESULT_OK;
import static com.imedhealthus.imeddoctors.common.fragments.ChatFragment.PICK_DOCUMENT_CODE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentEditFieldWithButtons#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentEditFieldWithButtons extends Fragment implements PopupMenu.OnMenuItemClickListener, ImageSelector.OnImageSelectionListener {


    @BindView(R.id.et_new_message)
    EditText etMessage;
    @BindView(R.id.fl_plus)
    FrameLayout flPlus;
    @BindView(R.id.btn_send)
    Button bSend;
    @BindView(R.id.main_layout)
    LinearLayout mainLayout;
    @BindView(R.id.ll_multiple_files_continer)
    LinearLayout llMultipleFilesContainer;

    Uri selectedImageUri;
    Uri selectedFileUri;

    boolean imageSelected = false;
    boolean fileSelected = false;

    private static final String EDIT_TEXT_VALIDATION = "param1";
    private static final String SHOW_PLUS_BUTTON = "param2";

    private boolean editTextValidation;
    private boolean showPlusButton;

    FragmentEditFieldWithButtonsListener fragmentEditFieldWithButtonsListener;
    private ImageSelector imageSelector;
    private ArrayList<String> filePaths;


    FragmentAttachMultipleImages fragmentAttachMultipleImages;
    FragmentAttachMultipleDocuments fragmentAttachMultipleDocuments;

    @Override
    public void onResume() {
        super.onResume();
        if (imageSelected == true && fragmentEditFieldWithButtonsListener != null)
            fragmentEditFieldWithButtonsListener.onImageSelected(selectedImageUri);
        if (fileSelected == true && fragmentEditFieldWithButtonsListener != null)
            fragmentEditFieldWithButtonsListener.onFileSelected(selectedFileUri);

        imageSelected = false;
        fileSelected = false;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentEditFieldWithButtonsListener)
            fragmentEditFieldWithButtonsListener = (FragmentEditFieldWithButtonsListener) context;
    }

    public FragmentEditFieldWithButtons() {
        // Required empty public constructor
    }


    public static FragmentEditFieldWithButtons newInstance(boolean editTextValidation, boolean showPlusButton) {
        FragmentEditFieldWithButtons fragment = new FragmentEditFieldWithButtons();
        Bundle args = new Bundle();
        args.putBoolean(EDIT_TEXT_VALIDATION, editTextValidation);
        args.putBoolean(SHOW_PLUS_BUTTON, showPlusButton);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            editTextValidation = getArguments().getBoolean(EDIT_TEXT_VALIDATION);
            showPlusButton = getArguments().getBoolean(SHOW_PLUS_BUTTON);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_edit_field_with_buttons, container, false);
        ButterKnife.bind(this, view);


        if (fragmentEditFieldWithButtonsListener == null && getActivity() instanceof FragmentEditFieldWithButtonsListener)
            fragmentEditFieldWithButtonsListener = (FragmentEditFieldWithButtonsListener) getActivity();
        if (fragmentEditFieldWithButtonsListener == null && getParentFragment() instanceof FragmentEditFieldWithButtonsListener)
            fragmentEditFieldWithButtonsListener = (FragmentEditFieldWithButtonsListener) getParentFragment();
        if (!showPlusButton)
            flPlus.setVisibility(View.GONE);

        bSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionManager.beginDelayedTransition(mainLayout);
                }
                llMultipleFilesContainer.setVisibility(View.GONE);
                setBackPressedHandled(true);
                if (editTextValidation && etMessage.getText().toString().isEmpty() && (fragmentAttachMultipleImages == null || fragmentAttachMultipleImages.getImagePaths().size() == 0) && (fragmentAttachMultipleDocuments == null || fragmentAttachMultipleDocuments.getFilePaths().size() == 0)) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.fill_out_field_error), Toast.LENGTH_SHORT).show();
                } else {
                    if (fragmentEditFieldWithButtonsListener != null)
                        fragmentEditFieldWithButtonsListener.onSendTapped(etMessage.getText().toString(), fragmentAttachMultipleImages != null ? fragmentAttachMultipleImages.getImagePaths() : new ArrayList<String>(), fragmentAttachMultipleDocuments != null ? fragmentAttachMultipleDocuments.getFilePaths() : new ArrayList<String>());
                    etMessage.setText("");

                }
            }
        });

        flPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(flPlus);
            }
        });

        return view;
    }

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(getActivity(), v);
        popup.setOnMenuItemClickListener(this);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.pick_up_file_menu, popup.getMenu());
        popup.show();

    }

    public void displaySelectedImages(ArrayList<? extends ListItemImage> generalListItemImages) {
        fragmentAttachMultipleImages = FragmentAttachMultipleImages.newInstance(false, generalListItemImages);
        loadFragmentAttachMultipleImages(fragmentAttachMultipleImages);
    }

    public void displaySelectedDocuments(ArrayList<? extends ListItemImage> generalListItemDocuments) {
        fragmentAttachMultipleDocuments = FragmentAttachMultipleDocuments.newInstance(false, generalListItemDocuments);
        loadFragmentAttachMultipleDocuments(fragmentAttachMultipleDocuments);
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.attach_image:
                fragmentAttachMultipleImages = FragmentAttachMultipleImages.newInstance(true, new ArrayList<GeneralListItemImage>());
                loadFragmentAttachMultipleImages(fragmentAttachMultipleImages);
                fragmentAttachMultipleDocuments = null;
                //openFileSelector();
                return true;
            case R.id.attach_document:
                fragmentAttachMultipleDocuments = FragmentAttachMultipleDocuments.newInstance(true, new ArrayList<GeneralListItemImage>());
                loadFragmentAttachMultipleDocuments(fragmentAttachMultipleDocuments);
                fragmentAttachMultipleImages = null;
                /*//FileUtils.chooseFiles("application/*", PICK_DOCUMENT_CODE, getActivity(), FragmentEditFieldWithButtons.this);
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.PICK_DOCUMENT_WRITE_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
                } else {
                    openDocumentSelector();
                }
                */
                return true;
            default:
                return false;
        }
    }

    private void loadFragmentAttachMultipleDocuments(FragmentAttachMultipleDocuments fragmentAttachMultipleDocuments) {
        getChildFragmentManager().beginTransaction().replace(R.id.ll_multiple_files_continer, fragmentAttachMultipleDocuments).commit();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.beginDelayedTransition(mainLayout);
        }
        llMultipleFilesContainer.setVisibility(View.VISIBLE);
        setBackPressedHandled(false);
    }

    private void loadFragmentAttachMultipleImages(FragmentAttachMultipleImages fragmentAttachMultipleImages) {
        getChildFragmentManager().beginTransaction().replace(R.id.ll_multiple_files_continer, fragmentAttachMultipleImages).commit();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.beginDelayedTransition(mainLayout);
        }
        llMultipleFilesContainer.setVisibility(View.VISIBLE);
        setBackPressedHandled(false);

    }


    private void setBackPressedHandled(boolean b) {
        if (getParentFragment() instanceof ChatFragment) {
            ((ChatFragment) getParentFragment()).setOnBackPressedHandled(b);
        }
    }

    private void openDocumentSelector() {
        FilePickerBuilder.getInstance().setMaxCount(1)
                .setSelectedFiles(filePaths)
                .setActivityTheme(R.style.LibAppTheme)

                .pickFile(getActivity());

    }

    private void openFileSelector() {
        if (imageSelector == null) {
            imageSelector = new ImageSelector(this, getActivity(), this);

        }
        imageSelector.showSourceAlert();

    }

    @Override
    public void onImageSelected(Bitmap bitmap, Uri croppedImageUri, Uri fullImageUri) {
        CropImage.activity(fullImageUri).start(getActivity());

    }

    public interface FragmentEditFieldWithButtonsListener {
        public void onSendTapped(String text, ArrayList<String> selectedImagesPaths, ArrayList<String> selectedFilesPaths);


        public void onImageSelected(Uri selectedImageUri);

        public void onFileSelected(Uri selectedFileUri);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (fragmentAttachMultipleDocuments != null)
            fragmentAttachMultipleDocuments.onActivityResult(requestCode, resultCode, data);

        if (fragmentAttachMultipleImages != null)
            fragmentAttachMultipleImages.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case FilePickerConst.REQUEST_CODE_PHOTO:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    filePaths = new ArrayList<>();
                    filePaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
                }
                break;
            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    filePaths = new ArrayList<>();
                    filePaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                   /* if (filePaths.size() > 0) {

                        //selectedFileUri = FileProvider.getUriForFile(getActivity(), getResources().getString(R.string.file_provide_authority), new File(filePaths.get(0)));
*//*
                        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
*//*
                        selectedFileUri = Uri.fromFile(new File(filePaths.get(0)));
                        *//*else
                            selectedFileUri = FileProvider.getUriForFile(getActivity(),getResources().getString(R.string.file_provide_authority),new File(filePaths.get(0)));
*//*
                        //FileUtils.openFile(getActivity(), selectedFileUri.toString());
                        String message = "Are you sure you want to share " + FileUtils.getFileNameFromUri(getActivity(), selectedFileUri);

                        DialogInterface.OnClickListener onPositiveClickListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                fragmentEditFieldWithButtonsListener.onFileSelected(selectedFileUri);
                            }
                        };

                        DialogInterface.OnClickListener onNegativeClickListne = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                selectedFileUri = null;
                                selectedImageUri = null;
                                dialog.dismiss();
                            }
                        };

                        *//*if (FileUtils.getFileNameFromUri(getActivity(), selectedFileUri).endsWith(".pdf"))
                     *//*
                        AlertUtils.showAlert(getActivity(), getResources().getString(R.string.share_file), message, onPositiveClickListener, onNegativeClickListne);
*//*
                        else {
                            Toast.makeText(getActivity(), "Please choose a pdf file", Toast.LENGTH_SHORT).show();
*//*
                        //selectedFileUri = null;

                    }*/
                }
                break;
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                selectedImageUri = resultUri;


                imageSelected = true;
                //fragmentSendImageDialog.setFragmentSendImageClickListener(this);
                // fragmentSendImageDialog.show(getActivity().getSupportFragmentManager());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        } else {


            //String path = FileUtils.getPathFromUri(getActivity(), uri1);
            switch (requestCode)

            {
                case ImageSelector.CAMERA_REQUEST_CODE:
                case ImageSelector.GALLERY_REQUEST_CODE:
                case Crop.REQUEST_CROP:
                    if (imageSelector != null) {
                        imageSelector.onActivityResult(requestCode, resultCode, data);
                    }
                    break;

                case PICK_DOCUMENT_CODE:

                    FilePickerBuilder.getInstance().setMaxCount(1)
                            .setSelectedFiles(filePaths)
                            .setActivityTheme(R.style.LibAppTheme)

                            .pickFile(getActivity());

                    /*Uri uri1 = data.getData();
                    if (uri1 != null) {
                        if (FileUtils.checkFilesFromDownloadFolder(getActivity(), uri1)) {
                            uri1 = FileUtils.copyFileAndGetUri(getActivity(), uri1, FileUtils.getFileNameFromUri(getActivity(), uri1));

                        }


                        selectedFileUri = uri1;

                        fileSelected = true;
*/
                    //uploadFile(FileUtils.getFileNameFromUri(getActivity(), uri1), uri1, onDocumentUploadCompletionListener);
                    break;

                //}

            }
        }

    }


    public void getFocusOnEditText() {
        etMessage.requestFocus();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (fragmentAttachMultipleImages != null)
            fragmentAttachMultipleImages.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (fragmentAttachMultipleDocuments != null)
            fragmentAttachMultipleDocuments.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.PICK_DOCUMENT_WRITE_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openDocumentSelector();
            return;
        }
        if (imageSelector != null)
            imageSelector.onRequestPermissionsResult(requestCode, permissions, grantResults);


    }

    public EditText getEtMessage() {
        return etMessage;
    }
}
