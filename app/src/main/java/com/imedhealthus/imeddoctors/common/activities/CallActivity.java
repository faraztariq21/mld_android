package com.imedhealthus.imeddoctors.common.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.CallFragment;
import com.imedhealthus.imeddoctors.common.fragments.ChatFragment;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.utils.Constants;

import butterknife.ButterKnife;
import pub.devrel.easypermissions.EasyPermissions;

import static com.imedhealthus.imeddoctors.common.fragments.CallFragment.VIDEO_APP_PERM;
import static com.imedhealthus.imeddoctors.common.fragments.CallFragment.WRITE_SETTINGS_PERM;
import static com.imedhealthus.imeddoctors.common.utils.Constants.CODE_DRAW_OVER_OTHER_APP_PERMISSION;

public class CallActivity extends BaseFragmentActivity {
    String[] permissions = {Manifest.permission.INTERNET, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        ButterKnife.bind(this);
        checkPermissionsAndStartCallFragment();

    }


    @Override
    public void onBackPressed() {
        if (currentOverlayFragment != null) {
            onDismissOverlayClick(null);
            return;
        }

        if (currentFragment instanceof ChatFragment && !((ChatFragment) currentFragment).isOnBackPressedHandled())
            ((ChatFragment) currentFragment).onBackPressed();
        else
            super.onBackPressed();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == WRITE_SETTINGS_PERM) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.System.canWrite(this)) {
                checkPermissionsAndStartCallFragment();
            } else {
                Toast.makeText(CallActivity.this, getResources().getString(R.string.permission_error), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == CODE_DRAW_OVER_OTHER_APP_PERMISSION) {

            //Check if the permission is granted or not.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(this)) {
                checkPermissionsAndStartCallFragment();
            } else { //Permission is not available
                Toast.makeText(CallActivity.this, getResources().getString(R.string.permission_error), Toast.LENGTH_SHORT).show();


            }
        } else if (currentFragment != null)
            currentFragment.onActivityResult(requestCode, resultCode, data);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CallFragment.VIDEO_APP_PERM:
                if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    checkPermissionsAndStartCallFragment();
                } else {
                    Toast.makeText(this, getResources().getString(R.string.permission_error), Toast.LENGTH_SHORT).show();
                }

             /*   if (currentFragment instanceof CallFragment) {
                    currentFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
                break;
            case AppointmentsFragment.REQUEST_CALL_PERMISSION:
                if (DoctorProfileFragment.currentFragment instanceof AppointmentsFragment) {
                    DoctorProfileFragment.currentFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
                break;
            case SelectTimeFragment.REQUEST_CALL_PERMISSION:
                if (currentFragment instanceof SelectTimeFragment) {
                    currentFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
                break;*/
        }
    }

    private void checkPermissionsAndStartCallFragment() {
        if (EasyPermissions.hasPermissions(this, permissions)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.System.canWrite(this)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                        if (Settings.canDrawOverlays(this)) {
                            openCallFragment();


                        } else {
                            SharedData.getInstance().setAskingPermissionsOnAcceptingCall(true);
                            startDrawOverOtherAppsActivity();
                        }
                    } else {
                        openCallFragment();
                    }


                } else {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, WRITE_SETTINGS_PERM);
                }
            } else {
                openCallFragment();
            }
        } else {

            ActivityCompat.requestPermissions(this, permissions, VIDEO_APP_PERM);

        }

    }

    public void openCallFragment() {
        changeFragment(Constants.Fragment.CallFragment, null);
    }
}
