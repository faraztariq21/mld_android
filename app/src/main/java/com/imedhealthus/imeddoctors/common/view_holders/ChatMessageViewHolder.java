package com.imedhealthus.imeddoctors.common.view_holders;

import android.content.Context;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.ChatMessage;
import com.imedhealthus.imeddoctors.common.utils.SquareLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 1/16/2018.
 */

public class ChatMessageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_message)
    ConstraintLayout contMessage;

    @BindView(R.id.tv_message_body)
    TextView tvMessage;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.iv_chat_image)
    public
    ImageView ivChatImage;
    @BindView(R.id.square_layout)
    public
    SquareLayout squareLayout;
    @BindView(R.id.progress_bar)
    public ProgressBar progressBar;

    @BindView(R.id.ll_message)
    LinearLayout llContMessage;
    Context context;

    @BindView(R.id.tv_file_name)
    public
    TextView tvFileName;

    public ChatMessageViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        context = view.getContext();

        tvMessage.setOnClickListener(this);
    }

    public void setData(ChatMessage msgItem) {

        if (msgItem != null && msgItem.getMsg() != null && !msgItem.getMsg().isEmpty() && !msgItem.getMsg().equals("null")) {
            tvMessage.setText(msgItem.getMsg());
            llContMessage.setVisibility(View.VISIBLE);
            } else
            llContMessage.setVisibility(View.GONE);
        tvTime.setText(msgItem.getTimeAgo());

        String id = CacheManager.getInstance().getCurrentUser().getLoginId();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_message:
            case R.id.tv_message_body:

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionManager.beginDelayedTransition(contMessage);
                }
                if (tvTime.getVisibility() == View.VISIBLE)
                    tvTime.setVisibility(View.GONE);
                else
                    tvTime.setVisibility(View.VISIBLE);
        }
    }
}
