package com.imedhealthus.imeddoctors.common.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.imedhealthus.imeddoctors.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentSelectFileType#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentSelectFileType extends BaseDialogFragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    FragmentSelectFileTypeListener fragmentSelectFileTypeListener;

    @BindView(R.id.ll_image)
    LinearLayout contPickImage;
    @BindView(R.id.ll_pdf)
    LinearLayout contPickDocument;

    public FragmentSelectFileType() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentSelectFileTypeListener)
            fragmentSelectFileTypeListener = (FragmentSelectFileTypeListener) context;
    }

    // TODO: Rename and change types and number of parameters
    public static FragmentSelectFileType newInstance() {
        FragmentSelectFileType fragment = new FragmentSelectFileType();
        Bundle args = new Bundle();
        /*args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        */fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_select_file_type, container, false);
        if (fragmentSelectFileTypeListener == null) {
            if (getActivity() instanceof FragmentSelectFileTypeListener) {
                fragmentSelectFileTypeListener = (FragmentSelectFileTypeListener) getActivity();
            }
        } else if (fragmentSelectFileTypeListener == null && getParentFragment() instanceof FragmentSelectFileTypeListener) {
            fragmentSelectFileTypeListener = (FragmentSelectFileTypeListener) getParentFragment();
        }

        contPickDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentSelectFileTypeListener.onPickDocumentTapped();
            }
        });

        contPickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentSelectFileTypeListener.onPickImageTapped();
            }
        });

        ButterKnife.bind(this, view);
        return view;
    }

    public interface FragmentSelectFileTypeListener {
        public void onPickImageTapped();

        public void onPickDocumentTapped();
    }

}
