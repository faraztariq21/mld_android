package com.imedhealthus.imeddoctors.common.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.adapters.QuestionsAdapter;
import com.imedhealthus.imeddoctors.common.listeners.OnChatItemClickListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.ForumComment;
import com.imedhealthus.imeddoctors.common.models.ForumQuestion;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.NetworkResponseParser;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.Logs;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ForumFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ForumFragment extends BaseFragment implements OnChatItemClickListener, View.OnClickListener, TextWatcher {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @BindView(R.id.rv_forum)
    RecyclerView rvForum;
    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;
    @BindView(R.id.tv_icon_plus)
    TextView tvIconPlus;
    @BindView(R.id.btn_post_question)
    RelativeLayout bPostQuestion;
    @BindView(R.id.et_search)
    EditText etSearch;

    private QuestionsAdapter questionsAdapter;
    private List<ForumQuestion> forumQuestions = new ArrayList<>();

    public ForumFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ForumFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ForumFragment newInstance(String param1, String param2) {
        ForumFragment fragment = new ForumFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopShimmerAnimation();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forum, container, false);
        ButterKnife.bind(this, view);
        initializeShimmer(view);
        initHelper();
        loadData();
        return view;
    }

    public void initHelper() {

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvForum.setLayoutManager(mLayoutManager);

        questionsAdapter = new QuestionsAdapter(getActivity(), tvNoRecords, this);
        rvForum.setAdapter(questionsAdapter);

        if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor) {
            bPostQuestion.setVisibility(View.GONE);
        } else {
            bPostQuestion.setVisibility(View.VISIBLE);
        }

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "FontAwesome.ttf");
        tvIconPlus.setTypeface(typeface);

        bPostQuestion.setOnClickListener(this);

        etSearch.addTextChangedListener(this);

    }

    @Override
    public String getName() {
        return "ForumFragment";
    }

    @Override
    public String getTitle() {
        return "Forum";
    }

    @Override
    public boolean showHeader() {
        return true;
    }

    @Override
    public void onChatItemClick(int position) {
        ForumQuestion forumQuestion = questionsAdapter.getItem(position);
        if (forumQuestion == null) {
            return;
        }


        Bundle bundle = new Bundle();
        bundle.putSerializable(ForumCommentsFragment.FORUM_QUESTION, forumQuestion);
        SimpleFragmentActivity.startActivity(getActivity(), Constants.Fragment.ForumCommentsFragment, bundle);
    }

    @Override
    public void onChatItemFileClic(int idx) {

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_post_question:
                SharedData.getInstance().setSelectedSpeciality(null);
                SharedData.getInstance().setPublic(true);
                Intent intent = new Intent(context, SimpleFragmentActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.ForumPostQuestionFragment.ordinal());
                ((BaseActivity) context).startActivity(intent, true);
                break;
        }
    }

    //Web services
    private void loadData() {

        startShimmerAnimation();
        String isPublic = "true";
        WebServicesHandler.instance.getAllForums(null, isPublic, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                stopShimmerAnimation();
                forumQuestions = new ArrayList<>();

                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                } else {
                    JSONArray arrJson = response.getJSONArray("QuestionInfoList");
                    if (arrJson != null) {
                        forumQuestions = NetworkResponseParser.getAllForums(arrJson);
                    }
                }
                questionsAdapter.setQuestions(forumQuestions);

                updateDoctorCommentsNotificationStatus();
                updatePatientCommentsNotificationStatus();
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                //AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        String searchTxt = etSearch.getText().toString().toLowerCase();
        List<ForumQuestion> filteredQuestions = new ArrayList<>();

        for (ForumQuestion forumQuestion : forumQuestions) {
            if (forumQuestion.getQuestion().toLowerCase().contains(searchTxt)) {
                filteredQuestions.add(forumQuestion);
            }
        }
        questionsAdapter.setQuestions(filteredQuestions);
    }

    public void updateDoctorCommentsNotificationStatus() {
        String userId = CacheManager.getInstance().getCurrentUser().getUserId();
        String userType = CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor ? "doctor" : "patient";
        String notificationType = "DoctorForumComments";
        boolean isPublic = true;
        WebServicesHandler.instance.updateNotificationStatus(userId, userType, notificationType, isPublic, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                //AlertUtils.dismissProgress();
                if (response.status()) {

                } else {

                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

            }
        });
    }

    public void updatePatientCommentsNotificationStatus() {
        String userId = CacheManager.getInstance().getCurrentUser().getUserId();
        String userType = CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor ? "doctor" : "patient";
        String notificationType = "PatientForumComments";
        boolean isPublic = true;
        WebServicesHandler.instance.updateNotificationStatus(userId, userType, notificationType, isPublic, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                //AlertUtils.dismissProgress();
                if (response.status()) {

                } else {

                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

            }
        });
    }

    @Override
    public void onCommentReceived(ForumComment forumComment) {

    }
}

