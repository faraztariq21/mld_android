package com.imedhealthus.imeddoctors.common.models;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.listeners.LocationLoaderListener;

/**
 * Created by umair on 1/6/18.
 */

public class LocationLoader implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    public static int LOCATION_SETTINGS_REQUEST_CODE = 999;

    private static LocationLoader shared;

    public static LocationLoader getShared() {
        if (shared == null) {
            shared = new LocationLoader();
        }
        return shared;
    }

    private GoogleApiClient googleApiClient;
    private Boolean permissionsChecked = false;
    private LocationRequest locationRequest;
    private LatLng currentLocation;

    private LocationLoaderListener locationLoaderListener;

    private LocationLoader() {

        googleApiClient = new GoogleApiClient.Builder(ApplicationManager.getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

        locationRequest = new LocationRequest();
        locationRequest.setInterval(30000);
        locationRequest.setFastestInterval(10000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(5);

    }

    public LocationLoader setLocationLoaderListener(LocationLoaderListener locationLoaderListener) {
        this.locationLoaderListener = locationLoaderListener;
        return this;
    }

    public LatLng getCurrentLocation() {
        return currentLocation;
    }

    public LocationLoader startLocationUpdate() {
        googleApiClient.connect();
        return this;
    }

    private Boolean checkPermissions() {

        Activity activity = ((ApplicationManager)ApplicationManager.getContext()).getCurrentActivity();
        if (activity == null) {
            Toast.makeText(ApplicationManager.getContext(), "Turn on location from settings.", Toast.LENGTH_LONG).show();
            return false;
        }

        LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

            alertDialog.setMessage("Turn on location from settings.");

            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                    Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    Activity activity = ((ApplicationManager)ApplicationManager.getContext()).getCurrentActivity();
                    activity.startActivityForResult(gpsOptionsIntent, LOCATION_SETTINGS_REQUEST_CODE);

                }
            });

            alertDialog.show();
            return false;
        }
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (!permissionsChecked) {
            permissionsChecked = true;
            if (!checkPermissions()){
                googleApiClient.disconnect();
                return;
            }

        }
        if (ActivityCompat.checkSelfPermission(ApplicationManager.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(ApplicationManager.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            Activity activity = ((ApplicationManager)ApplicationManager.getContext()).getCurrentActivity();
            if (activity != null && activity instanceof BaseActivity) {
                ((BaseActivity)activity).requestLocationPermission();
            }
            googleApiClient.disconnect();
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);

    }

    @Override
    public void onLocationChanged(Location location) {

        if (location == null) {
            return;
        }

        currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
        if (locationLoaderListener != null) {
            locationLoaderListener.locationReceived(currentLocation);
        }
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        Log.e("LocationLoader", "location received " + location.getLatitude() + "," + location.getLongitude());

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("LocationLoader", "suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
       // Log.e("LocationLoader", connectionResult.getErrorMessage());
    }
}

