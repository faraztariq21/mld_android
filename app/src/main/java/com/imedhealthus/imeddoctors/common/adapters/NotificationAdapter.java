package com.imedhealthus.imeddoctors.common.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.listeners.OnNotificationClickListener;
import com.imedhealthus.imeddoctors.common.models.Notification;

import com.imedhealthus.imeddoctors.common.view_holders.NotificationViewHolder;


import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Malik Hamid on 1/16/2018.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationViewHolder> {

    private List<Notification> notifications;
    private WeakReference<TextView> tvNoRecords;
    private WeakReference<OnNotificationClickListener> onNotificationClickListener;

    public NotificationAdapter(TextView tvNoRecords, OnNotificationClickListener onNotificationClickListener) {

        super();

        this.tvNoRecords = new WeakReference<>(tvNoRecords);
        this.onNotificationClickListener = new WeakReference<>(onNotificationClickListener);

    }

    public void setNotifications(List<Notification> notifications) {

        this.notifications = notifications;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }


    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_notification, parent, false);
        OnNotificationClickListener listener = null;
        if (onNotificationClickListener != null && onNotificationClickListener.get() != null) {
            listener = onNotificationClickListener.get();
        }

        return new NotificationViewHolder(view, listener);

    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        Notification notification=getItem(position);
        holder.setData(notification);

    }

    @Override
    public int getItemCount() {
        return notifications == null ? 0 : notifications.size();
    }

    public Notification getItem(int position) {
        if (position < 0 || position >= notifications.size()) {
            return null;
        }
        return notifications.get(position);
    }

}
