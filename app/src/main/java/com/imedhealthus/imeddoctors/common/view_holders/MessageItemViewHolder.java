package com.imedhealthus.imeddoctors.common.view_holders;

import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.listeners.OnChatItemClickListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.ChatItem;
import com.imedhealthus.imeddoctors.common.models.ChatUserModel;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.TinyDB;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 1/16/2018.
 */

public class MessageItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    RelativeLayout contItem;

    @BindView(R.id.iv_profile)
    ImageView ivProfile;
    @BindView(R.id.tv_name)
    TextView tvName;


    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_message)
    TextView tvMessage;

    private int idx;
    private WeakReference<OnChatItemClickListener> onChatItemClickListener;

    public MessageItemViewHolder(View view, OnChatItemClickListener onChatItemClickListener) {

        super(view);
        ButterKnife.bind(this, view);

        this.onChatItemClickListener = new WeakReference<>(onChatItemClickListener);
        contItem.setOnClickListener(this);
        tvMessage.setOnClickListener(this);

    }

    public void setData(ChatItem chatItem, int idx) {

        String userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        ChatUserModel partner = chatItem.getPartner(userLoginId);

        if (partner == null) {
            tvName.setText("");
            ivProfile.setImageResource(0);
        } else {
            tvName.setText(partner.getName());
            Glide.with(ApplicationManager.getContext()).load(partner.getImgURL()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivProfile);
        }

        tvMessage.setText(chatItem.getLastMessage());
        if (new TinyDB(ApplicationManager.getContext()).getBoolean(chatItem.getChatId() + Constants.hasUnreadMessages)) {
            tvName.setTypeface(tvName.getTypeface(), Typeface.BOLD_ITALIC);

            tvMessage.setTypeface(tvMessage.getTypeface(), Typeface.BOLD_ITALIC);
            contItem.setBackgroundColor(ContextCompat.getColor(ApplicationManager.getContext(), R.color.white));
        }
        tvTime.setText(chatItem.getTimeAgo());
        this.idx = idx;

    }

    @Override
    public void onClick(View view) {

        if (onChatItemClickListener == null || onChatItemClickListener.get() == null) {
            return;
        }

        switch (view.getId()) {
            case R.id.cont_item:
            case R.id.tv_message:
                onChatItemClickListener.get().onChatItemClick(getAdapterPosition());
                break;
        }
    }
}
