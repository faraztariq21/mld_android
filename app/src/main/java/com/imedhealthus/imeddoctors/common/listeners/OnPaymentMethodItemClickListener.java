package com.imedhealthus.imeddoctors.common.listeners;

/**
 * Created by umair on 1/9/18.
 */

public interface OnPaymentMethodItemClickListener {
    void onPaymentMethodItemDetailClick(int idx);
    void onPaymentMethodItemEditClick(int idx);
    void onPaymentMethodItemDeleteClick(int idx);
}
