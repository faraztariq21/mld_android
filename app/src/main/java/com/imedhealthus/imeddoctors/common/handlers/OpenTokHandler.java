package com.imedhealthus.imeddoctors.common.handlers;

/**
 * Created by Malik Hamid on 2/12/2018.
 */

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.CallActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.listeners.CallStatusListener;
import com.imedhealthus.imeddoctors.common.listeners.OnChatMessageReceivedListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.CallData;
import com.imedhealthus.imeddoctors.common.models.CallSignal;
import com.imedhealthus.imeddoctors.common.models.ChatMessage;
import com.imedhealthus.imeddoctors.common.models.ChatWrapper;
import com.imedhealthus.imeddoctors.common.models.ForumComment;
import com.imedhealthus.imeddoctors.common.models.ForumCommentWrapper;
import com.imedhealthus.imeddoctors.common.models.LiveNotifications;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.Logs;
import com.imedhealthus.imeddoctors.common.utils.NotificationUtils;
import com.opentok.android.Connection;
import com.opentok.android.OpentokError;
import com.opentok.android.Session;
import com.opentok.android.Stream;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;

import pub.devrel.easypermissions.EasyPermissions;

import static com.imedhealthus.imeddoctors.common.fragments.CallFragment.VIDEO_APP_PERM;
import static com.imedhealthus.imeddoctors.common.fragments.CallFragment.WRITE_SETTINGS_PERM;
import static com.imedhealthus.imeddoctors.common.utils.Constants.CODE_DRAW_OVER_OTHER_APP_PERMISSION;

public class OpenTokHandler implements Session.SessionListener, Session.SignalListener, Session.ReconnectionListener, Session.ConnectionListener {

    private static final OpenTokHandler ourInstance = new OpenTokHandler();
    private int Call_NOTIFICATION_ID = 1222;
    private Dialog dialog;
    private Ringtone ringtone;

    public static OpenTokHandler getInstance() {
        return ourInstance;
    }

    private static final String LOG_TAG = OpenTokHandler.class.getSimpleName();
    public static final String SIGNAL_TYPE_NOTIFICATION = "notification";
    public static final String SIGNAL_TYPE_CHAT = "msg";
    public static final String SIGNAL_TYPE_COMMENT = "comment";
    public static final String SIGNAL_TYPE_CALL = "begincall";
    public static final String SIGNAL_TYPE_CALL_REJECTED = "call_rejected";
    public static final String SIGNAL_TYPE_CALL_ACCEPTED = "acceptcall";
    public static final String SIGNAL_TYPE_CALL_CANCEL = "cancelcall";
    public static final String CALL_ACCEPTED_YES = "yes";
    public static final String CALL_ACCEPTED_NO = "no";
    public static final String CALL_END = "endcall";
    public static final String NO_RESPONSE = "no_response";
    private WeakReference<OnChatMessageReceivedListener> onChatMessageReceivedListener;
    private WeakReference<CallStatusListener> callStatusListenerWeakReference;
    private Session session;

    private Handler customHandler;
    private long timeInMilliseconds = 0;
    private long startTime = 0;
    private long callAcceptenceTime = 35000;

    private OpenTokHandler() {

    }

    public void intializeListerner(CallStatusListener callStatusListener) {
        callStatusListenerWeakReference = new WeakReference<CallStatusListener>(callStatusListener);
    }

    //Session
    public void initializeSession() {

        Log.d(LOG_TAG, "ChatWrapper session init started");

        if (session != null) {
            return;
        }

        String sessionId = CacheManager.getInstance().getChatSessionId();
        String token = CacheManager.getInstance().getChatToken();

        session = new Session.Builder(ApplicationManager.getContext(), Constants.API_KEY, sessionId).build();
        session.setSessionListener(this);
        session.setSignalListener(this);
        session.setConnectionListener(this);
        session.connect(token);

    }

    protected void pauseSession() {
        if (session != null) {
            session.onPause();
        }
    }


    protected void resumeSession() {
        if (session != null) {
            session.onResume();
        }
    }

    public void setListener(OnChatMessageReceivedListener listener) {
        this.onChatMessageReceivedListener = new WeakReference<>(listener);
    }

    public void sendChatMessage(ChatWrapper chatWrapper) {
        if (session == null) {
            return;
        }
        Gson gson = new Gson();
        String json = gson.toJson(chatWrapper);

        session.sendSignal(SIGNAL_TYPE_CHAT, json);

    }

    public void sendMessage(ChatMessage chatMessage) {


        if (session == null) {
            return;
        }
        Gson gson = new Gson();
        String json = gson.toJson(chatMessage);

        session.sendSignal(SIGNAL_TYPE_CHAT, json);

    }


    public void sendCommentWrapper(ForumCommentWrapper forumCommentWrapper) {
        if (session == null) {
            return;
        }
        Gson gson = new Gson();
        String json = gson.toJson(forumCommentWrapper);

        session.sendSignal(SIGNAL_TYPE_COMMENT, json);
    }

    public void sendComment(ForumComment forumComment) {
        if (session == null) {
            return;
        }
        Gson gson = new Gson();
        String json = gson.toJson(forumComment);

        session.sendSignal(SIGNAL_TYPE_COMMENT, json);
    }

    public void sendLiveNotification(LiveNotifications chatMessage) {

        if (session == null) {
            return;
        }
        Gson gson = new Gson();
        String json = gson.toJson(chatMessage);

        session.sendSignal(SIGNAL_TYPE_NOTIFICATION, json);

    }

    private void messageReceived(ChatMessage messageData) {
        if (messageData == null)
            return;
        String userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        if (!messageData.isReceiver(userLoginId)) {
            return;
        }

        if (onChatMessageReceivedListener == null || onChatMessageReceivedListener.get() == null) {
            return;
        }


        onChatMessageReceivedListener.get().onMessageReceived(messageData);

    }

    private void messageReceived(ChatWrapper chatWrapper) {

        String userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        if (!chatWrapper.getChatMessage().isReceiver(userLoginId)) {
            return;
        }

        if (onChatMessageReceivedListener == null || onChatMessageReceivedListener.get() == null) {
            return;
        }


        onChatMessageReceivedListener.get().onMessageReceived(chatWrapper);

    }

    private void notificationReceived(LiveNotifications liveNotifications) {

        String userLoginId = CacheManager.getInstance().getCurrentUser().getUserId();
        if (!liveNotifications.getProfileId().equals(userLoginId)) {
            return;
        }

        if (onChatMessageReceivedListener == null || onChatMessageReceivedListener.get() == null) {
            return;
        }

        onChatMessageReceivedListener.get().onNotificationReceived(liveNotifications);

    }


    private Runnable checkTimerThread = new Runnable() {
        public boolean shutdown = false;

        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

            if (!(timeInMilliseconds < callAcceptenceTime)) {
                if (dialog != null) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                }
                if (ringtone.isPlaying())
                    ringtone.stop();

                customHandler.removeCallbacks(checkTimerThread);
                return;
            }

            customHandler.postDelayed(this, 1000);
        }

        public void shutdown() {
            shutdown = true;
        }

    };

    public void sendCallSignal(CallData data) {
        if (session == null) {
            return;
        }


        Gson gson = new Gson();
        String json = gson.toJson(data);


        session.sendSignal(SIGNAL_TYPE_CALL, json);


    }

    public void sendCallEndSignal(CallSignal data) {

        if (session == null) {
            return;
        }


        Gson gson = new Gson();
        String json = gson.toJson(data);


        session.sendSignal(CALL_END, json);


    }

    public void sendCallCancelSignal(CallSignal data) {

        if (session == null) {
            return;
        }


        Gson gson = new Gson();
        String json = gson.toJson(data);


        session.sendSignal(SIGNAL_TYPE_CALL_CANCEL, json);


    }

    public void sendCallSignal(CallSignal data) {

        Constants.callStartTime = 0;
        if (session == null) {
            return;
        }


        Gson gson = new Gson();
        String json = gson.toJson(data);

        SharedData.getInstance().setSelectedCallSignal(data);

        session.sendSignal(SIGNAL_TYPE_CALL, json);


    }

    public void sendCallMissedSignal(CallSignal data) {
        if (session == null) {
            return;
        }


        Gson gson = new Gson();
        String json = gson.toJson(data);

        SharedData.getInstance().setSelectedCallSignal(data);

        session.sendSignal(Constants.CALL_MISSED, json);


    }

    public void sendCallRejectedSignal(CallSignal data) {

        if (session == null) {
            return;
        }
        data.setStatus(CALL_ACCEPTED_NO);

        String reciever = data.getLoginId();

        data.setRecieverLoginId(reciever);
        data.setLoginId(CacheManager.getInstance().getCurrentUser().getLoginId());
        data.setMsg(CacheManager.getInstance().getCurrentUser().getFullName());

        SharedData.getInstance().setSelectedCallSignal(data);

        Gson gson = new Gson();
        String json = gson.toJson(data);


        session.sendSignal(SIGNAL_TYPE_CALL_ACCEPTED, json);


    }


    public void sendCallAcceptSignal(CallSignal data) {

        SharedData.getInstance().setCallResumed(true);
        if (session == null) {
            return;
        }
        data.setStatus(CALL_ACCEPTED_YES);
        Gson gson = new Gson();
        String json = gson.toJson(data);
        session.sendSignal(SIGNAL_TYPE_CALL_ACCEPTED, json);


    }

    public void sendCallRejectedSignal(CallData data) {

        if (session == null) {
            return;
        }

        Gson gson = new Gson();
        String json = gson.toJson(data);


        session.sendSignal(SIGNAL_TYPE_CALL_REJECTED, json);


    }

    public void sendPushNotification(CallData data) {
        WebServicesHandler.instance.notifiyUserForCall(data.getReceiverId(),
                CacheManager.getInstance().getCurrentUser().getFullName() + " was calling you ",
                new CustomCallback<RetrofitJSONResponse>() {
                    @Override
                    public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                    }

                    @Override
                    public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

                    }
                });
    }


    private void callReceived(final CallSignal data) {


        String userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        if (data == null)
            return;
        if (data.getLoginId().equals(userLoginId)) {
            return;
        }
        SharedData.getInstance().setSelectedCallSignal(data);
        if (NotificationUtils.isAppIsInBackground(ApplicationManager.getContext())) {
            Context mContext = ApplicationManager.getContext();
            NotificationUtils notificationUtils = new NotificationUtils(ApplicationManager.getContext());
            notificationUtils.showNotificationMessageForCall("You have an incoming call from ", data.getMsg()
                    , new SimpleDateFormat("MMMM").format(new Date()), null);


            return;
        }


        SharedData.getInstance().setCallFeedback(true);
        // SharedData.getInstance().setCallData(data);
        Activity activity = ((ApplicationManager) ApplicationManager.getContext()).getCurrentActivity();

        if (activity != null)
            if (activity.getClass() == SimpleFragmentActivity.class) {
                if (((SimpleFragmentActivity) activity).getCurrentFragment().getName().equals("CallFragment"))
                    return;
            }

        showCallPopup(data, activity);
    }


    public void showCallPopup(final CallSignal data, final Activity context) {
        startTime = SystemClock.uptimeMillis();
        customHandler = new Handler();
        customHandler.postDelayed(checkTimerThread, 1000);

        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        ringtone = RingtoneManager.getRingtone(context, notification);
        ringtone.play();

        if (dialog != null) {
            if (dialog.isShowing())
                return;
        }

        dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.incoimg_call_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.dialog_msg);
        text.setText(data.getMsg() + "\n is calling you...");
        TextView tv_title = (TextView) dialog.findViewById(R.id.dialog_title);
        tv_title.setText("Incoming Call");


        Button rejectButton = (Button) dialog.findViewById(R.id.btn_reject);
        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ringtone.isPlaying())
                    ringtone.stop();
                dialog.dismiss();
                sendCallRejectedSignal(data);

            }
        });
        Button acceptButton = (Button) dialog.findViewById(R.id.btn_accept);
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ringtone.isPlaying())
                    ringtone.stop();

                dialog.dismiss();

                //     SharedData.getInstance().setOpenTokCallSessionId(data.getCallSessionId());
                //     SharedData.getInstance().setOpenTokCallToken(data.getCallToken());

                checkPermissionsAndSendSignal(context, data);

            }
        });

        Button acceptVideoButton = (Button) dialog.findViewById(R.id.btn_accept_video);
        acceptVideoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ringtone.isPlaying())
                    ringtone.stop();

                dialog.dismiss();
                /*sendCallAcceptSignal(data);

                //     SharedData.getInstance().setOpenTokCallSessionId(data.getCallSessionId());
                //     SharedData.getInstance().setOpenTokCallToken(data.getCallToken());


                Activity activity = ((ApplicationManager) ApplicationManager.getContext()).getCurrentActivity();


                Intent intent = new Intent(activity, CallActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CallFragment.ordinal());
                ((BaseActivity) activity).startActivity(intent, true);
*/


                checkPermissionsAndSendSignal(context, data);
            }
        });


        if (data.getCallType().equals("video")) {
            acceptVideoButton.setVisibility(View.VISIBLE);
            acceptButton.setVisibility(View.GONE);
        } else {
            acceptVideoButton.setVisibility(View.GONE);
            acceptButton.setVisibility(View.VISIBLE);
        }
        dialog.show();
    }

    private void checkPermissionsAndSendSignal(Context context, CallSignal data) {
        Activity activity = ((ApplicationManager) ApplicationManager.getContext()).getCurrentActivity();
        String[] permissions = {Manifest.permission.INTERNET, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};
        if (EasyPermissions.hasPermissions(context, permissions)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.System.canWrite(context)) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(activity)) {
                        Intent intent = new Intent(activity, CallActivity.class);
                        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CallFragment.ordinal());
                        ((BaseActivity) activity).startActivity(intent, true);

                        sendCallAcceptSignal(data);

                    } else {
                        SharedData.getInstance().setAskingPermissionsOnAcceptingCall(true);
                        startDrawOverOtherAppsActivity(activity);
                    }


                } else {
                    SharedData.getInstance().setAskingPermissionsOnAcceptingCall(true);
                    Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + activity.getPackageName()));
                    activity.startActivityForResult(intent, WRITE_SETTINGS_PERM);
                }
            }
        } else {
            SharedData.getInstance().setAskingPermissionsOnAcceptingCall(true);
            ActivityCompat.requestPermissions(activity, permissions, VIDEO_APP_PERM);

        }

    }

    private void startDrawOverOtherAppsActivity(Activity activity) {
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + activity.getPackageName()));
        activity.startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION);
    }

    public Session getSession() {
        return session;
    }

    /* Session Listener methods */
    @Override
    public void onConnected(Session session) {
        Log.i(LOG_TAG, "Session Connected");


    }


    public void distroySession() {
        if (session != null) {
            session.disconnect();
            session = null;
        }
    }

    @Override
    public void onDisconnected(Session session) {
        Log.i(LOG_TAG, "Session Disconnected");
    }

    @Override
    public void onStreamReceived(Session session, Stream stream) {
        Log.i(LOG_TAG, "Stream Received");
    }

    @Override
    public void onStreamDropped(Session session, Stream stream) {
        Log.i(LOG_TAG, "Stream Dropped");
    }

    @Override
    public void onError(Session session, OpentokError opentokError) {
        Log.e(LOG_TAG, "opentok error" + opentokError.getMessage());
    }

    @Override
    public void onConnectionCreated(Session session, Connection connection) {
        // New client connected to the session
        String loginId = connection.getData();
        changeUserStatus(loginId, true);
    }

    @Override
    public void onConnectionDestroyed(Session session, Connection connection) {
        // A client disconnected from the session
        String loginId = connection.getData();
        changeUserStatus(loginId, false);
    }


    /* Signal Listener methods */
    @Override
    public void onSignalReceived(Session session, String type, String data, Connection connection) {

        boolean remote = !connection.equals(session.getConnection());
        if (!remote || type == null || TextUtils.isEmpty(data)) {
            return;
        }

        /*if (type.equals(SIGNAL_TYPE_NOTIFICATION)) {

//            Gson gson = new Gson();
//            LiveNotifications liveMsg = gson.fromJson(data, LiveNotifications.class);
//            notificationReceived(liveMsg);

        } else*/
        /*if (type.equals(SIGNAL_TYPE_CHAT)) {

            try {
                Gson gson = new Gson();
                ChatWrapper chatWrapper;
                ChatMessage chatMessage = gson.fromJson(data, ChatMessage.class);
                if (chatMessage == null || (chatMessage.getMsg() == null && chatMessage.getChatId() == null && chatMessage.getLoginId() == null)) {
                    chatWrapper = gson.fromJson(data, ChatWrapper.class);
                    messageReceived(chatWrapper);
                } else {
                    messageReceived(chatMessage);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

        } else if (type.equals(Constants.CALL_MISSED)) {

            Gson gson = new Gson();
            CallSignal callSignal = gson.fromJson(data, CallSignal.class);
            if (!callSignal.getLoginId().equals(CacheManager.getInstance().getCurrentUser().getLoginId())) {
                new NotificationUtils(ApplicationManager.getContext()).showNotificationMessageForMessage("Missed Call", callSignal.getMsg(), new Date().toString(), null);
            }

        } else if (type.equals(SIGNAL_TYPE_COMMENT)) {
            Gson gson = new Gson();
            ForumCommentWrapper forumCommentWrapper = gson.fromJson(data, ForumCommentWrapper.class);
            if (onChatMessageReceivedListener != null && onChatMessageReceivedListener.get() != null)
                onChatMessageReceivedListener.get().onCommentReceived(forumCommentWrapper);
        } */
        if (type.equals(SIGNAL_TYPE_CHAT)) {

            Gson gson = new Gson();
            ChatMessage chatMessage = null;
            try {
                chatMessage = gson.fromJson(data, ChatMessage.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                String error = e.toString();
            }
            messageReceived(chatMessage);

        } else if (type.equals(Constants.CALL_MISSED)) {

            Gson gson = new Gson();
            CallSignal callSignal = gson.fromJson(data, CallSignal.class);
            if (!callSignal.getLoginId().equals(CacheManager.getInstance().getCurrentUser().getLoginId())) {
                new NotificationUtils(ApplicationManager.getContext()).showNotificationMessageForMessage("Missed Call", callSignal.getMsg(), new Date().toString(), null);
            }

        } else if (type.equals(SIGNAL_TYPE_COMMENT)) {
            Gson gson = new Gson();
            ForumComment forumComment = gson.fromJson(data, ForumComment.class);
            if (onChatMessageReceivedListener != null && onChatMessageReceivedListener.get() != null)
                onChatMessageReceivedListener.get().onCommentReceived(forumComment);
        } else if (type.equals(SIGNAL_TYPE_CALL)) {

            Gson gson = new Gson();
            CallSignal callData = gson.fromJson(data, CallSignal.class);
            callData.setConnectionId(connection.getConnectionId());
            if (!callData.getRecieverLoginId().equals(CacheManager.getInstance().getCurrentUser().getLoginId()))
                return;
            if(callData != null)
                SharedData.getInstance().setReceiverName(callData.getMsg());
            callReceived(callData);
        } else if (type.equals(SIGNAL_TYPE_CALL_CANCEL)) {
            Gson gson = new Gson();
            CallSignal callData = gson.fromJson(data, CallSignal.class);


            if (callData != null && callData.getRecieverLoginId() != null && callData.getRecieverLoginId().equals(CacheManager.getInstance().getCurrentUser().getLoginId()))
                callCancelled(callData);
            else
                return;

        } else if (type.equals(SIGNAL_TYPE_CALL_ACCEPTED)) {

            Gson gson = new Gson();
            CallSignal callData = gson.fromJson(data, CallSignal.class);
            if (callData.getRecieverLoginId().equals(CacheManager.getInstance().getCurrentUser().getLoginId()))
                callAcceptanceStatus(callData);
            else
                return;


        } else if (type.equals(CALL_END)) {

            Gson gson = new Gson();
            CallSignal callData = gson.fromJson(data, CallSignal.class);
            if (callData.getRecieverLoginId().equals(CacheManager.getInstance().getCurrentUser().getLoginId()))
                callAcceptanceStatus(callData);
            else
                return;

        }

    }

    private void callCancelled(CallSignal callData) {
        String userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        if (callData == null)
            return;
        if (callData.getRecieverLoginId().equals(userLoginId)) {

            if (dialog != null)
                if (dialog.isShowing())
                    dialog.dismiss();
            if (ringtone != null)
                if (ringtone.isPlaying())
                    ringtone.stop();


            if (callStatusListenerWeakReference == null || callStatusListenerWeakReference.get() == null) {
                return;
            }
            callStatusListenerWeakReference.get().callCancelListener();

        } else {
            return;
        }
    }

    private void callAcceptanceStatus(CallSignal callData) {

        String userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        if (!callData.getLoginId().equals(userLoginId)) {

            if (callData.getStatus().equals(CALL_ACCEPTED_NO)) {
                if (callStatusListenerWeakReference == null || callStatusListenerWeakReference.get() == null) {
                    return;
                }
                callStatusListenerWeakReference.get().callRejectListener();
            } else if (callData.getStatus().equals(CALL_ACCEPTED_YES)) {
                if (dialog != null)
                    if (dialog.isShowing())
                        dialog.dismiss();
            }

            //AlertUtils.dismisDialog();
            //((Activity)ApplicationManager.getContext()).finish();

            // AlertUtils.showAlertForBack(ApplicationManager.getContext(), Constants.ErrorAlertTitle,"Call has been rejected");
        } else {
            return;
        }
    }

    @Override
    public void onReconnecting(Session session) {

    }

    @Override
    public void onReconnected(Session session) {

    }

    public void changeUserStatus(String loginId, Boolean isOnline) {
        WebServicesHandler.instance.changeUserStatus(loginId, isOnline, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                AlertUtils.dismissProgress();
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
            }
        });

    }
}
