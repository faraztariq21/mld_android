package com.imedhealthus.imeddoctors.common.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.CustomPharmacySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.PharmacyMedicationListAdapter;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.Pharmacy;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.User;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.patient.models.Prescription;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SendToPharmacyFragment extends BaseFragment implements OnMapReadyCallback, AdapterView.OnItemSelectedListener {


    @BindView(R.id.iv_doc_profile)
    ImageView ivDocProfile;
    @BindView(R.id.tv_doc_name)
    TextView tvDocName;
    @BindView(R.id.tv_patient_name)
    TextView tvPatientName;

    @BindView(R.id.sp_pharmacy)
    Spinner spPharmacy;
    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;
    @BindView(R.id.rv_medications)
    RecyclerView rvMedications;


    public List<Pharmacy> pharmacies = new ArrayList<>();
    private PharmacyMedicationListAdapter adapterMedications;
    private Prescription prescription;
    protected GoogleMap googleMap;

    @Override
    public boolean showHeader() {
        return true;
    }

    @Override
    public String getName() {
        return "SendToPharmacyFragment";
    }

    @Override
    public String getTitle() {
        return "Send To Pharmacy";
    }


    public SendToPharmacyFragment() {
        prescription = SharedData.getInstance().getSelectPrescriptionForPharmacy();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_send_to_pharmacey, container, false);
        ButterKnife.bind(this,view);

        initHelper();;
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
        loadPharmacies();
    }

    protected void initHelper() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rvMedications.setLayoutManager(layoutManager);

        adapterMedications = new PharmacyMedicationListAdapter(tvNoRecords);
        rvMedications.setAdapter(adapterMedications);

        spPharmacy.setOnItemSelectedListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private void setData() {

        if (prescription == null) {
            return;
        }

        tvDocName.setText(prescription.getDoctorName());
        tvPatientName.setText(prescription.getPatientName());

        Glide.with(context).load(prescription.getDoctorProfileImageUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivDocProfile);
        adapterMedications.setMedications(prescription.getPrescribedMedications());

    }


    //Actions
    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.btn_send_to_pharmacy:
                if(validateFields()) {
                    sendToPharmacy();
                }
                break;
        }
    }

    private boolean validateFields() {

        String errorMsg = null;
        if (spPharmacy.getSelectedItemPosition() == 0) {
            errorMsg = "Please select a pharmacy to continue";
        }else if(adapterMedications.getSelectedMedications().size() == 0){
            errorMsg = "Please select some medications to continue";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(context, Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }



    //Map
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }


    //Pharmacy spinner
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

        if(position == 0) {
            return;
        }

        googleMap.clear();

        Pharmacy pharmacy = (Pharmacy)adapterView.getSelectedItem();
        if(pharmacy == null) {
            return;
        }
        LatLng latLng = new LatLng(pharmacy.getLat(),pharmacy.getLng());
        googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("Pharmacy Location")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_green)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    //Web services
    public void loadPharmacies(){

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.getPharmacies(new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();
                if(!response.status()){
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, response.message());
                    return;
                }

                pharmacies = Pharmacy.parsePharmacies(response.getJSONArray("Pharmacy"));
                CustomPharmacySpinnerAdapter dataAdapter = new CustomPharmacySpinnerAdapter(getContext(), R.layout.custom_spinner_item, pharmacies);
                spPharmacy.setAdapter(dataAdapter);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(context, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

    private void sendToPharmacy() {

        AlertUtils.showProgress((Activity) context);

        Prescription updatedPrescription = new Prescription();

        updatedPrescription.setId(prescription.getId());

        updatedPrescription.setPatientId(prescription.getPatientId());
        updatedPrescription.setPatientName(prescription.getPatientName());

        updatedPrescription.setDoctorId(prescription.getDoctorId());
        updatedPrescription.setDoctorName(prescription.getDoctorName());
        updatedPrescription.setDoctorProfileImageUrl(prescription.getDoctorProfileImageUrl());

        User user = CacheManager.getInstance().getCurrentUser();
        updatedPrescription.setCreatedBy(user.getUserId());
        updatedPrescription.setSentRequest(user.getUserType()==Constants.UserType.Patient?"patient":"doctor");
        Pharmacy pharmacy =(Pharmacy)spPharmacy.getSelectedItem();
        if(pharmacy != null) {
            updatedPrescription.setPharmacyId(pharmacy.getId());
        }

        updatedPrescription.setPrescribedMedications(adapterMedications.getSelectedMedications());


        WebServicesHandler.instance.sendToPharmacy(updatedPrescription, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                if(!response.status()) {
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, response.message());
                    return;
                }

                SharedData.getInstance().setRefreshRequired(true);
                AlertUtils.showAlertForBack(context, Constants.SuccessAlertTitle, "Prescription sent to pharmacy");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(context, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
