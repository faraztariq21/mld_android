package com.imedhealthus.imeddoctors.common.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.widget.Toast;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Created by umair on 1/5/18.
 */

public class IntentUtils {

    public static boolean isAppRunning(final Context context, final String packageName) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        if (procInfos != null)
        {
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                if (processInfo.processName.equals(packageName)) {
                    return true;
                }
            }
        }
        return false;
    }
    public static void makeCall(Context context, String phone) {

        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phone));
        if (canOpenIntent(context, intent)) {
            context.startActivity(intent);
        }

    }

    public static void openUrl(Context context, String url) {
        openUrl(context, url, false);
    }

    public static void openUrl(Context context, String url, boolean check) {
        if (!url.startsWith("https://") && !url.startsWith("http://")) {
            url = "https://" + url;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url.toLowerCase()));
        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
        if (!check || canOpenIntent(context, intent)) {
            context.startActivity(intent);
        }
    }

    private static Boolean canOpenIntent(Context context, Intent intent) {
        PackageManager packageManager = context.getPackageManager();
        if (intent.resolveActivity(packageManager) != null) {
            return true;
        } else {
            Toast.makeText(context, "Can't open. Invalid Url.", Toast.LENGTH_LONG).show();
            return false;
        }
    }

}
