package com.imedhealthus.imeddoctors.common.listeners;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by umair on 1/6/18.
 */

public interface LocationLoaderListener {

    void locationReceived(LatLng latLng);
}
