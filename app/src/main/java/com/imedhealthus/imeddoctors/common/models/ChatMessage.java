package com.imedhealthus.imeddoctors.common.models;

import com.google.gson.annotations.SerializedName;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.common.utils.SharedPrefsHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by hamid on 12/9/2017.
 */

public class ChatMessage implements Serializable {

    private String loginId, chatId;
    @SerializedName("recieverLoginId")
    private String receiverLoginId;
    private String msg;
    private long timeStamp;
    @SerializedName("FileName")
    private String imageUrl;
    @SerializedName("FileType")
    private String fileType;
    @SerializedName("FileSize")
    private long fileSize;
    @SerializedName("senderName")
    private String senderName;

    private String imageURI = null;
    String filePathLocal;

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderName() {
        return senderName;
    }

    private String timeAgo;
    private Boolean showAsSent;


    public ChatMessage(ChatMessage chatMessage, ChatItem chatItem) {
        this.loginId
                = chatMessage.getLoginId();
        this.chatId = chatMessage.getChatId();
        ChatUserModel receiver = chatItem.getPartner(loginId);

        receiverLoginId = receiver.getId();

        this.msg = chatMessage.getMsg();
        this.imageUrl = chatMessage.getImageUrl();
        if (this.imageUrl != null && this.imageUrl.equals("null"))
            this.imageUrl = null;
        this.fileType = chatMessage.getFileType();
        if (this.fileType != null && this.fileType.equals("null"))
            this.fileType = null;

/*
            try {
                this.fileSize = jsonObject.getLong("FileSize");

            } catch (JSONException e) {
                e.printStackTrace();
            }
*/

        timeStamp = chatMessage.getTimeStamp();

    }

    public ChatMessage(JSONObject jsonObject, ChatItem chatItem) {

        loginId = jsonObject.optString("LoginId");
        ChatUserModel sender = chatItem.getChatUser(loginId);

        chatId = chatItem.getChatId();

        ChatUserModel receiver = chatItem.getPartner(loginId);
        receiverLoginId = receiver.getId();


        this.msg = jsonObject.optString("Message");
        this.imageUrl = jsonObject.optString("FileName");
        if (this.imageUrl.equals("null"))
            this.imageUrl = null;
        this.fileType = jsonObject.optString("FileType");
        if (this.fileType.equals("null"))
            this.fileType = null;

        String filePath = SharedPrefsHelper.getFileLocalAddress(ApplicationManager.getContext(), this);
        if (filePath != null /*&& FileUtils.checkFileExists(Uri.parse(filePath))*/)
            this.setFilePathLocal(filePath);

/*
            try {
                this.fileSize = jsonObject.getLong("FileSize");

            } catch (JSONException e) {
                e.printStackTrace();
            }
*/

        timeStamp = GenericUtils.getTimeDateInLong(jsonObject.optString("CreatedDate"));

    }


    public String getFilePathLocal() {
        return filePathLocal;
    }

    public void setFilePathLocal(String filePathLocal) {
        this.filePathLocal = filePathLocal;
    }

    public String getImageURI() {
        return imageURI;
    }

    public void setImageURI(String imageURI) {
        this.imageURI = imageURI;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ChatMessage(String loginId, String receiverLoginId, String msg, String chatId, String imageUrl, String imageURI) {
        this.loginId = loginId;
        this.receiverLoginId = receiverLoginId;

        this.msg = msg;
        this.chatId = chatId;
        this.timeStamp = new Date().getTime();
        this.imageUrl = imageUrl;
        this.imageURI = imageURI;
    }

    public static List<ChatMessage> parseChatMessages(JSONArray arrJson, ChatItem item) {

        ArrayList<ChatMessage> messages = new ArrayList<>();

        for (int i = 0; i < arrJson.length(); i++) {

            JSONObject itemJson = arrJson.optJSONObject(i);
            if (itemJson != null) {
                ChatMessage message = new ChatMessage(itemJson, item);
                messages.add(message);
            }

        }

        Collections.sort(messages, new Comparator<ChatMessage>() {
            @Override
            public int compare(ChatMessage message1, ChatMessage message2) {
                return message1.timeStamp < message2.timeStamp ? -1 : message1.timeStamp > message2.timeStamp ? 1 : 0;
            }
        });

        return messages;

    }

    public Boolean showAsSent() {
        if (showAsSent == null) {
            showAsSent = CacheManager.getInstance().getCurrentUser().getLoginId().equals(loginId);
        }
        return showAsSent;
    }

    public String getTimeAgo() {
        if (timeAgo == null) {
            timeAgo = GenericUtils.getPassedTime(timeStamp);
        }
        return timeAgo;
    }

    public String getReceiverLoginId() {
        return receiverLoginId;
    }


    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public boolean isReceiver(String userLoginId) {
        if (receiverLoginId == null)
            return false;
        return receiverLoginId.equals(userLoginId);
    }

    public boolean isSameChat(String chatId) {
        return this.chatId.equals(chatId);
    }

    public String getMsg() {
        return msg;
    }


    public String getLoginId() {
        return loginId;
    }

    public String getChatId() {
        return chatId;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }
}