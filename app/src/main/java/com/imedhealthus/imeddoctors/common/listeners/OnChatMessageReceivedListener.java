package com.imedhealthus.imeddoctors.common.listeners;

import com.imedhealthus.imeddoctors.common.models.ChatMessage;
import com.imedhealthus.imeddoctors.common.models.ChatWrapper;
import com.imedhealthus.imeddoctors.common.models.ForumComment;
import com.imedhealthus.imeddoctors.common.models.LiveNotifications;

/**
 * Created by Malik Hamid on 2/12/2018.
 */

public interface OnChatMessageReceivedListener {
    void onMessageReceived(ChatMessage chatMessage);

    void onMessageReceived(ChatWrapper chatWrapper);

    void onNotificationReceived(LiveNotifications liveNotifications);

    void onCommentReceived(ForumComment forumComment);
}
