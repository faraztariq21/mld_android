package com.imedhealthus.imeddoctors.common.models;

import com.google.gson.annotations.SerializedName;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.doctor.models.Appointment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Malik Hamid on 1/9/2018.
 */

public class BookedAppointment extends Appointment implements Serializable{

    private String doctorId, doctorFullName, patientId, patientFullName,doctorLoginId,patientLoginId,patientProfile,doctorProfile;
    private String reason, details,id, mrNumber;

    public static   ArrayList<BookedAppointment> getBookedAppointments(JSONObject jsonObject){
        ArrayList<BookedAppointment> bookedAppointments = new ArrayList<>();
        try {
            JSONArray jsonArray = jsonObject.getJSONArray("BookedAppointments");
            for(int i=0;i<jsonArray.length();i++){
                BookedAppointment bookedAppointment =new BookedAppointment(jsonArray.getJSONObject(i));
                bookedAppointments.add(bookedAppointment);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return bookedAppointments;
    }

    public BookedAppointment() {
        super();
    }


    public BookedAppointment(JSONObject jsonObject) {

        try {
            id =jsonObject.optString("BookedAppointmentId","");
            mrNumber =jsonObject.optString("MRNumber","");
            appointmentId=jsonObject.getInt("AppointmentId");
            startTimeStamp = GenericUtils.getTimeDateInLong(jsonObject.getString("StartTimeStamp"));
            endTimeStamp = GenericUtils.getTimeDateInLong(jsonObject.getString("EndTimeStamp"));
            doctorLoginId = jsonObject.getString("DoctorLoginId");
            patientLoginId = jsonObject.getString("PatientLoginId");
            type = ((jsonObject.getString("Type").equalsIgnoreCase("Tele"))?Constants.AppointmentType.Tele:Constants.AppointmentType.OFFICE);
            physicalLocation = jsonObject.getString("PhysicalLocation");
            doctorId = jsonObject.getString("DoctorId");
            doctorFullName = jsonObject.getString("DoctorName");
            patientId = jsonObject.getString("PatientId");
            patientFullName = jsonObject.getString("PatientName");
            patientProfile = Constants.URLS.BaseApis+jsonObject.optString("PatientProfilePic");
            doctorProfile = Constants.URLS.BaseApis+jsonObject.optString("DoctorProfilePic");
            reason = jsonObject.getString("Description").replace("null","Reason Not Provided");

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String getPatientProfile() {
        return patientProfile;
    }

    public void setPatientProfile(String patientProfile) {
        this.patientProfile = patientProfile;
    }

    public String getDoctorProfile() {
        return doctorProfile;
    }

    public void setDoctorProfile(String doctorProfile) {
        this.doctorProfile = doctorProfile;
    }

    public String getDoctorLoginId() {
        return doctorLoginId;
    }

    public void setDoctorLoginId(String doctorLoginId) {
        this.doctorLoginId = doctorLoginId;
    }

    public String getPatientLoginId() {
        return patientLoginId;
    }

    public void setPatientLoginId(String patientLoginId) {
        this.patientLoginId = patientLoginId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMrNumber() {
        return mrNumber;
    }

    public void setMrNumber(String mrNumber) {
        this.mrNumber = mrNumber;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorFullName() {
        return doctorFullName;
    }

    public void setDoctorFullName(String doctorFullName) {
        this.doctorFullName = doctorFullName;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPatientFullName() {
        return patientFullName;
    }

    public void setPatientFullName(String patientFullName) {
        this.patientFullName = patientFullName;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
