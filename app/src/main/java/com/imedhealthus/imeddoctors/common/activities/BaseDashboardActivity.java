package com.imedhealthus.imeddoctors.common.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.adapters.MenuListAdapter;
import com.imedhealthus.imeddoctors.common.listeners.OnMenuItemClickListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.MenuItem;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.User;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by umair on 1/7/18.
 */

public abstract class BaseDashboardActivity extends BaseFragmentActivity implements OnMenuItemClickListener {

    @BindView(R.id.iv_nav_home)
    ImageView ivNavHome;
    @BindView(R.id.tv_nav_home)
    TextView tvNavHome;
    @BindView(R.id.tv_resume_call)
    TextView tvResumeCall;

    @BindView(R.id.iv_nav_notification)
    ImageView ivNavNotification;
    @BindView(R.id.tv_nav_notification)
    TextView tvNavNotification;

    @BindView(R.id.iv_nav_appointment)
    ImageView ivNavAppointment;
    @BindView(R.id.tv_nav_appointment)
    TextView tvNavAppointment;

    @BindView(R.id.iv_nav_profile)
    ImageView ivNavProfile;
    @BindView(R.id.tv_nav_profile)
    TextView tvNavProfile;

    @BindView(R.id.layout_drawer)
    DrawerLayout layoutDrawer;

    @BindView(R.id.iv_menu_profile)
    ImageView ivMenuProfile;
    @BindView(R.id.tv_menu_name)
    TextView tvMenuName;
    @BindView(R.id.tv_menu_email)
    TextView tvMenuEmail;
    @BindView(R.id.rv_menu)
    RecyclerView rvMenu;

    User user;
    private MenuListAdapter adapterMenu;
    private boolean doubleBackToExitPressedOnce = false;


    protected void initMenu() {

        user = CacheManager.getInstance().getCurrentUser();
        if (layoutDrawer == null) {
            return;
        }

        tvResumeCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResumeCall.setVisibility(View.GONE);
                SharedData.getInstance().setCallActive(false);
                SharedData.getInstance().setCallResumed(true);
                Intent intent = new Intent(BaseDashboardActivity.this, CallActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CallFragment.ordinal());
                ((BaseActivity) BaseDashboardActivity.this).startActivity(intent, true);
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvMenu.setLayoutManager(layoutManager);

        adapterMenu = new MenuListAdapter(createMenuItems(), this);
        rvMenu.setAdapter(adapterMenu);
        setMenuUserData();

    }

    protected void setMenuUserData() {


        tvMenuName.setText(user.getFullName());
        tvMenuEmail.setText(user.getEmail());
        Glide.with(ApplicationManager.getContext()).load(user.getImageUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivMenuProfile);

    }

    private List<MenuItem> createMenuItems() {

        List<MenuItem> menuItems = new ArrayList<>();

        if (user.getUserType() == Constants.UserType.Patient) {
            menuItems.add(new MenuItem(MenuItem.OPTION.HOME));
            menuItems.add(new MenuItem(MenuItem.OPTION.FORUM));
            menuItems.add(new MenuItem(MenuItem.OPTION.INBOX));
            menuItems.add(new MenuItem(MenuItem.OPTION.QUESTIONS));
            menuItems.add(new MenuItem(MenuItem.OPTION.CHANGE_PASSWORD));
            menuItems.add(new MenuItem(MenuItem.OPTION.FEEDBACK));
            menuItems.add(new MenuItem(MenuItem.OPTION.ABOUT_US));
            menuItems.add(new MenuItem(MenuItem.OPTION.LOGOUT));

        } else {
            menuItems.add(new MenuItem(MenuItem.OPTION.HOME));
            menuItems.add(new MenuItem(MenuItem.OPTION.FORUM));
            menuItems.add(new MenuItem(MenuItem.OPTION.INBOX));
            menuItems.add(new MenuItem(MenuItem.OPTION.QUESTIONS));
            menuItems.add(new MenuItem(MenuItem.OPTION.REFERRAL));
            menuItems.add(new MenuItem(MenuItem.OPTION.CHANGE_PASSWORD));
            menuItems.add(new MenuItem(MenuItem.OPTION.FEEDBACK));
            menuItems.add(new MenuItem(MenuItem.OPTION.ABOUT_US));
            menuItems.add(new MenuItem(MenuItem.OPTION.ARCHIVED_APPOINTMENTS));
            menuItems.add(new MenuItem(MenuItem.OPTION.LOGOUT));

        }

        return menuItems;

    }

    //Fragment handling
    @Override
    public void changeFragment(Constants.Fragment fragment, Bundle arguments) {

        super.changeFragment(fragment, arguments);

        switch (fragment) {
            case PatientHome:
            case DoctorHome:
                adjustNavBar(ivNavHome);
                break;

            case PatientProfile:
            case DoctorProfile:
                adjustNavBar(ivNavProfile);
                break;

            case Appointments:
                adjustNavBar(ivNavAppointment);
                break;

            case Notifications:
                adjustNavBar(ivNavNotification);
                break;

            default:
                adjustNavBar(null);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (SharedData.getInstance().isCallActive()) {
            if (SharedData.getInstance().isCanDrawOverOtherApps())
                tvResumeCall.setVisibility(View.GONE);
            else
                tvResumeCall.setVisibility(View.VISIBLE);
        } else {
            tvResumeCall.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {

        if (currentOverlayFragment != null) {
            super.onBackPressed();
            return;
        }

        Constants.UserType userType = CacheManager.getInstance().getCurrentUser().getUserType();
        if (userType == Constants.UserType.Doctor) {
            if (currentFragment.getName().equals("DoctorHomeFragment")) {
                GenericUtils.exitApp(this);
                return;
            } else {
                changeFragment(Constants.Fragment.DoctorHome, null);
            }
        } else {
            if (currentFragment.getName().equals("PatientHomeFragment")) {
                GenericUtils.exitApp(this);
                return;
            } else {
                changeFragment(Constants.Fragment.PatientHome, null);
            }
        }


/*
        if (doubleBackToExitPressedOnce) {
            GenericUtils.exitApp(this);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
*/
    }

    @Override
    protected void toggleMenu() {

        if (layoutDrawer == null) {
            return;
        }

        if (layoutDrawer.isDrawerOpen(Gravity.END)) {
            layoutDrawer.closeDrawer(Gravity.END);
        } else {
            layoutDrawer.openDrawer(Gravity.END);
        }

    }

    @Override
    public void onMenuItemClick(int idx) {

        MenuItem menuItem = adapterMenu.getItem(idx);
        if (menuItem == null) {
            return;
        }

        if (layoutDrawer != null) {
            layoutDrawer.closeDrawer(Gravity.END);
        }

        Constants.Fragment fragment = menuItem.getFragment();
        if (fragment == Constants.Fragment.None) {
            return;
        }

        adapterMenu.setSelectedIdx(idx);
        changeFragment(fragment, null);

    }

    //Nav bar
    private void adjustNavBar(ImageView ivSelected) {

        int normalColor = getResources().getColor(R.color.icon_green);
        int selectedColor = getResources().getColor(R.color.dark_gray);

        if (ivSelected == ivNavHome) {
            ivNavHome.setImageResource(R.drawable.ic_home_light_blue);
            ivNavHome.setColorFilter(selectedColor);
            tvNavHome.setTextColor(selectedColor);
        } else {
            ivNavHome.setImageResource(R.drawable.ic_home_dark_gray);
            ivNavHome.setColorFilter(normalColor);
            tvNavHome.setTextColor(normalColor);
        }

        if (ivSelected == ivNavNotification) {
            ivNavNotification.setImageResource(R.drawable.ic_notification);
            ivNavNotification.setColorFilter(selectedColor);
            tvNavNotification.setTextColor(selectedColor);
        } else {
            ivNavNotification.setImageResource(R.drawable.ic_notification);
            ivNavNotification.setColorFilter(normalColor);
            tvNavNotification.setTextColor(normalColor);
        }

        if (ivSelected == ivNavAppointment) {
            ivNavAppointment.setImageResource(R.drawable.ic_appointment_light_blue);
            ivNavAppointment.setColorFilter(selectedColor);
            tvNavAppointment.setTextColor(selectedColor);
        } else {
            ivNavAppointment.setImageResource(R.drawable.ic_appointment_dark_gray);
            ivNavAppointment.setColorFilter(normalColor);
            tvNavAppointment.setTextColor(normalColor);
        }

        if (ivSelected == ivNavProfile) {
            ivNavProfile.setImageResource(R.drawable.ic_profile_light_blue);
            ivNavProfile.setColorFilter(selectedColor);
            tvNavProfile.setTextColor(selectedColor);
        } else {
            ivNavProfile.setImageResource(R.drawable.ic_profile_dark_gray);
            ivNavProfile.setColorFilter(normalColor);
            tvNavProfile.setTextColor(normalColor);
        }

    }

}
