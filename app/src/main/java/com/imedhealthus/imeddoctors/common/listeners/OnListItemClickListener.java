package com.imedhealthus.imeddoctors.common.listeners;

public interface OnListItemClickListener {

    public void onListItemClicked(int index);
}
