package com.imedhealthus.imeddoctors.common.activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.auth.activities.LandingActivity;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.activities.DoctorDashboardActivity;
import com.imedhealthus.imeddoctors.patient.activities.PatientDashboardActivity;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by umair on 1/7/18.
 */

public class SimpleFragmentActivity extends BaseFragmentActivity {


    public static final String BUNDLE = "Bundle";
    /**
     * This variable tells us that whether this activity was opened from
     * {@link com.imedhealthus.imeddoctors.doctor.fragments.AddAppointmentSlotsFragment}
     * and when back is pressed it has to setResult
     */


    boolean isFromAppointmenntSlotsFragment;
    Bundle bundle;


    //    @BindView(R.id.tv_resume_call)
//    TextView tvResumeCall;

    public static void startActivity(Activity activity, Constants.Fragment fragment, Bundle bundle) {
        Intent intent = new Intent(activity, SimpleFragmentActivity.class);

        intent.putExtra(Constants.Keys.FragmentId, fragment.ordinal());
        intent.putExtra(BUNDLE, bundle);
        ((BaseActivity) activity).startActivity(intent, true);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_fragment);

        ButterKnife.bind(this);

        isFromAppointmenntSlotsFragment = getIntent().getBooleanExtra(Constants.IS_APPOINTMENT_SLOT_FRAGMENT, false);
        bundle = getIntent().getParcelableExtra(BUNDLE);
        Constants.Fragment fragment = Constants.Fragment.fromInt(getIntent().getIntExtra(Constants.Keys.FragmentId, Constants.Fragment.SearchDoctor.ordinal()));

        changeFragment(fragment, bundle);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                //   if(isSingleActivity())
                onBackPressed();
                break;

            default:
                super.onClick(view);
        }
    }

    private void openDashboard() {

        SharedData.getInstance().setSelectedFilter(null);

        Intent intent;
        if (SharedData.getInstance().isPostQuestion()) {
            SharedData.getInstance().setPostQuestion(false);
            intent = new Intent(getApplicationContext(), SimpleFragmentActivity.class);
            intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.Questions.ordinal());
        } else if (CacheManager.getInstance().isUserLoggedIn()) {
            if (!isSingleActivity()) {
                super.onBackPressed();
                return;
            }
            if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Patient) {
                intent = new Intent(this, PatientDashboardActivity.class);
            } else {
                intent = new Intent(this, DoctorDashboardActivity.class);
            }

        } else {
            intent = new Intent(this, LandingActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent, true);

    }


    @Override
    public void onBackPressed() {

        if (isFromAppointmenntSlotsFragment) {
            setResult(RESULT_OK);
            finish();
        } else {
            if (currentFragment.getName().equals("CallFragment")) {
                SharedData.getInstance().setCallActive(true);
                SharedData.getInstance().setCallResumed(false);

            }
            openDashboard();
        }


    }


    public boolean isSingleActivity() {
        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);

        if (taskList.get(0).numActivities == 1 &&
                taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            Log.i("", "This is last activity in the stack");
            return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        BaseFragment fragment = currentFragment;
        fragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        BaseFragment fragment = currentFragment;
        fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }
}
