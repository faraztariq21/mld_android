package com.imedhealthus.imeddoctors.common.models;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.imedhealthus.imeddoctors.common.utils.PreferenceUtils;

/**
 * Created by umair on 1/2/18.
 */

public class CacheManager {

    private static final CacheManager ourInstance = new CacheManager();

    public static CacheManager getInstance() {
        return ourInstance;
    }

    private User currentUser;
    private String chatSessionId;
    private String chatToken;

    private CacheManager() {
        currentUser = loadUserFromPreferences();
    }

    public Boolean isUserLoggedIn() {
        return currentUser != null && !TextUtils.isEmpty(currentUser.getUserId());
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setLoggedInUser(@NonNull User user) {
        currentUser = user;
        saveUserToPreferences(currentUser);
    }

    public void logoutUser() {
        currentUser = new User();
        saveUserToPreferences(currentUser);
    }


    //User storage in preferences
    private User loadUserFromPreferences() {

        String json = PreferenceUtils.getValueForKey("user");
        if (TextUtils.isEmpty(json)) {
            return new User();
        }
        Gson gson = new Gson();
        return gson.fromJson(json, User.class);

    }

    private void saveUserToPreferences(User user) {

        Gson gson = new Gson();
        String json = gson.toJson(user);
        PreferenceUtils.setValueForKey(json, "user");

    }


    public String getChatSessionId() {
        return PreferenceUtils.getValueForKey("chatSessionId");
    }

    public void setChatSessionId(String chatSessionId) {
        PreferenceUtils.setValueForKey(chatSessionId, "chatSessionId");
    }

    public String getChatToken() {
        return PreferenceUtils.getValueForKey("chatTokenId");
    }

    public void setChatToken(String chatToken) {
        PreferenceUtils.setValueForKey(chatToken, "chatTokenId");
    }

}
