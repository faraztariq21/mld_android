package com.imedhealthus.imeddoctors.common.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.bigkoo.svprogresshud.SVProgressHUD;
import com.imedhealthus.imeddoctors.R;

/**
 * Created by umair on 1/5/18.
 */

public class AlertUtils {
    static Dialog dialog;

    public static void showAlert(Context context, String title, String message) {

        if (title.equals(Constants.ErrorAlertTitle)) {
            try {
                showErrorAlert((Activity) context, title, message);
            } catch (WindowManager.BadTokenException e) {
                e.printStackTrace();
            }
        } else {
            try {
                showSuccessAlert((Activity) context, title, message);
            } catch (WindowManager.BadTokenException e) {
                e.printStackTrace();
            }
        }


/*
        AlertDialog dialog = new AlertDialog.Builder(context, R.style.Dialog)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setCancelable(false).create();

        dialog.show();
*/
    }

    public static void showDeleteAlert(Context context, String title, String message) {

        AlertDialog dialog = new AlertDialog.Builder(context, R.style.Dialog)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setCancelable(false).create();

        dialog.show();

    }

    public static void showDeleteAlert(Context context, String title, String message, DialogInterface.OnClickListener onClickListener) {

        AlertDialog dialog = new AlertDialog.Builder(context, R.style.Dialog)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(android.R.string.yes, onClickListener).setCancelable(false).create();

        dialog.show();

    }

    public static AlertDialog showAlert(Context context, String title, String message, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onNegativeClickListener) {

        AlertDialog dialog = new AlertDialog.Builder(context, R.style.Dialog)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton(android.R.string.cancel, onNegativeClickListener)
                .setPositiveButton(android.R.string.yes, onClickListener).setCancelable(false).create();

        dialog.show();
        return dialog;

    }

    public static void dismisDialog() {
        try {
            if (dialog != null) {
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        } catch (Exception e) {

        }

    }

    public static void showAlertForBack(final Context context, String title, String message) {

        dismisDialog();
        if (context == null)
            return;
        if (title != null && !title.equals(Constants.ErrorAlertTitle)) {
            dialog = new Dialog(context);
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.success_dialog);

            TextView text = (TextView) dialog.findViewById(R.id.dialog_msg);
            text.setText(message);
            TextView tv_title = (TextView) dialog.findViewById(R.id.dialog_title);
            tv_title.setText(title);


            Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    ((Activity) context).onBackPressed();
                }
            });

            dialog.show();
        } else {

            dialog = new Dialog(context);
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.error_dialog);

            TextView text = (TextView) dialog.findViewById(R.id.dialog_msg);
            text.setText(message);
            TextView tv_title = (TextView) dialog.findViewById(R.id.dialog_title);
            tv_title.setText(title);


            Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    ((Activity) context).onBackPressed();
                }
            });

            dialog.show();
        }



        /*
        AlertDialog dialog = new AlertDialog.Builder(context, R.style.Dialog)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ((Activity)context).onBackPressed();
                    }
                })
                .setCancelable(false)
                .create();

        dialog.show();
*/
    }

    public static void showErrorAlert(Activity activity, String Title, String msg) throws WindowManager.BadTokenException {

        dismisDialog();

        if (activity == null)
            return;
        dialog = new Dialog(activity);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.error_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.dialog_msg);
        text.setText(msg);
        TextView title = (TextView) dialog.findViewById(R.id.dialog_title);
        title.setText(Title);


        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public static void showSuccessAlert(Activity activity, String Title, String msg) throws WindowManager.BadTokenException {


        dismisDialog();

        dialog = new Dialog(activity);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.success_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.dialog_msg);
        text.setText(msg);
        TextView title = (TextView) dialog.findViewById(R.id.dialog_title);
        title.setText(Title);


        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }


    static SVProgressHUD progressHUD;

    public static void showProgress(Activity activity) {

        showProgress(activity, "Processing...");
    }

    public static void showProgress(Activity activity, String status) {
        dismissProgress();
        progressHUD = new SVProgressHUD(activity);


        progressHUD.getProgressBar().setCircleColor(R.color.light_blue);
        progressHUD.getProgressBar().setCircleProgressColor(R.color.light_blue);
        progressHUD.getProgressBar().setMinimumHeight(100);
        progressHUD.getProgressBar().setMinimumWidth(100);


        progressHUD.showWithStatus(status, SVProgressHUD.SVProgressHUDMaskType.Black);
        //progressHUD.show();
    }

    public static void dismissProgress() {
        if (progressHUD == null || !progressHUD.isShowing()) {
            return;
        }
        progressHUD.dismissImmediately();
    }

    public static boolean isShowingProgress() {
        return progressHUD != null && progressHUD.isShowing();
    }

}

