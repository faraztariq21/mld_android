package com.imedhealthus.imeddoctors.common.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Malik Hamid on 1/12/2018.
 */

public class PaymentMethod {

    private String methodId, title, details;
    private String cardNumber, cardExpiryMonth, cardExpiryYear, cardCVC;

    public PaymentMethod() {
    }
    public PaymentMethod(JSONObject jsonObject) {
        try {
            methodId = jsonObject.getString("MethodId");
            title = jsonObject.getString("Title");
            details = jsonObject.getString("Detail");
            cardNumber = jsonObject.getString("CardNumber");
            cardExpiryMonth = jsonObject.getString("CardExpiryMonth");
            cardExpiryYear = jsonObject.getString("CardExpiryYear");
            cardCVC = jsonObject.getString("CardCVC");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public PaymentMethod(String methodId, String title, String details, String cardNumber, String cardExpiryMonth, String cardExpiryYear, String cardCVC) {
        this.methodId = methodId;
        this.title = title;
        this.details = details;
        this.cardNumber = cardNumber;
        this.cardExpiryMonth = cardExpiryMonth;
        this.cardExpiryYear = cardExpiryYear;
        this.cardCVC = cardCVC;
    }

    public String getMethodId() {
        return methodId;
    }

    public void setMethodId(String methodId) {
        this.methodId = methodId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardExpiryMonth() {
        return cardExpiryMonth;
    }

    public void setCardExpiryMonth(String cardExpiryMonth) {
        this.cardExpiryMonth = cardExpiryMonth;
    }

    public String getCardExpiryYear() {
        return cardExpiryYear;
    }

    public void setCardExpiryYear(String cardExpiryYear) {
        this.cardExpiryYear = cardExpiryYear;
    }

    public String getCardCVC() {
        return cardCVC;
    }

    public void setCardCVC(String cardCVC) {
        this.cardCVC = cardCVC;
    }
}
