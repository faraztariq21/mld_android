package com.imedhealthus.imeddoctors.common.fragments;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.patient.adapters.PrescribedMedicationListAdapter;
import com.imedhealthus.imeddoctors.patient.listeners.OnMedicationItemClickListener;
import com.imedhealthus.imeddoctors.patient.models.PrescribedMedication;
import com.imedhealthus.imeddoctors.patient.models.Prescription;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PrescriptionDetailFragment extends BaseFragment implements OnMedicationItemClickListener {

    @BindView(R.id.iv_doc_profile)
    ImageView ivDocProfile;
    @BindView(R.id.tv_doc_name)
    TextView tvDocName;
    @BindView(R.id.tv_patient_name)
    TextView tvPatientName;

//    @BindView(R.id.et_notes)
//    RichEditor etNotes;

//    @BindView(R.id.tv_note)
//    TextView tvNotes;

    @BindView(R.id.rv_medications)
    RecyclerView rvMedications;
    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;

    @BindView(R.id.cont_buttons)
    ViewGroup contButtons;
    @BindView(R.id.cont_send_to_pharmacy)
    ViewGroup contSendToButton;
    private PrescribedMedicationListAdapter adapterMedications;
    FragmentRichEditor fragmentRichEditor;
    private Prescription prescription;
    private List<PrescribedMedication> updatedMedications = new ArrayList<>();

    @Override
    public String getName() {
        return "PrescriptionDetailFragment";
    }

    @Override
    public String getTitle() {
        return "Prescription Details";
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_prescription_detail, container, false);
        ButterKnife.bind(this, rootView);
        //GenericUtils.setEditTextScrollable(, getActivity());

        initHelper();
        return rootView;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentRichEditor = FragmentRichEditor.newInstance(null);
        getChildFragmentManager().beginTransaction().replace(R.id.ll_rich_editor_container, fragmentRichEditor).commitAllowingStateLoss();
        getChildFragmentManager().executePendingTransactions();
    }

    private void initHelper() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rvMedications.setLayoutManager(layoutManager);

        boolean isEditAllowed = CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor
                && CacheManager.getInstance().getCurrentUser().getUserId().equals(SharedData.getInstance().getSelectedPrescription().getDoctorId());//If same doctor

        adapterMedications = new PrescribedMedicationListAdapter(tvNoRecords, isEditAllowed, this);
        rvMedications.setAdapter(adapterMedications);

        if (isEditAllowed) {
            contButtons.setVisibility(View.VISIBLE);
//            etNotes.setVisibility(View.VISIBLE);
//            tvNotes.setVisibility(View.GONE);
        } else {
            contButtons.setVisibility(View.GONE);
//            etNotes.setVisibility(View.GONE);
//            tvNotes.setVisibility(View.VISIBLE);
        }

        //fragmentRichEditor.hideKeyboard();
        //GenericUtils.hideKeyboard(context, etNotes);


    }

    private void setData() {

        prescription = SharedData.getInstance().getSelectedPrescription();
        if (prescription == null) {
            return;
        }


        tvDocName.setText(prescription.getDoctorName());
        tvPatientName.setText(prescription.getPatientName());

        if (prescription.getNotes() != null)
            fragmentRichEditor.setEditorText(prescription.getNotes());
//        etNotes.setText(prescription.getNotes());
//        tvNotes.setText(prescription.getNotes());
        Picasso.with(context).load(prescription.getDoctorProfileImageUrl()).placeholder(R.drawable.ic_profile_placeholder).into(ivDocProfile);
        updatedMedications.addAll(prescription.getPrescribedMedications());
        adapterMedications.setMedications(updatedMedications);


        if (prescription.getId() == null) {
            return;
        }
        if (prescription.getPrescribedMedications().size() == 0)
            getTreatMentPlanById();

        contSendToButton.setVisibility(View.VISIBLE);


    }

    private void getTreatMentPlanById() {
        AlertUtils.showProgress((Activity) context);

        WebServicesHandler.instance.getTreatmentPlanDetail(prescription.getId(), prescription.getPatientId(), new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                if (!response.status()) {
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, response.message());
                } else {
                    JSONObject prescriptionsJson = response.optJSONObject("TreatmentPlan");
                    if (prescriptionsJson != null) {
                        prescription = new Prescription(prescriptionsJson);
                        updatedMedications.clear();
                        updatedMedications.addAll(prescription.getPrescribedMedications());
                        adapterMedications.setMedications(updatedMedications);
                    }
                }


            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

                AlertUtils.dismissProgress();
                AlertUtils.showAlert(context, Constants.SuccessAlertTitle, Constants.GenericErrorMsg);


            }

        });

    }


    //Actions
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_send_to_pharmacy:
                openSendToPharmacy();
                break;

            case R.id.btn_save:
                if (validateData()) {
                    savePrescription();
                }
                break;

            case R.id.btn_add_medication:
                openAddMedication();
                break;
        }

    }

    public boolean validateData() {

        String errorMsg = null;
        if (fragmentRichEditor.getEditorText().isEmpty()) {
            errorMsg = "Kindly enter some notes";
        } /*else if (updatedMedications.size() == 0) {
            errorMsg = "Kindly add at least one medication";
        }*/

        if (errorMsg != null) {
            AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    private void openAddMedication() {

        SharedData.getInstance().setSelectedPrescribedMedication(null);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditPrescribedMedication.ordinal());
        ((BaseActivity) context).startActivity(intent, true);

    }

    private void openSendToPharmacy() {
        Prescription updatedPrescription = new Prescription();

        updatedPrescription.setId(prescription.getId());

        updatedPrescription.setPatientId(prescription.getPatientId());
        updatedPrescription.setPatientName(prescription.getPatientName());

        updatedPrescription.setDoctorId(prescription.getDoctorId());
        updatedPrescription.setDoctorName(prescription.getDoctorName());
        updatedPrescription.setDoctorProfileImageUrl(prescription.getDoctorProfileImageUrl());

        updatedPrescription.setPrescribedMedications(updatedMedications);
        updatedPrescription.setNotes(fragmentRichEditor.getEditorText());

        SharedData.getInstance().setSelectPrescriptionForPharmacy(updatedPrescription);
        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.SendToPharmacy.ordinal());
        ((BaseActivity) context).startActivity(intent, true);
    }


    @Override
    public void onResume() {
        super.onResume();
        updatedMedications.clear();
        setData();
        handleAddEditMedication();
    }

    private void handleAddEditMedication() {

        PrescribedMedication medication = SharedData.getInstance().getSelectedPrescribedMedication();
        if (medication == null || prescription == null) {
            return;
        }

        if (!updatedMedications.contains(medication)) {//Add
            updatedMedications.add(medication);
        }
        adapterMedications.setMedications(updatedMedications);
        SharedData.getInstance().setSelectedPrescribedMedication(null);

    }


    //Medication item click
    @Override
    public void onMedicationDetailClick(int idx) {

    }

    @Override
    public void onMedicationEditClick(int idx) {

        PrescribedMedication medication = adapterMedications.getItem(idx);
        if (medication == null) {
            return;
        }

        SharedData.getInstance().setSelectedPrescribedMedication(medication);

        Intent intent = new Intent(getContext(), SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditPrescribedMedication.ordinal());
        ((BaseActivity) context).startActivity(intent, true);

    }

    @Override
    public void onMedicationDeleteClick(int idx) {

        PrescribedMedication medication = adapterMedications.getItem(idx);
        if (medication == null) {
            return;
        }
        showDeleteAlert(medication);

    }


    private void showDeleteAlert(final PrescribedMedication medication) {

        new AlertDialog.Builder(context)
                .setTitle("Confirm")
                .setMessage("Are you sure you want to delete this medication?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        deleteMedication(medication);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        dialog.dismiss();

                    }
                })
                .show();

    }

    private void deleteMedication(PrescribedMedication medication) {

        updatedMedications.remove(medication);
        adapterMedications.setMedications(updatedMedications);

    }


    //Webservices
    private void savePrescription() {

        AlertUtils.showProgress((Activity) context);

        Prescription updatedPrescription = new Prescription();

        updatedPrescription.setId(prescription.getId());
        updatedPrescription.setPatientId(prescription.getPatientId());
        updatedPrescription.setPatientName(prescription.getPatientName());
        updatedPrescription.setDoctorId(prescription.getDoctorId());
        updatedPrescription.setDoctorName(prescription.getDoctorName());
        updatedPrescription.setDoctorProfileImageUrl(prescription.getDoctorProfileImageUrl());
        updatedPrescription.setPrescribedMedications(updatedMedications);
        updatedPrescription.setNotes(fragmentRichEditor.getEditorText());

        WebServicesHandler.instance.addEditPrescription(updatedPrescription, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                if (!response.status()) {
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, response.message());
                    return;
                }

                SharedData.getInstance().setRefreshRequired(true);
                AlertUtils.showAlertForBack(context, Constants.SuccessAlertTitle, "Prescription saved successfully");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(context, Constants.SuccessAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

}
