package com.imedhealthus.imeddoctors.common.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler;
import com.imedhealthus.imeddoctors.common.listeners.OnChatMessageReceivedListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.ChatMessage;
import com.imedhealthus.imeddoctors.common.models.ChatWrapper;
import com.imedhealthus.imeddoctors.common.models.ForumComment;
import com.imedhealthus.imeddoctors.common.models.ForumCommentWrapper;
import com.imedhealthus.imeddoctors.common.models.ForumQuestion;
import com.imedhealthus.imeddoctors.common.models.LiveNotifications;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.services.CallService;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.NotificationUtils;
import com.imedhealthus.imeddoctors.common.utils.ScreenSharingCapturer;
import com.imedhealthus.imeddoctors.common.utils.TinyDB;
import com.imedhealthus.imeddoctors.patient.activities.PatientDashboardActivity;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by umair on 1/5/18.
 */

public abstract class BaseFragment extends Fragment implements OnChatMessageReceivedListener {

    protected Context context;
    private Dialog dialog;

    static boolean openTokSessionStarted = false;

    Bundle bundle;


    ShimmerFrameLayout shimmerFrameLayout;

    public void initializeShimmer(View view) {
        shimmerFrameLayout = view.findViewById(R.id.shimmer_view_container);
    }


    public void stopShimmerAnimation() {
        if (shimmerFrameLayout != null) {
            shimmerFrameLayout.stopShimmerAnimation();
            shimmerFrameLayout.setVisibility(View.GONE);
        } else {
            throw new NullPointerException("Shimmer not initialized. Initialize the shimmer in child fragment by calling initializeShimmer.");
        }
    }

    public void startShimmerAnimation() {
        if (shimmerFrameLayout != null) {
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmerAnimation();
        } else {
            throw new NullPointerException("Shimmer not initialized. Initialize the shimmer in child fragment by calling initializeShimmer.");
        }
    }


    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        this.context = context;


    }


    @Override
    public void onDetach() {

        super.onDetach();
        this.context = null;

    }

    public boolean showHeader() {
        return false;
    }

    public void onClick(View view) {

    }

    public void onDismissOverlayFragment(Object data) {

    }

    public abstract String getName();

    public abstract String getTitle();

    @Override
    public void onResume() {
        super.onResume();
        //if (!openTokSessionStarted)
        if (CallService.screenSharingCapturer == null)
            CallService.screenSharingCapturer = new ScreenSharingCapturer(getActivity(), getView());


        if (CallService.screenSharingCapturer != null)
            CallService.screenSharingCapturer.setContentView(getView());
        changeUserStatus();


    }


    @Override
    public void onMessageReceived(ChatWrapper chatWrapper) {
        if (!CacheManager.getInstance().isUserLoggedIn()) {
            return;
        }

        if (chatWrapper == null || chatWrapper.getChatMessage() == null)
            return;

        if (!chatWrapper.getChatMessage().getReceiverLoginId().equals(CacheManager.getInstance().getCurrentUser().getLoginId())) {
            return;
        }

        NotificationUtils notificationUtils = new NotificationUtils(ApplicationManager.getContext());
        notificationUtils.showNotificationMessageForMessage(chatWrapper
                , new SimpleDateFormat("MMMM").format(new Date()), new Intent());

        Activity activity = ((ApplicationManager) ApplicationManager.getContext()).getCurrentActivity();

        if (activity != null)
            if (activity.getClass() == SimpleFragmentActivity.class) {
                if (((SimpleFragmentActivity) activity).getCurrentFragment().getName().equals("ChatFragment"))
                    return;

            }


    }

    @Override
    public void onMessageReceived(ChatMessage chatMessage) {
        //
        if (!CacheManager.getInstance().isUserLoggedIn()) {
            return;
        }
        if (!chatMessage.getReceiverLoginId().equals(CacheManager.getInstance().getCurrentUser().getLoginId())) {
            return;
        }

        NotificationUtils notificationUtils = new NotificationUtils(ApplicationManager.getContext());
        notificationUtils.showNotificationMessageForMessage("You have a Message", chatMessage.getMsg()
                , new SimpleDateFormat("MMMM").format(new Date()), new Intent());

        Activity activity = ((ApplicationManager) ApplicationManager.getContext()).getCurrentActivity();

        if (activity != null)
            if (activity.getClass() == SimpleFragmentActivity.class) {
                if (((SimpleFragmentActivity) activity).getCurrentFragment().getName().equals("ChatFragment"))
                    return;

            }

        new TinyDB(getActivity()).putBoolean(chatMessage.getChatId() + Constants.hasUnreadMessages, true);

    }

    @Override
    public void onCommentReceived(ForumComment forumComment) {

        //region code to use when @ForumCommentWrapper is used
/*
        ForumComment forumComment = null;
        ForumQuestion forumQuestion = null;
        if (forumCommentWrapper != null) {
            forumComment = forumCommentWrapper.getForumComment();
            forumQuestion = forumCommentWrapper.getForumQuestion();
        }

        if (forumComment != null)
            Log.e("forum Comment", forumComment.toString());
        if (forumQuestion != null) {
            Log.e("forum question", forumQuestion.toString());
        }
        String msg = "", title;
        title = "You have received a comment";
        if (forumComment != null) {
            msg = forumComment.getComment();

        }


           */
/* if (!NotificationUtils.isAppIsInBackground(ApplicationManager.getContext())) {
                ApplicationManager.getContext().sendBroadcast(intent);
                return;
            }*//*

        NotificationUtils notificationUtils = new NotificationUtils(context);
        notificationUtils.showNotificationMessage(context, title, msg
                , new SimpleDateFormat("MMMM").format(new Date()), new Intent(), Constants.NOTIFICATION_TYPE_COMMENT, new Gson().toJson(forumCommentWrapper, ForumCommentWrapper.class));
*/
        //endregion



    }

    @Override
    public void onNotificationReceived(LiveNotifications liveNotifications) {
        if (!CacheManager.getInstance().isUserLoggedIn()) {
            return;
        }
        String userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        String userId = CacheManager.getInstance().getCurrentUser().getUserId();


        if (liveNotifications.getProfileType().equals("doctor")) {

            if (!liveNotifications.getProfileId().equals(userId)) {
                return;
            }
            showNotificationPopup(liveNotifications, getActivity());

        } else if (liveNotifications.getProfileType().equals("'doctorLogin'")) {

            if (!liveNotifications.getProfileId().equals(userLoginId)) {
                return;
            }
            showNotificationPopup(liveNotifications, getActivity());

        } else if (liveNotifications.getProfileType().equals("'patientLogin'")) {

            if (!liveNotifications.getProfileId().equals(userLoginId)) {
                return;
            }
            showNotificationPopup(liveNotifications, getActivity());

        } else if (liveNotifications.getProfileType().equals("'patient'")) {

            if (!liveNotifications.getProfileId().equals(userId)) {
                return;
            }
            showNotificationPopup(liveNotifications, getActivity());

        }

    }


    public void showMessagePopup(final ChatMessage data, final Activity context) {

        if (dialog != null)
            if (dialog.isShowing())
                dialog.dismiss();

        if (dialog == null) {
            dialog = new Dialog(context);
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.new_message_dialog);
        }


        TextView text = (TextView) dialog.findViewById(R.id.dialog_msg);
        text.setText("You have received a message  \n " + data.getMsg());
        TextView tv_title = (TextView) dialog.findViewById(R.id.dialog_title);
        tv_title.setText("New Message ");


        Button rejectButton = (Button) dialog.findViewById(R.id.btn_reject);
        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();


            }
        });
        Button acceptButton = (Button) dialog.findViewById(R.id.btn_accept);
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();

                //     SharedData.getInstance().setOpenTokCallId(data.getCallSessionId());
                //     SharedData.getInstance().setOpenTokCallToken(data.getCallToken());


                Activity activity = ((ApplicationManager) ApplicationManager.getContext()).getCurrentActivity();


                Intent intent = new Intent(activity, SimpleFragmentActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.Inbox.ordinal());
                ((BaseActivity) activity).startActivity(intent, true);


            }
        });

        dialog.show();
    }

    public void showNotificationPopup(final LiveNotifications data, final Activity context) {

        /* if (NotificationUtils.isAppIsInBackground(ApplicationManager.getContext())) {
         */
        NotificationUtils notificationUtils = new NotificationUtils(context);
        notificationUtils.showNotificationMessage(getActivity(), "You have a Message", data.getMessage()
                , new SimpleDateFormat("MMMM").format(new Date()), new Intent(), Constants.NOTIFICATION_TYPE_COMMENT, "");
        Activity activity = ((ApplicationManager) ApplicationManager.getContext()).getCurrentActivity();

        if (activity != null) {
            if (activity.getClass() == SimpleFragmentActivity.class) {
                if (((SimpleFragmentActivity) activity).getCurrentFragment().getName().equals("CallFragment")
                        )
                    return;
            }
            if (activity.getClass() == PatientDashboardActivity.class) {
                if (((PatientDashboardActivity) activity).getCurrentFragment().getName().equals("PatientHomeFragment")
                        )
                    return;
            }
        }

         /*   return;
        }*/
      /*  Activity activity = ((ApplicationManager) ApplicationManager.getContext()).getCurrentActivity();

        if (activity != null)
            if (activity.getClass() == SimpleFragmentActivity.class) {
                if (((SimpleFragmentActivity) activity).getCurrentFragment().getName().equals("CallFragment")
                        )
                    return;
            }

        if (dialog != null)
            if (dialog.isShowing())
                dialog.dismiss();

        if (dialog == null) {
            dialog = new Dialog(context);
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.new_notification_dialog);
        }


        TextView text = (TextView) dialog.findViewById(R.id.dialog_msg);
        text.setText(data.getMessage());
        TextView tv_title = (TextView) dialog.findViewById(R.id.dialog_title);
        tv_title.setText("Alert ");


        Button rejectButton = (Button) dialog.findViewById(R.id.btn_reject);
        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();


            }
        });
        Button acceptButton = (Button) dialog.findViewById(R.id.btn_accept);
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();

                //     SharedData.getInstance().setOpenTokCallSessionId(data.getCallSessionId());
                //     SharedData.getInstance().setOpenTokCallToken(data.getCallToken());


                Activity activity = ((ApplicationManager) ApplicationManager.getContext()).getCurrentActivity();


                Intent intent = new Intent(activity, SimpleFragmentActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.Notifications.ordinal());
                ((BaseActivity) activity).startActivity(intent, true);


            }
        });

        dialog.show();*/
    }


    public void changeUserStatus() {

        String userLoginId = "0";
        if (!CacheManager.getInstance().isUserLoggedIn()) {
            return;
        }
        userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();

        WebServicesHandler.instance.changeUserStatus(userLoginId, true, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                if (response.status()) {


                    String session = response.optString("SessionId");
                    String token = response.optString("TokenId");
                    CacheManager.getInstance().setChatSessionId(session);
                    CacheManager.getInstance().setChatToken(token);
                    OpenTokHandler.getInstance().initializeSession();

                    OpenTokHandler.getInstance().setListener(BaseFragment.this);
                    openTokSessionStarted = true;
                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

                //showDeleteAlert(SplashActivity.this,"Problem!",Constants.GenericErrorMsg+" Would you like to retry?");
            }
        });

    }
}

