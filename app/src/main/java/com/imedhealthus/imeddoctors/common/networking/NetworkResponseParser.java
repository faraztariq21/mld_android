package com.imedhealthus.imeddoctors.common.networking;

import com.google.gson.Gson;
import com.imedhealthus.imeddoctors.common.models.ForumComment;
import com.imedhealthus.imeddoctors.common.models.ForumQuestion;
import com.imedhealthus.imeddoctors.common.models.Notification;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dev-iMEDHealth-X5 on 18/04/2018.
 */

public class NetworkResponseParser {

    public static List<ForumQuestion> getAllForums(JSONArray response) {
        Gson gson = new Gson();
        List<ForumQuestion> forumQuestions = new ArrayList<>();
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject object = response.getJSONObject(i);
                ForumQuestion forumQuestion = gson.fromJson(object.toString(), ForumQuestion.class);
                forumQuestion.setProfilePicUrl(Constants.URLS.BaseApis + forumQuestion.getProfilePicUrl());
                forumQuestions.add(forumQuestion);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return forumQuestions;
    }

    public static List<ForumComment> getPublicForumComments(JSONArray response) {
        Gson gson = new Gson();
        List<ForumComment> forumComments = new ArrayList<>();
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject object = response.getJSONObject(i);
                ForumComment forumComment = gson.fromJson(object.toString(), ForumComment.class);
                forumComment.setProfilePicUrl(Constants.URLS.BaseApis + forumComment.getProfilePicUrl());
                forumComments.add(forumComment);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return forumComments;
    }

    public static List<Notification> getNotifications(JSONArray response) {
        int appointmentsCount = 0;
//        int conversationsCount = 0;
        int notificationsCount = 0;
        int myQuestionsCount = 0;
        int forumCommentsCount = 0;
        int questionCommentsCount = 0;
        Gson gson = new Gson();
        List<Notification> notifications = new ArrayList<>();
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject object = response.getJSONObject(i);
                Notification notification = gson.fromJson(object.toString(), Notification.class);
                if (notification.isActive()) {
                    if (notification.getNotificationType().equalsIgnoreCase("Appointment")) {
                        appointmentsCount++;
                    } else if (notification.getNotificationType().equalsIgnoreCase("ChatWrapper")) {
//                        conversationsCount++;
                    } else if (notification.getNotificationType().equalsIgnoreCase("AdminRefferQuestionToDoctors")) {
                        myQuestionsCount++;
                    } else if (notification.getNotificationType().equalsIgnoreCase("DoctorForumComments") || notification.getNotificationType().equalsIgnoreCase("PatientForumComments")) {
                        if (notification.isPublic()) {
                            forumCommentsCount++;
                        } else {
                            questionCommentsCount++;
                        }
                    }
                }
                notifications.add(notification);
            }

            SharedData.getInstance().setUnSeenAppointments(appointmentsCount);
//            SharedData.getInstance().setUnSeenConversations(conversationsCount);
            SharedData.getInstance().setUnSeenQuestions(myQuestionsCount);
            SharedData.getInstance().setUnSeenForumComments(forumCommentsCount);
            SharedData.getInstance().setUnSeenQuestionComments(questionCommentsCount);
            SharedData.getInstance().setUnSeenNotifications(appointmentsCount + /*conversationsCount +*/ myQuestionsCount + forumCommentsCount + questionCommentsCount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return notifications;
    }
}
