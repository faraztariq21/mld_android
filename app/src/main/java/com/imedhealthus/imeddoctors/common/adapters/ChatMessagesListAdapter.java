package com.imedhealthus.imeddoctors.common.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.fragments.FragmentImage;
import com.imedhealthus.imeddoctors.common.listeners.OnListItemClickListener;
import com.imedhealthus.imeddoctors.common.models.ChatMessage;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.FileUtils;
import com.imedhealthus.imeddoctors.common.view_holders.ChatMessageViewHolder;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.BlurTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Created by hamid on 12/9/2017.
 */

public class ChatMessagesListAdapter extends RecyclerView.Adapter<ChatMessageViewHolder> {

    private List<ChatMessage> messages = new ArrayList<ChatMessage>();
    private WeakReference<TextView> tvNoRecords;
    Context context;
    OnListItemClickListener onListItemClickListener;
    android.os.Handler handler;

    public ChatMessagesListAdapter(Context context, TextView tvNoRecords, List<ChatMessage> chatMessages, OnListItemClickListener onListItemClickListener) {
        super();

        this.messages = chatMessages;
        this.tvNoRecords = new WeakReference<>(tvNoRecords);
        this.context = context;
        this.onListItemClickListener = onListItemClickListener;
        handler = new android.os.Handler();
    }

    public void setMessages(List<ChatMessage> messages) {

        this.messages = messages;
        /*notifyDataSetChanged();
        adjustView();
*/
    }

    public void addMessage(ChatMessage message) {

        messages.add(message);

        notifyDataSetChanged();
        adjustView();

    }

    public void addMessages(ArrayList<ChatMessage> chatMessages) {

        messages.addAll(chatMessages);

        notifyDataSetChanged();
        adjustView();

    }


    private void adjustView() {
        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        } else {
            tvNoRecords.get().setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!getItem(position).showAsSent()) {
            return R.layout.list_item_chat_send;
        }
        return R.layout.list_item_chat_recieve;
    }

    @Override
    public ChatMessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView;
        if (viewType == R.layout.list_item_chat_recieve) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_chat_recieve, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_chat_send, parent, false);
        }

        return new ChatMessageViewHolder(itemView);

    }


    public List<ChatMessage> getMessages() {
        return messages;
    }

    @Override
    public void onBindViewHolder(final ChatMessageViewHolder holder, final int position) {
        final ChatMessage chatMessage = getItem(position);

        final int pos = position;

        final ChatMessage chatMessage1 = messages.get(pos);


        if (chatMessage1.getImageURI() != null && !chatMessage1.getImageURI().equals("null")) {
            if (chatMessage1.getFileType() == Constants.FileType.FILE_TYPE_IMAGE) {
                holder.ivChatImage.setVisibility(View.VISIBLE);
                holder.squareLayout.setVisibility(View.VISIBLE);
                holder.ivChatImage.setImageURI(Uri.parse(chatMessage1.getImageURI()));
                //Glide.with(context).load(Uri.parse(chatMessage1.getImageURI())).into(holder.ivChatImage);

                holder.progressBar.setVisibility(View.GONE);
                holder.tvFileName.setVisibility(View.GONE);
            } else {
                holder.tvFileName.setVisibility(View.VISIBLE);
                holder.progressBar.setVisibility(View.GONE);
                try {
                    holder.tvFileName.setText(FileUtils.getFileNameFromUri(context, Uri.parse(chatMessage1.getImageURI())));
                } catch (SecurityException e) {
                    e.printStackTrace();
                    holder.tvFileName.setText(FileUtils.getFileNameFromURL(Constants.BASE_URL + chatMessage1.getImageUrl()));

                }
                hideImageView(holder);
                holder.tvFileName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onListItemClickListener.onListItemClicked(position);
                    }
                });

            }
        } else if (chatMessage1.
                getImageUrl() != null && !chatMessage1.getImageUrl().isEmpty() && !chatMessage1.getImageUrl().equals("null")) {
            if (chatMessage1.getFileType().equals(Constants.FileType.FILE_TYPE_IMAGE)) {
                holder.tvFileName.setVisibility(View.GONE);
                showImageView(holder);
                holder.progressBar.setVisibility(View.GONE);
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
                requestOptions.placeholder(R.drawable.ic_attachment_placeholder).error(R.drawable.ic_attachment_placeholder);


                Glide.with(ApplicationManager.getContext()).load(Constants.BASE_URL + chatMessage1.getImageUrl()).apply(requestOptions.bitmapTransform(new BlurTransformation(25, 3)))
                        .thumbnail(0.02f).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {


                        Runnable runnable = new Runnable() {
                            @Override
                            public void run() {

                                RequestOptions requestOptions1 = new RequestOptions();
                                requestOptions1.transform(new RoundedCornersTransformation(20, 0,RoundedCornersTransformation.CornerType.ALL));


                                Glide.with(ApplicationManager.getContext()).load(Constants.BASE_URL + chatMessage1.getImageUrl()).apply(requestOptions1).thumbnail(0.2f).listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                        holder.progressBar.setVisibility(View.GONE);
                                        /*Resources res = context.getResources();
                                        Bitmap src = ImageUtils.getRoundedCornerBitmap(resource, 20);
                                        holder.ivChatImage.setImageBitmap(src);*/
                                        return false;
                                    }
                                }).into(holder.ivChatImage);
                            }
                        };
                        if (dataSource.equals(DataSource.REMOTE)) {
                            handler.postDelayed(runnable, 1000);
                        } else {
                            handler.post(runnable);

                        }
                        //holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                }).apply(requestOptions).into(holder.ivChatImage);
            } else {
                holder.tvFileName.setVisibility(View.VISIBLE);
                holder.tvFileName.setText(FileUtils.getFileNameFromURL(Constants.BASE_URL + chatMessage1.getImageUrl()));
                hideImageView(holder);
                holder.tvFileName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onListItemClickListener.onListItemClicked(position);
                    }
                });
            }

        } else {
            hideImageView(holder);
            holder.tvFileName.setVisibility(View.GONE);


        }
        holder.setData(chatMessage);
        holder.ivChatImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentImage.newInstance(messages.get(position).getImageURI(), Constants.BASE_URL + messages.get(position).getImageUrl()).show(((AppCompatActivity) context).getSupportFragmentManager());
                /*SharedData.getInstance().setSelectedImageURL(messages.get(position).getImageUrl());
                SimpleFragmentActivity.startActivity((Activity) context, Constants.Fragment.FragmentImageFull);*/
            }
        });


        String fileName = null;
        if (chatMessage1.getImageURI() != null)
            fileName = chatMessage1.getImageURI();
        else
            fileName = chatMessage1.getImageUrl();
        try {
            if (fileName != null) {
                if (fileName.endsWith(".pdf"))
                    holder.tvFileName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_pdf, 0, 0, 0);
                if (fileName.endsWith(".pptx"))
                    holder.tvFileName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_ppt, 0, 0, 0);
                if (fileName.endsWith(".xls"))
                    holder.tvFileName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_xls, 0, 0, 0);
                if (fileName.endsWith(".txt"))
                    holder.tvFileName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_txt, 0, 0, 0);
                if (fileName.endsWith(".docx"))
                    holder.tvFileName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_doc, 0, 0, 0);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //holder.setIsRecyclable(false);
    }

    private void hideImageView(ChatMessageViewHolder holder) {
        holder.progressBar.setVisibility(View.GONE);
        holder.ivChatImage.setVisibility(View.GONE);
        holder.squareLayout.setVisibility(View.GONE);
    }

    private void showImageView(ChatMessageViewHolder holder) {
        holder.progressBar.setVisibility(View.VISIBLE);
        holder.ivChatImage.setVisibility(View.VISIBLE);
        holder.squareLayout.setVisibility(View.VISIBLE);
    }


    @Override
    public int getItemCount() {
        return messages == null ? 0 : messages.size();
    }

    public ChatMessage getItem(int index) {
        if (messages == null) {
            return null;
        }
        return messages.get(index);
    }
}
