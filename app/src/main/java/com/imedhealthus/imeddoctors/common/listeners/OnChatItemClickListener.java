package com.imedhealthus.imeddoctors.common.listeners;

/**
 * Created by Malik Hamid on 1/16/2018.
 */

public interface OnChatItemClickListener {
    void onChatItemClick(int idx);

    void onChatItemFileClic(int idx);
}
