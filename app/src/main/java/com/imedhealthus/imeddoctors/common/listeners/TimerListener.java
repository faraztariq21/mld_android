package com.imedhealthus.imeddoctors.common.listeners;

/**
 * Created by Shehryar Zaheer on 9/15/2018.
 */

public interface TimerListener {

    public void onTimeUpdated(long updatedSeconds);

}
