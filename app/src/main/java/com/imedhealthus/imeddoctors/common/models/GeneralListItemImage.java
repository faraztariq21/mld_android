package com.imedhealthus.imeddoctors.common.models;

import java.util.ArrayList;

public class GeneralListItemImage implements ListItemImage {
    String imagePath;
    boolean isImageUri;
    boolean isFilePath;

    public GeneralListItemImage(String imagePath, boolean isImageUri, boolean isFilePath) {
        this.imagePath = imagePath;
        this.isImageUri = isImageUri;
        this.isFilePath = isFilePath;
    }

    public static ArrayList<GeneralListItemImage> getGeneralListItemsFromStrings(ArrayList<String> imagePaths, boolean isFileUri, boolean isFilePath) {
        ArrayList<GeneralListItemImage> generalListItemImages = new ArrayList<>();
        for (String imagePath : imagePaths) {
            generalListItemImages.add(new GeneralListItemImage(imagePath, isFileUri, isFilePath));
        }
        return generalListItemImages;

    }

    @Override
    public String getImagePath() {
        return imagePath;
    }

    @Override
    public boolean isImageUri() {
        return isImageUri;
    }

    @Override
    public boolean isFilePath() {
        return isFilePath;
    }


    public static GeneralListItemImage getGeneralListItemImage(String path,boolean isImageUri,boolean isFilePath){
        GeneralListItemImage generalListItemImage  = new GeneralListItemImage(path,isImageUri,isFilePath);
        return generalListItemImage;
    }

}
