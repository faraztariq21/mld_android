package com.imedhealthus.imeddoctors.common.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.listeners.OnChatItemClickListener;
import com.imedhealthus.imeddoctors.common.models.ChatItem;
import com.imedhealthus.imeddoctors.common.view_holders.MessageItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Malik Hamid on 1/13/2018.
 */

public class ChatItemsListAdapter extends RecyclerView.Adapter<MessageItemViewHolder> {

    private List<ChatItem> chatItems;
    private WeakReference<TextView> tvNoRecords;
    private Activity activity;

    private WeakReference<OnChatItemClickListener> onChatItemClickListener;

    public ChatItemsListAdapter(Activity activity, TextView tvNoRecords, OnChatItemClickListener onChatItemClickListener) {

        super();
        this.activity = activity;
        this.tvNoRecords = new WeakReference<>(tvNoRecords);
        this.onChatItemClickListener = new WeakReference<>(onChatItemClickListener);

    }

    public void setChatItems(List<ChatItem> chatItems) {

        this.chatItems = chatItems;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        } else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }

    @Override
    public MessageItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_question, parent, false);
        Typeface typeface = Typeface.createFromAsset(activity.getAssets(), "FontAwesome.ttf");
        TextView tvForward = itemView.findViewById(R.id.ic_forward);
        tvForward.setTypeface(typeface);
        OnChatItemClickListener listener = null;
        if (onChatItemClickListener.get() != null) {
            listener = onChatItemClickListener.get();
        }
        return new MessageItemViewHolder(itemView, listener);

    }

    @Override
    public void onBindViewHolder(MessageItemViewHolder viewHolder, int position) {

        viewHolder.setData(getItem(position), position);
        viewHolder.setIsRecyclable(true);

    }

    @Override
    public int getItemCount() {
        return chatItems == null ? 0 : chatItems.size();
    }

    public ChatItem getItem(int position) {
        if (chatItems == null) {
            return null;
        }
        return chatItems.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}