package com.imedhealthus.imeddoctors.common.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.auth.activities.LandingActivity;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.adapters.NotificationAdapter;
import com.imedhealthus.imeddoctors.common.listeners.OnNotificationClickListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.Notification;
import com.imedhealthus.imeddoctors.common.models.User;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationFragment extends BaseFragment implements OnNotificationClickListener {

    @BindView(R.id.rv_notification)
    RecyclerView rvInvoices;
    NotificationAdapter notificationAdapter;
    @BindView(R.id.tv_no_data)
    TextView tvNoRecords;
    ArrayList<Notification> notifications = new ArrayList<>();

    public NotificationFragment() {

    }

    @Override
    public boolean showHeader() {
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this, view);

        initializeShimmer(view);

        initHelper();
        loadData();
        return view;
    }

    private void initHelper() {

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvInvoices.setLayoutManager(mLayoutManager);

        notificationAdapter = new NotificationAdapter(tvNoRecords, this);
        rvInvoices.setAdapter(notificationAdapter);
        notificationAdapter.setNotifications(notifications);


    }


    @Override
    public String getName() {
        return "notification";
    }

    @Override
    public String getTitle() {
        return "Notifications";
    }

    @Override
    public void onNotificationItemClick(int idx) {
        Notification notification = notificationAdapter.getItem(idx);
        if (notification == null)
            return;


        if (notification.getNotificationType().equalsIgnoreCase("chat")) {
            openInbox();
        } else if (notification.getNotificationType().toLowerCase().contains("appointment")) {
            openBookedAppointment();
        } else if (notification.getNotificationType().toLowerCase().equalsIgnoreCase("AdminRefferQuestionToDoctors")) {
            openQuestion();
        } else if (notification.getNotificationType().toLowerCase().equalsIgnoreCase("CaseReferrals")
                || notification.getNotificationType().toLowerCase().equalsIgnoreCase("CaseReferralsResponse")) {
            openReferral();
        } else if (notification.getNotificationType().toLowerCase().equalsIgnoreCase("DoctorForumComments")
                || notification.getNotificationType().toLowerCase().equalsIgnoreCase("PatientForumComments")) {
            if (notification.isPublic()) {
                openForum();
            } else {
                openQuestion();
            }

        } else {

        }


    }

    private void openForum() {
        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.Forum.ordinal());
        ((BaseActivity) context).startActivity(intent, true);
    }

    private void openQuestion() {
        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.Questions.ordinal());
        ((BaseActivity) context).startActivity(intent, true);
    }

    private void openReferral() {
        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.Referral.ordinal());
        ((BaseActivity) context).startActivity(intent, true);
    }

    private void openBookedAppointment() {
        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.Appointments.ordinal());
        ((BaseActivity) context).startActivity(intent, true);
    }

    private void openInbox() {
        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.Inbox.ordinal());
        ((BaseActivity) context).startActivity(intent, true);
    }


    @Override
    public void onNotificationItemlongClick(int idx) {

    }

    public void loadData() {
        //AlertUtils.showProgress(getActivity());
        startShimmerAnimation();
        User user = CacheManager.getInstance().getCurrentUser();

        if (user.getLoginId() == null || user.getLoginId().equalsIgnoreCase("")) {
            Intent intent = new Intent(getActivity(), LandingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            getActivity().startActivity(intent);
            return;
        }

        WebServicesHandler.instance.getNotifications(user, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                //AlertUtils.dismissProgress();
                stopShimmerAnimation();
                if (response.status()) {
                    notifications = Notification.getNotifications(response.getJSONArray("GetNotification"));
                    notificationAdapter.setNotifications(notifications);
                } else {
                    AlertUtils.dismissProgress();
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                //AlertUtils.dismissProgress();
                stopShimmerAnimation();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

}
