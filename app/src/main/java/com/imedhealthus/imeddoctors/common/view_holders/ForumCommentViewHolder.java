package com.imedhealthus.imeddoctors.common.view_holders;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.listeners.OnChatItemClickListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.ForumComment;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.FileUtils;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.BlurTransformation;

/**
 * Created by Malik Hamid on 1/16/2018.
 */

public class ForumCommentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    ConstraintLayout contItem;

    @BindView(R.id.iv_profile)
    ImageView ivProfile;
    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_message)
    TextView tvMessage;
    @BindView(R.id.iv_forum_image)
    public ImageView ivForumImage;

    @BindView(R.id.tv_file_name)
    public
    TextView tvFileName;
    private int idx;
    private WeakReference<OnChatItemClickListener> onChatItemClickListener;
    android.os.Handler handler;

    public ForumCommentViewHolder(android.os.Handler handler, View view, OnChatItemClickListener onChatItemClickListener) {

        super(view);
        ButterKnife.bind(this, view);
        this.handler = handler;

        this.onChatItemClickListener = new WeakReference<>(onChatItemClickListener);
        contItem.setOnClickListener(this);
        ivForumImage.setOnClickListener(this);
        tvMessage.setOnClickListener(this);
    }

    public void setData(final ForumComment forumComment, final int idx) {

        String userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
//        ChatUserModel partner = forumQuestion.getPartner(userLoginId);
//        if (partner == null) {
//            tvName.setText("");
//            ivProfile.setImageResource(0);
//        }else {
//            Picasso.with(ApplicationManager.getContext()).load(partner.getImgURL()).placeholder(R.drawable.ic_profile_placeholder).into(ivProfile);
//        }

        if (forumComment.getName() != null && !forumComment.getName().equalsIgnoreCase("")) {
            tvName.setText(forumComment.getName());
        } else {
            tvName.setText("Anonymous");
        }
        tvMessage.setText(forumComment.getComment());
        if (forumComment.getCreatedDate() != null && !forumComment.getCreatedDate().equalsIgnoreCase("")) {
            long timeStamp = GenericUtils.getTimeDateInLong(forumComment.getCreatedDate());
            String timeAgo = GenericUtils.getPassedTime(timeStamp);
            tvTime.setText(timeAgo);
        }
        if (forumComment.getProfilePicUrl() != null && !forumComment.getProfilePicUrl().equalsIgnoreCase("")) {
            Glide.with(ApplicationManager.getContext()).load(forumComment.getProfilePicUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivProfile);
        }

        if (forumComment.getImageURI() != null && !forumComment.getImageURI().equals("null")) {
            if (forumComment.getFileType() == Constants.FileType.FILE_TYPE_IMAGE) {
                ivForumImage.setVisibility(View.VISIBLE);

                ivForumImage.setImageURI(Uri.parse(forumComment.getImageURI()));
/*
                progressBar.setVisibility(View.GONE);
*/
                tvFileName.setVisibility(View.GONE);
            } else {
                tvFileName.setVisibility(View.VISIBLE);
                // progressBar.setVisibility(View.GONE);
                try {
                    tvFileName.setText(FileUtils.getFileNameFromUri(ApplicationManager.getContext(), Uri.parse(forumComment.getImageURI())));
                } catch (SecurityException e) {
                    e.printStackTrace();
                    tvFileName.setText(FileUtils.getFileNameFromURL(Constants.BASE_URL + forumComment.getFileUrl()));

                }
                ivForumImage.setVisibility(View.GONE);


            }
        } else if (forumComment.
                getFileUrl() != null && !forumComment.getFileUrl().isEmpty() && !forumComment.getFileUrl().equals("null")) {
            if (forumComment.getFileType().equals(Constants.FileType.FILE_TYPE_IMAGE)) {
                tvFileName.setVisibility(View.GONE);
                ivForumImage.setVisibility(View.VISIBLE);
                if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor || forumComment.getLoginId().equals(CacheManager.getInstance().getCurrentUser().getLoginId())) {
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
                    requestOptions.placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder);

                    Glide.with(ApplicationManager.getContext()).load(Constants.BASE_URL + forumComment.getFileUrl()).apply(requestOptions.bitmapTransform(new BlurTransformation(25, 3)))
                            .thumbnail(0.02f).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Glide.with(ApplicationManager.getContext()).load(Constants.BASE_URL + forumComment.getFileUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).thumbnail(0.2f).listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                                            return false;
                                        }
                                    }).into(ivForumImage);
                                }
                            });
                            //holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    }).apply(requestOptions).into(ivForumImage);
                } else {
                    ivForumImage.setVisibility(View.GONE);
                }
            } else {
                tvFileName.setVisibility(View.VISIBLE);
                tvFileName.setText(FileUtils.getFileNameFromURL(Constants.BASE_URL + forumComment.getFileUrl()));
                //hideImageView(holder);
                tvFileName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onChatItemClickListener.get().onChatItemClick(idx);
                    }
                });

                ivForumImage.setVisibility(View.GONE);
            }

        } else {
            ivForumImage.setVisibility(View.GONE);
            tvFileName.setVisibility(View.GONE);


        }
   /*     if (forumComment.getImageURI() != null) {
            if (forumComment.getFileType().equals(Constants.FileType.FILE_TYPE_IMAGE)) {
                ivForumImage.setVisibility(View.VISIBLE);

                ivForumImage.setImageURI(Uri.parse(forumComment.getImageURI()));
            } else if (forumComment.getFileType().equals(Constants.FileType.FILE_TYPE_PDF)) {
                ivForumImage.setVisibility(View.GONE);
                tvFileName.setVisibility(View.VISIBLE);
                tvFileName.setText();

            }
        } else if (forumComment.
                getFileUrl() != null && !forumComment.getFileUrl().isEmpty() && !forumComment.getFileUrl().equals("null")) {
            ivForumImage.setVisibility(View.VISIBLE);
            Glide.with(ApplicationManager.getContext()).load(Constants.BASE_URL + forumComment.getFileUrl()).thumbnail(0.5f).diskCacheStrategy(DiskCacheStrategy.ALL).into(ivForumImage);
        } else {
            ivForumImage.setVisibility(View.GONE);
        }
*/
        tvFileName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChatItemClickListener.get().onChatItemFileClic(idx);
            }
        });

        this.idx = idx;

    }

    @Override
    public void onClick(View view) {

        if (onChatItemClickListener == null || onChatItemClickListener.get() == null) {
            return;
        }

        switch (view.getId()) {
            case R.id.cont_item:
            case R.id.tv_message:
                onChatItemClickListener.get().onChatItemClick(getAdapterPosition());
                break;

        }
    }
}
