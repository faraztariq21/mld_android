package com.imedhealthus.imeddoctors.common.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.AboutUsFragment;
import com.imedhealthus.imeddoctors.common.fragments.AddEditPaymentMethodFragment;
import com.imedhealthus.imeddoctors.common.fragments.AddEditPrescribedMedication;
import com.imedhealthus.imeddoctors.common.fragments.BaseFragment;
import com.imedhealthus.imeddoctors.common.fragments.BookedAppointmentsFragment;
import com.imedhealthus.imeddoctors.common.fragments.CallFragment;
import com.imedhealthus.imeddoctors.common.fragments.ChangePasswordFragment;
import com.imedhealthus.imeddoctors.common.fragments.ChatFragment;
import com.imedhealthus.imeddoctors.common.fragments.FeedBackFragment;
import com.imedhealthus.imeddoctors.common.fragments.ForumCommentsFragment;
import com.imedhealthus.imeddoctors.common.fragments.ForumFragment;
import com.imedhealthus.imeddoctors.common.fragments.ForumPostQuestionFragment;
import com.imedhealthus.imeddoctors.common.fragments.FragmentImageFull;
import com.imedhealthus.imeddoctors.common.fragments.InboxFragment;
import com.imedhealthus.imeddoctors.common.fragments.InvoicesFragment;
import com.imedhealthus.imeddoctors.common.fragments.NotificationFragment;
import com.imedhealthus.imeddoctors.common.fragments.OverlayFragment;
import com.imedhealthus.imeddoctors.common.fragments.PaymentFragment;
import com.imedhealthus.imeddoctors.common.fragments.PaymentMethodsFragment;
import com.imedhealthus.imeddoctors.common.fragments.PrescriptionDetailFragment;
import com.imedhealthus.imeddoctors.common.fragments.QuestionsFragment;
import com.imedhealthus.imeddoctors.common.fragments.SendToPharmacyFragment;
import com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler;
import com.imedhealthus.imeddoctors.common.listeners.FragmentChangeHandler;
import com.imedhealthus.imeddoctors.common.listeners.PopUpWindowListener;
import com.imedhealthus.imeddoctors.common.models.CallSignal;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.common.widgets.MarginChangeAnim;
import com.imedhealthus.imeddoctors.doctor.fragments.AddAppointmentSlotsFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.AddEditAcceptedInsuranceFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.AddEditAwardFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.AddEditCertificationFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.AddEditClinicLocationFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.AddEditEducationDetailFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.AddEditLanguageFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.AddEditMembershipFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.AddEditReferenceDetailFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.AddEditServiceFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.AddEditSkillTrainingFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.AddEditSocialLinkFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.AddEditSpecialityFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.AddEditTrainingProviderDetailFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.AddEditWorkExperienceFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.AppointmentsFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.ArchivedAppointments;
import com.imedhealthus.imeddoctors.doctor.fragments.DoctorHomeFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.DoctorProfileFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.DoctorsListFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.EditDoctorPersonalInfoFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.ReferralFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.SearchDoctorFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.SelectTimeFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.SubmitDoctorsFeedbackFragment;
import com.imedhealthus.imeddoctors.patient.fragments.AddAttachmentFragment;
import com.imedhealthus.imeddoctors.patient.fragments.AddEditAllergyFragment;
import com.imedhealthus.imeddoctors.patient.fragments.AddEditCurrentMedicationFragment;
import com.imedhealthus.imeddoctors.patient.fragments.AddEditDiseaseFragment;
import com.imedhealthus.imeddoctors.patient.fragments.AddEditEmergencyContactFragment;
import com.imedhealthus.imeddoctors.patient.fragments.AddEditFamilyHistoryFragment;
import com.imedhealthus.imeddoctors.patient.fragments.AddEditGuardianFragment;
import com.imedhealthus.imeddoctors.patient.fragments.AddEditHobbyFragment;
import com.imedhealthus.imeddoctors.patient.fragments.AddEditHospitalizationFragment;
import com.imedhealthus.imeddoctors.patient.fragments.AddEditInsuranceFragment;
import com.imedhealthus.imeddoctors.patient.fragments.AddEditPastPharmacyFragment;
import com.imedhealthus.imeddoctors.patient.fragments.AddEditPastPhysicianFragment;
import com.imedhealthus.imeddoctors.patient.fragments.AddEditPastSurgeryFragment;
import com.imedhealthus.imeddoctors.patient.fragments.AddEditPragnencyDetailFragment;
import com.imedhealthus.imeddoctors.patient.fragments.AddEditPregnancyFragment;
import com.imedhealthus.imeddoctors.patient.fragments.AddEditSocialHistoryFragment;
import com.imedhealthus.imeddoctors.patient.fragments.BookAppointmentFragment;
import com.imedhealthus.imeddoctors.patient.fragments.CompleteBookAppointmentFragment;
import com.imedhealthus.imeddoctors.patient.fragments.EditPatientPersonalInfoFragment;
import com.imedhealthus.imeddoctors.patient.fragments.PatientHomeFragment;
import com.imedhealthus.imeddoctors.patient.fragments.PatientProfileFragment;
import com.imedhealthus.imeddoctors.patient.fragments.PatientsListFragment;
import com.imedhealthus.imeddoctors.patient.fragments.TreatmentPlanFragment;
import com.opentok.android.Subscriber;

import butterknife.BindView;
import pub.devrel.easypermissions.EasyPermissions;

import static com.imedhealthus.imeddoctors.common.fragments.CallFragment.WRITE_SETTINGS_PERM;
import static com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler.SIGNAL_TYPE_CALL_ACCEPTED;
import static com.imedhealthus.imeddoctors.common.utils.Constants.CODE_DRAW_OVER_OTHER_APP_PERMISSION;

/**
 * Created by umair on 1/7/18.
 */

public abstract class BaseFragmentActivity extends BaseActivity implements FragmentChangeHandler, PopUpWindowListener {

    @BindView(R.id.cont_header)
    ViewGroup contHeader;
    @BindView(R.id.tv_header_title)
    TextView tvHeaderTitle;

    @BindView(R.id.cont_overlay)
    ViewGroup contOverlay;
    @BindView(R.id.tv_header_title_overlay)
    TextView tvHeaderTitleOverlay;
    @BindView(R.id.cont_content_overlay)
    View contContentOverlay;

    protected BaseFragment currentFragment;
    protected BaseFragment previousFragment;
    protected OverlayFragment currentOverlayFragment;
    PopupWindow popupWindow = null;

    @Override
    public void displayPopUpWindow(View view, Subscriber subscriber) {
        if (popupWindow == null /*&& subscriber != null*/) {
            LayoutInflater inflater = (LayoutInflater)
                    getSystemService(LAYOUT_INFLATER_SERVICE);
            final View popupView = inflater.inflate(R.layout.popup_window, null);

            FrameLayout frameLayout = popupView.findViewById(R.id.cont_window);

            // create the popup window
            int width = LinearLayout.LayoutParams.WRAP_CONTENT;
            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
            boolean focusable = false; // lets taps outside the popup also dismiss it
            popupWindow = new PopupWindow(popupView, width, height);

            // show the popup window
            // which view you pass in doesn't matter, it is only used for the window tolken
            //popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
            popupWindow.showAsDropDown(popupView);
            popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    popupWindow = null;
                }
            });
            /*if (subscriber.getView().getParent() != null)
                ((ViewGroup) subscriber.getView().getParent()).removeView(subscriber.getView()); // <- fix
            frameLayout.addView(subscriber.getView());
            */
            popupView.setOnTouchListener(new View.OnTouchListener() {
                int orgX, orgY;
                int offsetX, offsetY;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            orgX = (int) event.getX();
                            orgY = (int) event.getY();
                            break;
                        case MotionEvent.ACTION_MOVE:

                            offsetX = (int) event.getRawX() - orgX;
                            offsetY = (int) event.getRawY() - orgY;
                            popupWindow.update(offsetX, offsetY, -1, -1, true);
                            break;
                    }
                    return true;
                }
            });


            // dismiss the popup window when touched
        /*popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });*/
        }
    }

    @Override
    public void dismissPopUpWindow(View view) {
        if (popupWindow != null) {
            popupWindow.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


        Log.i("Base Fragment Activity", "OnResume is called");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("Base Fragment Activity", "onCreate is called");

    }

    //Fragment handling
    @Override
    public void changeFragment(Constants.Fragment fragment, Bundle arguments) {
        switch (fragment) {
            case FragmentImageFull:
                changeFragmentHelper(new FragmentImageFull(), arguments);
                break;

            case PatientHome:
                changeFragmentHelper(new PatientHomeFragment(), null);
                break;

            case Appointments:
                changeFragmentHelper(new BookedAppointmentsFragment(), null);
                break;

            case SearchDoctor:
                changeFragmentHelper(new SearchDoctorFragment(), null);
                break;

            case PaymentMethods:
                changeFragmentHelper(new PaymentMethodsFragment(), null);
                break;

            case Forum:
                changeFragmentHelper(new ForumFragment(), null);
                break;

            case Inbox:
                changeFragmentHelper(new InboxFragment(), null);
                break;

            case Questions:
                changeFragmentHelper(new QuestionsFragment(), null);
                break;

            case Referral:
                changeFragmentHelper(new ReferralFragment(), null);
                break;

            case Invoices:
                changeFragmentHelper(new InvoicesFragment(), null);
                break;

            case EditPatientPersonalInfo:
                changeFragmentHelper(new EditPatientPersonalInfoFragment(), null);
                break;

            case AddEditPrescribedMedication:
                changeFragmentHelper(new AddEditPrescribedMedication(), null);
                break;
            case AddAttachment:
                changeFragmentHelper(new AddAttachmentFragment(), null);
                break;

            case AddEditInsurance:
                changeFragmentHelper(new AddEditInsuranceFragment(), null);
                break;

            case DoctorsList:
                changeFragmentHelper(new DoctorsListFragment(), null);
                break;

            case TreatmentPlan:
                changeFragmentHelper(new TreatmentPlanFragment(), null);
                break;

            case PrescriptionDetail:
                changeFragmentHelper(new PrescriptionDetailFragment(), null);
                break;

            case Feedback:
                changeFragmentHelper(new FeedBackFragment(), null);
                break;
            case Notifications:
                changeFragmentHelper(new NotificationFragment(), null);
                break;
            case PatientProfile:
                changeFragmentHelper(new PatientProfileFragment(), null);
                break;

            case SelectTime:
                changeFragmentHelper(new SelectTimeFragment(), null);
                break;

            case PatientsList:
                changeFragmentHelper(new PatientsListFragment(), null);
                break;

            case DoctorProfile:
                changeFragmentHelper(DoctorProfileFragment.newInstance(SharedData.getInstance().getSelectedDoctorProfileTab()), null);
                break;

            case DoctorHome:
                changeFragmentHelper(new DoctorHomeFragment(), null);
                break;

            case BookAppointment:
                changeFragmentHelper(new BookAppointmentFragment(), null);
                break;

            case CompleteAppointment:
                changeFragmentHelper(new CompleteBookAppointmentFragment(), null);
                break;

            case AddEditPaymentMethod:
                changeFragmentHelper(new AddEditPaymentMethodFragment(), null);
                break;

            case AboutUs:
                changeFragmentHelper(new AboutUsFragment(), null);
                break;

            case Paybill:
                changeFragmentHelper(new PaymentFragment(), null);
                break;

            case SubmitDoctorFeedback:
                changeFragmentHelper(new SubmitDoctorsFeedbackFragment(), null);
                break;

            case AddEditEmergencyContact:
                changeFragmentHelper(new AddEditEmergencyContactFragment(), null);
                break;

            case AddEditGuardian:
                changeFragmentHelper(new AddEditGuardianFragment(), null);
                break;

            case ChangePassword:
                changeFragmentHelper(new ChangePasswordFragment(), null);
                break;

            case AddEditDisease:
                changeFragmentHelper(new AddEditDiseaseFragment(), null);
                break;

            case AddEditCurrentMedication:
                changeFragmentHelper(new AddEditCurrentMedicationFragment(), null);
                break;

            case AddEditPastPhysician:
                changeFragmentHelper(new AddEditPastPhysicianFragment(), null);
                break;

            case AddEditPastSurgery:
                changeFragmentHelper(new AddEditPastSurgeryFragment(), null);
                break;

            case AddEditAllergy:
                changeFragmentHelper(new AddEditAllergyFragment(), null);
                break;

            case AddEditPastPharmacy:
                changeFragmentHelper(new AddEditPastPharmacyFragment(), null);
                break;

            case AddEditHospitalization:
                changeFragmentHelper(new AddEditHospitalizationFragment(), null);
                break;

            case AddEditFamilyHistory:
                changeFragmentHelper(new AddEditFamilyHistoryFragment(), null);
                break;

            case AddEditHobby:
                changeFragmentHelper(new AddEditHobbyFragment(), null);
                break;

            case AddEditSocialHistory:
                changeFragmentHelper(new AddEditSocialHistoryFragment(), null);
                break;

            case AddEditPregnancyHistory:
                changeFragmentHelper(new AddEditPregnancyFragment(), null);
                break;

            case AddEditPregnancyDetail:
                changeFragmentHelper(new AddEditPragnencyDetailFragment(), null);
                break;

            case AddEditEducationDetail:
                changeFragmentHelper(new AddEditEducationDetailFragment(), null);
                break;

            case AddEditExperience:
                changeFragmentHelper(new AddEditWorkExperienceFragment(), null);
                break;

            case AddEditSpeciality:
                changeFragmentHelper(new AddEditSpecialityFragment(), null);
                break;

            case AddEditSkillTraining:
                changeFragmentHelper(new AddEditSkillTrainingFragment(), null);
                break;

            case AddEditTrainingProvider:
                changeFragmentHelper(new AddEditTrainingProviderDetailFragment(), null);
                break;

            case AddEditSocialLink:
                changeFragmentHelper(new AddEditSocialLinkFragment(), null);
                break;

            case AddEditLocation:
                changeFragmentHelper(new AddEditClinicLocationFragment(), null);
                break;

            case AddEditAcceptedInsurance:
                changeFragmentHelper(new AddEditAcceptedInsuranceFragment(), null);
                break;

            case AddEditCertificate:
                changeFragmentHelper(new AddEditCertificationFragment(), null);
                break;

            case AddEditMembership:
                changeFragmentHelper(new AddEditMembershipFragment(), null);
                break;

            case AddEditAward:
                changeFragmentHelper(new AddEditAwardFragment(), null);
                break;

            case AddEditLanguage:
                changeFragmentHelper(new AddEditLanguageFragment(), null);
                break;

            case AddEditReference:
                changeFragmentHelper(new AddEditReferenceDetailFragment(), null);
                break;

            case EditDoctorPersonalInfo:
                changeFragmentHelper(new EditDoctorPersonalInfoFragment(), null);
                break;

            case AddAppointmentSlots:
                changeFragmentHelper(new AddAppointmentSlotsFragment(), null);
                break;

            case AddEditService:
                changeFragmentHelper(new AddEditServiceFragment(), null);
                break;

            case CallFragment:


                changeFragmentHelper(new CallFragment(), null);
                break;

            case ForumPostQuestionFragment:
                changeFragmentHelper(new ForumPostQuestionFragment(), null);
                break;

            case ForumCommentsFragment:
                changeFragmentHelper(new ForumCommentsFragment(), arguments);
                break;

            case ChatFragment:
                changeFragmentHelper(new ChatFragment(), arguments);
                break;

            case SendToPharmacy:
                changeFragmentHelper(new SendToPharmacyFragment(), null);
                break;
            case ARCHIVED_APPOINTMENTS:
                changeFragmentHelper(new ArchivedAppointments(), null);
                break;

        }

    }

    public BaseFragment getCurrentFragment() {
        return currentFragment;
    }

    private void changeFragmentHelper(BaseFragment fragment, Bundle arguments) {

        currentFragment = fragment;

        if (currentFragment.getArguments() == null)
            currentFragment.setArguments(arguments);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.cont_content, fragment, fragment.getName());
        fragmentTransaction.commitAllowingStateLoss();
        getSupportFragmentManager().executePendingTransactions();
        adjustHeader();

    }

    private void adjustHeader() {

        if (currentFragment.showHeader()) {
            tvHeaderTitle.setText(currentFragment.getTitle());
            contHeader.setVisibility(View.VISIBLE);
        } else {
            contHeader.setVisibility(View.GONE);
        }

    }

    @Override
    public void showOverlayFragment(OverlayFragment fragment, Object data) {

        currentOverlayFragment = fragment;
        fragment.setData(data);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();

        fragmentTransaction.replace(R.id.cont_content_overlay, fragment, fragment.getName());
        fragmentTransaction.commit();
        manager.executePendingTransactions();

        tvHeaderTitleOverlay.setText(fragment.getTitle());

        //Animating from right to left
        int marginLeft = (int) GenericUtils.convertDpToPixels(this, 50);
        int screenWidth = GenericUtils.getScreenWidth(this);

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) contContentOverlay.getLayoutParams();
        params.width = screenWidth - marginLeft;
        contContentOverlay.setLayoutParams(params);

        MarginChangeAnim anim = new MarginChangeAnim(contContentOverlay, screenWidth, marginLeft);
        anim.setDuration(400);

        contContentOverlay.startAnimation(anim);
        contOverlay.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideOverlayFragment() {

        if (currentOverlayFragment != null) {
            onDismissOverlayClick(null);
        }
    }

    @Override
    public void refreshOverlayHeader() {
        if (currentOverlayFragment != null) {
            tvHeaderTitleOverlay.setText(currentOverlayFragment.getTitle());
        }
    }

    //Actions
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_more:
                toggleMenu();
                break;

            default:
                if (currentOverlayFragment != null) {
                    currentOverlayFragment.onClick(view);
                } else if (currentFragment != null) {
                    currentFragment.onClick(view);
                }
        }
    }

    protected void toggleMenu() {

    }

    public void onDismissOverlayClick(View view) {

        if (currentOverlayFragment == null || currentFragment == null) {
            return;
        }
        if (currentOverlayFragment.canHandleOnBackClick()) {
            currentOverlayFragment.onBackClick();
            return;
        }

        currentFragment.onDismissOverlayFragment(currentOverlayFragment.getData());
        currentOverlayFragment = null;

        //Animating from left to right
        MarginChangeAnim anim = new MarginChangeAnim(contContentOverlay, contContentOverlay.getLeft(), GenericUtils.getScreenWidth(this));
        anim.setDuration(400);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                contOverlay.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        contContentOverlay.startAnimation(anim);

    }

    @Override
    public void onBackPressed() {
        if (currentOverlayFragment != null) {
            onDismissOverlayClick(null);
            return;
        }

        if (currentFragment instanceof ChatFragment && !((ChatFragment) currentFragment).isOnBackPressedHandled())
            ((ChatFragment) currentFragment).onBackPressed();
        else
            super.onBackPressed();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (!SharedData.getInstance().isAskingPermissionsOnAcceptingCall()) {
            BaseFragment baseFragment = currentFragment;
            if (baseFragment != null)
                baseFragment.onActivityResult(requestCode, resultCode, data);
        } else {
            if (requestCode == CODE_DRAW_OVER_OTHER_APP_PERMISSION) {

                if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(BaseFragmentActivity.this)))
                    SharedData.getInstance().setCanDrawOverOtherApps(true);
                else
                    SharedData.getInstance().setCanDrawOverOtherApps(false);

                sendCallSignalAndStartCallFragment();

                return;
            } else if (requestCode == WRITE_SETTINGS_PERM && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.System.canWrite(BaseFragmentActivity.this)) {
                if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(BaseFragmentActivity.this)))
                    sendCallSignalAndStartCallFragment();
                else
                    startDrawOverOtherAppsActivity();
            } else {
                Toast.makeText(BaseFragmentActivity.this, "Permission denied. Unable to start call", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void sendCallSignalAndStartCallFragment() {
        SharedData.getInstance().setAskingPermissionsOnAcceptingCall(false);

        Intent intent = new Intent(BaseFragmentActivity.this, CallActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CallFragment.ordinal());
        startActivity(intent, true);
        sendCallAcceptSignal();

    }

    public void sendCallAcceptSignal() {

        SharedData.getInstance().setCallResumed(true);
        if (OpenTokHandler.getInstance().getSession() == null) {
            return;
        }

        CallSignal callSignal = SharedData.getInstance().getSelectedCallSignal();

        Gson gson = new Gson();
        String json = gson.toJson(callSignal);


        OpenTokHandler.getInstance().getSession().sendSignal(SIGNAL_TYPE_CALL_ACCEPTED, json);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (SharedData.getInstance().isAskingPermissionsOnAcceptingCall()) {
            if (EasyPermissions.hasPermissions(BaseFragmentActivity.this, permissions)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (Settings.System.canWrite(BaseFragmentActivity.this)) {


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(BaseFragmentActivity.this)) {
                            sendCallSignalAndStartCallFragment();
                        } else {
                            SharedData.getInstance().setAskingPermissionsOnAcceptingCall(true);
                            startDrawOverOtherAppsActivity();
                        }


                    } else {
                        SharedData.getInstance().setAskingPermissionsOnAcceptingCall(true);
                        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + BaseFragmentActivity.this.getPackageName()));
                        startActivityForResult(intent, WRITE_SETTINGS_PERM);
                    }
                }
            }


        } else {
            switch (requestCode) {
                case CallFragment.VIDEO_APP_PERM:
                case CallFragment.SETTINGS_SCREEN_PERM:
                    if (currentFragment instanceof CallFragment) {
                        currentFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                    }
                    break;
                case AppointmentsFragment.REQUEST_CALL_PERMISSION:
                    if (DoctorProfileFragment.currentFragment instanceof AppointmentsFragment) {
                        DoctorProfileFragment.currentFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                    }
                    break;
                case SelectTimeFragment.REQUEST_CALL_PERMISSION:
                    if (currentFragment instanceof SelectTimeFragment) {
                        currentFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                    }
                    break;
            }
        }
    }

    protected void startDrawOverOtherAppsActivity() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + BaseFragmentActivity.this.getPackageName()));
        BaseFragmentActivity.this.startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION);
    }


}
