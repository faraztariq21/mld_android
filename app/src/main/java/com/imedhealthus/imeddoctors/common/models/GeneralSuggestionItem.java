package com.imedhealthus.imeddoctors.common.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GeneralSuggestionItem implements SuggestionListItem {

    @SerializedName("Name")
    String suggestionItem;
    @SerializedName("Id")
    String id;

    public GeneralSuggestionItem(String suggestionItem) {
        this.suggestionItem = suggestionItem;
    }


    @Override
    public String getSuggestionText() {
        return suggestionItem;
    }

    @Override
    public SuggestionListItem getSuggestionListItem() {
        return GeneralSuggestionItem.this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static ArrayList<GeneralSuggestionItem> getSuggestions(ArrayList<String> suggestions) {
        ArrayList<GeneralSuggestionItem> generalSuggestionItems = new ArrayList<>();
        for (String suggestion : suggestions) {
            generalSuggestionItems.add(new GeneralSuggestionItem(suggestion));
        }

        return generalSuggestionItems;
    }

}
