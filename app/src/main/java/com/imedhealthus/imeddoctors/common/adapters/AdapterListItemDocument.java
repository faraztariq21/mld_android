package com.imedhealthus.imeddoctors.common.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.listeners.FragmentAttachImageListener;
import com.imedhealthus.imeddoctors.common.models.ListItemDocument;
import com.imedhealthus.imeddoctors.common.utils.FileUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterListItemDocument extends RecyclerView.Adapter<AdapterListItemDocument.DocumentViewHolder> {

    Context context;
    WeakReference<FragmentAttachImageListener> fragmentAttachImageListenerWeakReference;
    ArrayList<? extends ListItemDocument> listItemDocuments;
    boolean showCrossButton;

    public AdapterListItemDocument(Context context, FragmentAttachImageListener fragmentAttachImageListenerWeakReference, ArrayList<? extends ListItemDocument> listItemDocuments, boolean showCrossButton) {
        this.context = context;
        this.fragmentAttachImageListenerWeakReference = new WeakReference<>(fragmentAttachImageListenerWeakReference);
        this.listItemDocuments = listItemDocuments;
        this.showCrossButton = showCrossButton;
    }

    public void updateDocuments(ArrayList<? extends ListItemDocument> listItemDocuments) {

        this.listItemDocuments = listItemDocuments;
        notifyDataSetChanged();
    }

    public void removeDocument(int index) {
        this.listItemDocuments.remove(index);
        notifyItemRemoved(index);
        notifyItemRangeChanged(index, listItemDocuments.size());
    }

    @NonNull
    @Override
    public DocumentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_document, parent, false);
        return new DocumentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DocumentViewHolder holder, final int position) {

        ListItemDocument listItemDocument = listItemDocuments.get(position);
        String documentName;
        String extension;

        if (showCrossButton)
            holder.ivCancelImage.setVisibility(View.VISIBLE);
        else
            holder.ivCancelImage.setVisibility(View.INVISIBLE);

        if (listItemDocument.isDocumentUri()) {
            documentName = FileUtils.getFileNameFromPath(listItemDocument.getDocumentPath());
        } else
            documentName = FileUtils.getFileNameFromURL(listItemDocument.getDocumentPath());

        if (documentName.endsWith(".pdf"))
            holder.ivMainImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_pdf));
        if (documentName.endsWith(".pptx"))
            holder.ivMainImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_ppt));
        if (documentName.endsWith(".xls"))
            holder.ivMainImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_xls));
        if (documentName.endsWith(".txt"))
            holder.ivMainImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_txt));
        if (documentName.endsWith(".docx"))
            holder.ivMainImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_doc));


        holder.tvDocumentName.setText(documentName);

        holder.ivCancelImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentAttachImageListenerWeakReference.get().onImageCancelTapped(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return listItemDocuments.size();
    }

    public class DocumentViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_list_item_image)
        ImageView ivMainImage;
        @BindView(R.id.iv_cancel_image)
        ImageView ivCancelImage;
        @BindView(R.id.tv_document_name)
        TextView tvDocumentName;

        public DocumentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
