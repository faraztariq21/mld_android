package com.imedhealthus.imeddoctors.common.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v4.content.FileProvider;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.models.ChatMessage;
import com.imedhealthus.imeddoctors.common.models.ForumComment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by umair on 8/19/17.
 */

public class FileUtils {

    public static String getFileNameFromUri(Context activity, Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    @SuppressLint("NewApi")
    public static String getPathFromUri(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static Uri copyFileAndGetUri(Context context, Uri uri, String fileName) {
        // Create imageDir

        InputStream in = null;
        try {
            in = context.getContentResolver().openInputStream(uri);

            File dirctory = new File(Environment.getExternalStorageDirectory() + Constants.EXTERNAL_STORAGE_DIRECTORY);
            boolean result = dirctory.mkdirs();

            File file = new File(dirctory, fileName);

            OutputStream out = null;
            try {
                out = new FileOutputStream(file);

                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                out.close();
                in.close();
                Uri uri1 = FileProvider.getUriForFile(context, "com.mylivedoctor.provider", file);
                return uri1;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static boolean checkFilesFromDownloadFolder(Context context, Uri uri) {
        String path = null;
        try {
            path = getPathFromUri(context, uri);
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof IllegalArgumentException)
                return true;
        }
        if (path != null)
            return false;
        else
            return true;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public static ArrayList<String> parseFile(Context context, String fileName, String token) {

        BufferedReader reader = null;

        try {
            StringBuilder builder = new StringBuilder();
            reader = new BufferedReader(new InputStreamReader(context.getAssets().open(fileName)));
            String line;

            while ((line = reader.readLine()) != null) {
                builder.append(line).append("\n");
            }

            ArrayList<String> data = new ArrayList<>();
            data.addAll(Arrays.asList(builder.toString().split(token)));

            Collections.sort(data, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o1.compareTo(o2);
                }
            });

            return data;

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }

        return new ArrayList<>();

    }

    public static void chooseFiles(String minmeType, int requestCode, Activity activity, android.support.v4.app.Fragment fragment) {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(minmeType);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // special intent for Samsung file manager
        Intent sIntent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
        // if you want any file type, you can skip next line
        sIntent.putExtra("CONTENT_TYPE", minmeType);
        /*    String[] mimetypes = {"text*//*", "application*//*"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);*/
        sIntent.addCategory(Intent.CATEGORY_BROWSABLE);

        Intent chooserIntent;
        if (activity.getPackageManager().resolveActivity(sIntent, 0) != null) {
            // it is device with samsung file manager
            chooserIntent = Intent.createChooser(sIntent, "Open file");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{intent});
        } else {
            chooserIntent = Intent.createChooser(intent, "Open file");
        }

        try {
            if (fragment != null)
                fragment.startActivityForResult(chooserIntent, requestCode);
            else
                activity.startActivityForResult(chooserIntent, requestCode);

        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(activity, "No suitable File Manager was found.", Toast.LENGTH_SHORT).show();
        }
    }


    public static void openDocument(Activity activity, String filePath) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        File file = new File(filePath);
        String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString());
        String mimetype = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        if (extension.equalsIgnoreCase("") || mimetype == null) {
            // if there is no extension or there is no definite mimetype, still try to open the file
            intent.setDataAndType(FileProvider.getUriForFile(activity, activity.getResources().getString(R.string.file_provide_authority), file), "text/*");

        } else {
            intent.setDataAndType(FileProvider.getUriForFile(activity, activity.getResources().getString(R.string.file_provide_authority), file), mimetype);
        }

        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        // custom message for the intent
        activity.startActivity(Intent.createChooser(intent, "Choose an Application:"));
    }

    public static String getFileNameFromURL(String url) {
        if (url == null) {
            return "";
        }
        try {
            URL resource = new URL(url);
            String host = resource.getHost();
            if (host.length() > 0 && url.endsWith(host)) {
                // handle ...example.com
                return "";
            }
        } catch (MalformedURLException e) {
            return "";
        }

        int startIndex = url.lastIndexOf('/') + 1;
        int length = url.length();

        // find end index for ?
        int lastQMPos = url.lastIndexOf('?');
        if (lastQMPos == -1) {
            lastQMPos = length;
        }

        // find end index for #
        int lastHashPos = url.lastIndexOf('#');
        if (lastHashPos == -1) {
            lastHashPos = length;
        }

        // calculate the end index
        int endIndex = Math.min(lastQMPos, lastHashPos);
        return url.substring(startIndex, endIndex);
    }

    /**
     * @param chatMessage  Not necessary, It is provided if you are downloading a document from chat and want to save that document corresponding to the chat
     * @param forumComment Not necessary, It is provided if you are downloading a document from forum and want to save that document corresponding to the chat
     * @param context
     * @param url          url of the file to be downloaded
     * @param title        title of the file to be downloaded
     */
    public static void downloadFile(ChatMessage chatMessage, ForumComment forumComment, Context context, String url, String title) {
        Toast.makeText(context, context.getResources().getString(R.string.downloading), Toast.LENGTH_SHORT).show();
        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri Download_Uri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
        String dirctory = Environment.getExternalStorageDirectory().getPath() + Constants.EXTERNAL_STORAGE_DIRECTORY;
        File direct = new File(dirctory);

        if (!direct.exists()) {

            boolean check = direct.mkdirs();

        }

        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.setTitle("Downloading");
        request.setDescription("Downloading File");
        request.setMimeType("application/*");
        request.allowScanningByMediaScanner();
        try {
            if (chatMessage == null && forumComment == null)
                request.setDestinationInExternalPublicDir(Constants.EXTERNAL_STORAGE_IMAGES, title);
            else
                request.setDestinationInExternalPublicDir(Constants.EXTERNAL_STORAGE_DIRECTORY, title);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        try {
            long refId = downloadManager.enqueue(request);
            String filePath = Environment.getExternalStorageDirectory()
                    + Constants.EXTERNAL_STORAGE_DIRECTORY + "/" + title;
            if (chatMessage != null) {
                SharedPrefsHelper.saveChatFileWithDownloadId(context, refId, chatMessage, filePath);
                //SharedPrefsHelper.putFileDownloadStatus(context, refId, true);
            } else if (forumComment != null) {
                SharedPrefsHelper.saveForumCommentFileWithDownloadId(context, refId, forumComment, filePath);
                //SharedPrefsHelper.putF(context, refId, true);

            } else {
                new TinyDB(context).putBoolean("isDownload " + refId, true);
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public static boolean checkFileExists(Uri uri) {
        File file = new File(uri.getPath());
        if (file.getAbsoluteFile().exists())
            return true;
        else
            return false;
    }

    public static boolean checkFileExists(String filePath) {
        File file = new File(filePath);
        if (file.exists())
            return true;
        else
            return false;
    }

    public static void openFile(Context context, String uri) {
        MimeTypeMap myMime = MimeTypeMap.getSingleton();
        Intent newIntent = new Intent(Intent.ACTION_VIEW);
        String mimeType = "application/*";
        newIntent.setDataAndType(Uri.parse(uri), mimeType);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            context.startActivity(newIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "No handler for this type of file.", Toast.LENGTH_LONG).show();
        }
    }

    private static String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();

        }
    }

    public static String getFileNameFromPath(String path) {
        String filename = path.substring(path.lastIndexOf("/") + 1);
        return filename;
    }

    public static Uri compressImageAndGetNewImageUri(Context context, Uri selectedImageUri) {
        String newPath = ImageUtils.compressImage(getPathFromUri(context, selectedImageUri));
        File file = new File(newPath);
        Uri newImageUri = Uri.fromFile(file);
        return newImageUri;
    }
}
