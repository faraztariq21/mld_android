package com.imedhealthus.imeddoctors.common.models;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Malik Hamid on 1/5/2018.
 */

public class Invoice {

    private String invoiceId, invoiceTitle;
    private double amount;
    private long timeStamp;
    private Date date;

    public Invoice() {
    }

    public static List<Invoice> getInvoices(JSONObject jsonObject){
        List<Invoice> invoices =new ArrayList<>();
        try {
            JSONArray jsonArray = jsonObject.getJSONArray("Invoices");
            for(int i= 0;i<jsonArray.length();i++){
                Invoice invoice = new Invoice(jsonArray.getJSONObject(i));
                invoices.add(invoice);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return invoices;
    }
    public Invoice(JSONObject jsonObject) {
        try {
            invoiceId = jsonObject.getString("Id");
            invoiceTitle = jsonObject.getString("ChargeReason");
            amount = jsonObject.getDouble("AmountCharged");
            timeStamp = GenericUtils.getTimeDateInLong(jsonObject.getString("CreatedDate"));
            date = new Date(timeStamp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public Invoice(String invoiceId, String invoiceTitle, double amount, long timeStamp) {
        this.invoiceId = invoiceId;
        this.invoiceTitle = invoiceTitle;
        this.amount = amount;
        this.timeStamp = timeStamp;
    }

    public boolean isSameMonth(int year, int month) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        if (year == calendar.get(Calendar.YEAR) && month == calendar.get(Calendar.MONTH)) {
            return true;
        }
        return false;

    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceTitle() {
        return invoiceTitle;
    }

    public void setInvoiceTitle(String invoiceTitle) {
        this.invoiceTitle = invoiceTitle;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
