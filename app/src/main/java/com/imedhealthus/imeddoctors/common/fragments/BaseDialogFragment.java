package com.imedhealthus.imeddoctors.common.fragments;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentManager;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class BaseDialogFragment extends android.support.v4.app.DialogFragment {

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow()

                .getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        int width = MATCH_PARENT;
        int height = WRAP_CONTENT;
        getDialog().getWindow().setLayout(width, height);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, BaseDialogFragment.class.toString());
    }


}
