package com.imedhealthus.imeddoctors.common.view_holders;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.listeners.OnNotificationClickListener;
import com.imedhealthus.imeddoctors.common.models.Notification;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 1/16/2018.
 */

public class NotificationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    RelativeLayout contItem;

    @BindView(R.id.iv_type)
    ImageView ivIcon;
    @BindView(R.id.tv_title)
    TextView tvTitle;

    Context context;

    private int idx;
    private WeakReference<OnNotificationClickListener> onNotificationClickListener;

    public NotificationViewHolder(View view, OnNotificationClickListener onNotificationClickListener) {
        super(view);
        ButterKnife.bind(this, view);
        contItem.setOnClickListener(this);
        context = view.getContext();
        this.onNotificationClickListener = new WeakReference<>(onNotificationClickListener);

    }

    public void setData(Notification notification) {
        tvTitle.setText(notification.getNotificationTitle());

        if (notification.getNotificationType().equalsIgnoreCase("chat")) {

            ivIcon.setColorFilter(context.getResources().getColor(R.color.light_blue));
            ivIcon.setImageResource(R.drawable.ic_message);

        } else if (notification.getNotificationType().toLowerCase().contains("appointment")) {

            ivIcon.setImageResource(R.drawable.ic_appointment_light_blue);

        } else if (notification.getNotificationType().toLowerCase().equalsIgnoreCase("AdminRefferQuestionToDoctors")) {

            ivIcon.setColorFilter(context.getResources().getColor(R.color.light_blue));
            ivIcon.setImageResource(R.drawable.ic_questions);

        } else if (notification.getNotificationType().toLowerCase().equalsIgnoreCase("CaseReferrals")
                || notification.getNotificationType().toLowerCase().equalsIgnoreCase("CaseReferralsResponse")) {

            ivIcon.setColorFilter(context.getResources().getColor(R.color.light_blue));
            ivIcon.setImageResource(R.drawable.ic_reference);

        } else if (notification.getNotificationType().toLowerCase().equalsIgnoreCase("DoctorForumComments")
                || notification.getNotificationType().toLowerCase().equalsIgnoreCase("PatientForumComments")) {

            if (notification.isPublic()) {
                ivIcon.setColorFilter(context.getResources().getColor(R.color.light_blue));
                ivIcon.setImageResource(R.drawable.ic_forum);
            } else {
                ivIcon.setColorFilter(context.getResources().getColor(R.color.light_blue));
                ivIcon.setImageResource(R.drawable.ic_questions);
            }

        } else {
            ivIcon.setImageResource(R.drawable.ic_appointment_light_blue);
        }


        if (!notification.isActive()) {
            contItem.setBackgroundColor(context.getResources().getColor(R.color.previous_light_gray));
        }

        // Picasso.with(context).load(getResourceId(notification.getNotificationType())).into(ivIcon);
    }


    @Override
    public void onClick(View view) {

        if (onNotificationClickListener == null || onNotificationClickListener.get() == null) {
            return;
        }

        switch (view.getId()) {
            case R.id.cont_item:
                onNotificationClickListener.get().onNotificationItemClick(getAdapterPosition());
                break;


        }
    }

    public int getResourceId(String type) {
        if (type == null)
            return R.drawable.ic_menu_chat_light_blue;
        if (type.equalsIgnoreCase("message")) {

        } else if (type.equalsIgnoreCase("feedback")) {
            return R.drawable.ic_menu_feedback;
        } else if (type.equalsIgnoreCase("appointment")) {
            return R.drawable.ic_appointment_dark_gray;
        }
        return R.drawable.ic_menu_chat_light_blue;
    }
}
