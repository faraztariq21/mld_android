package com.imedhealthus.imeddoctors.common.models;

import java.io.Serializable;

public class Attachment implements ListItemImage, ListItemDocument,Serializable {
    String documentPath, imagePath;
    boolean isDocumentUri, isDocumentURL, isFileUri, isFilePath;


    public Attachment(String imagePath, boolean isFileUri, boolean isFilePath, String documentPath, boolean isDocumentUri, boolean isDocumentURL) {
        this.imagePath = imagePath;
        this.isFileUri = isFileUri;
        this.isFilePath = isFilePath;
        this.documentPath = documentPath;
        this.isDocumentUri = isDocumentUri;
        this.isDocumentURL = isDocumentURL;
    }


    @Override
    public String getDocumentPath() {
        return documentPath;
    }

    @Override
    public boolean isDocumentUri() {
        return isDocumentUri;
    }

    @Override
    public boolean isDocumentURL() {
        return isDocumentURL;
    }

    @Override
    public String getImagePath() {
        return imagePath;
    }

    @Override
    public boolean isImageUri() {
        return isFileUri;
    }

    @Override
    public boolean isFilePath() {
        return isFilePath;
    }
}
