package com.imedhealthus.imeddoctors.common.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 2/11/2018.
 */

public class ChatUsers {

    @SerializedName("Users")
    List<ChatUserModel> chatUserModels = new ArrayList<>();

    public ChatUsers(List<ChatUserModel> chatUserModels) {
        this.chatUserModels = chatUserModels;
    }

    public List<ChatUserModel> getChatUserModels() {
        return chatUserModels;
    }

    public void setChatUserModels(List<ChatUserModel> chatUserModels) {
        this.chatUserModels = chatUserModels;
    }

}
