package com.imedhealthus.imeddoctors.common.view_holders;

import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.listeners.OnPrescriptionItemClickListener;
import com.imedhealthus.imeddoctors.patient.models.Prescription;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 1/11/2018.
 */

public class PrescriptionItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    ConstraintLayout contItem;

    @BindView(R.id.iv_profile)
    ImageView ivProfile;


    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_notes)
    TextView tvNotes;

    @BindView(R.id.iv_edit)
    ImageView ivEdit;

    @BindView(R.id.tv_date)
    TextView tvDate;


    private int idx;
    private WeakReference<OnPrescriptionItemClickListener> onPrescriptionItemClickListener;

    public PrescriptionItemViewHolder(View view, OnPrescriptionItemClickListener onPrescriptionItemClickListener) {

        super(view);
        ButterKnife.bind(this, view);

        contItem.setOnClickListener(this);
        ivEdit.setOnClickListener(this);

        this.onPrescriptionItemClickListener = new WeakReference<>(onPrescriptionItemClickListener);

    }

    public void setData(Prescription prescription, boolean isEditAllowed, int idx) {

        tvName.setText(prescription.getDoctorName());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            String htmlString = Html.fromHtml(prescription.getNotes(), Html.FROM_HTML_MODE_COMPACT).toString();
            tvNotes.setText(Html.fromHtml(Html.fromHtml(htmlString, Html.FROM_HTML_MODE_LEGACY).toString(), Html.FROM_HTML_MODE_LEGACY));
        } else {
            tvNotes.setText(Html.fromHtml(Html.fromHtml(prescription.getNotes()).toString()));
        }

        Picasso.with(ApplicationManager.getContext()).load(prescription.getDoctorProfileImageUrl()).placeholder(R.drawable.ic_profile_placeholder).into(ivProfile);

        if (isEditAllowed) {
            ivEdit.setVisibility(View.VISIBLE);

        } else {
            ivEdit.setVisibility(View.GONE);

        }

        tvDate.setText(prescription.getPrescribedMedications().size() > 0 ? prescription.getPrescribedMedications().get(prescription.getPrescribedMedications().size() - 1).getStartingDateFormatted() : "");

        this.idx = idx;

    }

    @Override
    public void onClick(View view) {

        if (onPrescriptionItemClickListener == null || onPrescriptionItemClickListener.get() == null) {
            return;
        }

        switch (view.getId()) {
            case R.id.cont_item:
                onPrescriptionItemClickListener.get().OnPrescriptionDetailClickListener(idx);
                break;
            case R.id.iv_edit:
                onPrescriptionItemClickListener.get().OnPrescriptionEditClickListener(idx);
                break;

        }
    }

}

