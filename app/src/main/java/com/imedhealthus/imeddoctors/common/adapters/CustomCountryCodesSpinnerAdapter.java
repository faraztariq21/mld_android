package com.imedhealthus.imeddoctors.common.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.models.Country;

import java.util.List;

import static android.support.annotation.Dimension.DP;
import static android.support.annotation.Dimension.SP;


public class CustomCountryCodesSpinnerAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<Country> items;
    private final int mResource;

    public CustomCountryCodesSpinnerAdapter(@NonNull Context context, @LayoutRes int resource,
                                            @NonNull List objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }
    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);

        TextView tvValue = view.findViewById(R.id.spinner_txt);
        tvValue.setTextSize(SP, 16);

        Country offerData = items.get(position);
        if(position == 0) {
            tvValue.setText(offerData.getCountryName());
        } else {
            tvValue.setText(offerData.getSortName() + " (" + offerData.getPhoneCode() + ")");
        }

        return view;
    }

}