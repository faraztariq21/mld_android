package com.imedhealthus.imeddoctors.common.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.doctor.models.StaticData;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class NetworkChangeReceiver extends BroadcastReceiver {

    Context context;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        this.context = context;


        if (IntentUtils.isAppRunning(context, "com.imedhealthus.imeddoctors")) {

            new WaitAsync().execute();

            /*
            final ConnectivityManager connMgr = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            final android.net.NetworkInfo wifi = connMgr
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            final android.net.NetworkInfo mobile = connMgr
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (wifi.isAvailable() || mobile.isAvailable()) {
*/

        }

    }

    public class WaitAsync extends AsyncTask {


        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (!GenericUtils.isNetworkNotAvailable(context)) {
                // Do something
                Toast.makeText(context, "Internent available", Toast.LENGTH_SHORT).show();
                try {
                    getStaticData();
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (NoClassDefFoundError noClassDefFoundError) {

                }
                Log.d("Network Available ", "Flag No 1");
            } else {
                Toast.makeText(context, "Internent not available", Toast.LENGTH_SHORT).show();

            }
        }
    }


    public void getStaticData() throws Exception {

        String userLoginId = "0";
        if (CacheManager.getInstance().isUserLoggedIn()) {
            userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        }

        WebServicesHandler.instance.getStaticData(userLoginId, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                if (response.status()) {
                    String session = response.optString("SessionId");
                    String token = response.optString("Token");
                    CacheManager.getInstance().setChatSessionId(session);
                    CacheManager.getInstance().setChatToken(token);
                    OpenTokHandler.getInstance().initializeSession();

                    JSONArray speciality = response.getJSONArray("Speciality");
                    List<StaticData> specialityList = new ArrayList<>();
                    specialityList.add(new StaticData("-1", "Speciality"));
                    if (speciality != null)
                        for (int i = 0; i < speciality.length(); i++) {

                            StaticData staticData = new StaticData(speciality.getJSONObject(i).getString("Id"), speciality.getJSONObject(i).getString("Title"));
                            specialityList.add(staticData);
                        }
                    SharedData.getInstance().setSpecialityList(specialityList);

                    List<StaticData> insuranceList = new ArrayList<>();
                    insuranceList.add(new StaticData("-1", "Select Insurace"));
                    JSONArray insurance = response.getJSONArray("Insurance");
                    if (insurance != null)
                        for (int i = 0; i < insurance.length(); i++) {

                            StaticData staticData = new StaticData(insurance.getJSONObject(i).getString("Id"), insurance.getJSONObject(i).getString("Title"));
                            insuranceList.add(staticData);
                        }
                    SharedData.getInstance().setInsuranceList(insuranceList);

                    List<StaticData> languageList = new ArrayList<>();
                    JSONArray language = response.getJSONArray("Language");
                    if (language != null)
                        for (int i = 0; i < language.length(); i++) {

                            StaticData staticData = new StaticData(language.getJSONObject(i).getString("Id"), language.getJSONObject(i).getString("Title"));
                            languageList.add(staticData);
                        }
                    SharedData.getInstance().setLanguageList(languageList);
                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

            }
        });

    }

   /* public void retryGetStaticDataAlert(Context context, String title, String message) {

        AlertDialog dialog = new AlertDialog.Builder(context, R.style.Dialog)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            getStaticData();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).setCancelable(false).create();

        dialog.show();

    }
*/
}