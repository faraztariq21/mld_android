package com.imedhealthus.imeddoctors.common.fragments;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.listeners.LocationLoaderListener;
import com.imedhealthus.imeddoctors.common.models.LocationLoader;

/**
 * Created by umair on 1/6/18.
 */

public abstract class MapBaseFragment extends OverlayFragment implements OnMapReadyCallback, LocationLoaderListener {

    protected GoogleMap googleMap;
    private Marker userMarker;
    protected boolean canZoomToUserLocation;

    protected void initHelper() {

        canZoomToUserLocation = true;
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        LocationLoader.getShared().setLocationLoaderListener(this).startLocationUpdate();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.googleMap = googleMap;
        if (LocationLoader.getShared().getCurrentLocation() != null) {
            locationReceived(LocationLoader.getShared().getCurrentLocation());
        }

    }


    @Override
    public void locationReceived(LatLng latLng) {

        if (googleMap == null) {
            return;
        }
        addUserMarker(latLng);

    }

    protected void addUserMarker(LatLng latLng) {
        if (userMarker != null) {
            userMarker.remove();
        }

        userMarker = googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("Your Location")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_green)));

        if (canZoomToUserLocation) {
            canZoomToUserLocation = false;
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        }

    }


}

