package com.imedhealthus.imeddoctors.common.adapters.GenericAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.imedhealthus.imeddoctors.common.models.SuggestionListItem;

public abstract class BaseSuggestionViewHolder<T extends SuggestionListItem> extends RecyclerView.ViewHolder {
    public BaseSuggestionViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void onBind(T item);

}
