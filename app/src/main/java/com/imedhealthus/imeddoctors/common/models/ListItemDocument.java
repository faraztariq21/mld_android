package com.imedhealthus.imeddoctors.common.models;

public interface ListItemDocument {
    public String getDocumentPath();

    public boolean isDocumentUri();

    public boolean isDocumentURL();

}
