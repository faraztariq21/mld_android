package com.imedhealthus.imeddoctors.common.listeners;

/**
 * Created by umair on 1/9/18.
 */

public interface OnBookedAppointmentItemClickListener {
    void onBookedAppointmentItemDetailClick(int idx);

    void onBookedAppointmentItemCompleteClick(int idx);

    void onBookedAppointmentItemCancelClick(int idx);

    void onBookedAppointmentItemCallClick(int idx);

    void onBookedAppointmentItemVideoCallClick(int idx);

    void onBookedAppointmentItemChatClick(int idx);

    void onBookedAppointmentItemDropDownClick(int idx);

    void onBookedAppointmentItemPatientProfileClick(int idx);

    void onBookedAppointmentItemDoctorProfileClick(int idx);


}
