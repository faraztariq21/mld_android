package com.imedhealthus.imeddoctors.common.adapters;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.listeners.OnChatItemClickListener;
import com.imedhealthus.imeddoctors.common.models.ForumQuestion;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuestionsAdapter extends RecyclerView
        .Adapter<QuestionsAdapter
        .DataObjectHolder> {
    String TAG = "QuestionsAdapter";
    private WeakReference<OnChatItemClickListener> onChatItemClickListener;
    private Context mContext;
    private List<ForumQuestion> forumQuestions;
    private WeakReference<TextView> tvNoRecords;


    public QuestionsAdapter(Context mContext, TextView tvNoRecords, OnChatItemClickListener onChatItemClickListener) {
        this.mContext = mContext;
        this.tvNoRecords = new WeakReference<>(tvNoRecords);
        this.onChatItemClickListener = new WeakReference<>(onChatItemClickListener);
        forumQuestions = new ArrayList<>();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        @BindView(R.id.cont_item)
        RelativeLayout rlView;
        @BindView(R.id.iv_profile)
        ImageView ivProfile;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.tv_message)
        TextView tvMessage;

        public DataObjectHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            rlView.setOnClickListener(this);
            tvMessage.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (onChatItemClickListener == null || onChatItemClickListener.get() == null) {
                return;
            }

            switch (view.getId()) {
                case R.id.cont_item:
                case R.id.tv_message:
                    onChatItemClickListener.get().onChatItemClick(getAdapterPosition());
                    break;
            }
        }
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_question, parent, false);
        Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "FontAwesome.ttf");
        TextView tvForward = itemView.findViewById(R.id.ic_forward);
        tvForward.setTypeface(typeface);
        OnChatItemClickListener listener = null;
        if (onChatItemClickListener.get() != null) {
            listener = onChatItemClickListener.get();
        }
        DataObjectHolder dataObjectHolder = new DataObjectHolder(itemView);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        ForumQuestion forumQuestion = forumQuestions.get(position);
        if ((forumQuestion.getName() != null && !forumQuestion.getName().equalsIgnoreCase(""))
                && !forumQuestion.isAnonymous()) {
            holder.tvName.setText(forumQuestion.getName());
        } else {
            holder.tvName.setText("Anonymous");
        }
        holder.tvMessage.setText(forumQuestion.getQuestion());
        if (forumQuestion.getCreatedDate() != null && !forumQuestion.getCreatedDate().equalsIgnoreCase("")) {
            long timeStamp = GenericUtils.getTimeDateInLong(forumQuestion.getCreatedDate());
            String timeAgo = GenericUtils.getPassedTime(timeStamp);
            holder.tvTime.setText(timeAgo);
        }
        if (forumQuestion.getProfilePicUrl() != null && !forumQuestion.getProfilePicUrl().equalsIgnoreCase("")) {
            Glide.with(ApplicationManager.getContext()).load(forumQuestion.getProfilePicUrl()).into(holder.ivProfile);
        }
    }

    public void setQuestions(List<ForumQuestion> forumQuestions) {

        this.forumQuestions = forumQuestions;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        } else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }

    public ForumQuestion getItem(int position) {
        if (forumQuestions == null) {
            return null;
        }
        return forumQuestions.get(position);
    }


    @Override
    public int getItemCount() {
        return forumQuestions == null ? 0 : forumQuestions.size();
    }


}
