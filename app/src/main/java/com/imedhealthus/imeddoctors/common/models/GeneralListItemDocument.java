package com.imedhealthus.imeddoctors.common.models;

import java.util.ArrayList;

public class GeneralListItemDocument implements ListItemDocument {
    String documentPath;
    boolean isDocumentUri;
    boolean isDocumentURL;

    public GeneralListItemDocument(String documentPath, boolean isDocumentUri, boolean isDocumentURL) {
        this.documentPath = documentPath;
        this.isDocumentUri = isDocumentUri;
        this.isDocumentURL = isDocumentURL;
    }

    public static ArrayList<GeneralListItemDocument> getGeneralListItemDocumentsFromStrings(ArrayList<String> filePaths, boolean isDocumentUri, boolean isDocumentURL) {
        ArrayList<GeneralListItemDocument> generalListItemDocuments = new ArrayList<>();
        for (String filePath : filePaths) {
            generalListItemDocuments.add(new GeneralListItemDocument(filePath, isDocumentUri, isDocumentURL));
        }
        return generalListItemDocuments;

    }

    @Override
    public String getDocumentPath() {
        return documentPath;
    }

    @Override
    public boolean isDocumentUri() {
        return isDocumentUri;
    }

    @Override
    public boolean isDocumentURL() {
        return isDocumentURL;
    }

    public static GeneralListItemDocument getGeneralListItemDocument(String path, boolean isDocumentUri, boolean isFilePath) {
        GeneralListItemDocument generalListItemImage = new GeneralListItemDocument(path, isDocumentUri, isFilePath);
        return generalListItemImage;
    }

}

