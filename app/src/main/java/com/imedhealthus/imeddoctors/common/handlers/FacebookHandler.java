package com.imedhealthus.imeddoctors.common.handlers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.imedhealthus.imeddoctors.common.listeners.OnFacebookResultsListener;


import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Arrays;

/**
 * Created by Malik Hamid on 1/5/2018.
 */

public class FacebookHandler {

    private CallbackManager facebookCallbackManager;
    private AccessToken facebookAccessToken;
    private WeakReference<OnFacebookResultsListener> onFacebookLoginListener;

    public FacebookHandler(Context context, OnFacebookResultsListener onFacebookResultsListener) {
        this.onFacebookLoginListener = new WeakReference<>(onFacebookResultsListener);
        initHelper(context);
    }

    private void initHelper(Context context) {

        FacebookSdk.sdkInitialize(context);
        facebookCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(facebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                facebookAccessToken = loginResult.getAccessToken();
                getFbUserData();
               // PreferenceUtils.setUserLoginWithFacebook(true, Constants.FACEBOOKLOGIN);
            }

            @Override
            public void onCancel() {
                if (onFacebookLoginListener == null || onFacebookLoginListener.get() == null) {
                    return;
                }
                onFacebookLoginListener.get().onFacebookLoginError("User Canceled");
            }

            @Override
            public void onError(FacebookException error) {
                if (onFacebookLoginListener == null || onFacebookLoginListener.get() == null) {
                    return;
                }
                onFacebookLoginListener.get().onFacebookLoginError(error.toString());
            }
        });

    }

    public void login(Activity activity) {

        LoginManager.getInstance().logOut();

        facebookAccessToken = AccessToken.getCurrentAccessToken();
        if (facebookAccessToken == null) {
            LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "email"));
        } else {
            getFbUserData();
        }

    }

    private void getFbUserData() {
        GraphRequest graphRequest = GraphRequest.newMeRequest(facebookAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                JSONObject userData = response.getJSONObject();

                String id = userData.optString("id");
                String fullName = userData.optString("name");
                if (onFacebookLoginListener == null || onFacebookLoginListener.get() == null) {
                    return;
                }
                onFacebookLoginListener.get().onFacebookLoginComplete(id, fullName);

            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,education,work,link,picture.type(large)");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        facebookCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

}


