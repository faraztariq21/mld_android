package com.imedhealthus.imeddoctors.common.models;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import java.io.Serializable;

/**
 * Created by Dev-iMEDHealth-X5 on 18/04/2018.
 */

public class ForumQuestion implements Serializable {
    @SerializedName("Id")
    String questionId;
    @SerializedName("ProfilePix")
    String profilePicUrl;
    @SerializedName("Name")
    String name;
    @SerializedName("Question")
    String question;
    @SerializedName("CommentCount")
    String commentCount;
    @SerializedName("CreatedDate")
    String createdDate;
    @SerializedName("LoginId")
    String loginId;
    @SerializedName("IsAnonymous")
    boolean isAnonymous;
    @Nullable
    @SerializedName("FileName")
    String fileUrl;
    @Nullable
    @SerializedName("FileType")
    String fileType;
    @Nullable
    @SerializedName("FileSize")
    String fileSize;


    public boolean isAnonymous() {
        return isAnonymous;
    }

    public void setAnonymous(boolean anonymous) {
        isAnonymous = anonymous;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(String commentCount) {
        this.commentCount = commentCount;
    }

    public String getCreatedDate() {
        String currentTime = GenericUtils.getLocalTimeInStringFromUTC(createdDate);
        return currentTime;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public String toString() {
        return getLoginId() + " " + getQuestion() + " " + getQuestionId() + " " + getName();
    }
}
