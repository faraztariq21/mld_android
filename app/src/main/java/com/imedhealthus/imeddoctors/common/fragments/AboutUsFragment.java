package com.imedhealthus.imeddoctors.common.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.imedhealthus.imeddoctors.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AboutUsFragment extends BaseFragment {

    @BindView(R.id.webView1)
    WebView webView;
    public AboutUsFragment() {
        // Required empty public constructor
    }

    @Override
    public boolean showHeader() {
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_about_us, container, false);
        ButterKnife.bind(this,view);
        initView();
        return view;
    }
    public void initView(){
        String htmlText = "<html><body style="+"text-align:"+"justify"+"> %s </body></Html>";
        String myData = getResources().getString(R.string.about_us_text2);

        webView.loadData(String.format(htmlText, myData), "text/html", "utf-8");
    }

    @Override
    public String getName() {
        return "aboutUs";
    }

    @Override
    public String getTitle() {
        return "About Us";
    }
}
