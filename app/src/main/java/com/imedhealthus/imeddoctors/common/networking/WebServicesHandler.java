package com.imedhealthus.imeddoctors.common.networking;

import android.text.TextUtils;

import com.imedhealthus.imeddoctors.common.models.BaseResponse;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.ChatMessage;
import com.imedhealthus.imeddoctors.common.models.ChatUsers;
import com.imedhealthus.imeddoctors.common.models.User;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.common.utils.Logs;
import com.imedhealthus.imeddoctors.doctor.models.AcceptedInsurance;
import com.imedhealthus.imeddoctors.doctor.models.Award;
import com.imedhealthus.imeddoctors.doctor.models.Certification;
import com.imedhealthus.imeddoctors.doctor.models.ClinicLocation;
import com.imedhealthus.imeddoctors.doctor.models.DoctorExperience;
import com.imedhealthus.imeddoctors.doctor.models.EducationDetail;
import com.imedhealthus.imeddoctors.doctor.models.Filter;
import com.imedhealthus.imeddoctors.doctor.models.Language;
import com.imedhealthus.imeddoctors.doctor.models.Membership;
import com.imedhealthus.imeddoctors.doctor.models.PersonalInfo;
import com.imedhealthus.imeddoctors.doctor.models.ReferenceDetail;
import com.imedhealthus.imeddoctors.doctor.models.Service;
import com.imedhealthus.imeddoctors.doctor.models.SkillTraining;
import com.imedhealthus.imeddoctors.doctor.models.SocialLink;
import com.imedhealthus.imeddoctors.doctor.models.Speciality;
import com.imedhealthus.imeddoctors.doctor.models.TrainingProviderDetail;
import com.imedhealthus.imeddoctors.doctor.models.WorkExperience;
import com.imedhealthus.imeddoctors.patient.models.Allergy;
import com.imedhealthus.imeddoctors.patient.models.Attachment;
import com.imedhealthus.imeddoctors.patient.models.CurrentMedication;
import com.imedhealthus.imeddoctors.patient.models.Disease;
import com.imedhealthus.imeddoctors.patient.models.EmergencyContact;
import com.imedhealthus.imeddoctors.patient.models.FamilyHistory;
import com.imedhealthus.imeddoctors.patient.models.Guardian;
import com.imedhealthus.imeddoctors.patient.models.Hobby;
import com.imedhealthus.imeddoctors.patient.models.Hospitalization;
import com.imedhealthus.imeddoctors.patient.models.Insurance;
import com.imedhealthus.imeddoctors.patient.models.PastPharmacy;
import com.imedhealthus.imeddoctors.patient.models.PastPhysician;
import com.imedhealthus.imeddoctors.patient.models.PastSurgery;
import com.imedhealthus.imeddoctors.patient.models.PregnanciesHistory;
import com.imedhealthus.imeddoctors.patient.models.PregnancyDetailsHistory;
import com.imedhealthus.imeddoctors.patient.models.Prescription;
import com.imedhealthus.imeddoctors.patient.models.SocialHistory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

//  Copyright © 2017 AirFive. All rights reserved.

public class WebServicesHandler {

    public static WebServicesHandler instance = new WebServicesHandler();

    private WebServices webServices;

    private WebServicesHandler() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(120, TimeUnit.SECONDS);
        httpClient.connectTimeout(120, TimeUnit.SECONDS);
        httpClient.writeTimeout(120, TimeUnit.SECONDS);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Constants.URLS.BaseApis)
                .addConverterFactory(RetrofitGSONConverter.create())
                .client(httpClient.build());

        Retrofit retrofit = builder.build();
        webServices = retrofit.create(WebServices.class);

    }

    public void login(String email, String password, String userType, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Username", email);
        parameters.put("Password", password);
        parameters.put("UserType", userType);
        Logs.apiCall(parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.login(parameters);
        call.enqueue(callback);

    }

    public void getOpenTokKey(final NetworkResponseListener networkResponseListener) {
        Call<BaseResponse> baseResponseCall = webServices.getOpenTokKey();
        baseResponseCall.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                networkResponseListener.onSuccess(call.toString(), response.body());
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

            }
        });

    }

    public void logout(String id, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("LoginId", id);

        Call<RetrofitJSONResponse> call = webServices.logout(parameters);
        call.enqueue(callback);

    }

    public void changePassword(String id, String password, String newPassword, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("LoginId", id);
        parameters.put("OldPassword", password);
        parameters.put("NewPassword", newPassword);
        Call<RetrofitJSONResponse> call = webServices.changePassword(parameters);
        call.enqueue(callback);

    }

    public void forgotPassword(String email, String userType, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Email", email);
        parameters.put("UserType", userType);

        Call<RetrofitJSONResponse> call = webServices.forgotPassword(parameters);
        call.enqueue(callback);

    }

    public void changeForgotPassword(String loginId, String password, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Password", password);
        parameters.put("LoginId", loginId);

        Call<RetrofitJSONResponse> call = webServices.changeForgotPassword(parameters);
        call.enqueue(callback);

    }

    public void sendFeedback(String id, String subject, String comment, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("LoginId", id);
        parameters.put("Subject", subject);
        parameters.put("Description", comment);
        Call<RetrofitJSONResponse> call = webServices.sendFeedback(parameters);
        call.enqueue(callback);
    }

    public void getSchedule(String id, String usertype, CustomCallback<RetrofitJSONResponse> callback) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("UserId", id);
        parameters.put("UserType", usertype);
        Logs.apiCall("getSchedule : " + parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.getSchedule(parameters);
        call.enqueue(callback);
    }

    public void getPatientList(String id, CustomCallback<RetrofitJSONResponse> callback) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", id);
        Logs.apiCall("getPatientList : " + parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.getPatientList(parameters);
        call.enqueue(callback);
    }

    public void addPaymentMethod(String id, String title, String detail, String cardNumber, String cardExpiringMonth, String cardExpiringYear, String cardCVC, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("LoginId", id);
        parameters.put("Title", title);
        parameters.put("Detail", detail);
        parameters.put("CardNumber", cardNumber);
        parameters.put("CardExpiryMonth", cardExpiringMonth);
        parameters.put("CardExpiryYear", cardExpiringYear);
        parameters.put("CardCVC", cardCVC);

        Call<RetrofitJSONResponse> call = webServices.addPaymentMethod(parameters);
        call.enqueue(callback);

    }

    public void updatePaymentMethod(String id, String title, String detail, String cardNumber, String cardExpiringMonth, String cardExpiringYear, String cardCVC, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", id);
        parameters.put("Title", title);
        parameters.put("Detail", detail);
        parameters.put("CardNumber", cardNumber);
        parameters.put("CardExpiryMonth", cardExpiringMonth);
        parameters.put("CardExpiryYear", cardExpiringYear);
        parameters.put("CardCVC", cardCVC);

        Call<RetrofitJSONResponse> call = webServices.updatePaymentMethod(parameters);
        call.enqueue(callback);

    }


    public void register(String userName, String prefix, String firstName, String lastName, String password,
                         String email, String userType, String imageUrl, String phone, String SpecialityId, String referralCode, boolean isItYourProfile, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("UserName", userName);
        parameters.put("Prefix", prefix);
        parameters.put("FirstName", firstName);
        parameters.put("LastName", lastName);
        parameters.put("Password", password);
        parameters.put("Email", email);
        parameters.put("UserType", userType);
        parameters.put("ImageURL", imageUrl);
        parameters.put("PhoneNo", phone);
        if (SpecialityId != null && !SpecialityId.equalsIgnoreCase(""))
            parameters.put("SpecialityId", SpecialityId);
        parameters.put("ReferralCode", referralCode);
        parameters.put("IsItyourProfile", isItYourProfile);
        Logs.apiCall("register : " + parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.register(parameters);
        call.enqueue(callback);

    }

    public void validatePhoneNumber(String phoneNumber, String countryCode, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PhoneNo", phoneNumber);
        parameters.put("CountryCode", countryCode);
        Logs.apiCall("validatePhoneNumber : " + parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.validatePhoneNumber(parameters);
        call.enqueue(callback);

    }

    public void phoneVerification(CustomCallback<RetrofitJSONResponse> callback, String loginId, String code) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("LoginId", loginId);
        parameters.put("Code", code);
        Logs.apiCall("phoneVerification : " + parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.phoneVerification(parameters);
        call.enqueue(callback);
    }

    public void resendPhoneVerificationCode(CustomCallback<RetrofitJSONResponse> callback, String loginId) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("LoginId", loginId);
        Logs.apiCall("resendPhoneVerificationCode : " + parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.resendPhoneVerificationCode(parameters);
        call.enqueue(callback);
    }

    public void manualEmail(CustomCallback<RetrofitJSONResponse> callback, String loginId, String name, String email, String type) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("LoginId", loginId);
        parameters.put("Name", name);
        parameters.put("Email", email);
        parameters.put("Type", type);
        Logs.apiCall("requestManualVerification : " + parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.manualEmail(parameters);
        call.enqueue(callback);
    }

    public void findDoctors(Filter filter, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, Object> parameters = new HashMap<>();

        parameters.put("AppointmentType", filter.isOfficeAppointment() ? "OFFICE" : "Tele");
        parameters.put("Country", filter.getCountry());
        parameters.put("City", filter.getCity());
        parameters.put("SpecialityId", filter.getSpeciality());
        parameters.put("AcceptedInsurance", filter.getInsurance());
        parameters.put("Gender", filter.getGender());
        parameters.put("MinConsultationFee", (int) filter.getMinConsultationFee());
        parameters.put("MaxConsultationFee", (int) filter.getConsultationFee());
        parameters.put("MinimumRating", (int) filter.getRating());

        parameters.put("Latitude", filter.getLatitude());
        parameters.put("Longitude", filter.getLongitude());
        parameters.put("Radius", filter.getDistance());

        Call<RetrofitJSONResponse> call = webServices.findDoctors(filter);
        call.enqueue(callback);

    }

    public void getDoctorById(String id, CustomCallback<RetrofitJSONResponse> callback) throws IllegalArgumentException {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", id);

        Call<RetrofitJSONResponse> call = webServices.getDoctorById(parameters);
        call.enqueue(callback);

    }

    public void getDoctorByLoginId(String loginId, CustomCallback<RetrofitJSONResponse> callback) throws IllegalArgumentException {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", loginId);

        Call<RetrofitJSONResponse> call = webServices.getDoctorByLoginId(parameters);
        call.enqueue(callback);

    }


    public void getPatientProfileById(String id, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", id);
        Logs.apiCall("getProfileById : " + parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.getPatientProfile(parameters);
        call.enqueue(callback);

    }

    public void getPatientProfileByLoginId(String id, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", id);
        Logs.apiCall("getProfileById : " + parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.getPatientProfileByLoginId(parameters);
        call.enqueue(callback);

    }

    public void addEditInsurance(String userId, Insurance insurance, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("PatientId", userId);

        parameters.put("SubscriberName", insurance.getSubscription());
        // parameters.put("InsuranceId", insurance.getInsuranceId());
        parameters.put("CompanyName", insurance.getCompanyName());
        parameters.put("CompanyCode", insurance.getCompanyCode());
        parameters.put("CompanyGroup", insurance.getCompanyGroup());
        parameters.put("SNN", insurance.getCompanySSN());
        parameters.put("CompanyAddress", insurance.getCompanyAddress());
        parameters.put("PhoneNo", insurance.getCompanyPhone());
        parameters.put("Notes", insurance.getInsuranceNotes());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(insurance.getId())) {
            call = webServices.addInsurance(parameters);
        } else {
            parameters.put("Id", insurance.getId());
            call = webServices.updateInsurance(parameters);
        }


        call.enqueue(callback);

    }

    public void deleteInsurance(Insurance insurance, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", insurance.getId());
        Call<RetrofitJSONResponse> call = webServices.deleteInsurance(parameters);
        call.enqueue(callback);
    }


    public void addAttachment(Attachment attachment, String userId, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();

        parameters.put("PatientId", userId);
        parameters.put("Name", attachment.getTitle());
        parameters.put("Url", attachment.getFileUrl());
        parameters.put("Title", attachment.getTitle());
        parameters.put("Date", GenericUtils.getTimeDateString(new Date(attachment.getTimeStamp())));
        parameters.put("Notes", attachment.getNotes());

        Call<RetrofitJSONResponse> call = webServices.addAttachment(parameters);
        call.enqueue(callback);

    }

    public void deleteAttachment(Attachment attachment, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", attachment.getId());
        Call<RetrofitJSONResponse> call = webServices.deleteAttachment(parameters);
        call.enqueue(callback);
    }


    public void addEditGuardian(String userId, Guardian guardian, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("PatientId", userId);

        parameters.put("GuardianName", guardian.getName());
        parameters.put("Email", guardian.getEmail());

        parameters.put("GuardianHomeContact", guardian.getHomePhone());
        parameters.put("GuardianOfficeContact", guardian.getOfficePhone());
        parameters.put("GuardianPersonalContact", guardian.getPersonalPhone());
        parameters.put("GuardianRelation", guardian.getRelation());

        parameters.put("ZidCode", guardian.getZip());
        parameters.put("PrimaryAddress", guardian.getPrimaryAddress());
        parameters.put("SecondaryAddress", guardian.getSecondaryAddress());

        parameters.put("CityId", guardian.getCityId());
        parameters.put("StateId", guardian.getStateId());
        parameters.put("CountryId", guardian.getCountryId());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(guardian.getGuardianId())) {
            call = webServices.addGuardianInfo(parameters);
        } else {
            parameters.put("Id", guardian.getGuardianId());
            call = webServices.updateGuardianInfo(parameters);
        }
        call.enqueue(callback);

    }

    public void deleteGuardian(Guardian guardian, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", guardian.getGuardianId());

        Call<RetrofitJSONResponse> call = webServices.deleteGuardianInfo(parameters);
        call.enqueue(callback);

    }


    public void updatePersonalInfo(String userId, String loginId, com.imedhealthus.imeddoctors.patient.models.PersonalInfo personalInfo, CustomCallback<RetrofitJSONResponse> callback) {


        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", userId);
        parameters.put("UserId", loginId);

        parameters.put("ProfilePix", personalInfo.getImageUrl());
        parameters.put("Title", personalInfo.getPrefix());
        parameters.put("FirstName", personalInfo.getFirstName());
        parameters.put("LastName", personalInfo.getLastName());
        parameters.put("Email", personalInfo.getEmail());

        parameters.put("DOB", personalInfo.getDob());
        parameters.put("GenderId", personalInfo.getGender());
        parameters.put("Occupation", personalInfo.getOccupation());
        parameters.put("StatusId", personalInfo.getMaritalStatus());

        parameters.put("ReligionId", personalInfo.getReligion());
        parameters.put("LanguageId", personalInfo.getLanguages());
        parameters.put("BloodGroupId", personalInfo.getBloodGroup());

        parameters.put("HomePhone", personalInfo.getHomePhone());
        parameters.put("OfficePhone", personalInfo.getOfficePhone());
        parameters.put("PersonalPhone", personalInfo.getPersonalPhone());
        parameters.put("ZipCode", personalInfo.getZip());

        parameters.put("PrimaryAddress", personalInfo.getPrimaryAddress());
        parameters.put("SecondaryAddress", personalInfo.getSecondaryAddress());
        parameters.put("CityId", personalInfo.getCityId());
        parameters.put("StateId", personalInfo.getStateId());
        parameters.put("CountryId", personalInfo.getCountryId());

        Call<RetrofitJSONResponse> call = webServices.updatePersonalInfo(parameters);
        call.enqueue(callback);

    }


    public void addEditEmergencyContact(String userId, EmergencyContact emergencyContact, CustomCallback<RetrofitJSONResponse> callback) {


        Map<String, String> parameters = new HashMap<>();
        parameters.put("PatientId", userId);

        parameters.put("Name", emergencyContact.getEmergencyContactName());
        parameters.put("Relationship", emergencyContact.getEmergencyContactRelation());
        parameters.put("ContactNo", emergencyContact.getEmergencyContactPhone());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(emergencyContact.getEmergencyContactId())) {
            call = webServices.addEmergencyContact(parameters);
        } else {
            parameters.put("Id", emergencyContact.getEmergencyContactId());
            call = webServices.updateEmergencyContact(parameters);
        }
        call.enqueue(callback);

    }

    public void deleteEmergencyContact(EmergencyContact emergencyContact, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", emergencyContact.getEmergencyContactId());

        Call<RetrofitJSONResponse> call = webServices.deleteEmergencyContact(parameters);
        call.enqueue(callback);

    }


    public void bookAppointment(String patientId, String appointmentId, String description, CustomCallback<RetrofitJSONResponse> callback) {


        Map<String, String> parameters = new HashMap<>();

        parameters.put("PatientId", patientId);
        parameters.put("AppointmentId", appointmentId);
        parameters.put("Description", description);


        Call<RetrofitJSONResponse> call = webServices.bookAppointment(parameters);
        call.enqueue(callback);

    }

    public void getUpdatedAppVersion(CustomCallback<RetrofitJSONResponse> callback) {
        Map<String, String> parameters = new HashMap<>();
        Call<RetrofitJSONResponse> call = webServices.getUpdatedAppVersion(parameters);
        call.enqueue(callback);
    }

    public void getAllPhoneCodes(String loginId, CustomCallback<RetrofitJSONResponse> callback) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("LoginId", loginId);
        Logs.apiCall(parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.getAllPhoneCodes(parameters);
        call.enqueue(callback);
    }

    public void getStaticData(String userLoginId, CustomCallback<RetrofitJSONResponse> callback) {

        if (userLoginId == null)
            userLoginId = "0";
        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", userLoginId);
        Call<RetrofitJSONResponse> call = webServices.getStaticData(parameters);
        call.enqueue(callback);
    }

    public void getInvoices(String userId, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", userId);
        Call<RetrofitJSONResponse> call = webServices.getInvoices(parameters);
        call.enqueue(callback);
    }

    public void getNotifications(User userId, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("UserId", userId.getUserId());
        parameters.put("UserType", userId.getUserType() == Constants.UserType.Patient ? "patient" : "doctor");
        Logs.apiCall("getNotifications : " + parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.getNotifications(parameters);
        call.enqueue(callback);
    }

    public void updateNotificationStatus(String userId, String userType, String notificationType, boolean isPublic, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("UserId", userId);
        parameters.put("UserType", userType);
        parameters.put("NotificationType", notificationType);
        parameters.put("IsPublic", isPublic);
        Logs.apiCall("updateNotificationStatus : " + parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.updateNotificationStatus(parameters);
        call.enqueue(callback);
    }

    public void submitDoctorFeedback(String DoctorId, String userId, String totalWaitTime, Boolean isRecommendDoctor, float easeofAppointment,
                                     float accurateDiagnosis, float followsUpAfterVisit, float promptness,
                                     float bedsideManner, float medicalKnowledge, float courteousStaff,
                                     float spendsTimewithMe, String reviewReason, String description,
                                     CustomCallback<RetrofitJSONResponse> callback) {


        Map<String, Object> parameters = new HashMap<>();
        parameters.put("DoctorId", DoctorId);
        parameters.put("CreatedBy", userId);
        parameters.put("TotalWaitTime", totalWaitTime);
        parameters.put("IsRecommendDoctor", isRecommendDoctor);
        parameters.put("EaseofAppointment", easeofAppointment);
        parameters.put("AccurateDiagnosis", accurateDiagnosis);
        parameters.put("FollowsUpAfterVisit", followsUpAfterVisit);
        parameters.put("Promptness", promptness);
        parameters.put("BedsideManner", bedsideManner);
        parameters.put("MedicalKnowledge", medicalKnowledge);
        parameters.put("CourteousStaff", courteousStaff);
        parameters.put("SpendsTimewithMe", spendsTimewithMe);
        parameters.put("ReviewReason", reviewReason);
        parameters.put("Description", description);


        Call<RetrofitJSONResponse> call = webServices.submitDoctorFeedback(parameters);
        call.enqueue(callback);
    }


    public void getDoctorsListForPatient(String id, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", id);
        Call<RetrofitJSONResponse> call = webServices.getDoctorList(parameters);
        call.enqueue(callback);
    }

    public void getDoctorsAvaiableAppointmentSlots(String id, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", id);
        Call<RetrofitJSONResponse> call = webServices.getAppointmentSlots(parameters);
        call.enqueue(callback);
    }


    //Medical history
    public void addEditDisease(String userId, Disease disease, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, Object> parameters = new HashMap<>();

        parameters.put("PatientId", userId);
        parameters.put("Name", disease.getName());
        parameters.put("Notes", disease.getNotes());

        Call<RetrofitJSONResponse> call;

        if (TextUtils.isEmpty(disease.getId())) {
            call = webServices.addDisease(parameters);
        } else {
            parameters.put("Id", disease.getId());
            call = webServices.updateDisease(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteDisease(Disease disease, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("Id", disease.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteDisease(parameters);
        call.enqueue(callback);

    }


    public void addEditMedication(String userId, CurrentMedication medication, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("PatientId", userId);

        parameters.put("Name", medication.getName());
        parameters.put("ReasonForMedicine", medication.getReason());

        parameters.put("Dose", medication.getDose());
        parameters.put("DoseFrequency", medication.getFrequency());
        parameters.put("AsNeeded", String.valueOf(medication.getAsNeeded()));

        parameters.put("DocName", medication.getDoctorName());
        parameters.put("DocNumber", medication.getDoctorPhone());

        parameters.put("PharName", medication.getPharmacyName());
        parameters.put("PharNumber", medication.getPharmacyNumber());
        parameters.put("Notes", medication.getNotes());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(medication.getId())) {
            call = webServices.addMedication(parameters);
        } else {
            parameters.put("Id", medication.getId());
            call = webServices.updateMedication(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteMedication(CurrentMedication pastMedication, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", pastMedication.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteMedication(parameters);
        call.enqueue(callback);

    }


    public void addEditPastPhysician(String userId, PastPhysician physician, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("PatientId", userId);

        parameters.put("Name", physician.getName());
        parameters.put("Location", physician.getLocation());

        parameters.put("Hospital", physician.getHospital());
        parameters.put("PhoneNo", physician.getPhone());

        parameters.put("Specialty", physician.getSpeciality());
        parameters.put("Notes", physician.getNotes());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(physician.getId())) {
            call = webServices.addPastPhysician(parameters);
        } else {
            parameters.put("Id", physician.getId());
            call = webServices.updatePastPhysician(parameters);
        }

        call.enqueue(callback);

    }

    public void deletePastPhysician(PastPhysician physician, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", physician.getId());

        Call<RetrofitJSONResponse> call = webServices.deletePastPhysician(parameters);
        call.enqueue(callback);

    }


    public void addEditPastSurgery(String userId, PastSurgery surgery, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("PatientId", userId);

        parameters.put("Disease", surgery.getDisease());
        parameters.put("SurgeryType", surgery.getType());

        parameters.put("SurgeonName", surgery.getSurgeonName());
        parameters.put("Year", surgery.getYear());
        parameters.put("Notes", surgery.getNotes());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(surgery.getId())) {
            call = webServices.addPastSurgery(parameters);
        } else {
            parameters.put("Id", surgery.getId());
            call = webServices.updatePastSurgery(parameters);
        }

        call.enqueue(callback);

    }

    public void deletePastSurgery(PastSurgery surgery, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", surgery.getId());

        Call<RetrofitJSONResponse> call = webServices.deletePastSurgery(parameters);
        call.enqueue(callback);

    }


    public void addEditAllergy(String userId, Allergy allergy, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("PatientId", userId);

        parameters.put("AllergyTo", allergy.getAllergyTo());
        parameters.put("Recation", allergy.getReaction());
        parameters.put("Notes", allergy.getNotes());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(allergy.getId())) {
            call = webServices.addAllergy(parameters);
        } else {
            parameters.put("Id", allergy.getId());
            call = webServices.updateAllergy(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteAllergy(Allergy allergy, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", allergy.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteAllergy(parameters);
        call.enqueue(callback);

    }


    public void addEditPastPharmacy(String userId, PastPharmacy pharmacy, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("PatientId", userId);

        parameters.put("Name", pharmacy.getName());
        parameters.put("PhoneNo", pharmacy.getPhoneNumber());
        parameters.put("FaxNo", pharmacy.getFaxNumber());

        parameters.put("Address", pharmacy.getAddress());
        parameters.put("CityId", pharmacy.getCityId());
        parameters.put("StateId", pharmacy.getStateId());
        parameters.put("CountryId", pharmacy.getCountryId());


        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(pharmacy.getId())) {
            call = webServices.addPastPharmacy(parameters);
        } else {
            parameters.put("Id", pharmacy.getId());
            call = webServices.updatePastPharmacy(parameters);
        }

        call.enqueue(callback);

    }

    public void deletePastPharmacy(PastPharmacy pharmacy, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", pharmacy.getId());

        Call<RetrofitJSONResponse> call = webServices.deletePastPharmacy(parameters);
        call.enqueue(callback);

    }


    public void addEditHospitalization(String userId, Hospitalization hospitalization, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("PatientId", userId);

        parameters.put("HospitalName", hospitalization.getHospitalName());
        parameters.put("PhoneNo", hospitalization.getPhone());
        parameters.put("Address", hospitalization.getAddress());

        parameters.put("illnessORinjury", hospitalization.getReason());
        parameters.put("Year", hospitalization.getYear());
        parameters.put("Notes", hospitalization.getNotes());


        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(hospitalization.getId())) {
            call = webServices.addHospitalization(parameters);
        } else {
            parameters.put("Id", hospitalization.getId());
            call = webServices.updateHospitalization(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteHospitalization(Hospitalization hospitalization, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", hospitalization.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteHospitalization(parameters);
        call.enqueue(callback);

    }


    public void addEditFamilyHistory(String userId, FamilyHistory familyHistory, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("PatientId", userId);

        parameters.put("Relation", familyHistory.getRelation());
        parameters.put("DeceasedOrDeathAge", familyHistory.getDeceasedAge());
        parameters.put("MedicalConditionsOrCauseDeath", familyHistory.getMedicationCondition());
        parameters.put("Notes", familyHistory.getNotes());


        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(familyHistory.getId())) {
            call = webServices.addFamilyHistory(parameters);
        } else {
            parameters.put("Id", familyHistory.getId());
            call = webServices.updateFamilyHistory(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteFamilyHistory(FamilyHistory familyHistory, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", familyHistory.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteFamilyHistory(parameters);
        call.enqueue(callback);

    }


    public void addEditHobby(String userId, Hobby hobby, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("PatientId", userId);
        parameters.put("Hobbies", hobby.getName());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(hobby.getId())) {
            call = webServices.addHobby(parameters);
        } else {
            parameters.put("Id", hobby.getId());
            call = webServices.updateHobby(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteHobby(Hobby hobby, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", hobby.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteHobby(parameters);
        call.enqueue(callback);

    }


    //Treatment plan
    public void getTreatmentPlanDetail(String visitId, String patientId, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PatientId", patientId);
        parameters.put("VisitId", visitId);

        Call<RetrofitJSONResponse> call = webServices.getTreatmentPlanById(parameters);
        call.enqueue(callback);

    }

    public void getTreatmentPlan(String patientId, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PatientId", patientId);

        Call<RetrofitJSONResponse> call = webServices.getTreatmentPlan(parameters);
        call.enqueue(callback);

    }

    public void addEditPrescription(Prescription prescription, CustomCallback<RetrofitJSONResponse> callback) {

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(prescription.getId())) {
            call = webServices.addPrescription(prescription);
        } else {
            call = webServices.updatePrescription(prescription);
        }
        call.enqueue(callback);

    }

    public void deletePrescription(Prescription prescription, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("Id", prescription.getId());

        Call<RetrofitJSONResponse> call = webServices.deletePrescription(parameters);
        call.enqueue(callback);

    }


    public void updateSocialHistory(SocialHistory socialHistory, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("Id", socialHistory.getSocailId());
        parameters.put("Smoking", socialHistory.getSmoking());
        parameters.put("Drugs", socialHistory.getDrugs());

        parameters.put("Alcohol", socialHistory.getAlcohol());
        parameters.put("AlcoholDrinks", socialHistory.getAlcoholDrinks());
        parameters.put("AlcoholPreferredDrink", socialHistory.getAlcoholPreferredDrink());
        parameters.put("AlcoholYearQuit", socialHistory.getAlcoholYearQuit());

        parameters.put("Caffeine", socialHistory.getCaffeine());
        parameters.put("CaffeineDrinks", socialHistory.getCaffeineDrinks());

        parameters.put("Tobacco", socialHistory.getTobacco());
        parameters.put("TobaccoType", socialHistory.getTobaccoType());
        parameters.put("TobaccoUsedYear", socialHistory.getTobaccoUsedYear());
        parameters.put("TobaccoAmountPerDay", socialHistory.getTobaccoAmountPerDay());
        parameters.put("TobaccoYearQuit", socialHistory.getTobaccoYearQuit());

        parameters.put("RecreationalDrugs", socialHistory.getRecreationalDrugs());
        parameters.put("RecreationalDrugsType", socialHistory.getRecreationalDrugsType());
        parameters.put("RecreationalDrugsAmountPerWeek", socialHistory.getRecreationalDrugsAmountPerWeek());
        parameters.put("RecreationalDrugsLastUsed", socialHistory.getRecreationalDrugsLastUsed());

        parameters.put("Exercise", socialHistory.getExercise());
        parameters.put("ExerciseType", socialHistory.getExerciseType());
        parameters.put("ExerciseNoOfDaysPerWeek", socialHistory.getExerciseNoOfDaysPerWeek());
        parameters.put("HobbiesORLeisureActivities", socialHistory.getHobbiesORLeisureActivities());
        parameters.put("LastMonthFeeling", socialHistory.getLastMonthFeeling());
        parameters.put("LastMonthPleasure", socialHistory.getLastMonthPleasure());

        Call<RetrofitJSONResponse> call = webServices.updateSocialHistory(parameters);
        call.enqueue(callback);

    }

    public void addSocialHistory(SocialHistory socialHistory, String userId, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PatientId", userId);
        parameters.put("Smoking", socialHistory.getSmoking());
        parameters.put("Drugs", socialHistory.getDrugs());

        parameters.put("Alcohol", socialHistory.getAlcohol());
        parameters.put("AlcoholDrinks", socialHistory.getAlcoholDrinks());
        parameters.put("AlcoholPreferredDrink", socialHistory.getAlcoholPreferredDrink());
        parameters.put("AlcoholYearQuit", socialHistory.getAlcoholYearQuit());

        parameters.put("Caffeine", socialHistory.getCaffeine());
        parameters.put("CaffeineDrinks", socialHistory.getCaffeineDrinks());

        parameters.put("Tobacco", socialHistory.getTobacco());
        parameters.put("TobaccoType", socialHistory.getTobaccoType());
        parameters.put("TobaccoUsedYear", socialHistory.getTobaccoUsedYear());
        parameters.put("TobaccoAmountPerDay", socialHistory.getTobaccoAmountPerDay());
        parameters.put("TobaccoYearQuit", socialHistory.getTobaccoYearQuit());

        parameters.put("RecreationalDrugs", socialHistory.getRecreationalDrugs());
        parameters.put("RecreationalDrugsType", socialHistory.getRecreationalDrugsType());
        parameters.put("RecreationalDrugsAmountPerWeek", socialHistory.getRecreationalDrugsAmountPerWeek());
        parameters.put("RecreationalDrugsLastUsed", socialHistory.getRecreationalDrugsLastUsed());

        parameters.put("Exercise", socialHistory.getExercise());
        parameters.put("ExerciseType", socialHistory.getExerciseType());
        parameters.put("ExerciseNoOfDaysPerWeek", socialHistory.getExerciseNoOfDaysPerWeek());
        parameters.put("HobbiesORLeisureActivities", socialHistory.getHobbiesORLeisureActivities());
        parameters.put("LastMonthFeeling", socialHistory.getLastMonthFeeling().equalsIgnoreCase("true"));
        parameters.put("LastMonthPleasure", socialHistory.getLastMonthPleasure().equalsIgnoreCase("true"));

        Call<RetrofitJSONResponse> call = webServices.addSocialHistory(parameters);
        call.enqueue(callback);

    }


    //Doctor profile
    public void addEditEducationDetail(String userId, EducationDetail educationDetail, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("DoctorProfileId", userId);

        parameters.put("EducationTypeId", educationDetail.getEducationType());
        parameters.put("DegreeId", educationDetail.getDegree());

        parameters.put("StartDate", educationDetail.getStartDate());
        parameters.put("EndDate", educationDetail.getEndDate());
        parameters.put("IsCompleted", String.valueOf(educationDetail.isComplete()));

        parameters.put("InstituteId", educationDetail.getInstitute());
        parameters.put("CityId", educationDetail.getCityId());
        parameters.put("StateId", educationDetail.getStateId());
        parameters.put("CountryId", educationDetail.getCountryId());

        parameters.put("Majors", educationDetail.getMajors());
        parameters.put("DegreeDescription", educationDetail.getNotes());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(educationDetail.getId())) {
            call = webServices.addDoctorEducation(parameters);
        } else {
            parameters.put("Id", educationDetail.getId());
            call = webServices.updateDoctorEducation(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteEducationDetail(EducationDetail educationDetail, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", educationDetail.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteDoctorEducation(parameters);
        call.enqueue(callback);

    }


    public void addEditSpeciality(Speciality speciality, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("DoctorProfileId", speciality.getDoctorId());

        parameters.put("Speciality", speciality.getName());

        Logs.apiCall("add/updateSpeciality : " + parameters.toString());
        Call<RetrofitJSONResponse> call;
        if (speciality.getUniqueId().equalsIgnoreCase("0")) {
            call = webServices.addSpeciality(speciality);
        } else {
            parameters.put("Id", speciality.getUniqueId());
            call = webServices.updateSpeciality(speciality);
        }

        call.enqueue(callback);

    }

    public void deleteSpeciality(Speciality speciality, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", speciality.getUniqueId());

        Call<RetrofitJSONResponse> call = webServices.deleteSpeciality(parameters);
        call.enqueue(callback);

    }


    public void addEditSkillTraining(String userId, SkillTraining skillTraining, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("DoctorProfileId", userId);

        parameters.put("EducationTypeId", skillTraining.getEducationType());
        parameters.put("SkillTitle", skillTraining.getSkill());

        parameters.put("StartDate", skillTraining.getStartDate());
        parameters.put("EndDate", skillTraining.getEndDate());

        parameters.put("InstituteId", skillTraining.getInstitute());
        parameters.put("Supervisor", skillTraining.getSupervisor());

        parameters.put("PhoneNo", skillTraining.getPhone());
        parameters.put("Address", skillTraining.getAddress());

        parameters.put("CityId", skillTraining.getCityId());
        parameters.put("StateId", skillTraining.getStateId());
        parameters.put("CountryId", skillTraining.getCountryId());

        parameters.put("Description", skillTraining.getNotes());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(skillTraining.getId())) {
            call = webServices.addDoctorSkill(parameters);
        } else {
            parameters.put("Id", skillTraining.getId());
            call = webServices.updateDoctorSkill(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteSkillTraining(SkillTraining skillTraining, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", skillTraining.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteDoctorSkill(parameters);
        call.enqueue(callback);

    }


    public void deleteWorkExperience(WorkExperience workExperience, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", workExperience.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteDoctorExperience(parameters);
        call.enqueue(callback);

    }


    public void addEditClinicLocation(String userId, ClinicLocation clinicLocation, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();

        parameters.put("DoctorProfileId", userId);

        parameters.put("StreetAddress", clinicLocation.getAddress());
        // parameters.put("PostalCode", clinicLocation.getPostalCode());
        parameters.put("City", clinicLocation.getCityName());
        parameters.put("State", clinicLocation.getStateName());
        parameters.put("Country", clinicLocation.getCountryName());

        parameters.put("Radius", String.valueOf((int) clinicLocation.getRadius()));
        parameters.put("Latitude", String.valueOf(clinicLocation.getLatitude()));
        parameters.put("Longitude", String.valueOf(clinicLocation.getLongitude()));


        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(clinicLocation.getId())) {
            call = webServices.addClinicLocation(parameters);
        } else {
            parameters.put("Id", clinicLocation.getId());
            call = webServices.updateClinicLocation(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteClinicLocation(ClinicLocation clinicLocation, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", clinicLocation.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteClinicLocation(parameters);
        call.enqueue(callback);

    }


    public void addEditService(String userId, Service service, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("DoctorProfileId", userId);

        parameters.put("Service", service.getTitle());
        parameters.put("Description", service.getDetails());
        parameters.put("Price", (int) service.getPrice());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(service.getId())) {
            call = webServices.addDoctorService(parameters);
        } else {
            parameters.put("Id", service.getId());
            call = webServices.updateDoctorService(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteService(Service service, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", service.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteDoctorService(parameters);
        call.enqueue(callback);

    }


    public void addEditReferenceDetail(String userId, ReferenceDetail referenceDetail, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("DoctorProfileId", userId);

        parameters.put("Name", referenceDetail.getName());
        parameters.put("RelationshipId", referenceDetail.getRelation());
        parameters.put("YearAssociated", String.valueOf(referenceDetail.getYears()));

        parameters.put("Email", referenceDetail.getEmail());
        parameters.put("Phone", referenceDetail.getPhone());
        parameters.put("Fax", referenceDetail.getFax());
        parameters.put("Address", referenceDetail.getAddress());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(referenceDetail.getId())) {
            call = webServices.addDoctorReference(parameters);
        } else {
            parameters.put("Id", referenceDetail.getId());
            call = webServices.updateDoctorReference(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteReferenceDetail(ReferenceDetail referenceDetail, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", referenceDetail.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteDoctorReference(parameters);
        call.enqueue(callback);

    }


    public void addEditAcceptedInsurance(String userId, AcceptedInsurance acceptedInsurance, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("DoctorProfileId", userId);

        parameters.put("InsuranceId", acceptedInsurance.getName());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(acceptedInsurance.getId())) {
            call = webServices.addDoctorAcceptedInsurance(parameters);
        } else {
            parameters.put("Id", acceptedInsurance.getId());
            call = webServices.updateDoctorAcceptedInsurance(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteAcceptedInsurance(AcceptedInsurance acceptedInsurance, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", acceptedInsurance.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteDoctorAcceptedInsurance(parameters);
        call.enqueue(callback);

    }


    public void addEditLanguage(String userId, Language language, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("DoctorProfileId", userId);

        parameters.put("LanguageId", language.getName());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(language.getId())) {
            call = webServices.addDoctorLanguage(parameters);
        } else {
            parameters.put("Id", language.getId());
            call = webServices.updateDoctorLanguage(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteLanguage(Language language, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", language.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteDoctorLanguage(parameters);
        call.enqueue(callback);

    }


    public void addEditSocialLink(String userId, SocialLink socialLink, Constants.UserType userType, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("DoctorProfileId", userId);
        parameters.put("PatientId", userId);

        parameters.put("LinkName", socialLink.getType());
        parameters.put("URL", socialLink.getUrl());

        Call<RetrofitJSONResponse> call;
        if (userType == Constants.UserType.Doctor) {
            if (TextUtils.isEmpty(socialLink.getId())) {
                call = webServices.addDoctorSocialLink(parameters);
            } else {
                parameters.put("Id", socialLink.getId());
                call = webServices.updateDoctorSocialLink(parameters);
            }

        } else {
            if (TextUtils.isEmpty(socialLink.getId())) {
                call = webServices.addSocialLink(parameters);
            } else {
                parameters.put("Id", socialLink.getId());
                call = webServices.updateSocialLink(parameters);
            }
        }

        call.enqueue(callback);

    }

    public void deleteSocialLink(SocialLink socialLink, Constants.UserType userType, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", socialLink.getId());


        Call<RetrofitJSONResponse> call = webServices.deleteDoctorSocialLink(parameters);
        if (userType == Constants.UserType.Patient)
            call = webServices.deleteSocialLink(parameters);
        call.enqueue(callback);

    }


    public void addEditTrainingProviderDetail(String userId, TrainingProviderDetail trainingProviderDetail, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("DoctorProfileId", userId);

        parameters.put("TrainingTypeId", trainingProviderDetail.getType());
        parameters.put("TrainingTitle", trainingProviderDetail.getTitle());

        parameters.put("InstituteId", trainingProviderDetail.getInstitute());
        parameters.put("Department", trainingProviderDetail.getDepartment());
        parameters.put("Supervisor", trainingProviderDetail.getSupervisor());

        parameters.put("StartDate", trainingProviderDetail.getStartDate());
        parameters.put("EndDate", trainingProviderDetail.getEndDate());

        parameters.put("PhoneNo", trainingProviderDetail.getPhone());
        parameters.put("Address", trainingProviderDetail.getAddress());

        parameters.put("CityId", trainingProviderDetail.getCityId());
        parameters.put("StateId", trainingProviderDetail.getStateId());
        parameters.put("CountryId", trainingProviderDetail.getCountryId());

        parameters.put("Description", trainingProviderDetail.getNotes());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(trainingProviderDetail.getId())) {
            call = webServices.addDoctorTrainingProvider(parameters);
        } else {
            parameters.put("Id", trainingProviderDetail.getId());
            call = webServices.updateDoctorTrainingProvider(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteTrainingProviderDetail(TrainingProviderDetail trainingProviderDetail, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", trainingProviderDetail.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteDoctorTrainingProvider(parameters);
        call.enqueue(callback);

    }


    public void addEditCertification(String userId, Certification certification, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("DoctorProfileId", userId);

        parameters.put("RegistrationId", certification.getCertificateNumber());
        parameters.put("RegistrationTitle", certification.getType());

        parameters.put("RegistrationDate", certification.getIssueDate());
        parameters.put("RegistrationEndDate", certification.getExpiryDate());

        parameters.put("StateId", certification.getStateId());
        parameters.put("CountryId", certification.getCountryId());

        parameters.put("DocPath", certification.getFileUrl());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(certification.getId())) {
            call = webServices.addDoctorCertificate(parameters);
        } else {
            parameters.put("Id", certification.getId());
            call = webServices.updateDoctorCertificate(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteCertification(Certification certification, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", certification.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteDoctorCertificate(parameters);
        call.enqueue(callback);

    }

    public void getArchivedAppointments(String id, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("DoctorProfileId", id);

        Call<RetrofitJSONResponse> call = webServices.getArchivedAppointments(parameters);
        call.enqueue(callback);

    }


    public void addEditMembership(String userId, Membership membership, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("DoctorProfileId", userId);

        parameters.put("MembershipId", membership.getDesignation());

        parameters.put("MembershipStartDate", membership.getStartDate());
        parameters.put("MembershipEndDate", membership.getEndDate());
        parameters.put("IscurrentlyMember", String.valueOf(membership.isCurrentlyMember()));

        parameters.put("CityId", membership.getCityId());
        parameters.put("StateId", membership.getStateId());
        parameters.put("CountryId", membership.getCountryId());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(membership.getId())) {
            call = webServices.addDoctorMembership(parameters);
        } else {
            parameters.put("Id", membership.getId());
            call = webServices.updateDoctorMembership(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteMembership(Membership membership, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", membership.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteDoctorMembership(parameters);
        call.enqueue(callback);

    }


    public void addEditAward(String userId, Award award, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("DoctorProfileId", userId);

        parameters.put("AwardTitle", award.getTitle());
        parameters.put("InstituteId", award.getInstitute());
        parameters.put("AwardDate", award.getDate());

        parameters.put("CityId", award.getCityId());
        parameters.put("StateId", award.getStateId());
        parameters.put("CountryId", award.getCountryId());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(award.getId())) {
            call = webServices.addDoctorAward(parameters);
        } else {
            parameters.put("Id", award.getId());
            call = webServices.updateDoctorAward(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteAward(Award award, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", award.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteDoctorAward(parameters);
        call.enqueue(callback);

    }


    public void updateDoctorPersonalInfo(String userId, PersonalInfo personalInfo, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", userId);

        parameters.put("ProfilePicPath", personalInfo.getProfileImageUrl());
        parameters.put("Prefix", personalInfo.getPrefix());
        parameters.put("FirstName", personalInfo.getFirstName());
        parameters.put("LastName", personalInfo.getLastName());
        parameters.put("SecondaryEmail", personalInfo.getSecondaryEmail());

        parameters.put("DOB", personalInfo.getDob());
        parameters.put("GenderId", personalInfo.getGender());
        parameters.put("MaritalStatusId", personalInfo.getMaritalStatus());


        parameters.put("Fax", personalInfo.getFax());
        parameters.put("HomePhone", personalInfo.getHomePhone());
        parameters.put("OfficePhone", personalInfo.getOfficePhone());
        parameters.put("PhoneNo", personalInfo.getPersonalPhone());
        parameters.put("ProfessionalSummary", personalInfo.getIntroduction());

        parameters.put("PrimaryAddress", personalInfo.getPrimaryAddress());
        parameters.put("SecondaryAddress", personalInfo.getSecondaryAddress());
        parameters.put("CityId", personalInfo.getCityId());
        parameters.put("StateId", personalInfo.getStateId());
        parameters.put("CountryId", personalInfo.getCountryId());
        parameters.put("PracticeCountryId", personalInfo.getPracticeCountryId());

        Call<RetrofitJSONResponse> call = webServices.updateDoctorPersonalInfo(parameters);
        call.enqueue(callback);

    }


    public void addEditAppointmentSlots(String appointmentId, String userId, String type, String date,
                                        String startTime, String endTime, String slotDuration, String location,
                                        String notes, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();


        parameters.put("Type", type);
        parameters.put("Date", date);
        parameters.put("StartTime", startTime);
        parameters.put("EndTime", endTime);

        parameters.put("SlotDuration", slotDuration);
        parameters.put("Location", location);
        parameters.put("Notes", notes);

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(appointmentId)) {

            parameters.put("DoctorId", userId);
            call = webServices.addAppointmentSlots(parameters);
        } else {

            parameters.put("Id", appointmentId);
            call = webServices.updateAppointmentSlots(parameters);
        }
        call.enqueue(callback);

    }

    public void deleteAppointmentSlot(String appointmentId, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("AppointmentId", appointmentId);

        Call<RetrofitJSONResponse> call = webServices.deleteAppointmentSlot(parameters);
        call.enqueue(callback);

    }

    public void undoCompleteAppointment(String appointmentId, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("BookedAppointmentId", appointmentId);

        Call<RetrofitJSONResponse> call = webServices.undoCompleteOfficeAppointment(parameters);
        call.enqueue(callback);

    }

    public void completeAppointment(String appointmentId, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("BookedAppointmentId", appointmentId);

        Call<RetrofitJSONResponse> call = webServices.completeOfficeAppointment(parameters);
        call.enqueue(callback);

    }

    public void cancelBookedAppointment(String appointmentId, String bookedId, String reason, String type, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("BookedAppointmentId", appointmentId);
        parameters.put("AppointmentId", bookedId);
        parameters.put("CancelSloatOrPatientBit", type);
        parameters.put("Reason", reason);

        Call<RetrofitJSONResponse> call = webServices.cancelBookedAppointment(parameters);
        call.enqueue(callback);

    }

    //ChatWrapper
    public void getChat(String userId, String userType, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("loginId", userId);
        Call<RetrofitJSONResponse> call = webServices.getChats(parameters);
        call.enqueue(callback);
    }

    public void getAllForums(String profileId, String isPublic, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        if (profileId != null) {
            parameters.put("ProfileId", profileId);
        }
        parameters.put("isPublic", isPublic);
        Logs.apiCall("getAllForums : " + parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.getAllForums(parameters);
        call.enqueue(callback);
    }

    public void getFilteredForums(String searchText, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Searchtxt", searchText);
        parameters.put("isPublic", "true");
        Call<RetrofitJSONResponse> call = webServices.getAllForums(parameters);
        call.enqueue(callback);
    }

    public void getPublicForumComments(String questionId, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", questionId);
        Logs.apiCall("getPublicForumComments : " + parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.getPublicForumComments(parameters);
        call.enqueue(callback);
    }

    public void postForumQuestion(String loginId, String userId, String doctorProfileId, String specialityId, String question, String isAnonymous, String isPublic, String fileUrl, String fileType, String fileSize, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("LoginId", loginId);
        parameters.put("CreatedBy", userId);
        parameters.put("DoctorProfileId", doctorProfileId);
        parameters.put("SpecialityId", specialityId);
        parameters.put("Question", question);
        parameters.put("IsAnonymous", isAnonymous);
        parameters.put("IsPublic", isPublic);
        parameters.put("FileName", fileUrl != null ? fileUrl : "null");

        parameters.put("FileType", fileUrl != null ? "message" : fileType);
        parameters.put("FileSize", fileSize);

        Logs.apiCall("postForumQuestion : " + parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.postForumQuestion(parameters);
        call.enqueue(callback);
    }

    public void postForumComment(String questionId, String loginId, String profileId, String comment, String isDoctor, String fileName, String fileType, String fileSize, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("QuestionId", questionId);
        parameters.put("LoginId", loginId);
        parameters.put("CreatedBy", profileId);
        parameters.put("Comments", comment);
        parameters.put("IsDoctor", isDoctor);
        parameters.put("FileName", fileName != null ? fileName : "null");
        parameters.put("FileType", fileType);
        parameters.put("FileSize", fileSize);
        Logs.apiCall(parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.postForumComment(parameters);
        call.enqueue(callback);
    }

    public void updateForumComment(String commentId, String questionId, String loginId, String profileId, String comment, String isDoctor, String fileName, String fileType, String fileSize, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", commentId);
        parameters.put("QuestionId", questionId);
        parameters.put("LoginId", loginId);
        parameters.put("CreatedBy", profileId);
        parameters.put("Comments", comment);
        parameters.put("IsDoctor", isDoctor);
        parameters.put("FileName", fileName != null ? fileName : "null");
        parameters.put("FileType", fileType);
        parameters.put("FileSize", fileSize);
        Logs.apiCall(parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.updateForumComment(parameters);
        call.enqueue(callback);
    }

    public void deleteComment(String questionId, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", questionId);
        Logs.apiCall(parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.deleteForumComment(parameters);
        call.enqueue(callback);
    }

    public void getDoctorQuestion(String loginId, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("LoginId", loginId);
        Call<RetrofitJSONResponse> call = webServices.getDoctorQuestion(parameters);
        call.enqueue(callback);
    }

    public void getDoctorQuestionComments(String questionId, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("QuestionId", questionId);
        Call<RetrofitJSONResponse> call = webServices.getDoctorQuestionComments(parameters);
        call.enqueue(callback);
    }

    public void sendChatMessage(ChatMessage chatItems, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("LoginId", chatItems.getLoginId());
        parameters.put("ChatId", chatItems.getChatId());
        parameters.put("Message", chatItems.getMsg());
        parameters.put("RecieverLoginId", chatItems.getReceiverLoginId());
        parameters.put("FileName", chatItems.getImageUrl() != null ? chatItems.getImageUrl() : "null");
        parameters.put("FileType", chatItems.getFileType());
        parameters.put("FileSize", chatItems.getFileSize() + "");
        Logs.apiCall("SendNewMessage : " + parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.sendNewMessage(parameters);
        call.enqueue(callback);
    }

    public void getReadCount(String receiverId, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("ReciverId", receiverId);
        Logs.apiCall("getReadCount : " + parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.getReadCount(parameters);
        call.enqueue(callback);
    }

    public void updateReadCount(String senderLoginId, String receiverLoginId, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("SenderId", senderLoginId);
        parameters.put("ReciverId", receiverLoginId);
        Logs.apiCall("updateReadCount : " + parameters.toString());
        Call<RetrofitJSONResponse> call = webServices.updateReadCount(parameters);
        call.enqueue(callback);
    }

    public void notifiyUserForCall(String loginId, String message, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("LoginId", loginId);
        parameters.put("Message", message);

        Call<RetrofitJSONResponse> call = webServices.notifiyUserForCall(parameters);
        call.enqueue(callback);
    }

    public void changeUserStatus(String loginId, Boolean setOnline, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("LoginId", loginId);
        parameters.put("LoginStatus", setOnline);

        Call<RetrofitJSONResponse> call = webServices.changeUserStatus(parameters);
        call.enqueue(callback);
    }

    public void startChat(ChatUsers users, CustomCallback<RetrofitJSONResponse> callback) {

        Call<RetrofitJSONResponse> call = webServices.startNewChat(users);
        call.enqueue(callback);

    }

    public void getChatMessages(String chatId, String page, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("ChatId", chatId);
        parameters.put("PageNo", page);
        Call<RetrofitJSONResponse> call = webServices.getChatMEssages(parameters);
        call.enqueue(callback);
    }

    public void notifyUserForCall(String userLoginId, String partnerName, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("LoginId", userLoginId);
        parameters.put("PartnerName", partnerName);

        Call<RetrofitJSONResponse> call = webServices.notifyUserForCall(parameters);
        call.enqueue(callback);

    }

    public void updateDeviceToken(String userLoginId, String deviceToken, CustomCallback<RetrofitJSONResponse> callback) {

        if (userLoginId == null)
            userLoginId = "0";

        Map<String, String> parameters = new HashMap<>();
        parameters.put("LoginId", userLoginId);
        parameters.put("MobileToken", deviceToken);

        Call<RetrofitJSONResponse> call = webServices.updateDeviceToken(parameters);
        call.enqueue(callback);

    }

    public void getDataForCallWithoutNotification(String userLoginId, String partnerLoginId, String myName, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();

        if (userLoginId == null)
            userLoginId = "0";

        parameters.put("MyLoginId", userLoginId);
        parameters.put("PartnerLoginId", partnerLoginId);
        parameters.put("MyName", myName);
        parameters.put("MyImage", CacheManager.getInstance().getCurrentUser().getImageUrl());
        Call<RetrofitJSONResponse> call = webServices.getDataForCallWithNoNOtification(parameters);
        call.enqueue(callback);

    }

    public void getDataForCallWithNotification(String userLoginId, String partnerLoginId, String myName, String callType, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();

        if (userLoginId == null)
            userLoginId = "0";

        parameters.put("MyLoginId", userLoginId);
        parameters.put("PartnerLoginId", partnerLoginId);
        parameters.put("MyName", myName);
        parameters.put("CallType", callType);
        parameters.put("MyImage", CacheManager.getInstance().getCurrentUser().getImageUrl());
        Call<RetrofitJSONResponse> call = webServices.getDataForCall(parameters);
        call.enqueue(callback);

    }


    //Send to Pharmacy
    public void getPharmacies(CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", "0");
        Call<RetrofitJSONResponse> call = webServices.getPharmacies(parameters);
        call.enqueue(callback);
    }

    public void sendToPharmacy(Prescription prescription, CustomCallback<RetrofitJSONResponse> callback) {

        Call<RetrofitJSONResponse> call = webServices.sendToPharmacy(prescription);
        call.enqueue(callback);
    }


    //----------------------Pregnancy Apis-----------------------------------///

    public void addMHPregnanciesHistory(String userId, PregnanciesHistory pregnanciesHistory, CustomCallback<RetrofitJSONResponse> callback) {


        Map<String, String> parameters = new HashMap<>();
        parameters.put("PatientId", userId);

        parameters.put("Mammogram", GenericUtils.getTimeDateString(new Date(pregnanciesHistory.getMammogram())));
        parameters.put("BreastExam", GenericUtils.getTimeDateString(new Date(pregnanciesHistory.getBreastExam())));

        parameters.put("PapSmear", GenericUtils.getTimeDateString(new Date(pregnanciesHistory.getPapSmear())));
        parameters.put("Contraception", pregnanciesHistory.getContraceptionType());
        parameters.put("ContraceptionDetail", pregnanciesHistory.getUseContraception());
        parameters.put("FirstMensesAge", pregnanciesHistory.getAgeAtFirstMenses());

        parameters.put("MenstrualPeriods", pregnanciesHistory.getMenstrualPeriod());
        parameters.put("MenopauseAge", pregnanciesHistory.getAgeAtMenopause());
        parameters.put("HotFlashesOrOther", pregnanciesHistory.getSymptoms());

        parameters.put("GynecologicalConditions", pregnanciesHistory.getGynecologicalCondition());
        parameters.put("Notes", pregnanciesHistory.getNotes());


        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(pregnanciesHistory.getId())) {
            call = webServices.updateMHPregnanciesHistory(parameters);
        } else {
            parameters.put("Id", pregnanciesHistory.getId());
            call = webServices.createMHPregnanciesHistory(parameters);
        }
        call.enqueue(callback);

    }

    public void addPregnancyDetail(String userId, PregnancyDetailsHistory pregnancyDetailsHistory, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("PatientId", userId);


        parameters.put("DeliveryDate", GenericUtils.getTimeDateString(new Date(pregnancyDetailsHistory.getDeliveryDate())));
        parameters.put("DeliveryType", pregnancyDetailsHistory.getDeliveryType());

        parameters.put("Gender", pregnancyDetailsHistory.getGender());
        parameters.put("Complications", pregnancyDetailsHistory.getComplications());
        parameters.put("NoOfPregnancies", pregnancyDetailsHistory.getNumberOfPregnancies());
        parameters.put("NoOfLivingChildren", pregnancyDetailsHistory.getNumberOfLivingChild());
        parameters.put("NoOfAbortions", pregnancyDetailsHistory.getNumberOfAbortions());
        parameters.put("NoOfMiscarriages", pregnancyDetailsHistory.getNumberOfMiscarriages());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(pregnancyDetailsHistory.getId())) {
            call = webServices.createMHDetailPregnanciesHistory(parameters);
        } else {
            parameters.put("Id", pregnancyDetailsHistory.getId());
            call = webServices.updateMHDetailPregnanciesHistory(parameters);
        }

        call.enqueue(callback);

    }

    public void deletePregnancyDetail(PregnancyDetailsHistory pregnancyDetailsHistory, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", pregnancyDetailsHistory.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteMHDetailPregnanciesHistory(parameters);
        call.enqueue(callback);

    }

    public void addDoctorExperience(String userId, WorkExperience doctorExperience, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("DoctorProfileId", userId);


        parameters.put("JobTitle", doctorExperience.getJobTitle());
        parameters.put("StartDate", doctorExperience.getStartDate());

        parameters.put("EndDate", doctorExperience.getEndDate());
        parameters.put("IsCurrentlyWorking", doctorExperience.isCurrentJob());
        parameters.put("OrganizationName", doctorExperience.getOrganizationName());
        parameters.put("SupervisorName", doctorExperience.getSupervisor());
        parameters.put("Email", doctorExperience.getEmail());
        parameters.put("Address", doctorExperience.getAddress());
        parameters.put("CountryId", doctorExperience.getCountryId());
        parameters.put("StateId", doctorExperience.getStateId());
        parameters.put("CityId", doctorExperience.getCityId());
        parameters.put("ContactNo", doctorExperience.getPhone());
        parameters.put("Fax", doctorExperience.getFax());

        Call<RetrofitJSONResponse> call;
        if (TextUtils.isEmpty(doctorExperience.getId())) {
            call = webServices.addDoctorExperience(parameters);
        } else {
            parameters.put("Id", doctorExperience.getId());
            call = webServices.updateDoctorExperience(parameters);
        }

        call.enqueue(callback);

    }

    public void deleteDoctorExperience(DoctorExperience doctorExperience, CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("Id", doctorExperience.getId());

        Call<RetrofitJSONResponse> call = webServices.deleteDoctorExperience(parameters);
        call.enqueue(callback);

    }


}
