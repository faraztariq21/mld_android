package com.imedhealthus.imeddoctors.common.services;

import android.os.AsyncTask;

import com.imedhealthus.imeddoctors.common.listeners.TimerListener;


/**
 * Created by Shehryar Zaheer on 9/15/2018.
 */

public class Timer extends AsyncTask<Long, Long, Long> {

    boolean stopTimer;
    TimerListener timerListener;
    long startingSeconds;
    private boolean isRunning;

    public Timer(long startingSeconds, TimerListener timerListener) {
        this.startingSeconds = startingSeconds;
        this.timerListener = timerListener;
        isRunning = false;
        stopTimer = false;
    }

    public void startTimer() {
        stopTimer = false;
        try {
            this.execute(startingSeconds);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void stopTimer() {
        stopTimer = true;
        this.cancel(false);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        isRunning = true;
    }

    @Override
    protected Long doInBackground(Long... longs) {
        if (!stopTimer) {
            try {
                Thread.sleep(1000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                ++startingSeconds;
                publishProgress(startingSeconds);
                doInBackground(startingSeconds);
            }
        }
        return startingSeconds;
    }

    @Override
    protected void onProgressUpdate(Long... values) {
        super.onProgressUpdate(values);
        timerListener.onTimeUpdated(values[0]);

    }

    @Override
    protected void onPostExecute(Long value) {
        super.onPostExecute(value);

        isRunning = false;
    }


    public boolean isRunning() {
        return isRunning;
    }


}
