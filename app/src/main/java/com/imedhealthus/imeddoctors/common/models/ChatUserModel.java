package com.imedhealthus.imeddoctors.common.models;

import com.google.gson.annotations.SerializedName;
import com.imedhealthus.imeddoctors.common.utils.Constants;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Malik Hamid on 2/11/2018.
 */

public class ChatUserModel implements Serializable {

    @SerializedName("LoginId")
    private String id;
    private String mrNumber;

    private String name,imgURL;


    public ChatUserModel() {
    }

    public ChatUserModel(String id, String type) {
        this.id = id;

    }

    public ChatUserModel(JSONObject jsonObject) {
        this.id = jsonObject.optString("LoginId");
        this.mrNumber = jsonObject.optString("MRNumber");
        this.name = jsonObject.optString("Name");
        this.imgURL = Constants.URLS.BaseApis+jsonObject.optString("ImageUrl").replace("..","");
    }
    static ChatUserModel getUserByLoginId(List<ChatUserModel> userModels,String id){
        for (int i=0;i<userModels.size();i++){
            if(userModels.get(i).getId()==id)
                return userModels.get(i);
        }
        return null;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMrNumber() {
        return mrNumber;
    }

    public void setMrNumber(String mrNumber) {
        this.mrNumber = mrNumber;
    }
}
