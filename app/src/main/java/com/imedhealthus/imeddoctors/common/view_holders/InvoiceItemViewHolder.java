package com.imedhealthus.imeddoctors.common.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.models.Invoice;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 1/10/2018.
 */

public class InvoiceItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_amount)
    TextView tvAmount;

    public InvoiceItemViewHolder(View view) {

        super(view);
        ButterKnife.bind(this, view);

    }

    public void setData(Invoice invoice) {

        tvTitle.setText(invoice.getInvoiceTitle());
        tvAmount.setText(String.format("$%.2f", invoice.getAmount()));

    }

}

