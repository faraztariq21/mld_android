package com.imedhealthus.imeddoctors.common.networking;

import com.imedhealthus.imeddoctors.common.models.BaseResponse;
import com.imedhealthus.imeddoctors.common.models.ChatUsers;
import com.imedhealthus.imeddoctors.doctor.models.Filter;
import com.imedhealthus.imeddoctors.doctor.models.Speciality;
import com.imedhealthus.imeddoctors.patient.models.Prescription;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

//  Copyright © 2017 AirFive. All rights reserved.

public interface WebServices {

    @FormUrlEncoded
    @POST("/api/Login/Login")
    Call<RetrofitJSONResponse> login(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Login/Logout")
    Call<RetrofitJSONResponse> logout(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Login/ChangePassword")
    Call<RetrofitJSONResponse> changePassword(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Login/ForgotPassword")
    Call<RetrofitJSONResponse> forgotPassword(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Login/ChangeForgotPassword")
    Call<RetrofitJSONResponse> changeForgotPassword(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Login/AppFeedback")
    Call<RetrofitJSONResponse> sendFeedback(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Login/AddPaymentMethod")
    Call<RetrofitJSONResponse> addPaymentMethod(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Login/UpdatePaymentMethod")
    Call<RetrofitJSONResponse> updatePaymentMethod(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Login/Register")
    Call<RetrofitJSONResponse> register(@FieldMap Map<String, Object> parameters);

    @FormUrlEncoded

    @POST("/api/Login/ValidatePhoneNo")
    Call<RetrofitJSONResponse> validatePhoneNumber(@FieldMap Map<String, Object> parameters);

    @FormUrlEncoded

    @POST("/api/Login/PhoneVerification")
    Call<RetrofitJSONResponse> phoneVerification(@FieldMap Map<String, Object> parameters);

    @FormUrlEncoded
    @POST("/api/Login/ResendPhoneVerificationCode")
    Call<RetrofitJSONResponse> resendPhoneVerificationCode(@FieldMap Map<String, Object> parameters);

    @FormUrlEncoded
    @POST("/api/Login/ManualEmail")
    Call<RetrofitJSONResponse> manualEmail(@FieldMap Map<String, Object> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/FindDoctor")
    Call<RetrofitJSONResponse> findDoctors(@FieldMap Map<String, Object> parameters);

    @Headers("Content-Type: application/json")
    @POST("/api/Doctor/FindDoctor")
    Call<RetrofitJSONResponse> findDoctors(@Body Filter filter);

    @GET("/api/Chat/GetOpentokKey")
    Call<BaseResponse> getOpenTokKey();

    @FormUrlEncoded
    @POST("/api/Doctor/GetSchedule")
    Call<RetrofitJSONResponse> getSchedule(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Patient/SaveMobilePatientSocialLink")
    Call<RetrofitJSONResponse> addSocialLink(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/SaveMobilePatientSocialLink")
    Call<RetrofitJSONResponse> updateSocialLink(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/DeleteMobilePatientSocialLink")
    Call<RetrofitJSONResponse> deleteSocialLink(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/GetPatientsList")
    Call<RetrofitJSONResponse> getPatientList(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/GetDoctorProfileByDocId")
    Call<RetrofitJSONResponse> getDoctorById(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/GetDoctorProfileByLoginId")
    Call<RetrofitJSONResponse> getDoctorByLoginId(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Patient/AddInsurance")
    Call<RetrofitJSONResponse> addInsurance(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Login/GetUpdatedAppVersion")
    Call<RetrofitJSONResponse> getUpdatedAppVersion(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Login/GetAllPhoneCodes")
    Call<RetrofitJSONResponse> getAllPhoneCodes(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/GetStaticData")
    Call<RetrofitJSONResponse> getStaticData(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/UpdateInsurance")
    Call<RetrofitJSONResponse> updateInsurance(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/DeleteInsurance")
    Call<RetrofitJSONResponse> deleteInsurance(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/AddAttachment")
    Call<RetrofitJSONResponse> addAttachment(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/DeleteAttachment")
    Call<RetrofitJSONResponse> deleteAttachment(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/UpdatePersonalInfo")
    Call<RetrofitJSONResponse> updatePersonalInfo(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Patient/AddGuardianInfo")
    Call<RetrofitJSONResponse> addGuardianInfo(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/UpdateGuardianInfo")
    Call<RetrofitJSONResponse> updateGuardianInfo(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/DeleteGuardianInfo")
    Call<RetrofitJSONResponse> deleteGuardianInfo(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Patient/AddEmergencyContact")
    Call<RetrofitJSONResponse> addEmergencyContact(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/UpdateEmergencyContact")
    Call<RetrofitJSONResponse> updateEmergencyContact(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/DeleteEmergencyContact")
    Call<RetrofitJSONResponse> deleteEmergencyContact(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Doctor/BookAppointment")
    Call<RetrofitJSONResponse> bookAppointment(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/GetProfileById")
    Call<RetrofitJSONResponse> getPatientProfile(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Patient/GetPatientProfileByLoginId")
    Call<RetrofitJSONResponse> getPatientProfileByLoginId(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/GetInvoices")
    Call<RetrofitJSONResponse> getInvoices(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Login/GetNotification")
    Call<RetrofitJSONResponse> getNotifications(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Login/UpdateNotification")
    Call<RetrofitJSONResponse> updateNotificationStatus(@FieldMap Map<String, Object> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/SubmitFeedback")
    Call<RetrofitJSONResponse> submitDoctorFeedback(@FieldMap Map<String, Object> parameters);


    //Medical history
    @FormUrlEncoded
    @POST("/api/Patient/AddPastMedicalHistory")
    Call<RetrofitJSONResponse> addDisease(@FieldMap Map<String, Object> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/UpdatePastMedicalHistory")
    Call<RetrofitJSONResponse> updateDisease(@FieldMap Map<String, Object> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/DeletePastMedicalHistory")
    Call<RetrofitJSONResponse> deleteDisease(@FieldMap Map<String, Object> parameters);


    @FormUrlEncoded
    @POST("/api/Patient/AddPastMedication")
    Call<RetrofitJSONResponse> addMedication(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/UpdatePastMedication")
    Call<RetrofitJSONResponse> updateMedication(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/DeletePastMedication")
    Call<RetrofitJSONResponse> deleteMedication(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Patient/AddMyPhysician")
    Call<RetrofitJSONResponse> addPastPhysician(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/UpdateMyPhysician")
    Call<RetrofitJSONResponse> updatePastPhysician(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/DeleteMyPhysician")
    Call<RetrofitJSONResponse> deletePastPhysician(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Patient/AddSurgicalHistory")
    Call<RetrofitJSONResponse> addPastSurgery(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/UpdateSurgicalHistory")
    Call<RetrofitJSONResponse> updatePastSurgery(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/DeleteSurgicalHistory")
    Call<RetrofitJSONResponse> deletePastSurgery(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Patient/AddAllergyHistory")
    Call<RetrofitJSONResponse> addAllergy(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/UpdateAllergyHistory")
    Call<RetrofitJSONResponse> updateAllergy(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/DeleteAllergyHistory")
    Call<RetrofitJSONResponse> deleteAllergy(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Patient/AddPharmacyInfo")
    Call<RetrofitJSONResponse> addPastPharmacy(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/UpdatePharmacyInfo")
    Call<RetrofitJSONResponse> updatePastPharmacy(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/DeletePharmacyInfo")
    Call<RetrofitJSONResponse> deletePastPharmacy(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/GetPharmacies")
    Call<RetrofitJSONResponse> getPharmacies(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Patient/AddHospitalizationInfo")
    Call<RetrofitJSONResponse> addHospitalization(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/UpdateHospitalizationInfo")
    Call<RetrofitJSONResponse> updateHospitalization(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/DeleteHospitalizationInfo")
    Call<RetrofitJSONResponse> deleteHospitalization(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Patient/AddFamilyHistory")
    Call<RetrofitJSONResponse> addFamilyHistory(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/UpdateFamilyHistory")
    Call<RetrofitJSONResponse> updateFamilyHistory(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/DeleteFamilyHistory")
    Call<RetrofitJSONResponse> deleteFamilyHistory(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Patient/AddPatientHobbiesHistory")
    Call<RetrofitJSONResponse> addHobby(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/UpdatePatientHobbiesHistory")
    Call<RetrofitJSONResponse> updateHobby(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/DeletePatientHobbiesHistory")
    Call<RetrofitJSONResponse> deleteHobby(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/GetDoctorsList")
    Call<RetrofitJSONResponse> getDoctorList(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/GetAppointmentSlots")
    Call<RetrofitJSONResponse> getAppointmentSlots(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Doctor/GetTreatmentPlanByVisitId")
    Call<RetrofitJSONResponse> getTreatmentPlanById(@FieldMap Map<String, Object> parameters);

    //Treatment plan
    @FormUrlEncoded
    @POST("/api/Doctor/GetTreatmentPlan")
    Call<RetrofitJSONResponse> getTreatmentPlan(@FieldMap Map<String, Object> parameters);

    @Headers("Content-Type: application/json")
    @POST("/api/Doctor/AddTreatmentPlan")
    Call<RetrofitJSONResponse> addPrescription(@Body Prescription prescription);

    @Headers("Content-Type: application/json")
    @POST("/api/Doctor/UpdateTreatmentPlan")
    Call<RetrofitJSONResponse> updatePrescription(@Body Prescription prescription);

    @FormUrlEncoded
    @POST("/api/Doctor/DeleteTreatmentPlan")
    Call<RetrofitJSONResponse> deletePrescription(@FieldMap Map<String, Object> parameters);

    @Headers("Content-Type: application/json")
    @POST("/api/Doctor/SendToPharmacy")
    Call<RetrofitJSONResponse> sendToPharmacy(@Body Prescription prescription);

    //Social History
    @FormUrlEncoded
    @POST("/api/Patient/AddSocialHistory")
    Call<RetrofitJSONResponse> addSocialHistory(@FieldMap Map<String, Object> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/UpdateSocialHistory")
    Call<RetrofitJSONResponse> updateSocialHistory(@FieldMap Map<String, Object> parameters);

    //Doctor profile
    @Headers("Content-Type: application/json")
    @POST("/api/Doctor/AddSpeciality")
    Call<RetrofitJSONResponse> addSpeciality(@Body Speciality speciality);


    @Headers("Content-Type: application/json")
    @POST("/api/Doctor/UpdateSpeciality")
    Call<RetrofitJSONResponse> updateSpeciality(@Body Speciality speciality);

    @FormUrlEncoded
    @POST("/api/Doctor/DeleteSpeciality")
    Call<RetrofitJSONResponse> deleteSpeciality(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Doctor/AddDoctorLocation")
    Call<RetrofitJSONResponse> addClinicLocation(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/UpdateDoctorLocation")
    Call<RetrofitJSONResponse> updateClinicLocation(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/DeleteDoctorLocation")
    Call<RetrofitJSONResponse> deleteClinicLocation(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Doctor/UpdateDoctorProfile")
    Call<RetrofitJSONResponse> updateDoctorPersonalInfo(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Patient/updateMHPregnanciesHistory")
    Call<RetrofitJSONResponse> updateMHPregnanciesHistory(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Patient/UpdateMHPregnanciesHistory")
    Call<RetrofitJSONResponse> createMHPregnanciesHistory(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/AddMHDetailPregnanciesHistory")
    Call<RetrofitJSONResponse> createMHDetailPregnanciesHistory(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/DeleteMHDetailPregnanciesHistory")
    Call<RetrofitJSONResponse> deleteMHDetailPregnanciesHistory(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Patient/UpdateMHDetailPregnanciesHistory")
    Call<RetrofitJSONResponse> updateMHDetailPregnanciesHistory(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Doctor/AddApiAppointment")
    Call<RetrofitJSONResponse> addAppointmentSlots(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/UpdateApiAppointment")
    Call<RetrofitJSONResponse> updateAppointmentSlots(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/DeleteDoctorAppointment")
    Call<RetrofitJSONResponse> deleteAppointmentSlot(@FieldMap Map<String, String> parameters);


    //Chat
    @Headers("Content-Type: application/json")
    @POST("/api/Chat/StartNewChat")
    Call<RetrofitJSONResponse> startNewChat(@Body ChatUsers parameters);

    @FormUrlEncoded
    @POST("/api/Chat/SendNewMessage")
    Call<RetrofitJSONResponse> sendNewMessage(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Chat/GetReadCount")
    Call<RetrofitJSONResponse> getReadCount(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Chat/UpdateReadCount")
    Call<RetrofitJSONResponse> updateReadCount(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Chat/NotifiyUserForCall")
    Call<RetrofitJSONResponse> notifiyUserForCall(@FieldMap Map<String, String> parameters);


    @FormUrlEncoded
    @POST("/api/Login/ChangeUserStatus")
    Call<RetrofitJSONResponse> changeUserStatus(@FieldMap Map<String, Object> parameters);

    @FormUrlEncoded
    @POST("/api/Chat/GetChats")
    Call<RetrofitJSONResponse> getChats(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Forum/GetAllForums")
    Call<RetrofitJSONResponse> getAllForums(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Forum/GetPublicForumComments")
    Call<RetrofitJSONResponse> getPublicForumComments(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Forum/PostForumQuestion")
    Call<RetrofitJSONResponse> postForumQuestion(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Forum/PostForumComment")
    Call<RetrofitJSONResponse> postForumComment(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Forum/UpdateForumComment")
    Call<RetrofitJSONResponse> updateForumComment(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Forum/DeleteForumComment")
    Call<RetrofitJSONResponse> deleteForumComment(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Forum/GetDoctorQuestion")
    Call<RetrofitJSONResponse> getDoctorQuestion(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Forum/GetDoctorQuestionComments")
    Call<RetrofitJSONResponse> getDoctorQuestionComments(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Chat/GetChatMessages")
    Call<RetrofitJSONResponse> getChatMEssages(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Chat/GetChatMessages")
    Call<RetrofitJSONResponse> notifyUserForCall(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Login/UpdateMobileToken")
    Call<RetrofitJSONResponse> updateDeviceToken(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Chat/StartVideoChat")
    Call<RetrofitJSONResponse> getDataForCall(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Chat/StartVideoChatWithoutNotification")
    Call<RetrofitJSONResponse> getDataForCallWithNoNOtification(@FieldMap Map<String, String> parameters);

    //----Doctor Profile Apis-------------//

    @FormUrlEncoded
    @POST("/api/Doctor/AddDoctorExperience")
    Call<RetrofitJSONResponse> addDoctorExperience(@FieldMap Map<String, Object> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/UpdateDoctorExperience")
    Call<RetrofitJSONResponse> updateDoctorExperience(@FieldMap Map<String, Object> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/DeleteDoctorExperience")
    Call<RetrofitJSONResponse> deleteDoctorExperience(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/AddDoctorEducation")
    Call<RetrofitJSONResponse> addDoctorEducation(@FieldMap Map<String, Object> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/UpdateDoctorEducation")
    Call<RetrofitJSONResponse> updateDoctorEducation(@FieldMap Map<String, Object> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/DeleteDoctorEducation")
    Call<RetrofitJSONResponse> deleteDoctorEducation(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/AddDoctorLanguage")
    Call<RetrofitJSONResponse> addDoctorLanguage(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/UpdateDoctorLanguage")
    Call<RetrofitJSONResponse> updateDoctorLanguage(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/DeleteDoctorLanguage")
    Call<RetrofitJSONResponse> deleteDoctorLanguage(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/AddDoctorAward")
    Call<RetrofitJSONResponse> addDoctorAward(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/UpdateDoctorAward")
    Call<RetrofitJSONResponse> updateDoctorAward(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/DeleteDoctorAward")
    Call<RetrofitJSONResponse> deleteDoctorAward(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/AddDoctorMembership")
    Call<RetrofitJSONResponse> addDoctorMembership(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/UpdateDoctorMembership")
    Call<RetrofitJSONResponse> updateDoctorMembership(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/DeleteDoctorMembership")
    Call<RetrofitJSONResponse> deleteDoctorMembership(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/AddDoctorSkill")
    Call<RetrofitJSONResponse> addDoctorSkill(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/UpdateDoctorSkill")
    Call<RetrofitJSONResponse> updateDoctorSkill(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/DeleteDoctorSkill")
    Call<RetrofitJSONResponse> deleteDoctorSkill(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/AddDoctorTrainingProvider")
    Call<RetrofitJSONResponse> addDoctorTrainingProvider(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/UpdateDoctorTrainingProvider")
    Call<RetrofitJSONResponse> updateDoctorTrainingProvider(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/DeleteDoctorTrainingProvider")
    Call<RetrofitJSONResponse> deleteDoctorTrainingProvider(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/AddDoctorAcceptedInsurance")
    Call<RetrofitJSONResponse> addDoctorAcceptedInsurance(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/UpdateDoctorAcceptedInsurance")
    Call<RetrofitJSONResponse> updateDoctorAcceptedInsurance(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/DeleteDoctorAcceptedInsurance")
    Call<RetrofitJSONResponse> deleteDoctorAcceptedInsurance(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/AddDoctorReference")
    Call<RetrofitJSONResponse> addDoctorReference(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/UpdateDoctorReference")
    Call<RetrofitJSONResponse> updateDoctorReference(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/DeleteDoctorReference")
    Call<RetrofitJSONResponse> deleteDoctorReference(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/AddDoctorSocialLink")
    Call<RetrofitJSONResponse> addDoctorSocialLink(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/UpdateDoctorSocialLink")
    Call<RetrofitJSONResponse> updateDoctorSocialLink(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/DeleteDoctorSocialLink")
    Call<RetrofitJSONResponse> deleteDoctorSocialLink(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/AddDoctorService")
    Call<RetrofitJSONResponse> addDoctorService(@FieldMap Map<String, Object> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/UpdateDoctorService")
    Call<RetrofitJSONResponse> updateDoctorService(@FieldMap Map<String, Object> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/DeleteDoctorService")
    Call<RetrofitJSONResponse> deleteDoctorService(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/AddCertificate")
    Call<RetrofitJSONResponse> addDoctorCertificate(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/UpdateCertificate")
    Call<RetrofitJSONResponse> updateDoctorCertificate(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/DeleteCertificate")
    Call<RetrofitJSONResponse> deleteDoctorCertificate(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/ArchivedAppointment")
    Call<RetrofitJSONResponse> getArchivedAppointments(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/UndoCompleteOfficeAppointment")
    Call<RetrofitJSONResponse> undoCompleteOfficeAppointment(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/CompleteOfficeAppointment")
    Call<RetrofitJSONResponse> completeOfficeAppointment(@FieldMap Map<String, String> parameters);

    @FormUrlEncoded
    @POST("/api/Doctor/CancelBookedAppointment")
    Call<RetrofitJSONResponse> cancelBookedAppointment(@FieldMap Map<String, String> parameters);

}