package com.imedhealthus.imeddoctors.common.models;

import java.io.Serializable;

public interface ListItemImage extends Serializable {

    public String getImagePath();

    public boolean isImageUri();

    public boolean isFilePath();
}
