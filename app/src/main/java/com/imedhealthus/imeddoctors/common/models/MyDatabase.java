package com.imedhealthus.imeddoctors.common.models;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

/**
 * Created by Malik Hamid on 2/15/2018.
 */

public class MyDatabase extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "country_state.db";
    private static final int DATABASE_VERSION = 1;


    private static final String ID="Id";
    private static final String NAME="CountryName";
    private static final String SORTNAME ="Sortname";
    private static final String PHONECODE ="Phonecode";
    private static final String COUNTRIESTABLE ="Countries";


    private static final String StateNAME="StateName";
    private static final String COUNTRYID ="CountryId";
    private static final String STATESTABLE ="States";

    private static final String CITYNAME ="CityName" ;
    private static final String CITYTABLE ="Cities";
    private static final String STATEID="StateId";

    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public ArrayList<Country> getCountries() throws ExceptionInInitializerError,IllegalStateException{

        SQLiteDatabase db=getWritableDatabase();
        String[] columns={"id",MyDatabase.NAME,MyDatabase.SORTNAME,MyDatabase.PHONECODE};
//        String[] selectionArgs={categoryId+"",subjectId+"",yearId+""};
        Cursor cursor=db.query(MyDatabase.COUNTRIESTABLE, columns, null, null, null, null, null);
//        Cursor cursor=db.query(MyDatabase.TABLE_NAME, columns, null,null, null, null, null);
        ArrayList<Country> countriesList=new ArrayList<>();
        countriesList.add(new Country("Select Country"));
        while(cursor.moveToNext()){
            Country country=new Country();
            country.countryId=cursor.getInt(cursor.getColumnIndex("id"));
            country.CountryName=cursor.getString(cursor.getColumnIndex(MyDatabase.NAME));
            country.SortName=cursor.getString(cursor.getColumnIndex(MyDatabase.SORTNAME));
            country.PhoneCode=cursor.getString(cursor.getColumnIndex(MyDatabase.PHONECODE));

            countriesList.add(country);
        }
        return countriesList;
    }

    public ArrayList<Country> getPracticeCountries(){

        SQLiteDatabase db=getWritableDatabase();
        String[] columns={MyDatabase.ID,MyDatabase.NAME,MyDatabase.SORTNAME,MyDatabase.PHONECODE};
//        String[] selectionArgs={categoryId+"",subjectId+"",yearId+""};
        Cursor cursor=db.query(MyDatabase.COUNTRIESTABLE, columns, null, null, null, null, null);
//        Cursor cursor=db.query(MyDatabase.TABLE_NAME, columns, null,null, null, null, null);
        ArrayList<Country> ArrayList=new ArrayList<>();
        ArrayList.add(new Country("Select Practice Country"));
        while(cursor.moveToNext()){
            Country country=new Country();
            country.countryId=cursor.getInt(cursor.getColumnIndex("id"));
            country.CountryName=cursor.getString(cursor.getColumnIndex(MyDatabase.NAME));
            country.SortName=cursor.getString(cursor.getColumnIndex(MyDatabase.SORTNAME));
            country.PhoneCode=cursor.getString(cursor.getColumnIndex(MyDatabase.PHONECODE));

            ArrayList.add(country);
        }
        return ArrayList;
    }

    public ArrayList<State> getStates(){
        SQLiteDatabase db=getWritableDatabase();
        String[] columns={MyDatabase.ID,MyDatabase.StateNAME,MyDatabase.COUNTRYID};
//        String[] selectionArgs={categoryId+"",subjectId+"",yearId+""};
        Cursor cursor=db.query(MyDatabase.STATESTABLE, columns, null, null, null, null, null);
//        Cursor cursor=db.query(MyDatabase.TABLE_NAME, columns, null,null, null, null, null);
        ArrayList<State> ArrayList=new ArrayList<>();
        ArrayList.add(new State(""));
        while(cursor.moveToNext()){
            State state=new State();
            state.countryId=Integer.toString(cursor.getInt(cursor.getColumnIndex(MyDatabase.COUNTRYID)));
            state.stateName=cursor.getString(cursor.getColumnIndex(MyDatabase.StateNAME));
            state.stateId=cursor.getString(cursor.getColumnIndex(MyDatabase.ID));

            ArrayList.add(state);
        }
        return ArrayList;
    }

    public ArrayList<City> getCities(){
        SQLiteDatabase db=getWritableDatabase();
        String[] columns={MyDatabase.ID,MyDatabase.CITYNAME,MyDatabase.STATEID};
//        String[] selectionArgs={categoryId+"",subjectId+"",yearId+""};
        Cursor cursor=db.query(MyDatabase.CITYTABLE, columns, null, null, null, null, null);
//        Cursor cursor=db.query(MyDatabase.TABLE_NAME, columns, null,null, null, null, null);
        ArrayList<City> ArrayList=new ArrayList<>();
        ArrayList.add(new City(""));
        while(cursor.moveToNext()){
            City city=new City();
            city.stateId=Integer.toString(cursor.getInt(cursor.getColumnIndex(MyDatabase.STATEID)));
            city.cityName=cursor.getString(cursor.getColumnIndex(MyDatabase.CITYNAME));
            city.cityId=cursor.getString(cursor.getColumnIndex(MyDatabase.ID));

            ArrayList.add(city);
        }
        return ArrayList;
    }


}