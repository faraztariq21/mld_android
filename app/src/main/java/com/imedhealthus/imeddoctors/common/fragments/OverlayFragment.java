package com.imedhealthus.imeddoctors.common.fragments;

/**
 * Created by umair on 1/6/18.
 */

public abstract class OverlayFragment extends BaseFragment {

    public abstract void setData(Object data);
    public abstract Object getData();

    public boolean canHandleOnBackClick() {
        return false;
    }

    public void onBackClick() {

    }

}
