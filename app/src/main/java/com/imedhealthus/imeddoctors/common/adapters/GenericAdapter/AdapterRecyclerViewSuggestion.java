package com.imedhealthus.imeddoctors.common.adapters.GenericAdapter;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterRecyclerViewSuggestion extends RecyclerView.Adapter<AdapterRecyclerViewSuggestion.SuggestionViewholder>{


    @NonNull
    @Override
    public SuggestionViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull SuggestionViewholder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class SuggestionViewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_suggestion)
        TextView tvSuggestion;
        @BindView(R.id.main_layout)
        ConstraintLayout clMainLayout;

        public SuggestionViewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
