package com.imedhealthus.imeddoctors.common.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.listeners.OnPrescriptionItemClickListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.view_holders.PrescriptionItemViewHolder;
import com.imedhealthus.imeddoctors.patient.models.Prescription;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Malik Hamid on 1/11/2018.
 */

public class PrescriptionListAdapter extends RecyclerView.Adapter<PrescriptionItemViewHolder> {

    private List<Prescription> prescriptions;
    private WeakReference<TextView> tvNoRecords;
    private WeakReference<OnPrescriptionItemClickListener> onPrescriptionItemClickListener;
    private boolean canShowEdit;


    public PrescriptionListAdapter(TextView tvNoRecords, boolean canShowEdit, OnPrescriptionItemClickListener onPrescriptionItemClickListener) {

        super();
        this.tvNoRecords = new WeakReference<>(tvNoRecords);
        this.canShowEdit = canShowEdit;
        this.onPrescriptionItemClickListener = new WeakReference<>(onPrescriptionItemClickListener);

    }

    public void setPrescriptions(List<Prescription> prescriptions) {

        this.prescriptions = prescriptions;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }

    public void removePrescription(Prescription prescription) {
        prescriptions.remove(prescription);
        setPrescriptions(prescriptions);
    }


    @Override
    public PrescriptionItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_prescription, parent, false);

        OnPrescriptionItemClickListener listener = null;
        if (onPrescriptionItemClickListener != null && onPrescriptionItemClickListener.get() != null) {
            listener = onPrescriptionItemClickListener.get();
        }

        return new PrescriptionItemViewHolder(view, listener);

    }

    @Override
    public void onBindViewHolder(PrescriptionItemViewHolder viewHolder, int position) {

        Prescription prescription = getItem(position);
        boolean isEditAllowed = canShowEdit && CacheManager.getInstance().getCurrentUser().getUserId().equals(prescription.getDoctorId());//If user is same doctor

        viewHolder.setData(getItem(position), isEditAllowed,  position);
    }

    @Override
    public int getItemCount() {
        return prescriptions == null ? 0 : prescriptions.size();
    }

    public Prescription getItem(int position) {
        if (position < 0 || position >= prescriptions.size()) {
            return null;
        }
        return prescriptions.get(position);
    }

}
