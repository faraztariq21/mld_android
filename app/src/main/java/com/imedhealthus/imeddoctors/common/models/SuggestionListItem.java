package com.imedhealthus.imeddoctors.common.models;

import java.io.Serializable;

public interface SuggestionListItem extends Serializable{

    public String getSuggestionText();

    public SuggestionListItem getSuggestionListItem();
}
