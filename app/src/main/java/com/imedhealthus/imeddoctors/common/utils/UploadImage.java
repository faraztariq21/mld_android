package com.imedhealthus.imeddoctors.common.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

/**
 * Created by Malik Hamid on 2/6/2018.
 */

/**
 * @param "file uploading destination"
 * @param "file local path"
 */
public class UploadImage extends AsyncTask<String, UploadImage.FileToUpload, UploadImage.FileToUpload> {

    public interface OnImageUploadCompletionListener {
        void onImageUploadComplete(boolean success, int index, String uploadPath);
    }

    ProgressDialog progressDialog;
    private String uploadPath, localPath;
    private ArrayList<String> localPaths;
    private OnImageUploadCompletionListener listener;

    Context context;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public UploadImage(String uploadPath, String localPath, OnImageUploadCompletionListener listener) {
        this.uploadPath = uploadPath;
        this.localPath = localPath;
        this.listener = listener;
    }

    public UploadImage(String uploadPath, ArrayList<String> localPaths, OnImageUploadCompletionListener listener) {
        this.uploadPath = uploadPath;
        this.localPaths = localPaths;
        this.listener = listener;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    /*    if (context != null) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(getContext().getResources().getString(R.string.uploading_image));
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        }
    */
    }

    protected FileToUpload doInBackground(String... urls) {

        if (localPaths != null) {
            for (int i = 0; i < localPaths.size(); i++) {
                if (localPaths.get(i) == null)

                    publishProgress(new FileToUpload(false, null, i));
                else {
                    FileToUpload fileToUpload = uploadFile(new FileToUpload(false, localPaths.get(i), i));
                    publishProgress(fileToUpload);
                }
            }
        } else {
            FileToUpload fileToUpload = uploadFile(new FileToUpload(false, localPath, -1));
            publishProgress(fileToUpload);
        }
        return null;

    }


    @Override
    protected void onProgressUpdate(FileToUpload... values) {
        super.onProgressUpdate(values);
        if (listener != null) {
            listener.onImageUploadComplete(values[0].isSuccess(), values[0].getIndex(), values[0].getUploadPath());
        }
    }

    private FileToUpload uploadFile(FileToUpload fileToUpload) {
        FTPClient client = null;
        boolean success = false;

        try {

            client = new FTPClient();
            client.connect(Constants.FTP.hostName);

            if (client.login(Constants.FTP.username, Constants.FTP.password)) {
                client.enterLocalPassiveMode(); // important!
                client.setFileType(FTP.BINARY_FILE_TYPE);

                FileInputStream in = new FileInputStream(new File(fileToUpload.getLocalPath()));
                if (uploadPath == null)
                    uploadPath = Constants.FTP.patientDataPath + FileUtils.getFileNameFromPath(fileToUpload.getLocalPath());
                success = client.storeFile(uploadPath, in);
                in.close();
                client.logout();
                client.disconnect();

            }

        } catch (Exception e) {
            fileToUpload.setSuccess(false);
            return fileToUpload;
        }


        fileToUpload.setSuccess(success);

        if (fileToUpload.isSuccess())
            fileToUpload.setUploadPath(uploadPath);

        uploadPath = null;
        return fileToUpload;

    }

    @Override
    protected void onPostExecute(FileToUpload fileToUpload) {


     /*   if (progressDialog != null)
            progressDialog.dismiss();
     */
        super.onPostExecute(fileToUpload);
        /*if (listener != null) {
            listener.onImageUploadComplete(success, 0);
        }*/
        Log.e("iMed", "Image upload complete");

    }


    public class FileToUpload {
        boolean success;
        String localPath;
        int index;
        String uploadPath;

        public FileToUpload(boolean success, String localPath, int index) {
            this.success = success;
            this.localPath = localPath;
            this.index = index;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getLocalPath() {
            return localPath;
        }

        public void setLocalPath(String localPath) {
            this.localPath = localPath;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public void setUploadPath(String uploadPath) {
            this.uploadPath = uploadPath;
        }

        public String getUploadPath() {
            return uploadPath;
        }
    }


}
