package com.imedhealthus.imeddoctors.common.models;

/**
 * Created by Malik Hamid on 4/4/2018.
 */

public class CallSignal  {
        private String recieverLoginId, receiverProfileId, msg, chatId , loginId, profileId, callType,connectionId,status,image;

        public CallSignal(String receiverId, String receiverProfileId, String msg,String callType,String connectionId,String chatId,String login, String profileId) {
            this.recieverLoginId = receiverId;
            this.receiverProfileId = receiverProfileId;
            this.msg = msg;
            this.chatId = chatId;
            this.loginId = login;
            this.profileId = profileId;
            this.callType = callType;
            this.connectionId = connectionId;
        }

    public CallSignal() {
    }

    public void setMsg(String msg) {
            this.msg = msg;
        }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMsg() {
            return msg;
        }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRecieverLoginId() {
        return recieverLoginId;
    }

    public void setRecieverLoginId(String recieverLoginId) {
        this.recieverLoginId = recieverLoginId;
    }

    public String getReceiverProfileId() {
        return receiverProfileId;
    }

    public void setReceiverProfileId(String receiverProfileId) {
        this.receiverProfileId = receiverProfileId;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }
}
