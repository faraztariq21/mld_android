package com.imedhealthus.imeddoctors.common.utils;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.util.regex.Pattern;

public class DownloadCompleteReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case DownloadManager.ACTION_DOWNLOAD_COMPLETE:

                long refId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                if (new TinyDB(context).getBoolean("isDownload " + refId)) {
                    Toast.makeText(context, "Download Complete. File saved in " + Constants.EXTERNAL_STORAGE_IMAGES, Toast.LENGTH_LONG).show();
                } else {

                    String chatFile = SharedPrefsHelper.getChatFileFromDownloadId(context, refId);
                    String commentFile = SharedPrefsHelper.getForumCommentFileFromDownloadId(context, refId);
                    String chatFileArray[] = new String[0];
                    if (chatFile != null)
                        chatFileArray = chatFile.split(Pattern.quote("|"));
                    else if (commentFile != null)
                        chatFileArray = commentFile.split(Pattern.quote("|"));
                    if (chatFileArray.length > 1) {
                        String chatMessage = chatFileArray[0];
                        String chatFileAddress = chatFileArray[1];
                        if (FileUtils.checkFileExists(chatFileAddress))
                            Toast.makeText(context, "Download Completed", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(context, "Download Unsuccessful", Toast.LENGTH_SHORT).show();

                        if (chatFile != null)
                            SharedPrefsHelper.saveFileLocalAddress(context, chatMessage, chatFileAddress);
                        else
                            SharedPrefsHelper.saveForumCommentFileLocalAddress(context, chatMessage, chatFileAddress);
                    }
                }
                break;


        }
    }
}

