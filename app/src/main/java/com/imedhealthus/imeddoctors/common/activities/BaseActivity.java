package com.imedhealthus.imeddoctors.common.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.fragments.CallFragment;
import com.imedhealthus.imeddoctors.common.fragments.ChatFragment;
import com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.LocationLoader;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.Logs;
import com.imedhealthus.imeddoctors.common.utils.NotificationUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by umair on 1/3/18.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private final int LOCATION_PERMISSIONS = 992;
    private Dialog dialog;

    protected void onResume() {

        super.onResume();
        ((ApplicationManager) getApplicationContext()).setCurrentActivity(this);

        IntentFilter filter = new IntentFilter(Constants.Keys.PushNotificationBroadCast);
        registerReceiver(pushNotificationReceiver, filter);


    }

    protected void onPause() {

        super.onPause();



        unregisterReceiver(pushNotificationReceiver);

    }

    protected void onDestroy() {
//        clearReferences();
        super.onDestroy();
    }

    private void clearReferences() {
        Activity currActivity = ((ApplicationManager) getApplicationContext()).getCurrentActivity();
        if (this.equals(currActivity))
            ((ApplicationManager) getApplicationContext()).setCurrentActivity(null);
    }


    public void startActivity(Intent intent, boolean withAnimation) {
        startActivity(intent);
        if (withAnimation) {
            overridePendingTransition(R.anim.slide_from_right, R.anim.stable);
        }
    }

    public void startActivityForResult(Intent intent, int requestCode, boolean withAnimation) {
        startActivityForResult(intent, requestCode);
        if (withAnimation) {
            overridePendingTransition(R.anim.slide_from_right, R.anim.stable);
        }
    }


    @Override
    public void onBackPressed() {
        Logs.appLog("On Back Pressed");
        onBackPressed(true);
    }

    public void onBackPressed(boolean withAnimation) {
        super.onBackPressed();
        if (withAnimation) {
            overridePendingTransition(R.anim.stable, R.anim.slide_to_right);
        }
    }


    //For location
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LocationLoader.LOCATION_SETTINGS_REQUEST_CODE) {
            LocationLoader.getShared().startLocationUpdate();
        }
    }

    public void requestLocationPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSIONS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    LocationLoader.getShared().startLocationUpdate();
                }
            }
            break;

        }
    }


    //Push notification
    protected void handlePusNotification(HashMap<String, String> data) {

        String title = data.get("title");
        String body = data.get("msg");

        if (TextUtils.isEmpty(title)) {
            title = "New Message";
        }
        if (TextUtils.isEmpty(body)) {
            title = "You have a new message";
        }

        if ((this instanceof BaseFragmentActivity) && (((BaseFragmentActivity) this).currentFragment instanceof ChatFragment
                || ((BaseFragmentActivity) this).currentFragment instanceof CallFragment)) {//It is already showing chat or call
            return;
        }
        NotificationUtils notificationUtils = new NotificationUtils(ApplicationManager.getContext());
        notificationUtils.showNotificationMessageForMessage("You have a Message", body
                , new SimpleDateFormat("MMMM").format(new Date()), new Intent());

        //showNotificationPopup(body, this);

    }

    public void showNotificationPopup(final String msg, final Activity context) {


        Activity activity = ((ApplicationManager) ApplicationManager.getContext()).getCurrentActivity();

        if (activity != null)
            if (activity.getClass() == SimpleFragmentActivity.class) {
                if (((SimpleFragmentActivity) activity).getCurrentFragment().getName().equals("CallFragment")
                        )
                    return;
            }

        if (dialog != null)
            if (dialog.isShowing())
                dialog.dismiss();

        if (dialog == null) {
            dialog = new Dialog(context);
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.new_notification_dialog);
        }


        TextView text = (TextView) dialog.findViewById(R.id.dialog_msg);
        text.setText(msg);
        TextView tv_title = (TextView) dialog.findViewById(R.id.dialog_title);
        tv_title.setText("Alert ");


        Button rejectButton = (Button) dialog.findViewById(R.id.btn_reject);
        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();


            }
        });
        Button acceptButton = (Button) dialog.findViewById(R.id.btn_accept);
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();

                //     SharedData.getInstance().setOpenTokCallSessionId(data.getCallSessionId());
                //     SharedData.getInstance().setOpenTokCallToken(data.getCallToken());


                Activity activity = ((ApplicationManager) ApplicationManager.getContext()).getCurrentActivity();


                Intent intent = new Intent(activity, SimpleFragmentActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.Notifications.ordinal());
                ((BaseActivity) activity).startActivity(intent, true);


            }
        });

        dialog.show();
    }

    BroadcastReceiver pushNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Log.e("iMed", "Push: BroadcastReceiver");
            handlePusNotification((HashMap<String, String>) intent.getSerializableExtra(Constants.Keys.PushNotificationData));

        }
    };


}
