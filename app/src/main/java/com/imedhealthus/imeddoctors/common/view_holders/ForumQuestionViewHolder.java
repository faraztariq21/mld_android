package com.imedhealthus.imeddoctors.common.view_holders;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.listeners.OnChatItemClickListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.ChatItem;
import com.imedhealthus.imeddoctors.common.models.ChatUserModel;
import com.imedhealthus.imeddoctors.common.models.ForumQuestion;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 1/16/2018.
 */

public class ForumQuestionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    RelativeLayout contItem;

    @BindView(R.id.iv_profile)
    ImageView ivProfile;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.ic_forward)
    TextView icForward;

    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_message)
    TextView tvMessage;

    private int idx;
    private WeakReference<OnChatItemClickListener> onChatItemClickListener;

    public ForumQuestionViewHolder(View view, OnChatItemClickListener onChatItemClickListener) {

        super(view);
        ButterKnife.bind(this, view);
        this.onChatItemClickListener = new WeakReference<>(onChatItemClickListener);
        contItem.setOnClickListener(this);

    }

    public void setData(final ForumQuestion forumQuestion, int idx){

        String userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
//        ChatUserModel partner = forumQuestion.getPartner(userLoginId);
//        if (partner == null) {
//            tvName.setText("");
//            ivProfile.setImageResource(0);
//        }else {
//            Picasso.with(ApplicationManager.getContext()).load(partner.getImgURL()).placeholder(R.drawable.ic_profile_placeholder).into(ivProfile);
//        }


        if((forumQuestion.getName() != null && !forumQuestion.getName().equalsIgnoreCase(""))
                && !forumQuestion.isAnonymous()) {
            tvName.setText(forumQuestion.getName());
        } else {
            tvName.setText("Anonymous");
        }
        tvMessage.setText(forumQuestion.getQuestion());
        if(forumQuestion.getCreatedDate() != null && !forumQuestion.getCreatedDate().equalsIgnoreCase("")) {
            long timeStamp = GenericUtils.getTimeDateInLong(forumQuestion.getCreatedDate());
            String timeAgo = GenericUtils.getPassedTime(timeStamp);
            tvTime.setText(timeAgo);
        }
        if(forumQuestion.getProfilePicUrl()!= null && !forumQuestion.getProfilePicUrl().equalsIgnoreCase("")) {
            Glide.with(ApplicationManager.getContext()).load(forumQuestion.getProfilePicUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivProfile);
        }
        this.idx = idx;

    }

    @Override
    public void onClick(View view) {

        if (onChatItemClickListener == null || onChatItemClickListener.get() == null) {
            return;
        }

        switch (view.getId()) {
            case R.id.cont_item:
                onChatItemClickListener.get().onChatItemClick(getAdapterPosition());
                break;
        }
    }
}
