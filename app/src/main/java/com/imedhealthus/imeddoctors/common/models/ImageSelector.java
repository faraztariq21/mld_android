package com.imedhealthus.imeddoctors.common.models;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;

import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.ImageUtils;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;

import static android.os.Build.VERSION_CODES.M;

/**
 * Created by umair on 1/5/18.
 */

public class ImageSelector {

    private Uri fileUri;

    public interface OnImageSelectionListener {
        void onImageSelected(Bitmap bitmap, Uri croppedImageUri, Uri fullImageUri);
    }

    private boolean isSourceCamera = false;
    public static final int CAMERA_REQUEST_CODE = 123, GALLERY_REQUEST_CODE = 124, PERMISSIONS_CODE = 111;

    private Activity activity;
    android.support.v4.app.Fragment fragment;
    private OnImageSelectionListener listener;


    public ImageSelector(android.support.v4.app.Fragment fragment, Activity activity, OnImageSelectionListener listener) {
        this.activity = activity;
        this.listener = listener;
        this.fragment = fragment;
    }


    public void showSourceAlert() {

        new AlertDialog.Builder(activity)
                .setTitle("Select image source")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        isSourceCamera = true;
                        if (checkPermissions(Manifest.permission.CAMERA)) {
                            openCameraHelper();
                        }

                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        dialog.dismiss();
                        isSourceCamera = false;
                        if (checkPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            openGalleryHelper();
                        }

                    }
                })
                .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();

    }

    private boolean checkPermissions(String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{permission}, PERMISSIONS_CODE);
            return false;
        }
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (isSourceCamera) {
                                openCameraHelper();
                            } else {
                                openGalleryHelper();
                            }

                        }
                    });
                } else {
                    AlertUtils.showAlert(activity, Constants.ErrorAlertTitle, "Can't continue without required permissions.");
                }
            }
            break;

        }
    }

    private void openCameraHelper() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {


            if (Build.VERSION.SDK_INT > M) {
                try {
                    fileUri = FileProvider.getUriForFile(activity, "com.mylivedoctor.provider", createImageFile());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                fileUri = Uri.fromFile(createImageFile());
            }

            takePicture.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*if (fragment != null)
            fragment.startActivityForResult(takePicture, CAMERA_REQUEST_CODE);
        else
            */
        activity.startActivityForResult(takePicture, CAMERA_REQUEST_CODE);

    }

    private void openGalleryHelper() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        /*if (fragment != null)
            fragment.startActivityForResult(intent, GALLERY_REQUEST_CODE);
        else
            */
        activity.startActivityForResult(intent, GALLERY_REQUEST_CODE);

    }

    private File createImageFile() throws IOException {

        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return new File(storageDir, "image.jpg");

    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Uri fullImageUri = null;
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Crop.REQUEST_CROP) {
                if (data == null)
                    return;

                fullImageUri = SharedData.getInstance().getFullImageUri();

                Uri selectedImage = Crop.getOutput(data);
                Bitmap bitmap = null;
                if (selectedImage != null) {
                    try {
                        bitmap = ImageUtils.handleSamplingAndRotationBitmap(activity, selectedImage);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if (listener != null) {
                        listener.onImageSelected(bitmap, selectedImage, fullImageUri);
                    }
                }

            } else if (requestCode == CAMERA_REQUEST_CODE || requestCode == GALLERY_REQUEST_CODE) {

                Uri selectedImage = null;
                if (requestCode == CAMERA_REQUEST_CODE) {
                    try {
                        selectedImage = Uri.fromFile(createImageFile());
                        fullImageUri = selectedImage;
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                    selectedImage = data.getData();
                    fullImageUri = selectedImage;


                }

                SharedData.getInstance().setFullImageUri(fullImageUri);
                if (selectedImage == null) {
                    return;
                }

                Uri destination = Uri.fromFile(new File(activity.getCacheDir(), "cropped"));
                //Crop.of(selectedImage, destination).asSquare().start(activity);
                Bitmap bitmap = null;
                if (selectedImage != null) {
                    try {
                        bitmap = ImageUtils.handleSamplingAndRotationBitmap(activity, selectedImage);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if (listener != null) {
                        listener.onImageSelected(bitmap, selectedImage, fullImageUri);
                    }
                }
            }
        }
    }

}
