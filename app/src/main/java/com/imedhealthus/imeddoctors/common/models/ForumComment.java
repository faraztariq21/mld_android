package com.imedhealthus.imeddoctors.common.models;

import com.google.gson.annotations.SerializedName;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

/**
 * Created by Dev-iMEDHealth-X5 on 18/04/2018.
 */

public class ForumComment {
    @SerializedName("LoginId")
    String loginId;
    @SerializedName("Id")
    String commentId;
    @SerializedName("QuestionId")
    String questionId;
    @SerializedName("ProfilePix")
    String profilePicUrl;
    @SerializedName("Name")
    String name;
    @SerializedName("Comment")
    String comment;
    @SerializedName("CreateDate")
    String createdDate;

    String imageURI;

    @SerializedName("FileName")
    String fileUrl;
    @SerializedName("FileType")
    String fileType;
    @SerializedName("FileSize")
    String fileSize;

    String filePathLocal;


    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public void setFilePathLocal(String filePathLocal) {
        this.filePathLocal = filePathLocal;
    }

    public String getFilePathLocal() {
        return filePathLocal;
    }

    public String getImageURI() {
        return imageURI;
    }

    public void setImageURI(String imageURI) {
        this.imageURI = imageURI;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreatedDate() {
        return GenericUtils.getLocalTimeInStringFromUTC(createdDate);
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }


    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public boolean isFromSameThread(String questionId) {
        return this.commentId.equals(questionId);
    }

    @Override
    public String toString() {
        return getLoginId() + " " + getComment() + " " + getCommentId() + " " + getQuestionId() + " ";
    }


    public boolean equals(ForumComment forumComment) {
        try {
            return this.getQuestionId().equals(forumComment.getQuestionId()) && this.getLoginId().equals(forumComment.getLoginId()) && this.getCommentId().equals(forumComment.getCommentId());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
