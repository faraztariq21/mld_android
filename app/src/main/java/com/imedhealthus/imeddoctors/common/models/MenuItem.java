package com.imedhealthus.imeddoctors.common.models;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.auth.activities.LandingActivity;
import com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;

/**
 * Created by umair on 1/9/18.
 */

public class MenuItem {

    public enum OPTION {
        HOME, PAYMENTS, INVOICES, FORUM, INBOX, QUESTIONS, REFERRAL, CHANGE_PASSWORD, FEEDBACK, ABOUT_US, LOGOUT, ARCHIVED_APPOINTMENTS
    }

    private OPTION option;
    private String title;
    private int count, iconResource;

    public MenuItem(OPTION option) {

        this.option = option;
        this.count = 0;

        switch (option) {
            case HOME:
                title = "Home";
                iconResource = R.drawable.ic_home_light_blue;
                break;

            case PAYMENTS:
                title = "Payment Methods";
                iconResource = R.drawable.ic_credit;
                break;

            case INVOICES:
                title = "Invoices";
                iconResource = R.drawable.ic_menu_invoice;
                break;

            case FORUM:
                title = "Forum";
                iconResource = R.drawable.ic_forum;
                break;

            case INBOX:
                if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor)
                    title = ApplicationManager.getContext().getResources().getString(R.string.my_patients_conversation);
                else
                    title = ApplicationManager.getContext().getResources().getString(R.string.my_doctors_conversation);

                iconResource = R.drawable.ic_menu_chat_light_blue;
                break;

            case QUESTIONS:
                title = "Questions";
                iconResource = R.drawable.ic_questions;
                break;

            case REFERRAL:
                title = "Referral";
                iconResource = R.drawable.ic_reference;
                break;

            case CHANGE_PASSWORD:
                title = "Change Password";
                iconResource = R.drawable.ic_lock;
                break;

            case FEEDBACK:
                title = "Feedback";
                iconResource = R.drawable.ic_menu_feedback;
                break;

            case ABOUT_US:
                title = "About Us";
                iconResource = R.drawable.ic_menu_about;
                break;

            case LOGOUT:
                title = "Logout";
                iconResource = R.drawable.ic_menu_logout;
                break;

            case ARCHIVED_APPOINTMENTS:
                title = "Archived Appointment";
                iconResource = R.drawable.ic_appointment_gray;
                break;
            default:
                break;
        }

    }

    public Constants.Fragment getFragment() {

        switch (option) {
            case HOME:
                if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Patient) {
                    return Constants.Fragment.PatientHome;
                } else {
                    return Constants.Fragment.DoctorHome;
                }

            case PAYMENTS:
                return Constants.Fragment.PaymentMethods;

            case INVOICES:
                return Constants.Fragment.Invoices;

            case FORUM:
                return Constants.Fragment.Forum;

            case INBOX:
                return Constants.Fragment.Inbox;

            case QUESTIONS:
                return Constants.Fragment.Questions;

            case REFERRAL:
                return Constants.Fragment.Referral;

            case CHANGE_PASSWORD:
                return Constants.Fragment.ChangePassword;

            case FEEDBACK:
                return Constants.Fragment.Feedback;

            case ABOUT_US:
                return Constants.Fragment.AboutUs;

            case ARCHIVED_APPOINTMENTS:
                return Constants.Fragment.ARCHIVED_APPOINTMENTS;

            case LOGOUT:
                logout();
                break;

        }

        return Constants.Fragment.None;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getIconResource() {
        return iconResource;
    }

    public void setIconResource(int iconResource) {
        this.iconResource = iconResource;
    }

    private void logout() {


        final Activity activity = ((ApplicationManager) ApplicationManager.getContext()).getCurrentActivity();

        AlertUtils.showProgress(activity);
        WebServicesHandler.instance.logout(CacheManager.getInstance().getCurrentUser().getLoginId(), new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                makeUserOffline();

                NotificationManager notificationManager = (NotificationManager) ApplicationManager.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancelAll();

                CacheManager.getInstance().logoutUser();

                OpenTokHandler.getInstance().distroySession();


                AlertUtils.dismissProgress();
                Intent intent = new Intent(activity, LandingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(intent);
                activity.finish();


            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(activity, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });


    }

    public void makeUserOffline() {

        String userLoginId = "0";
        if (!CacheManager.getInstance().isUserLoggedIn()) {
            return;
        }
        userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();

        WebServicesHandler.instance.changeUserStatus(userLoginId, false, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                AlertUtils.dismissProgress();
                if (response.status()) {

                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
            }
        });

    }

}
