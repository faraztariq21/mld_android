package com.imedhealthus.imeddoctors.common.models;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Malik Hamid on 2/13/2018.
 */

public class Country implements Serializable, SuggestionListItem {
    @SerializedName("id")
    public int countryId;
    String CountryName;
    @SerializedName("Sortname")
    String SortName;
    @SerializedName("Phonecode")
    String PhoneCode;

    public Country(String dummy) {
        countryId = 0;
        CountryName = dummy;
    }

    public Country() {
    }

    public Country(int countryId, String countryName, String sortName, String phoneCode) {
        this.countryId = countryId;
        CountryName = countryName;
        SortName = sortName;
        PhoneCode = phoneCode;
    }

    @Override
    public String toString() {
        return CountryName;
    }


    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getSortName() {
        return SortName;
    }

    public void setSortName(String sortName) {
        SortName = sortName;
    }

    public String getPhoneCode() {
        return PhoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        PhoneCode = phoneCode;
    }

    @Override
    public String getSuggestionText() {
        if (SharedData.getInstance().isGetCountryCodes())
            return getCountryName() + " " +
                    "(" + getPhoneCode() + ")";
        else
            return getCountryName();
    }

    public static ArrayList<SuggestionListItem> getSuggestions(ArrayList<Country> countries) {
        ArrayList<SuggestionListItem> suggestionListItems = new ArrayList<>();
        for (Country country : countries)
            suggestionListItems.add(country);

        return suggestionListItems;
    }

    @Override
    public SuggestionListItem getSuggestionListItem() {
        return Country.this;
    }
}
