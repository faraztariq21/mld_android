package com.imedhealthus.imeddoctors.common.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 2/20/2018.
 */

public class FilterLanguages {
    @SerializedName("languageId")
    private String language;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public FilterLanguages(String language) {
        this.language = language;
    }

    public List<FilterLanguages> getSerielizeableList(List<String> strings ){
        List<FilterLanguages> filterLanguages = new ArrayList<>();

        for(int i=0;i<strings.size();i++){
            filterLanguages.add(new FilterLanguages(strings.get(i)));
        }

        return filterLanguages;
    }
}
