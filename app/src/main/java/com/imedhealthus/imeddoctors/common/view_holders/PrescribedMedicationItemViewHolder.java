package com.imedhealthus.imeddoctors.common.view_holders;

import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.patient.listeners.OnMedicationItemClickListener;
import com.imedhealthus.imeddoctors.patient.models.PrescribedMedication;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 1/30/2018.
 */

public class PrescribedMedicationItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    ViewGroup contItem;

    @BindView(R.id.iv_icon)
    ImageView ivIcon;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_dose)
    TextView tvDose;
    @BindView(R.id.tv_notes)
    TextView tvNotes;

    @BindView(R.id.tv_from)
    TextView tvFrom;
    @BindView(R.id.tv_to)
    TextView tvTo;

    @BindView(R.id.iv_edit)
    ImageView ivEdit;
    @BindView(R.id.iv_delete)
    ImageView ivDelete;

    private int idx;
    private WeakReference<OnMedicationItemClickListener> onMedicationItemClickListener;

    public PrescribedMedicationItemViewHolder(View view, boolean isEditAllowed, OnMedicationItemClickListener onMedicationItemClickListener) {

        super(view);
        ButterKnife.bind(this, view);

        if (isEditAllowed) {
            ivEdit.setVisibility(View.VISIBLE);
            ivDelete.setVisibility(View.VISIBLE);
        } else {
            ivEdit.setVisibility(View.GONE);
            ivDelete.setVisibility(View.GONE);
        }

        contItem.setOnClickListener(this);
        ivEdit.setOnClickListener(this);
        ivDelete.setOnClickListener(this);
        this.onMedicationItemClickListener = new WeakReference<>(onMedicationItemClickListener);

    }

    public void setData(PrescribedMedication medication, int idx) {

        tvName.setText(medication.getName());
        String str = medication.getDose() + " (" + medication.getUnit() + ") - " + medication.getFrequency();
        tvDose.setText(str);

        tvFrom.setText(medication.getStartingDateFormatted());
        tvTo.setText(medication.getEndingDateFormatted());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvNotes.setText(Html.fromHtml(Html.fromHtml(medication.getNotes(), Html.FROM_HTML_MODE_LEGACY).toString(),Html.FROM_HTML_MODE_LEGACY));
        } else {
            tvNotes.setText(Html.fromHtml(Html.fromHtml(medication.getNotes()).toString()));
        }


        this.idx = idx;

    }

    @Override
    public void onClick(View view) {

        if (onMedicationItemClickListener == null || onMedicationItemClickListener.get() == null) {
            return;
        }

        switch (view.getId()) {
            case R.id.cont_item:
                onMedicationItemClickListener.get().onMedicationDetailClick(idx);
                break;

            case R.id.iv_edit:
                onMedicationItemClickListener.get().onMedicationEditClick(idx);
                break;

            case R.id.iv_delete:
                onMedicationItemClickListener.get().onMedicationDeleteClick(idx);
                break;
        }
    }

}

