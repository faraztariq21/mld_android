package com.imedhealthus.imeddoctors.common.models;

import com.google.gson.annotations.SerializedName;

public class BaseResponse<ResponseObject> {

    @SerializedName("Status")
    public int status;
    @SerializedName("Message")
    public String message;

    @SerializedName("Data")
    ResponseObject responseObject;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseObject getResponseObject() {
        return responseObject;
    }
}
