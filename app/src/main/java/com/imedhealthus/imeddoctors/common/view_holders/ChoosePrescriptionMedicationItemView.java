package com.imedhealthus.imeddoctors.common.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.listeners.OnPharmacyMedicationClickListener;
import com.imedhealthus.imeddoctors.patient.models.PrescribedMedication;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 2/13/2018.
 */

public class ChoosePrescriptionMedicationItemView extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.cont_item)
    ViewGroup contItem;

    @BindView(R.id.cb_send)
    CheckBox cbSend;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_dose)
    TextView tvDose;

    @BindView(R.id.tv_from)
    TextView tvFrom;
    @BindView(R.id.tv_to)
    TextView tvTo;

    private int idx;
    public  WeakReference<OnPharmacyMedicationClickListener> onPharmacyMedicationClickListener;

    public ChoosePrescriptionMedicationItemView(View view, OnPharmacyMedicationClickListener onPharmacyMedicationClickListener) {

        super(view);
        ButterKnife.bind(this, view);

        cbSend.setOnCheckedChangeListener(this);
        this.onPharmacyMedicationClickListener = new WeakReference<>(onPharmacyMedicationClickListener);

    }

    public void setData(PrescribedMedication medication, boolean isSelected, int idx) {

        cbSend.setChecked(isSelected);

        tvName.setText(medication.getName());
        String str = medication.getDose() + " (" + medication.getUnit() + ") - " + medication.getFrequency();
        tvDose.setText(str);

        tvFrom.setText(medication.getStartingDateFormatted());
        tvTo.setText(medication.getEndingDateFormatted());

        this.idx = idx;

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (onPharmacyMedicationClickListener == null || onPharmacyMedicationClickListener.get() == null) {
            return;
        }
        onPharmacyMedicationClickListener.get().onPharmacyMedicationToggleSelection(idx);
    }
}

