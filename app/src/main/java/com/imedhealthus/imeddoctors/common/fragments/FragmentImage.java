package com.imedhealthus.imeddoctors.common.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.FileUtils;
import com.jsibbold.zoomage.ZoomageView;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * <p>
 * Use the {@link FragmentImage#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentImage extends DialogFragment {

    private static final String IMAGE_URI = "image_uri";
    private static final String IMAGE_URL = "image_url";

    private String imageUri;
    private String imageUrl;

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow()

                .getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        int width = MATCH_PARENT;
        int height = MATCH_PARENT;
        getDialog().getWindow().setLayout(width, height);

    }


    @BindView(R.id.image_view)
    ZoomageView zoomageView;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.iv_download)
    ImageView ivDownload;
    @BindView(R.id.btn_download)
    Button btnDownload;

    public FragmentImage() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static FragmentImage newInstance(String imageUri, String imageUrl) {
        FragmentImage fragment = new FragmentImage();
        Bundle args = new Bundle();
        args.putString(IMAGE_URI, imageUri);
        args.putString(IMAGE_URL, imageUrl);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            imageUri = getArguments().getString(IMAGE_URI);
            imageUrl = getArguments().getString(IMAGE_URL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        ButterKnife.bind(this, view);

        if (imageUri != null) {
            zoomageView.setImageURI(Uri.parse(imageUri));
            btnDownload.setVisibility(View.GONE);
            //ivDownload.setVisibility(View.GONE);
        } else {
            //ivDownload.setVisibility(View.VISIBLE);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
            requestOptions.placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder);
            Glide.with(getActivity()).load(imageUrl).apply(requestOptions).into(zoomageView);
        }
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentImage.this.dismiss();
            }
        });
        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileUtils.downloadFile(null, null, getActivity(), Constants.BASE_URL + imageUrl, FileUtils.getFileNameFromURL(Constants.BASE_URL + imageUrl));
            }
        });

        ivDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File file = new File(Environment.getExternalStorageDirectory() + Constants.EXTERNAL_STORAGE_IMAGES);
                file.mkdirs();
                int downloadId = PRDownloader.download(Constants.BASE_URL + imageUrl, Environment.getExternalStorageDirectory() + Constants.EXTERNAL_STORAGE_IMAGES, FileUtils.getFileNameFromURL(imageUrl))
                        .build()
                        .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                            @Override
                            public void onStartOrResume() {
                                Toast.makeText(getActivity(), "Downloading", Toast.LENGTH_SHORT).show();

                            }
                        })
                        .setOnPauseListener(new OnPauseListener() {
                            @Override
                            public void onPause() {

                            }
                        })
                        .setOnCancelListener(new OnCancelListener() {
                            @Override
                            public void onCancel() {

                            }
                        })
                        .setOnProgressListener(new OnProgressListener() {
                            @Override
                            public void onProgress(Progress progress) {

                            }
                        })
                        .start(new OnDownloadListener() {
                            @Override
                            public void onDownloadComplete() {
                                Toast.makeText(getActivity(), "Download Complete", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onError(Error error) {

                            }
                        });
            }
        });
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, FragmentImage.class.toString());
    }
}
