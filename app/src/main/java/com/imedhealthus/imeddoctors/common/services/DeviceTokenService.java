package com.imedhealthus.imeddoctors.common.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;


/**
 * Created by umair on 2/14/18.
 */

public class DeviceTokenService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {

        super.onTokenRefresh();

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("iMed", "Refreshed token: " + refreshedToken);

        if (CacheManager.getInstance().isUserLoggedIn()) {
            SharedData.getInstance().updateUserDeviceToken();
        }

    }
}
