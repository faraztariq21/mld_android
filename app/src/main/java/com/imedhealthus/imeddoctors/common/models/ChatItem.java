package com.imedhealthus.imeddoctors.common.models;

import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 1/13/2018.
 */

public class ChatItem implements Serializable {

    private String chatId, lastMessage;
    private String lastMessageTime, timeAgo;
    private List<ChatUserModel> chatUsers;

    public ChatItem() {
    }

    public ChatItem(JSONObject jsonObject) {

        chatId = jsonObject.optString("ChatId");
        ;
        lastMessage = jsonObject.optString("LastMessage").replace("null", "");

        lastMessageTime = jsonObject.optString("LastMessageTime").replace("null", "");

        long timeStamp = GenericUtils.getUTCBasedTime(lastMessageTime);
        timeAgo = GenericUtils.getPassedTime(timeStamp);

        JSONArray jsonArray = jsonObject.optJSONArray("ChatUser");
        chatUsers = new ArrayList<>();

        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject userJson = jsonArray.optJSONObject(i);
                if (userJson != null) {
                    this.chatUsers.add(new ChatUserModel(userJson));
                }
            }
        }

    }

    public static List<ChatItem> parseChatItems(JSONArray arrJson) {

        ArrayList<ChatItem> items = new ArrayList<>();

        for (int i = 0; i < arrJson.length(); i++) {

            JSONObject itemJson = arrJson.optJSONObject(i);
            if (itemJson != null) {
                ChatItem item = new ChatItem(itemJson);
                items.add(item);
            }

        }

        return items;

    }

    public ChatUserModel getPartner(String userLoginId) {

        for (ChatUserModel user : chatUsers) {
            if (!user.getId().equals(userLoginId)) {
                return user;
            }
        }

        return null;

    }

    public ChatUserModel getChatUser(String userLoginId) {

        for (ChatUserModel user : chatUsers) {
            if (user.getId().equals(userLoginId)) {
                return user;
            }
        }

        return null;

    }

    public ChatUserModel getSenderChatUser(String userLoginId) {

        for (ChatUserModel user : chatUsers) {
            if (!user.getId().equals(userLoginId)) {
                return user;
            }
        }

        return null;

    }


    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(String lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }

    public String getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(String timeAgo) {
        this.timeAgo = timeAgo;
    }

    public List<ChatUserModel> getChatUsers() {
        return chatUsers;
    }

    public void setChatUsers(List<ChatUserModel> chatUsers) {
        this.chatUsers = chatUsers;
    }
}
