package com.imedhealthus.imeddoctors.common.models;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.doctor.models.AcceptedInsurance;
import com.imedhealthus.imeddoctors.doctor.models.Appointment;
import com.imedhealthus.imeddoctors.doctor.models.Award;
import com.imedhealthus.imeddoctors.doctor.models.Certification;
import com.imedhealthus.imeddoctors.doctor.models.ClinicLocation;
import com.imedhealthus.imeddoctors.doctor.models.Doctor;
import com.imedhealthus.imeddoctors.doctor.models.DoctorProfile;
import com.imedhealthus.imeddoctors.doctor.models.EducationDetail;
import com.imedhealthus.imeddoctors.doctor.models.Filter;
import com.imedhealthus.imeddoctors.doctor.models.Language;
import com.imedhealthus.imeddoctors.doctor.models.Membership;
import com.imedhealthus.imeddoctors.doctor.models.ReferenceDetail;
import com.imedhealthus.imeddoctors.doctor.models.Service;
import com.imedhealthus.imeddoctors.doctor.models.SkillTraining;
import com.imedhealthus.imeddoctors.doctor.models.SocialLink;
import com.imedhealthus.imeddoctors.doctor.models.Speciality;
import com.imedhealthus.imeddoctors.doctor.models.StaticData;
import com.imedhealthus.imeddoctors.doctor.models.TrainingProviderDetail;
import com.imedhealthus.imeddoctors.doctor.models.WorkExperience;
import com.imedhealthus.imeddoctors.patient.models.Allergy;
import com.imedhealthus.imeddoctors.patient.models.CurrentMedication;
import com.imedhealthus.imeddoctors.patient.models.Disease;
import com.imedhealthus.imeddoctors.patient.models.EmergencyContact;
import com.imedhealthus.imeddoctors.patient.models.FamilyHistory;
import com.imedhealthus.imeddoctors.patient.models.Guardian;
import com.imedhealthus.imeddoctors.patient.models.Hobby;
import com.imedhealthus.imeddoctors.patient.models.Hospitalization;
import com.imedhealthus.imeddoctors.patient.models.Insurance;
import com.imedhealthus.imeddoctors.patient.models.PastPharmacy;
import com.imedhealthus.imeddoctors.patient.models.PastPhysician;
import com.imedhealthus.imeddoctors.patient.models.PastSurgery;
import com.imedhealthus.imeddoctors.patient.models.Patient;
import com.imedhealthus.imeddoctors.patient.models.PatientProfile;
import com.imedhealthus.imeddoctors.patient.models.PregnanciesHistory;
import com.imedhealthus.imeddoctors.patient.models.PregnancyDetailsHistory;
import com.imedhealthus.imeddoctors.patient.models.PrescribedMedication;
import com.imedhealthus.imeddoctors.patient.models.Prescription;
import com.imedhealthus.imeddoctors.patient.models.SocialHistory;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 1/7/18.
 */

public class SharedData {


    boolean canDrawOverOtherApps = false;
    boolean getCountryCodes;
    private boolean finishCall = false;

    private boolean isCallConnected = false;

    boolean editPatientProfile;
    private String selectedImageURL;
    private boolean isAskingPermissionsOnAcceptingCall;

    public void setEditPatientProfile(boolean editPatientProfile) {
        this.editPatientProfile = editPatientProfile;
    }

    public boolean isEditPatientProfile() {
        return editPatientProfile;
    }

    public void setCallConnected(boolean callConnected) {
        isCallConnected = callConnected;
    }

    public boolean isCallConnected() {
        return isCallConnected;
    }

    public boolean isGetCountryCodes() {
        return getCountryCodes;
    }

    public void setGetCountryCodes(boolean getCountryCodes) {
        this.getCountryCodes = getCountryCodes;
    }


    Uri fullImageUri;

    String selectedDoctorId;

    String selectedDoctorLoginId, selectedPatientLoginId;

    public String getSelectedDoctorLoginId() {
        return selectedDoctorLoginId;
    }

    public void setSelectedDoctorLoginId(String selectedDoctorLoginId) {
        this.selectedDoctorLoginId = selectedDoctorLoginId;
    }

    public String getSelectedPatientLoginId() {
        return selectedPatientLoginId;
    }

    public void setSelectedPatientLoginId(String selectedPatientLoginId) {
        this.selectedPatientLoginId = selectedPatientLoginId;
    }

    public void setCanDrawOverOtherApps(boolean canDrawOverOtherApps) {
        this.canDrawOverOtherApps = canDrawOverOtherApps;
    }

    public boolean isCanDrawOverOtherApps() {
        return canDrawOverOtherApps;
    }

    public String getSelectedDoctorId() {
        return selectedDoctorId;
    }

    public void setSelectedDoctorId(String selectedDoctorId) {
        this.selectedDoctorId = selectedDoctorId;
    }

    public void setFullImageUri(Uri fullImageUri) {
        this.fullImageUri = fullImageUri;
    }

    public Uri getFullImageUri() {
        return fullImageUri;
    }

    private static final SharedData ourInstance = new SharedData();

    public static SharedData getInstance() {
        return ourInstance;
    }

    private boolean isRefreshRequired;
    private PaymentMethod selectedPaymentMethod;
    private Prescription selectedPrescription;
    private ChatItem selectedChatItem;
    private ForumQuestion selectedForumQuestion;
    private String selectedChatDocttorId;
    private BookedAppointment selectedBookedApointment;
    private String openTokCallSessionId;
    private String openTokCallToken;


    private boolean showDoctorProfileEditIcon;
    private int unSeenAppointments = 0;
    private int unSeenConversations = 0;
    private int unSeenNotifications = 0;
    private int unSeenQuestions = 0;
    private int unSeenForumComments = 0;
    private int unSeenQuestionComments = 0;

    private int selectedDoctorProfileTab = 0;

    //Doctor
    private Doctor selectedDoctor;
    private DoctorProfile selectedDoctorProfile;

    private EducationDetail selectedEducationDetail;
    private Speciality selectedSpeciality;
    private SkillTraining selectedSkillTraining;
    private WorkExperience selectedWorkExperience;
    private ClinicLocation selectedClinicLocation;
    private Service selectedService;
    private ReferenceDetail selectedReferenceDetail;
    private AcceptedInsurance selectedAcceptedInsurance;
    private Language selectedLanguage;
    private List<SocialLink> selectedSocialLink;
    private TrainingProviderDetail selectedTrainingProviderDetail;
    private Certification selectedCertification;
    private Membership selectedMembership;
    private Award selectedAward;
    private Prescription selectPrescriptionForPharmacy;
    private Filter selectedFilter;

    private CallData callData;
    //Appointment
    private Appointment selectedAppointment;
    private String bookAppointmentReason, bookAppointmentDescription, postQuestionText;
    private String receiverId, receiverName, receiverImageUrl;
    private int postQuestionSpecialityIndex;
    private boolean shouldContinueBooking;
    private boolean isPostQuestion;
    private boolean isPublic;
    private boolean isCallActive = false;
    private boolean isCallResumed = false;
    private boolean isDoctorSelected = false;


    //Patient
    private Patient selectedPatient;
    private PatientProfile selectedPatientProfile;
    private String selectedPatientId;
    private String selectedPatientName;

    private Insurance selectedInsurance;
    private EmergencyContact selectedEmergencyContact;
    private Guardian selectedGuardian;

    private Disease selectedDisease;
    private CurrentMedication selectedCurrentMedication;
    private PastPhysician selectedPastPhysician;
    private PastSurgery selectedPastSurgery;
    private Allergy selectedAllergy;
    private PastPharmacy selectedPastPharmacy;
    private Hospitalization selectedHospitalization;
    private FamilyHistory selectedFamilyHistory;
    private Hobby selectedHobby;
    private PrescribedMedication selectedPrescribedMedication;
    private SocialHistory selectedSocialHistory;
    private PregnanciesHistory selectedPregnanciesHistory;
    private PregnancyDetailsHistory selectedPregnancyDetailsHistory;
    private Boolean refreshViaServerHit = false;
    private String selectedLoginId, selectedVerificationCode;
    private CallSignal selectedCallSignal;



    public boolean isShowDoctorProfileEditIcon() {
        return showDoctorProfileEditIcon;
    }

    public void setShowDoctorProfileEditIcon(boolean showDoctorProfileEditIcon) {
        this.showDoctorProfileEditIcon = showDoctorProfileEditIcon;
    }

    public String getSelectedLoginId() {
        return selectedLoginId;
    }


    public BookedAppointment getSelectedBookedApointment() {
        return selectedBookedApointment;
    }

    public void setSelectedBookedApointment(BookedAppointment selectedBookedApointment) {
        this.selectedBookedApointment = selectedBookedApointment;
    }

    public void setSelectedLoginId(String selectedLoginId) {
        this.selectedLoginId = selectedLoginId;
    }

    public String getSelectedVerificationCode() {
        return selectedVerificationCode;
    }

    public void setSelectedVerificationCode(String selectedVerificationCode) {
        this.selectedVerificationCode = selectedVerificationCode;
    }


    public void setSelectedDoctorProfileTab(int selectedDoctorProfileTab) {
        this.selectedDoctorProfileTab = selectedDoctorProfileTab;
    }

    public int getSelectedDoctorProfileTab() {
        return selectedDoctorProfileTab;
    }

    //Static data
    List<StaticData> specialityList = new ArrayList<>();
    List<StaticData> insuranceList = new ArrayList<>();
    List<StaticData> languageList = new ArrayList<>();
    List<Country> countries = new ArrayList<>();

//    private List<Country> countriesList = new ArrayList<>();
//    private List<State> statesList = new ArrayList<>();
//    private List<City> citiesList = new ArrayList<>();


    private List<Country> countriesList = new MyDatabase(ApplicationManager.getContext()).getCountries();
    private List<Country> PracticingCountriesList = new MyDatabase(ApplicationManager.getContext()).getPracticeCountries();

    private List<State> statesList = new MyDatabase(ApplicationManager.getContext()).getStates();
    private List<City> citiesList = new MyDatabase(ApplicationManager.getContext()).getCities();

    public Boolean getRefreshViaServerHit() {
        boolean value = refreshViaServerHit;
        refreshViaServerHit = false;
        return value;
    }

    public void setRefreshViaServerHit(Boolean refreshViaServerHit) {
        this.refreshViaServerHit = refreshViaServerHit;
    }

    public Filter getSelectedFilter() {
        return selectedFilter;
    }

    public void setSelectedFilter(Filter selectedFilter) {
        this.selectedFilter = selectedFilter;
    }

    public void setCountriesList(List<Country> countriesList) {
        this.countriesList = countriesList;
    }

    public void setStatesList(List<State> statesList) {
        this.statesList = statesList;
    }

    public void setCitiesList(List<City> citiesList) {
        this.citiesList = citiesList;
    }

    public CallSignal getSelectedCallSignal() {
        return selectedCallSignal;
    }

    public void setSelectedCallSignal(CallSignal selectedCallSignal) {
        this.selectedCallSignal = selectedCallSignal;
    }

    public List<Country> getCountriesList() {
        return countriesList;
    }

    public List<State> getStatesList() {
        return statesList;
    }

    public List<City> getCitiesList() {

        return citiesList;
    }

    public Country getCountryByID(String CountryId) {

        Country country = null;
        for (int i = 0; i < countriesList.size(); i++) {
            if (countriesList.get(i).countryId == Integer.parseInt(CountryId))
                country = countriesList.get(i);
        }

        return country;
    }

    public State getStateByID(String stateId) {
        State state = null;
        for (int i = 0; i < statesList.size(); i++) {
            if (statesList.get(i).stateId.equalsIgnoreCase(stateId))
                state = statesList.get(i);
        }

        return state;
    }

    public int getStateIndexInList(List<State> stateList, String stateId) {

        for (int i = 0; i < stateList.size(); i++) {
            if (stateList.get(i).getStateId().equalsIgnoreCase(stateId)) {
                return i;
            }
        }
        return 0;
    }

    public int getCountryIndexInList(List<Country> countryList, String countryIdStr) {

        int countryId = 0;
        try {
            countryId = Integer.parseInt(countryIdStr.replace("null", "0"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        for (int i = 0; i < countryList.size(); i++) {
            if (countryList.get(i).countryId == countryId) {
                return i;
            }
        }
        return 0;

    }

    public int getCityIndexInList(List<City> cities, String cityIdStr) {

        int cityId = 0;
        try {
            cityId = Integer.parseInt(cityIdStr.replace("null", "0"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        for (int i = 0; i < cities.size(); i++) {
            if (Integer.parseInt(cities.get(i).cityId) == cityId) {
                return i;
            }
        }
        return 0;

    }

    public int getIndexInList(List<StaticData> list, String value) {


        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getValue().equalsIgnoreCase(value)) {
                return i;
            }
        }
        return 0;

    }

    public City getCityByID(String CityId) {
        for (int i = 0; i < citiesList.size(); i++) {
            if (citiesList.get(i).cityId.equalsIgnoreCase(CityId))
                return citiesList.get(i);
        }

        return null;
    }

    public String getSelectedChatDocttorId() {
        return selectedChatDocttorId;
    }

    public void setSelectedChatDocttorId(String selectedChatDocttorId) {
        this.selectedChatDocttorId = selectedChatDocttorId;
    }

    public List<State> getStatesListAgainstCountryByCountryId(String CountryId) {
        return State.getStatesAgainstCountryId(statesList, CountryId);
    }


    public List<City> getCitiesListByStateId(String stateId) {
        return City.getCitiesAgainstCountryId(citiesList, stateId);
    }


    public void getStaticData() {

        String userLoginId = "0";
        if (CacheManager.getInstance().isUserLoggedIn()) {
            userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        }

        WebServicesHandler.instance.getStaticData(userLoginId, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                if (response.status()) {


                    String session = response.optString("SessionId");
                    String token = response.optString("Token");
                    CacheManager.getInstance().setChatSessionId(session);
                    CacheManager.getInstance().setChatToken(token);
                    OpenTokHandler.getInstance().initializeSession();

                    JSONArray speciality = response.getJSONArray("Speciality");
                    List<StaticData> specialityList = new ArrayList<>();
                    specialityList.add(new StaticData("", "Specialty"));
                    if (speciality != null)
                        for (int i = 0; i < speciality.length(); i++) {

                            StaticData staticData = new StaticData(speciality.getJSONObject(i).getString("Id"), speciality.getJSONObject(i).getString("Title"));
                            specialityList.add(staticData);
                        }
                    SharedData.getInstance().setSpecialityList(specialityList);

                    List<StaticData> insuranceList = new ArrayList<>();
                    insuranceList.add(new StaticData("", "Select Insurace"));
                    JSONArray insurance = response.getJSONArray("Insurance");
                    if (insurance != null)
                        for (int i = 0; i < insurance.length(); i++) {

                            StaticData staticData = new StaticData(insurance.getJSONObject(i).getString("Id"), insurance.getJSONObject(i).getString("Title"));
                            insuranceList.add(staticData);
                        }
                    SharedData.getInstance().setInsuranceList(insuranceList);

                    List<StaticData> languageList = new ArrayList<>();
                    JSONArray language = response.getJSONArray("Language");
                    if (language != null)
                        for (int i = 0; i < language.length(); i++) {

                            StaticData staticData = new StaticData(language.getJSONObject(i).getString("Id"), language.getJSONObject(i).getString("Title"));
                            languageList.add(staticData);
                        }
                    SharedData.getInstance().setLanguageList(languageList);

                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

            }
        });

    }

    public void updateUserDeviceToken() {

        if (!CacheManager.getInstance().isUserLoggedIn()) {
            return;
        }
        String userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        String deviceToken = FirebaseInstanceId.getInstance().getToken();

        if (deviceToken == null)
            return;
        WebServicesHandler.instance.updateDeviceToken(userLoginId, deviceToken, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                if (!response.status()) {
                    Log.e("iMed", "updateUserDeviceToken: " + response.message());
                    return;
                }
                Log.e("iMed", "updateUserDeviceToken: success");

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                Log.e("iMed", "updateUserDeviceToken: failure");
            }
        });

    }


    public List<StaticData> getSpecialityList() {
        return specialityList;
    }

    public void setSpecialityList(List<StaticData> specialityList) {
        this.specialityList = specialityList;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    public CallData getCallData() {
        return callData;
    }

    public void setCallData(CallData callData) {
        this.callData = callData;
    }

    public Prescription getSelectPrescriptionForPharmacy() {
        return selectPrescriptionForPharmacy;
    }

    public void setSelectPrescriptionForPharmacy(Prescription selectPrescriptionForPharmacy) {
        this.selectPrescriptionForPharmacy = selectPrescriptionForPharmacy;
    }

    public SocialHistory getSelectedSocialHistory() {
        return selectedSocialHistory;
    }

    public void setSelectedSocialHistory(SocialHistory selectedSocialHistory) {
        this.selectedSocialHistory = selectedSocialHistory;
    }

    public boolean isDoctorSelected() {
        return isDoctorSelected;
    }

    public void setDoctorSelected(boolean doctorSelected) {
        isDoctorSelected = doctorSelected;
    }

    public List<StaticData> getInsuranceList() {
        return insuranceList;
    }

    public EmergencyContact getSelectedEmergencyContact() {
        return selectedEmergencyContact;
    }

    public void setSelectedEmergencyContact(EmergencyContact selectedEmergencyContact) {
        this.selectedEmergencyContact = selectedEmergencyContact;
    }

    public Guardian getSelectedGuardian() {
        return selectedGuardian;
    }

    public void setSelectedGuardian(Guardian selectedGuardian) {
        this.selectedGuardian = selectedGuardian;
    }

    public void setInsuranceList(List<StaticData> insuranceList) {
        this.insuranceList = insuranceList;
    }

    public List<StaticData> getLanguageList() {
        return languageList;
    }

    public void setLanguageList(List<StaticData> languageList) {
        this.languageList = languageList;
    }

    private SharedData() {

    }

    public Doctor getSelectedDoctor() {
        return selectedDoctor;
    }

    public void setSelectedDoctor(Doctor selectedDoctor) {
        this.selectedDoctor = selectedDoctor;
    }

    public Appointment getSelectedAppointment() {
        return selectedAppointment;
    }

    public void setSelectedAppointment(Appointment selectedAppointment) {
        this.selectedAppointment = selectedAppointment;
    }


    public Patient getSelectedPatient() {
        return selectedPatient;
    }

    public void setSelectedPatient(Patient selectedPatient) {
        this.selectedPatient = selectedPatient;
    }

    public PatientProfile getSelectedPatientProfile() {
        return selectedPatientProfile;
    }

    public void setSelectedPatientProfile(PatientProfile selectedPatientProfile) {
        this.selectedPatientProfile = selectedPatientProfile;
    }

    public String getSelectedPatientId() {
        return selectedPatientId;
    }

    public void setSelectedPatientId(String selectedPatientId) {
        this.selectedPatientId = selectedPatientId;
    }

    public String getSelectedPatientName() {
        return selectedPatientName;
    }

    public void setSelectedPatientName(String selectedPatientName) {
        this.selectedPatientName = selectedPatientName;
    }

    public PrescribedMedication getSelectedPrescribedMedication() {
        return selectedPrescribedMedication;
    }

    public void setSelectedPrescribedMedication(PrescribedMedication selectedPrescribedMedication) {
        this.selectedPrescribedMedication = selectedPrescribedMedication;
    }

    public Insurance getSelectedInsurance() {
        return selectedInsurance;
    }

    public void setSelectedInsurance(Insurance selectedInsurance) {
        this.selectedInsurance = selectedInsurance;
    }

    public boolean isRefreshRequired() {
        boolean value = isRefreshRequired;
        isRefreshRequired = false;
        return value;
    }

    public void setRefreshRequired(boolean refreshRequired) {
        isRefreshRequired = refreshRequired;
    }

    public String getBookAppointmentReason() {
        return bookAppointmentReason;
    }

    public void setBookAppointmentReason(String bookAppointmentReason) {
        this.bookAppointmentReason = bookAppointmentReason;
    }

    public String getBookAppointmentDescription() {
        return bookAppointmentDescription;
    }

    public void setBookAppointmentDescription(String bookAppointmentDescription) {
        this.bookAppointmentDescription = bookAppointmentDescription;
    }

    public String getPostQuestionText() {
        return postQuestionText;
    }

    public void setPostQuestionText(String postQuestionText) {
        this.postQuestionText = postQuestionText;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverImageUrl() {
        return receiverImageUrl;
    }

    public void setReceiverImageUrl(String receiverImageUrl) {
        this.receiverImageUrl = receiverImageUrl;
    }

    public int getPostQuestionSpecialityIndex() {
        return postQuestionSpecialityIndex;
    }

    public void setPostQuestionSpecialityIndex(int postQuestionSpecialityIndex) {
        this.postQuestionSpecialityIndex = postQuestionSpecialityIndex;
    }

    public boolean shouldContinueBooking() {
        return shouldContinueBooking;
    }

    public void setShouldContinueBooking(boolean shouldContinueBooking) {
        this.shouldContinueBooking = shouldContinueBooking;
    }

    public boolean isPostQuestion() {
        return isPostQuestion;
    }

    public void setPostQuestion(boolean postQuestion) {
        isPostQuestion = postQuestion;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public boolean isCallActive() {
        return isCallActive;
    }

    public void setCallActive(boolean callActive) {
        isCallActive = callActive;
    }

    public boolean isCallResumed() {
        return isCallResumed;
    }

    public void setCallResumed(boolean callResumed) {
        isCallResumed = callResumed;
    }

    public PaymentMethod getSelectedPaymentMethod() {
        return selectedPaymentMethod;
    }

    public void setSelectedPaymentMethod(PaymentMethod selectedPaymentMethod) {
        this.selectedPaymentMethod = selectedPaymentMethod;
    }

    public Prescription getSelectedPrescription() {
        return selectedPrescription;
    }

    public void setSelectedPrescription(Prescription selectedPrescription) {
        this.selectedPrescription = selectedPrescription;
    }

    public Disease getSelectedDisease() {
        return selectedDisease;
    }

    public void setSelectedDisease(Disease selectedDisease) {
        this.selectedDisease = selectedDisease;
    }

    public CurrentMedication getSelectedCurrentMedication() {
        return selectedCurrentMedication;
    }

    public void setSelectedCurrentMedication(CurrentMedication selectedCurrentMedication) {
        this.selectedCurrentMedication = selectedCurrentMedication;
    }

    public PastPhysician getSelectedPastPhysician() {
        return selectedPastPhysician;
    }

    public void setSelectedPastPhysician(PastPhysician selectedPastPhysician) {
        this.selectedPastPhysician = selectedPastPhysician;
    }

    public PastSurgery getSelectedPastSurgery() {
        return selectedPastSurgery;
    }

    public void setSelectedPastSurgery(PastSurgery selectedPastSurgery) {
        this.selectedPastSurgery = selectedPastSurgery;
    }

    public Allergy getSelectedAllergy() {
        return selectedAllergy;
    }

    public void setSelectedAllergy(Allergy selectedAllergy) {
        this.selectedAllergy = selectedAllergy;
    }

    public PastPharmacy getSelectedPastPharmacy() {
        return selectedPastPharmacy;
    }

    public void setSelectedPastPharmacy(PastPharmacy selectedPastPharmacy) {
        this.selectedPastPharmacy = selectedPastPharmacy;
    }

    public Hospitalization getSelectedHospitalization() {
        return selectedHospitalization;
    }

    public void setSelectedHospitalization(Hospitalization selectedHospitalization) {
        this.selectedHospitalization = selectedHospitalization;
    }

    public FamilyHistory getSelectedFamilyHistory() {
        return selectedFamilyHistory;
    }

    public void setSelectedFamilyHistory(FamilyHistory selectedFamilyHistory) {
        this.selectedFamilyHistory = selectedFamilyHistory;
    }

    public Hobby getSelectedHobby() {
        return selectedHobby;
    }

    public void setSelectedHobby(Hobby selectedHobby) {
        this.selectedHobby = selectedHobby;
    }

    public DoctorProfile getSelectedDoctorProfile() {
        return selectedDoctorProfile;
    }

    public void setSelectedDoctorProfile(DoctorProfile selectedDoctorProfile) {
        this.selectedDoctorProfile = selectedDoctorProfile;
    }

    public EducationDetail getSelectedEducationDetail() {
        return selectedEducationDetail;
    }

    public void setSelectedEducationDetail(EducationDetail selectedEducationDetail) {
        this.selectedEducationDetail = selectedEducationDetail;
    }

    public Speciality getSelectedSpeciality() {
        return selectedSpeciality;
    }

    public void setSelectedSpeciality(Speciality selectedSpeciality) {
        this.selectedSpeciality = selectedSpeciality;
    }

    public SkillTraining getSelectedSkillTraining() {
        return selectedSkillTraining;
    }

    public void setSelectedSkillTraining(SkillTraining selectedSkillTraining) {
        this.selectedSkillTraining = selectedSkillTraining;
    }

    public WorkExperience getSelectedWorkExperience() {
        return selectedWorkExperience;
    }

    public void setSelectedWorkExperience(WorkExperience selectedWorkExperience) {
        this.selectedWorkExperience = selectedWorkExperience;
    }

    public ClinicLocation getSelectedClinicLocation() {
        return selectedClinicLocation;
    }

    public void setSelectedClinicLocation(ClinicLocation selectedClinicLocation) {
        this.selectedClinicLocation = selectedClinicLocation;
    }

    public Service getSelectedService() {
        return selectedService;
    }

    public void setSelectedService(Service selectedService) {
        this.selectedService = selectedService;
    }

    public ReferenceDetail getSelectedReferenceDetail() {
        return selectedReferenceDetail;
    }

    public void setSelectedReferenceDetail(ReferenceDetail selectedReferenceDetail) {
        this.selectedReferenceDetail = selectedReferenceDetail;
    }

    public AcceptedInsurance getSelectedAcceptedInsurance() {
        return selectedAcceptedInsurance;
    }

    public void setSelectedAcceptedInsurance(AcceptedInsurance selectedAcceptedInsurance) {
        this.selectedAcceptedInsurance = selectedAcceptedInsurance;
    }

    public Language getSelectedLanguage() {
        return selectedLanguage;
    }

    public void setSelectedLanguage(Language selectedLanguage) {
        this.selectedLanguage = selectedLanguage;
    }

    public List<SocialLink> getSelectedSocialLink() {
        return selectedSocialLink;
    }

    public void setSelectedSocialLink(List<SocialLink> selectedSocialLink) {
        this.selectedSocialLink = selectedSocialLink;
    }

    public TrainingProviderDetail getSelectedTrainingProviderDetail() {
        return selectedTrainingProviderDetail;
    }

    public void setSelectedTrainingProviderDetail(TrainingProviderDetail selectedTrainingProviderDetail) {
        this.selectedTrainingProviderDetail = selectedTrainingProviderDetail;
    }

    public Certification getSelectedCertification() {
        return selectedCertification;
    }

    public void setSelectedCertification(Certification selectedCertification) {
        this.selectedCertification = selectedCertification;
    }

    public Membership getSelectedMembership() {
        return selectedMembership;
    }

    public void setSelectedMembership(Membership selectedMembership) {
        this.selectedMembership = selectedMembership;
    }

    public Award getSelectedAward() {
        return selectedAward;
    }

    public void setSelectedAward(Award selectedAward) {
        this.selectedAward = selectedAward;
    }

    public ChatItem getSelectedChatItem() {
        return selectedChatItem;
    }

    public void setSelectedChatItem(ChatItem selectedChatItem) {
        this.selectedChatItem = selectedChatItem;
    }

    public ForumQuestion getSelectedForumQuestion() {
        return selectedForumQuestion;
    }

    public void setSelectedForumQuestion(ForumQuestion selectedForumQuestion) {
        this.selectedForumQuestion = selectedForumQuestion;
    }

    public PregnanciesHistory getSelectedPregnanciesHistory() {
        return selectedPregnanciesHistory;
    }

    public void setSelectedPregnanciesHistory(PregnanciesHistory selectedPregnanciesHistory) {
        this.selectedPregnanciesHistory = selectedPregnanciesHistory;
    }

    public PregnancyDetailsHistory getSelectedPregnancyDetailsHistory() {
        return selectedPregnancyDetailsHistory;
    }

    public void setSelectedPregnancyDetailsHistory(PregnancyDetailsHistory selectedPregnancyDetailsHistory) {
        this.selectedPregnancyDetailsHistory = selectedPregnancyDetailsHistory;
    }

    public String getOpenTokCallSessionId() {

        return openTokCallSessionId;

    }

    public void setOpenTokCallSessionId(String openTokCallSessionId) {
        this.openTokCallSessionId = openTokCallSessionId;
    }

    public String getOpenTokCallToken() {

        return openTokCallToken;

        //return Constants.OpenTokConfig.token;
    }

    public void setOpenTokCallToken(String openTokCallToken) {
        this.openTokCallToken = openTokCallToken;
    }

    public int getUnSeenAppointments() {
        return unSeenAppointments;
    }

    public void setUnSeenAppointments(int unSeenAppointments) {
        this.unSeenAppointments = unSeenAppointments;
    }

    public int getUnSeenConversations() {
        return unSeenConversations;
    }

    public void setUnSeenConversations(int unSeenConversations) {
        this.unSeenConversations = unSeenConversations;
    }

    public int getUnSeenNotifications() {
        return unSeenNotifications;
    }

    public void setUnSeenNotifications(int unSeenNotifications) {
        this.unSeenNotifications = unSeenNotifications;
    }

    public int getUnSeenQuestions() {
        return unSeenQuestions;
    }

    public void setUnSeenQuestions(int unSeenQuestions) {
        this.unSeenQuestions = unSeenQuestions;
    }

    public int getUnSeenForumComments() {
        return unSeenForumComments;
    }

    public void setUnSeenForumComments(int unSeenForumComments) {
        this.unSeenForumComments = unSeenForumComments;
    }

    public int getUnSeenQuestionComments() {
        return unSeenQuestionComments;
    }

    public void setUnSeenQuestionComments(int unSeenQuestionComments) {
        this.unSeenQuestionComments = unSeenQuestionComments;
    }

    private boolean isCallFeedback = false;

    public void setCallFeedback(boolean b) {
        isCallFeedback = false;
    }

    public boolean isCallFeedback() {
        return isCallFeedback;
    }

    public void setFinishCall(boolean finishCall) {
        this.finishCall = finishCall;
    }

    public boolean isFinishCall() {
        return finishCall;
    }

    public String getSelectedImageURL() {
        return selectedImageURL;
    }

    public void setSelectedImageURL(String selectedImageURL) {
        this.selectedImageURL = selectedImageURL;
    }

    public void setAskingPermissionsOnAcceptingCall(boolean b) {
        this.isAskingPermissionsOnAcceptingCall = b;
    }

    public boolean isAskingPermissionsOnAcceptingCall() {
        return isAskingPermissionsOnAcceptingCall;
    }
}
