package com.imedhealthus.imeddoctors.common.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.imedhealthus.imeddoctors.application.ApplicationManager;

/**
 * Created by umair on 1/1/18.
 */

public class PreferenceUtils {

    public static void setValueForKey(String value, String key) {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ApplicationManager.getContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();

    }

    public static String getValueForKey(String key) {

        return getValueForKey(key, null);

    }

    public static String getValueForKey(String key, String defaultValue) {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ApplicationManager.getContext());
        return sharedPref.getString(key, defaultValue);

    }

}
