package com.imedhealthus.imeddoctors.common.widgets;


import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by umair on 1/6/18.
 */

public class MarginChangeAnim extends Animation {

    private View view;
    private int startLeft, targetLeft;

    public MarginChangeAnim(View view, int startLeft, int targetLeft) {

        super();
        this.view = view;
        this.startLeft = startLeft;
        this.targetLeft = targetLeft;

    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {

        int left = (int) (startLeft + (targetLeft - startLeft) * interpolatedTime);

        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        params.setMargins(left, 0, 0, 0);

        view.setLayoutParams(params);

    }

}
