package com.imedhealthus.imeddoctors.common.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.Html;
import android.text.TextPaint;
import android.text.format.DateUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Scroller;

import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.auth.activities.LandingActivity;
import com.imedhealthus.imeddoctors.common.models.SuggestionListItem;
import com.imedhealthus.imeddoctors.doctor.models.StaticData;

import org.apache.commons.lang.StringEscapeUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by umair on 1/5/18.
 */

public class GenericUtils {
    static SimpleDateFormat dateFormatTimeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    static SimpleDateFormat TimeStamp = new SimpleDateFormat("HH:mm:ss");

    public static String getTimeInStringFormat(long startingSeconds) {


        long hours = startingSeconds / 3600;
        long minutes = (startingSeconds % 3600) / 60;
        long seconds = startingSeconds % 60;

        String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
        return timeString;
    }

    public static void finishWithDelay(final Activity activity, long delay) {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                activity.finish();
            }
        }, delay);
    }

    public static int getValueFromListByCode(int id, String code) {
        int i = -1;
        for (String cc : ApplicationManager.getContext().getResources().getStringArray(id)) {
            i++;
            if (cc.equalsIgnoreCase(code))
                break;
        }
        return i;
    }


    public static void exitApp(Activity activity) {

        Intent intent = new Intent(activity, LandingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Exit", true);
        activity.startActivity(intent);
        activity.finish();

    }

    /**
     * This method checks whether the start Date is not after the end date. If yes it returns -ve value otherwise 0 or +ve
     *
     * @param startDate start Date
     * @param endDate   end date
     * @return
     */
    public static boolean validateStartAndEndDateOrder(Date startDate, Date endDate) {
        if (startDate != null && endDate != null && startDate.compareTo(endDate) >= 0) {
            return false;
        } else
            return true;
    }


    public static void setEditTextScrollable(EditText editText, Context context) {
        editText.setScroller(new Scroller(context));
        editText.setVerticalScrollBarEnabled(true);
        editText.setMovementMethod(new ScrollingMovementMethod());

        editText.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });

    }

    public static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isValidPhone(CharSequence target) {
        Pattern pattern = Pattern.compile("\\.*+03\\d{9}"); // {9}
        Matcher matcher = pattern.matcher(target);
        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    //keyboard
    public static void hideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }


    public static float convertDpToPixels(Context context, float dp) {
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static int convertSpToPixels(Context context, float sp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }

    public static int getScreenWidth(Activity activity) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;

    }

    public static int getScreenHeight(Activity activity) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;

    }


    public static Date getDateWithMonthDiff(Date date, int diff) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, diff);
        return calendar.getTime();

    }

    public static ArrayList<SuggestionListItem> getFilteredSuggestionListItems(ArrayList<SuggestionListItem> suggestionListItems, String searchQuery) {
        ArrayList<SuggestionListItem> filteredResults = new ArrayList<>();
        for (SuggestionListItem suggestionListItem : suggestionListItems) {
            if (suggestionListItem.getSuggestionText().toLowerCase().contains(searchQuery.toLowerCase())) {
                filteredResults.add(suggestionListItem);
            }
        }

        return filteredResults;
    }


    public static String decodeHtml(String encodedHtml) {
        String decodedXML = StringEscapeUtils.unescapeHtml(encodedHtml);


        return Html.fromHtml(decodedXML).toString();
    }

    public static Date getDateWithHourDiff(Date date, int diff) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, diff);
        return calendar.getTime();

    }


    public static BitmapImageViewTarget getRoundedImageTarget(@NonNull final Context context, @NonNull final ImageView imageView,
                                                              final float radius) {
        return new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(final Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCornerRadius(radius);
                imageView.setImageDrawable(circularBitmapDrawable);
            }
        };

    }

    public static String getDayOfMonthSuffix(final int n) {
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    public static String getTimeDateString(Date date) {
        String s = "";
        s = dateFormatTimeStamp.format(date);
        return s;
    }

    public static long getTimeInLong(String time) {

        try {
            Date date = TimeStamp.parse(time);
            return date.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return (new Date()).getTime();
    }

    public static long getTimeDateInLong(String time) {

        try {

            Date date = dateFormatTimeStamp.parse(time);
            return date.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return (new Date()).getTime();
    }

    public static Date getDateFromString(String time) {

        try {
            return dateFormatTimeStamp.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public static String getUTCBasedTime(Date date) {

        dateFormatTimeStamp.setTimeZone(TimeZone.getTimeZone("UTC"));
        String covertedDate = dateFormatTimeStamp.format(date);
        return covertedDate;

    }

    public static long getLocalTimeFromUTC(String time) {

        SimpleDateFormat dateFormatTimeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        dateFormatTimeStamp.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {

            Date date = dateFormatTimeStamp.parse(time);

            dateFormatTimeStamp.setTimeZone(TimeZone.getDefault());
            String formattedDate = dateFormatTimeStamp.format(date);
            return getTimeDateInLong(formattedDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return (new Date()).getTime();

    }

    public static String getLocalTimeInStringFromUTC(String time) {

        SimpleDateFormat dateFormatTimeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        dateFormatTimeStamp.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {

            Date date = dateFormatTimeStamp.parse(time);

            dateFormatTimeStamp.setTimeZone(TimeZone.getDefault());
            String formattedDate = dateFormatTimeStamp.format(date);
            return formattedDate;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date().toString();

    }

    public static long getUTCBasedTime(String time) {

        SimpleDateFormat dateFormatTimeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        dateFormatTimeStamp.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {

            Date date = dateFormatTimeStamp.parse(time);
            return date.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return (new Date()).getTime();

    }

    public static String getPassedTime(long timeStamp) {
        return (String) DateUtils.getRelativeTimeSpanString(timeStamp, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
    }

    public static String formatDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
        return dateFormat.format(date);
    }

    public static Date unFormatDate(String string) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
        try {
            return dateFormat.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public static String formatTime(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        return dateFormat.format(date);
    }

    public static Date unFormatTime(String string) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        try {
            return dateFormat.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public static String formatDateTime(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a MMM dd, yyyy");
        return dateFormat.format(date);
    }

    public static int getTextHeight(String text, int maxWidth, float textSize, Typeface typeface,
                                    int maxLinesAllowed) {
        TextPaint paint = new TextPaint(Paint.ANTI_ALIAS_FLAG | Paint.SUBPIXEL_TEXT_FLAG);
        paint.setTextSize(textSize);
        paint.setTypeface(typeface);

        int lineCount = 0;

        int index = 0;
        int length = text.length();

        while (index < length - 1) {
            index += paint.breakText(text, index, length, true, maxWidth, null);
            lineCount++;
        }

        lineCount = Math.min(lineCount, maxLinesAllowed);
        Rect bounds = new Rect();
        paint.getTextBounds("Py", 0, 2, bounds);
        return (int) Math.floor(lineCount * bounds.height());
    }

    public static String getImageUrl(String url) {
        if (url.contains(Constants.URLS.BaseApis)) {
            return url;
        }
        return Constants.URLS.BaseApis + url.replace("..", "");
    }

    public static int getColor(int resourceId) {
        return ApplicationManager.getContext().getResources().getColor(resourceId);
    }

    public static boolean containKey(String key, List<StaticData> list) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).compaireKey(key))
                return true;
        }
        return false;
    }

    public static String getValueFromKey(String key, List<StaticData> list) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).compaireKey(key))
                return list.get(i).getValue();
        }
        return null;
    }


    public static boolean isNetworkNotAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return !(activeNetworkInfo != null && activeNetworkInfo.isConnected());
    }

    /**
     * Open another app.
     *
     * @param context     current Context, like Activity, App, or Service
     * @param packageName the full package name of the app to open
     * @return true if likely successful, false if unsuccessful
     */
    public static boolean openApp(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        try {
            Intent i = manager.getLaunchIntentForPackage(packageName);
            if (i == null) {
                return false;
                //throw new ActivityNotFoundException();
            }
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            context.startActivity(i);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }

    public static String decodeToHtml(String html) {
        String convertedHtml = html;
        if (convertedHtml.contains("lt;"))
            while (!convertedHtml.contains("<")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    convertedHtml = Html.fromHtml(convertedHtml, Html.FROM_HTML_MODE_LEGACY).toString();
                } else {
                    convertedHtml = Html.fromHtml(convertedHtml).toString();
                }
            }
        return convertedHtml;
    }

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        //final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    public static String getCurrentTimeStamp() {
        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String format = s.format(new Date());
        return format;

    }

    public static String getCurrentTimeInString() {
        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String currentTime = s.format(new Date().getTime());
        return currentTime;

    }

    public static long getDateTimeDifferenceInMilliSecond(String startTime) {

        if (startTime != null && !startTime.equals("")) {
            try {
                SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                String endTime = s.format(new Date());
                DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                Date startDateTime = (Date) formatter.parse(startTime);
                Date endDateTime = (Date) formatter.parse(endTime);
                return (endDateTime.getTime() - startDateTime.getTime());

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return Constants.TWO_HOURS_DIFFERENCE_IN_MILLISECOND + 1;
    }


}
