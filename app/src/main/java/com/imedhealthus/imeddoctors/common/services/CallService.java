package com.imedhealthus.imeddoctors.common.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.CallActivity;
import com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler;
import com.imedhealthus.imeddoctors.common.listeners.CallStatusListener;
import com.imedhealthus.imeddoctors.common.listeners.TimerListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.CallSignal;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.NotificationUtils;
import com.imedhealthus.imeddoctors.common.utils.ScreenSharingCapturer;
import com.opentok.android.BaseVideoRenderer;
import com.opentok.android.Connection;
import com.opentok.android.OpentokError;
import com.opentok.android.Publisher;
import com.opentok.android.PublisherKit;
import com.opentok.android.Session;
import com.opentok.android.Stream;
import com.opentok.android.Subscriber;
import com.opentok.android.SubscriberKit;

import java.util.Date;

import static android.app.NotificationManager.IMPORTANCE_HIGH;
import static com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler.CALL_END;
import static com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler.SIGNAL_TYPE_CALL_CANCEL;

public class CallService extends Service implements TimerListener {

    public static ScreenSharingCapturer screenSharingCapturer;
    private WindowManager mWindowManager;
    private View mFloatingView;
    static Subscriber subscriber;
    static Publisher publisher;
    String notificationTitle;
    String notificationText;
    public static final String ACTION_START_FOREGROUND_SERVICE = "ACTION_START_FOREGROUND_SERVICE";
    public static final String ACTION_STOP_FOREGROUND_SERVICE = "ACTION_STOP_FOREGROUND_SERVICE";
    String CHANNEL_ONE_ID = "com.mylivedoctor";
    String CHANNEL_ONE_NAME = "MyLiveDoctors";
    public static boolean isServiceRunning = false;
    public static boolean IS_AUDIO_CALL = false;
    public static boolean IS_VIDEO_CALL = false;
    public static boolean IS_CALL_CONNECTED = false;
    public static final String NOTIFICATION_TITLE = "notification_title";
    public static final String NOTIFICATION_TEXT = "notification_text";
    private Session session;
    private Ringtone ringtone;
    private long startTime;
    private Handler customHandler = new Handler();
    Timer timer;
    FrameLayout frameLayout;
    ImageView ivProfile;
    private String CALL_TAG = "Call Tag";


    public static void setSubscriber(Subscriber subscriber) {
        CallService.subscriber = subscriber;
    }

    public static Subscriber getSubscriber() {
        return subscriber;
    }


    public static void setPublisher(Publisher publisher) {
        CallService.publisher = publisher;
    }

    public static Publisher getPublisher() {
        return publisher;
    }

    public CallService() {
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String extra = intent.getStringExtra(Constants.TAG);
            if (extra != null) {
                if (extra.equals(Constants.END_CALL))
                    endCall();
                else if (extra.equals(Constants.OPEN_POP_UP_WINDOW)) {
                    if (mFloatingView != null) {
                        mFloatingView.setVisibility(View.VISIBLE);
                        getVideoFromSubscriber();
                    }
                } else if (extra.equals(Constants.CLOSE_POP_UP_WINDOW)) {
                    if (mFloatingView != null) {
                        mFloatingView.setVisibility(View.GONE);

                    }
                }
            }

        }
    };

    private void getVideoFromSubscriber() {
        if (subscriber != null) {
            if (subscriber.getView().getParent() != null)
                ((ViewGroup) subscriber.getView().getParent()).removeView(subscriber.getView());
            if (publisher.getPublishVideo()) {
                if (frameLayout != null)
                    frameLayout.addView(subscriber.getView());
                if (ivProfile != null)
                    ivProfile.setVisibility(View.GONE);
            } else {
                if (ivProfile != null)
                    ivProfile.setVisibility(View.VISIBLE);
            }
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //Inflate the floating view layout we created

        timer = new Timer(0, this);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter(Constants.BROADCAST_EVENT));
        if (SharedData.getInstance().isCanDrawOverOtherApps()) {


            mFloatingView = LayoutInflater.from(this).inflate(R.layout.popup_window, null);

            //Add the view to the window.
            final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY : WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);

            //Specify the view position
            params.gravity = Gravity.TOP | Gravity.LEFT;        //Initially view will be added to top-left corner
            params.x = 0;
            params.y = 100;

            //Add the view to the window
            mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
            try {
                mWindowManager.addView(mFloatingView, params);
            } catch (Exception e) {
                e.printStackTrace();
            }

            frameLayout = mFloatingView.findViewById(R.id.cont_window);


            ivProfile = (ImageView) mFloatingView.findViewById(R.id.iv_profile);

            Button ivRejectCall = (Button) mFloatingView.findViewById(R.id.iv_reject_call);

            TextView tvResumeCall = mFloatingView.findViewById(R.id.tv_resume_call);
            ivRejectCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    endCall();
                }
            });


            frameLayout.setOnTouchListener(new View.OnTouchListener() {
                private int initialX;
                private int initialY;
                private float initialTouchX;
                private float initialTouchY;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:

                            //remember the initial position.
                            initialX = params.x;
                            initialY = params.y;

                            //get the touch location
                            initialTouchX = event.getRawX();
                            initialTouchY = event.getRawY();
                            return true;
                        case MotionEvent.ACTION_UP:
                            int Xdiff = (int) (event.getRawX() - initialTouchX);
                            int Ydiff = (int) (event.getRawY() - initialTouchY);

                            //The check for Xdiff <10 && YDiff< 10 because sometime elements moves a little while clicking.
                            //So that is click event.
                            if (Xdiff < 10 && Ydiff < 10) {
                                if (isViewCollapsed()) {
                                    //When user clicks on the image view of the collapsed layout,
                                    //visibility of the collapsed layout will be changed to "View.GONE"
                                    //and expanded view will become visible.

                                    //GenericUtils.openApp(CallService.this, "com.imedhealthus.imeddoctors");
                                /*    Intent intent = new Intent(CallService.this, SimpleFragmentActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CallFragment.ordinal());
                                    startActivity(intent);
                                */
                                }
                            }
                            return true;
                        case MotionEvent.ACTION_MOVE:
                            //Calculate the X and Y coordinates of the view.
                            params.x = initialX + (int) (event.getRawX() - initialTouchX);
                            params.y = initialY + (int) (event.getRawY() - initialTouchY);

                            //Update the layout with new X & Y coordinate
                            mWindowManager.updateViewLayout(mFloatingView, params);
                            return true;
                    }
                    return false;
                }
            });
            tvResumeCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //GenericUtils.openApp(CallService.this, "com.imedhealthus.imeddoctors");
                    Intent intent = new Intent(CallService.this, CallActivity.class);
                    intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CallFragment.ordinal());
                    startActivity(intent);

                }
            });
            if (publisher != null) {
                if (!publisher.getPublishVideo())
                    ivProfile.setVisibility(View.GONE);
                else
                    ivProfile.setVisibility(View.VISIBLE);
            }

            mFloatingView.setVisibility(View.GONE);
        }
    }

    /**
     * Detect if the floating view is collapsed or expanded.
     *
     * @return true if the floating view is collapsed.
     */
    private boolean isViewCollapsed() {
        return mFloatingView == null || mFloatingView.findViewById(R.id.cont_window).getVisibility() == View.VISIBLE;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            notificationTitle = "My Live Doctors";

            if (SharedData.getInstance().getSelectedCallSignal().getCallType() != null
                    && SharedData.getInstance().getSelectedCallSignal().getCallType().equals("audio")) {
                notificationText = "Audio call in progress";
            } else
                notificationText = "Video call in progress";

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (intent.getAction().equals(ACTION_START_FOREGROUND_SERVICE))
            startForegroundService();
        else if (intent.getAction().equals(ACTION_STOP_FOREGROUND_SERVICE))
            endCall();


        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (mFloatingView != null) mWindowManager.removeView(mFloatingView);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        subscriber = null;
        SharedData.getInstance().setFinishCall(false);

        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);


    }

    private void startForegroundService() {

        NotificationChannel notificationChannel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(CHANNEL_ONE_ID,
                    CHANNEL_ONE_NAME, IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            manager.createNotificationChannel(notificationChannel);
        }

        // Create notification default intent.
        Intent intent = new Intent(CallService.this, CallActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CallFragment.ordinal());
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setChannelId(CHANNEL_ONE_ID);
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(notificationTitle);
        bigTextStyle.bigText(notificationText);
        builder.setStyle(bigTextStyle);
        builder.setWhen(System.currentTimeMillis());
        builder.setSmallIcon(R.drawable.mylivedoctor_landing_icon);
        Bitmap largeIconBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.mylivedoctor_landing_icon);
        builder.setLargeIcon(largeIconBitmap);
        builder.setContentIntent(pendingIntent);


        Notification notification = builder.build();

        isServiceRunning = true;

        startCall();
        startForeground(1, notification);
    }


    private void stopForegroundService() {
        screenSharingCapturer = null;
        stopSelf();
        isServiceRunning = false;
    }

    public void startCall() {
        startTime = SystemClock.uptimeMillis();
        customHandler.postDelayed(checkTimerThread, 1000);


        CallSignal callSignal = SharedData.getInstance().getSelectedCallSignal();
        if (callSignal != null) {


            String userId = CacheManager.getInstance().getCurrentUser().getLoginId();
            String recieverId = callSignal.getLoginId().equals(userId) ? callSignal.getRecieverLoginId() : callSignal.getLoginId();
            startCall(userId, recieverId, callSignal.getMsg(), callSignal.getCallType());

            if (SharedData.getInstance().getSelectedCallSignal().getCallType() != null
                    && SharedData.getInstance().getSelectedCallSignal().getCallType().equals("audio")) {
                //sendBroadcast(Constants.SET_UP_VIEW_FOR_AUDIO_CALL);
                IS_AUDIO_CALL = true;
                IS_VIDEO_CALL = false;
            } else {
                //sendBroadcast(Constants.SET_UP_VIEW_FOR_VIDEO_CALL);
                IS_AUDIO_CALL = false;
                IS_VIDEO_CALL = true;
            }
            return;
        }

    }

    private void startCall(final String userLoginId, final String partnerLoginId,
                           final String recieverName, final String callType) {


        String myName = CacheManager.getInstance().getCurrentUser().getFullName();

        WebServicesHandler.instance.getDataForCallWithNotification(userLoginId, partnerLoginId, myName, callType, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                String sessionId = response.optString("SessionId", "");
                String token = response.optString("TokenId", "");
                boolean isUserLive = response.optBoolean("IsUserLive", false);
                String chatId = response.optString("ChatId", "");
                String doctorId = response.optString("DoctorId", "");
                String patientId = response.optString("PatientId", "");


                String senderName = CacheManager.getInstance().getCurrentUser().getFullName();

                if (!isUserLive) {
                    //  AlertUtils.showAlertForBack(context, Constants.ErrorAlertTitle, recieverName + " is not online right now. We have notified him. Kindly try again in a few minutes.");

                    // CallData data = new CallData(bookedAppointment.getPatientLoginId(), senderName, "", "","");
                    // OpenTokHandler.getInstance().sendPushNotification(data);

                    //  return;
                }

                SharedData.getInstance().setOpenTokCallSessionId(sessionId);
                SharedData.getInstance().setOpenTokCallToken(token);

                SharedData.getInstance().setSelectedChatDocttorId(doctorId);

                CallSignal callSignal = new CallSignal(partnerLoginId, patientId, senderName, callType, "", chatId, CacheManager.getInstance().getCurrentUser().getLoginId(), CacheManager.getInstance().getCurrentUser().getUserId());

                SharedData.getInstance().setSelectedCallSignal(callSignal);
                // OpenTokHandler.getInstance().sendCallAcceptSignal(SharedData.getInstance().getSelectedCallSignal());
                initSession();
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

                sendBroadcast(Constants.START_CALL_FAILURE);
            }

        });

    }

    private boolean isConnected = false;
    private long callAcceptenceTime = 35000;
    private Runnable checkTimerThread = new Runnable() {
        public long timeInMilliseconds;
        public boolean shutdown = false;

        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

            if (isConnected) {
                customHandler.removeCallbacks(checkTimerThread);
                return;
            }

            if (!(timeInMilliseconds < callAcceptenceTime) && !isConnected) {


                stopRinging();
                if (session != null)
                    session.disconnect();
                SharedData.getInstance().setCallActive(false);
                SharedData.getInstance().setCallResumed(false);
                sendBroadcast(Constants.CALL_DROPPED);
                sendNoResponseSignal(SharedData.getInstance().getSelectedCallSignal());

                customHandler.removeCallbacks(checkTimerThread);


                stopForegroundService();

                return;
            }

            customHandler.postDelayed(this, 1000);
        }

        public void shutdown() {
            shutdown = true;
        }

    };

    //Session
    private void initSession() {

        //  AlertUtils.showProgress((Activity)CallService.this, "Connecting...");

        Uri notification = Uri.parse("android.resource://" + "com.imedhealthus.imeddoctors" + "/raw/dialtune.mp3");
//        Uri notification = RingtoneManager.getActualDefaultRingtoneUri(getActivity(), RingtoneManager.TYPE_RINGTONE);
        RingtoneManager.setActualDefaultRingtoneUri(
                CallService.this, RingtoneManager.TYPE_RINGTONE,
                notification);
        ringtone = RingtoneManager.getRingtone(CallService.this, notification);
//        ringtone.play();

        if (Constants.mediaPlayer == null || !Constants.mediaPlayer.isPlaying()) {
            if (!SharedData.getInstance().isCallResumed()) {
                Constants.mediaPlayer = MediaPlayer.create(CallService.this, R.raw.dialtune);
                Constants.mediaPlayer.start();
            }
        }


        if (session == null) {
            OpenTokHandler.getInstance().intializeListerner(new CallStatusListener() {
                @Override
                public void callRejectListener() {
                    sendBroadcast(Constants.CALL_REJECTED);
                    stopRinging();
                    endCall();
                }

                @Override
                public void callCancelListener() {

                }

                @Override
                public void callAcceptListener() {

                }
            });
            session = new Session.Builder(CallService.this, Constants.API_KEY, SharedData.getInstance().getOpenTokCallSessionId()).build();
            session.setSessionListener(new Session.SessionListener() {
                @Override
                public void onConnected(Session session) {
                    onSessionConnected(session);
                }

                @Override
                public void onDisconnected(Session session) {

                }

                @Override
                public void onStreamReceived(Session session, Stream stream) {
                    onStreamReceivedFromSession(session, stream);
                }


                @Override
                public void onStreamDropped(Session session, Stream stream) {
                    IS_CALL_CONNECTED = false;
                    sendBroadcast(Constants.STREAM_DROPPED);
                }

                @Override
                public void onError(Session session, OpentokError opentokError) {
                    Log.e(CALL_TAG, "call session error " + opentokError.getMessage());
                    onSessionError();
                }
            });
            session.connect(SharedData.getInstance().getOpenTokCallToken());

            session.setSignalListener(new Session.SignalListener() {
                @Override
                public void onSignalReceived(Session session, String s, String s1, Connection connection) {


                    if (s.equals(OpenTokHandler.CALL_END) || s.equals(SIGNAL_TYPE_CALL_CANCEL)) {
                        sendBroadcast(Constants.CALL_ENDED);
                        if (session != null)
                            session.disconnect();

                        endCall();
                    } else if (s.equals(OpenTokHandler.NO_RESPONSE)) {
                        new NotificationUtils(CallService.this).showNotificationMessageForMessage("Missed call", SharedData.getInstance().getReceiverName(), new Date().toString(), null);

                    }


                }

            });
        }

    }

    private void onSessionError() {
        stopRinging();
        sendBroadcast(Constants.SESSION_ERROR);
    }

    private void onStreamReceivedFromSession(Session session, Stream stream) {
        {

            stopRinging();

            IS_CALL_CONNECTED = true;
            timer.startTimer();


            if (subscriber != null) {
                return;
            }
            if (stream == null) {
                return;
            }


            subscriber = new Subscriber.Builder(CallService.this, stream).build();

            subscriber.getRenderer().setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL);
            subscriber.setSubscriberListener(new SubscriberKit.SubscriberListener() {
                @Override
                public void onConnected(SubscriberKit subscriberKit) {

                }

                @Override
                public void onDisconnected(SubscriberKit subscriberKit) {

                }

                @Override
                public void onError(SubscriberKit subscriberKit, OpentokError opentokError) {

                }
            });
            session.subscribe(subscriber);
            CallService.setSubscriber(subscriber);
            sendBroadcast(Constants.STREAM_RECEIVED);
            isConnected = true;
        }
    }

    private void onSessionConnected(Session session) {

        Log.e(CALL_TAG, "call session connected");
        //stopRinging();
        if (SharedData.getInstance().getSelectedCallSignal() != null) {
            if (SharedData.getInstance().getSelectedCallSignal().getCallType() != null
                    && SharedData.getInstance().getSelectedCallSignal().getCallType().equals("audio")) {
                publisher = new Publisher.Builder(this).videoTrack(false).build();
                sendBroadcast(Constants.SET_UP_VIEW_FOR_AUDIO_CALL);
            } else {
                publisher = new Publisher.Builder(this).build();
                sendBroadcast(Constants.SET_UP_VIEW_FOR_VIDEO_CALL);
            }

        } else {
            publisher = new Publisher.Builder(this).videoTrack(false).build();
            sendBroadcast(Constants.SET_UP_VIEW_FOR_AUDIO_CALL);
        }
        publisher.setPublisherListener(new PublisherKit.PublisherListener() {
            @Override
            public void onStreamCreated(PublisherKit publisherKit, Stream stream) {

            }

            @Override
            public void onStreamDestroyed(PublisherKit publisherKit, Stream stream) {

            }

            @Override
            public void onError(PublisherKit publisherKit, OpentokError opentokError) {

            }
        });


        //CallService.setPublisher(publisher);
        publisher.setPublisherVideoType(PublisherKit.PublisherKitVideoType.PublisherKitVideoTypeScreen);
        publisher.setAudioFallbackEnabled(false);

        //publisher.getRenderer().setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL);
        session.publish(publisher);

        SharedData.getInstance().setCallActive(true);

    }

    public void stopRinging() {
        if (Constants.mediaPlayer == null)
            return;

        if (Constants.mediaPlayer.isPlaying()) {
            Constants.mediaPlayer.stop();
            return;
        }
    }

    private void sendBroadcast(String intentExtra) {
        Intent intent = new Intent(Constants.BROADCAST_EVENT);
        intent.putExtra(Constants.TAG, intentExtra);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    @Override
    public void onTimeUpdated(long updatedSeconds) {
        Intent intent = new Intent(Constants.BROADCAST_EVENT_TIME);
        intent.putExtra(Constants.TIME, updatedSeconds);
        LocalBroadcastManager.getInstance(CallService.this).sendBroadcast(intent);

    }


    private void endCall() {


        stopRinging();
        timer.stopTimer();
        SharedData.getInstance().setCallActive(false);
        SharedData.getInstance().setCallResumed(false);
        Constants.callStartTime = 0;

        if (!isConnected) {
            sendCallCancelSignal(SharedData.getInstance().getSelectedCallSignal());
            if (session != null) {
                session.disconnect();
            }


        } else if (isConnected) {

            if (session != null) {
                sendCallEndSignal(SharedData.getInstance().getSelectedCallSignal());
                session.disconnect();
            }
        }


        //OpenTokHandler.getInstance().sendCallEndSignal(SharedData.getInstance().getSelectedCallSignal());
        stopForegroundService();
    }

    public void sendCallEndSignal(CallSignal data) {

        if (session == null) {
            return;
        }

        //ToDO remove these line after apis are fixed
        // data.setCallSessionId(Constants.OpenTokConfig.sessionId);
        // data.setCallToken(Constants.OpenTokConfig.sessionId);

        Gson gson = new Gson();
        String json = gson.toJson(data);


        session.sendSignal(CALL_END, json);

        // sendPushNotification(data);

    }

    public void sendCallCancelSignal(CallSignal data) {

        if (session == null) {
            return;
        }


        Gson gson = new Gson();
        String json = gson.toJson(data);


        session.sendSignal(SIGNAL_TYPE_CALL_CANCEL, json);


    }


    public void sendNoResponseSignal(CallSignal data) {


        //ToDO remove these line after apis are fixed
        // data.setCallSessionId(Constants.OpenTokConfig.sessionId);
        // data.setCallToken(Constants.OpenTokConfig.sessionId);


        OpenTokHandler.getInstance().sendCallMissedSignal(data);

        // sendPushNotification(data);

    }

}
