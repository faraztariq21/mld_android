package com.imedhealthus.imeddoctors.common.models;

/**
 * Created by umair on 2/14/18.
 */

public class CallData {

    private String receiverId, msg, callSessionId, callToken,doctorId,senderId;

    public CallData(String receiverId, String msg, String callSessionId, String callToken, String docId) {
        this.receiverId = receiverId;
        this.msg = msg;
        this.callSessionId = callSessionId;
        this.callToken = callToken;
        this.doctorId = docId;
        senderId = CacheManager.getInstance().getCurrentUser().getLoginId();
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setCallSessionId(String callSessionId) {
        this.callSessionId = callSessionId;
    }

    public void setCallToken(String callToken) {
        this.callToken = callToken;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public String getMsg() {
        return msg;
    }

    public String getCallSessionId() {
        return callSessionId;
    }

    public String getCallToken() {
        return callToken;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }
}
