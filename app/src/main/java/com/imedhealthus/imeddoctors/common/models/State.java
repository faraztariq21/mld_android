package com.imedhealthus.imeddoctors.common.models;



import com.imedhealthus.imeddoctors.doctor.models.StaticData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 2/14/2018.
 */

public class State  implements Serializable,SuggestionListItem{
    String stateId = "0";
    String countryId = "0";
    String stateName ="";

    public State(String dummy) {
        this.stateId = "0";
        this.countryId = "0";
        this.stateName = "Select State";
    }




    public static List<State> getStatesAgainstCountryId(List<State> stateList,String countryId){
        List<State> list = new ArrayList<>();
        list.add(stateList.get(0));
        for(int i=1;i<stateList.size();i++){
            if(stateList.get(i).getCountryId().equalsIgnoreCase(countryId))
                list.add(stateList.get(i));
        }
        return list;
    }
    public State() {
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getStateName() {

        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    @Override
    public String toString() {
        return stateName;
    }

    @Override
    public String getSuggestionText() {
        return stateName;
    }

    @Override
    public SuggestionListItem getSuggestionListItem() {
        return State.this;
    }


    public static ArrayList<SuggestionListItem> getSuggestions(ArrayList<State> states){
        ArrayList<SuggestionListItem> suggestionListItems  = new ArrayList<>();
        for(State state:states){
            suggestionListItems.add(state);
        }
        return suggestionListItems;
    }

}
