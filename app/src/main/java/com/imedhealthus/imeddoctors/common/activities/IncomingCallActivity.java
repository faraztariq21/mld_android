package com.imedhealthus.imeddoctors.common.activities;

import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.auth.activities.SplashActivity;
import com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler;
import com.imedhealthus.imeddoctors.common.listeners.CallStatusListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.CallSignal;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.models.StaticData;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class IncomingCallActivity extends BaseActivity implements View.OnClickListener,CallStatusListener {


    @BindView(R.id.btn_accept)
    Button btnAccept;
    @BindView(R.id.btn_accept_video)
    Button btnAcceptVideo;


    @BindView(R.id.btn_reject)
    Button btnReject;

    @BindView(R.id.iv_profile)
    CircleImageView ivProfile;

    @BindView(R.id.dialog_msg)
    TextView tvMsg;
    @BindView(R.id.dialog_title)
    TextView tvTitle;

    private Ringtone ring;
    private CallSignal callSignal;
    private PowerManager.WakeLock fullWakeLock;
    private PowerManager.WakeLock partialWakeLock;
    private long timeInMilliseconds = 0;
    private long startTime = 0;
    private Handler customHandler = new Handler();
    final private long callAcceptanceTime = 30000; // 30 seconds


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incoming_call);

        ButterKnife.bind(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);


        callSignal = SharedData.getInstance().getSelectedCallSignal();

        getStaticData();

        startTime = SystemClock.uptimeMillis();
        customHandler.postDelayed(checkTimerThread,1000);

        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        ring = RingtoneManager.getRingtone(this, notification);
        ring.play();

        if(callSignal == null) {
            HashMap<String, String> data = (HashMap<String, String>) getIntent().getExtras().get(Constants.Keys.PushNotificationData);
            if(data!=null){
                CallSignal callSignal = new CallSignal();
                callSignal.setChatId(data.get("chatId"));
                callSignal.setMsg(data.get("senderName"));
                callSignal.setRecieverLoginId(data.get("receiverLoginId"));
                callSignal.setReceiverProfileId(data.get("patientProfileId"));
                callSignal.setLoginId(data.get("loginId"));
                callSignal.setProfileId(data.get("doctorProfileId"));
                callSignal.setImage(data.get("image"));
                callSignal.setCallType(data.get("callType"));
                SharedData.getInstance().setReceiverId(data.get("loginId"));
                SharedData.getInstance().setReceiverName(data.get("senderName"));
                SharedData.getInstance().setReceiverImageUrl(data.get("image"));
                SharedData.getInstance().setSelectedCallSignal(callSignal);

                this.callSignal = callSignal;
            }
        }

        initView();


    }

    private void initView() {
        if(callSignal==null)
            return;

        tvTitle.setText("Incoming Call");
        tvMsg.setText(callSignal.getMsg());
        Glide.with(ApplicationManager.getContext()).load(Constants.URLS.BaseApis+callSignal.getImage()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivProfile);
        OpenTokHandler.getInstance().intializeListerner(this);
        btnAccept.setOnClickListener(this);
        btnAcceptVideo.setOnClickListener(this);
        btnReject.setOnClickListener(this);
        if(callSignal.getCallType()!=null&&callSignal.getCallType().equals("video")){

            setViewForAudioCall();

        } else {
            btnAcceptVideo.setVisibility(View.GONE);
            btnAccept.setVisibility(View.VISIBLE);
        }

    }

    private void setViewForAudioCall() {
        btnAcceptVideo.setVisibility(View.VISIBLE);
        btnAccept.setVisibility(View.GONE);


    }

    @Override
    public void onClick(View view) {

        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(Constants.NOTIFICATIONSID.callNotificationId);

        if(view.getId()==btnAccept.getId()){
            if(ring.isPlaying())
                ring.stop();

            callAccept();


        }else if(view.getId()==btnReject.getId()){
            if(ring.isPlaying())
                ring.stop();

            callReject();

        }else if(view.getId()==btnAcceptVideo.getId()){
            if(ring.isPlaying())
                ring.stop();

            callAccept();

        }


    }

    private Runnable checkTimerThread = new Runnable() {
        public boolean shutdown = false;
        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

            if(!(timeInMilliseconds < callAcceptanceTime)) {

                if(ring.isPlaying())
                    ring.stop();

                customHandler.removeCallbacks(checkTimerThread);
                finish();
                return;
            }

            customHandler.postDelayed(this, 1000);
        }
        public void shutdown() {
            shutdown = true;
        }

    };

    public void getStaticData(){

        String userLoginId = "0";
        if (CacheManager.getInstance().isUserLoggedIn()) {
            userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        }

        WebServicesHandler.instance.getStaticData(userLoginId, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                if(response.status()){


                    String session = response.optString("SessionId");
                    String token = response.optString("Token");
                    CacheManager.getInstance().setChatSessionId(session);
                    CacheManager.getInstance().setChatToken(token);
                    OpenTokHandler.getInstance().initializeSession();



                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

              //  showDeleteAlert(SplashActivity.this,"Problem!",Constants.GenericErrorMsg+" Would you like to retry?");
            }
        });

    }


    private void callAccept() {
        Intent intent = new Intent(this, CallActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CallFragment.ordinal());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent, true);
        OpenTokHandler.getInstance().sendCallAcceptSignal(callSignal);
//        finish();
    }


    public void callCancelled(){
        if(ring.isPlaying())
            ring.stop();

    }

    private void callReject() {

        OpenTokHandler.getInstance().sendCallRejectedSignal(callSignal);

        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void callRejectListener() {

    }

    @Override
    public void callCancelListener() {
        if(ring.isPlaying())
            ring.stop();

        finish();
    }

    @Override
    public void callAcceptListener() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(ring.isPlaying())
            ring.stop();
    }
}
