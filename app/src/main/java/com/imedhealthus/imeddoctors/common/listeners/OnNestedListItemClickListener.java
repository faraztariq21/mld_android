package com.imedhealthus.imeddoctors.common.listeners;

public interface OnNestedListItemClickListener {

    public void onNestedListItemClicked(int parentIndex, int childIndex);
}
