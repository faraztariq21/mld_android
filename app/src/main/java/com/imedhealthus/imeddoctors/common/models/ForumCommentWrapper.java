package com.imedhealthus.imeddoctors.common.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ForumCommentWrapper implements Serializable {

    @SerializedName("ForumComment")
    ForumComment forumComment;

    @SerializedName("ForumQuestion")
    ForumQuestion forumQuestion;

    public ForumCommentWrapper(ForumComment forumComment, ForumQuestion forumQuestion) {
        this.forumComment = forumComment;
        this.forumQuestion = forumQuestion;
    }

    public ForumComment getForumComment() {
        return forumComment;
    }

    public void setForumComment(ForumComment forumComment) {
        this.forumComment = forumComment;
    }

    public ForumQuestion getForumQuestion() {
        return forumQuestion;
    }

    public void setForumQuestion(ForumQuestion forumQuestion) {
        this.forumQuestion = forumQuestion;
    }
}
