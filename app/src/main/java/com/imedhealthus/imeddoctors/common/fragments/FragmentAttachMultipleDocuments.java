package com.imedhealthus.imeddoctors.common.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.adapters.AdapterListItemDocument;
import com.imedhealthus.imeddoctors.common.listeners.FragmentAttachImageListener;
import com.imedhealthus.imeddoctors.common.models.GeneralListItemDocument;
import com.imedhealthus.imeddoctors.common.models.ImageSelector;
import com.imedhealthus.imeddoctors.common.models.ListItemDocument;
import com.imedhealthus.imeddoctors.common.models.ListItemImage;
import com.imedhealthus.imeddoctors.common.utils.FileUtils;
import com.soundcloud.android.crop.Crop;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

import static android.app.Activity.RESULT_OK;
import static com.imedhealthus.imeddoctors.common.fragments.ChatFragment.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE_DOCUMENT;


public class FragmentAttachMultipleDocuments extends android.support.v4.app.Fragment implements ImageSelector.OnImageSelectionListener, FragmentAttachImageListener {

    @BindView(R.id.iv_add_image)
    ImageView ivAddImage;
    @BindView(R.id.rv_images)
    RecyclerView rvImages;
    @BindView(R.id.tv_heading)
    TextView tvHeading;
    @BindView(R.id.tv_no_images)
    TextView tvNoDocuments;

    boolean showAddDocumentButton;
    ArrayList<GeneralListItemDocument> generalListItemDocuments;

    AdapterListItemDocument adapterListItemDocument;


    public static final String SHOW_ADD_DOCUMENT_BUTTON = "show_add_document_button";
    public static final String GENERAL_LIST_ITEM_DOCUMENTS = "general_list_item_documents";

    private ImageSelector imageSelector;
    private Uri selectedImageUri;
    private ArrayList<String> filePaths;

    public static FragmentAttachMultipleDocuments newInstance(boolean showAddDocumentButton, ArrayList<? extends ListItemImage> generalListItemImages) {
        FragmentAttachMultipleDocuments fragmentAttachMultipleDocuments = new FragmentAttachMultipleDocuments();
        Bundle bundle = new Bundle();
        bundle.putBoolean(SHOW_ADD_DOCUMENT_BUTTON, showAddDocumentButton);
        bundle.putSerializable(GENERAL_LIST_ITEM_DOCUMENTS, generalListItemImages);
        fragmentAttachMultipleDocuments.setArguments(bundle);
        return fragmentAttachMultipleDocuments;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            showAddDocumentButton = getArguments().getBoolean(SHOW_ADD_DOCUMENT_BUTTON);
            generalListItemDocuments = (ArrayList<GeneralListItemDocument>) getArguments().getSerializable(GENERAL_LIST_ITEM_DOCUMENTS);
            filePaths = new ArrayList<>();
            for (ListItemDocument listItemDocument : generalListItemDocuments) {
                filePaths.add(listItemDocument.getDocumentPath());
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attach_multiple_images, container, false);
        ButterKnife.bind(this, view);


        tvHeading.setText(getResources().getString(R.string.attach_documents));
        if (showAddDocumentButton)
            ivAddImage.setVisibility(View.VISIBLE);
        else {
            ivAddImage.setVisibility(View.GONE);
            if (generalListItemDocuments == null || generalListItemDocuments.size() == 0) {
                tvNoDocuments.setVisibility(View.VISIBLE);
            }
            tvNoDocuments.setText("No Documents Attached");

            tvHeading.setVisibility(View.GONE);
        }
        ivAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(getActivity(),
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE_DOCUMENT);
                } else {
                    openDocumentSelector();
                }

            }
        });

        adapterListItemDocument = new AdapterListItemDocument(getActivity(), this, generalListItemDocuments, showAddDocumentButton);
        rvImages.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvImages.setItemAnimator(new DefaultItemAnimator());
        rvImages.setAdapter(adapterListItemDocument);
        return view;
    }

    @Override
    public void onImageSelected(Bitmap bitmap, Uri croppedImageUri, Uri fullImageUri) {
        CropImage.activity(fullImageUri).start(getActivity());
        selectedImageUri = fullImageUri;
    }

    @Override
    public void onImageCancelTapped(int position) {
        filePaths.remove(position);
        adapterListItemDocument.removeDocument(position);

    }

    @Override
    public void onImageTapped(int position) {
    /*    SharedData.getInstance().setSelectedImageURL(generalListItemDocuments.get(position).getImagePath());
        SimpleFragmentActivity.startActivity(getActivity(), Constants.Fragment.FragmentImageFull);
    */
        if (generalListItemDocuments.get(position).isDocumentUri()) {
            FileUtils.openDocument(getActivity(), FileUtils.getPathFromUri(getActivity(), Uri.parse(generalListItemDocuments.get(position).getDocumentPath())));
        }

    }


    private void openFileSelector() {
        if (imageSelector == null) {
            imageSelector = new ImageSelector(this, getActivity(), this);
        }
        imageSelector.showSourceAlert();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE_DOCUMENT: {

                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    openDocumentSelector();
                } else {
                    Toast.makeText(getActivity(), "Permission denied. Unable to proceed", Toast.LENGTH_SHORT).show();

                }
                return;
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    filePaths = new ArrayList<>();
                    filePaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                    generalListItemDocuments = GeneralListItemDocument.getGeneralListItemDocumentsFromStrings(filePaths, true, false);
                    adapterListItemDocument.updateDocuments(generalListItemDocuments);
                }
                break;
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                selectedImageUri = resultUri;

                generalListItemDocuments.add(new GeneralListItemDocument(resultUri.toString(), true, false));
                adapterListItemDocument.updateDocuments(generalListItemDocuments);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        } else {


            switch (requestCode)

            {
                case ImageSelector.CAMERA_REQUEST_CODE:
                case ImageSelector.GALLERY_REQUEST_CODE:
                case Crop.REQUEST_CROP:
                    if (imageSelector != null) {
                        imageSelector.onActivityResult(requestCode, resultCode, data);
                    }
                    break;


            }
        }

    }

    private void openDocumentSelector() {
        FilePickerBuilder.getInstance().setMaxCount(10)
                .setSelectedFiles(filePaths)
                .setActivityTheme(R.style.LibAppTheme)
                .pickFile(getActivity());

    }

    public ArrayList<String> getFilePaths() {
        return filePaths != null ? filePaths : new ArrayList<String>();
    }
}
