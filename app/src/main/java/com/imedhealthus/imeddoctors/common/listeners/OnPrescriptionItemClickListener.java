package com.imedhealthus.imeddoctors.common.listeners;

/**
 * Created by Malik Hamid on 1/11/2018.
 */

public interface OnPrescriptionItemClickListener {
    void OnPrescriptionDetailClickListener(int idx);
    void OnPrescriptionEditClickListener(int idx);
    void OnPrescriptionDeleteClickListener(int idx);
}
