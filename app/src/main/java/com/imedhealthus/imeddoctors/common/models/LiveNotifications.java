package com.imedhealthus.imeddoctors.common.models;

/**
 * Created by Dev-iMEDHealth-X5 on 26/04/2018.
 */

public class LiveNotifications {
    String ProfileId;
    String ProfileType;
    String message;

    public LiveNotifications(String profileId, String profileType, String message) {
        ProfileId = profileId;
        ProfileType = profileType;
        this.message = message;
    }

    public LiveNotifications() {
    }

    public String getProfileId() {
        return ProfileId;
    }

    public void setProfileId(String profileId) {
        ProfileId = profileId;
    }

    public String getProfileType() {
        return ProfileType;
    }

    public void setProfileType(String profileType) {
        ProfileType = profileType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
