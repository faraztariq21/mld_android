package com.imedhealthus.imeddoctors.common.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PaymentFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {


    @BindView(R.id.tv_add_payment_method)
    TextView tvAddPaymentMethod;
    @BindView(R.id.et_card_number)
    TextView tvCardNumber;
    @BindView(R.id.et_payment_method_details)
    TextView tvPaymentDetails;
    @BindView(R.id.cont_card)
    LinearLayout contCard;
    @BindView(R.id.cont_others)
    LinearLayout contPaymentDetail;



    @BindView(R.id.sp_payment_method)
    Spinner spPaymentMethod;


    public PaymentFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_payment, container, false);
        ButterKnife.bind(this,view);
        init();
        return view;
    }

    public void init(){
        spPaymentMethod.setOnItemSelectedListener(this);
        tvAddPaymentMethod.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.tv_add_payment_method:
                addNewPaymentMethod();
                break;
        }
    }

    public void addNewPaymentMethod(){

        Intent intent=new Intent(getActivity(), SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditPaymentMethod.ordinal());
        ((BaseActivity)getActivity()).startActivity(intent, true);

    }

    private void setCardData() {
        contCard.setVisibility(View.VISIBLE);
        contPaymentDetail.setVisibility(View.INVISIBLE);
        tvCardNumber.setText("***********7676");
    }

    private void setPaymentDetail() {
        contCard.setVisibility(View.INVISIBLE);
        contPaymentDetail.setVisibility(View.VISIBLE);
        tvPaymentDetails.setText("Payoneer account");
    }


    @Override
    public boolean showHeader() {
        return true;
    }

    @Override
    public String getName() {
        return "paymentFragment";
    }

    @Override
    public String getTitle() {
        return "Payment";
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if(adapterView.getItemAtPosition(i).toString().equalsIgnoreCase("Card")){
            setCardData();
        }else{
            setPaymentDetail();
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
