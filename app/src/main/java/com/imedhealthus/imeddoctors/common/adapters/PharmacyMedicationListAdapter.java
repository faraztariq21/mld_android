package com.imedhealthus.imeddoctors.common.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.listeners.OnPharmacyMedicationClickListener;
import com.imedhealthus.imeddoctors.common.view_holders.ChoosePrescriptionMedicationItemView;
import com.imedhealthus.imeddoctors.patient.models.PrescribedMedication;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 2/13/2018.
 */

public class PharmacyMedicationListAdapter extends RecyclerView.Adapter<ChoosePrescriptionMedicationItemView> implements OnPharmacyMedicationClickListener {

    private List<PrescribedMedication> medications;
    private WeakReference<TextView> tvNoRecords;
    private List<PrescribedMedication> selectedMedications;

    public PharmacyMedicationListAdapter(TextView tvNoRecords) {

        super();
        this.tvNoRecords = new WeakReference<>(tvNoRecords);
        this.selectedMedications = new ArrayList<>();

    }

    public void setMedications(List<PrescribedMedication> medications) {

        this.medications = medications;
        this.selectedMedications.clear();
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }

    public List<PrescribedMedication> getSelectedMedications() {
        return selectedMedications;
    }

    @Override
    public ChoosePrescriptionMedicationItemView onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_choose_prescribed_medication, parent, false);

        return new ChoosePrescriptionMedicationItemView(view, this);

    }

    @Override
    public void onBindViewHolder(ChoosePrescriptionMedicationItemView viewHolder, int position) {

        PrescribedMedication medication = getItem(position);
        boolean isSelected = selectedMedications.contains(medication);
        viewHolder.setData(medication, isSelected, position);

    }

    @Override
    public int getItemCount() {
        return medications == null ? 0 : medications.size();
    }

    public PrescribedMedication getItem(int position) {
        if (position < 0 || position >= medications.size()) {
            return null;
        }
        return medications.get(position);
    }

    @Override
    public void onPharmacyMedicationToggleSelection(int idx) {

        PrescribedMedication medication = getItem(idx);
        if (medication == null) {
            return;
        }

        if (selectedMedications.contains(medication)) {
            selectedMedications.remove(medication);
        }else {
            selectedMedications.add(medication);
        }

    }

}
