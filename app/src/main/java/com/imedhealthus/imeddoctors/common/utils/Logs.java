package com.imedhealthus.imeddoctors.common.utils;

import android.util.Log;

/**
 * Created by Dev-iMEDHealth-X5 on 18/04/2018.
 */
public class Logs {
    public static String API_RESPONSE_LOG = "apiResponse";
    public static String API_CALL_LOG = "apiCall";
    public static String APP_LOG = "app";

    public static void apiResponse(String response) {
        Log.e(API_RESPONSE_LOG, response);
    }

    public static void apiCall(String call) {
        Log.e(API_CALL_LOG, call);
    }

    public static void appLog(String message) {
        Log.e(APP_LOG, message);
    }
}
