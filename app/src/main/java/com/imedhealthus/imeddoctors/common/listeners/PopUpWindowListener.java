package com.imedhealthus.imeddoctors.common.listeners;

import android.view.View;

import com.opentok.android.Subscriber;

public interface PopUpWindowListener {

    public void displayPopUpWindow(View view,Subscriber subscriber);

    public void dismissPopUpWindow(View view);
}
