package com.imedhealthus.imeddoctors.common.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.listeners.OnBookedAppointmentItemClickListener;
import com.imedhealthus.imeddoctors.common.models.BookedAppointment;
import com.imedhealthus.imeddoctors.common.view_holders.BookedAppointmentItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Malik Hamid on 12/29/2017.
 */

public class BookedAppointmentsListAdapter extends RecyclerView.Adapter<BookedAppointmentItemViewHolder> {

    private List<BookedAppointment> appointments;
    private WeakReference<TextView> tvNoRecords;
    private WeakReference<OnBookedAppointmentItemClickListener> onBookedAppointmentItemClickListener;
    private int expandedIdx;

    public BookedAppointmentsListAdapter(TextView tvNoRecords, OnBookedAppointmentItemClickListener onBookedAppointmentItemClickListener) {

        super();
        this.tvNoRecords = new WeakReference<>(tvNoRecords);
        this.onBookedAppointmentItemClickListener = new WeakReference<>(onBookedAppointmentItemClickListener);
        expandedIdx = -1;

    }

    public void setAppointments(List<BookedAppointment> appointments) {

        this.appointments = appointments;
        expandedIdx = -1;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }


    }

    public void toggleExpandAt(int idx) {


        if (idx == expandedIdx) {
            expandedIdx = -1;
        } else {
            expandedIdx = idx;
        }
        notifyDataSetChanged();

    }

    @Override
    public BookedAppointmentItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_booked_appointment, parent, false);

        OnBookedAppointmentItemClickListener listener = null;
        if (onBookedAppointmentItemClickListener != null && onBookedAppointmentItemClickListener.get() != null) {
            listener = onBookedAppointmentItemClickListener.get();
        }

        return new BookedAppointmentItemViewHolder(view, listener);

    }

    @Override
    public void onBindViewHolder(BookedAppointmentItemViewHolder viewHolder, int position) {
        viewHolder.setData(getItem(position), position == expandedIdx, position);
    }

    @Override
    public int getItemCount() {
        return appointments == null ? 0 : appointments.size();
    }

    public BookedAppointment getItem(int position) {
        if (position < 0 || position >= appointments.size()) {
            return null;
        }
        return appointments.get(position);
    }

}