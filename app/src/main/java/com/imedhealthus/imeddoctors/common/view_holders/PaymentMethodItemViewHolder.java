package com.imedhealthus.imeddoctors.common.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.listeners.OnPaymentMethodItemClickListener;
import com.imedhealthus.imeddoctors.common.models.PaymentMethod;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/9/18.
 */

public class PaymentMethodItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    ViewGroup contItem;

    @BindView(R.id.iv_icon)
    ImageView ivIcon;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_details)
    TextView tvDetails;

    @BindView(R.id.iv_edit)
    ImageView ivEdit;
    @BindView(R.id.iv_delete)
    ImageView ivDelete;

    private int idx;
    private WeakReference<OnPaymentMethodItemClickListener> onPaymentMethodItemClickListener;

    public PaymentMethodItemViewHolder(View view, OnPaymentMethodItemClickListener onPaymentMethodItemClickListener) {

        super(view);
        ButterKnife.bind(this, view);

        contItem.setOnClickListener(this);
        ivEdit.setOnClickListener(this);
        ivDelete.setOnClickListener(this);
        this.onPaymentMethodItemClickListener = new WeakReference<>(onPaymentMethodItemClickListener);

    }

    public void setData(PaymentMethod method, int idx) {

        tvTitle.setText(method.getTitle());
        tvDetails.setText(method.getDetails());

        this.idx = idx;

    }


    @Override
    public void onClick(View view) {

        if (onPaymentMethodItemClickListener == null || onPaymentMethodItemClickListener.get() == null) {
            return;
        }

        switch (view.getId()){
            case R.id.cont_item:
                onPaymentMethodItemClickListener.get().onPaymentMethodItemDetailClick(idx);
                break;

            case R.id.iv_edit:
                onPaymentMethodItemClickListener.get().onPaymentMethodItemEditClick(idx);
                break;

            case R.id.iv_delete:
                onPaymentMethodItemClickListener.get().onPaymentMethodItemDeleteClick(idx);
                break;
        }
    }

}
