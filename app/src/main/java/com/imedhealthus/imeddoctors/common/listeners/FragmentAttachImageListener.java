package com.imedhealthus.imeddoctors.common.listeners;

public interface FragmentAttachImageListener {
    public void onImageCancelTapped(int position);

    public void onImageTapped(int position);
}
