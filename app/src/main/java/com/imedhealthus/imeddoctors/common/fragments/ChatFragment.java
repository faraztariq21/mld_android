package com.imedhealthus.imeddoctors.common.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.CallActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.adapters.ChatMessagesListAdapter;
import com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler;
import com.imedhealthus.imeddoctors.common.listeners.OnListItemClickListener;
import com.imedhealthus.imeddoctors.common.listeners.PopUpWindowListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.CallSignal;
import com.imedhealthus.imeddoctors.common.models.ChatItem;
import com.imedhealthus.imeddoctors.common.models.ChatMessage;
import com.imedhealthus.imeddoctors.common.models.ChatUserModel;
import com.imedhealthus.imeddoctors.common.models.ChatWrapper;
import com.imedhealthus.imeddoctors.common.models.ForumComment;
import com.imedhealthus.imeddoctors.common.models.ImageSelector;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.FileUtils;
import com.imedhealthus.imeddoctors.common.utils.Logs;
import com.imedhealthus.imeddoctors.common.utils.SharedPrefsHelper;
import com.imedhealthus.imeddoctors.common.utils.TinyDB;
import com.imedhealthus.imeddoctors.common.utils.UploadImage;
import com.imedhealthus.imeddoctors.doctor.fragments.FragmentSendImageDialog;
import com.theartofdev.edmodo.cropper.CropImage;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import org.json.JSONArray;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import droidninja.filepicker.FilePickerBuilder;

import static android.app.Activity.RESULT_OK;

public class ChatFragment extends BaseFragment implements ImageSelector.OnImageSelectionListener, PopupMenu.OnMenuItemClickListener, View.OnClickListener, OnListItemClickListener, FragmentEditFieldWithButtons.FragmentEditFieldWithButtonsListener {


    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1000;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE_DOCUMENT = 169;
    private static final int DOWNLOAD_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 383;
    public static final String CHAT_MESSAGE = "CHAT_MESSAGE";
    public static final String CHAT_WRAPPER = "CHAT_WRAPPER";
    boolean showImageDetailDialog = false;
    @BindView(R.id.iv_profile)
    ImageView ivProfile;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_mr_number)
    TextView tvMrNumber;

    @BindView(R.id.rv_messages)
    RecyclerView rvMessages;
    @BindView((R.id.tv_no_records))
    TextView tvNoRecords;


    @BindView((R.id.iv_audio))
    ImageView ivAudio;
    @BindView((R.id.iv_video))
    ImageView ivVideo;


    @BindView(R.id.iv_selected_image)
    ImageView ivSelectedImage;

    @BindView(R.id.iv_cross)
    ImageView ivCross;

    @BindView(R.id.fl_image)
    FrameLayout flImageLayout;

    private ChatMessagesListAdapter adapterChatMessages;
    private ChatItem chatItem;
    int totalPages = 0, currentPageNumber = 0;
    private ImageSelector imageSelector;
    private String uploadName;
    private String uploadPath = null;
    private String imageUploadPath = null;
    private boolean imageUploading = false;
    Uri selectedImageUri, selectedFileUri;

    List<ChatMessage> messages;
    private ChatMessage chatMessage;
    private Uri selectedFullImageUri;

    public static final int PICK_DOCUMENT_CODE = 99;
    private String urlToDownload;
    private int indexToDownload;
    private ArrayList<String> filePaths;
    private FragmentEditFieldWithButtons fragmentEditFieldWithButtons;
    ArrayList<ChatMessage> chatMessagesToSend;
    PopUpWindowListener popUpWindowListener;
    View view;

    private boolean onBackPressedHandled = true;

    public ChatFragment() {

        chatItem = SharedData.getInstance().getSelectedChatItem();
        if (chatItem != null)
            new TinyDB(ApplicationManager.getContext()).putBoolean(chatItem.getChatId() + Constants.hasUnreadMessages, false);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PopUpWindowListener)
            popUpWindowListener = (PopUpWindowListener) context;
    }

    @Override
    public String getName() {
        return "ChatFragment";
    }

    @Override
    public String getTitle() {
        return "Chat";
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ChatWrapper chatWrapper = (ChatWrapper) getArguments().getSerializable(CHAT_WRAPPER);

            chatMessage = chatWrapper.getChatMessage();
            chatItem = chatWrapper.getChatItem();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        ButterKnife.bind(this, view);


        this.view = view;
        KeyboardVisibilityEvent.setEventListener(
                getActivity(),
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        if (rvMessages != null && adapterChatMessages != null) {
                            rvMessages.scrollToPosition(adapterChatMessages.getItemCount());

                        }
                    }
                });

        initHelper();

        ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedImageUri = null;
                selectedFullImageUri = null;
                flImageLayout.setVisibility(View.GONE);
            }
        });

        fragmentEditFieldWithButtons = FragmentEditFieldWithButtons.newInstance(true, true);

        getChildFragmentManager().beginTransaction().replace(R.id.ll_cont_edit_text, fragmentEditFieldWithButtons).commit();

        if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Patient) {
            ivAudio.setVisibility(View.GONE);
            ivVideo.setVisibility(View.GONE);
        }
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }


    @Override
    public void onResume() {
        super.onResume();
        updateReadCount();

        OpenTokHandler.getInstance().setListener(this);
    }


    private void openFileSelector() {
        if (imageSelector == null) {
            imageSelector = new ImageSelector(ChatFragment.this, getActivity(), this);

        }
        imageSelector.showSourceAlert();

    }

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(getActivity(), v);
        popup.setOnMenuItemClickListener(this);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.pick_up_file_menu, popup.getMenu());
        popup.show();
    }

    public void initHelper() {

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());


        if (SharedData.getInstance().getSelectedBookedApointment() != null && SharedData.getInstance().getSelectedBookedApointment().getType() == Constants.AppointmentType.OFFICE) {
            ivAudio.setVisibility(View.GONE);
            ivVideo.setVisibility(View.GONE);
        }

        rvMessages.setLayoutManager(mLayoutManager);
        rvMessages.setHasFixedSize(true);
        rvMessages.setDrawingCacheEnabled(true);
        rvMessages.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);
        adapterChatMessages = new ChatMessagesListAdapter(getActivity(), tvNoRecords, new ArrayList<ChatMessage>(), this);

        rvMessages.setAdapter(adapterChatMessages);

        String userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        ChatUserModel partner = chatItem.getPartner(userLoginId);

        if (partner != null) {
            tvName.setText(partner.getName());
            if (partner.getMrNumber() != null && !partner.getMrNumber().equalsIgnoreCase("") && !partner.getMrNumber().equalsIgnoreCase("null")) {
                tvMrNumber.setVisibility(View.VISIBLE);
                tvMrNumber.setText(partner.getMrNumber());
            } else {
                tvMrNumber.setVisibility(View.GONE);
            }
            Glide.with(ApplicationManager.getContext()).load(partner.getImgURL()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivProfile);
        }

    }


    @Override
    public void onPause() {
        super.onPause();
        SharedData.getInstance().setSelectedBookedApointment(null);


    }


    public void onBackPressed() {
        onBackPressedHandled = true;


        if (fragmentEditFieldWithButtons.llMultipleFilesContainer.getVisibility() == View.VISIBLE) {
            fragmentEditFieldWithButtons.llMultipleFilesContainer.setVisibility(View.GONE);
            fragmentEditFieldWithButtons.fragmentAttachMultipleImages = null;
            fragmentEditFieldWithButtons.fragmentAttachMultipleDocuments = null;
        }
    }


    public void setOnBackPressedHandled(boolean onBackPressedHandled) {
        this.onBackPressedHandled = onBackPressedHandled;
    }

    public boolean isOnBackPressedHandled() {
        return onBackPressedHandled;
    }

    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.btn_send:
                addMessageToList(null);
                break;
            case R.id.iv_audio:
                if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor) {
                    startCallDoctor(false);
                } else {
                    startCallPatient(false);
                }
                break;
            case R.id.iv_video:
                if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor) {
                    startCallDoctor(true);
                } else {
                    startCallPatient(true);
                }
                break;
            case R.id.fl_plus:
                showPopup(view);
                break;
            case R.id.iv_profile:
            case R.id.tv_name:
                if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor) {
                    SharedData.getInstance().setSelectedPatientLoginId(chatItem.getPartner(CacheManager.getInstance().getCurrentUser().getLoginId()).getId());
                    SimpleFragmentActivity.startActivity(getActivity(), Constants.Fragment.PatientProfile, null);
                } else {
                    SharedData.getInstance().setSelectedDoctorLoginId(chatItem.getPartner(CacheManager.getInstance().getCurrentUser().getLoginId()).getId());
                    SimpleFragmentActivity.startActivity(getActivity(), Constants.Fragment.DoctorProfile, null);

                }

        }
    }


    private ChatMessage getMessageToSend(String text, Uri selectedImageUri, Uri selectedFileUri) {
        String userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        String message;
        message = text;


        ChatUserModel sender = chatItem.getChatUser(userLoginId);
        ChatUserModel receiver = chatItem.getPartner(userLoginId);
        chatMessage = new ChatMessage(sender.getId(), receiver.getId(), message, chatItem.getChatId(), imageUploadPath, selectedImageUri != null ? selectedImageUri.toString() : selectedFileUri != null ? selectedFileUri.toString() : null);
        chatMessage.setSenderName(sender.getName());

        if (selectedImageUri != null) {
            chatMessage.setFileType(Constants.FileType.FILE_TYPE_IMAGE);
            try {
                chatMessage.setFilePathLocal(FileUtils.getPathFromUri(getActivity(), selectedImageUri));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                chatMessage.setFilePathLocal(selectedImageUri.getPath());
            }
        } else if (selectedFileUri != null) {
            chatMessage.setFileType(Constants.FileType.FILE_TYPE_PDF);
            try {
                chatMessage.setFilePathLocal(FileUtils.getPathFromUri(getActivity(), selectedFileUri));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                chatMessage.setFilePathLocal(selectedFileUri.getPath());
            }
        } else {
            chatMessage.setFileType(Constants.FileType.FILE_TYPE_MESSAGE);
            chatMessage.setFilePathLocal(null);
        }

        return chatMessage;

    }


    private void addMessageToList(String messageToSend) {
        flImageLayout.setVisibility(View.GONE);
        String userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        String message;
        message = messageToSend;


        ChatUserModel sender = chatItem.getChatUser(userLoginId);
        ChatUserModel receiver = chatItem.getPartner(userLoginId);
        chatMessage = new ChatMessage(sender.getId(), receiver.getId(), message, chatItem.getChatId(), imageUploadPath, selectedFullImageUri != null ? selectedFullImageUri.toString() : selectedFileUri != null ? selectedFileUri.toString() : null);

        if (selectedImageUri != null) {
            chatMessage.setFileType(Constants.FileType.FILE_TYPE_IMAGE);
        } else if (selectedFileUri != null) {
            chatMessage.setFileType(Constants.FileType.FILE_TYPE_PDF);
            try {
                chatMessage.setFilePathLocal(FileUtils.getPathFromUri(getActivity(), selectedFileUri));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                chatMessage.setFilePathLocal(selectedFileUri.getPath());
            }
        } else {
            chatMessage.setFilePathLocal(null);
            chatMessage.setFileType(Constants.FileType.FILE_TYPE_MESSAGE);
        }


        adapterChatMessages.getMessages().add(chatMessage);
        adapterChatMessages.notifyDataSetChanged();

        rvMessages.smoothScrollToPosition(adapterChatMessages.getItemCount());

        if (selectedImageUri != null)
            uploadFile((new Date()).getTime() + ".png", selectedImageUri, onImageUploadCompletionListener);
        else if (selectedFileUri != null)
            uploadFile(FileUtils.getFileNameFromUri(getActivity(), selectedFileUri), selectedFileUri, onDocumentUploadCompletionListener);
        else
            sendChatMessage();

    }

    private void sendChatMessage() {

        if (this.chatMessage.getFileType().equals(Constants.FileType.FILE_TYPE_PDF))
            SharedPrefsHelper.saveFileLocalAddress(getActivity(), this.chatMessage, chatMessage.getFilePathLocal());

        ChatMessage chatMessage = new ChatMessage(this.chatMessage, chatItem);

        selectedImageUri = null;
        selectedFullImageUri = null;
        imageUploadPath = null;
        selectedFileUri = null;
        OpenTokHandler.getInstance().sendMessage(chatMessage);
        sendChatMessageToServer(chatMessage);
    }


    @Override
    public void onMessageReceived(ChatMessage chatMessage) {

        if (!chatMessage.isSameChat(chatItem.getChatId())) {
            return;
        }
        chatMessage.setTimeStamp(new Date().getTime());

        if (adapterChatMessages != null && adapterChatMessages.getMessages() != null) {
            adapterChatMessages.getMessages().add(chatMessage);
            adapterChatMessages.notifyDataSetChanged();
            rvMessages.smoothScrollToPosition(adapterChatMessages.getItemCount());
        }

    }

    @Override
    public void onMessageReceived(ChatWrapper chatWrapper) {
        super.onMessageReceived(chatWrapper);
        if (chatWrapper == null || chatWrapper.getChatMessage() == null)
            return;

        if (!chatWrapper.getChatMessage().isSameChat(chatItem.getChatId()))
            return;

        chatWrapper.getChatMessage().setTimeStamp(new Date().getTime());

        if (adapterChatMessages != null && adapterChatMessages.getMessages() != null) {
            adapterChatMessages.getMessages().add(chatWrapper.getChatMessage());
            adapterChatMessages.notifyDataSetChanged();
            rvMessages.smoothScrollToPosition(adapterChatMessages.getItemCount());
        }
    }

    @Override
    public void onCommentReceived(ForumComment forumComment) {

    }


    //Web services
    public void loadData() {

        AlertUtils.showProgress((Activity) context);

        WebServicesHandler.instance.getChatMessages(chatItem.getChatId(), Integer.toString(currentPageNumber), new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                AlertUtils.dismissProgress();
                messages = new ArrayList<>();

                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                } else {
                    JSONArray arrJson = response.optJSONArray("Messages");
                    if (arrJson != null) {
                        messages = ChatMessage.parseChatMessages(arrJson, chatItem);
                    }
                }
                adapterChatMessages.setMessages(messages);
                adapterChatMessages.notifyDataSetChanged();
                rvMessages.post(new Runnable() {
                    @Override
                    public void run() {
                        rvMessages.smoothScrollToPosition(adapterChatMessages.getItemCount());
                    }
                });
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(context, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });
    }

    private void sendChatMessageToServer(ChatMessage chatMessage) {

        WebServicesHandler.instance.sendChatMessage(chatMessage, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                if (!response.status()) {
                    Log.d("iMEd", "sendChatMessageToServer: " + response.message());
                    return;
                }
                Log.d("iMEd", "sendChatMessageToServer: success");
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                Log.d("iMEd", "sendChatMessageToServer: failure");
            }
        });

    }

    public void updateReadCount() {
        String userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        String receiverLoginId = chatItem.getPartner(userLoginId).getId();
        WebServicesHandler.instance.updateReadCount(receiverLoginId, userLoginId, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                if (response.status()) {
                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                updateReadCount();
            }
        });
    }

    private void startCallDoctor(final boolean isVideo) {


        String currentUserLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();

        String userLoginId = chatItem.getChatUser(currentUserLoginId).getId();
        final String partnerLoginId = chatItem.getPartner(currentUserLoginId).getId();
        final String partnerName = chatItem.getPartner(currentUserLoginId).getName();
        final String partnerId = chatItem.getPartner(currentUserLoginId).getId();
        final String partnerImageUrl = chatItem.getPartner(currentUserLoginId).getImgURL();
        String myName = CacheManager.getInstance().getCurrentUser().getFullName();

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.getDataForCallWithoutNotification(userLoginId, partnerLoginId, myName, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                String sessionId = response.optString("SessionId", "");
                String token = response.optString("TokenId", "");
                boolean isUserLive = response.optBoolean("IsUserLive", false);
                String chatId = response.optString("ChatId", "");
                String receiverProfileId = response.optString("PatientId", "");

                String senderName = CacheManager.getInstance().getCurrentUser().getFullName();

                if (!isUserLive) {
                    //  AlertUtils.showAlert(context, Constants.ErrorAlertTitle, bookedAppointment.getPatientFullName() + " is not online right now. We have notified him. Kindly try again in a few minutes.");

                    // CallData data = new CallData(bookedAppointment.getPatientLoginId(), senderName, "", "","");
                    // OpenTokHandler.getInstance().sendPushNotification(data);

                    // return;
                }

                SharedData.getInstance().setOpenTokCallSessionId(sessionId);
                SharedData.getInstance().setOpenTokCallToken(token);
                SharedData.getInstance().setReceiverId(partnerId);
                SharedData.getInstance().setReceiverName(partnerName);
                SharedData.getInstance().setReceiverImageUrl(partnerImageUrl);
                // CallData data = new CallData(bookedAppointment.getPatientLoginId(), senderName, sessionId, token,bookedAppointment.getDoctorId());
                //OpenTokHandler.getInstance().sendCallSignal(data);

                CallSignal callSignal = new CallSignal(partnerLoginId, receiverProfileId, senderName, isVideo ? "video" : "audio", "", chatId, CacheManager.getInstance().getCurrentUser().getLoginId(), CacheManager.getInstance().getCurrentUser().getUserId());
                SharedData.getInstance().setSelectedCallSignal(callSignal);
                //OpenTokHandler.getInstance().sendCallSignal(callSignal);

                Intent intent = new Intent(context, CallActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CallFragment.ordinal());
                ((BaseActivity) context).startActivity(intent, true);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

    private void startCallPatient(final boolean isVideo) {

        String currentUserLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();

        String userLoginId = chatItem.getChatUser(currentUserLoginId).getId();
        final String partnerLoginId = chatItem.getPartner(currentUserLoginId).getId();
        final String partnerName = chatItem.getPartner(currentUserLoginId).getName();
        final String partnerId = chatItem.getPartner(currentUserLoginId).getId();
        final String partnerImageUrl = chatItem.getPartner(currentUserLoginId).getImgURL();

        String myName = CacheManager.getInstance().getCurrentUser().getFullName();

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.getDataForCallWithoutNotification(userLoginId, partnerLoginId, myName, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                String sessionId = response.optString("SessionId", "");
                String token = response.optString("TokenId", "");
                boolean isUserLive = response.optBoolean("IsUserLive", false);
                String chatId = response.optString("ChatId", "");

                String receiverProfileId = response.optString("DoctorId", "");

                String senderName = CacheManager.getInstance().getCurrentUser().getFullName();

                if (!isUserLive) {
                    // AlertUtils.showAlert(context, Constants.ErrorAlertTitle, bookedAppointment.getDoctorFullName() + " is not online right now. We have notified him. Kindly try again in a few minutes.");

                    // CallData data = new CallData(bookedAppointment.getPatientLoginId(), senderName, "", "","");
                    // OpenTokHandler.getInstance().sendPushNotification(data);

                    //return;
                }

                SharedData.getInstance().setOpenTokCallSessionId(sessionId);
                SharedData.getInstance().setOpenTokCallToken(token);
                SharedData.getInstance().setReceiverId(partnerId);
                SharedData.getInstance().setReceiverName(partnerName);
                SharedData.getInstance().setReceiverImageUrl(partnerImageUrl);
                // CallData data = new CallData(bookedAppointment.getDoctorLoginId(), senderName, sessionId, token,bookedAppointment.getDoctorId());
                //OpenTokHandler.getInstance().sendCallSignal(data);


                CallSignal callSignal = new CallSignal(partnerLoginId, receiverProfileId, senderName, isVideo ? "video" : "audio", "", chatId, CacheManager.getInstance().getCurrentUser().getLoginId(), CacheManager.getInstance().getCurrentUser().getUserId());
                SharedData.getInstance().setSelectedCallSignal(callSignal);
                //OpenTokHandler.getInstance().sendCallSignal(callSignal);

                Intent intent = new Intent(context, CallActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CallFragment.ordinal());
                ((BaseActivity) context).startActivity(intent, true);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

    @Override
    public void onImageSelected(Bitmap bitmap, Uri croppedImageUri, Uri fullImageUri) {

        //uploadFile(uri);
        selectedImageUri = croppedImageUri;
        selectedFullImageUri = fullImageUri;
        //ivCamera.setImageURI(selectedImageUri);
        //ivSelectedImage.setImageURI(selectedImageUri);
        //flImageLayout.setVisibility(View.VISIBLE);
        CropImage.activity(selectedFullImageUri).start(getActivity());


        selectedFileUri = null;
    }

    UploadImage.OnImageUploadCompletionListener onImageUploadCompletionListener = new UploadImage.OnImageUploadCompletionListener() {
        @Override
        public void onImageUploadComplete(boolean success, int index, String uploadPath) {
            //Toast.makeText(getActivity(), "Image Attatched", Toast.LENGTH_SHORT).show();
            if (success) {
                imageUploadPath = ChatFragment.this.uploadPath;
                imageUploading = false;
                if (chatMessage != null)
                    chatMessage.setImageUrl(ChatFragment.this.uploadPath);
                sendChatMessage();
            } else {
                Toast.makeText(getActivity(), "Unable to send message", Toast.LENGTH_SHORT).show();

                adapterChatMessages.getMessages().remove(adapterChatMessages.getItemCount() - 1);
                adapterChatMessages.notifyDataSetChanged();
                //rvMessages.setAdapter(adapterChatMessages);
                rvMessages.smoothScrollToPosition(adapterChatMessages.getItemCount());


            }

        }

    };
    UploadImage.OnImageUploadCompletionListener onDocumentUploadCompletionListener = new UploadImage.OnImageUploadCompletionListener() {
        @Override
        public void onImageUploadComplete(boolean success, int index, String uploadPath) {
            //Toast.makeText(getActivity(), "Image Attatched", Toast.LENGTH_SHORT).show();
            if (success) {
                imageUploadPath = ChatFragment.this.uploadPath;
                imageUploading = false;
                chatMessage.setImageUrl(ChatFragment.this.uploadPath);
                sendChatMessage();
            } else {
                Toast.makeText(getActivity(), "Unable to send message", Toast.LENGTH_SHORT).show();

                adapterChatMessages.getMessages().remove(adapterChatMessages.getItemCount() - 1);
                adapterChatMessages.notifyDataSetChanged();
                //rvMessages.setAdapter(adapterChatMessages);
                rvMessages.smoothScrollToPosition(adapterChatMessages.getItemCount());


            }

        }

    };


    UploadImage.OnImageUploadCompletionListener onMessageFilesUploadCompletionListener = new UploadImage.OnImageUploadCompletionListener() {
        @Override
        public void onImageUploadComplete(boolean success, int index, String uploadPath) {
            chatMessagesToSend.get(index).setImageUrl(uploadPath);
            OpenTokHandler.getInstance().sendMessage(chatMessagesToSend.get(index));
            sendChatMessageToServer(chatMessagesToSend.get(index));
        }
    };

    private String uploadMessageFilesToTheServer(UploadImage.OnImageUploadCompletionListener onImageUploadCompletionListener) {


        ArrayList<String> messageFilePaths = getMessagesFilePaths(chatMessagesToSend);


        UploadImage uploadImage = new UploadImage(null, messageFilePaths, onImageUploadCompletionListener);
        uploadImage.setContext(getActivity());
        uploadImage.execute();
        setUriFieldsToNull();
        return uploadPath;
    }

    private ArrayList<String> getMessagesFilePaths(ArrayList<ChatMessage> chatMessagesToSend) {
        ArrayList<String> messagesFilePaths = new ArrayList<>();
        for (ChatMessage chatMessage : chatMessagesToSend) {

            messagesFilePaths.add(chatMessage.getFilePathLocal());
        }

        return messagesFilePaths;

    }


    private String uploadFile(String uploadName, Uri uri, UploadImage.OnImageUploadCompletionListener onImageUploadCompletionListener) {


        this.uploadName = uploadName;


        this.uploadName = this.uploadName.replaceAll("\\s", "_");
     /*   if (!this.uploadName.endsWith(".pdf"))
            this.uploadName = this.uploadName.concat(".pdf");*/

        uploadPath = Constants.FTP.patientDataPath + this.uploadName;


        UploadImage uploadImage = null;
        try {
            uploadImage = new UploadImage(uploadPath, FileUtils.getPathFromUri(getActivity(), uri), onImageUploadCompletionListener);
        } catch (Exception e) {
            e.printStackTrace();
            uploadImage = new UploadImage(uploadPath, uri.getPath(), onImageUploadCompletionListener);
        }
        uploadImage.setContext(getActivity());
        uploadImage.execute();
        setUriFieldsToNull();
        return uploadPath;
    }

    private void setUriFieldsToNull() {
        selectedFileUri = null;
        selectedImageUri = null;
        selectedFullImageUri = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)

        {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                selectedFullImageUri = resultUri;
                selectedImageUri = resultUri;
                showImageDetailDialog = true;
                selectedFileUri = selectedImageUri;
                //fragmentSendImageDialog.setFragmentSendImageClickListener(this);
                // fragmentSendImageDialog.show(getActivity().getSupportFragmentManager());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        } else
            fragmentEditFieldWithButtons.onActivityResult(requestCode, resultCode, data);

    }

    private File createImageFile() throws IOException {

        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return new File(storageDir, "image.jpg");

    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.attach_image:
                openFileSelector();
                return true;
            case R.id.attach_document:
                FilePickerBuilder.getInstance().setMaxCount(1)
                        .setSelectedFiles(filePaths)
                        .setActivityTheme(R.style.LibAppTheme)

                        .pickFile(getActivity());

                // FileUtils.chooseFiles("application/*", PICK_DOCUMENT_CODE, getActivity(), null);
                return true;
            default:
                return false;
        }
    }


    @Override
    public void onListItemClicked(int index) {
        if (messages.get(index).getFilePathLocal() == null || !FileUtils.checkFileExists(messages.get(index).getFilePathLocal()))
            messages.get(index).setFilePathLocal(SharedPrefsHelper.getFileLocalAddress(getActivity(), messages.get(index)));

        if (messages.get(index).getFilePathLocal() != null && FileUtils.checkFileExists(messages.get(index).getFilePathLocal())) {
            FileUtils.openDocument(getActivity(), messages.get(index).getFilePathLocal());

        } else if (SharedPrefsHelper.getFileLocalAddress(getActivity(), messages.get(index)) != null) {
            Toast.makeText(getActivity(), "This item is being downloaded. Please wait.", Toast.LENGTH_SHORT).show();
        } else {
            urlToDownload = Constants.BASE_URL + messages.get(index).getImageUrl();
            indexToDownload = index;

            if (ContextCompat.checkSelfPermission(getActivity(),
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        DOWNLOAD_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            } else {
                FileUtils.downloadFile(messages.get(index), null, getActivity(), urlToDownload, FileUtils.getFileNameFromURL(urlToDownload));
            }
        }
        //Toast.makeText(getActivity(), "Downloading", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        switch (requestCode) {
            case DOWNLOAD_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    FileUtils.downloadFile(messages.get(indexToDownload), null, getActivity(), urlToDownload, FileUtils.getFileNameFromURL(urlToDownload));
                } else {
                    Toast.makeText(getActivity(), "Permission denied. Unable to download", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    // functionality that depends on this permission.
                }
                return;
            }
        }

        if (fragmentEditFieldWithButtons != null)
            fragmentEditFieldWithButtons.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onSendTapped(String text, ArrayList<String> imagePaths, ArrayList<String> filePaths) {


        prepareChatMessagesToSend(text, imagePaths, filePaths);
        displayChatMessagesInList();
        uploadMessageFilesToTheServer(onMessageFilesUploadCompletionListener);

    }

    private void displayChatMessagesInList() {
        adapterChatMessages.addMessages(chatMessagesToSend);
        rvMessages.smoothScrollToPosition(adapterChatMessages.getItemCount());

    }

    private void prepareChatMessagesToSend(String text, ArrayList<String> imagePaths, ArrayList<String> filePaths) {
        chatMessagesToSend = new ArrayList<>();
        if (imagePaths.size() > 0) {
            for (String imagePath : imagePaths) {
                chatMessagesToSend.add(getMessageToSend("", FileUtils.compressImageAndGetNewImageUri(getActivity(), Uri.fromFile(new File(imagePath))), null));
            }
        } else if (filePaths.size() > 0) {
            for (String filePath : filePaths) {
                chatMessagesToSend.add(getMessageToSend("", null, Uri.fromFile(new File(filePath))));
            }
        } else
            chatMessagesToSend.add(getMessageToSend(text, null, null));

        imagePaths.clear();
        filePaths.clear();
    }

    @Override
    public void onImageSelected(Uri selectedImageUri) {
        this.selectedImageUri = selectedFullImageUri;
        this.selectedFileUri = selectedFileUri;
        FragmentSendImageDialog.newInstance(selectedImageUri.toString()).show(getChildFragmentManager());
        fragmentEditFieldWithButtons.getEtMessage().setText("");
    }

    @Override
    public void onFileSelected(Uri selectedFileUri) {

        this.selectedFileUri = selectedFileUri;
        addMessageToList("");
    }


}

