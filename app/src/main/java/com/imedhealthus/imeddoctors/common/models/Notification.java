package com.imedhealthus.imeddoctors.common.models;

import com.google.gson.annotations.SerializedName;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Malik Hamid on 1/16/2018.
 */

public class Notification {
    @SerializedName("NotificationText")
    String notificationTitle;
    @SerializedName("NotificationType")
    String notificationType;
    @SerializedName("UserId")
    String userId;
    @SerializedName("CreatedBy")
    String createdById ;
    @SerializedName("CreatedByName")
    String createdByName ;
    @SerializedName("CreatedDate")
    String createdDate;
    long createdOn;
    @SerializedName("IsActive")
    boolean isActive = false;
    @SerializedName("IsPublic")
    boolean isPublic = false;

    public Notification() {
    }
    public static ArrayList<Notification> getNotifications(JSONArray jsonObject){
        ArrayList<Notification> attachments = new ArrayList<>();
        for(int i=0;i<jsonObject.length();i++){
            try {
                Notification attachment=new Notification(jsonObject.getJSONObject(i));

                //todo when server side check is done remove this
                if(attachment.userId!="-1")
                attachments.add(attachment);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return attachments;
    }
    public Notification(JSONObject jsonObject) {
        try {
            notificationTitle = jsonObject.getString("NotificationText");
            notificationType = jsonObject.getString("NotificationType");
            userId = jsonObject.getString("UserId");
            createdById = jsonObject.getString("CreatedBy");
            createdByName = jsonObject.getString("CreatedByName");
            createdOn = GenericUtils.getTimeDateInLong(jsonObject.getString("CreatedDate"));
            isActive = jsonObject.getBoolean("IsActive");
            isPublic = jsonObject.getBoolean("IsPublic");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isActive() {
        return isActive;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public Notification(String notificationTitle, String notificationType) {
        this.notificationTitle = notificationTitle;
        this.notificationType = notificationType;
    }

    public String getNotificationTitle() {
        return notificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }
}
