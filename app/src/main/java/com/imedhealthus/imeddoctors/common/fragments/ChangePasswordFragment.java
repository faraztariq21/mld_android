package com.imedhealthus.imeddoctors.common.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePasswordFragment extends BaseFragment {

    @BindView(R.id.et_old_password)
    EditText etOldPassword;
    @BindView(R.id.et_new_password)
    EditText etNewPassword;
    @BindView(R.id.et_confirm_password)
    EditText etConfirmPassword;

    @BindView(R.id.iv_show_pass)
    ImageView ivShowPass;
    @BindView(R.id.iv_show_confirm_pass)
    ImageView ivShowConfirmPass;
    @BindView(R.id.iv_show_old_password)
    ImageView ivShowOldPass;
    private boolean isShowingPassword= false;


    @Override
    public String getName() {
        return "ChangePasswordFragment";
    }

    @Override
    public String getTitle() {
        return "Change Password";
    }

    @Override
    public boolean showHeader() {
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        ButterKnife.bind(this, view);
        
        return view;

    }

    //Actions
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_update:
                if (validateFields()) {
                    updatePassword();
                }
                break;

            case R.id.iv_show_old_password:
            case R.id.iv_show_pass:
            case R.id.iv_show_confirm_pass:
                togglePasswordVisibility();
                break;

            default:
                break;
        }
    }


    private void togglePasswordVisibility() {

        isShowingPassword = !isShowingPassword;
        if (isShowingPassword) {
            ivShowPass.setImageResource(R.drawable.ic_eye_crossed_blue);
            ivShowConfirmPass.setImageResource(R.drawable.ic_eye_crossed_blue);
            ivShowOldPass.setImageResource(R.drawable.ic_eye_crossed_blue);
            etOldPassword.setInputType(InputType.TYPE_CLASS_TEXT);
            etNewPassword.setInputType(InputType.TYPE_CLASS_TEXT);
            etConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT);
        }else {
            ivShowPass.setImageResource(R.drawable.ic_eye_normal_blue);
            ivShowOldPass.setImageResource(R.drawable.ic_eye_normal_blue);
            ivShowConfirmPass.setImageResource(R.drawable.ic_eye_normal_blue);
            etNewPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            etOldPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            etConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
        //etPassword.setSelection(etPassword.getText().length());
        //etConfirmPassword.setSelection(etPassword.getText().length());

    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;
        if (etOldPassword.getText().toString().isEmpty()) {
            errorMsg = "Kindly enter old password";
        } else if (etNewPassword.getText().toString().isEmpty()) {
            errorMsg = "Kindly enter new password";
        } else if (etConfirmPassword.getText().toString().isEmpty()) {
            errorMsg = "Kindly enter confirm new password";
        } else if (!etConfirmPassword.getText().toString().equals(etNewPassword.getText().toString())) {
            errorMsg = "Password Mismatch";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(context, Constants.ErrorAlertTitle, errorMsg);
            return false;
        }

        return true;

    }

    //Webservices
    private void updatePassword() {

        AlertUtils.showProgress(getActivity());
        String userId = CacheManager.getInstance().getCurrentUser().getLoginId();
        String oldPassword = etOldPassword.getText().toString();
        String newPassword = etNewPassword.getText().toString();
        WebServicesHandler.instance.changePassword(userId, oldPassword, newPassword, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                AlertUtils.dismissProgress();
                if(response.status())
                {
                    AlertUtils.showAlert(context, Constants.SuccessAlertTitle, "Password updated successfully");
                }else{
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, response.message());
                }

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(context, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);

            }
        });


    }
}