package com.imedhealthus.imeddoctors.common.listeners;

/**
 * Created by Malik Hamid on 1/11/2018.
 */

public interface OnPharmacyMedicationClickListener {
    void onPharmacyMedicationToggleSelection(int idx);
}
