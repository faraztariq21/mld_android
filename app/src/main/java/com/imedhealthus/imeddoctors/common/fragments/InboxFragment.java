package com.imedhealthus.imeddoctors.common.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.auth.activities.LandingActivity;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.adapters.ChatItemsListAdapter;
import com.imedhealthus.imeddoctors.common.listeners.OnChatItemClickListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.ChatItem;
import com.imedhealthus.imeddoctors.common.models.ChatMessage;
import com.imedhealthus.imeddoctors.common.models.ChatWrapper;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.Logs;
import com.imedhealthus.imeddoctors.common.utils.TinyDB;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class InboxFragment extends BaseFragment implements OnChatItemClickListener {

    @BindView(R.id.rv_inbox)
    RecyclerView rvInbox;
    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;

    private ChatItemsListAdapter adapterChatItems;

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public boolean showHeader() {
        return true;
    }

    @Override
    public String getName() {
        return "InboxFragment";
    }

    @Override
    public String getTitle() {
        if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor)
            return getResources().getString(R.string.my_patients_conversation);
        else
            return getResources().getString(R.string.my_doctors_conversation);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_inbox, container, false);
        ButterKnife.bind(this, view);
        initializeShimmer(view);

        initHelper();
        startShimmerAnimation();
        loadData();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void initHelper() {

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvInbox.setLayoutManager(mLayoutManager);

        adapterChatItems = new ChatItemsListAdapter(getActivity(), tvNoRecords, this);
        rvInbox.setAdapter(adapterChatItems);

    }


    //On chat item click
    @Override
    public void onChatItemClick(int idx) {

        ChatItem chatItem = adapterChatItems.getItem(idx);
        if (chatItem == null) {
            return;
        }

        SharedData.getInstance().setSelectedChatItem(chatItem);

        Intent intent = new Intent(context, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.ChatFragment.ordinal());
        ((BaseActivity) context).startActivity(intent, true);

    }

    @Override
    public void onChatItemFileClic(int idx) {

    }


    //Web services
    private void loadData() {

        /*if (!AlertUtils.isShowingProgress()) {
            AlertUtils.showProgress(getActivity());
        }*/

        String loginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        String userType = CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Patient ? "patient" : "doctor";

        if (loginId == null || loginId.equalsIgnoreCase("")) {
            Intent intent = new Intent(getActivity(), LandingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            getActivity().startActivity(intent);
            return;
        }

        WebServicesHandler.instance.getChat(loginId, userType, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                List<ChatItem> chatItems = new ArrayList<>();

                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                } else {
                    JSONArray arrJson = response.getJSONArray("Chats");
                    if (arrJson != null) {
                        chatItems = ChatItem.parseChatItems(arrJson);
                    }
                }
                stopShimmerAnimation();
                adapterChatItems.setChatItems(chatItems);
                updateNotificationStatus();

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
                stopShimmerAnimation();
            }
        });

    }

    public void updateNotificationStatus() {
        String userId = CacheManager.getInstance().getCurrentUser().getUserId();
        String userType = CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor ? "doctor" : "patient";
        String notificationType = "ChatWrapper";
        boolean isPublic = false;
        WebServicesHandler.instance.updateNotificationStatus(userId, userType, notificationType, isPublic, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                AlertUtils.dismissProgress();
                if (response.status()) {

                } else {

                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

            }
        });
    }

    @Override
    public void onMessageReceived(ChatMessage chatMessage) {
        super.onMessageReceived(chatMessage);
        new TinyDB(getActivity()).putBoolean(chatMessage.getChatId() + Constants.hasUnreadMessages, true);
        loadData();

    }

    @Override
    public void onMessageReceived(ChatWrapper chatWrapper) {
        super.onMessageReceived(chatWrapper);
        loadData();
    }
}
