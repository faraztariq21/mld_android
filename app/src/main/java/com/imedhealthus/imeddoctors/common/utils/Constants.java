package com.imedhealthus.imeddoctors.common.utils;

import android.content.Context;
import android.media.MediaPlayer;

import com.imedhealthus.imeddoctors.R;

/**
 * Created by umair on 1/1/18.
 */

public class Constants {

    public static final int FOREGROUND_NOTIFICATION_ID = 15;
    public static final String BROADCAST_EVENT = "broadcast_event";
    public static final String BROADCAST_EVENT_TIME = "broadcast_event_time";

    public static final String IS_TIME = "is_time";

    public static final String PREVIOUS_LOGIN_TYPE = "previous_login_type";
    public static final String PREVIOUS_LOGIN_DOCTOR = "previous_login_doctor";
    public static final String PREVIOUS_LOGIN_PATIENT = "previous_login_patient";

    public static final String hasUnreadMessages = " hasUnReadMessages";
    public static final String FirebaseMessage = "FIREBASE_MESSAGE";
    public static final String EXTERNAL_STORAGE_DIRECTORY = "/MyLiveDoctors/files";
    public static final String EXTERNAL_STORAGE_IMAGES = "/MyLiveDoctors/images";
    public static final String EXTERNAL_STORAGE_DIRECTORY_SENT_FILES = "/MyLiveDoctors/files/sent";
    public static final int DOWNLOAD_FILE_WRITE_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 1002;
    public static final int PICK_DOCUMENT_WRITE_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 1003;
    public static final int CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084;
    public static final String TIME = "time_in_seconds";

    public static final String TAG = "tag";
    public static final String SET_UP_VIEW_FOR_AUDIO_CALL = "setup_view_for_audio_call";
    public static final String SET_UP_VIEW_FOR_VIDEO_CALL = "setup_view_for_video_call";
    public static final String STREAM_RECEIVED = "stream_received";
    public static final String STREAM_DROPPED = "stream_dropped";
    public static final String SESSION_ERROR = "session_error";
    public static final String CALL_REJECTED = "call_rejected";
    public static final String CALL_ENDED = "call_ended";
    public static final String START_CALL = "start_call";
    public static final String START_CALL_FAILURE = "start_call_failure";
    public static final String CALL_DROPPED = "call_dropped";
    public static final String CALL_MISSED = "call_missed";
    public static final String OPEN_POP_UP_WINDOW = "open_pop_up_window";
    public static final String CLOSE_POP_UP_WINDOW = "close_pop_up_window";
    public static final String END_CALL = "end_call";
    public static final String END_CALL_TIME = "_end_call_time";
    public static final long   TWO_HOURS_DIFFERENCE_IN_MILLISECOND = 7200000;


    public static final String NOTIFICATION_TYPE_COMMENT = "NOTIFICATION";
    public static final String NOTIFICATION_TYPE_MESSAGE = "MSG";
    public static String API_KEY = "46197082";


    public interface FileType {

        public static final String FILE_TYPE_IMAGE = "image";
        public static final String FILE_TYPE_PDF = "pdf";
        public static final String FILE_TYPE_MESSAGE = "message";

    }


    public static final String IS_APPOINTMENT_SLOT_FRAGMENT = "is_appointment_slot_fragment";
    public static final String IMAGE_URL = "ImageUrl";
    public static final int ADD_CLINIC_REQUEST = 1999;
    public static String CRITTERCISM_APP_ID = "";
    public static MediaPlayer mediaPlayer;
    public static final String BASE_URL = "https://mylivedoctors.com";

    public interface NOTIFICATIONSID {

        int callNotificationId = 1976;
    }

    public enum UserType {
        Patient, Doctor, NONE;

        public static UserType fromInteger(int value) {
            switch (value) {
                case 0:
                    return Patient;
                case 1:
                    return Doctor;
                default:
                    return NONE;
            }
        }

        public static String getString(UserType value) {
            switch (value) {
                case Patient:
                    return "patient";
                case Doctor:
                    return "doctor";
                default:
                    return "";
            }
        }
    }

    public enum AppointmentType {

        OFFICE, Tele;

        public String toString(Context context) {
            switch (this) {
                case OFFICE:
                    return context.getResources().getString(R.string.physical_appointment);
                case Tele:
                    return context.getResources().getString(R.string.tele_appointment);
            }
            return "";
        }

        public static AppointmentType fromString(String value) {
            if (value.equalsIgnoreCase("physical") || value.equalsIgnoreCase("physical appointment")) {
                return AppointmentType.OFFICE;
            } else {
                return AppointmentType.Tele;
            }
        }

    }

    public enum Fragment {

        PatientHome, DoctorHome, Notifications, SearchDoctor, DoctorsList, Appointments, SelectTime,
        PatientProfile, PatientsList, TreatmentPlan, PrescriptionDetail,
        EditPatientPersonalInfo, AddEditPrescribedMedication, AddAttachment, AddEditInsurance, PaymentMethods,
        DoctorProfile, Invoices, BookAppointment, CompleteAppointment, AddEditPaymentMethod, Forum, Inbox, Questions, Referral, ChangePassword,
        Feedback, AboutUs, None, Paybill, SubmitDoctorFeedback, AddEditEmergencyContact, AddEditGuardian,
        AddEditDisease, AddEditCurrentMedication, AddEditPastPhysician, AddEditPastSurgery, AddEditAllergy, AddEditPastPharmacy,
        AddEditHospitalization, AddEditFamilyHistory, AddEditHobby, AddEditSocialHistory,
        AddEditEducationDetail, AddEditExperience, AddEditSpeciality, AddEditSkillTraining, AddEditTrainingProvider, AddEditSocialLink, AddEditLocation,
        AddEditAcceptedInsurance, AddEditCertificate, AddEditMembership, AddEditReference, AddEditAward, AddEditLanguage, EditDoctorPersonalInfo, AddAppointmentSlots,
        AddEditService, AddEditPregnancyHistory, AddEditPregnancyDetail, CallFragment, ForumPostQuestionFragment, ForumCommentsFragment, ChatFragment, SendToPharmacy, ARCHIVED_APPOINTMENTS, FragmentImageFull;

        public static Fragment fromInt(int idx) {
            return Fragment.values()[idx];
        }

    }

    public static String SuccessAlertTitle = "Success";
    public static String ErrorAlertTitle = "Problem";
    public static String GenericErrorMsg = "Oops! It looks like your phone has lost Internet connection.";

    public static long callStartTime = 0;

    public interface URLS {
        // Staging URL
        //String BaseApis = "http://staging.mylivedoctors.com";
        // Live URL
        String BaseApis = "https://mylivedoctors.com";
        String PrivacyPolicy = "https://mylivedoctors.com/Home/Contact";
    }

    public interface FTP {
        // Staging FTP
        // String hostName = "67.225.176.164";
        // String username = "MobileDev";
        // String password = "Fswb288^";

        // Live FTP
        String hostName = "mylivedoctors.com";
        String username = "mylivedrftp";
        String password = "admin@123";

        String patientPath = "/Images/Patient/";
        String patientDataPath = "/Images/PatientPhotos/";
    }

    public interface Keys {
        String FragmentId = "FragmentId";
        String PushNotificationBroadCast = "PushNotificationBroadCast";
        String PushNotificationData = "PushNotificationData";
        String NotificationTitle = "NotificationTitle";
        String NotificationBody = "NotificationBody";
    }

    public interface OpenTokConfig {

        //        String API_KEY = "46080142";


        //--- independent credintials
        //String API_KEY = "46080122";
        String sessionId = "1_MX40NjA4MDEyMn5-MTUyMTExMjM5Njc3MH51ak5WTklyVHkzVGJUL0VZVGhCR0RWeGZ-fg";
        String token = "T1==cGFydG5lcl9pZD00NjA4MDEyMiZzaWc9Yzk4YTc5MDhmYWE2ZGE1OWQwOTk2MGYzZWZiNjUyOWM5OTdhMjY1ZDpzZXNzaW9uX2lkPTFfTVg0ME5qQTRNREV5TW41LU1UVXlNVEV4TWpNNU5qYzNNSDUxYWs1V1RrbHlWSGt6VkdKVUwwVlpWR2hDUjBSV2VHWi1mZyZjcmVhdGVfdGltZT0xNTIxMTEyNDIzJm5vbmNlPTAuNTAwNTA0NTYzNDE3NDM1JnJvbGU9cHVibGlzaGVyJmV4cGlyZV90aW1lPTE1MjM3MDQ0MjEmaW5pdGlhbF9sYXlvdXRfY2xhc3NfbGlzdD0=";
    }

}
