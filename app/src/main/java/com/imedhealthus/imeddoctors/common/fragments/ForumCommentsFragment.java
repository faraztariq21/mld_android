package com.imedhealthus.imeddoctors.common.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.adapters.ForumCommentsAdapter;
import com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler;
import com.imedhealthus.imeddoctors.common.listeners.OnChatItemClickListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.ChatMessage;
import com.imedhealthus.imeddoctors.common.models.ForumComment;
import com.imedhealthus.imeddoctors.common.models.ForumQuestion;
import com.imedhealthus.imeddoctors.common.models.GeneralListItemImage;
import com.imedhealthus.imeddoctors.common.models.ImageSelector;
import com.imedhealthus.imeddoctors.common.models.ListItemImage;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.NetworkResponseParser;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.FileUtils;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.common.utils.Logs;
import com.imedhealthus.imeddoctors.common.utils.SharedPrefsHelper;
import com.imedhealthus.imeddoctors.common.utils.UploadImage;
import com.imedhealthus.imeddoctors.doctor.fragments.FragmentSendImageDialog;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import org.json.JSONArray;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForumCommentsFragment extends BaseFragment implements FragmentEditDelete.OnFragmentEditDeleteClickListener, ImageSelector.OnImageSelectionListener, FragmentEditFieldWithButtons.FragmentEditFieldWithButtonsListener, FragmentSendImageDialog.FragmentSendImageClickListener {

    @BindView(R.id.iv_profile)
    ImageView ivProfile;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_question)
    TextView tvQuestion;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.rv_messages)
    RecyclerView rvMessages;
    @BindView((R.id.tv_no_records))
    TextView tvNoRecords;
    @BindView(R.id.comments_scroll_view)
    NestedScrollView nestedScrollView;
    String commentToEditImageUrl;
    /* @BindView((R.id.et_new_message))
     EditText etNewMessage;
     @BindView((R.id.btn_send))
     Button btnSend;*/
    @BindView(R.id.iv_question)
    ImageView ivQuestion;


   /* @BindView(R.id.iv_camera)
    ImageView ivCamera;*/

  /*  @BindView(R.id.pb_image)
    ProgressBar progressBar;*/

    FragmentEditFieldWithButtons fragmentEditFieldWithButtons;


    public static final String FORUM_QUESTION = "FORUM_QUESTION";
    private ForumCommentsAdapter forumCommentsAdapter;
    private ForumQuestion forumQuestion;
    int totalPages = 0, currentPageNumber = 0;
    private boolean imageUploading = false;
    private ImageSelector imageSelector;
    private Uri selectedImageUri = null;
    private String uploadName;
    private String uploadPath;
    private String imageUploadPath;
    List<ForumComment> forumComments;
    private ForumComment forumComment;
    private int commentIndexToEdit = -1;
    private ForumComment forumCommentToEdit;
    private Uri selectedFileUri;
    private ArrayList<ForumComment> forumCommentsToSend;

    public ForumCommentsFragment() {
        forumQuestion = SharedData.getInstance().getSelectedForumQuestion();


    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            forumQuestion = (ForumQuestion) getArguments().getSerializable(FORUM_QUESTION);
        }
    }

    @Override
    public String getName() {
        return "ForumCommentsFragment";
    }

    @Override
    public String getTitle() {
        return "Comments";
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    public void scrollScrollView() {
        nestedScrollView.post(new Runnable() {
            @Override
            public void run() {
                nestedScrollView.fullScroll(View.FOCUS_DOWN);
                fragmentEditFieldWithButtons.getFocusOnEditText();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_forum_comments, container, false);
        ButterKnife.bind(this, view);

        if (forumQuestion == null) {
            ((BaseActivity) context).onBackPressed();

        }
        KeyboardVisibilityEvent.setEventListener(
                getActivity(),
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        if (rvMessages != null && forumCommentsAdapter != null) {
                            // rvMessages.smoothScrollToPosition(forumCommentsAdapter.getItemCount());
                            scrollScrollView();
                        }
                    }
                });
        initHelper();

        fragmentEditFieldWithButtons = FragmentEditFieldWithButtons.newInstance(true, true);
        getChildFragmentManager().beginTransaction().replace(R.id.ll_cont_edit_text, fragmentEditFieldWithButtons).commit();
        loadComments();
        return view;

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onResume() {
        super.onResume();
        OpenTokHandler.getInstance().setListener(this);
    }

    FragmentEditDelete fragmentEditDelete;

    private String urlToDownload;
    private int idxToDownload;
    OnChatItemClickListener onChatItemClickListener = new OnChatItemClickListener() {
        @Override
        public void onChatItemClick(int idx) {

            if (forumComments.get(idx).getLoginId() == null || forumComments.get(idx).getLoginId().equals(CacheManager.getInstance().getCurrentUser().getLoginId())) {
                fragmentEditDelete = FragmentEditDelete.newInstance(getResources().getString(R.string.edit), getResources().getString(R.string.delete), idx);
                //fragmentEditDelete.show(getChildFragmentManager(), FragmentEditDelete.class.toString());
            }
        }

        @Override
        public void onChatItemFileClic(int idx) {
            if (forumComments.get(idx).getFilePathLocal() == null || !FileUtils.checkFileExists(forumComments.get(idx).getFilePathLocal()))
                forumComments.get(idx).setFilePathLocal(SharedPrefsHelper.getForumCommentFileLocalAddress(getActivity(), forumComments.get(idx)));

            if (forumComments.get(idx).getFilePathLocal() != null && FileUtils.checkFileExists(forumComments.get(idx).getFilePathLocal())) {
                FileUtils.openDocument(getActivity(), forumComments.get(idx).getFilePathLocal());

            } else if (SharedPrefsHelper.getForumCommentFileLocalAddress(getActivity(), forumComments.get(idx)) != null) {
                Toast.makeText(getActivity(), "This item is being downloaded. Please wait.", Toast.LENGTH_SHORT).show();
            } else {
                urlToDownload = Constants.BASE_URL + forumComments.get(idx).getFileUrl();
                idxToDownload = idx;

                if (ContextCompat.checkSelfPermission(getActivity(),
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            Constants.DOWNLOAD_FILE_WRITE_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
                } else {
                    FileUtils.downloadFile(null, forumComments.get(idx), getActivity(), urlToDownload, FileUtils.getFileNameFromURL(urlToDownload));
                }
            }
        }
    };


    public void setForumComments(ArrayList<ForumComment> forumComments) {
        this.forumComments = forumComments;

    }


    public void initHelper() {

        if (forumQuestion == null)
            return;
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());

        rvMessages.setLayoutManager(mLayoutManager);

        forumCommentsAdapter = new ForumCommentsAdapter(getActivity(), tvNoRecords, onChatItemClickListener);
        rvMessages.setAdapter(forumCommentsAdapter);

        if ((forumQuestion.getName() != null && !forumQuestion.getName().equalsIgnoreCase(""))
                && !forumQuestion.isAnonymous()) {
            tvName.setText(forumQuestion.getName());
        } else {
            tvName.setText("Anonymous");
        }

        if (forumQuestion.getFileUrl() != null && !forumQuestion.getFileUrl().equals("null")) {
            if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor || CacheManager.getInstance().getCurrentUser().getLoginId().equals(forumQuestion.getLoginId()))
                Glide.with(getActivity()).load(Constants.BASE_URL + forumQuestion.getFileUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).thumbnail(0.5f).into(ivQuestion);
            else
                ivQuestion.setVisibility(View.GONE);

        } else
            ivQuestion.setVisibility(View.GONE);


        tvQuestion.setText(forumQuestion.getQuestion());
        long timeStamp = GenericUtils.getTimeDateInLong(forumQuestion.getCreatedDate());
        String timeAgo = GenericUtils.getPassedTime(timeStamp);
        tvTime.setText(timeAgo);
        if (forumQuestion.getProfilePicUrl() != null && !forumQuestion.getProfilePicUrl().equalsIgnoreCase("")) {
            Glide.with(ApplicationManager.getContext()).load(forumQuestion.getProfilePicUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivProfile);
        }


        ivQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*SharedData.getInstance().setSelectedImageURL(forumQuestion.getFileUrl());
                SimpleFragmentActivity.startActivity(getActivity(),Constants.Fragment.FragmentImageFull);
                */
                FragmentImage fragmentImageDialog = FragmentImage.newInstance(null, Constants.BASE_URL + forumQuestion.getFileUrl());
                fragmentImageDialog.show(getActivity().getSupportFragmentManager());
            }
        });

    }

    private void displayForumCommentsInList() {


        forumCommentsAdapter.addForumComments(forumCommentsToSend);
        scrollScrollView();
        //rvMessages.smoothScrollToPosition(forumCommentsAdapter.getItemCount());

    }


    //Actions
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_send:
                addCommentToList("");
                break;

            case R.id.iv_camera:
                openFileSelector();
                break;
        }
    }

    UploadImage.OnImageUploadCompletionListener onImageUploadCompletionListener = new UploadImage.OnImageUploadCompletionListener() {
        @Override
        public void onImageUploadComplete(boolean success, int index, String uploadPath) {
            //Toast.makeText(getActivity(), "Image Attatched", Toast.LENGTH_SHORT).show();
            if (success) {
                imageUploadPath = ForumCommentsFragment.this.uploadPath;
                //progressBar.setVisibility(View.GONE);
                imageUploading = false;
                if (forumComment != null)
                    forumComment.setFileUrl(ForumCommentsFragment.this.uploadPath);
                sendChatMessage(" ");
            } else {
                Toast.makeText(getActivity(), "Unable to send message", Toast.LENGTH_SHORT).show();

                forumCommentsAdapter.getForumComments().remove(forumCommentsAdapter.getItemCount() - 1);
                forumCommentsAdapter.notifyDataSetChanged();
                //rvMessages.setAdapter(adapterChatMessages);
                //rvMessages.smoothScrollToPosition(forumCommentsAdapter.getItemCount());
                //nestedScrollView.scrollTo(0, nestedScrollView.getBottom());
                scrollScrollView();
            }

            setUriFieldsToNull();

        }

    };
    UploadImage.OnImageUploadCompletionListener onDocumentUploadCompletionListener = new UploadImage.OnImageUploadCompletionListener() {
        @Override
        public void onImageUploadComplete(boolean success, int index, String uploadPath) {
            //Toast.makeText(getActivity(), "Image Attatched", Toast.LENGTH_SHORT).show();
            if (success) {
                imageUploadPath = ForumCommentsFragment.this.uploadPath;
                //progressBar.setVisibility(View.GONE);
                imageUploading = false;
                forumComment.setFileUrl(ForumCommentsFragment.this.uploadPath);
                sendChatMessage("");
            } else {
                Toast.makeText(getActivity(), "Unable to send message", Toast.LENGTH_SHORT).show();

                forumCommentsAdapter.getForumComments().remove(forumCommentsAdapter.getItemCount() - 1);
                forumCommentsAdapter.notifyDataSetChanged();
                //rvMessages.setAdapter(adapterChatMessages);
                //rvMessages.smoothScrollToPosition(forumCommentsAdapter.getItemCount());
                //nestedScrollView.scrollTo(0, nestedScrollView.getBottom());
                scrollScrollView();

            }

            setUriFieldsToNull();

        }

    };

    private void addCommentToList(String comment) {
        String userName = CacheManager.getInstance().getCurrentUser().getFullName();
        String profileImageUrl = CacheManager.getInstance().getCurrentUser().getImageUrl();
        //String comment = etNewMessage.getText().toString();
        if (TextUtils.isEmpty(comment.trim()) && (selectedImageUri == null && selectedFileUri == null)) {
            AlertUtils.showAlert(context, Constants.ErrorAlertTitle, "Please add some text");
            return;
        }

        if (GenericUtils.isNetworkNotAvailable(getActivity())) {
            AlertUtils.showAlert(context, Constants.ErrorAlertTitle, "Internet Not Avaiable");
            return;
        }
//        ChatUserModel sender = chatItem.getChatUser(userLoginId);
//        ChatUserModel receiver = chatItem.getPartner(userLoginId);
//
        if (forumCommentToEdit != null) {
            forumComment = forumCommentToEdit;
            forumComment.setComment(comment);
            //forumComment.setFileUrl(imageUploadPath);
            //forumComment.setImageURI(selectedFullImageUri != null ? selectedFullImageUri.toString() : null);
            String fileURI = null;
            if (selectedImageUri != null)
                fileURI = selectedImageUri.toString();
            if (selectedFileUri != null)
                fileURI = selectedFileUri.toString();
            forumComment.setImageURI(fileURI);
            //        OpenTokHandler.getInstance().sendMessage(chatMessage);
            if (selectedImageUri != null) {
                forumComment.setFileType(Constants.FileType.FILE_TYPE_IMAGE);
            } else if (selectedFileUri != null) {
                forumComment.setFileType(Constants.FileType.FILE_TYPE_PDF);
                try {
                    forumComment.setFilePathLocal(FileUtils.getPathFromUri(getActivity(), selectedFileUri));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    forumComment.setFilePathLocal(selectedFileUri.getPath());
                }
            } else {
                forumComment.setFileType(Constants.FileType.FILE_TYPE_MESSAGE);
            }


            forumComments.add(commentIndexToEdit, forumComment);

            forumCommentsAdapter.setForumComments(forumComments);


        } else {
            forumComment = new ForumComment();
            forumComment.setName(userName);
            forumComment.setComment(comment);

            forumComment.setCommentId(forumQuestion.getQuestionId());
            forumComment.setCreatedDate(Long.toString(new Date().getTime()));
            forumComment.setProfilePicUrl(profileImageUrl);
            forumComment.setFileUrl(imageUploadPath);
            String fileURI = null;
            if (selectedImageUri != null)
                fileURI = selectedImageUri.toString();
            if (selectedFileUri != null)
                fileURI = selectedFileUri.toString();
            forumComment.setImageURI(fileURI);
            //        OpenTokHandler.getInstance().sendMessage(chatMessage);
            if (selectedImageUri != null) {
                forumComment.setFileType(Constants.FileType.FILE_TYPE_IMAGE);
            } else if (selectedFileUri != null) {
                forumComment.setFileType(Constants.FileType.FILE_TYPE_PDF);
                try {
                    forumComment.setFilePathLocal(FileUtils.getPathFromUri(getActivity(), selectedFileUri));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    forumComment.setFilePathLocal(selectedFileUri.getPath());
                }
            } else {
                forumComment.setFileType(Constants.FileType.FILE_TYPE_MESSAGE);
            }
            if (forumComments != null)
                forumComments.add(forumComment);
            forumCommentsAdapter.setForumComments(forumComments);
        }


        //rvMessages.setAdapter(forumCommentsAdapter);
        rvMessages.post(new Runnable() {
            @Override
            public void run() {
                //rvMessages.smoothScrollToPosition(forumCommentsAdapter.getItemCount());
                //nestedScrollView.scrollTo(0, nestedScrollView.getBottom());
                scrollScrollView();
            }
        });
        //rvMessages.smoothScrollToPosition(forumCommentsAdapter.getItemCount());
        //  ivCamera.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_camera));

        if (selectedImageUri != null)
            uploadFile((new Date()).getTime() + ".png", selectedImageUri, onImageUploadCompletionListener);
        else if (selectedFileUri != null)
            uploadFile(FileUtils.getFileNameFromUri(getActivity(), selectedFileUri), selectedFileUri, onDocumentUploadCompletionListener);
        else
            sendChatMessage("");
    }

    private void sendChatMessage(String comment) {


        OpenTokHandler.getInstance().sendComment(forumComment);

        if (forumCommentToEdit != null)
            updateForumComment(forumCommentToEdit);
        else
            postForumComment(forumQuestion.getQuestionId(), forumComment.getComment());

        //etNewMessage.setText("");
        //forumCommentsAdapter.addForumComment(forumComment);
        //ivCamera.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ic_camera));
    }


    private void openFileSelector() {
        if (imageSelector == null) {
            imageSelector = new ImageSelector(this, getActivity(), this);
        }
        imageSelector.showSourceAlert();

    }

    //Message receiver
    @Override
    public void onMessageReceived(ChatMessage chatMessage) {

//        if(!chatMessage.isSameChat(chatItem.getChatId())){
//            return;
//        }
//        adapterChatMessages.addMessage(chatMessage);
//        rvMessages.smoothScrollToPosition(adapterChatMessages.getItemCount());
//        adapterChatMessages.notifyDataSetChanged();
    }

    @Override
    public void onCommentReceived(ForumComment forumComment) {
        /*if (forumCommentWrapper != null) {
            this.forumComment = forumCommentWrapper.getForumComment();
            this.forumQuestion = forumCommentWrapper.getForumQuestion();
        }
*/
        if (forumComment.isFromSameThread(forumQuestion.getQuestionId())) {
            forumCommentsAdapter.addForumComment(forumComment);
            scrollScrollView();
        }
    }


    public void loadComments() {

        AlertUtils.showProgress((Activity) context);

        WebServicesHandler.instance.getPublicForumComments(forumQuestion.getQuestionId(), new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                AlertUtils.dismissProgress();
                forumComments = new ArrayList<>();

                if (!response.status()) {

                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                } else {
                    JSONArray arrJson = response.getJSONArray("CommentInfo");
                    if (arrJson != null) {
                        forumComments = NetworkResponseParser.getPublicForumComments(arrJson);
                    }
                }
                forumCommentsAdapter.setForumComments(forumComments);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });
    }

    private void postForumComment(String questionId, String comment) {

        String loginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        String userId = CacheManager.getInstance().getCurrentUser().getUserId();
        String isDoctor = CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor ? "true" : "false";
        String fileType = null;
        if (selectedImageUri != null)
            fileType = Constants.FileType.FILE_TYPE_IMAGE;
        else if (selectedFileUri != null)
            fileType = Constants.FileType.FILE_TYPE_PDF;
        else
            fileType = Constants.FileType.FILE_TYPE_MESSAGE;
        WebServicesHandler.instance.postForumComment(questionId, loginId, userId, comment, isDoctor, uploadPath, fileType, "0", new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                AlertUtils.dismissProgress();

                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });
    }

    private void postForumComment(ForumComment forumComment) {

        String loginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        String userId = CacheManager.getInstance().getCurrentUser().getUserId();
        String isDoctor = CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor ? "true" : "false";


        WebServicesHandler.instance.postForumComment(forumComment.getCommentId(), loginId, userId, forumComment.getComment(), isDoctor, forumComment.getFileUrl(), forumComment.getFileType(), "0", new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                AlertUtils.dismissProgress();

                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });
    }

    private void updateForumComment(ForumComment forumComment) {

        String loginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        String userId = CacheManager.getInstance().getCurrentUser().getUserId();
        String isDoctor = CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor ? "true" : "false";

        WebServicesHandler.instance.updateForumComment(forumComment.getCommentId(), forumQuestion.getQuestionId(), loginId, userId, forumComment.getComment(), isDoctor, forumComment.getFileUrl(), forumComment.getFileType(), "0", new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                AlertUtils.dismissProgress();

                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });
        forumCommentToEdit = null;

    }

    @Override
    public void onEditClick(int idx) {
        commentIndexToEdit = idx;
        forumCommentToEdit = forumComments.get(idx);
        commentToEditImageUrl = forumCommentToEdit.getFileUrl();
        forumComments.remove(idx);
        forumCommentsAdapter.notifyItemRemoved(idx);
        forumCommentsAdapter.notifyItemRangeChanged(idx, forumComments.size());
        fragmentEditFieldWithButtons.getEtMessage().setText(forumCommentToEdit.getComment());

        if (forumCommentToEdit.getFileType().equals(Constants.FileType.FILE_TYPE_IMAGE)) {
            if (fragmentEditFieldWithButtons != null) {

                ArrayList<ListItemImage> listItemImages = new ArrayList<>();
                listItemImages.add(new GeneralListItemImage(forumCommentToEdit.getFileUrl(), false, false));
                fragmentEditFieldWithButtons.displaySelectedImages(listItemImages);
            }

        } else if (forumCommentToEdit.getFileType().equals(Constants.FileType.FILE_TYPE_PDF)) {
            if (fragmentEditFieldWithButtons != null) {

                ArrayList<ListItemImage> listItemImages = new ArrayList<>();
                listItemImages.add(new GeneralListItemImage(forumCommentToEdit.getFileUrl(), false, false));
                fragmentEditFieldWithButtons.displaySelectedDocuments(listItemImages);
            }
        }


    }

    @Override
    public void onDeleteClick(final int idx) {
        //Toast.makeText(getActivity(), "Delete was clicked", Toast.LENGTH_SHORT).show();

        DialogInterface.OnClickListener positiveClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteForumComment(forumComments.get(idx).getCommentId(), idx);
            }
        };

        AlertUtils.showDeleteAlert(getActivity(), "Delete this comment", "Are you sure you want to delete this comment. It cannot be undone.", positiveClickListener);


//        deleteForumComment(forumComments.get(idx).getCommentId(), idx);
    }


    public void deleteForumComment(String questionId, final int idx) {
        WebServicesHandler.instance.deleteComment(questionId, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                forumComments.remove(idx);
                forumCommentsAdapter.notifyItemRemoved(idx);
                forumCommentsAdapter.notifyItemRangeChanged(idx, forumComments.size());

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

            }
        });
    }

    @Override
    public void onImageSelected(Bitmap bitmap, Uri uri, Uri fullImageUri) {

        //uploadFile(uri);
        selectedImageUri = uri;
        // ivCamera.setImageURI(selectedImageUri);
        selectedFileUri = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        fragmentEditFieldWithButtons.onActivityResult(requestCode, resultCode, data);

    }


    private String uploadFile(String uploadName, Uri uri, UploadImage.OnImageUploadCompletionListener onImageUploadCompletionListener) {


        this.uploadName = uploadName;


        this.uploadName = this.uploadName.replaceAll("\\s", "_");
     /*   if (!this.uploadName.endsWith(".pdf"))
            this.uploadName = this.uploadName.concat(".pdf");*/

        uploadPath = Constants.FTP.patientDataPath + this.uploadName;


        UploadImage uploadImage = null;
        try {
            uploadImage = new UploadImage(uploadPath, FileUtils.getPathFromUri(getActivity(), uri), onImageUploadCompletionListener);
        } catch (Exception e) {
            e.printStackTrace();
            uploadImage = new UploadImage(uploadPath, uri.getPath(), onImageUploadCompletionListener);

        }
        uploadImage.setContext(getActivity());
        uploadImage.execute();
        //  setUriFieldsToNull();
        return uploadPath;
    }

    UploadImage.OnImageUploadCompletionListener onMessageFilesUploadCompletionListener = new UploadImage.OnImageUploadCompletionListener() {
        @Override
        public void onImageUploadComplete(boolean success, int index, String uploadPath) {
            forumCommentsToSend.get(index).setFileUrl(uploadPath);
            OpenTokHandler.getInstance().sendComment(forumCommentsToSend.get(index));
            postForumComment(forumCommentsToSend.get(index));

        }
    };

    UploadImage.OnImageUploadCompletionListener onEditMessageFilesUploadCompletionListener = new UploadImage.OnImageUploadCompletionListener() {
        @Override
        public void onImageUploadComplete(boolean success, int index, String uploadPath) {
            forumCommentsToSend.get(index).setFileUrl(uploadPath);
            OpenTokHandler.getInstance().sendComment(forumCommentsToSend.get(index));
            updateForumComment(forumCommentsToSend.get(index));
        }
    };

    private String uploadCommentsFilesToTheServer(UploadImage.OnImageUploadCompletionListener onImageUploadCompletionListener) {


        ArrayList<String> messageFilePaths = getCommentsFilePaths(forumCommentsToSend);
        UploadImage uploadImage = new UploadImage(null, messageFilePaths, onImageUploadCompletionListener);
        uploadImage.setContext(getActivity());
        uploadImage.execute();
        setUriFieldsToNull();
        return uploadPath;
    }

    private ArrayList<String> getCommentsFilePaths(ArrayList<ForumComment> forumCommentsToSend) {

        ArrayList<String> commentsFilePaths = new ArrayList<>();
        for (ForumComment forumComment : forumCommentsToSend) {
            commentsFilePaths.add(forumComment.getFilePathLocal());
        }
        return commentsFilePaths;

    }

    @Override
    public void onSendTapped(String text, ArrayList<String> imagePaths, ArrayList<String> filePaths) {
        if (forumCommentToEdit == null) {
            prepareForumCommentsToSend(text, imagePaths, filePaths);
            displayForumCommentsInList();
            uploadCommentsFilesToTheServer(onMessageFilesUploadCompletionListener);
        } else {

            forumCommentsToSend = new ArrayList<>();
            forumCommentToEdit.setComment(fragmentEditFieldWithButtons.getEtMessage().getText().toString());
            if ((forumCommentToEdit.getFileType().equals(Constants.FileType.FILE_TYPE_IMAGE) && imagePaths.size() == 0) || (forumCommentToEdit.getFileType().equals(Constants.FileType.FILE_TYPE_PDF) && filePaths.size() == 0)) {
                forumCommentToEdit.setFilePathLocal(null);
                forumCommentToEdit.setFileUrl(null);
                forumCommentToEdit.setFileType(Constants.FileType.FILE_TYPE_MESSAGE);
            }


            /*else {
                if (commentToEditImageUrl != null && !commentToEditImageUrl.equals(imagePaths.get(0)))
                    forumCommentToEdit.setFilePathLocal(FileUtils.getPathFromUri(getActivity(), FileUtils.compressImageAndGetNewImageUri(getActivity(), Uri.fromFile(new File(imagePaths.get(0))))));

            }
            */


            forumCommentsToSend.clear();
            forumCommentsToSend.add(forumCommentToEdit);

            forumCommentsAdapter.getForumComments().add(commentIndexToEdit, forumCommentToEdit);
            forumCommentsAdapter.notifyDataSetChanged();

            uploadCommentsFilesToTheServer(onEditMessageFilesUploadCompletionListener);


        }

    }

    private void prepareForumCommentsToSend(String text, ArrayList<String> imagePaths, ArrayList<String> filePaths) {
        forumCommentsToSend = new ArrayList<>();
        if (imagePaths.size() > 0) {
            for (String imagePath : imagePaths) {
                forumCommentsToSend.add(getForumCommentToSend("", FileUtils.compressImageAndGetNewImageUri(getActivity(), Uri.fromFile(new File(imagePath))), null));
            }
        } else if (filePaths.size() > 0) {
            for (String filePath : filePaths) {
                forumCommentsToSend.add(getForumCommentToSend("", null, Uri.fromFile(new File(filePath))));
            }
        } else
            forumCommentsToSend.add(getForumCommentToSend(text, null, null));

        imagePaths.clear();
        filePaths.clear();


    }


    private ForumComment getForumCommentToSend(String comment, Uri selectedImageUri, Uri selectedFileUri) {
        String userName = CacheManager.getInstance().getCurrentUser().getFullName();
        String profileImageUrl = CacheManager.getInstance().getCurrentUser().getImageUrl();
        if (forumCommentToEdit != null) {
            forumComment = forumCommentToEdit;
            forumComment.setComment(comment);

            String fileURI = null;
            if (selectedImageUri != null)
                fileURI = selectedImageUri.toString();
            if (selectedFileUri != null)
                fileURI = selectedFileUri.toString();
            forumComment.setImageURI(fileURI);
            if (fileURI != null)
                forumComment.setFilePathLocal(FileUtils.getPathFromUri(getActivity(), Uri.parse(fileURI)));
            //        OpenTokHandler.getInstance().sendMessage(chatMessage);
            if (selectedImageUri != null) {
                forumComment.setFileType(Constants.FileType.FILE_TYPE_IMAGE);
            } else if (selectedFileUri != null) {
                forumComment.setFileType(Constants.FileType.FILE_TYPE_PDF);
                try {
                    forumComment.setFilePathLocal(FileUtils.getPathFromUri(getActivity(), selectedFileUri));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    forumComment.setFilePathLocal(selectedFileUri.getPath());
                }
            } else {
                forumComment.setFileType(Constants.FileType.FILE_TYPE_MESSAGE);
            }


            //forumComments.add(commentIndexToEdit, forumComment);

            //forumCommentsAdapter.setForumComments(forumComments);


        } else {
            forumComment = new ForumComment();
            forumComment.setName(userName);
            forumComment.setComment(comment);

            forumComment.setCommentId(forumQuestion.getQuestionId());
            forumComment.setCreatedDate(Long.toString(new Date().getTime()));
            forumComment.setProfilePicUrl(profileImageUrl);
            forumComment.setFileUrl(imageUploadPath);
            String fileURI = null;
            if (selectedImageUri != null)
                fileURI = selectedImageUri.toString();
            if (selectedFileUri != null)
                fileURI = selectedFileUri.toString();

            //        OpenTokHandler.getInstance().sendMessage(chatMessage);
            if (selectedImageUri != null) {
                forumComment.setFileType(Constants.FileType.FILE_TYPE_IMAGE);
                forumComment.setImageURI(fileURI);
                try {
                    forumComment.setFilePathLocal(FileUtils.getPathFromUri(getActivity(), Uri.parse(fileURI)));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    forumComment.setFilePathLocal(Uri.parse(fileURI).getPath());

                }
            } else if (selectedFileUri != null) {
                forumComment.setFileType(Constants.FileType.FILE_TYPE_PDF);
                forumComment.setImageURI(fileURI);
                try {
                    forumComment.setFilePathLocal(FileUtils.getPathFromUri(getActivity(), selectedFileUri));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    forumComment.setFilePathLocal(selectedFileUri.getPath());
                }
            } else {
                forumComment.setFileType(Constants.FileType.FILE_TYPE_MESSAGE);
            }
        }


        return forumComment;

    }


    @Override
    public void onImageSelected(Uri selectedImageUri) {
        this.selectedImageUri = selectedImageUri;
        FragmentSendImageDialog.newInstance(selectedImageUri.toString()).show(getChildFragmentManager());
        fragmentEditFieldWithButtons.getEtMessage().setText("");
    }

    @Override
    public void onFileSelected(Uri selectedFileUri) {
        this.selectedFileUri = selectedFileUri;
        addCommentToList("");
        selectedImageUri = null;
    }

    @Override
    public void onSendImageBackTapped() {
        setUriFieldsToNull();
    }

    private void setUriFieldsToNull() {
        selectedImageUri = null;
        selectedFileUri = null;
    }

    @Override
    public void onSendImageSendTapped(String text, String imageUri) {

        selectedImageUri = FileUtils.compressImageAndGetNewImageUri(getActivity(), selectedImageUri);
        if (selectedImageUri != null)
            addCommentToList(text);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (fragmentEditFieldWithButtons != null)
            fragmentEditFieldWithButtons.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.DOWNLOAD_FILE_WRITE_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    FileUtils.downloadFile(null, forumComments.get(idxToDownload), getActivity(), urlToDownload, FileUtils.getFileNameFromURL(urlToDownload));
                } else {
                    Toast.makeText(getActivity(), "Permission denied. Unable to download", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }


}