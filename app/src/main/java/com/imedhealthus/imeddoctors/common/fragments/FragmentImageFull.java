package com.imedhealthus.imeddoctors.common.fragments;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.jsibbold.zoomage.ZoomageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.BlurTransformation;

public class FragmentImageFull extends BaseFragment {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.image_view)
    ZoomageView imageView;
    @BindView(R.id.btn_download)
    Button btnDownload;

    ProgressDialog progressDialog;
    private Handler handler;

    @Override
    public String getName() {
        return FragmentImageFull.class.toString();
    }

    @Override
    public String getTitle() {
        return "Image";
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        ButterKnife.bind(this, view);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Loading");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        handler = new Handler();
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
        requestOptions.placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder);

        Glide.with(context).load(SharedData.getInstance().getSelectedImageURL()).apply(requestOptions.bitmapTransform(new BlurTransformation(25, 3)))
                .thumbnail(0.02f).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {


                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        Glide.with(context).load(SharedData.getInstance().getSelectedImageURL()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).thumbnail(0.2f).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                progressDialog.dismiss();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                progressDialog.dismiss();
                                return false;
                            }
                        }).into(imageView);
                    }
                };
                //holder.progressBar.setVisibility(View.GONE);

                if (dataSource.equals(DataSource.REMOTE)) {
                    handler.postDelayed(runnable, 1000);
                } else {
                    handler.post(runnable);

                }
                return false;
            }
        }).apply(requestOptions).into(imageView);
        ivBack.setVisibility(View.GONE);
        btnDownload.setVisibility(View.GONE);

        return view;
    }
}
