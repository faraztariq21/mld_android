package com.imedhealthus.imeddoctors.common.fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.Ringtone;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler;
import com.imedhealthus.imeddoctors.common.listeners.PopUpWindowListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.CallSignal;
import com.imedhealthus.imeddoctors.common.models.ForumComment;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.services.CallService;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.common.utils.TinyDB;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.internal.Util;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static com.imedhealthus.imeddoctors.common.utils.Constants.CODE_DRAW_OVER_OTHER_APP_PERMISSION;
import static com.imedhealthus.imeddoctors.common.utils.Constants.END_CALL_TIME;
import static com.imedhealthus.imeddoctors.common.utils.Constants.START_CALL_FAILURE;


public class CallFragment extends BaseFragment implements EasyPermissions.PermissionCallbacks,
        View.OnClickListener {


    Handler handler;

    PopUpWindowListener popUpWindowListener;

    @BindView(R.id.cont_subscriber)
    ViewGroup contSubscriber;
    @BindView(R.id.cont_publisher)
    ViewGroup contPublisher;


    @BindView(R.id.cont_audio)
    ViewGroup contAudio;

    @BindView(R.id.tv_status)
    TextView tvStatus;

    @BindView(R.id.tv_waiting)
    TextView tvWaiting;
    @BindView(R.id.btn_end_call)
    Button btnEndCall;

    Ringtone ringtone;

    @BindView(R.id.iv_switch_camera)
    ImageView iv_SwitchCam;
    @BindView(R.id.iv_video_recorder)
    ImageView ivCamOnOff;
    @BindView(R.id.iv_mic)
    ImageView ivMicOnOff;
    @BindView(R.id.iv_treatment_plan)
    ImageView ivTreatmentPlan;

    @BindView(R.id.iv_profile)
    CircleImageView ivProfile;
    @BindView(R.id.iv_patient_profile)
    ImageView ivPatientProfile;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_time)
    TextView tvTime;

    public static final int VIDEO_APP_PERM = 112;
    public static final int SETTINGS_SCREEN_PERM = 113;
    public static final int WRITE_SETTINGS_PERM = 561;


    private static final int REQUEST_MEDIA_PROJECTION = 931;

    public Dialog dialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PopUpWindowListener)
            popUpWindowListener = (PopUpWindowListener) context;
    }

    @Override
    public boolean showHeader() {
        return true;
    }

    @Override
    public String getName() {
        return "CallFragment";
    }

    @Override
    public String getTitle() {
        return "Tele Appointment";
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_call, container, false);
        ButterKnife.bind(this, view);

        if (SharedData.getInstance().getSelectedCallSignal() != null)
            OpenTokHandler.getInstance().sendCallSignal(SharedData.getInstance().getSelectedCallSignal());
        else if (SharedData.getInstance().getCallData() != null)
            OpenTokHandler.getInstance().sendCallSignal(SharedData.getInstance().getCallData());
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        handler = new Handler();
        // register the broadcasts
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadCastReceiver,
                new IntentFilter(Constants.BROADCAST_EVENT));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(timeBroadCastReceiver, new IntentFilter(Constants.BROADCAST_EVENT_TIME));


     /*   getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);*/


        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(getActivity())))
            SharedData.getInstance().setCanDrawOverOtherApps(true);


        if (SharedData.getInstance().getSelectedCallSignal().getCallType() != null
                && SharedData.getInstance().getSelectedCallSignal().getCallType().equals("audio")) {
            setViewForAudioCall();
            setViewForAudioCall();
        } else {
            setViewForVideoCall();
        }
        if (!CallService.isServiceRunning) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions();
            }
        } else {
            if (CallService.IS_AUDIO_CALL) {
                setViewForAudioCall();
                updateViewOnSessionConnected();

            } else {
                setViewForVideoCall();
                updateViewOnSessionConnected();
                updateViewOnStreamReceived();
            }
        }
    }


    private void setViewForVideoCall() {
        contAudio.setVisibility(View.VISIBLE);
        updateReceiverName();
        ivProfile.setVisibility(View.GONE);
        tvName.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        tvTime.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        // updateViewOnSessionConnected();
    }

    private void startCallService() {
        Intent intent = new Intent(getActivity(), CallService.class);
        if (!CallService.isServiceRunning) {
            intent.setAction(CallService.ACTION_START_FOREGROUND_SERVICE);
            getActivity().startService(intent);
        }
    }

    private void setViewForAudioCall() {
        tvStatus.setTextColor(Color.parseColor("#000000"));
        contAudio.setVisibility(View.VISIBLE);
        contSubscriber.setVisibility(View.GONE);
        contPublisher.setVisibility(View.GONE);
        ivCamOnOff.setVisibility(View.GONE);

        ivMicOnOff.setImageResource(R.drawable.microphone);
        ivMicOnOff.setColorFilter(R.color.icon_green);

        updateReceiverName();
    }

    private void updateReceiverName() {
        Glide.with(ApplicationManager.getContext()).load(SharedData.getInstance().
                getReceiverImageUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(ivProfile);
        tvName.setText(SharedData.getInstance().getReceiverName());
        contAudio.bringToFront();

    }


    @Override
    public void onPause() {
        super.onPause();

       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(getActivity())) {

        } else {
            if (SharedData.getInstance().isCallActive())
                startCallService();
        }*/

        if (SharedData.getInstance().isCallActive()) {
            Intent intent = new Intent(Constants.BROADCAST_EVENT);
            intent.putExtra(Constants.TAG, Constants.OPEN_POP_UP_WINDOW);
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);

        }
    }

    @SuppressLint("InvalidWakeLockTag")
    @Override
    public void onResume() {
        super.onResume();

        Intent broadcastIntent = new Intent(Constants.BROADCAST_EVENT);
        broadcastIntent.putExtra(Constants.TAG, Constants.CLOSE_POP_UP_WINDOW);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(broadcastIntent);

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(getActivity())) {
            startDrawOverOtherAppsActivity();
        }
*/


        if (CallService.IS_CALL_CONNECTED) {
            updateViewOnStreamReceived();
            if (CallService.getPublisher() != null) {
                if (CallService.getPublisher().getPublishAudio())
                    changeMicIcon(true);
                else
                    changeMicIcon(false);

                if (CallService.getPublisher().getPublishVideo())
                    changeVideoIcon(true);
                else
                    changeVideoIcon(false);
            }
        } else
            updateViewOnSessionConnected();
/*
        try {
            getActivity().stopService(new Intent(getActivity(), CallService.class));
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        /* This status together with the one in onDestroy()
         * will make the screen be always on until this Activity gets destroyed. */

    }

    private void startDrawOverOtherAppsActivity() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + getActivity().getPackageName()));
        getActivity().startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_end_call:
                if (CallService.isServiceRunning) {
                    Intent intent = new Intent(Constants.BROADCAST_EVENT);
                    intent.putExtra(Constants.TAG, Constants.END_CALL);
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);

                    endCall();
                }
                break;

            case R.id.iv_treatment_plan: {
                CallSignal callSignal = SharedData.getInstance().getSelectedCallSignal();
                String receiverProfileId = callSignal.getReceiverProfileId().equals(CacheManager.getInstance().getCurrentUser().getUserId()) ? callSignal.getProfileId() : callSignal.getReceiverProfileId();
                String receiverName = callSignal.getReceiverProfileId().equals(CacheManager.getInstance().getCurrentUser().getUserId()) ? callSignal.getMsg() : CacheManager.getInstance().getCurrentUser().getFullName();
                SharedData.getInstance().setSelectedPatientId(receiverProfileId);
                SharedData.getInstance().setSelectedPatientName(receiverName);
                Intent intent = new Intent(context, SimpleFragmentActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.TreatmentPlan.ordinal());
                ((BaseActivity) context).startActivity(intent, true);
            }
            break;
            case R.id.iv_patient_profile: {

                SharedData.getInstance().setEditPatientProfile(true);
                CallSignal callSignal = SharedData.getInstance().getSelectedCallSignal();
                String receiverProfileId = callSignal.getReceiverProfileId().equals(CacheManager.getInstance().getCurrentUser().getUserId()) ? callSignal.getProfileId() : callSignal.getReceiverProfileId();
                SharedData.getInstance().setSelectedPatientId(receiverProfileId);
                Intent intent = new Intent(context, SimpleFragmentActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.PatientProfile.ordinal());
                ((BaseActivity) context).startActivity(intent, true);

            }
            break;

            case R.id.iv_mic:
                turnAudioOnOff();
                break;
            case R.id.iv_video_recorder:
                turnCamOnOff();
                break;
            case R.id.iv_switch_camera:
                toggleCamera();
                break;

        }
    }


    private void endCall() {
        SharedData.getInstance().setCallActive(false);
        SharedData.getInstance().setCallResumed(false);
        Constants.callStartTime = 0;
        tvTime.setVisibility(View.GONE);
        ((BaseActivity) context).finish();


        tvStatus.setText("Call Ended");

        /*if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Patient)
            openSubmitFeedback();*/
    }


    private void openSubmitFeedback() {


        try {
            Intent intent = new Intent(getContext(), SimpleFragmentActivity.class);
            intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.SubmitDoctorFeedback.ordinal());

            try {
                if (context != null)
                    ((BaseActivity) context).startActivity(intent, true);
            } catch (Exception e) {

            }

        } catch (Exception e) {

        }


    }

    public void toggleCamera() {
        if (CallService.getPublisher() == null)
            return;
        CallService.getPublisher().cycleCamera();
    }

    public void turnCamOnOff() {
        if (CallService.getPublisher() == null)
            return;
        if (CallService.getPublisher().getPublishVideo()) {
            CallService.getPublisher().setPublishVideo(false);
            changeVideoIcon(false);
        } else {
            CallService.getPublisher().setPublishVideo(true);
            changeVideoIcon(true);
        }

    }

    public void changeVideoIcon(boolean isVideoOn) {
        if (isVideoOn)
            ivCamOnOff.setImageResource(R.drawable.movie_recorder);
        else
            ivCamOnOff.setImageResource(R.drawable.ic_video_off);

    }


    public void turnAudioOnOff() {
        if (CallService.getPublisher() == null)
            return;
        if (CallService.getPublisher().getPublishAudio()) {

            CallService.getPublisher().setPublishAudio(false);
            changeMicIcon(false);
        } else {
            CallService.getPublisher().setPublishAudio(true);
            changeMicIcon(true);
        }


    }

    public void changeMicIcon(boolean isAudioOn) {
        if (isAudioOn)
            ivMicOnOff.setImageResource(R.drawable.microphone);
        else
            ivMicOnOff.setImageResource(R.drawable.ic_mic_off);
    }


    //Easy permissions
    @RequiresApi(api = Build.VERSION_CODES.M)
    @AfterPermissionGranted(VIDEO_APP_PERM)
    private void requestPermissions() {
        String[] permissions = {Manifest.permission.INTERNET, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};
        if (EasyPermissions.hasPermissions(context, permissions)) {
            if (Settings.System.canWrite(context)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(getActivity())) {
                    startCallService();
                } else {
                    startDrawOverOtherAppsActivity();
                }


            } else {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getActivity().getPackageName()));
                startActivityForResult(intent, WRITE_SETTINGS_PERM);
            }
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.video_perm_msg), VIDEO_APP_PERM, permissions);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        Log.d("iMed", "onPermissionsGranted:" + requestCode + ":" + perms.size());
        if (requestCode == VIDEO_APP_PERM) {
            if (Settings.System.canWrite(context)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(getActivity()))
                    SharedData.getInstance().setCanDrawOverOtherApps(true);
                else
                    SharedData.getInstance().setCanDrawOverOtherApps(false);
                startCallService();


                //startCallService();
            } else {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getActivity().getPackageName()));
                startActivityForResult(intent, WRITE_SETTINGS_PERM);
            }
        } else if (requestCode == WRITE_SETTINGS_PERM) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(getActivity()))
                SharedData.getInstance().setCanDrawOverOtherApps(true);
            else
                SharedData.getInstance().setCanDrawOverOtherApps(false);
            startCallService();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

        Log.d("iMed", "onPermissionsDenied:" + requestCode + ":" + perms.size());

        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this)
                    .setTitle(getString(R.string.title_settings_dialog))
                    .setRationale(getString(R.string.rationale_ask_again))
                    .setPositiveButton(getString(R.string.settings))
                    .setNegativeButton(getString(R.string.cancel))
                    .setRequestCode(SETTINGS_SCREEN_PERM)
                    .build()
                    .show();
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE_DRAW_OVER_OTHER_APP_PERMISSION) {

            if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(getActivity())))
                SharedData.getInstance().setCanDrawOverOtherApps(true);
            else
                SharedData.getInstance().setCanDrawOverOtherApps(false);

            startCallService();

            return;
        }
        if (context != null) {


            if (Settings.System.canWrite(context)) {

                startDrawOverOtherAppsActivity();

            } else {
                ((BaseActivity) context).finish();
            }
        }
    }

    int dots = 0;
    Runnable continueTextRunnable = new Runnable() {
        @Override
        public void run() {
            if (dots >= 4) {
                dots = 0;
            }
            dots++;
            StringBuffer connectingString = new StringBuffer();
            for (int i = 0; i < dots; i++) {
                connectingString.append(". ");
            }

            tvStatus.setText("Connecting " + connectingString.toString());
            handler.postDelayed(continueTextRunnable, 800);
        }
    };


    public void updateViewOnSessionConnected() {


        //screenSharingCapturer = new ScreenSharingCapturer(getActivity(), getView());
        if (CallService.getPublisher() != null) {
            if (CallService.getPublisher().getView().getParent() != null)
                ((ViewGroup) CallService.getPublisher().getView().getParent()).removeView(CallService.getPublisher().getView());

            contPublisher.addView(CallService.getPublisher().getView());
            if (CallService.getPublisher().getView() instanceof GLSurfaceView) {
                ((GLSurfaceView) CallService.getPublisher().getView()).setZOrderOnTop(true);
            }
        }

        handler.post(continueTextRunnable);
        tvWaiting.setVisibility(View.VISIBLE);
        btnEndCall.setVisibility(View.VISIBLE);
        btnEndCall.setOnClickListener(this);
        ivPatientProfile.setOnClickListener(this);
        ivTreatmentPlan.setOnClickListener(this);

    }


    public void updateViewOnStreamReceived() {
        handler.removeCallbacks(continueTextRunnable);
        tvStatus.setText("");
        if (CallService.getSubscriber() != null) {
            if (CallService.getSubscriber().getView().getParent() != null)
                ((ViewGroup) CallService.getSubscriber().getView().getParent()).removeView(CallService.getSubscriber().getView());

            contSubscriber.addView(CallService.getSubscriber().getView());
            tvWaiting.setVisibility(View.GONE);
        }


        if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor) {
            // Not showing views currently due to some navigation problems
            ivPatientProfile.setVisibility(View.VISIBLE);
            ivTreatmentPlan.setVisibility(View.VISIBLE);
        } else {
            ivPatientProfile.setVisibility(View.GONE);
            ivTreatmentPlan.setVisibility(View.GONE);
        }

    }

    public void updateViewOnStreamDropped() {
        if (CallService.getSubscriber() != null) {
            CallService.setSubscriber(null);
            contSubscriber.removeAllViews();
        }
        tvStatus.setText("Reconnecting");

        tvWaiting.setVisibility(View.VISIBLE);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadCastReceiver);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(timeBroadCastReceiver);
    }


    public void showAlertForBack(final Context context, String title, String message) {


        if (context == null)
            return;


        dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.error_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.dialog_msg);
        text.setText(message);
        TextView tv_title = (TextView) dialog.findViewById(R.id.dialog_title);
        tv_title.setText(title);


        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ((Activity) context).finish();
            }
        });

        dialog.show();
    }

    @Override
    public void onCommentReceived(ForumComment forumComment) {

    }

    public BroadcastReceiver broadCastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String extra = intent.getStringExtra(Constants.TAG);
            if (extra.equals(Constants.SET_UP_VIEW_FOR_AUDIO_CALL)) {
                setViewForAudioCall();
                updateViewOnSessionConnected();
            } else if (extra.equals(Constants.SET_UP_VIEW_FOR_VIDEO_CALL)) {
                setViewForVideoCall();
                updateViewOnSessionConnected();
            } else if (extra.equals(Constants.STREAM_RECEIVED))
                updateViewOnStreamReceived();
            else if (extra.equals(Constants.STREAM_DROPPED))
                updateViewOnStreamDropped();
            else if (extra.equals(Constants.SESSION_ERROR))
                updateViewOnSessionError();
            else if (extra.equals(Constants.CALL_REJECTED))
                updateViewOnCallRejected();
            else if (extra.equals(Constants.CALL_ENDED))
                updateViewOnCallEnded();
            else if (extra.equals(Constants.START_CALL))
                AlertUtils.showProgress(getActivity());
            else if (extra.equals(START_CALL_FAILURE)) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            } else if (extra.equals(Constants.CALL_DROPPED)) {
                showAlertForBack(getContext(), "", "Call Dropped");
            }
        }
    };

    private void updateViewOnCallEnded() {
        SharedData.getInstance().setCallActive(false);
        SharedData.getInstance().setCallResumed(false);
        Constants.callStartTime = 0;
        tvTime.setVisibility(View.GONE);
        if (context != null)
            ((BaseActivity) context).finish();

        new TinyDB(getContext()).putString(SharedData.getInstance().getSelectedDoctorId()+"_"+SharedData.getInstance().getSelectedPatientId()+Constants.END_CALL_TIME,GenericUtils.getCurrentTimeInString());

        tvStatus.setText("Call Ended");

        if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Patient)
            openSubmitFeedback();
    }

    private void updateViewOnCallRejected() {
        SharedData.getInstance().setCallActive(false);
        SharedData.getInstance().setCallResumed(false);

        //((BaseActivity) context).onBackPressed();
        showAlertForBack(context, "", "Call has been rejected");


    }

    private void updateViewOnSessionError() {
        SharedData.getInstance().setCallConnected(false);
        SharedData.getInstance().setCallActive(false);
        SharedData.getInstance().setCallResumed(false);

        AlertUtils.dismissProgress();
        AlertUtils.showAlertForBack(context, Constants.ErrorAlertTitle, "Can't connect now. Kindly try later.");

    }

    public BroadcastReceiver timeBroadCastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long seconds = intent.getLongExtra(Constants.TIME, 0);
            tvTime.setText(GenericUtils.getTimeInStringFormat(seconds));

        }
    };
}
