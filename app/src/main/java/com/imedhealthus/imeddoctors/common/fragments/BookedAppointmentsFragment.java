package com.imedhealthus.imeddoctors.common.fragments;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.CallActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.adapters.BookedAppointmentsListAdapter;
import com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler;
import com.imedhealthus.imeddoctors.common.listeners.OnBookedAppointmentItemClickListener;
import com.imedhealthus.imeddoctors.common.models.BookedAppointment;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.CallSignal;
import com.imedhealthus.imeddoctors.common.models.ChatItem;
import com.imedhealthus.imeddoctors.common.models.ChatUserModel;
import com.imedhealthus.imeddoctors.common.models.ChatUsers;
import com.imedhealthus.imeddoctors.common.models.LiveNotifications;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.User;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.Logs;
import com.imedhealthus.imeddoctors.doctor.fragments.DoctorProfileFragment;
import com.imedhealthus.imeddoctors.doctor.fragments.FragmentBookedAppointmentDetail;
import com.imedhealthus.imeddoctors.doctor.models.Appointment;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookedAppointmentsFragment extends BaseFragment implements OnBookedAppointmentItemClickListener, CompactCalendarView.CompactCalendarViewListener, View.OnClickListener {

    @BindView(R.id.tv_calendar_date)
    TextView tvCalendarDate;
    @BindView(R.id.tv_calendar_month)
    TextView tvCalendarMonth;
    @BindView(R.id.tv_plans_count)
    TextView tvPlansCount;

    @BindView(R.id.calendar)
    CompactCalendarView calendar;

    @BindView(R.id.rv_appointments)
    RecyclerView rvAppointments;
    @BindView(R.id.tv_no_records)
    TextView tvNoRecords;
    @BindView(R.id.tv_create_appointment)
    TextView tvCreateAppointment;

    @BindView(R.id.ll_next_appointment)
    LinearLayout llNextAppointment;
    @BindView(R.id.tv_next_apt_date)
    TextView tvNextAppointmentDate;
    List<BookedAppointment> filteredAppointments;
    private List<BookedAppointment> bookedAppointments;
    private Date currentDate;
    private BookedAppointmentsListAdapter adapterAppointments;
    private SimpleDateFormat dateFormat;

    @Override
    public String getName() {
        return "BookedAppointmentsFragment";
    }

    @Override
    public String getTitle() {
        return "Appointments";
    }

    @Override
    public boolean showHeader() {
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_schedule, container, false);
        ButterKnife.bind(this, view);
        initializeShimmer(view);
        initHelper();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    public void initHelper() {

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvAppointments.setLayoutManager(mLayoutManager);

        adapterAppointments = new BookedAppointmentsListAdapter(tvNoRecords, this);
        rvAppointments.setAdapter(adapterAppointments);

        bookedAppointments = new ArrayList<>();
        filteredAppointments = new ArrayList<>();

        dateFormat = new SimpleDateFormat("MMMM");
        currentDate = new Date();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setListener(this);

        tvCreateAppointment.setOnClickListener(this);


        filterAppointments(currentDate);

    }

    private void setData(List<BookedAppointment> bookedAppointment) {

        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d");
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");

        for (BookedAppointment appointment : bookedAppointment) {
            appointment.adjustData(dateFormat, timeFormat, currentDate);
        }
        this.bookedAppointments = bookedAppointment;

        calendar.refreshDrawableState();


        filterAppointments(currentDate);

    }

    //Filtering
    private void filterAppointments(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int filterDay = calendar.get(Calendar.DATE);
        int filterYear = calendar.get(Calendar.YEAR);
        int filterMonth = calendar.get(Calendar.MONTH);

        this.calendar.setCurrentDate(date);


        filteredAppointments = new ArrayList<>();

        for (BookedAppointment appointment : bookedAppointments) {
            if (appointment.isSameDay(filterYear, filterMonth, filterDay)) {
                filteredAppointments.add(appointment);
            }

        }


        adapterAppointments.setAppointments(filteredAppointments);


        String plans;
        if (adapterAppointments.getItemCount() == 0) {
            plans = "No plans";
            if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor) {
                tvCreateAppointment.setVisibility(View.VISIBLE);
            }
        } else if (adapterAppointments.getItemCount() == 1) {
            plans = "1 plan";
            tvCreateAppointment.setVisibility(View.GONE);
        } else {
            plans = adapterAppointments.getItemCount() + " plans";
            tvCreateAppointment.setVisibility(View.GONE);
        }
        tvPlansCount.setText(plans);

        tvCalendarDate.setText(String.valueOf(date.getDate()));
        tvCalendarMonth.setText(dateFormat.format(date));

        currentDate = date;

        if (filteredAppointments.size() == 0) {

            tvNoRecords.setVisibility(View.VISIBLE);

            final Appointment appointment = getNextAppointment(date, bookedAppointments);
            if (appointment != null) {
                llNextAppointment.setVisibility(View.VISIBLE);
                tvNextAppointmentDate.setText(appointment.getFormattedDate());
                tvNextAppointmentDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        filterAppointments(appointment.getStartDate());
                    }
                });
            } else {
                llNextAppointment.setVisibility(View.GONE);
            }

        } else {
            llNextAppointment.setVisibility(View.GONE);
            tvNoRecords.setVisibility(View.GONE);

        }

    }


    private Appointment getNextAppointment(Date currentDate, List<BookedAppointment> appointments) {


        ArrayList<Appointment> appointmentsAfterGivenDate = new ArrayList<>();

        for (int i = 0; i < appointments.size(); i++) {
            if (appointments.get(i).getStartDate().compareTo(currentDate) > 0) {
                appointmentsAfterGivenDate.add(appointments.get(i));
            }
        }


        Collections.sort(appointmentsAfterGivenDate, new Comparator<Appointment>() {
            @Override
            public int compare(Appointment o1, Appointment o2) {
                return o1.getStartDate().compareTo(o2.getStartDate());
            }
        });

        if (appointmentsAfterGivenDate.size() > 0)
            return appointmentsAfterGivenDate.get(0);


        return null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_create_appointment:
                SharedData.getInstance().setSelectedDoctorProfileTab(DoctorProfileFragment.DoctorProfileTabs.APPOINTMENTS_TAB);
                SimpleFragmentActivity.startActivity(getActivity(), Constants.Fragment.DoctorProfile, null);
                break;
        }
    }

    //Calendar actions
    @Override
    public void onDayClick(Date dateClicked) {

        filterAppointments(dateClicked);
    }

    @Override
    public void onMonthScroll(Date firstDayOfNewMonth) {
        filterAppointments(firstDayOfNewMonth);
    }


    //Appointment item click
    @Override
    public void onBookedAppointmentItemDetailClick(int idx) {
        adapterAppointments.toggleExpandAt(idx);
    }

    @Override
    public void onBookedAppointmentItemCompleteClick(int idx) {

        BookedAppointment appointment = adapterAppointments.getItem(idx);
        if (appointment == null) {
            return;
        }
        showPostponeAlert(appointment);

    }

    @Override
    public void onBookedAppointmentItemCancelClick(int idx) {

        BookedAppointment appointment = adapterAppointments.getItem(idx);
        if (appointment == null) {
            return;
        }
        showCancelAlert(appointment);

    }

    @Override
    public void onBookedAppointmentItemCallClick(int idx) {
        BookedAppointment appointment = adapterAppointments.getItem(idx);
        if (appointment == null) {
            return;
        }

        if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor) {
            startCallDoctor(appointment, false);
        } else {
            startCallPatient(appointment, false);
        }
    }

    @Override
    public void onBookedAppointmentItemVideoCallClick(int idx) {
        BookedAppointment appointment = adapterAppointments.getItem(idx);
        if (appointment == null) {
            return;
        }

        if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor) {
            startCallDoctor(appointment, true);
        } else {
            startCallPatient(appointment, true);
        }
    }

    @Override
    public void onBookedAppointmentItemChatClick(int idx) {
        BookedAppointment appointment = adapterAppointments.getItem(idx);
        SharedData.getInstance().setSelectedBookedApointment(appointment);
        if (appointment == null) {
            return;
        }


        ArrayList<ChatUserModel> chatUsers = new ArrayList<>();
        chatUsers.add(new ChatUserModel(appointment.getPatientLoginId(), "patient"));
        chatUsers.add(new ChatUserModel(appointment.getDoctorLoginId(), "doctor"));
        ChatUsers users = new ChatUsers(chatUsers);

        startNewOrOpenChat(users);
    }

    @Override
    public void onBookedAppointmentItemDropDownClick(int idx) {

        FragmentBookedAppointmentDetail.newInstance(filteredAppointments.get(idx)).show(getActivity().getSupportFragmentManager());
    }

    @Override
    public void onBookedAppointmentItemPatientProfileClick(int idx) {
        SharedData.getInstance().setSelectedPatientId(filteredAppointments.get(idx).getPatientId());
        SimpleFragmentActivity.startActivity(getActivity(), Constants.Fragment.PatientProfile, null);
    }

    @Override
    public void onBookedAppointmentItemDoctorProfileClick(int idx) {
        SharedData.getInstance().setSelectedDoctorId(filteredAppointments.get(idx).getDoctorId());
        SimpleFragmentActivity.startActivity(getActivity(), Constants.Fragment.DoctorProfile, null);

    }

    private void startCallDoctor(final BookedAppointment bookedAppointment, final boolean isVideo) {

        AlertUtils.showProgress(getActivity());

        String userLoginId = bookedAppointment.getDoctorLoginId();
        String partnerLoginId = bookedAppointment.getPatientLoginId();
        String myName = CacheManager.getInstance().getCurrentUser().getFullName();

        WebServicesHandler.instance.getDataForCallWithoutNotification(userLoginId, partnerLoginId, myName, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                String sessionId = response.optString("SessionId", "");
                String token = response.optString("TokenId", "");
                boolean isUserLive = response.optBoolean("IsUserLive", false);
                String chatId = response.optString("ChatId", "");
                String receiverProfileId = response.optString("PatientId", "");
                String senderName = CacheManager.getInstance().getCurrentUser().getFullName();

                if (!isUserLive) {
                    //  AlertUtils.showAlert(context, Constants.ErrorAlertTitle, bookedAppointment.getPatientFullName() + " is not online right now. We have notified him. Kindly try again in a few minutes.");

                    // CallData data = new CallData(bookedAppointment.getPatientLoginId(), senderName, "", "","");
                    // OpenTokHandler.getInstance().sendPushNotification(data);

                    // return;
                }

                SharedData.getInstance().setOpenTokCallSessionId(sessionId);
                SharedData.getInstance().setOpenTokCallToken(token);
                SharedData.getInstance().setReceiverId(bookedAppointment.getPatientId());
                SharedData.getInstance().setReceiverName(bookedAppointment.getPatientFullName());
                SharedData.getInstance().setReceiverImageUrl(bookedAppointment.getPatientProfile());
                // CallData data = new CallData(bookedAppointment.getPatientLoginId(), senderName, sessionId, token,bookedAppointment.getDoctorId());
                //OpenTokHandler.getInstance().sendCallSignal(data);
                CallSignal callSignal = new CallSignal(bookedAppointment.getPatientLoginId(), receiverProfileId, senderName, isVideo ? "video" : "audio", "", chatId, CacheManager.getInstance().getCurrentUser().getLoginId(), CacheManager.getInstance().getCurrentUser().getUserId());
                SharedData.getInstance().setSelectedCallSignal(callSignal);
                //OpenTokHandler.getInstance().sendCallSignal(callSignal);

                Intent intent = new Intent(context, CallActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CallFragment.ordinal());
                ((BaseActivity) context).startActivity(intent, true);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

    private void startCallPatient(final BookedAppointment bookedAppointment, final boolean isVideo) {

        AlertUtils.showProgress(getActivity());

        String userLoginId = bookedAppointment.getPatientLoginId();
        String partnerLoginId = bookedAppointment.getDoctorLoginId();
        String myName = CacheManager.getInstance().getCurrentUser().getFullName();

        WebServicesHandler.instance.getDataForCallWithoutNotification(userLoginId, partnerLoginId, myName, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                String sessionId = response.optString("SessionId", "");
                String token = response.optString("TokenId", "");
                boolean isUserLive = response.optBoolean("IsUserLive", false);
                String chatId = response.optString("ChatId", "");
                String receiverProfileId = response.optString("DoctorId", "");

                String senderName = CacheManager.getInstance().getCurrentUser().getFullName();

                if (!isUserLive) {
                    // AlertUtils.showAlert(context, Constants.ErrorAlertTitle, bookedAppointment.getDoctorFullName() + " is not online right now. We have notified him. Kindly try again in a few minutes.");

                    // CallData data = new CallData(bookedAppointment.getPatientLoginId(), senderName, "", "","");
                    // OpenTokHandler.getInstance().sendPushNotification(data);

                    //return;
                }

                SharedData.getInstance().setOpenTokCallSessionId(sessionId);
                SharedData.getInstance().setOpenTokCallToken(token);
                SharedData.getInstance().setReceiverId(bookedAppointment.getDoctorId());
                SharedData.getInstance().setReceiverName(bookedAppointment.getDoctorFullName());
                SharedData.getInstance().setReceiverImageUrl(bookedAppointment.getDoctorProfile());
                // CallData data = new CallData(bookedAppointment.getDoctorLoginId(), senderName, sessionId, token,bookedAppointment.getDoctorId());
                //OpenTokHandler.getInstance().sendCallSignal(data);


                CallSignal callSignal = new CallSignal(bookedAppointment.getDoctorLoginId(), receiverProfileId, senderName, isVideo ? "video" : "audio", "", chatId, CacheManager.getInstance().getCurrentUser().getLoginId(), CacheManager.getInstance().getCurrentUser().getUserId());
                SharedData.getInstance().setSelectedCallSignal(callSignal);
                //OpenTokHandler.getInstance().sendCallSignal(callSignal);

                Intent intent = new Intent(context, CallActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CallFragment.ordinal());
                ((BaseActivity) context).startActivity(intent, true);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }

        });

    }

    //Other
    private void showPostponeAlert(final BookedAppointment appointment) {

        new AlertDialog.Builder(context)
                .setTitle("Confirm")
                .setMessage("Are you sure you want to Complete this appointment?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        completeAppointment(appointment);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        dialog.dismiss();

                    }
                })
                .show();

    }

    private void showCancelAlert(final BookedAppointment appointment) {

        new AlertDialog.Builder(context)
                .setTitle("Confirm")
                .setMessage("Are you sure you want to cancel this appointment?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        askForAppointmentCancelationOption(appointment);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        dialog.dismiss();

                    }
                })
                .show();

    }


    private void startNewOrOpenChat(ChatUsers users) {

        AlertUtils.showProgress(getActivity());

        WebServicesHandler.instance.startChat(users, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                if (!response.status()) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                    return;
                }

                JSONObject chatJson = response.optJSONObject("Chats");
                if (chatJson == null) {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
                    return;
                }

                ChatItem chatItem = new ChatItem(chatJson);
                SharedData.getInstance().setSelectedChatItem(chatItem);

                Intent intent = new Intent(context, SimpleFragmentActivity.class);
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.ChatFragment.ordinal());
                ((BaseActivity) context).startActivity(intent, true);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }


    public void askForAppointmentCancelationOption(final BookedAppointment bookedAppointment) {
        final Dialog alertDialog = new Dialog(getActivity());
        alertDialog.setContentView(R.layout.dialog_feedback);
        alertDialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_dialog);
        final EditText editText = (EditText) alertDialog.findViewById(R.id.et_reason);

        Button patientCancel = (Button) alertDialog.findViewById(R.id.dialog_btn_submit);
        Button slotCancel = (Button) alertDialog.findViewById(R.id.dialog_btn_cancel_slot);


        patientCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                String reason = editText.getText().toString();
                alertDialog.dismiss();
                if (TextUtils.isEmpty(reason.trim())) {
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, "Reason is required");
                    return;
                }

                cancelAppointment(bookedAppointment, "patient", reason);

            }
        });

        slotCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                String reason = editText.getText().toString();
                alertDialog.dismiss();
                if (TextUtils.isEmpty(reason.trim())) {
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, "Reason is required");
                    return;
                }

                cancelAppointment(bookedAppointment, "slot", reason);

            }
        });
        // Write your status here to invoke YES event
        alertDialog.show();
    }


    //Webservices
    private void loadData() {

        startShimmerAnimation();
        // AlertUtils.showProgress(getActivity());
        User user = CacheManager.getInstance().getCurrentUser();
        String userId = user.getUserId();
        String userType = "patient";
        if (user.getUserType() == Constants.UserType.Patient)
            userType = "patient";
        else
            userType = "doctor";

        WebServicesHandler.instance.getSchedule(userId, userType, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                //AlertUtils.dismissProgress();
                stopShimmerAnimation();
                Logs.apiResponse(response.toString());
                if (response.status()) {
                    bookedAppointments = BookedAppointment.getBookedAppointments(response);

                    filteredAppointments = new ArrayList<>(bookedAppointments);
                    for (BookedAppointment appointment : bookedAppointments) {
                        @SuppressLint("ResourceType") Event ev1 = new Event(Color.parseColor(getContext().getResources().getString(R.color.dark_blue)), appointment.getStartTimeStamp(), "");
                        calendar.addEvent(ev1);
                    }
                    calendar.setCurrentDate(currentDate);

                    updateNotificationStatus();
                } else {
                    AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, response.message());
                }
                setData(bookedAppointments);
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                stopShimmerAnimation();
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(getContext(), Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });


    }

    private void completeAppointment(final BookedAppointment appointment) {

        AlertUtils.showProgress(getActivity());
        WebServicesHandler.instance.completeAppointment(appointment.getId(), new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                AlertUtils.dismissProgress();
                if (!response.status()) {
                    AlertUtils.showAlert(context, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
                    return;
                }
                bookedAppointments.remove(appointment);
                AlertUtils.showAlert(context, Constants.SuccessAlertTitle, response.message());
                SharedData.getInstance().setRefreshRequired(true);
                filterAppointments(currentDate);
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(context, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            }
        });

    }

    private void cancelAppointment(final BookedAppointment appointment, String cancelType, String reason) {

        AlertUtils.showProgress(getActivity());
        WebServicesHandler.instance.cancelBookedAppointment(appointment.getId(), appointment.getId(),
                reason, cancelType, new CustomCallback<RetrofitJSONResponse>() {
                    @Override
                    public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                        AlertUtils.dismissProgress();
                        if (!response.status()) {
                            AlertUtils.showAlert(context, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
                            return;
                        }

                        LiveNotifications liveNotifications = new LiveNotifications();
                        liveNotifications.setProfileId(appointment.getPatientId());
                        liveNotifications.setProfileType("patient");
                        liveNotifications.setMessage(appointment.getDoctorFullName() + " canceled your appointment");
                        OpenTokHandler.getInstance().sendLiveNotification(liveNotifications);

                        bookedAppointments.remove(appointment);
                        AlertUtils.showAlert(context, Constants.SuccessAlertTitle, response.message());
                        SharedData.getInstance().setRefreshRequired(true);
                        filterAppointments(currentDate);


                    }

                    @Override
                    public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                        AlertUtils.dismissProgress();
                        AlertUtils.showAlert(context, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
                    }
                });

    }

    public void updateNotificationStatus() {
        String userId = CacheManager.getInstance().getCurrentUser().getUserId();
        String userType = CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Doctor ? "doctor" : "patient";
        String notificationType = "Appointment";
        boolean isPublic = false;
        WebServicesHandler.instance.updateNotificationStatus(userId, userType, notificationType, false, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                AlertUtils.dismissProgress();
                if (response.status()) {

                } else {

                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

            }
        });
    }


}
