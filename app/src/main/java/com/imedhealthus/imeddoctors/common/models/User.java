package com.imedhealthus.imeddoctors.common.models;

import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 1/2/18.
 */

public class User {

    private String userId, imageUrl, firstName, lastName, email, phone, location,loginId;
    private Constants.UserType userType;
    private List<PaymentMethod> paymentMethods;
    private boolean isPhoneVerified;

    public User() {
    }

    public User(String userId, String imageUrl, String firstName, String lastName, String email, String phone, String location, Constants.UserType userType, boolean isPhoneVerified, List<PaymentMethod> paymentMethods) {
        this.userId = userId;
        this.imageUrl = imageUrl;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.location = location;
        this.userType = userType;
        this.isPhoneVerified = isPhoneVerified;
        this.paymentMethods = paymentMethods;
    }

    public User(JSONObject json) {

        try {

        loginId = json.getString("LoginId");
        JSONObject detailsJson = json.optJSONObject("User");
        userId = detailsJson.optString("UserId", "");
        firstName = detailsJson.optString("FirstName", "").replace("null","N/A");
        lastName = detailsJson.optString("LastName", "").replace("null","N/A");
        email = detailsJson.optString("Email", "").replace("null","N/A");
        if(detailsJson.optString("UserType","").equalsIgnoreCase("patient"))
           userType = Constants.UserType.Patient;
        else
            userType = Constants.UserType.Doctor;

        imageUrl= GenericUtils.getImageUrl(detailsJson.optString("ImageUrl",""));
        isPhoneVerified = detailsJson.optBoolean("isPhoneVerified");
        paymentMethods=new ArrayList<>();
        JSONArray paymentMethodsJSON = detailsJson.getJSONArray("PaymentMethods");


        for(int i=0;i<paymentMethodsJSON.length();i++){
            PaymentMethod paymentMethod=new PaymentMethod(paymentMethodsJSON.getJSONObject(i));
            paymentMethods.add(paymentMethod);
        }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setUserType(Constants.UserType userType) {
        this.userType = userType;
    }

    public String getFullName() {
        return (firstName + " " + lastName).replace("null","");
    }

    public String getUserId() {
        return userId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getLocation() {
        return location;
    }

    public Constants.UserType getUserType() {
        return userType;
    }

    public boolean isPhoneVerified() {
        return isPhoneVerified;
    }

    public void setPhoneVerified(boolean phoneVerified) {
        isPhoneVerified = phoneVerified;
    }

    public List<PaymentMethod> getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }
}
