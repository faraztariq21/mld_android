package com.imedhealthus.imeddoctors.common.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.listeners.FragmentAttachImageListener;
import com.imedhealthus.imeddoctors.common.models.ListItemImage;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.FileUtils;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterListItemImage extends RecyclerView.Adapter<AdapterListItemImage.ImageViewHolder> {

    Context context;
    WeakReference<FragmentAttachImageListener> fragmentAttachImageListenerWeakReference;
    ArrayList<? extends ListItemImage> listItemImages;
    boolean showCrossButton;

    public AdapterListItemImage(Context context, FragmentAttachImageListener fragmentAttachImageListenerWeakReference, ArrayList<? extends ListItemImage> listItemImages, boolean showCrossButton) {
        this.context = context;
        this.fragmentAttachImageListenerWeakReference = new WeakReference<>(fragmentAttachImageListenerWeakReference);
        this.listItemImages = listItemImages;
        this.showCrossButton = showCrossButton;
    }

    public void updateImages(ArrayList<? extends ListItemImage> listItemImage) {

        this.listItemImages = listItemImage;
        notifyDataSetChanged();
    }

    public void removeImage(int index) {
        this.listItemImages.remove(index);
        notifyItemRemoved(index);
        notifyItemRangeChanged(index, listItemImages.size());
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_image, parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, final int position) {

        if (showCrossButton)
            holder.ivCancelImage.setVisibility(View.VISIBLE);
        else
            holder.ivCancelImage.setVisibility(View.INVISIBLE);


        if (listItemImages.get(position).isFilePath()) {
            Glide.with(context).load(new File(listItemImages.get(position).getImagePath())).into(holder.ivMainImage);
        } else if (listItemImages.get(position).isImageUri()) {
            String filePath = FileUtils.getPathFromUri(context, Uri.parse(listItemImages.get(position).getImagePath()));
            Glide.with(context).load(new File(filePath)).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(holder.ivMainImage);
        } else
            Glide.with(context).load(Constants.BASE_URL + listItemImages.get(position).getImagePath()).apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)).into(holder.ivMainImage);

        holder.ivMainImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentAttachImageListenerWeakReference.get().onImageTapped(position);
            }
        });

        holder.ivCancelImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentAttachImageListenerWeakReference.get().onImageCancelTapped(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return listItemImages.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_list_item_image)
        ImageView ivMainImage;
        @BindView(R.id.iv_cancel_image)
        ImageView ivCancelImage;

        public ImageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
