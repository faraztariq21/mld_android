package com.imedhealthus.imeddoctors.common.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.fragments.FragmentImage;
import com.imedhealthus.imeddoctors.common.listeners.OnChatItemClickListener;
import com.imedhealthus.imeddoctors.common.models.ForumComment;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.view_holders.ForumCommentViewHolder;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dev-iMEDHealth-X5 on 18/04/2018.
 */

public class ForumCommentsAdapter extends RecyclerView.Adapter<ForumCommentViewHolder> {

    private List<ForumComment> forumComments;
    private WeakReference<TextView> tvNoRecords;
    Context context;
    private WeakReference<OnChatItemClickListener> onChatItemClickListener;
    Handler handler;

    public ForumCommentsAdapter(Context context, TextView tvNoRecords, OnChatItemClickListener onChatItemClickListener) {

        super();
        this.context = context;
        handler = new Handler();
        this.tvNoRecords = new WeakReference<>(tvNoRecords);
        this.onChatItemClickListener = new WeakReference<>(onChatItemClickListener);
        forumComments = new ArrayList<>();

    }

    public void setForumComments(List<ForumComment> forumComments) {

        this.forumComments = forumComments;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        } else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }

    public List<ForumComment> getForumComments() {
        return forumComments;
    }

    @Override
    public ForumCommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_message, parent, false);
        OnChatItemClickListener listener = null;
        if (onChatItemClickListener.get() != null) {
            listener = onChatItemClickListener.get();
        }
        return new ForumCommentViewHolder(handler, itemView, listener);

    }

    @Override
    public void onBindViewHolder(ForumCommentViewHolder viewHolder, final int position) {

        viewHolder.setData(getItem(position), position);


        viewHolder.ivForumImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCompatActivity appCompatActivity = null;
                if (context instanceof AppCompatActivity)
                    appCompatActivity = (AppCompatActivity) context;
                if (appCompatActivity != null) {
                    /*SharedData.getInstance().setSelectedImageURL(forumComments.get(position).getFileUrl());
                    SimpleFragmentActivity.startActivity(appCompatActivity, Constants.Fragment.FragmentImageFull);
                    */
                    FragmentImage.newInstance(forumComments.get(position).getImageURI(), Constants.BASE_URL + forumComments.get(position).getFileUrl()).show(appCompatActivity.getSupportFragmentManager());
                }
            }
        });

        try {
            if (forumComments.get(position).getFileUrl().endsWith(".pdf"))
                viewHolder.tvFileName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_pdf, 0, 0, 0);
            if (forumComments.get(position).getFileUrl().endsWith(".pptx"))
                viewHolder.tvFileName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_ppt, 0, 0, 0);
            if (forumComments.get(position).getFileUrl().endsWith(".xls"))
                viewHolder.tvFileName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_xls, 0, 0, 0);
            if (forumComments.get(position).getFileUrl().endsWith(".txt"))
                viewHolder.tvFileName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_txt, 0, 0, 0);
            if (forumComments.get(position).getFileUrl().endsWith(".docx"))
                viewHolder.tvFileName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_doc, 0, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return forumComments == null ? 0 : forumComments.size();
    }

    public ForumComment getItem(int position) {
        if (forumComments == null) {
            return null;
        }
        return forumComments.get(position);
    }

    public void addForumComment(ForumComment forumComment) {

        forumComments.add(forumComment);
        notifyDataSetChanged();
        adjustView();

    }

    public void addForumComments(ArrayList<ForumComment> forumComments) {

        this.forumComments.addAll(forumComments);
        notifyDataSetChanged();
        adjustView();

    }

    private void adjustView() {
        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        } else {
            tvNoRecords.get().setVisibility(View.GONE);
        }
    }

}
