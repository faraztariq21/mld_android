package com.imedhealthus.imeddoctors.auth.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.imedhealthus.imeddoctors.BuildConfig;
import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.models.BaseResponse;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.Country;
import com.imedhealthus.imeddoctors.common.models.Notification;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.User;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.NetworkResponseListener;
import com.imedhealthus.imeddoctors.common.networking.NetworkResponseParser;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.Logs;
import com.imedhealthus.imeddoctors.doctor.activities.DoctorDashboardActivity;
import com.imedhealthus.imeddoctors.doctor.models.StaticData;
import com.imedhealthus.imeddoctors.patient.activities.PatientDashboardActivity;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class SplashActivity extends BaseActivity {

    private Dialog dialog;

    // Test commit
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

//        Crittercism.initialize(getApplicationContext(), Constants.CRITTERCISM_APP_ID);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            getUpdatedAppVersion();
        } catch (NoClassDefFoundError noClassDefFoundError) {
            noClassDefFoundError.printStackTrace();
        }
    }

    private void isAppUpdated(String updatedVersion) {
        if (Double.parseDouble(BuildConfig.VERSION_NAME) < Double.parseDouble(updatedVersion)) {
            //perform your task here like show alert dialogue "Need to upgrade app"
            updateAppDialog();
        } else {
            getCountriesCodes("0");
        }


    }

    private void openNextScreen() {
        openDashboard();
        finish();
    }

    public void updateAppDialog() {
        if (dialog != null)
            if (dialog.isShowing())
                dialog.dismiss();

        if (dialog == null) {
            dialog = new Dialog(this);
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.new_message_dialog);
        }

        TextView text = (TextView) dialog.findViewById(R.id.dialog_msg);
        text.setText("Your app is not updated. Please update it now.");
        TextView tv_title = (TextView) dialog.findViewById(R.id.dialog_title);
        tv_title.setText("Update Required");


        Button rejectButton = (Button) dialog.findViewById(R.id.btn_reject);
        rejectButton.setText("UPDATE LATER");
        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getCountriesCodes("0");
                finish();
            }
        });
        Button acceptButton = (Button) dialog.findViewById(R.id.btn_accept);
        acceptButton.setText("UPDATE NOW");
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                String appPackageName = getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

            }
        });

        dialog.show();
    }

    private void openDashboard() {

        SharedData.getInstance().setSelectedFilter(null);

        Intent intent;
        if (CacheManager.getInstance().isUserLoggedIn()) {
            getNotificationsData();
            if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Patient) {
                intent = new Intent(this, PatientDashboardActivity.class);
            } else {
                intent = new Intent(this, DoctorDashboardActivity.class);
            }
        } else {
            intent = new Intent(this, LandingActivity.class);
        }

        startActivity(intent, true);

    }

    public void getNotificationsData() {
        User user = CacheManager.getInstance().getCurrentUser();
        WebServicesHandler.instance.getNotifications(user, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                if (response.status()) {
                    List<Notification> notifications = NetworkResponseParser.getNotifications(response.getJSONArray("GetNotification"));
                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

            }
        });

    }

    public void getUpdatedAppVersion() throws NoClassDefFoundError {
        WebServicesHandler.instance.getUpdatedAppVersion(new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                if (response.status()) {
                    String updatedVersion = response.optString("Version");
                    isAppUpdated(updatedVersion);
                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                retryGetUpdatedAppVersionAlert(SplashActivity.this, "Problem!", Constants.GenericErrorMsg + " Would you like to retry?");
            }
        });

    }

    public void getCountriesCodes(String loginId) {
        WebServicesHandler.instance.getAllPhoneCodes(loginId, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                if (response.status()) {
                    List<Country> countries = new ArrayList<>();
                    /*Country title = new Country();
                    title.setCountryName("Select Country");
                    title.setCountryId(-1);
                    title.setSortName("Select Country");
                    title.setPhoneCode("");
                    countries.add(title);
                    */
                    Gson gson = new Gson();
                    JSONArray dataArray = response.optJSONArray("Countries");
                    for (int i = 0; i < dataArray.length(); i++) {
                        Country country = gson.fromJson(dataArray.get(i).toString(), Country.class);
                        countries.add(country);
                    }
                    SharedData.getInstance().setCountries(countries);
                    getStaticData();
                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                retryGetCountriesCodesAlert(SplashActivity.this, "Problem!", Constants.GenericErrorMsg + " Would you like to retry?");
            }
        });

    }

    public void getStaticData() {

        String userLoginId = "0";
        if (CacheManager.getInstance().isUserLoggedIn()) {
            userLoginId = CacheManager.getInstance().getCurrentUser().getLoginId();
        }

        WebServicesHandler.instance.getStaticData(userLoginId, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                Logs.apiResponse(response.toString());
                if (response.status()) {
                    String session = response.optString("SessionId");
                    String token = response.optString("Token");
                    CacheManager.getInstance().setChatSessionId(session);
                    CacheManager.getInstance().setChatToken(token);

                    JSONArray speciality = response.getJSONArray("Speciality");
                    List<StaticData> specialityList = new ArrayList<>();
                    specialityList.add(new StaticData("-1", "Speciality"));
                    if (speciality != null)
                        for (int i = 0; i < speciality.length(); i++) {

                            StaticData staticData = new StaticData(speciality.getJSONObject(i).getString("Id"), speciality.getJSONObject(i).getString("Title"));
                            specialityList.add(staticData);
                        }
                    SharedData.getInstance().setSpecialityList(specialityList);

                    List<StaticData> insuranceList = new ArrayList<>();
                    insuranceList.add(new StaticData("-1", "Select Insurace"));
                    JSONArray insurance = response.getJSONArray("Insurance");
                    if (insurance != null)
                        for (int i = 0; i < insurance.length(); i++) {

                            StaticData staticData = new StaticData(insurance.getJSONObject(i).getString("Id"), insurance.getJSONObject(i).getString("Title"));
                            insuranceList.add(staticData);
                        }
                    SharedData.getInstance().setInsuranceList(insuranceList);

                    List<StaticData> languageList = new ArrayList<>();
                    JSONArray language = response.getJSONArray("Language");
                    if (language != null)
                        for (int i = 0; i < language.length(); i++) {

                            StaticData staticData = new StaticData(language.getJSONObject(i).getString("Id"), language.getJSONObject(i).getString("Title"));
                            languageList.add(staticData);
                        }
                    SharedData.getInstance().setLanguageList(languageList);
                    openNextScreen();
                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

                retryGetStaticDataAlert(SplashActivity.this, "Problem!", Constants.GenericErrorMsg + " Would you like to retry?");
            }
        });

        WebServicesHandler.instance.getOpenTokKey(new NetworkResponseListener() {
            @Override
            public void onSuccess(String call, Object response) {
                BaseResponse baseResponse = ((BaseResponse) response);

                if (baseResponse != null && baseResponse.status == 1) {
                    Constants.API_KEY = baseResponse.message;
                }
            }

            @Override
            public void onFailure(String call, Throwable t) {

            }
        });

    }

    public void retryGetStaticDataAlert(Context context, String title, String message) {

        AlertDialog dialog = new AlertDialog.Builder(context, R.style.Dialog)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        getStaticData();
                    }
                }).setCancelable(false).create();

        dialog.show();

    }

    public void retryGetCountriesCodesAlert(Context context, String title, String message) {

        AlertDialog dialog = new AlertDialog.Builder(context, R.style.Dialog)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        getCountriesCodes("0");
                    }
                }).setCancelable(false).create();

        dialog.show();

    }

    public void retryGetUpdatedAppVersionAlert(Context context, String title, String message) {

        AlertDialog dialog = new AlertDialog.Builder(context, R.style.Dialog)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            getUpdatedAppVersion();
                        } catch (NoClassDefFoundError noClassDefFoundError) {
                            noClassDefFoundError.printStackTrace();
                        }
                    }
                }).setCancelable(false).create();

        dialog.show();

    }
}
