package com.imedhealthus.imeddoctors.auth.activities;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.handlers.FacebookHandler;
import com.imedhealthus.imeddoctors.common.handlers.OpenTokHandler;
import com.imedhealthus.imeddoctors.common.listeners.OnFacebookResultsListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.User;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.IntentUtils;
import com.imedhealthus.imeddoctors.common.utils.Logs;
import com.imedhealthus.imeddoctors.common.utils.TinyDB;
import com.imedhealthus.imeddoctors.doctor.activities.DoctorDashboardActivity;
import com.imedhealthus.imeddoctors.patient.activities.PatientDashboardActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/5/18.
 */

public class LoginActivity extends BaseActivity implements OnFacebookResultsListener {

    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.btn_doctor)
    Button btnDoctor;
    @BindView(R.id.btn_patient)
    Button btnPatient;


    @BindView(R.id.iv_show_pass)
    ImageView ivShowPass;


    Boolean isShowingPassword = false;
    FacebookHandler fbHandler;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private Constants.UserType role;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        // get the previous logged in account type from sharePrefs and set current account type accordingle
        String previousAccountType = new TinyDB(this).getString(Constants.PREVIOUS_LOGIN_TYPE);
        if (previousAccountType == null)
            toggleAccountType(false);
        else if (previousAccountType.equals(Constants.PREVIOUS_LOGIN_DOCTOR))
            toggleAccountType(true);
        else if (previousAccountType.equals(Constants.PREVIOUS_LOGIN_PATIENT))
            toggleAccountType(false);


        if (SharedData.getInstance().isPostQuestion()) {
            btnDoctor.setVisibility(View.GONE);
            btnPatient.setVisibility(View.GONE);
        } else {

        }

        ivShowPass.setColorFilter(Color.WHITE);
        fbHandler = new FacebookHandler(this, this);
    }

    //Actions
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_login:
                if (validateFields()) {
                    login();
                }
                break;

            case R.id.btn_facebook:
                startFbLogin();
                break;

            case R.id.tv_register:
                openRegister();
                break;

            case R.id.tv_forgot_password:
                openForgotPassword();
                break;

            case R.id.tv_privacy:
                openPrivacyPolicy();
                break;
            case R.id.btn_doctor:
                toggleAccountType(true);
                break;

            case R.id.btn_patient:
                toggleAccountType(false);
                break;

            case R.id.iv_show_pass:
                togglePasswordVisibility();
                break;


            default:
                break;
        }
    }


    private void togglePasswordVisibility() {

        isShowingPassword = !isShowingPassword;
        if (isShowingPassword) {
            ivShowPass.setImageResource(R.drawable.ic_eye_crossed_blue);

            etPassword.setInputType(InputType.TYPE_CLASS_TEXT);

        } else {
            ivShowPass.setImageResource(R.drawable.ic_eye_normal_blue);

            etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        }
        etPassword.setSelection(etPassword.getText().length());
        //etConfirmPassword.setSelection(etPassword.getText().length());

    }

    private void toggleAccountType(boolean isDoctor) {

        SharedData.getInstance().setDoctorSelected(isDoctor);

        btnDoctor.setSelected(isDoctor);
        btnPatient.setSelected(!isDoctor);

    }

    private void openRegister() {

        Intent intent = new Intent(this, RegisterActivity.class);
        intent.putExtra("email", etEmail.getText().toString());
        startActivity(intent, true);

    }

    private void openForgotPassword() {

        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent, true);

    }

    private void openPrivacyPolicy() {
        IntentUtils.openUrl(this, Constants.URLS.PrivacyPolicy);
    }

    private void startFbLogin() {

    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;
        if (etEmail.getText().toString().isEmpty() || etPassword.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        }/* else if (!GenericUtils.isValidEmail(etEmail.getText().toString())) {
            errorMsg = "Invalid email address";
        }
        */
        if (errorMsg != null) {
            AlertUtils.showAlert(this, Constants.ErrorAlertTitle, errorMsg);
            return false;
        }
        return true;

    }

    private void openNextScreen(User user) {


        Intent intent;

        if (SharedData.getInstance().shouldContinueBooking()) {

            intent = new Intent(this, SimpleFragmentActivity.class);
            intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CompleteAppointment.ordinal());
           /* if (user.getPaymentMethods() != null && user.getPaymentMethods().size() > 0) {
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CompleteAppointment.ordinal());
            }else {
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditPaymentMethod.ordinal());
            }
*/
        } else if (SharedData.getInstance().isPostQuestion()) {
            SharedData.getInstance().setPublic(false);
            intent = new Intent(this, SimpleFragmentActivity.class);
            intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.ForumPostQuestionFragment.ordinal());
        } else {
            if (user.getUserType() == Constants.UserType.Patient) {
                intent = new Intent(this, PatientDashboardActivity.class);
            } else {
                intent = new Intent(this, DoctorDashboardActivity.class);
            }

        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intent, true);


    }


    public void getStaticData(final User user) {
        WebServicesHandler.instance.getStaticData(user.getLoginId(), new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
                AlertUtils.dismisDialog();
                if (response.status()) {
                    String session = response.optString("SessionId");
                    String token = response.optString("Token");
                    CacheManager.getInstance().setChatSessionId(session);
                    CacheManager.getInstance().setChatToken(token);
                    OpenTokHandler.getInstance().initializeSession();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            openNextScreen(user);
                        }
                    }, 1000);
                }
            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
                AlertUtils.dismisDialog();
                //  showDeleteAlert(SplashActivity.this,"Problem!",Constants.GenericErrorMsg+" Would you like to retry?");
            }
        });

    }


    //Fb login handling
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fbHandler.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onFacebookLoginComplete(String id, String fullName) {
        socialLogin(id, fullName);
    }

    @Override
    public void onFacebookLoginError(String error) {
        if (!error.equals("User Canceled")) {
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    //Webservices
    private void login() {

        AlertUtils.showProgress(this);
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        String userType = btnDoctor.isSelected() ? "doctor" : "patient";

        WebServicesHandler.instance.login(email, password, userType, loginResponseListener);

    }

    private void socialLogin(String fbId, String fullName) {

    }

    private CustomCallback<RetrofitJSONResponse> loginResponseListener = new CustomCallback<RetrofitJSONResponse>() {
        @Override
        public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

            if (btnDoctor.isSelected()) {
                new TinyDB(LoginActivity.this).putString(Constants.PREVIOUS_LOGIN_TYPE, Constants.PREVIOUS_LOGIN_DOCTOR);
            } else {
                new TinyDB(LoginActivity.this).putString(Constants.PREVIOUS_LOGIN_TYPE, Constants.PREVIOUS_LOGIN_PATIENT);
            }


            Logs.apiResponse(response.toString());
            if (!response.status()) {
                AlertUtils.dismissProgress();
                AlertUtils.showAlert(LoginActivity.this, Constants.ErrorAlertTitle,
                        response.message() + " or check your account type");
                return;
            }

            User user = new User(response);
            if (user.isPhoneVerified()) {
                CacheManager.getInstance().setLoggedInUser(user);
                getStaticData(user);
            } else {
                AlertUtils.dismissProgress();
                showActivationDialog(user);
            }


        }

        @Override
        public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

            AlertUtils.dismissProgress();
            AlertUtils.showAlert(LoginActivity.this, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);

        }
    };

    public void showActivationDialog(final User user) {
        final Dialog alertDialog = new Dialog(this);
        alertDialog.setContentView(R.layout.dialog_code_verification);
        alertDialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_dialog);
        alertDialog.setCancelable(false);
        final EditText etCode = alertDialog.findViewById(R.id.et_code);
        final TextView tvResend = alertDialog.findViewById(R.id.tv_resend);

        final Button bVerify = alertDialog.findViewById(R.id.btn_verify);
        Button bCancel = alertDialog.findViewById(R.id.btn_cancel);

        etCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 4) {
                    bVerify.setBackgroundResource(R.drawable.rounded_corner_button_green);
                } else {
                    bVerify.setBackgroundResource(R.drawable.rounded_corner_button_dark_green);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        tvResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resendCode(user.getLoginId());
            }
        });

        bVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                String code = etCode.getText().toString();
                if (code.length() == 4) {
                    verifyCode(user.getLoginId(), code);
                }
            }
        });

        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                alertDialog.dismiss();
                requestManualVerification(user.getLoginId(), user.getFirstName() + user.getLastName(), user.getEmail(), Constants.UserType.getString(user.getUserType()));
            }
        });

        alertDialog.show();
    }

    private void verifyCode(String loginId, String code) {

        if (!AlertUtils.isShowingProgress()) {
            AlertUtils.showProgress(this);
        }


        WebServicesHandler.instance.phoneVerification(phoneVerificationResponseListener, loginId, code);
    }

    private CustomCallback<RetrofitJSONResponse> phoneVerificationResponseListener = new CustomCallback<RetrofitJSONResponse>() {
        @Override
        public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
            Logs.apiResponse(response.toString());
            AlertUtils.dismissProgress();

            if (!response.status()) {
                AlertUtils.showAlert(LoginActivity.this, Constants.ErrorAlertTitle, response.message());
                return;
            }

            User user = new User(response);
            CacheManager.getInstance().setLoggedInUser(user);
            getStaticData(user);

        }

        @Override
        public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
            AlertUtils.dismissProgress();
            AlertUtils.showAlert(LoginActivity.this, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
        }
    };

    private void resendCode(String loginId) {

        if (!AlertUtils.isShowingProgress()) {
            AlertUtils.showProgress(this);
        }


        WebServicesHandler.instance.resendPhoneVerificationCode(resendCodeListener, loginId);
    }

    private CustomCallback<RetrofitJSONResponse> resendCodeListener = new CustomCallback<RetrofitJSONResponse>() {
        @Override
        public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
            Logs.apiResponse(response.toString());
            AlertUtils.dismissProgress();

            if (!response.status()) {
                AlertUtils.showAlert(LoginActivity.this, Constants.ErrorAlertTitle, response.message());
            } else {
                AlertUtils.showAlert(LoginActivity.this, Constants.SuccessAlertTitle, "Verification status has been sent to your phone.");
            }

        }

        @Override
        public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
            AlertUtils.dismissProgress();
            AlertUtils.showAlert(LoginActivity.this, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
        }
    };

    private void requestManualVerification(String loginId, String name, String email, String type) {

        if (!AlertUtils.isShowingProgress()) {
            AlertUtils.showProgress(this);
        }


        WebServicesHandler.instance.manualEmail(requestManualVerificationListener, loginId, name, email, type);
    }

    private CustomCallback<RetrofitJSONResponse> requestManualVerificationListener = new CustomCallback<RetrofitJSONResponse>() {
        @Override
        public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
            Logs.apiResponse(response.toString());
            AlertUtils.dismissProgress();

            if (!response.status()) {
                AlertUtils.showAlert(LoginActivity.this, Constants.ErrorAlertTitle, response.message());
            } else {
                AlertUtils.showAlert(LoginActivity.this, Constants.SuccessAlertTitle, "We have received your request for manual verification. We will review your information and will be back to you shortly.");
            }

        }

        @Override
        public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
            AlertUtils.dismissProgress();
            AlertUtils.showAlert(LoginActivity.this, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
        }
    };


}
