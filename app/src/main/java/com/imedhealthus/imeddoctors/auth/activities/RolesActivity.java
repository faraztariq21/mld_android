package com.imedhealthus.imeddoctors.auth.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;


import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RolesActivity extends AppCompatActivity {



    @BindView(R.id.tv_patient)
    TextView tvPatient;
    @BindView(R.id.tv_physician)
    TextView tvPhysician;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roles);

        ButterKnife.bind(this);


    }



    public void onClick(View view){

        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.cont_physician:
                selectRole(Constants.UserType.Doctor);
                break;

            case R.id.cont_patient:
                selectRole(Constants.UserType.Patient);
                break;

        }

    }

    private void selectRole(Constants.UserType role) {

        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("role", role.ordinal());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.stable);

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        overridePendingTransition(R.anim.stable, R.anim.slide_to_right);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
