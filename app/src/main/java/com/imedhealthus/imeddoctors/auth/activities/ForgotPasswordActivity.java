package com.imedhealthus.imeddoctors.auth.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/5/18.
 */

public class ForgotPasswordActivity extends BaseActivity {

    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.btn_doctor)
    Button btnDoctor;
    @BindView(R.id.btn_patient)
    Button btnPatient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        ButterKnife.bind(this);
        toggleAccountType(SharedData.getInstance().isDoctorSelected());
    }


    private void toggleAccountType(boolean isDoctor) {
        SharedData.getInstance().setDoctorSelected(isDoctor);
        btnDoctor.setSelected(isDoctor);
        btnPatient.setSelected(!isDoctor);

    }
    //Actions
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_back:
                onBackPressed(true);
                break;

            case R.id.btn_send:
                if (validateFields()) {
                    sendPassword();
                }
                break;

            case R.id.btn_doctor:
                toggleAccountType(true);
                break;

            case R.id.btn_patient:
                toggleAccountType(false);
                break;
            default:
                break;
        }
    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;
        if (etEmail.getText().toString().isEmpty() ) {
            errorMsg = "Kindly fill all fields";
        }else if(!GenericUtils.isValidEmail(etEmail.getText().toString() )) {
            errorMsg = "Kindly Enter a valid email";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(this, Constants.ErrorAlertTitle, errorMsg);
            return false;
        }
        return true;

    }

    private void showSuccessAlert() {

        new AlertDialog.Builder(this)
                .setTitle("Password Has been update successfully")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        onBackPressed();

                    }
                })
                .show();

    }


    //Webservices
    private void sendPassword() {

        AlertUtils.showProgress(this);
        String email = etEmail.getText().toString();
        String userType = btnDoctor.isSelected() ? "doctor" : "patient";

        WebServicesHandler.instance.forgotPassword(email,userType, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                if (!response.status()) {
                    AlertUtils.showAlert(ForgotPasswordActivity.this, Constants.ErrorAlertTitle, response.message());
                    return;
                }

                moveToChangePassword(response);

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

                AlertUtils.dismissProgress();
                AlertUtils.showAlert(ForgotPasswordActivity.this, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);

            }
        });

    }

    private void moveToChangePassword(JSONObject jsonObject) {

        SharedData.getInstance().setSelectedLoginId(jsonObject.optString("LoginId"));
        SharedData.getInstance().setSelectedVerificationCode(jsonObject.optString("VerificationCode"));

        Intent intent = new Intent(this, NewPasswordActivity.class);
        startActivity(intent, true);

    }

}
