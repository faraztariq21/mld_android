package com.imedhealthus.imeddoctors.auth.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.fragments.FragmentSuggestionDialog;
import com.imedhealthus.imeddoctors.common.handlers.FacebookHandler;
import com.imedhealthus.imeddoctors.common.listeners.OnFacebookResultsListener;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.Country;
import com.imedhealthus.imeddoctors.common.models.ImageSelector;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.SuggestionListItem;
import com.imedhealthus.imeddoctors.common.models.User;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.FileUtils;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;
import com.imedhealthus.imeddoctors.common.utils.Logs;
import com.imedhealthus.imeddoctors.common.utils.UploadImage;
import com.imedhealthus.imeddoctors.doctor.activities.DoctorDashboardActivity;
import com.imedhealthus.imeddoctors.doctor.models.StaticData;
import com.imedhealthus.imeddoctors.patient.activities.PatientDashboardActivity;
import com.soundcloud.android.crop.Crop;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umair on 1/5/18.
 */

public class RegisterActivity extends BaseActivity implements ImageSelector.OnImageSelectionListener, OnFacebookResultsListener {

    @BindView(R.id.iv_profile)
    ImageView ivProfile;

    @BindView(R.id.btn_doctor)
    Button btnDoctor;
    @BindView(R.id.btn_patient)
    Button btnPatient;

    @BindView(R.id.tv_country)
    EditText etCountry;
    @BindView(R.id.tv_speciality)
    EditText etSpeciality;
    @BindView(R.id.et_username)
    EditText etUserName;
    @BindView(R.id.et_first_name)
    EditText etFirstName;
    @BindView(R.id.et_last_name)
    EditText etLastName;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_confirm_password)
    EditText etConfirmPassword;
    @BindView(R.id.et_referral)
    EditText etReferral;
    @BindView(R.id.sp_prefix)
    Spinner spPrefix;


    @BindView(R.id.cont_referral)
    LinearLayout llReferral;
    @BindView(R.id.cont_speciality)
    LinearLayout llSpeciality;

    @BindView(R.id.et_phone_number)
    EditText etPhone;

    @BindView(R.id.iv_show_pass)
    ImageView ivShowPass;
    @BindView(R.id.iv_show_confirm_pass)
    ImageView ivShowConfirmPass;


    Boolean isShowingPassword = false;
    private ImageSelector imageSelector;
    private Uri selectedImgUri;
    private boolean isImageSelected = false;
    private FacebookHandler fbHandler;
    private Country selectedCountry;
    private StaticData selectedSpeciality;
    private int selectedSpecialityIndex = 0;
    private int selectedCountryIndex = 0;

    ProgressDialog progressDialog;

    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ButterKnife.bind(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Signing up");
        Intent intent = getIntent();
        if (intent != null) {
            String email = intent.getStringExtra("email");
            etEmail.setText(email);
        }

        initHelper();

    }

    private void initHelper() {
        toggleAccountType(SharedData.getInstance().isDoctorSelected());
        fbHandler = new FacebookHandler(this, this);
        ivShowPass.setColorFilter(Color.WHITE);
        ivShowConfirmPass.setColorFilter(Color.WHITE);

        final List<StaticData> listSpeciality = SharedData.getInstance().getSpecialityList();
        final List<Country> countries = SharedData.getInstance().getCountries();

        //countries.add(new Country(1, "Pakistan", "Pk", "+92"));
        //listSpeciality.add(new StaticData(1 + "", "General Physician"));

        final FragmentSuggestionDialog.FragmentSuggestionDialogListener countrySelectionListener = new FragmentSuggestionDialog.FragmentSuggestionDialogListener() {
            @Override
            public void onSuggestionSelected(int index, SuggestionListItem suggestionListItem) {
                selectedCountry = (Country) suggestionListItem.getSuggestionListItem();
                etCountry.setText(selectedCountry.getPhoneCode());
                selectedCountryIndex = index + 1;
            }
        };

        etCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedData.getInstance().setGetCountryCodes(true);
                FragmentSuggestionDialog.newInstance(getResources().getString(R.string.select_country), Country.getSuggestions((ArrayList<Country>) countries), false, countrySelectionListener).show(getSupportFragmentManager());

            }
        });

        final FragmentSuggestionDialog.FragmentSuggestionDialogListener specialitySelectionListener = new FragmentSuggestionDialog.FragmentSuggestionDialogListener() {
            @Override
            public void onSuggestionSelected(int index, SuggestionListItem suggestionListItem) {
                etSpeciality.setText(suggestionListItem.getSuggestionText());
                selectedSpeciality = (StaticData) suggestionListItem.getSuggestionListItem();
                //selectedSpecialityIndex = index;
                SharedData.getInstance().setGetCountryCodes(false);
            }
        };


        etSpeciality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentSuggestionDialog.newInstance(getResources().getString(R.string.select_speciality), StaticData.getSuggestions((ArrayList<StaticData>) listSpeciality), false, specialitySelectionListener).show(getSupportFragmentManager());
            }
        });
        spPrefix.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("Value", "" + i);
                try {
                    ((TextView) ((Spinner) findViewById(R.id.sp_prefix)).getSelectedView()).setTextColor(getResources().getColor(R.color.white));
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                } catch (NullPointerException npe) {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    //Actions
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_back:
                onBackPressed(true);
                break;

            case R.id.cont_sel_image:
                selectProfileImage();
                break;

            case R.id.btn_register:
                if (validateFields()) {
                    validatePhoneNumber();
                }
                break;

            case R.id.btn_facebook:
                startFbLogin();
                break;

            case R.id.btn_doctor:
                toggleAccountType(true);
                break;

            case R.id.btn_patient:
                toggleAccountType(false);
                break;

            case R.id.iv_show_pass:
            case R.id.iv_show_confirm_pass:
                togglePasswordVisibility();
                break;
        }
    }


    private void togglePasswordVisibility() {

        isShowingPassword = !isShowingPassword;
        if (isShowingPassword) {
            ivShowPass.setImageResource(R.drawable.ic_eye_crossed_blue);
            ivShowConfirmPass.setImageResource(R.drawable.ic_eye_crossed_blue);
            etPassword.setInputType(InputType.TYPE_CLASS_TEXT);
            etConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT);
        } else {
            ivShowPass.setImageResource(R.drawable.ic_eye_normal_blue);
            ivShowConfirmPass.setImageResource(R.drawable.ic_eye_normal_blue);
            etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            etConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
        etPassword.setSelection(etPassword.getText().length());
        //etConfirmPassword.setSelection(etPassword.getText().length());

    }

    private void selectProfileImage() {

        if (imageSelector == null) {
            imageSelector = new ImageSelector(null, this, this);
        }
        imageSelector.showSourceAlert();

    }

    private void toggleAccountType(boolean isDoctor) {

        SharedData.getInstance().setDoctorSelected(isDoctor);
        btnDoctor.setSelected(isDoctor);
        btnPatient.setSelected(!isDoctor);
        if (isDoctor) {
            llReferral.setVisibility(View.VISIBLE);
            llSpeciality.setVisibility(View.VISIBLE);
        } else {
            llReferral.setVisibility(View.GONE);
            llSpeciality.setVisibility(View.GONE);
        }
    }

    //Image selection
    @Override
    public void onImageSelected(Bitmap bitmap, Uri uri, Uri fullImageUri) {

        isImageSelected = true;
        selectedImgUri = uri;
        if (bitmap != null) {
            ivProfile.setImageBitmap(bitmap);
        } else {
            ivProfile.setImageURI(uri);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ImageSelector.PERMISSIONS_CODE:
                if (imageSelector != null) {
                    imageSelector.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ImageSelector.CAMERA_REQUEST_CODE:
            case ImageSelector.GALLERY_REQUEST_CODE:
            case Crop.REQUEST_CROP:
                if (imageSelector != null) {
                    imageSelector.onActivityResult(requestCode, resultCode, data);
                }
                break;
            default:
                fbHandler.onActivityResult(requestCode, resultCode, data);
                break;

        }

    }

    //Other
    private boolean validateFields() {

        String errorMsg = null;
        if (etUserName.getText().toString().isEmpty() || etFirstName.getText().toString().isEmpty() || etLastName.getText().toString().isEmpty()
                || etEmail.getText().toString().isEmpty() || etPassword.getText().toString().isEmpty()
                || etConfirmPassword.getText().toString().isEmpty() || etPhone.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields to continue";
        } else if (!GenericUtils.isValidEmail(etEmail.getText().toString())) {
            errorMsg = "Invalid email address";
        }else if(!GenericUtils.isValidPassword(etPassword.getText().toString())){
            errorMsg = "Password does not comply with passwod policy | Minimum length of 8 and contraining at least 1 upper, lower and numeric character (a-z,A-Z,0-9)";
        }else if (!etPassword.getText().toString().equals(etConfirmPassword.getText().toString())) {
            errorMsg = "Passwords don't match";
        } else if (spPrefix.getSelectedItemPosition() == 0) {
            errorMsg = "Kindly select a prefix";
        } else if (btnDoctor.isSelected() && (etSpeciality.getText().toString().isEmpty() || etSpeciality.getText().toString().equals("Speciality"))) {
            errorMsg = "Kindly select specialty";
        } else if (selectedCountryIndex == 0) {
            errorMsg = "Kindly select country";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(this, Constants.ErrorAlertTitle, errorMsg);
            return false;
        }
        return true;

    }

    private void startFbLogin() {
        fbHandler.login(this);
    }

    private void openNextScreen(User user) {

        CacheManager.getInstance().setLoggedInUser(user);

        Intent intent;

        if (SharedData.getInstance().shouldContinueBooking()) {

            intent = new Intent(this, SimpleFragmentActivity.class);

            if (user.getPaymentMethods() != null && user.getPaymentMethods().size() > 0) {
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.CompleteAppointment.ordinal());
            } else {
                intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.AddEditPaymentMethod.ordinal());
            }

        } else {

            if (user.getUserType() == Constants.UserType.Patient) {
                intent = new Intent(this, PatientDashboardActivity.class);
            } else {
                intent = new Intent(this, DoctorDashboardActivity.class);
            }

        }

        startActivity(intent, true);
        SharedData.getInstance().getStaticData();

    }


    //Fb login
    @Override
    public void onFacebookLoginComplete(String id, String fullName) {
        socialLogin(id, fullName);
    }

    @Override
    public void onFacebookLoginError(String error) {
        if (!error.equals("User Canceled")) {
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }


    //Webservices
    private void register(String imageUrl) {

        if (!progressDialog.isShowing())
            progressDialog.show();
        /*if (!AlertUtils.isShowingProgress()) {
            AlertUtils.showProgress(this);
        }*/

        String prefix = spPrefix.getSelectedItem().toString();
        String userName = etUserName.getText().toString();

        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();

        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        String phoneCode = selectedCountry.getPhoneCode();
        String phone = phoneCode + etPhone.getText().toString();
        String SpecialityId = null;
        if (selectedSpeciality != null)
            SpecialityId = selectedSpeciality.getId();
        String referral = btnDoctor.isSelected() ? etReferral.getText().toString() : "";

        String userType = btnDoctor.isSelected() ? "doctor" : "patient";

        WebServicesHandler.instance.register(userName, prefix, firstName, lastName, password, email,
                userType, imageUrl, phone, SpecialityId, referral, true, registerResponseListener);


    }

    //Webservices
    private void validatePhoneNumber() {


        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
        /*


        if (!AlertUtils.isShowingProgress()) {
            AlertUtils.showProgress(this);
        }*/

        String phoneCode = selectedCountry.getPhoneCode();
        String phoneNumber = phoneCode + etPhone.getText().toString();
        String countryCode = selectedCountry.getSortName();

        WebServicesHandler.instance.validatePhoneNumber(phoneNumber, countryCode, validatePhoneNumberListener);
    }

    private void uploadImage() {

        if (!progressDialog.isShowing())
            progressDialog.show();

        //AlertUtils.showProgress(this);

        final String uploadPath = Constants.FTP.patientPath + (new Date()).getTime() + ".png";

        this.selectedImgUri = FileUtils.compressImageAndGetNewImageUri(RegisterActivity.this, selectedImgUri);

        UploadImage uploadImage = new UploadImage(uploadPath, FileUtils.getPathFromUri(RegisterActivity.this, selectedImgUri), new UploadImage.OnImageUploadCompletionListener() {
            @Override
            public void onImageUploadComplete(boolean success, int index, final String uploadPath) {

                if (!success) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();

                    //  AlertUtils.dismissProgress();
                    //AlertUtils.showAlert(RegisterActivity.this, Constants.ErrorAlertTitle, "Can't upload image now. Kindly try later.");
                    Toast.makeText(RegisterActivity.this, "Can't upload image now. Please try later.", Toast.LENGTH_LONG).show();


                    return;
                }

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        register(uploadPath);
                    }
                });

            }
        });
        uploadImage.execute();

    }

    private void socialLogin(String fbId, String fullName) {

    }

    private CustomCallback<RetrofitJSONResponse> registerResponseListener = new CustomCallback<RetrofitJSONResponse>() {
        @Override
        public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
            Logs.apiResponse(response.toString());
            /*AlertUtils.dismissProgress();*/
            if (progressDialog.isShowing())
                progressDialog.dismiss();
            if (!response.status()) {
                AlertUtils.showAlert(RegisterActivity.this, Constants.ErrorAlertTitle, response.message());
                return;
            }

            User user = new User(response);
            showActivationDialog(user);

        }

        @Override
        public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
            AlertUtils.dismissProgress();
            AlertUtils.showAlert(RegisterActivity.this, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
        }
    };

    private CustomCallback<RetrofitJSONResponse> validatePhoneNumberListener = new CustomCallback<RetrofitJSONResponse>() {
        @Override
        public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
            Logs.apiResponse(response.toString());
            AlertUtils.dismissProgress();

            if (!response.status()) {
                AlertUtils.showAlert(RegisterActivity.this, Constants.ErrorAlertTitle, "Invalid phone number");
                progressDialog.dismiss();
                return;
            }

            if (isImageSelected) {
                uploadImage();
            } else {
                register("");
            }


        }

        @Override
        public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
            AlertUtils.dismissProgress();
            AlertUtils.showAlert(RegisterActivity.this, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
            progressDialog.dismiss();
        }
    };

    public void showActivationDialog(final User user) {
        final Dialog alertDialog = new Dialog(this);
        alertDialog.setContentView(R.layout.dialog_code_verification);
        alertDialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_dialog);
        alertDialog.setCancelable(false);
        final EditText etCode = alertDialog.findViewById(R.id.et_code);
        final TextView tvResend = alertDialog.findViewById(R.id.tv_resend);

        final Button bVerify = (Button) alertDialog.findViewById(R.id.btn_verify);
        Button bCancel = (Button) alertDialog.findViewById(R.id.btn_cancel);

        etCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 4) {
                    bVerify.setBackgroundResource(R.drawable.rounded_corner_button_green);
                } else {
                    bVerify.setBackgroundResource(R.drawable.rounded_corner_button_dark_green);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });


        tvResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resendCode(user.getLoginId());
            }
        });

        bVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                String code = etCode.getText().toString();
                if (code.length() == 4) {
                    verifyCode(user.getLoginId(), code);
                }
            }
        });

        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                alertDialog.dismiss();
                requestManualVerification(user.getLoginId(), user.getFirstName() + user.getLastName(), user.getEmail(), Constants.UserType.getString(user.getUserType()));
            }
        });

        alertDialog.show();
    }

    private void verifyCode(String loginId, String code) {

        if (!AlertUtils.isShowingProgress()) {
            AlertUtils.showProgress(this);
        }


        WebServicesHandler.instance.phoneVerification(phoneVerificationResponseListener, loginId, code);
    }

    private CustomCallback<RetrofitJSONResponse> phoneVerificationResponseListener = new CustomCallback<RetrofitJSONResponse>() {
        @Override
        public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
            Logs.apiResponse(response.toString());
            AlertUtils.dismissProgress();

            if (!response.status()) {
                AlertUtils.showAlert(RegisterActivity.this, Constants.ErrorAlertTitle, response.message());
                return;
            }

            User user = new User(response);
            openNextScreen(user);

        }

        @Override
        public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
            AlertUtils.dismissProgress();
            AlertUtils.showAlert(RegisterActivity.this, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
        }
    };

    private void resendCode(String loginId) {

        if (!AlertUtils.isShowingProgress()) {
            AlertUtils.showProgress(this);
        }


        WebServicesHandler.instance.resendPhoneVerificationCode(resendCodeListener, loginId);
    }

    private CustomCallback<RetrofitJSONResponse> resendCodeListener = new CustomCallback<RetrofitJSONResponse>() {
        @Override
        public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
            Logs.apiResponse(response.toString());
            AlertUtils.dismissProgress();

            if (!response.status()) {
                AlertUtils.showAlert(RegisterActivity.this, Constants.ErrorAlertTitle, response.message());
            } else {
                AlertUtils.showAlert(RegisterActivity.this, Constants.SuccessAlertTitle, "Verification status has been sent to your phone.");
            }

        }

        @Override
        public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
            AlertUtils.dismissProgress();
            AlertUtils.showAlert(RegisterActivity.this, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
        }
    };

    private void requestManualVerification(String loginId, String name, String email, String type) {

        if (!AlertUtils.isShowingProgress()) {
            AlertUtils.showProgress(this);
        }


        WebServicesHandler.instance.manualEmail(requestManualVerificationListener, loginId, name, email, type);
    }

    private CustomCallback<RetrofitJSONResponse> requestManualVerificationListener = new CustomCallback<RetrofitJSONResponse>() {
        @Override
        public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {
            Logs.apiResponse(response.toString());
            AlertUtils.dismissProgress();

            if (!response.status()) {
                AlertUtils.showAlert(RegisterActivity.this, Constants.ErrorAlertTitle, response.message());
            } else {
                AlertUtils.showAlert(RegisterActivity.this, Constants.SuccessAlertTitle, "We have received your request for manual verification. We will review your information and will be back to you shortly.");
            }

        }

        @Override
        public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {
            AlertUtils.dismissProgress();
            AlertUtils.showAlert(RegisterActivity.this, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);
        }
    };

}
