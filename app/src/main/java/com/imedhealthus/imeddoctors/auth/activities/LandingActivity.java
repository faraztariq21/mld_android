package com.imedhealthus.imeddoctors.auth.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.application.ApplicationManager;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.activities.SimpleFragmentActivity;
import com.imedhealthus.imeddoctors.common.adapters.CustomCitySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.adapters.CustomCountrySpinnerAdapter;
import com.imedhealthus.imeddoctors.common.fragments.FragmentSuggestionDialog;
import com.imedhealthus.imeddoctors.common.models.CacheManager;
import com.imedhealthus.imeddoctors.common.models.Country;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.models.SuggestionListItem;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.doctor.activities.DoctorDashboardActivity;
import com.imedhealthus.imeddoctors.doctor.models.Filter;
import com.imedhealthus.imeddoctors.doctor.models.StaticData;
import com.imedhealthus.imeddoctors.patient.activities.PatientDashboardActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

import static com.imedhealthus.imeddoctors.common.fragments.CallFragment.VIDEO_APP_PERM;

public class LandingActivity extends BaseActivity {


    @BindView(R.id.btn_search_discuss)
    TextView btnSearchDiscuss;
    @BindView(R.id.btn_dashboard)
    TextView btnDashboard;
    @BindView(R.id.et_question)
    EditText etQuestion;

    @BindView(R.id.tv_speciality)
    TextView tvSpeciality;
    @BindView(R.id.tv_country)
    TextView tvCountry;
    @BindView(R.id.ll_cont_country)
    LinearLayout contCountry;
    @BindView(R.id.ll_cont_specialty)
    LinearLayout contSpecialty;

    Country selectedCountry;
    StaticData selectedSpecialty;

    private List<Country> countries;
    private CustomCountrySpinnerAdapter customCountryAdapter;


    private CustomCitySpinnerAdapter cityAdapter;
    private int selectedSpecialtyIndex = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (getIntent().getBooleanExtra("Exit", false)) {
            finish();
            return;
        }

        setContentView(R.layout.activity_landing);

        ButterKnife.bind(this);
        initHelper();

    }

    private void initHelper() {
        final FragmentSuggestionDialog.FragmentSuggestionDialogListener countrySelectionListener = new FragmentSuggestionDialog.FragmentSuggestionDialogListener() {
            @Override
            public void onSuggestionSelected(int index, SuggestionListItem suggestionListItem) {
                tvCountry.setText(suggestionListItem.getSuggestionText());
                selectedCountry = (Country) suggestionListItem.getSuggestionListItem();
            }
        };
        countries = SharedData.getInstance().getCountriesList();
        contCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentSuggestionDialog.newInstance(getResources().getString(R.string.select_country), Country.getSuggestions((ArrayList<Country>) countries), false, countrySelectionListener).show(getSupportFragmentManager());
            }
        });
        final List<StaticData> listSpeciality = SharedData.getInstance().getSpecialityList();

        final FragmentSuggestionDialog.FragmentSuggestionDialogListener specialtySelectionListener = new FragmentSuggestionDialog.FragmentSuggestionDialogListener() {
            @Override
            public void onSuggestionSelected(int index, SuggestionListItem suggestionListItem) {
                selectedSpecialtyIndex = index;
                tvSpeciality.setText(suggestionListItem.getSuggestionText());
                selectedSpecialty = (StaticData) suggestionListItem.getSuggestionListItem();
            }
        };


        contSpecialty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentSuggestionDialog.newInstance(getResources().getString(R.string.select_speciality), StaticData.getSuggestions((ArrayList<StaticData>) listSpeciality), false, specialtySelectionListener).show(getSupportFragmentManager());
            }
        });


        if (!CacheManager.getInstance().isUserLoggedIn()) {
            btnDashboard.setText("Login / Register");
        }


        requestPermissions();
        if (ActivityCompat.checkSelfPermission(ApplicationManager.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(ApplicationManager.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            Activity activity = ((ApplicationManager) ApplicationManager.getContext()).getCurrentActivity();
            if (activity != null && activity instanceof BaseActivity) {
                ((BaseActivity) activity).requestLocationPermission();
            }
            return;
        }

    }

    @AfterPermissionGranted(VIDEO_APP_PERM)
    private void requestPermissions() {

        String[] permissions = {Manifest.permission.INTERNET, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.LOCATION_HARDWARE};
        if (EasyPermissions.hasPermissions(getApplicationContext(), permissions)) {

        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.video_perm_msg), VIDEO_APP_PERM, permissions);
        }

    }

    //Actions
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_search_discuss:
                String question = etQuestion.getText().toString();
                if (TextUtils.isEmpty(question)) {
                    openFindDoctor();
                } else {
                    navigateToLoginScreen();
                }
                break;

            case R.id.btn_dashboard:
                openDashboard();
                break;

        }
    }

    private void openFindDoctor() {
        Filter filter = new Filter();

        if (selectedSpecialty != null) {
            if (selectedSpecialty.getSuggestionText() != "Specialty")
                filter.setSpeciality(selectedSpecialty.getId());
        }

        if (selectedCountry != null)
            filter.setCountry(selectedCountry.getCountryName());

        SharedData.getInstance().setSelectedFilter(filter);

        Intent intent = new Intent(this, SimpleFragmentActivity.class);
        intent.putExtra(Constants.Keys.FragmentId, Constants.Fragment.SearchDoctor.ordinal());
        startActivity(intent, true);

    }

    private void navigateToLoginScreen() {
        SharedData.getInstance().setPostQuestion(true);
        SharedData.getInstance().setPostQuestionText(etQuestion.getText().toString());
        SharedData.getInstance().setPostQuestionSpecialityIndex(selectedSpecialtyIndex);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent, true);

    }

    private void openDashboard() {

        SharedData.getInstance().setSelectedFilter(null);

        Intent intent;
        if (CacheManager.getInstance().isUserLoggedIn()) {
            if (CacheManager.getInstance().getCurrentUser().getUserType() == Constants.UserType.Patient) {
                intent = new Intent(this, PatientDashboardActivity.class);
            } else {
                intent = new Intent(this, DoctorDashboardActivity.class);
            }
        } else {
            intent = new Intent(this, LoginActivity.class);
        }
        startActivity(intent, true);

    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedData.getInstance().setPostQuestion(false);
    }
}
