package com.imedhealthus.imeddoctors.auth.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.imedhealthus.imeddoctors.R;
import com.imedhealthus.imeddoctors.common.activities.BaseActivity;
import com.imedhealthus.imeddoctors.common.models.SharedData;
import com.imedhealthus.imeddoctors.common.networking.CustomCallback;
import com.imedhealthus.imeddoctors.common.networking.RetrofitJSONResponse;
import com.imedhealthus.imeddoctors.common.networking.WebServicesHandler;
import com.imedhealthus.imeddoctors.common.utils.AlertUtils;
import com.imedhealthus.imeddoctors.common.utils.Constants;
import com.imedhealthus.imeddoctors.common.utils.GenericUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewPasswordActivity extends BaseActivity {


    @BindView(R.id.et_verification_code)
    EditText etVerificationCode;
    @BindView(R.id.et_password)
    EditText etNewPassword;
    @BindView(R.id.et_confirm_password)
    EditText etConfirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);
        ButterKnife.bind(this);
    }

    private boolean validateFields() {

        String errorMsg = null;
        if (etVerificationCode.getText().toString().isEmpty() ||etConfirmPassword.getText().toString().isEmpty()
                || etConfirmPassword.getText().toString().isEmpty()) {
            errorMsg = "Kindly fill all fields";
        }else if(!GenericUtils.isValidPassword(etNewPassword.getText().toString())){
            errorMsg = "New Password does not comply with passwod policy | Minimum length of 8 and contraining at least 1 upper, lower and numeric character (a-z,A-Z,0-9)";
        }else  if (!etNewPassword.getText().toString().equals(etConfirmPassword.getText().toString())) {
            errorMsg = "Password Mismatched";
        }
        else  if (!etVerificationCode.getText().toString().equals(SharedData.getInstance().getSelectedVerificationCode())) {
            errorMsg = "Invalid verification status";
        }

        if (errorMsg != null) {
            AlertUtils.showAlert(this, Constants.ErrorAlertTitle, errorMsg);
            return false;
        }
        return true;

    }


    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_back:
                onBackPressed(true);
                break;

            case R.id.btn_send:
                if (validateFields()) {
                    sendPassword();
                }
                break;

            default:
                break;
        }
    }
    //Webservices
    private void sendPassword() {

        AlertUtils.showProgress(this);
        String password = etNewPassword.getText().toString();


        WebServicesHandler.instance.changeForgotPassword(SharedData.getInstance().getSelectedLoginId(),password, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                AlertUtils.dismissProgress();

                if (!response.status()) {
                    AlertUtils.showAlert(NewPasswordActivity.this, Constants.ErrorAlertTitle, response.message());
                    return;
                }

                moveToLogin();

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

                AlertUtils.dismissProgress();
                AlertUtils.showAlert(NewPasswordActivity.this, Constants.ErrorAlertTitle, Constants.GenericErrorMsg);

            }
        });

    }

    private void moveToLogin() {
        SharedData.getInstance().setSelectedLoginId(null);
        SharedData.getInstance().setSelectedVerificationCode(null);

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent, true);
    }

}
